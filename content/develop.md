---
title: "KDE Developers Area"
description: "KDE provides libraries for other KDE projects and third-parties. Learn how to use them here."
noTitle: true
type: develop
alias:
  - /developerplatform
---

