---
title: "KDE Plasma 5.18: More Convenient and with Long Term Stability"
description: "Plasma 5.18 adds an emoji selector, user feedback capabilties, a global edit mode and improves and expands System Settings, the Discover software manager, widgets, GTK integration and much more."
release: "plasma-5.18.0"
version: "5.18.0"
date: 2020-02-11
changelog: plasma-5.17.5-5.18.0-changelog
---

{{< peertube "cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5" >}}

{{<figure src="/announcements/plasma-5.18/global_themes.png" alt="Plasma 5.18" class="text-center" width="600px" caption="KDE Plasma 5.18">}}

Tuesday, 11 February 2020.

Plasma 5.18 LTS is out!

A brand new version of the Plasma desktop is now available. In Plasma 5.18 you will find neat new features that make notifications clearer, settings more streamlined and the overall look more attractive. Plasma 5.18 is easier and more fun to use, while at the same time allowing you to be more productive when it is time to work.

Apart from all the cool new stuff, Plasma 5.18 also comes with an LTS status. LTS stands for "Long Term Support". This means 5.18 will be updated and maintained by KDE contributors for the next two years (regular versions are maintained for 4 months). If you are thinking of updating or migrating your school, company or organization to Plasma, this version is your best bet, as you get the most stable version of Plasma *and* all the new features too.

## Plasma

Plasma 5.18 is even more user-friendly as we have added more features that let you work, play and express yourself better. Take the new <i>Emoji Selector</i>: it is literally always just two keystrokes away. Hold down the Meta (Windows) key and press the period (.) and it will pop up. Click on the icon that best represents your feelings and you can paste the emoji into your email, social media post, text message or even your terminal.

{{<figure src="/announcements/plasma-5.18/emoji.png" alt="Emoji selector" caption="$3" width="600px" >}}

You can also find the emoji finder in the <i>Kickoff</i> application launcher — the main menu button usually located in the bottom left-hand corner of the screen. Look under <i>Applications</i> &rarr; <i>Utilities</i>.

Another big change is the new <i>Global Edit</i> mode. The old desktop toolbox, the menu that used to live by default in the top right corner of your screen, is gone now, leaving a roomier, tidier desktop. Instead, we are introducing the <i>Global Edit</i> bar that you can activate by right-clicking on an empty area of your desktop and choosing "<i>Customize layout</i>" from the popup menu. The <i>Global Edit</i> bar appears across the top of the screen and gives you access to widgets, activities (extra workspaces you can set up separately from your regular virtual desktops) and the desktop configuration set of options.

{{<figure src="/announcements/plasma-5.18/customize-layout.png" alt="The new Global Edit mode" caption="$3" width="600px" >}}

Continuing with improvements to the overall look, Plasma 5.18 comes with better support for GTK applications using client-side decorations. These applications now show proper shadows and the resize areas for them. GTK apps now also automatically inherit Plasma's settings for fonts, icons, mouse cursors and more.

To help relax your eyesight, there's a new system tray widget that lets you toggle the <i>Night Color</i> feature. You can also configure keyboard shortcuts to turn <i>Night Color</i> and <i>Do Not Disturb</i> modes on or off.

{{<figure src="/announcements/plasma-5.18/NightColorWidget.png" alt="Night color widget" caption="$3" width="600px" >}}

Still down in the system tray, the <i>Audio Volume System</i> widget now sports a more compact design that makes it easier to choose the default audio device. There is also a clickable volume indicator for the apps shown in the <i>Task Manager</i> that are playing audio.

{{<figure src="/announcements/plasma-5.18/InteractiveVolumeTaskManager.png" alt="Per-application mute button in Task Manager" caption="$3" width="600px" >}}

Then there are all the much more subtle details that help make Plasma look and feel smoother and slicker. These include from the tweaks to the weather widget, that now lets you view the wind conditions, down to the details of the <i>Kickoff</i> application launcher which is more touch-friendly and shows a user icon which is now circular instead of square.

## Notifications

The new more rounded and smoother Plasma look has also arrived to the timeout indicator on notifications, as this is also circular and surrounds the close button, making for a more compact and modern look.

But changes to notifications are not only aesthetic, as Plasma now shows you a notification when a connected Bluetooth device is about to run out of power. Notifications also add interactivity to their capabilities and the <i>File downloaded</i> notification now comes with a draggable icon, so you can quickly drag the file you just downloaded to where you want to keep it.

{{<figure src="/announcements/plasma-5.18/draggingDownloadwiget.png" alt="Drag and drop files from the notifications" caption="$3" width="600px" >}}

## System Settings

There are quite a few new things in Plasma 5.18's <i>System Settings</i>. First and foremost is the optional <I>User Feedback</I> settings. These are disabled by default to protect your privacy.

That said, if you do decide to share information about your installation with us, none of the options allows the system to send any kind of personal information. In fact, the <i>Feedback settings</i> slider lets you decide how much you want to share with KDE developers. KDE developers can later use this information to improve Plasma further and better adapt it to your needs.

{{<figure src="/announcements/plasma-5.18/UserFeedback.png" alt="User feedback settings" caption="$3" width="600px" >}}

Talking of adapting, if Plasma is famous for anything, it is for its flexibility and how it can be tweaked to all tastes. We made the process of customizing your desktop even more pleasant by redesigning the <i>Application Style</i> settings with a grid view. This makes it easier to see what your applications will look like once you pick your favorite style.

{{<figure src="/announcements/plasma-5.18/StyleLayout.png" alt="Application style settings" caption="$3" width="600px" >}}

The grid view look with big previews has been extended to many other areas of <i>System Settings</i>. When you search for new themes to download in <i>Global Themes</i>, you can now appreciate the different designs in all their glory.

{{<figure src="/announcements/plasma-5.18/global_themes.png" alt="Global Theme: Get Hot New Stuff" caption="$3" width="600px" >}}

Another visual aspect that you can tweak is the animations of the windows. We have added a slider for that under <i>General Behavior</i>: pull it all the way to the right, and windows will pop into existence instantaneously. Drag it all the way to the left, and they will sloooowly emerge from the task bar. Our advice is to go with the Goldilocks option and leave it somewhere in the middle. That provides enough visual cues for you to see what is happening, but is not so slow as to make it a drag to use.

If you find all these options overwhelming, don't worry: the <i>System Settings</i> search function has been much improved and will help you find the feature you want to change quickly. Start typing into the box at the top of the list and all the unrelated options will disappear, leaving you with a list of exactly what you're looking for.

## Discover

Discover is Plasma's visual software manager. From Discover, you can install, remove and update applications and libraries with a few clicks.

When you open Discover in Plasma 5.18, your cursor will be in the search field, inviting you to immediately start looking for the programs you want to install. But it is not only applications you can find here; you can also search for addons for Plasma (such as themes, splash screens, and cursor sets) and you can read the comments for each package users have written and find out what others think.

{{<figure src="/announcements/plasma-5.18/discover.png" alt="Plasma Discover" caption="$3" width="600px" >}}

## More

Finally, we have improved the overall performance of Plasma on graphics hardware and decreased the amount of visual glitches in apps when using fractional scaling on X11. We have also included NVIDIA GPU stats into KSysGuard.

## New Since 5.12 LTS

For those upgrading from our previous Long Term Support release (Plasma 5.12), here are some of the highlights from the last two years of development:

- Completely rewritten notification system
- Browser integration
- Redesigned system settings pages, either using a consistent grid view or an overhauled interface
- GTK applications now respect more settings, use KDE color schemes, have shadows on X11, and support the global menu
- Display management improvements, including new OSD and widget
- Flatpak portal support
- Night Color feature
- Thunderbolt device management
