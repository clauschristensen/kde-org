---
title: Mobile Target Makes KDE Platform Lose Weight
date: "2011-01-26"
hidden: true
---

<h2>KDE Releases Development Platform 4.6.0</h2>
<p>
KDE proudly announces the release of KDE Platform 4.6. The foundation for the Plasma workspaces and the KDE applications has seen significant improvements in many areas. Besides the introduction of new technology, a lot of work has been spent on improving the performance and stability of the underlying pillars of the Platform.
</p>
<h2>Low-Fat Mobile Profile Cuts Dependency Chains</h2>
<p>
By modularizing the KDE libraries further, parts of the KDE platform can now be built for mobile and embedded target systems. Reduced cross-library dependencies and allowing certain features to be disabled, allow KDE frameworks to now be easily deployed on mobile devices. The mobile profile is already used for mobile and tablet versions of KDE applications, such as <a href="http://userbase.kde.org/Kontact_Touch">Kontact Touch</a>, KDE's mobile office suite and the <a href="http://www.notmart.org/index/Software/Plasma:_now_comes_in_tablets">tablet</a> and handset Plasma user interfaces.
</p>

<h2>Harness the Power of Plasma with QML</h2>
<p>With the new release, the Plasma framework gains support for Plasma widgets written in <b>QML</b>, Qt Quick's declarative UI language. While existing widgets continue to function just as before, QML is now the preferred way to create new widgets. Plasma data engines receive new features, including the ability to share files using a Javascript plugin and a storage service allowing data engines to cache data for offline usage. The new Plasma KPart is now available, making it easy for developers to integrate these new and of course previously existing plasma technologies inside their applications – there is already work on using the Plasma framework in Kontact and Skrooge.
</p>

<h2>UPower, UDev and UDisks Support, Metadata Backup</h2>
<p>
Thanks to Solid's new <b>UPower, UDev and UDisks</b> backends, the deprecated HAL is no longer needed to manage hardware on Linux. Applications do not need to be updated to make use of these new backends. The HAL backend is still available for systems that do not support UPower.
</p>
<p>
<b>Nepomuk</b>, the semantic desktop technology of the KDE Platform, gained <a href="http://vhanda.in/blog/2010/11/nepomuk-backup/">backup and synchronisation</a> support to make transferring meta-data between devices easier. Users can now back up their semantic data using a graphical interface. Additional synchronization features are currently only available from the command line.
</p>

<div class="text-center">
	<a href="/announcements/announce-4.6/46-p01.png">
	<img src="/announcements/announce-4.6/thumbs/46-p01.png" class="img-fluid" alt="Semantic data can now be easily backed up and restored using a graphical interface">
	</a> <br/>
	<em>Semantic data can now be easily backed up and restored using a graphical interface</em>
</div>
<br/>

<h2>KWin Becomes Scriptable, Oxygen-GTK Improves cross-desktop Integration</h2>
<p>
<b>Kwin</b>, KDE's window and compositing manager now has a <a href="http://rohanprabhu.com/?p=116">scripting interface</a>. This gives advanced users and distributors more power over the behavior of windows in a KDE Plasma workspace – for example it is possible to set a window as “keep above” until the window is maximized, treating it as a normal window that can be covered when maximized. When the window is un-maximized, automatically set it as “keep above” again. The KWin team is currently working on support for OpenGL-ES, expected to be ready for prime-time with the release of 4.7 in summer 2011, so that KWin will be usable on handheld systems.
</p>
<p>
<b>Oxygen</b>, the visual component of the KDE Platform has also seen extensive work with a complete remake of all mimetype icons, the introduction of the <a href="http://hugo-kde.blogspot.com/2010/11/oxygen-gtk.html">Oxygen-GTK theme engine</a>, enabling better integration of GTK apps into KDE Plasma Workspaces, including gradients and many of the features users expect from Oxygen.
</p>

<div class="text-center">
	<a href="/announcements/announce-4.6/46-p02.png">
	<img src="/announcements/announce-4.6/thumbs/46-p02.png" class="img-fluid" alt="The Oxygen-GTK theme allows KDE and GTK applications to be mixed seamlessly">
	</a> <br/>
	<em>The Oxygen-GTK theme allows KDE and GTK applications to be mixed seamlessly</em>
</div>
<br/>

<h2>New Bluetooth Framework Allows Easy Device Pairing</h2>
<p>
<b>BlueDevil</b>, a brand new Bluetooth framework, allows easy device pairing and management. BlueDevil replaces KBluetooth and builds on top of the BlueZ stack. It allows the following:
</p>
<ul>
    <li>You can now use a wizard to pair devices,</li>
    <li>Browse files on bluetooth devices using Dolphin or Konqueror, and</li>
    <li>Manage bluetooth settings from KDE’s System Management or the system tray.</li>
    <li>The new framework also ‘listens’ for incoming requests, for example, to receive files or to enter a PIN code.</li>
</ul>

<h4>Installing the KDE Development Platform</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.6.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.6.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.6.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.6.0">4.6 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.6.0 may be <a
href="http://download.kde.org/stable/4.6.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.6.0
  are available from the <a href="/info/4.6.0#binary">4.6.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.7.2. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
Graphics drivers may under certain conditions fall back to XRender rather than OpenGL for compositing. If you have issues with notably slow graphics performance, switching off desktop effects is likely to help, depending on the graphics driver and setup used. In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability. 
</p>

<h4>
Credits
</h4>
<p>
These release notes have been compiled by Vivek Prakash, Stefan Majewsky, Guillaume De Bure, Nikhil Marathe, Markus Slopianka, Stuart Jarvis, Jos Poortvliet, Nuno Pinheiro, Carl Symons, Marco Martin, Sebastian Kügler, Nick P and many others in the KDE Promotion Team.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Released Today:</h2>

<h3>
<a href="../plasma">
Plasma Workspaces Put You in Control
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/announce-4.6/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.6.0" />
</a>

The<b> KDE Plasma Workspaces</b> gain from a new Activities system, making it easier to associate applications with particular activities such as work or home tasks. Revised power management exposes new features but has a simpler configuration interface. KWin, the Plasma workspace window manager, receives a new scripting and the workspaces receive visual enhancements. <b>Plasma Netbook</b>, optimized for mobile computing devices receives speed enhancements and becomes easier to use via a touchscreen interface. For more details read the <a href="../plasma">KDE Plasma Workspaces 4.6 announcement</a>.

</p>

<h3>
<a href="../applications">
KDE’s Dolphin Adds Faceted Browsing
</a>
</h3>

<p>

<a href="../applications">
<img src="/announcements/announce-4.6/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.6.0"/>
</a>
</a>
Many of the <b>KDE Application</b> teams have also released new versions. Particular highlights include improved routing capabilities in KDE’s virtual globe, Marble, and advanced filtering and searching using file metadata in the KDE file manager, Dolphin: Faceted Browsing. The KDE Games collection receives many enhancements and the image viewer Gwenview and screenshot program KSnapshot gain the ability to instantly share images to a number of popular social networking sites. For more details read the <a href="../applications">KDE Applications 4.6 announcement</a>.<br /><br />
</p>
