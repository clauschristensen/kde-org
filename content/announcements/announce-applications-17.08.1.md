---
title: KDE Ships KDE Applications 17.08.1
description: KDE Ships KDE Applications 17.08.1
date: 2017-09-07
version: 17.08.1
changelog: fulllog_applications-17.08.1
layout: application
---

{{% i18n_var "September 7, 2017. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-17.08.0" %}}

More than 20 recorded bugfixes include improvements to Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello, KDE games, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.36" %}}

