---
title: "KDE Plasma 5.20.2, bugfix Release for October"
description: Today KDE releases a bugfix update to KDE Plasma 5
date: 2020-10-27
changelog: plasma-5.20.1-5.20.2-changelog
layout: plasma
draft: false
---

{{< plasma-5-20-video >}}

Tuesday, 27 October 2020.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.20.2" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in October 2020 with many feature refinements and new modules to complete the desktop experience." "5.20" >}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

+ Daemon: improve consistency of the lid behaviour. [Commit.](http://commits.kde.org/kscreen/420c60adfcb95b09ab352b614fdd2dc2a66dcfa8) 
+ Fix bug: Some user profile fields won't apply unless they all have unique new values. [Commit.](http://commits.kde.org/plasma-desktop/3308dee942d1982a0aba68fc41368a284afc8e21) Fixes bug [#427348](https://bugs.kde.org/427348)
