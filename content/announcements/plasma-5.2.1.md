---
title: KDE Ships Plasma 5.2.1, Bugfix Release for February
date: "2015-02-24"
description: KDE Ships Plasma 5.2.1.
release: plasma-5.2.1
layout: plasma
changelog: plasma-5.2.0-5.2.1-changelog
---

{{<figure src="/announcements/plasma-5.2/full-screen.png" alt="Dual monitor setup" class="text-center" width="600px">}}

Tuesday, 24 February 2015. Today KDE releases a bugfix update to Plasma 5, versioned 5.2.1. <a href='https://www.kde.org/announcements/plasma-5.2.0.php'>Plasma 5.2</a> was released in January with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Don't turn off the screen or suspend the computer when watching videos in a web browser
- Fix Powerdevil from using full CPU
- Show the correct prompt for a fingerprint reader swipe
- Show correct connection name in Plasma Network Manager
- Remove kdelibs4support code in many modules
- Fix crash when switching to/from Breeze widget style
- In KScreen fix crash when multiple EDID requests for the same output are enqueued
- In KScreen fix visual representation of output rotation
- In Oxygen style improved rendering of checkbox menu item's contrast pixel, especially when selected using Strong highlight.
- In Plasma Desktop improve rubber band feel and consistency with Dolphin.
- In Plasma Desktop use smooth transformation for scaling down the user picture
- When setting color scheme information for KDElibs 4, don't read from KF5 kdeglobals
- Baloo KCM: Show proper icons (porting bug)
