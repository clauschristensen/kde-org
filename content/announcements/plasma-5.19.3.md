---
title: KDE Plasma 5.19.3, bugfix Release for July
version: 5.19.3
layout: plasma
changelog: plasma-5.19.2-5.19.3-changelog
date: 2020-07-07
---


{{% plasma-5-19-video %}}

Tuesday, 7 July 2020.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.19.3" >}}

{{< i18n_var "<a href='https://kde.org/announcements/plasma-%[1]s.0'>Plasma %[1]s</a> was released in June 2020 with many feature refinements and new modules to complete the desktop experience." "5.19" >}}

This release adds a fortnight's worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:

+ KWin: Make sure tablet coordinates take decorations into account. <a href="https://commits.kde.org/kwin/311094ad8be4c76df2a4655fe71e7085fa9c4e14">Commit.</a> Fixes bug <a href="https://bugs.kde.org/423833">#423833</a>
+ Fix a KCM crash when no file manager is installed. <a href="https://commits.kde.org/plasma-desktop/74753128180a36c9fba154914b3a2384025c4893">Commit.</a> Fixes bug <a href="https://bugs.kde.org/422819">#422819</a>
+ Powerdevil: Fix compilation with Qt 5.15, this hit the time bomb too. <a href="https://commits.kde.org/powerdevil/0fa63b8685f82e5f626058dfc0f9461ae158599b">Commit.</a>
