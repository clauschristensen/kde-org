---
title: KDE Plasma 5.9.3, Bugfix Release for February
release: "plasma-5.9.3"
version: "5.9.3"
description: KDE Ships Plasma 5.9.3.
date: 2017-02-28
layout: plasma
---

{{< youtube id="lm0sqqVcotA" >}}

{{<figure src="/announcements/plasma-5.9/plasma-5.9.png" alt="KDE Plasma 5.9 " class="text-center" width="600px" caption="KDE Plasma 5.9">}}

Tuesday, 28 February 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "bugfix" "5.9.3." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.9" "January" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "two week's" %}}

- KSSHAskPass: Fix git ssh password detection. <a href='https://commits.kde.org/ksshaskpass/408c284f6a35694f7e6be76c3416b80f3ef7fdba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376228'>#376228</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4540'>D4540</a>
- Fix default fixed font in fonts kcm. <a href='https://commits.kde.org/plasma-desktop/d02de0db36a35cc2e66ff91b8f5796962fa4e04e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4794'>D4794</a>
- Fix crash when switching activities. <a href='https://commits.kde.org/plasma-workspace/05826bd5ba25f6ed7be94ff8d760d6c8372f148c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376055'>#376055</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4631'>D4631</a>

<a href="../plasma-5.9.2-5.9.3-changelog">{{% i18n_var "Full Plasma %[1]s changelog" "5.9.3" %}}</a>
