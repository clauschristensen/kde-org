---
title: KDE Plasma 5.18.5, bugfix Release for May
release: "plasma-5.18.5"
version: "5.18.5"
description: KDE Ships Plasma 5.18.5
date: 2020-05-05
changelog: plasma-5.18.4-5.18.5-changelog
---

{{< peertube "cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5" >}}

{{<figure src="/announcements/plasma-5.18/plasma-5.18.png" alt="Plasma 5.18" class="text-center" width="600px" caption="KDE Plasma 5.18">}}

Tuesday, 5 May 2020.

{{% i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.18.5" %}}

{{% i18n_var "<a href='https://kde.org/announcements/plasma-%[1]s.0'>Plasma %[1]s</a> was released in February 2020 with many feature refinements and new modules to complete the desktop experience." "5.18" %}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- KCM Colors fix apply button always disabled. <a href="https://commits.kde.org/plasma-desktop/443b028a1cb344604751963770105903b0a9e391">Commit.</a> Fixes bug <a href="https://bugs.kde.org/418604">#418604</a>. Phabricator Code review <a href="https://phabricator.kde.org/D27944">D27944</a>
- [Notifications] Don't show do not disturb end date beyond 100 days. <a href="https://commits.kde.org/plasma-workspace/a5c9e000b9c9c814faacfecba7f6bf42eea64943">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D28740">D28740</a>
- xdg-desktop-portal-kde: ScreenSharing: close dialogs when session is closed. <a href="https://commits.kde.org/xdg-desktop-portal-kde/556f26ac2db57fc6087a958f8eb75c1bda4592c0">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D28618">D28618</a>
