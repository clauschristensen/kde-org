---
title: KDE Plasma 5.15.4, Bugfix Release for April
release: "plasma-5.15.4"
version: "5.15.4"
description: KDE Ships Plasma 5.15.4.
date: 2019-04-02
changelog: plasma-5.15.3-5.15.4-changelog
layout: plasma
---

{{<figure src="/announcements/plasma-5.15/plasma-5.15-apps.png" alt="Plasma 5.15" class="text-center" width="600px" caption="KDE Plasma 5.15">}}

Tuesday, 2 April 2019.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.15.4." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in February with many feature refinements and new modules to complete the desktop experience." "5.15" %}}

This release adds three week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- [platforms/x11] Force glXSwapBuffers to block with NVIDIA driver. <a href="https://commits.kde.org/kwin/3ce5af5c21fd80e3da231b50c39c3ae357e9f15c">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D19867">D19867</a>
- Don't allow refreshing during updates. <a href="https://commits.kde.org/discover/84f70236f4d98ee53c83ec7fa30a396754e332ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/403333">#403333</a>
- The crash dialog no longer cuts off text after backtracing. <a href="https://commits.kde.org/drkonqi/cc640cea3e9cc1806000804bbf424cb611622e45">Commit.</a> Fixes bug <a href="https://bugs.kde.org/337319">#337319</a>. Phabricator Code review <a href="https://phabricator.kde.org/D19390">D19390</a>
