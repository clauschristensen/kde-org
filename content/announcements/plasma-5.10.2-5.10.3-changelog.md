---
title: Plasma 5.10.3 Complete Changelog
version: 5.10.3
hidden: true
plasma: true
type: fulllog
---

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- - hide shadow when mask is empty. <a href='https://commits.kde.org/breeze/466c4bab2ade755b8016f9e99c4d4801a65fb4f0'>Commit.</a> See bug <a href='https://bugs.kde.org/379790'>#379790</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Simplify KNSBackend fetch logic. <a href='https://commits.kde.org/discover/3bb34859dcb67a1daf17929198a68eb7c9b52b5e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380138'>#380138</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6191'>D6191</a>
- Ignore updates for which we didn't get latest commit. <a href='https://commits.kde.org/discover/8f1ff90762c5962b65d3cd484eeae5e61f5d92ae'>Commit.</a>
- Improvements in installation or updates for flatpak backend. <a href='https://commits.kde.org/discover/3a2ad5ca8b6d45fca96f51baa4e60572dba754a3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6298'>D6298</a>
- Flatpak backend: display a popup with error when update or installation fails. <a href='https://commits.kde.org/discover/fe5de3ff7146cbeec4d516b86d7aecaf08b4cf10'>Commit.</a>
- Avoid starting update transactions twice. <a href='https://commits.kde.org/discover/4067dec5d27b00afc5a4bf8d10b4de6f75df412a'>Commit.</a>
- Remove unused code. <a href='https://commits.kde.org/discover/562ab1d3e8d0816a9d93aa61826c3db066db9b1f'>Commit.</a>
- Make sure changelogs are displayed properly. <a href='https://commits.kde.org/discover/1ad1a04a51e28c88bc52d3fb49a00847f79b8273'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381248'>#381248</a>
- Fix crash when using Discover during updates. <a href='https://commits.kde.org/discover/005d8742c1db89233f7a9b22a72ea71822df6dab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370906'>#370906</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Fix no-display of CPU bars per core (and fix some warnings). <a href='https://commits.kde.org/kdeplasma-addons/2b8482906779bc1e1196ad616c53ef6f66af1634'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129838/'>r/129838/</a>

### <a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a>

- Support mercurial (hg) ssh prompts. <a href='https://commits.kde.org/ksshaskpass/4a6d3f932c3627e0f4d3f1a452ce1097c55f070b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380085'>#380085</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6327'>D6327</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Properly block the edge also for touch screen edges. <a href='https://commits.kde.org/kwin/6267d597311ccea26a8e70d57bd730ad13d146c2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380476'>#380476</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6304'>D6304</a>
- [platforms/x11] Add support for GLX_NV_robustness_video_memory_purge. <a href='https://commits.kde.org/kwin/97fa72ee48b7525e722822e7d7d41bb08343e337'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344326'>#344326</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6344'>D6344</a>
- Fix double click on window deco if tripple clicked. <a href='https://commits.kde.org/kwin/152be60cc04f81fb54fdbe0afec41470aa188d54'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361241'>#361241</a>
- Make shadows work for windows 100% width or height. <a href='https://commits.kde.org/kwin/b7cb301deb3b191c7ff0bd04d87d6c1b93d90407'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380825'>#380825</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6164'>D6164</a>
- [tabbox] Fix casts to Client where it should be AbstractClient. <a href='https://commits.kde.org/kwin/3b9ccc65a3a853ee5212486f69918eddd4db1e0a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6273'>D6273</a>
- Fix switch desktop on screenedge while resizing a Wayland window. <a href='https://commits.kde.org/kwin/7a3c2926122603752916f8b6ecc00a3333528707'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6264'>D6264</a>
- Fix switch desktop through edge when moving window. <a href='https://commits.kde.org/kwin/c45e165514ec70314d721d6e0238431ab92bd441'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380440'>#380440</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6257'>D6257</a>
- Workaround Qt regression of no longer delivering events for the root window. <a href='https://commits.kde.org/kwin/a6dee74ee455d1da47dd5c9d55a84adbb5e1426a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360841'>#360841</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6258'>D6258</a>
- Better handle cases when the xkb keymap fails to be created. <a href='https://commits.kde.org/kwin/0df09a8cbbb1caa217c8deb59700bd12f27e45b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381210'>#381210</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6260'>D6260</a>

### <a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a>

- - hide shadow when mask is empty. <a href='https://commits.kde.org/oxygen/0853eac4dfdc96b36006c6bc5f3277a46ecb424d'>Commit.</a> See bug <a href='https://bugs.kde.org/379790'>#379790</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Fix clicking outside of preview popups to dismiss them corrupting mouse state. <a href='https://commits.kde.org/plasma-desktop/20bf7ec11458afdf7b55d7ab2f0fe338f32d1081'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380982'>#380982</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6247'>D6247</a>
- Fix crashes with 'Drag me' in touchpad test area. <a href='https://commits.kde.org/plasma-desktop/5c3b8c7afa4a00fccd820d1a34975b0791cda257'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346900'>#346900</a>. Fixes bug <a href='https://bugs.kde.org/366113'>#366113</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6334'>D6334</a>
- Fix DND onto Task Manager for groups, group dialog scrollbar. <a href='https://commits.kde.org/plasma-desktop/84300b516b799fae94fefd7a9a418e5584d37987'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379888'>#379888</a>. Fixes bug <a href='https://bugs.kde.org/379037'>#379037</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6231'>D6231</a>
- Remove implicit use of QtQml in Qt 5.7. <a href='https://commits.kde.org/plasma-desktop/89a08418b3e8815a6eb610067f521cee6218b8b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380240'>#380240</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6007'>D6007</a>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

- KDE QFileDialog helper: support name filters without parenthesis. <a href='https://commits.kde.org/plasma-integration/4736950ec2e7073d1c317acc7f1a7946e33d06cc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6351'>D6351</a>
- Introduce KDE_NO_GLOBAL_MENU env variable to disable global menu. <a href='https://commits.kde.org/plasma-integration/1ba4bca8342ac3d55bf29bdd8f622cd304e11816'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6310'>D6310</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- Context: Return early from connectToDaemon when already connected. <a href='https://commits.kde.org/plasma-pa/70f0903142968ce950e7d05341fc3d9abbecddd4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381411'>#381411</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6282'>D6282</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [xembedsniproxy] Free windowAttributes in every case. <a href='https://commits.kde.org/plasma-workspace/d45584d1de4fdedd0e43c7b1d6d4215ca3abfb64'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6389'>D6389</a>
- Use KRun::runApplication when possible; split out and share more code. <a href='https://commits.kde.org/plasma-workspace/ee79239a5de72ea7349f593357698dd50f78c30a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6358'>D6358</a>
- Use KRun::runApplication, add missing KActivities::notifyAccessed calls. <a href='https://commits.kde.org/plasma-workspace/d1b5447f57e4c233523dd3fc0e12f4c863957f56'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381500'>#381500</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6354'>D6354</a>
- [AppMenu Applet] Workaround Connections bug in Qt >= 5.8. <a href='https://commits.kde.org/plasma-workspace/f00e4eb97e5d6a6abe76269342653a93f6b53245'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375535'>#375535</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6335'>D6335</a>
- [AppMenu Applet] Wire up DBusMenuImporter::actionActivationRequested. <a href='https://commits.kde.org/plasma-workspace/92a7089e9a103e5c7f93a46aaf67b01460dd8cb0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376726'>#376726</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6336'>D6336</a>
- Make attaching backtraces to existing bug reports work. <a href='https://commits.kde.org/plasma-workspace/774a23fc0a7c112f86193ee6e07947fee6282ef4'>Commit.</a>
- [xembedsniproxy] Fix memleak and possible crash. <a href='https://commits.kde.org/plasma-workspace/a17de5957d4ba0f07c77fa99860c9046ff8aa1be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6252'>D6252</a>

### <a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a>

- Revert "skip the disabled backlight device". <a href='https://commits.kde.org/powerdevil/5c57cf64b5e5c880b1a5f3a0177293f6958e1b9a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381114'>#381114</a>. Fixes bug <a href='https://bugs.kde.org/381199'>#381199</a>
