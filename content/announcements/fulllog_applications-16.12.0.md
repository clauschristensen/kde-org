---
title: KDE Applications 16.12.0 Full Log Page
type: fulllog
version: 16.12.0
hidden: true
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix Bug 373363 - Kmail folder properties aren't shown after closing them. <a href='http://commits.kde.org/akonadi/4795732f92667bf022ce972298a61ceab6ec3780'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373363'>#373363</a></li>
<li>Add remoteid in debug info + use removeid in qhash too. <a href='http://commits.kde.org/akonadi/eab92c2cad58d59a7132812395aa86acde9fb654'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129527'>#129527</a></li>
<li>Fix relation copy and valid method. <a href='http://commits.kde.org/akonadi/1f37580aa509d6eaddea6f2e638366cf09b5517a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129515'>#129515</a></li>
<li>Fix some clazy warning. <a href='http://commits.kde.org/akonadi/45922f5fa13e08b2b14856d8da1b327c252ee5e1'>Commit.</a> </li>
<li>Use isEmpty. <a href='http://commits.kde.org/akonadi/f7dbed669675f038b40be7f84c3c72d7d83155d6'>Commit.</a> </li>
<li>Cache end value. <a href='http://commits.kde.org/akonadi/2e5f83261f5b837bad43e0824e59aa14689b8c64'>Commit.</a> </li>
<li>Reserve some size. <a href='http://commits.kde.org/akonadi/3a9f83f5933041c23a644d1ac9e9d27d492bbcd5'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/akonadi/474a2b1dd807c3590360dad2d7157fbee9f9af6b'>Commit.</a> </li>
<li>Use isEmpty. <a href='http://commits.kde.org/akonadi/6d544f0c9091e92f47a765bf3de4ebf436d69215'>Commit.</a> </li>
<li>Fix missing / char in constructing default socket path. <a href='http://commits.kde.org/akonadi/c8ae486e9a08e9d54f4b9b46341477aa660dccb4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129350'>#129350</a></li>
<li>Port away from obsolete QStyleOptionViewItemV4. <a href='http://commits.kde.org/akonadi/53e3e69f5417496386ca2eac8c251ce5205372b4'>Commit.</a> </li>
<li>Knut: use qCDebug. <a href='http://commits.kde.org/akonadi/29fcd2d8f20e8855ee145414a70c1f9e3446865e'>Commit.</a> </li>
<li>Fix the kdepim-runtime build issue caused by 88eeb24. <a href='http://commits.kde.org/akonadi/9b6defee312155be79ad88190f90ce2afc0653e6'>Commit.</a> </li>
<li>Fix crash when Scotland is in the wrong thread. <a href='http://commits.kde.org/akonadi/e61d2d03e9fc9d8d353c6906984cf7c6b1f4f2ea'>Commit.</a> </li>
<li>Fix Krazy issues. <a href='http://commits.kde.org/akonadi/88eeb24aaa9757f680f332e6895337adeb00f7b7'>Commit.</a> </li>
<li>Fix logging category name in akdebug. <a href='http://commits.kde.org/akonadi/f5f49f9163e02fd20fbce5bcbbea729f22f7dd55'>Commit.</a> </li>
<li>Fix FakeAkonadiServer-based tests. <a href='http://commits.kde.org/akonadi/78947797001deba456a6b2ce525e7ff6484a2c7d'>Commit.</a> </li>
<li>Fix DbInitializerTest. <a href='http://commits.kde.org/akonadi/6055f43757d5d028b5fba63041ee95465ca19260'>Commit.</a> </li>
<li>Bump protocol version in ProtocolTest. <a href='http://commits.kde.org/akonadi/9178715877dcb12c451c8ed9e0e38199aa89fa06'>Commit.</a> </li>
<li>Introduce concept of database generation. <a href='http://commits.kde.org/akonadi/d263b9360093b4423c9521cc3e5096581490e3dd'>Commit.</a> </li>
<li>Remove the unused KDSignalBlocker. <a href='http://commits.kde.org/akonadi/2aa2d64d7b45c792a26d8f5b71e635594f895c97'>Commit.</a> </li>
<li>Try to fix crash on shutdown. <a href='http://commits.kde.org/akonadi/c467dba1ef433147f645ccc82e08043cbb31d483'>Commit.</a> </li>
<li>Increase maximum minutes for "Retrieve message bodies on demand" option. <a href='http://commits.kde.org/akonadi/9376f6317bc0c0ee60e45f3474280499771b33f4'>Commit.</a> See bug <a href='https://bugs.kde.org/316888'>#316888</a></li>
<li>Now we depend against 5.6.0. <a href='http://commits.kde.org/akonadi/c96f026ca1b5c52e46d9e9a833e129fa103b4899'>Commit.</a> </li>
<li>We already depend against 5.24. <a href='http://commits.kde.org/akonadi/fa06f65c89fc83075012778f6cfcb176f25d229b'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/akonadi/f49ccad7ab43a1da4b04d15f905750539eefea3b'>Commit.</a> </li>
<li>NtfCollector: ensure ntfs always have a valid resource set. <a href='http://commits.kde.org/akonadi/73e6a4c320d652c40c2c8e948d473bee4afde1b9'>Commit.</a> </li>
<li>NtfCollector: fix incremental col stats update on mark as unread. <a href='http://commits.kde.org/akonadi/a5f7969f7cca2b5063cc13daef1086b2dbfa30a0'>Commit.</a> </li>
<li>Some more crash fixes in handling of NotificationSubscribers. <a href='http://commits.kde.org/akonadi/6f1a9e76a329eeaa5000108b839feefd84108133'>Commit.</a> </li>
<li>Check for nullptr in mSubscribers list before derefencing it. <a href='http://commits.kde.org/akonadi/c6db044c3788477eac0fd27d3b3e7676f9785acb'>Commit.</a> </li>
<li>Don't create a QPointer for a QObject in destruction. <a href='http://commits.kde.org/akonadi/6c93d25330b5d1392c71eaa099cdce045f70cca8'>Commit.</a> </li>
<li>[ETM] Disable change recording it Monitor is a CR. <a href='http://commits.kde.org/akonadi/d9f3ff0465abca616971567ebd042b224891d2fc'>Commit.</a> </li>
<li>[BIC] Let ETM be constructed with Monitor instead of ChangeRecorder. <a href='http://commits.kde.org/akonadi/48f07eae8ca198dfc6991a16edd96f0001ca6d5f'>Commit.</a> </li>
<li>Reupload monitor configuration when reconnecting to NtfBus. <a href='http://commits.kde.org/akonadi/dad52613f5aa8717ff92d076db452b9c3f9f5c20'>Commit.</a> </li>
<li>Pass --basedir to mysql_install_db. <a href='http://commits.kde.org/akonadi/37c639a158f78b810e84c84a9c42b6b63571c447'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367075'>#367075</a></li>
<li>If the mysql server is already running, try to connect. <a href='http://commits.kde.org/akonadi/2ef0fe95bd83643d0a1e69b0b59124fa76be5b2f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340813'>#340813</a></li>
<li>Reduce collection statistics queries. <a href='http://commits.kde.org/akonadi/c7944585b6e68e67d49e35d09cd148f1e1d5baed'>Commit.</a> </li>
<li>Monitor: update cache for MOVE operations. <a href='http://commits.kde.org/akonadi/254a636c748857732ec26ee53b774d3935b569e2'>Commit.</a> </li>
<li>Remove a wrong assert. <a href='http://commits.kde.org/akonadi/db0ab1e9d1bbaf7ac77fccb5ceac757e4024d75c'>Commit.</a> </li>
<li>Fix attribute external part cleanup. <a href='http://commits.kde.org/akonadi/4d83c90496deed2110b1d21e177de857ed6b5698'>Commit.</a> </li>
<li>PartStreamerTest: correctly verify older parts were removed. <a href='http://commits.kde.org/akonadi/ab1ddb2a848cea10599fa4dd322b5bb8e4415a7a'>Commit.</a> </li>
<li>Revert "Fix part version handling". <a href='http://commits.kde.org/akonadi/516defa9838ef2393fc125be8e028bacf0fdd5ac'>Commit.</a> </li>
<li>Fix NotificationManager crash on shutdown. <a href='http://commits.kde.org/akonadi/3ffb9c1f7409643a163e11de27d7068c55ed929f'>Commit.</a> </li>
<li>Fix upgrade issue in dbupdate.xml. <a href='http://commits.kde.org/akonadi/414e09a5e80fa175845b2a0a1466bd0901a71e52'>Commit.</a> </li>
<li>Remove no longer necessary moc include. <a href='http://commits.kde.org/akonadi/9606b674e6b370f3611d1b3deef50321efdfdd8a'>Commit.</a> </li>
<li>Fix logic handling in KF5AkonadiMacros.cmake. <a href='http://commits.kde.org/akonadi/cc48ba875fd253c5e2a382d876e16189579f49cb'>Commit.</a> </li>
<li>Enable MySQL and PostgreSQL tests by default. <a href='http://commits.kde.org/akonadi/4f5008d76010ff162662126357a1936b055b8ada'>Commit.</a> </li>
<li>Adapt dbinitializertest data for PSQL. <a href='http://commits.kde.org/akonadi/5902de9b81aa7201efeea57e29ab60267f545fb6'>Commit.</a> </li>
<li>Fix potential crash when subscriber disconnects. <a href='http://commits.kde.org/akonadi/7650e9f5c9800b4ba2873b122f037947e28086a0'>Commit.</a> </li>
<li>Make ChangeRecorderTest more robust. <a href='http://commits.kde.org/akonadi/8f4bcd969dfd4e243532b96643c98749ae666a2b'>Commit.</a> </li>
<li>Fix part version handling. <a href='http://commits.kde.org/akonadi/87633ed7bb8db3b22eaed5a372091fc2793ce0aa'>Commit.</a> </li>
<li>Fix AKONADI_OVERRIDE_SEARCHPLUGIN in MySQL/PSQL tests. <a href='http://commits.kde.org/akonadi/ec825e98bd694c6b7f11d163da73477ebc27e8a1'>Commit.</a> </li>
<li>PSQL: make foreign key DEFERRABLE INITIALLY DEFERRED. <a href='http://commits.kde.org/akonadi/0d123dd796966c50c35556b83bd7ebf2ec702712'>Commit.</a> </li>
<li>DataStore: append collection mimetypes atomically. <a href='http://commits.kde.org/akonadi/39aff54e8bb6a26116ffffeab37700646a944fc9'>Commit.</a> </li>
<li>Don't hardcode identifiers in pre-defined values in akonadidb.xml. <a href='http://commits.kde.org/akonadi/9e0e38adbbbad6bee1196192a55824bc620b230a'>Commit.</a> </li>
<li>DBSchema: allow specifying custom identificationColumn for PSQL. <a href='http://commits.kde.org/akonadi/37817e26d88a777d45dcb624a0766a91e73c8bdf'>Commit.</a> </li>
<li>DataStore: remove unused appendMimeType() method. <a href='http://commits.kde.org/akonadi/0d5bcf350ed0a5d0b6248493a24172c0fbfbedb4'>Commit.</a> </li>
<li>ItemStoreTest: check if RID is empty, not null. <a href='http://commits.kde.org/akonadi/99dc67b1134285abe32932e5f01f50ad0bca880e'>Commit.</a> </li>
<li>Fix MTIME handling accross multiple backends. <a href='http://commits.kde.org/akonadi/a290219c516eb2d2de955a7f1951e9a874608c82'>Commit.</a> </li>
<li>AkAppend: don't do UPDATE when nothing changed. <a href='http://commits.kde.org/akonadi/f918dafea123a82d9000b5292bb1f697b970247b'>Commit.</a> </li>
<li>Enable MySQL and PSQL integration tests by default too. <a href='http://commits.kde.org/akonadi/6e246aa9844d5e538824818721fe37129795c09f'>Commit.</a> </li>
<li>Fix server crash on shutdown. <a href='http://commits.kde.org/akonadi/fc487710a5a2486a58e29832b02334287497e270'>Commit.</a> </li>
<li>Extend ItemDeleteTest. <a href='http://commits.kde.org/akonadi/fa3ee6c55654be79a70757bee1a518c036f57d4a'>Commit.</a> </li>
<li>Fix crash in ItemRetrieverTest. <a href='http://commits.kde.org/akonadi/dbe3e080eaa5848ffe2a2378805afdff932b7a8a'>Commit.</a> </li>
<li>Monitor: fix parent col retrieval for Item Remove notifications. <a href='http://commits.kde.org/akonadi/9f8129f817291a2cafb7e7a935fa689f91e0543d'>Commit.</a> </li>
<li>Add tracing code to NotificationSubscriber (off by default). <a href='http://commits.kde.org/akonadi/cd374d5aedab0c996d170cbdfffd98d3036fba0c'>Commit.</a> </li>
<li>Add MoveHandlerTest, improve ItemMoveTest, minor MOVE fix. <a href='http://commits.kde.org/akonadi/d9650a099ec5549a9cdc35c00278a16e0a75fd75'>Commit.</a> </li>
<li>Knut: re-enable disabled code. <a href='http://commits.kde.org/akonadi/38216a7638281545b34892e8dca7d28764842fec'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/akonadi/00b20af8afa1d67a4ef37e64e999cacde328ce7a'>Commit.</a> </li>
<li>Enable KCrash in Akonadi Server. <a href='http://commits.kde.org/akonadi/769fe236f43293afdee00dd58687b820e20dd1fd'>Commit.</a> </li>
<li>Call malloc_trim() every 15 minutes to free more memory. <a href='http://commits.kde.org/akonadi/d2a2fcb3eb7d286cb09ac1ae6642f9f245e1d904'>Commit.</a> </li>
<li>Remove some noisy debug messages. <a href='http://commits.kde.org/akonadi/593ca2f40dd456ad3b37a36877cf6ee5f9df7549'>Commit.</a> </li>
<li>Fix linkhandler test ASAN error on CI. <a href='http://commits.kde.org/akonadi/49743a22f184b4948904b485c88f8af3321812b6'>Commit.</a> </li>
<li>Update ProtocolTest. <a href='http://commits.kde.org/akonadi/cd404da0442a09e97c6259198a7c7d8c10d5ac0e'>Commit.</a> </li>
<li>Reintroduce change notifications debugging. <a href='http://commits.kde.org/akonadi/93b7b841f7730bc5d5cad4a8a1bced7ef0a28032'>Commit.</a> </li>
<li>Remove not necessary method. <a href='http://commits.kde.org/akonadi/e01392d98a041b8ddfac777b5842cb1a74ddcce8'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/akonadi/6e1d02eaf723ed7fd5829bc7db68b2684fcb9638'>Commit.</a> </li>
<li>MOVE: fix crash in MMC-error handling debug output. <a href='http://commits.kde.org/akonadi/e92a7a8e20451c9bffddff43250966eb7168cc91'>Commit.</a> </li>
<li>MOVE: commit current transaction before emitting notification. <a href='http://commits.kde.org/akonadi/1adcbb3e1617446832dbc697374a43ff6effbd71'>Commit.</a> </li>
<li>AKAPPEND: Improve debug output in case of 'Multiple merge candidates' error. <a href='http://commits.kde.org/akonadi/59c3247073d916bd53f281ab8db7a1457f02dddf'>Commit.</a> </li>
<li>MOVE: fix concurrency bug and improve performance on complex moves. <a href='http://commits.kde.org/akonadi/9d1d7cdff89b6a0cd4b77fa83b59830279fe117f'>Commit.</a> </li>
<li>PrintUsage when we use unknow argument. <a href='http://commits.kde.org/akonadi/561bd5febb2183b854733b5af5335bbd89b00e8e'>Commit.</a> </li>
<li>Merge two lines. <a href='http://commits.kde.org/akonadi/59afa3115e9194fdd828063957961f14acff614a'>Commit.</a> </li>
<li>Fix removing entries from subscription. <a href='http://commits.kde.org/akonadi/34c1f14836f4d0f04b2175b70b4073384a522b16'>Commit.</a> </li>
<li>Add missing copyright header. <a href='http://commits.kde.org/akonadi/d3b1145ae114f4cd8792e23142ba4591c4cd977b'>Commit.</a> </li>
<li>Expose SubscriptionChangeNotification to Monitor (Protocol change). <a href='http://commits.kde.org/akonadi/f82aa88b10f68e91289333c7043892df1a41f22e'>Commit.</a> </li>
<li>Use KF5_VERSION for ecm too. <a href='http://commits.kde.org/akonadi/c2c0a34d4145eb8a0187348e4f51784ab8d12f66'>Commit.</a> </li>
<li>Bump minimal Frameworks dependency. <a href='http://commits.kde.org/akonadi/a3390ddc9007f42053ac4d040ceac1ee08fc82f1'>Commit.</a> </li>
<li>FakeAkonadiServer: delete old NotificationCollector in correct thread. <a href='http://commits.kde.org/akonadi/5bd20653b8dc9db40a008f571c49d539caea4713'>Commit.</a> </li>
<li>KApiDox: if several maintainers, set a list. <a href='http://commits.kde.org/akonadi/0149865f90d31af471c20c7f25ffe3fe0d8fe8f4'>Commit.</a> </li>
<li>Merge current state of Notification Stream from dev/ntf-stream branch. <a href='http://commits.kde.org/akonadi/6eaeeb2bcfcb05478f6d2eda8e37e85e32fb4785'>Commit.</a> </li>
<li>Fix typo in Monitor. <a href='http://commits.kde.org/akonadi/ae660b94dbc6357d7b58eebeb1070e56b1d10546'>Commit.</a> </li>
<li>Fix coding style. <a href='http://commits.kde.org/akonadi/634b6f92f496c22817058cc86bf8b8dc5fd32567'>Commit.</a> </li>
<li>Upload session ID as part of RegisterSubscriberCommand. <a href='http://commits.kde.org/akonadi/1d97d08337f805c95ae6060e4d04b34c1a8a18b7'>Commit.</a> </li>
<li>Fix build of Akonadi server. <a href='http://commits.kde.org/akonadi/1eae329d4e4815e0bf97771e25762807e7e1fdd2'>Commit.</a> </li>
<li>Fix NotificationManagerTest. <a href='http://commits.kde.org/akonadi/b2ea8721d40e44080c6fc32c33cd4056087869db'>Commit.</a> </li>
<li>NtfManager: process outgoing notifications in parallel. <a href='http://commits.kde.org/akonadi/f43ab1cfc55282d1b4bf38a6cc1d3dd896999afb'>Commit.</a> </li>
<li>Monitor: Print warnings about unexpected notification commands. <a href='http://commits.kde.org/akonadi/ce10712749bef7c422ac18f55b8a83f976fa2fb2'>Commit.</a> </li>
<li>Add an inter-resource move case to NotificationManagerTest. <a href='http://commits.kde.org/akonadi/d5655c6a80692a39d285b9b085d91dec89af31d5'>Commit.</a> </li>
<li>Fix build. <a href='http://commits.kde.org/akonadi/77fd01428eef6e2fddb20ff7410932230e70a35e'>Commit.</a> </li>
<li>Use Monitor's objectName() if available to identify subscriber. <a href='http://commits.kde.org/akonadi/91846f4199a7bcc8b055492360326527f72c1acb'>Commit.</a> </li>
<li>Manualy call Connection::reconnect() from Monitor. <a href='http://commits.kde.org/akonadi/c82fc387b450f08bbf87990d431476343a210a08'>Commit.</a> </li>
<li>Remove noisy debugs from Monitor and Session. <a href='http://commits.kde.org/akonadi/b08d53682bee758e5004a5050363c1076ba17d24'>Commit.</a> </li>
<li>Reject notifications in uninitialized NotificationSubscriber. <a href='http://commits.kde.org/akonadi/65d246031bc4bc47e74ff6d176552feaa5917b55'>Commit.</a> </li>
<li>Implement SubscriptionChangeNotification to monitor subscribers. <a href='http://commits.kde.org/akonadi/4a3d8d206c2a6b0032fba8793569b6e721fc757a'>Commit.</a> </li>
<li>Create a per-type ChangeNotification Protocol command. <a href='http://commits.kde.org/akonadi/040ce64b962e738f726d3478f57783feb3ddb02c'>Commit.</a> </li>
<li>Workaround a crash when destroying global statics on shutdown. <a href='http://commits.kde.org/akonadi/d54510766e87acee58030a8dc4336afce278d9c5'>Commit.</a> </li>
<li>Fix connection threads cleanup on server shutdown. <a href='http://commits.kde.org/akonadi/08bdf758310378a7bac442172e5e599042e44548'>Commit.</a> </li>
<li>Fix crash when destroying Session. <a href='http://commits.kde.org/akonadi/e023874cf5177a910e10a381029af932a808d9a9'>Commit.</a> </li>
<li>Rename the static library liblibakonadiserver.a to libakonadiserver.a. <a href='http://commits.kde.org/akonadi/b2b5de8b1069f42002166e478a0f88d526ca61e1'>Commit.</a> </li>
<li>Implement notification subscription management via Protocol. <a href='http://commits.kde.org/akonadi/f257797f1c8e10507d51696aaf9da8655c79da52'>Commit.</a> </li>
<li>Make AkonadiServer QObject, manage multiple QLocalServers for different purposes. <a href='http://commits.kde.org/akonadi/0e478e2b0fb7ed01ddf1fd4f856c3157ed9076c9'>Commit.</a> </li>
<li>Protocol: add Create/ModifySubscription commands. <a href='http://commits.kde.org/akonadi/d7a64c888753c3f0bca6cc598ffe58a7bbf9d0da'>Commit.</a> </li>
<li>Make COPY and MOVE handlers more interactive. <a href='http://commits.kde.org/akonadi/1bdde3b97122d93f99305fe3ec30755d0162bb8b'>Commit.</a> </li>
<li>Fix a loop in ItemRetriever and make it interactive, add a unit test. <a href='http://commits.kde.org/akonadi/39bcc7d5bb101188f14c4327c002f374e92d8ffa'>Commit.</a> </li>
<li>Improve documentation of ResourceBase::retrieveItems(). <a href='http://commits.kde.org/akonadi/77da41c2431ed9403779aab24f7b6d9abdc74a49'>Commit.</a> </li>
<li>AkAppend/Merge: always bump PimItem revision when merging. <a href='http://commits.kde.org/akonadi/67f395298d5eb67c9a7ec126dcade1bbdd39f986'>Commit.</a> </li>
<li>ResourceBase: add missing transaction commit, ensure parents are present. <a href='http://commits.kde.org/akonadi/62beb4a1348e1d36240f1adca62a581a0979adc8'>Commit.</a> </li>
<li>ItemRetriever: only discard request with empty parts if mFullPayload is false. <a href='http://commits.kde.org/akonadi/c926296a3f5f8f27fbbe8d5412cb19a0dda8a1e8'>Commit.</a> </li>
<li>ResourceBase: use Merge instead of Modify for FetchItems results. <a href='http://commits.kde.org/akonadi/2c9662aae78f701b84307238fde64970149fdbcb'>Commit.</a> </li>
<li>ResourceBase::itemsRetrieved: batch ModifyJobs into a transaction. <a href='http://commits.kde.org/akonadi/c7263359200f63023e9638ce61d9bf50cd6dd357'>Commit.</a> </li>
<li>Further optimize ItemRetriever batches. <a href='http://commits.kde.org/akonadi/ef3dbc38e09b95c10544ef815d75a986e6ee06b5'>Commit.</a> </li>
<li>Remove a noisy debug from SessionPrivate. <a href='http://commits.kde.org/akonadi/d9f2d24b1ca431e8bbf48880139b3103458cd5ce'>Commit.</a> </li>
<li>Add API to get and set server config path. <a href='http://commits.kde.org/akonadi/bc674b405eadf1514defc135e60373e98927b012'>Commit.</a> </li>
<li>QApplication must be first line. <a href='http://commits.kde.org/akonadi/73a5b9cffc16560605b40cd40189a9b62a87c65c'>Commit.</a> </li>
<li>ItemRetriever: group retrieval requests by Collection. <a href='http://commits.kde.org/akonadi/766c18137eca6195ae2f4742f467c7562b09e255'>Commit.</a> </li>
<li>Introduce batch Resource Item retrieval (ABI break). <a href='http://commits.kde.org/akonadi/816526018ba478fc896c892928535591c75e51b3'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/akonadi/aff413b533c98bc4e6122ee1f8c47be8853ff069'>Commit.</a> </li>
<li>Revert "QT_STATICPLUGIN is not used". <a href='http://commits.kde.org/akonadi/330b9a5fb651ca22b93362db3b6099d46b561cf0'>Commit.</a> </li>
<li>Update Akonadi documentation, enable docs generation on api.kde.org. <a href='http://commits.kde.org/akonadi/fd7e958536ca35709f048e90ad7031f4cd5555ac'>Commit.</a> </li>
<li>Having licence in same directory not clear enough for some documentors so put it explicitly into the files. <a href='http://commits.kde.org/akonadi/324f087e7ff8671e00e4b49d61d85a0e261fa5d8'>Commit.</a> </li>
<li>Remove unused define. <a href='http://commits.kde.org/akonadi/a6efd34dd6a5910ccae26b030592f5a3d04d5ab7'>Commit.</a> </li>
<li>QT_STATICPLUGIN is not used. <a href='http://commits.kde.org/akonadi/efc8f6a384c0b0568ef26cc45ae3283df7686867'>Commit.</a> </li>
<li>Akonadi_nepomuktag_resource doesn't exist from long time. <a href='http://commits.kde.org/akonadi/ef6fe86d309662c10f43b639c918db8fcbe58017'>Commit.</a> </li>
<li>Revert "Not necessary to use private  Q_SLOTS here". <a href='http://commits.kde.org/akonadi/9d76d64bfe91fd7a1cd03e45c52a8dd516f4dd2d'>Commit.</a> </li>
<li>Not necessary to use private  Q_SLOTS here. <a href='http://commits.kde.org/akonadi/92dd2b545a969668d89ccd672d40a59ef6ea160c'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-calendar' href='https://cgit.kde.org/akonadi-calendar.git'>akonadi-calendar</a> <a href='#akonadi-calendar' onclick='toggle("ulakonadi-calendar", this)'>[Show]</a></h3>
<ul id='ulakonadi-calendar' style='display: none'>
<li>Fix some clazy warning. <a href='http://commits.kde.org/akonadi-calendar/3223fb244447ed4afde75fc2ff619f25a68a5482'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/akonadi-calendar/aa7edbf99b39eb6fec119dd08bf3a463de223ab7'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/akonadi-calendar/62409960ca2172f640ca86cf6d91d426c0c886f6'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/akonadi-calendar/e7023c6f7caf4b4697cbb5a82f07826a0ad6c920'>Commit.</a> </li>
<li>Try to fix build after BIC change. <a href='http://commits.kde.org/akonadi-calendar/3601f4175e43eeaacb23bc27777dbdeafd9e6f60'>Commit.</a> </li>
<li>[BIC] Adapt to ETM ctor change. <a href='http://commits.kde.org/akonadi-calendar/8826846e2e5c46b39f398831ca362fdad71284c1'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/akonadi-calendar/d47c178752902654928550f51e433ada3b4b56e8'>Commit.</a> </li>
<li>Add parent. <a href='http://commits.kde.org/akonadi-calendar/ef664236984ad7246ac8d176577cafa6d1b1457d'>Commit.</a> </li>
<li>Use Q_SIGNALS here too. <a href='http://commits.kde.org/akonadi-calendar/b014de580238db71fe9f0eff37121ff86dcf3bc7'>Commit.</a> </li>
<li>Use Q_SLOTS. <a href='http://commits.kde.org/akonadi-calendar/a76a3bc0e6a731ccded750d41586f1d70da550d7'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/akonadi-calendar/3308d38b80de177eff5718f0fc3f7fd2ef344578'>Commit.</a> </li>
<li>Remove unused variable + fix typo. <a href='http://commits.kde.org/akonadi-calendar/2705260d76e243b9daf290275f5ed502e3cc4c5e'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/akonadi-calendar/affc3f857e1e2134bb431f2642cd63dbba005c4e'>Commit.</a> </li>
<li>Not necessary to use private Q_sLOTS here. <a href='http://commits.kde.org/akonadi-calendar/45d4d2a5a9926e7619a379d617e8fd67fb0ed0d1'>Commit.</a> </li>
<li>Add metainfo file. <a href='http://commits.kde.org/akonadi-calendar/a4600508cf8b652e316906c6f8b701b19672421f'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-calendar-tools' href='https://cgit.kde.org/akonadi-calendar-tools.git'>akonadi-calendar-tools</a> <a href='#akonadi-calendar-tools' onclick='toggle("ulakonadi-calendar-tools", this)'>[Show]</a></h3>
<ul id='ulakonadi-calendar-tools' style='display: none'><li>New in this release</li></ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Show]</a></h3>
<ul id='ulakonadi-contacts' style='display: none'>
<li>Fix some clazy warning. <a href='http://commits.kde.org/akonadi-contacts/71c1c87b87a181c4f4582ad10ec4e45d610c96d2'>Commit.</a> </li>
<li>Disable some qtwebengine feature. <a href='http://commits.kde.org/akonadi-contacts/692e5e3f0923b1ba3dc0c01d6383ce9ab16890c9'>Commit.</a> </li>
<li>Fix Bug 369489 - please use openstreetmap instead of mapquest. <a href='http://commits.kde.org/akonadi-contacts/9216c53839ee536b3fac80fb920ff2ef4b8defdf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369489'>#369489</a></li>
<li>Rename categories. <a href='http://commits.kde.org/akonadi-contacts/7a77ddc395c9312374590339d10758778ec6bc05'>Commit.</a> </li>
<li>Remove unused check. <a href='http://commits.kde.org/akonadi-contacts/46b7c96f4bdec0bf7534ca8b77208f4f18ae9b66'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/akonadi-contacts/684e8c41ae2b3173c955648898100e193d2c1048'>Commit.</a> </li>
<li>Use new connect api. <a href='http://commits.kde.org/akonadi-contacts/9c317a1af9a370dd83759e055228855dc1800f97'>Commit.</a> </li>
<li>Adapt to ETM ctor change in Akonadi lib. <a href='http://commits.kde.org/akonadi-contacts/f9cb37545d6f375a4ba3e150a7f82d49af21ca09'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/akonadi-contacts/9d9c26008c249ea93b8b89845c9026bec12ae515'>Commit.</a> </li>
<li>Remove not necessary define. <a href='http://commits.kde.org/akonadi-contacts/1c7db388cd2be347c98b1bd4ddca40c2678e4cd1'>Commit.</a> </li>
<li>Don't call it now as parent is setting. <a href='http://commits.kde.org/akonadi-contacts/67006cd953dd3642ca7d9ea2e99e4253bc8463ad'>Commit.</a> </li>
<li>Move in own directory. <a href='http://commits.kde.org/akonadi-contacts/d52e3cf7431958978fbac018853b96a8dce79728'>Commit.</a> </li>
<li>Move in own directory. <a href='http://commits.kde.org/akonadi-contacts/61879db7c77de1eea10119e70a513eac5c621eb1'>Commit.</a> </li>
<li>Clean up. Add parent. Minor poptimization. <a href='http://commits.kde.org/akonadi-contacts/1e3ce35cf3a9cc1f10799e3ead18492270c230b1'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/akonadi-contacts/8fb5f4f48c0ceb93892b44943ddfea10eea9d82a'>Commit.</a> </li>
<li>Adapt to new api. <a href='http://commits.kde.org/akonadi-contacts/5d2ed64c1bdb75f65e517c77e99536038092d9ce'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/akonadi-contacts/438e0221b271ad86aa13b8599a6b823c43c3a18f'>Commit.</a> </li>
<li>Prison use KF5_VERSION now. <a href='http://commits.kde.org/akonadi-contacts/05525bf521069d55e4c09fc3565148e96731edb3'>Commit.</a> </li>
<li>Don't add type if type is empty. <a href='http://commits.kde.org/akonadi-contacts/82985d3b02c31302fd33197ffbcc21cee2960ffe'>Commit.</a> </li>
<li>Remove commented code. <a href='http://commits.kde.org/akonadi-contacts/9764181593d6fc2de82f341d2f101433870d9156'>Commit.</a> </li>
<li>Use QUrlQuery to access query items. <a href='http://commits.kde.org/akonadi-contacts/b124b0c6482d0f59e1717bdc1dff1e9769856c59'>Commit.</a> </li>
<li>IconThemes, DBusAddons and TextWidgets are required. <a href='http://commits.kde.org/akonadi-contacts/f1009a12d38be04bf92d17b161bb5074fcfcba4e'>Commit.</a> </li>
<li>Find KI18N in CMake, required for k18n_wrap_ui:. <a href='http://commits.kde.org/akonadi-contacts/8f2ca9557aca122f48ca1b40c50125b37d0c1203'>Commit.</a> </li>
<li>Clean up includes. <a href='http://commits.kde.org/akonadi-contacts/f54bc64fa18afc9fcd192f32bfb137b56bfad9c9'>Commit.</a> </li>
<li>Move in own directory. <a href='http://commits.kde.org/akonadi-contacts/5bd747956aaab0192f37fa2e21e8644b9a413a25'>Commit.</a> </li>
<li>Add TODO. <a href='http://commits.kde.org/akonadi-contacts/dc2ee4066dfdadd3e2c10f0f6bb157c3892ec234'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/akonadi-contacts/cd2aae7c5f4cd5fa588a67168208b67c33954763'>Commit.</a> </li>
<li>This dependancy is not necessary. <a href='http://commits.kde.org/akonadi-contacts/16cc0ac38069130af4cd20e1b04a2baf01f5c974'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/akonadi-contacts/f577501874f64133a5d69ef2a150bde3e007bd2e'>Commit.</a> </li>
<li>USe new connect api. <a href='http://commits.kde.org/akonadi-contacts/74caed08cb778a9a8e1d9d6465c8a84c956addfc'>Commit.</a> </li>
<li>Add metainfo file. <a href='http://commits.kde.org/akonadi-contacts/42d335d9515612004ae91b0b234040a009cad4ea'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-import-wizard' href='https://cgit.kde.org/akonadi-import-wizard.git'>akonadi-import-wizard</a> <a href='#akonadi-import-wizard' onclick='toggle("ulakonadi-import-wizard", this)'>[Show]</a></h3>
<ul id='ulakonadi-import-wizard' style='display: none'><li>New in this release</li></ul>
<h3><a name='akonadi-mime' href='https://cgit.kde.org/akonadi-mime.git'>akonadi-mime</a> <a href='#akonadi-mime' onclick='toggle("ulakonadi-mime", this)'>[Show]</a></h3>
<ul id='ulakonadi-mime' style='display: none'>
<li>Fix some clazy warning. <a href='http://commits.kde.org/akonadi-mime/5820203f9577a4adf509622eda42b2b77c08a6f1'>Commit.</a> </li>
<li>Use term 'Message' consistently. <a href='http://commits.kde.org/akonadi-mime/6e3cc14e036a2e3decd2290b86d5023541825547'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129306'>#129306</a></li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/akonadi-mime/29f85a01358cb440069ffa8228be9a09bba8303c'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/akonadi-mime/330551417d4cd7287398b65a87089de7f27e451f'>Commit.</a> </li>
<li>Set fancyname in metainfo.yaml. <a href='http://commits.kde.org/akonadi-mime/2cb82af80ab8cad30dbcc7431ca1c1d9f602c673'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/akonadi-mime/af762b9230dda02adc2995f807718f3b2687eeea'>Commit.</a> </li>
<li>Add metainfo file. <a href='http://commits.kde.org/akonadi-mime/078240679ae27508f5a2bccad5c6dae4d33ef41b'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-notes' href='https://cgit.kde.org/akonadi-notes.git'>akonadi-notes</a> <a href='#akonadi-notes' onclick='toggle("ulakonadi-notes", this)'>[Show]</a></h3>
<ul id='ulakonadi-notes' style='display: none'>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/akonadi-notes/49b7ccef5a20e4d36afa86a8342b81279a901a64'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/akonadi-notes/6e7cf68ec8c45b3c33bc30ab272d27354bc238c0'>Commit.</a> </li>
<li>Set fancyname in metainfo.yaml. <a href='http://commits.kde.org/akonadi-notes/74c6a0c2e5af20dca239086f413e472024547c3d'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/akonadi-notes/8256f518426a2dd4567987e1640cd3543618bdb5'>Commit.</a> </li>
<li>Add metainfo file. <a href='http://commits.kde.org/akonadi-notes/151bb2b074a75d0cefc85d9699fbcd6e4207f9fb'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-search' href='https://cgit.kde.org/akonadi-search.git'>akonadi-search</a> <a href='#akonadi-search' onclick='toggle("ulakonadi-search", this)'>[Show]</a></h3>
<ul id='ulakonadi-search' style='display: none'>
<li>Rename categories. <a href='http://commits.kde.org/akonadi-search/705b56b373f6a3f70aefc7e3a5173b4cda9b3ce9'>Commit.</a> </li>
<li>Migrate away from baloorc to agent configuration. <a href='http://commits.kde.org/akonadi-search/2c1f50ee1e6f0669447db804f01db2d0c02a0446'>Commit.</a> </li>
<li>Adapt unittestenv to support all backends. <a href='http://commits.kde.org/akonadi-search/14872aa3b953bd8fd562eadbdff5e5759cd1368e'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/akonadi-search/ccf13ae625dcb85f5a935474543f09b349262e1f'>Commit.</a> </li>
</ul>
<h3><a name='akonadiconsole' href='https://cgit.kde.org/akonadiconsole.git'>akonadiconsole</a> <a href='#akonadiconsole' onclick='toggle("ulakonadiconsole", this)'>[Show]</a></h3>
<ul id='ulakonadiconsole' style='display: none'><li>New in this release</li></ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Show]</a></h3>
<ul id='ulakregator' style='display: none'><li>New in this release</li></ul>
<h3><a name='analitza' href='https://cgit.kde.org/analitza.git'>analitza</a> <a href='#analitza' onclick='toggle("ulanalitza", this)'>[Show]</a></h3>
<ul id='ulanalitza' style='display: none'>
<li>Remove pointless comments. <a href='http://commits.kde.org/analitza/7593261f03b9151d1ee4d34c176a391dae78f293'>Commit.</a> </li>
<li>Use OpenGL variables correctly. <a href='http://commits.kde.org/analitza/a872aa34d73e93191bb24ecf3a007ad3958f1020'>Commit.</a> </li>
<li>Fix C99 warnings. <a href='http://commits.kde.org/analitza/30429a0968179ac0ebc8e53f195233baf2c88952'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Pluginmanager: always return valid mimetypes. <a href='http://commits.kde.org/ark/1c86d08166ae8e9e937b9ea95e6ad7b5ee5fa511'>Commit.</a> </li>
<li>Pluginmanager: cache list of preferred plugins. <a href='http://commits.kde.org/ark/7618ea17108877e308d6c7b4e767fedda9534f9f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372999'>#372999</a></li>
<li>Fix resizing of infopanel. <a href='http://commits.kde.org/ark/7a5e4e4c402e64ef5815e7bc10d9efb6468846a9'>Commit.</a> </li>
<li>Sort mimetypes by comment only when necessary. <a href='http://commits.kde.org/ark/1b817f61f187eda4dc14a3227e0c0430c30d808e'>Commit.</a> See bug <a href='https://bugs.kde.org/372999'>#372999</a></li>
<li>Properly kill BatchExtract jobs. <a href='http://commits.kde.org/ark/864d77f14c28ef48d8d8c6c9a2ebca4928e92a09'>Commit.</a> </li>
<li>Improve ordering of encryption methods. <a href='http://commits.kde.org/ark/a188149abf80140955ec66275bffe8d4e912b172'>Commit.</a> </li>
<li>Fix sizeAdjustPolicy on the combobox with the encryption methods. <a href='http://commits.kde.org/ark/3d9e4dcd4832fe80ec6432ed9202efb5a6f0c239'>Commit.</a> </li>
<li>Don't show two progress bars with batch extractions. <a href='http://commits.kde.org/ark/660076c4c297a6bd1c12612b524d6168ae8dc09e'>Commit.</a> </li>
<li>Fix percentage progress in batch extractions. <a href='http://commits.kde.org/ark/32439e4d7a751063fa5f345b42df47f0d3210571'>Commit.</a> </li>
<li>Use KStandardGuiItem::overwrite(). <a href='http://commits.kde.org/ark/decfd47d0334b8419764b95122306440a646d320'>Commit.</a> </li>
<li>Remove extra KGuiItem for "Cancel". <a href='http://commits.kde.org/ark/e7e47a6a53fcc89da696feeec4b8fd1d1f8d3fce'>Commit.</a> </li>
<li>Fix/simplify QDialogButtonBox usage. <a href='http://commits.kde.org/ark/ce6556a0d8b0fe8267444614b6ca7f5c31b0864f'>Commit.</a> </li>
<li>Clizip: Improve detection of compression methods. <a href='http://commits.kde.org/ark/646da9e32f79839d9f3bee11d9ad24da38ac43f0'>Commit.</a> </li>
<li>Clizip: fix renaming files in the root. <a href='http://commits.kde.org/ark/aacfc786ddb0098dc6fdda4a89e6f9133a542507'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368530'>#368530</a></li>
<li>Part: stop changing the window title when the toolbar gets edited. <a href='http://commits.kde.org/ark/47e45e73ea6d9559951b2e0dd2b8368b99f63749'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357660'>#357660</a></li>
<li>Remove unnecessary version check. <a href='http://commits.kde.org/ark/ebae4b1ca046fc96f14847f03943bb8b831e08ef'>Commit.</a> </li>
<li>Set required ECM version to same as required KF5 version. <a href='http://commits.kde.org/ark/ae6ce265b603ebb889ae5b171787d74aadfab227'>Commit.</a> </li>
<li>Enable KMessageWidget word-wrapping if kwidgetsaddons >= 5.29. <a href='http://commits.kde.org/ark/5696ec946c6d3c510b10d4a5943a39d8e07dc58b'>Commit.</a> </li>
<li>Move defaultOpenAction to General Settings page. <a href='http://commits.kde.org/ark/5e3906ccf56dc54d373970fddae66fcdc35047b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357658'>#357658</a></li>
<li>Add option to disable AES encryption warning. <a href='http://commits.kde.org/ark/1caeff2b888dbff865bd8c879e21b10f10aec940'>Commit.</a> See bug <a href='https://bugs.kde.org/357658'>#357658</a></li>
<li>Add info about encryption methods. <a href='http://commits.kde.org/ark/6058d547e163a209dc883e1932bbbd5611a02949'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129365'>#129365</a></li>
<li>Stop crashing when lsar's output is too big. <a href='http://commits.kde.org/ark/0b981c544e994175403504124a1e44ecfa144e33'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372210'>#372210</a></li>
<li>Fix description signal regression in CreateJob. <a href='http://commits.kde.org/ark/0e1653f48acd7df64c8d461235b93269aea69055'>Commit.</a> </li>
<li>Show warning when using AES encryption with zip. <a href='http://commits.kde.org/ark/046b209258456a2e81b1c06e3b6601a4f77432f8'>Commit.</a> </li>
<li>CliInterface: hide the temporary extraction folder. <a href='http://commits.kde.org/ark/ca57e721819ff05d5f2d8367eb65b5bbafe1bb5a'>Commit.</a> </li>
<li>Fix minor typo:: compressy -> compress. <a href='http://commits.kde.org/ark/5a575955f82b3b4e65a95a04c94c21c7600c9739'>Commit.</a> </li>
<li>Improve error handling for CliPlugins. <a href='http://commits.kde.org/ark/702468a11f12d20b828c54785e92bdea7d254185'>Commit.</a> </li>
<li>Part: port away from QMessageBox. <a href='http://commits.kde.org/ark/f399eaa2df9f08eb7ae90aa1937f0999227cfa47'>Commit.</a> </li>
<li>Part: use word wrap in messagewidgets. <a href='http://commits.kde.org/ark/fc0144e1bc3c54e716217b9f41f3884263317325'>Commit.</a> </li>
<li>Libarchive: stop using xi18nc in errors. <a href='http://commits.kde.org/ark/b608b3e474a9f6f9bdd496f67bfe516ece3e7d09'>Commit.</a> </li>
<li>Clizip: Detect correct encryption method for AES-encrypted zips. <a href='http://commits.kde.org/ark/dfeb049e9251ccb4babbe0e91dff7cac95dccf81'>Commit.</a> </li>
<li>Mainwindow: properly disable 'Open Recent' action. <a href='http://commits.kde.org/ark/61d9f87227711c9b9a7d0180479f94447c6039f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365200'>#365200</a></li>
<li>Infopanel: always use KSqueezedTextLabel. <a href='http://commits.kde.org/ark/a6a38b5bf64a58df332aa83441154fb85b68464d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363719'>#363719</a></li>
<li>Cliinterface: use QScopedPointer for temp folders/files. <a href='http://commits.kde.org/ark/a2a39db8a665b0c6e63b3c82955f22fc414610b7'>Commit.</a> </li>
<li>Cliinterface: add missing finished() signal. <a href='http://commits.kde.org/ark/0025a4f2529f2bf1e541a89e4519a907754a6e67'>Commit.</a> </li>
<li>Fix archiveview column resize. <a href='http://commits.kde.org/ark/47e970709e31b82939fc2fb5404884918a8e6d3c'>Commit.</a> </li>
<li>Make all values in PropertiesDialog selectable by mouse. <a href='http://commits.kde.org/ark/57341779c0fd85e57972838802c27c8819f3d70a'>Commit.</a> </li>
<li>Implement support for setting encryption method. <a href='http://commits.kde.org/ark/039392195050098045269c0baebfe7a72864a515'>Commit.</a> </li>
<li>Archivemodel: drop global static map. <a href='http://commits.kde.org/ark/e03d534df430da4b16ec718a455d0a0e775f9b11'>Commit.</a> </li>
<li>Fix entry counter in libarchiveplugin when overwriting entries. <a href='http://commits.kde.org/ark/ce4bec203b8a6c26c085a1f8df66b44676f3715a'>Commit.</a> </li>
<li>Introduce deletetest. <a href='http://commits.kde.org/ark/9aaeb99c3c5028cf27d52913d73cb04e58258520'>Commit.</a> </li>
<li>Archivemodel: drop ArchiveModelSorter class. <a href='http://commits.kde.org/ark/6c83f46c31a0f97bde3d6cd638b74eda8d4264fd'>Commit.</a> </li>
<li>Show progress in percentage when listing with CliPlugins. <a href='http://commits.kde.org/ark/9de9b974eb6cdcbb77c64d97a304cc22411a84d3'>Commit.</a> </li>
<li>Archivemodel: port away from deprecated qStableSort. <a href='http://commits.kde.org/ark/862ac22eedcf2e3e68ed157da88ac31834395829'>Commit.</a> </li>
<li>Cmake: use find_program for cli executables. <a href='http://commits.kde.org/ark/6fca7438f1d27b949015f366b65ec9e23ad2296b'>Commit.</a> </li>
<li>Simplify Query usage in CLI plugins. <a href='http://commits.kde.org/ark/9f3e8511d9d92f41d3cfc01454b10c97fcc292ff'>Commit.</a> </li>
<li>Refactor CliInterface. <a href='http://commits.kde.org/ark/1074bf07a9b971a9cc6cb406b51436fbcd891b81'>Commit.</a> </li>
<li>Fix minor typo. <a href='http://commits.kde.org/ark/b01c0c21f23d2926899e59ec3298a4d48efd0788'>Commit.</a> </li>
<li>Update ark docbook to 16.12. <a href='http://commits.kde.org/ark/596e7a665b1c50240f44d3b97c71090943653f4b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128883'>#128883</a></li>
<li>Rename 'Add Files to' action. <a href='http://commits.kde.org/ark/62ef7be7544ae2b7d8b48e0bd1fe2fea00474f7e'>Commit.</a> </li>
<li>Fix crash when aborting LoadJobs. <a href='http://commits.kde.org/ark/04d8a98133ff611cfcb042753c6bca8ef24f9ddb'>Commit.</a> </li>
<li>Pluginmanager: always load the arkrc config file. <a href='http://commits.kde.org/ark/e68ab2f61fbd09f7ba98a0182bd13cbbeb4bde48'>Commit.</a> </li>
<li>Filter out duplicate plugins. <a href='http://commits.kde.org/ark/33b94c4d09da98113473ea3fbfd574e20cb22bc1'>Commit.</a> </li>
<li>Libarchive: don't hang when aborting read-write jobs. <a href='http://commits.kde.org/ark/9edb02c22feb2c01e056e967abf1626caed8f7a5'>Commit.</a> </li>
<li>Fix race condition in LoadJob. <a href='http://commits.kde.org/ark/1121db92c31ba0413038c32e69e12b193ed75d0f'>Commit.</a> </li>
<li>Drop TestHelper test functions. <a href='http://commits.kde.org/ark/ddc2785c0c96051aa571a4480c80c992e8ed0c08'>Commit.</a> </li>
<li>Move entryRemoved signal to the read-write interface. <a href='http://commits.kde.org/ark/768117e66f37b240ae845e0f69eb781ad3c4f075'>Commit.</a> </li>
<li>Check numberOfEntries in Add-, Copy- and MoveTest. <a href='http://commits.kde.org/ark/34a46359d27d472c81a08de712bd1fddd8cfed9a'>Commit.</a> </li>
<li>Show progress in percentage for all job types in LibarchivePlugin. <a href='http://commits.kde.org/ark/ef1753b4b3b0123127c757c5de034f8c45232402'>Commit.</a> </li>
<li>Clizip: fix processing of destination when moving files. <a href='http://commits.kde.org/ark/48d180b6660f2ad4daf43d43848fd69aeefe0d52'>Commit.</a> </li>
<li>Drop testhelper code in movetest. <a href='http://commits.kde.org/ark/38e1b6a1a65472b5f83a3bef9d0e3f6452e088fc'>Commit.</a> </li>
<li>Turn extraction/compression options into classes. <a href='http://commits.kde.org/ark/89a7b5bbb52ee6acac620b50efea419446795e82'>Commit.</a> </li>
<li>Fix crash when moving files with clizip. <a href='http://commits.kde.org/ark/377dfcfebdd6bd3cafcc1497182efd0660273503'>Commit.</a> </li>
<li>Use an enum for trailing slash handling. <a href='http://commits.kde.org/ark/2bd1c76ea35b519592837a2bdbeefff46166cc5f'>Commit.</a> </li>
<li>Switch to QVector for list of entries. <a href='http://commits.kde.org/ark/75703351bac6170b5a51535a9f1b70abcbfb02af'>Commit.</a> </li>
<li>New screenshot with setting compression method. <a href='http://commits.kde.org/ark/a2107564e8ce1c78d899c2dfdf8326db5958b4db'>Commit.</a> </li>
<li>Support for setting compression method. <a href='http://commits.kde.org/ark/ae73797768a57d3b7f01b951de40510c8ba96d47'>Commit.</a> </li>
<li>Unittest detection of compression method. <a href='http://commits.kde.org/ark/ea2e4d466f58212875ae5a499578a8c9b242d242'>Commit.</a> </li>
<li>Clirar: Emit compressionMethodFound signal for unrar 3/4. <a href='http://commits.kde.org/ark/5f79138bc86da22d8d3e96ffb511b80814ef5c0e'>Commit.</a> </li>
<li>Detect compression method. <a href='http://commits.kde.org/ark/50a9f590bd2d0a32ac3fe3e4d129637864c977de'>Commit.</a> </li>
<li>Clirar: Detect error when unrar version is too old. <a href='http://commits.kde.org/ark/a3921c6ceced49499ac024d0ae67563275eaadb6'>Commit.</a> </li>
<li>Add archive name to job descriptions. <a href='http://commits.kde.org/ark/21d5f356f86c673c3be317ec74aefd015919870c'>Commit.</a> </li>
<li>Fix KMessageWidget not disappearing after adding files. <a href='http://commits.kde.org/ark/f45e9e69c89f5aea2cbb70baaeea168643434c7c'>Commit.</a> </li>
<li>Process entries while listing archives. <a href='http://commits.kde.org/ark/491f1f7854bf2ea081bca6989acea0f43c69eea0'>Commit.</a> </li>
<li>Register jobs in KIO's jobtracker. <a href='http://commits.kde.org/ark/e7a74d59eef5f4e0d2fc16c1ff9a16fc3389f5eb'>Commit.</a> </li>
<li>Libarchiveplugin: Show progress in percentage when listing archives. <a href='http://commits.kde.org/ark/43008ab1f710b561a255914a528c2464fecec7ea'>Commit.</a> </li>
<li>BacthExtractJob: fix forwarding of errors. <a href='http://commits.kde.org/ark/6db65d75fa5c1a394f72a752990e77918775896b'>Commit.</a> </li>
<li>Enable support for application/x-archive. <a href='http://commits.kde.org/ark/c311157525337d6a12bbc3da445e0c22e77f58d9'>Commit.</a> </li>
<li>Remove Comment property from Archive::Entry. <a href='http://commits.kde.org/ark/7bba06c67085f247ef16b7570072b2e48157b541'>Commit.</a> </li>
<li>Cleanup and optimization in ArchiveModel and Archive::Entry. <a href='http://commits.kde.org/ark/4d9be9fdb5fd2dda2001b843408aa690ddb0726a'>Commit.</a> </li>
<li>Refactor archive loading. <a href='http://commits.kde.org/ark/88acd303900853f796e08107e34337bcb3a49971'>Commit.</a> </li>
<li>Fix extraction of selected entries from AppImage. <a href='http://commits.kde.org/ark/e67866a52cb401aaf8153d0e3081c706e9c3a414'>Commit.</a> </li>
<li>Clizip: Also read compression method property. <a href='http://commits.kde.org/ark/606d387b3f1b6d3d34a84b0a5d36122ed91fec1f'>Commit.</a> </li>
<li>Properly unregister BatchExtract jobs. <a href='http://commits.kde.org/ark/b4685c6698cc683db0166d3423ffe989ff5538f3'>Commit.</a> </li>
<li>Remove unused qRegisterMetaType. <a href='http://commits.kde.org/ark/4c77272c6fc5f9eead93b25e3a3e391bb5e84b86'>Commit.</a> </li>
<li>Part: fix regression when adding files to a new archive. <a href='http://commits.kde.org/ark/0be92fe6bb179e36a19fca11d7608f7a3db31d95'>Commit.</a> </li>
<li>Fix broken merge. <a href='http://commits.kde.org/ark/76415d098f6b523ba5981c9c0090de6dbf0fbe7d'>Commit.</a> </li>
<li>Fix race condition when killing jobs. <a href='http://commits.kde.org/ark/8ad610bf1a5e08e68d89080bb81f036788867718'>Commit.</a> </li>
<li>Drop unused event loop code. <a href='http://commits.kde.org/ark/0fffb96407b5ca9c297c092f34a333a1717f4bd9'>Commit.</a> </li>
<li>Port to Q_ENUM. <a href='http://commits.kde.org/ark/ed4ba1488ad03960f84348b28fe450208a3118bb'>Commit.</a> </li>
<li>[GSoC] Merge master into gsoc2016/master. <a href='http://commits.kde.org/ark/7eb3304db549df12afca7e25698cdf369e6e4a7e'>Commit.</a> </li>
<li>[GSoC] Implement AddTo, Rename, Copy, Move, Paste Actions and Jobs. <a href='http://commits.kde.org/ark/ace277330b373098e4ad96a2d4f57178e9c10a97'>Commit.</a> </li>
<li>AddJob: Count number of entries to be added. <a href='http://commits.kde.org/ark/41e3d16e783b3f61793e21752a883edc3333e1b2'>Commit.</a> </li>
<li>Update Ark documentation to 16.08. <a href='http://commits.kde.org/ark/4171721497331a20bf17f7bcd294123be52bfac0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128645'>#128645</a></li>
<li>Reduce noisy debug output in Archive::deleteFiles(). <a href='http://commits.kde.org/ark/14a4666635e4485327bff76a1ccb96ac29534223'>Commit.</a> </li>
<li>Show time for building model in debug output. <a href='http://commits.kde.org/ark/15c4ecc9795fb326908bbdeefbcd015a43d34c9b'>Commit.</a> </li>
<li>Fix double slashes being added to destination path. <a href='http://commits.kde.org/ark/6e40c57ad2ba28b3eb1d4cbaf4d2c17350c00122'>Commit.</a> </li>
<li>Reduce noisy debug output in ExtractJob::doWork(). <a href='http://commits.kde.org/ark/18916a4d1e19186eabb0f0ff3aa371018af6590d'>Commit.</a> </li>
<li>Port some more connect() calls to the new syntax. <a href='http://commits.kde.org/ark/e2c392833bae3dddca3c7baba104cd435eefc545'>Commit.</a> </li>
<li>[GSoC] Refactor ArchiveInterface API with entries metadata. <a href='http://commits.kde.org/ark/e1bc57bc626e5664bf2e808a0d2839a6dd7cb51b'>Commit.</a> </li>
<li>[GSoC] Refactor ArchiveNode and ArchiveDirNode classes. <a href='http://commits.kde.org/ark/a530116b619aed62d519188adc44e7588cada063'>Commit.</a> </li>
</ul>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Show]</a></h3>
<ul id='ulartikulate' style='display: none'>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/artikulate/b387c98e953ad96d1be320d4be015b2bed8d8d25'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/artikulate/381626289633bbe0d2e0e138c4a65997781cb157'>Commit.</a> </li>
</ul>
<h3><a name='audiocd-kio' href='https://cgit.kde.org/audiocd-kio.git'>audiocd-kio</a> <a href='#audiocd-kio' onclick='toggle("ulaudiocd-kio", this)'>[Show]</a></h3>
<ul id='ulaudiocd-kio' style='display: none'>
<li>Do not follow KDE Application for the library version. <a href='http://commits.kde.org/audiocd-kio/b72ca04323f904aa60a10467734326babec3acac'>Commit.</a> </li>
<li>Reenable translations after the KF5 port. <a href='http://commits.kde.org/audiocd-kio/3c56cb0a133f61a6cfbb034ab3eb483783c33740'>Commit.</a> </li>
<li>[cmake] Move call to feature_summary to the end. <a href='http://commits.kde.org/audiocd-kio/2955361d13eaccf13c6b20116e1061f6f3885f70'>Commit.</a> </li>
<li>Fix KF5 port. <a href='http://commits.kde.org/audiocd-kio/ed8c2ae2c0f16bf76fea1e0f4eac5121707d52d4'>Commit.</a> </li>
<li>Update dtd for kf5. <a href='http://commits.kde.org/audiocd-kio/ad922e0bf985a30e8ef91bf7a6906955fc44795e'>Commit.</a> </li>
<li>Move kioslave -> kioslave5. <a href='http://commits.kde.org/audiocd-kio/992392f77f253bff07855c0faed900304d119e08'>Commit.</a> </li>
<li>Update CMakeLists for KF5Cddb property. <a href='http://commits.kde.org/audiocd-kio/fb22214f4f5052754e582a332e89665d3e3fcd29'>Commit.</a> </li>
<li>Follow the original release tag version. <a href='http://commits.kde.org/audiocd-kio/bca2a7d9058eb1785a18075664db57a0c676e91a'>Commit.</a> </li>
<li>Update CMakeLists for KF5Cddb property. <a href='http://commits.kde.org/audiocd-kio/b3f3c03d2bfcf817a73c396af7d935da9217bdb2'>Commit.</a> </li>
<li>Port to KF5 and test pass. <a href='http://commits.kde.org/audiocd-kio/ae9a47e5435fc25334f63aaac437ed1dcda7df73'>Commit.</a> </li>
</ul>
<h3><a name='blinken' href='https://cgit.kde.org/blinken.git'>blinken</a> <a href='#blinken' onclick='toggle("ulblinken", this)'>[Show]</a></h3>
<ul id='ulblinken' style='display: none'>
<li>Reverse DNS appdata.xml. <a href='http://commits.kde.org/blinken/c7710e63d6ef0341a9bd076f160a13bcf91c57c5'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/blinken/bc345df14d743bcb07b591638f1b9feda59f5af0'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/blinken/137a80c3edace01cc19fe1d54804e137b978d6c8'>Commit.</a> </li>
</ul>
<h3><a name='blogilo' href='https://cgit.kde.org/blogilo.git'>blogilo</a> <a href='#blogilo' onclick='toggle("ulblogilo", this)'>[Show]</a></h3>
<ul id='ulblogilo' style='display: none'><li>New in this release</li></ul>
<h3><a name='calendarsupport' href='https://cgit.kde.org/calendarsupport.git'>calendarsupport</a> <a href='#calendarsupport' onclick='toggle("ulcalendarsupport", this)'>[Show]</a></h3>
<ul id='ulcalendarsupport' style='display: none'>
<li>Fix some clazy warning. <a href='http://commits.kde.org/calendarsupport/694a761bc9016bb50de175984a33f2055f9a3b93'>Commit.</a> </li>
<li>Fix rename categories. <a href='http://commits.kde.org/calendarsupport/e76f984aa084ca886b69aa718c42cc502cc9d385'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/calendarsupport/67044aebf77776c5a52766ebf1e225daee000d10'>Commit.</a> </li>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/calendarsupport/b9434798f0af5ad6884b8bd24f41984f3635a861'>Commit.</a> </li>
<li>Don't install files twice. <a href='http://commits.kde.org/calendarsupport/29e38865271ec39d347ca0cd688d5c0a900329f0'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/calendarsupport/fba0194d515834a39bfb05d4f7a7a74e5c335447'>Commit.</a> </li>
<li>Use variable. <a href='http://commits.kde.org/calendarsupport/84b75101901d5765502a71f22a387e7ffd9d771a'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/calendarsupport/ba2f428befca1485746df2945b4dbde225beabf3'>Commit.</a> </li>
<li>Cache tags by name instead of gid. <a href='http://commits.kde.org/calendarsupport/a7e00390d31de4fb18c43f16afc21bfdbb460841'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/333754'>#333754</a></li>
<li>Cache tags by name instead of gid. <a href='http://commits.kde.org/calendarsupport/c527e0a0e1ffec9892fa30c9510aeb8cc3639757'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/333754'>#333754</a></li>
<li>Minor optimization. <a href='http://commits.kde.org/calendarsupport/c6622835f203c491582f06b752cab023a713b2b9'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/calendarsupport/a906a1d2d763e53df9a5c323d4cd2698950f3187'>Commit.</a> </li>
<li>Set fancyname in metainfo.yaml. <a href='http://commits.kde.org/calendarsupport/726f7805d153a8d01040c6432fd4da6ab0778b7e'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/calendarsupport/06040a768904c91a272657b72982a8a9bbcfcd82'>Commit.</a> </li>
<li>Port to qt5. <a href='http://commits.kde.org/calendarsupport/d1ae4bb617c453383f65bc108b09d239c73cdde7'>Commit.</a> </li>
<li>It was moved here without change namespace... <a href='http://commits.kde.org/calendarsupport/ac9b701b47c4150b21b19fda1ab6a73b61458a47'>Commit.</a> </li>
<li>Remove some private Q_SLOTS. <a href='http://commits.kde.org/calendarsupport/24d2288d5d245b69e34c2646bc9b0004b209a327'>Commit.</a> </li>
<li>Add metainfo file. <a href='http://commits.kde.org/calendarsupport/8244a866998cfb7ae423edd0b9087c8012e1abef'>Commit.</a> </li>
<li>Remove this line. <a href='http://commits.kde.org/calendarsupport/a1178e0b51ce6c21fcc535a246309f03b501a0a3'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Show]</a></h3>
<ul id='ulcantor' style='display: none'>
<li>Honor BUILD_TESTING cmake variable in julia backend. <a href='http://commits.kde.org/cantor/e071d41a88f018ce9fdb0c8eb774b61226daaff9'>Commit.</a> See bug <a href='https://bugs.kde.org/372676'>#372676</a></li>
<li>Correctly parse multi-valued function definitions in maxima variable model. <a href='http://commits.kde.org/cantor/0659bcf5e4b6d6da278e3894057d3bde873c6ea5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363915'>#363915</a></li>
<li>Remove line breaks in maxima's lists with many elements. <a href='http://commits.kde.org/cantor/0dd2b9672369ebda020993fb2f583fe6c3c6f865'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363916'>#363916</a></li>
<li>Julia compilation fixes. <a href='http://commits.kde.org/cantor/3ef0e7ba639622d4f9e613043bb99adde5c19d05'>Commit.</a> </li>
<li>Show the image config dialog automaticaly right after the image entry was added. <a href='http://commits.kde.org/cantor/6205ab584e72e295ff6162424750e521c960c5bf'>Commit.</a> </li>
<li>Removed unused variables and includes in maximasession. <a href='http://commits.kde.org/cantor/6ffe8bfed1601d3d9ca998907682a839a0566d4a'>Commit.</a> </li>
<li>Removed compiler warnings in case no libspectre is used. <a href='http://commits.kde.org/cantor/f1a564b9a773889af501e68e1eabe1a89b3bc161'>Commit.</a> </li>
<li>Don't try to save/load variables if no file name was provided. <a href='http://commits.kde.org/cantor/fc63cd2c6d1cb8ec13a5179326f56c84a535f126'>Commit.</a> </li>
<li>Fix Sage backend keywords.xml location. <a href='http://commits.kde.org/cantor/1c129e725c99aad7fbbf8cffe96412a6000faeda'>Commit.</a> </li>
<li>More robust version check for Sage. <a href='http://commits.kde.org/cantor/8d3d07a683ca6758eada1cd8442047401c0fa83d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128926'>#128926</a></li>
<li>Save session as a plain script file if the user asked to. <a href='http://commits.kde.org/cantor/270f9bfc00ea0147185d0045eba1c659238fca4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348338'>#348338</a></li>
<li>Don't crash when loading a file in an unmodified worksheet. <a href='http://commits.kde.org/cantor/4ed136409c34ee699f0117d54c267931c8d2b952'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/225733'>#225733</a></li>
<li>Don't crash when closing Cantor with an open python, scilab or julia session. <a href='http://commits.kde.org/cantor/dd718f19d26641152cf4db8a837938cf4160d53f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363605'>#363605</a></li>
<li>Fixed a warning message and removed couple of unneeded includes in cantor.cpp. <a href='http://commits.kde.org/cantor/0e070cd9b81ea171b8493e9ccb6e55b155145c2c'>Commit.</a> </li>
<li>Doc: fix typo in "Qalculate". <a href='http://commits.kde.org/cantor/a12f413b3d5dcc9e7f0cdcc4316e868fd3dfffda'>Commit.</a> </li>
<li>Presents the recommended version of the programming. <a href='http://commits.kde.org/cantor/a22f2220502a99d8eecfb69c4b50975bdd9f12b5'>Commit.</a> </li>
<li>Minor changes in Julia description. <a href='http://commits.kde.org/cantor/4a9d44ce83ae1683722143b8226ea9a6f37eb2fa'>Commit.</a> </li>
<li>Add icon to Julia backend. <a href='http://commits.kde.org/cantor/2ad3c6263003d7002a3c297aa964a05d63e6ec01'>Commit.</a> </li>
<li>Fixed for build system from 'vaness-julia-backend' branch. <a href='http://commits.kde.org/cantor/a8ccb47404f593013d2e900b35bc1c91608adb1b'>Commit.</a> </li>
<li>Fixes for build system and added FindJulia.cmake. <a href='http://commits.kde.org/cantor/b25211738b84f915335625992826dbfe6b2d860f'>Commit.</a> </li>
<li>Restore alphabetical order of backends in README. <a href='http://commits.kde.org/cantor/dd18199d2f448ec767502b4e0ecca98227fb434f'>Commit.</a> </li>
<li>Add Julia to the docs. <a href='http://commits.kde.org/cantor/4140f264180687d46a0f3b5f02f516d781ef4ce8'>Commit.</a> </li>
<li>Added unit-tests for several parts. <a href='http://commits.kde.org/cantor/66a2dc3499fdb9d19f84bd4f27964662b1768e85'>Commit.</a> </li>
<li>Documentation for Julia backend stuff. <a href='http://commits.kde.org/cantor/c4d18c576645412af466f723729a20cf0c4fe183'>Commit.</a> </li>
<li>Implemented inline plots. <a href='http://commits.kde.org/cantor/8da94fe3427c7dbc2157e3385012eb69b6392e82'>Commit.</a> </li>
<li>Implemented code completion. <a href='http://commits.kde.org/cantor/facd869bb88e933f50d84e6915fb1df16a7b77c3'>Commit.</a> </li>
<li>Extensions implementation. <a href='http://commits.kde.org/cantor/f774eb096362755bfafda4f2f1947f70474ea6b7'>Commit.</a> </li>
<li>Variable management. <a href='http://commits.kde.org/cantor/df70016a0e0210824785499eae5422a36a8a8e32'>Commit.</a> </li>
<li>Implementation of syntax highlighting. <a href='http://commits.kde.org/cantor/97277343f389fd76ccd5e0cf35e87f2d09957aa8'>Commit.</a> </li>
<li>Added unittests. <a href='http://commits.kde.org/cantor/890616f6acd65956d5ccfadb140b52de4b9176c8'>Commit.</a> </li>
<li>Impelemented command execution with DBUS. <a href='http://commits.kde.org/cantor/7e6af394e4cbf34afd9041dfcfef80c42c1f96c7'>Commit.</a> </li>
<li>Fixed python syntax highlight of strings and comments. <a href='http://commits.kde.org/cantor/5eb7ca2a5a6339445d82e09d5494244535bcf504'>Commit.</a> </li>
<li>Use the right ECM variable for the appstream path. <a href='http://commits.kde.org/cantor/cb8a57c325a0b9315260b2a7683b06484e0ebeab'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/cantor/8ec12f8045f13df65ae69cc6e7c984103563eeb8'>Commit.</a> </li>
</ul>
<h3><a name='cervisia' href='https://cgit.kde.org/cervisia.git'>cervisia</a> <a href='#cervisia' onclick='toggle("ulcervisia", this)'>[Show]</a></h3>
<ul id='ulcervisia' style='display: none'>
<li>Update two screenshots. <a href='http://commits.kde.org/cervisia/e72baa18921baa7093ab33ff7a691b497dd6fe07'>Commit.</a> </li>
<li>Fix QUrl creation with local file path. <a href='http://commits.kde.org/cervisia/a7edf13b1be872a4f5ae4035b7897a827ff53c2d'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/cervisia/f75d8578a51c66c2d165fba128b68e1de985a2cb'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/cervisia/e1c8b749b07045af729c6939ca72a93af5930a4e'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>Fix slow scrolling in dock panels. <a href='http://commits.kde.org/dolphin/90beb4a5e37b887caad1e767046a42dad0af1ab3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365968'>#365968</a>. Code review <a href='https://git.reviewboard.kde.org/r/129409'>#129409</a></li>
<li>The tab navigation shortcut workaround for RTL layouts is not needed anymore, as Qt supports that by default. <a href='http://commits.kde.org/dolphin/57a19efe93dd1e899c3e2e061ebca84d1e90ce46'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128853'>#128853</a></li>
<li>Pass current dir explicitly to QUrl::fromUserInput. <a href='http://commits.kde.org/dolphin/0309bb18185f77c425cf7c69508f049c439eedb0'>Commit.</a> </li>
<li>Popupmenu: use KIO 5.27's new addPluginActionsTo method. <a href='http://commits.kde.org/dolphin/f54174f9f6b99aac433e81cad9965a7f31786ce7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129085'>#129085</a></li>
<li>Follow changes in konqpopupmenu: no longer back/forward/up in part's contextmenu. <a href='http://commits.kde.org/dolphin/345e3e7a8276c12b481c3b6e91fb2e7ca6b6ab2b'>Commit.</a> </li>
<li>Used KUrlMimeData::setUrls for kfileitemmodel's createMimeData for implementing the upcoming stash:/ ioslave. <a href='http://commits.kde.org/dolphin/1710304e9ba926d2aec4226d00974b826f9bcbc0'>Commit.</a> </li>
<li>Add Donate standard action to control menu. <a href='http://commits.kde.org/dolphin/ca53974181c17234f4522bd1635d4c0ab4281e1d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128946'>#128946</a></li>
<li>Use tab for switching active split. <a href='http://commits.kde.org/dolphin/b706108206be1c9e777e1ace02aa99247fdfc3ca'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128564'>#128564</a>. Code review <a href='https://git.reviewboard.kde.org/r/110970'>#110970</a>. Fixes bug <a href='https://bugs.kde.org/171743'>#171743</a></li>
<li>Replace kappname -> dolphin + add Paste menuitems. <a href='http://commits.kde.org/dolphin/1bdebe6d7c38d6c263b34917f10f3daaf2b00b54'>Commit.</a> </li>
<li>Fix changed menuitem string + add some comments. <a href='http://commits.kde.org/dolphin/8529547344cf940e9e51e74e8711d41f008add03'>Commit.</a> </li>
<li>Fix some low-hanging warning fruits. <a href='http://commits.kde.org/dolphin/676cdcbbdbea031c1bec56230561bf8a0efcd0f6'>Commit.</a> </li>
<li>Fix scrolling on hidpi screens. <a href='http://commits.kde.org/dolphin/f688bcd1f14175f1624e2abcc6452882448467c0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128432'>#128432</a>. Fixes bug <a href='https://bugs.kde.org/357618'>#357618</a></li>
</ul>
<h3><a name='dragon' href='https://cgit.kde.org/dragon.git'>dragon</a> <a href='#dragon' onclick='toggle("uldragon", this)'>[Show]</a></h3>
<ul id='uldragon' style='display: none'>
<li>Fix typo in application description. <a href='http://commits.kde.org/dragon/1db650beedd1c1df5322c731a947fa5fec1516be'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/dragon/f38cde85690746e7b471828ce17a307bae9fe695'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/dragon/9107c07d1211fd5fa8b836ca375b411c2c95bfac'>Commit.</a> </li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Show]</a></h3>
<ul id='uleventviews' style='display: none'>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/eventviews/e013491cbb20c78b6a004cc599f61fa73d00e94f'>Commit.</a> </li>
<li>Use new categories format. <a href='http://commits.kde.org/eventviews/775dacbc949eb078c9fe2e61315184c1c22965e2'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/eventviews/a77769cc1ee8c55e7f163edae0d544995c5ccc9a'>Commit.</a> </li>
<li>Remove unused includes. <a href='http://commits.kde.org/eventviews/59963450ffe62c4adab27e08414d6688fc8378ac'>Commit.</a> </li>
<li>Remove unused variable. Minor optimization. <a href='http://commits.kde.org/eventviews/219fed46dadca5dfef78abf6db46aa2d0935c27e'>Commit.</a> </li>
<li>Clean up layout. <a href='http://commits.kde.org/eventviews/ca261d03559001b785cd92ed5a459bef5b8b46f1'>Commit.</a> </li>
<li>Use new connect api. <a href='http://commits.kde.org/eventviews/15213e928f4bf29c0fe9f1a62cf0c9602f9e87c4'>Commit.</a> </li>
<li>Update version. <a href='http://commits.kde.org/eventviews/c2f6cc69cc832839109f3ad5287e08952e63cd56'>Commit.</a> </li>
<li>Implement hidden. <a href='http://commits.kde.org/eventviews/123e018a0a912be35bff9221a1930e9597998b09'>Commit.</a> </li>
<li>Remove private Q_SLOTS. <a href='http://commits.kde.org/eventviews/4cd620fc332ee3f3b30ba0cc4583540c3a7f6565'>Commit.</a> </li>
<li>It's protected method. <a href='http://commits.kde.org/eventviews/6baf1dc16640f70a58e5b80153d0609bba248344'>Commit.</a> </li>
<li>Use kdiagram now. <a href='http://commits.kde.org/eventviews/b8ba0ff8aecd28e97d603bf659cf1c8a0191e291'>Commit.</a> </li>
<li>Make it compile with QT_NO_CAST_FROM_BYTEARRAY. <a href='http://commits.kde.org/eventviews/182292dd5cf1174cd60950740981e8d4bd9e0583'>Commit.</a> </li>
<li>Remove deprecated method. <a href='http://commits.kde.org/eventviews/bfd9db82c2fd6af9fde99188c035919bf30992d3'>Commit.</a> </li>
<li>Port to kdiagram. <a href='http://commits.kde.org/eventviews/1653a8b21df6c5000194f0ad851cf854f9c0dd6d'>Commit.</a> </li>
<li>Adapt code to support kdiagram. <a href='http://commits.kde.org/eventviews/3ae2cd1c9bf328030fd39a7dfbab9dce738d2b72'>Commit.</a> </li>
<li>Start to migrate to kdiagram. <a href='http://commits.kde.org/eventviews/95a0a8036612ca742062e200fcc82e1f93a1526c'>Commit.</a> </li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Show]</a></h3>
<ul id='ulfilelight' style='display: none'>
<li>Tweak the progress thing a bit. <a href='http://commits.kde.org/filelight/d53de780a1b7239d0a2299629b38d5b264d16abc'>Commit.</a> </li>
<li>Tighten up labels a bit. <a href='http://commits.kde.org/filelight/6c37154aa16d7ff9a6bcd348de7df4c6be487c6d'>Commit.</a> </li>
<li>Clean up painting of labels. <a href='http://commits.kde.org/filelight/4e8c4c5ca28faa4c0480fdca2eb60c9f28db19ea'>Commit.</a> </li>
<li>Replace qSort with std::sort. <a href='http://commits.kde.org/filelight/255fbf418418a98b0926efecdf81fb5b612ab78e'>Commit.</a> </li>
<li>Replace tooltip with our own hack. <a href='http://commits.kde.org/filelight/2fb122d6ebefc9f350f5fcf5ae39ef18b74fe1d5'>Commit.</a> </li>
<li>Fix potential crash. <a href='http://commits.kde.org/filelight/63b31e2cd7566ada7dfd5472910d8d7829525734'>Commit.</a> </li>
<li>Fix handling remote urls in radialmap widget. <a href='http://commits.kde.org/filelight/c213b906b80e7ed617b7a2c074f0094f069dc901'>Commit.</a> </li>
<li>Sort local files by size. <a href='http://commits.kde.org/filelight/a71a22ec9ec0fbfcc42a47aefd25e95ac939f83b'>Commit.</a> </li>
<li>Port away from homemade data structure. <a href='http://commits.kde.org/filelight/ee71f61218704248bd77a68c6d3ada9b913ba3d7'>Commit.</a> </li>
<li>Port radial map from Chain to QList. <a href='http://commits.kde.org/filelight/6175850d0195bccf362d19ea7a874c6a81252c6a'>Commit.</a> </li>
<li>Port cache to QList. <a href='http://commits.kde.org/filelight/4de788b10cf2753dc341d6d34c2a9190505640f0'>Commit.</a> </li>
<li>Delete dangerous constructor. <a href='http://commits.kde.org/filelight/f15cd5c8f9b8fe867ae62d04eedb9c9cb7d5b250'>Commit.</a> </li>
<li>Tweak the limits for showing small files. <a href='http://commits.kde.org/filelight/6a49b5c220b141f29dbff9fbc30a27b7ff320d47'>Commit.</a> </li>
<li>Kill the builder class. <a href='http://commits.kde.org/filelight/c171a24ceffa99f4b8a81dd48dbf13c993e36dad'>Commit.</a> </li>
</ul>
<h3><a name='grantlee-editor' href='https://cgit.kde.org/grantlee-editor.git'>grantlee-editor</a> <a href='#grantlee-editor' onclick='toggle("ulgrantlee-editor", this)'>[Show]</a></h3>
<ul id='ulgrantlee-editor' style='display: none'><li>New in this release</li></ul>
<h3><a name='grantleetheme' href='https://cgit.kde.org/grantleetheme.git'>grantleetheme</a> <a href='#grantleetheme' onclick='toggle("ulgrantleetheme", this)'>[Show]</a></h3>
<ul id='ulgrantleetheme' style='display: none'>
<li>Fix clazy warning. <a href='http://commits.kde.org/grantleetheme/a0465ad4167a1128dbc42bb1cb49f368dfc2633c'>Commit.</a> </li>
<li>Use qobject_cast. <a href='http://commits.kde.org/grantleetheme/197586f4aa8e59fae5ae3b69c41df08430714e2a'>Commit.</a> </li>
<li>Try to fix crash. <a href='http://commits.kde.org/grantleetheme/e80bb74227dea5047060a64366f9192214ea0def'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/grantleetheme/40d93ca2c1fcfcb93767bd30dcfe3321aaebb385'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/grantleetheme/b020bdf4127c7e7c534d0e0dc86e05a9c36972dc'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/grantleetheme/d358f3238d6c7ede0c66e91d5c5f65e17cdb195e'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/grantleetheme/5c11fcf16b12aeeceb6db97a6883c26422228267'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org, add tags documentation. <a href='http://commits.kde.org/grantleetheme/73bd97b75d76e06fe758e590ddeaa778f03c994f'>Commit.</a> </li>
<li>Add metainfo file. <a href='http://commits.kde.org/grantleetheme/5614e65eb6217e5aa9d29dbb86d752b660b8eac6'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Show]</a></h3>
<ul id='ulgwenview' style='display: none'>
<li>Fix crash when updating the scaler. <a href='http://commits.kde.org/gwenview/8f6d726fce8fae49991d8f4c9062cbedba70e58a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357548'>#357548</a></li>
<li>Compile with Qt 5.6. <a href='http://commits.kde.org/gwenview/304e496a87d3a9268f52b1d5875179c0fbee2c19'>Commit.</a> </li>
<li>Port saving away from KImageIO. <a href='http://commits.kde.org/gwenview/906d226cfaf78e65fda1733cead8f3dedbc42679'>Commit.</a> </li>
<li>More porting away from kde_file.h. <a href='http://commits.kde.org/gwenview/1bce6d37e5c0e63404fc77d54b4cd82f23b18956'>Commit.</a> </li>
<li>Port away from KMimeType. <a href='http://commits.kde.org/gwenview/92a44490bd3d2b6ff892a4df1729022017e41c02'>Commit.</a> </li>
<li>Port tests away from kdebug. <a href='http://commits.kde.org/gwenview/3068071069c8384151c7831b608dc1805ff1a5ff'>Commit.</a> </li>
<li>Port away from KIO::NetAccess. <a href='http://commits.kde.org/gwenview/efcd7fa495002d5711a05a568fa2d25e1ace56df'>Commit.</a> </li>
<li>Port away from KGlobalSettings. <a href='http://commits.kde.org/gwenview/373b0c2a04ad71b96b571923b57051b294f2b627'>Commit.</a> </li>
<li>Port away from KIO::NetAccess, kde_file.h. <a href='http://commits.kde.org/gwenview/2d3a5775485ba84ad21c84523064708998aa6cac'>Commit.</a> </li>
<li>Port away from KSharedPtr. <a href='http://commits.kde.org/gwenview/759234c0e3666536496660746e97995024dc61c2'>Commit.</a> </li>
<li>Port away from KDialog::marginHint(). <a href='http://commits.kde.org/gwenview/4ec60eff3d58f17c100b20c2f39ae90cc4287a76'>Commit.</a> </li>
<li>Phonon and Exiv2 are required. <a href='http://commits.kde.org/gwenview/d3970cf40bf0cc4b26e3d5d1d89e66d378761f1d'>Commit.</a> </li>
<li>Fix whitespace in docstring. <a href='http://commits.kde.org/gwenview/7861c642e5c524b1b83500a1ce2febf9f9dc01b2'>Commit.</a> </li>
<li>Remove FullScreenContentPrivate. <a href='http://commits.kde.org/gwenview/4cd987f5888c745438e9e59cc75682c7592257e5'>Commit.</a> </li>
<li>Remove FolderViewContextManagerItemPrivate. <a href='http://commits.kde.org/gwenview/dc208c20e8df525c1053b73dace4d2beeef66092'>Commit.</a> </li>
<li>Remove another round of pimpls. <a href='http://commits.kde.org/gwenview/ad3b07f3c88e6fa964bee08731e166e68cd295d9'>Commit.</a> </li>
<li>Remove unused variable warnings for MainWindow::eventFilter. <a href='http://commits.kde.org/gwenview/617e443254faec59a6433fd07a34e120fd4680e2'>Commit.</a> </li>
<li>Remove FileOpsContextManagerItemPrivate. <a href='http://commits.kde.org/gwenview/4606d365a3717c280af5226980cbab5233ac6df8'>Commit.</a> </li>
<li>Remove DocumentInfoProviderPrivate. <a href='http://commits.kde.org/gwenview/2f13bfb68c8b6ff81a8370adbec98e0e27cccaa7'>Commit.</a> </li>
<li>Remove ConfigDialogPrivate. <a href='http://commits.kde.org/gwenview/562cc26f382ae665be96087f8a1c6647e1433a25'>Commit.</a> </li>
<li>More macro removal and simplification. <a href='http://commits.kde.org/gwenview/3007431d1a85ce4161f819ce18dd0f5b7fd65a71'>Commit.</a> </li>
<li>Turn addAction macro into a function. <a href='http://commits.kde.org/gwenview/d64e59886896a5ae7d9fb524767abf03262c1977'>Commit.</a> </li>
<li>No need to forward declaire. <a href='http://commits.kde.org/gwenview/3422f902586b2079b72f34cca02d1e9d3588a77a'>Commit.</a> </li>
<li>No need to set parent. <a href='http://commits.kde.org/gwenview/80c2c695e10f3ae78d7be684b6d0136582ea3a7c'>Commit.</a> </li>
<li><sitter> Riddell: note that the watcher should be made a stack variable. <a href='http://commits.kde.org/gwenview/d5a0bacfa64a60f7e190be48221249c9eaba4f6a'>Commit.</a> </li>
<li>Don't delete watcher so keep harald happy, use QObject::connect() in a struct which is not a QObject. <a href='http://commits.kde.org/gwenview/da9320eeab35b1ecd25595b24a4ad79075fbf627'>Commit.</a> </li>
<li>Show only install action in menu. <a href='http://commits.kde.org/gwenview/64bb234dae1a44a7824f5171e2f6e60559198de0'>Commit.</a> </li>
<li>Give timer a better name, make on stack instead of pointer. <a href='http://commits.kde.org/gwenview/b740cfbfd607f8da4ba12fe70a558cfbacad41c0'>Commit.</a> </li>
<li>Use a lambda for simple slot. <a href='http://commits.kde.org/gwenview/790b497bfcb4812377dc315cbd6e2b87138ad439'>Commit.</a> </li>
<li>No need for argument. <a href='http://commits.kde.org/gwenview/b80354e6578605db1e2b87eb98e53538200fa22d'>Commit.</a> </li>
<li>New connect syntax. <a href='http://commits.kde.org/gwenview/57d587650391c4373bdf86572e4ac9f35d37ba0d'>Commit.</a> </li>
<li>Use a shorter timer which resets if new files are written on kipi install. <a href='http://commits.kde.org/gwenview/c1b33455556bf611c25dcad0d3574238594c5ede'>Commit.</a> </li>
<li>Define url in one place. <a href='http://commits.kde.org/gwenview/4667c41fb734aa29a32080ef4bd6aa98491ab12c'>Commit.</a> </li>
<li>Use new signal/slot syntax. <a href='http://commits.kde.org/gwenview/5e840eb1a4f7f031cbad7f624769878853c52e35'>Commit.</a> </li>
<li>Give context to string. <a href='http://commits.kde.org/gwenview/a2bb243bd458290ae61f91f1e43e2e56a499bb84'>Commit.</a> </li>
<li>Give context to string. <a href='http://commits.kde.org/gwenview/6ed6a7afb1bcbda461c77e80b1e6970e57901d5b'>Commit.</a> </li>
<li>Test for ability to install with KIO::DesktopExecParser::hasSchemeHandler((). <a href='http://commits.kde.org/gwenview/f6348c238db4004e5e576e4578daa93e93cb1716'>Commit.</a> </li>
<li>Use nullptr. <a href='http://commits.kde.org/gwenview/1846dbcac72331e5804996cc4ec9b8529a24293a'>Commit.</a> </li>
<li>Use nullptr. <a href='http://commits.kde.org/gwenview/a39bc609f02c47a8e6361b1198a1851dcd6604c1'>Commit.</a> </li>
<li>Delete pluginWatcher sooner. <a href='http://commits.kde.org/gwenview/c694918da8ec414ece761c9e26fe8daa9e3c1ae6'>Commit.</a> </li>
<li>Remove debugging, delete plugin loader rather than leak it. <a href='http://commits.kde.org/gwenview/71f0f010d5f77573531418b5955bb994782b6b64'>Commit.</a> </li>
<li>Use qpointer. <a href='http://commits.kde.org/gwenview/121bb850a546be34ab2052bb0c7a49214900fd0a'>Commit.</a> </li>
<li>Tidying. <a href='http://commits.kde.org/gwenview/f97701e56baccbbc95e47a24d757a822d31617d6'>Commit.</a> </li>
<li>Implementation using appstream URLs and plasma discover. <a href='http://commits.kde.org/gwenview/4d75abc91339e1bf4ac7c731f8a6a654a36109c1'>Commit.</a> </li>
<li>Fix handling of multiple arguments in command line. <a href='http://commits.kde.org/gwenview/3d0531c0f253e469126b463f0dfb374a19dee641'>Commit.</a> </li>
<li>Use Message Box at end of process, nicer than reusing the Progress Dialog. <a href='http://commits.kde.org/gwenview/892ce0bd1bb930b689fb40d9cc9a32f3cdad94ae'>Commit.</a> </li>
<li>I18n, check return from appstream, give dialog parent. <a href='http://commits.kde.org/gwenview/d28eb4c620bc1d36d6c396dd6dc1bd421e7f22ae'>Commit.</a> </li>
<li>Handle cancel button. <a href='http://commits.kde.org/gwenview/8431c32e087f8ca9b3dfe8e25f02ea55ee93ce00'>Commit.</a> </li>
<li>Set perfentage of dialog during install. <a href='http://commits.kde.org/gwenview/fc475a805c80b1d5841b562b6c53023217cc4b31'>Commit.</a> </li>
<li>White space tidy. <a href='http://commits.kde.org/gwenview/f00915e88af1514e209319ac5758b74bd2696bf9'>Commit.</a> </li>
<li>Fix compile without appstream library. <a href='http://commits.kde.org/gwenview/89281b19aa0caebbf80fb6ff04cd9f26a9e90cae'>Commit.</a> </li>
<li>Remove excessive debugging. <a href='http://commits.kde.org/gwenview/29dbb15a3964303264c403a06ab8b0d394507170'>Commit.</a> </li>
<li>Make appstream database on stack rather than leak. <a href='http://commits.kde.org/gwenview/8ec1016e439f64a2d4ca57dd7ee73420651ca52b'>Commit.</a> </li>
<li>Remove debugging. <a href='http://commits.kde.org/gwenview/6b62563b44f252e168e6a3923e0d958ea747b32d'>Commit.</a> </li>
<li>Allow to be compiled without packagekit/appstream. <a href='http://commits.kde.org/gwenview/a1b224482b4e870dbed9605d0bcab3e3dcd31929'>Commit.</a> </li>
<li>Yay, an installing plugin installer. <a href='http://commits.kde.org/gwenview/155c99030370dfcc2aaf44c834db353854c587ff'>Commit.</a> </li>
<li>Gwenview is now using qAsConst, it requiers Qt 5.7. <a href='http://commits.kde.org/gwenview/b8d109cc19f06b1711ad7a82dde6ccf703e93691'>Commit.</a> </li>
<li>Add emit keyword to signal emission. <a href='http://commits.kde.org/gwenview/7b8463c8ea363054cf967cc3b45839a019fe96d9'>Commit.</a> </li>
<li>Remove deprecated KImageIO::mimeTypes. <a href='http://commits.kde.org/gwenview/93bc279101e42b768a76e3ff43b524c4a0740be7'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/gwenview/99b22da2406814c52ff6a42c06067fd75f6ec080'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/gwenview/1f5a22c2b9403e409c4c93521704f1277a3eaef1'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Show]</a></h3>
<ul id='ulincidenceeditor' style='display: none'>
<li>Use KStandardGuiItem more. <a href='http://commits.kde.org/incidenceeditor/48f4a55c4822b8e10c0e9212679559c0d97e5bfa'>Commit.</a> </li>
<li>Fix some clazy warning. <a href='http://commits.kde.org/incidenceeditor/2993599f02b5feefb53638b493d903d352c3acb7'>Commit.</a> </li>
<li>Now we depend against 5.6.0. <a href='http://commits.kde.org/incidenceeditor/bde9d3e1934dc7641f73276b418e642fd7aacbe0'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/incidenceeditor/570af1f3e9f1761431fe98d5ebfe3ca65d9506d6'>Commit.</a> </li>
<li>Don't install files twice. <a href='http://commits.kde.org/incidenceeditor/16a6f6a7f8f10b80aeefa838020d67f5201d50b3'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/incidenceeditor/c452af091414651ffa623ae165ae660fa129338d'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/incidenceeditor/f67ddbad3c038df0a3f4d7cdf31d58ffbb17410e'>Commit.</a> </li>
<li>Update version. <a href='http://commits.kde.org/incidenceeditor/f96ea2bc2eec3d2536b49aa35dbe4ddfa566fc28'>Commit.</a> </li>
<li>Adapt to new api. <a href='http://commits.kde.org/incidenceeditor/15ba41e0de92c6aacd1688c51f7efbbe07f74165'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/incidenceeditor/5c199e5c896dbdf3e21b183b8fdd8d8227bcd23a'>Commit.</a> </li>
<li>Use new connect api. <a href='http://commits.kde.org/incidenceeditor/2dc9203635a9111ab5d8792e03870b4079a2c206'>Commit.</a> </li>
<li>Add metainfo. <a href='http://commits.kde.org/incidenceeditor/f53cdde5aca09e1ff0db121c64431bc3ec06cb63'>Commit.</a> </li>
<li>Use KDIAGRAM now. <a href='http://commits.kde.org/incidenceeditor/b2c87c34123f09e6b8476c301b2b469e768b289e'>Commit.</a> </li>
<li>Reactivate flags. <a href='http://commits.kde.org/incidenceeditor/8af061e282310fc8fd5ce6775c6aaf94195edf17'>Commit.</a> </li>
<li>Make it private. <a href='http://commits.kde.org/incidenceeditor/a0e97b33c397146122d3fbd9cd7bbd8365640455'>Commit.</a> </li>
<li>Port to KDiagram. <a href='http://commits.kde.org/incidenceeditor/8b7e60c7a7c3ba1eb882ec344be900ed25017d8a'>Commit.</a> </li>
<li>Continue to port to kdiagram. <a href='http://commits.kde.org/incidenceeditor/823fa746aa3db89185b0008c2672ab3e0befdcab'>Commit.</a> </li>
<li>Fix lib. <a href='http://commits.kde.org/incidenceeditor/cc1f7f0ee59fdad2f4a5a649f6d7eb08b2c7bde6'>Commit.</a> </li>
<li>Start to migrate to KDiagram. <a href='http://commits.kde.org/incidenceeditor/db42c71532764449b37ead24a4067506a5930642'>Commit.</a> </li>
</ul>
<h3><a name='jovie' href='https://cgit.kde.org/jovie.git'>jovie</a> <a href='#jovie' onclick='toggle("uljovie", this)'>[Show]</a></h3>
<ul id='uljovie' style='display: none'>
<li>Add namespace to desktop and appdata files. <a href='http://commits.kde.org/jovie/fbfc97cd69f3108253656d8dc0bf56b6fdfbb82b'>Commit.</a> </li>
</ul>
<h3><a name='kaddressbook' href='https://cgit.kde.org/kaddressbook.git'>kaddressbook</a> <a href='#kaddressbook' onclick='toggle("ulkaddressbook", this)'>[Show]</a></h3>
<ul id='ulkaddressbook' style='display: none'><li>New in this release</li></ul>
<h3><a name='kajongg' href='https://cgit.kde.org/kajongg.git'>kajongg</a> <a href='#kajongg' onclick='toggle("ulkajongg", this)'>[Show]</a></h3>
<ul id='ulkajongg' style='display: none'>
<li>When not using KDE python bindings, add /usr/share/apps/kajongg to appdata search path. <a href='http://commits.kde.org/kajongg/d801f84d285f44c84fd87d300e61ebfb7aa4de60'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373210'>#373210</a></li>
<li>New file tilesource.py was not installed by cmake. <a href='http://commits.kde.org/kajongg/9813779689c20e5b9c26654ddcf55f3f43fd8e15'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372759'>#372759</a></li>
<li>Add namespace to desktop and appdata files. <a href='http://commits.kde.org/kajongg/871c53ccd14d05e526117ccd6ee405d9132820fe'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129184'>#129184</a></li>
<li>Git pre-commit: pylint3 needs twisted symlink. <a href='http://commits.kde.org/kajongg/8c6a0d392280090670c5cddf2c3392a8c79cdd2b'>Commit.</a> </li>
<li>Dialogs must not use logDebug. <a href='http://commits.kde.org/kajongg/50a38df823af0d1b7fd87667b5ee835e2ee1655b'>Commit.</a> </li>
<li>Git pre-commit really do execute pylint3. <a href='http://commits.kde.org/kajongg/be2aa57954ec0d9b651b9b7fdd9f5f477fec4ac6'>Commit.</a> </li>
<li>Kajonggtest: simplify socketname. <a href='http://commits.kde.org/kajongg/89b8712761b04576c642ef8e474878061653fdb3'>Commit.</a> </li>
<li>Kajonggtest: simplify output. <a href='http://commits.kde.org/kajongg/b2865f1195f92ba0b39964edf1981a508b70e7a9'>Commit.</a> </li>
<li>Kajonggtest: git clones are now in ~/.kajongg/.cache. <a href='http://commits.kde.org/kajongg/7578437b9d1115de0fcae3ef01ac7a577e07f256'>Commit.</a> </li>
<li>New: Debug.scoring with output as far as needed for current problem. <a href='http://commits.kde.org/kajongg/ac60805e0bd559be1f177fb4af4e6de94017db49'>Commit.</a> </li>
<li>Get rid of kajonggserver3.py again. <a href='http://commits.kde.org/kajongg/f03d3004a5cd2e2b9dd0b1bf264f042a15a4d426'>Commit.</a> </li>
<li>Remove option --server3. Twisted needs the same (either 2 or 3). <a href='http://commits.kde.org/kajongg/c5acebd9a3cdf11fb4e84ce73e7b040e8721be19'>Commit.</a> </li>
<li>Kajonggtest: --log used wrong directories with --23. <a href='http://commits.kde.org/kajongg/613f7b625dcd0b582729230ab7ea4902302d96ed'>Commit.</a> </li>
<li>Url: simplify. <a href='http://commits.kde.org/kajongg/80deebb51e676140763f0992e6d85771c874a7e8'>Commit.</a> </li>
<li>Fixes for using the new Wind class. <a href='http://commits.kde.org/kajongg/a96379678137e207e27d90fb61ea67e74e1ae962'>Commit.</a> </li>
<li>Remove a wrong assertion. <a href='http://commits.kde.org/kajongg/fa0211104a098ea871bf9a839fad4b7b568453e0'>Commit.</a> </li>
<li>Servertable.__unicode__ is now more robust, also works for empty table. <a href='http://commits.kde.org/kajongg/87243de90ff72864647c0d06be3f62e45f08be1e'>Commit.</a> </li>
<li>Remove a few TODOs, put some of them into the TODO File. <a href='http://commits.kde.org/kajongg/05f73d45f212859b9f99554813828c09277ba7ad'>Commit.</a> </li>
<li>Pre-commit: also execute kajongg.py with both py2 and py3. <a href='http://commits.kde.org/kajongg/aa2822335529cc4c3bab32cc3cccced7eef68e76'>Commit.</a> </li>
<li>Player: if Debug.hand, assert that Player.hand is always current. <a href='http://commits.kde.org/kajongg/f7a5a1e2ab2ccb0d3e5f7769944f546d8cc060c0'>Commit.</a> </li>
<li>Player: changing lastSource now invalidates _hand. <a href='http://commits.kde.org/kajongg/5c4ab229ac0dfe0b877636b0d12e10ba343bbed2'>Commit.</a> </li>
<li>Player.computeHands becomes private: __computeHand. <a href='http://commits.kde.org/kajongg/ae7bdee607d5ae6b78e9e10df9064ae55d3d7e00'>Commit.</a> </li>
<li>ScoringDialog does not need Player.computeHand anymore. <a href='http://commits.kde.org/kajongg/89709c2591c1e977fa4108af3f3f87436bd1e36d'>Commit.</a> </li>
<li>Player.computeHand: clarify code. <a href='http://commits.kde.org/kajongg/4a4523e13f5fd2a8a62f1f88fb6aaec529b27907'>Commit.</a> </li>
<li>Sound: If md5sum is wrong, explain why and offer a solution. <a href='http://commits.kde.org/kajongg/198024f088c010684ee40fc25cde5dda6ace8780'>Commit.</a> </li>
<li>Improve rules for GatesOfHeaven and NineGates. <a href='http://commits.kde.org/kajongg/2a77ed2c94c4769acd28711f0fe35fe1cb9a1cdd'>Commit.</a> </li>
<li>Rulecode: docstrings. <a href='http://commits.kde.org/kajongg/6fd1260c8a2dba615e9e31a8a7acc0305c6c7058'>Commit.</a> </li>
<li>NineGates: last tile may also be 1 or 9. <a href='http://commits.kde.org/kajongg/8a5e679af85ac07d89054656821c5744efb1829b'>Commit.</a> </li>
<li>NineGates gets its own appliesToHand. No change in behaviour. <a href='http://commits.kde.org/kajongg/d7e7930d4a56a33e155eec0e2d117cc2e1f283b5'>Commit.</a> </li>
<li>GatesOfHeaven and NineGates got docstrings. <a href='http://commits.kde.org/kajongg/b6fd5ea085087db08a8acb7926a1c86d9fe85ee6'>Commit.</a> </li>
<li>Separate classes GatesOfHeaven and NineGates. <a href='http://commits.kde.org/kajongg/39d483567bc45ae278a265c6449f6b018b4d3807'>Commit.</a> </li>
<li>GatesOfHeaven: unite options pair28 and lastExtra in one option BMJA. <a href='http://commits.kde.org/kajongg/ccd24eb84fd0fbee46427d6caf65d0304d0593aa'>Commit.</a> </li>
<li>Fix RuntimeError: dict changed size during iteration. <a href='http://commits.kde.org/kajongg/a51e9c21716144729d9ac2a982706915dd285529'>Commit.</a> </li>
<li>Blessing rules: add assertions. <a href='http://commits.kde.org/kajongg/13f890e8a64f3808a182ad3039139f8421b46e53'>Commit.</a> </li>
<li>Player.__maySayXXXX: less code. <a href='http://commits.kde.org/kajongg/a3dc66168ad2ad36584a9cd1de88a2e8107b2a6a'>Commit.</a> </li>
<li>Message initialization: make it clearer. <a href='http://commits.kde.org/kajongg/e9ee8a5fa7f40b779e5a3c2cf369a79ff21f7741'>Commit.</a> </li>
<li>Player: remove one FIXME, just leave it as a comment. <a href='http://commits.kde.org/kajongg/0b046bb066077ed0033c682fed188e8281891929'>Commit.</a> </li>
<li>Hand.__arrange: try to correctly treat lastTile. <a href='http://commits.kde.org/kajongg/1ab4f50c58ff41cce711d586a7c8416908c81ff9'>Commit.</a> </li>
<li>New class TileSource. <a href='http://commits.kde.org/kajongg/969267f20af4c4af8cfec32e6935c5d56283e90a'>Commit.</a> </li>
<li>BlessingOfEarth had no tests. <a href='http://commits.kde.org/kajongg/f259b1eedc7b06ad59f33a763f899079ff001bc1'>Commit.</a> </li>
<li>Improve assertion message. <a href='http://commits.kde.org/kajongg/2482451d5ecd0d4ac6ba4fac6f2b37c5cb2b2eb3'>Commit.</a> </li>
<li>Ruleset: remove old TODO. <a href='http://commits.kde.org/kajongg/e861763c9b350292e005d2e2a30588b9af4e009d'>Commit.</a> </li>
<li>Accept UTF-8 in appdataDir. <a href='http://commits.kde.org/kajongg/b28faa9df1fd5474c42504abe174f1a95958e7cf'>Commit.</a> </li>
<li>Do not stop after wrong claim. <a href='http://commits.kde.org/kajongg/c219ffc2fcfc6a040595ce6a349d4efbecde7f49'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362381'>#362381</a></li>
<li>SelectDialog: do not stop timer for error messages. <a href='http://commits.kde.org/kajongg/13c861ab9ef22156fe5a6637e8255e07ae76026c'>Commit.</a> </li>
<li>Hand: __NotWon has no .message in Py3. <a href='http://commits.kde.org/kajongg/f001f762ae0f9cf7d419bddfeb16c1575505831b'>Commit.</a> </li>
<li>Fix docstring. <a href='http://commits.kde.org/kajongg/7207f99698aec6dec3819d4bc61ad17045e51ed2'>Commit.</a> </li>
<li>Tile: assert that there are no duplicates. <a href='http://commits.kde.org/kajongg/377c79b28c1a7f431c5f6dce045193d907f9be1e'>Commit.</a> </li>
<li>Kdestub i18n: suppress debug output about @action. <a href='http://commits.kde.org/kajongg/9252d6aeea5b91fcbbe4a19883bee51e73b34c56'>Commit.</a> </li>
<li>Move.prettyKwArgs: sort by key, making debug logs easier to compare. <a href='http://commits.kde.org/kajongg/d4c888042f16fad4b9c79c402c52f26cbd4b0995'>Commit.</a> </li>
<li>IntDict gets __str__. <a href='http://commits.kde.org/kajongg/91b09b09db0c24a6dc6dc8239d2ea40be2784568'>Commit.</a> </li>
<li>Debug AI: fixed float output format: Is now the same for Py3 and Py2. <a href='http://commits.kde.org/kajongg/5eda0a6464e00774d31c502ecfa865b5a07a3c7a'>Commit.</a> </li>
<li>Client: nativeString for logging was wrong. <a href='http://commits.kde.org/kajongg/f18b43cce5dd1be908046065a7936517bbe33634'>Commit.</a> </li>
<li>Sql: No need to define empty update methods. <a href='http://commits.kde.org/kajongg/1503b5dcc0621ed523d5c89f0773afeb3f52b931'>Commit.</a> </li>
<li>Update git-pre-commit (and use it again). <a href='http://commits.kde.org/kajongg/e157a674408cfd85a4c582a954f02dc7a1920eb8'>Commit.</a> </li>
<li>New Classes Wind, East etc. <a href='http://commits.kde.org/kajongg/0ab35cc2db81888d65deb5be918bb1c09bef9855'>Commit.</a> </li>
<li>Debug.quit also logs aboutToQuit ending. <a href='http://commits.kde.org/kajongg/ed9bf140b747b56bfb3c7347d5d796b7a7d27539'>Commit.</a> </li>
<li>Make source clean for pylint3 version 1.5.2. <a href='http://commits.kde.org/kajongg/8b224462a87c18c9c02c42bd7ce7878d59eabafc'>Commit.</a> </li>
<li>MainWindows.showSettings now works with both current KDE and Qt. <a href='http://commits.kde.org/kajongg/f6fe1f0ddfefdf9065b9622c6c4828745260c3a9'>Commit.</a> </li>
<li>Player._computeHand: split off _computeHandWithDiscard. <a href='http://commits.kde.org/kajongg/70c9f30a607a26a36a5d3b6f326740a5f21f013c'>Commit.</a> </li>
<li>Player: better assertion error message. <a href='http://commits.kde.org/kajongg/9b7bb161abbc5e33f632bf264e5897b228a51b4b'>Commit.</a> </li>
<li>Hand: improve Debug.mahJongg output. <a href='http://commits.kde.org/kajongg/f59c69bf0d90bdef5938386ab9dbb860705c3332'>Commit.</a> </li>
<li>Simplifiy usage of util.callers(). <a href='http://commits.kde.org/kajongg/e8db54fb2405b3ed343c3b7c039f4483c17431a5'>Commit.</a> </li>
<li>Wall: simplify. We always get Tile, never str. <a href='http://commits.kde.org/kajongg/c09b03ee146fa6aa829b9339aef2e53df4f1c5b7'>Commit.</a> </li>
<li>Player.robsTile is new: try to change player.lastSource only within Player. <a href='http://commits.kde.org/kajongg/cfd851759c367ade3e502bedc9429c03d7c4a669'>Commit.</a> </li>
<li>Player.robTile renamed to Player.robTileFrom. <a href='http://commits.kde.org/kajongg/820cc36bfca23420ea7c3075610724cb686f4d9b'>Commit.</a> </li>
<li>Server: small simplification. <a href='http://commits.kde.org/kajongg/823802086b748fbc7b65029c8a3248d0435770c0'>Commit.</a> </li>
<li>Server: remove code duplication. <a href='http://commits.kde.org/kajongg/ae42cc62b3ad944f63adb4b436f32d17b67487e7'>Commit.</a> </li>
<li>HandBoard: do not need player.mjString anymore for building newLowerMelds. <a href='http://commits.kde.org/kajongg/4b9febb1a21383806aad99ca7d6cfa86cfcb160c'>Commit.</a> </li>
<li>HandBoard: small simplification. <a href='http://commits.kde.org/kajongg/9cf5050f30807daf60eddacfd477fbecf8b663bb'>Commit.</a> </li>
<li>Player.computeHand renamed to Player._computeHand. <a href='http://commits.kde.org/kajongg/73e103ea2cae916c5b6d322e2ac663a00f4f6a24'>Commit.</a> </li>
<li>Hand.__init__: simplify assertions around Hand.lastTile. <a href='http://commits.kde.org/kajongg/e2c5a2dace10563166af06f1bf3fa46b6dd0e5a7'>Commit.</a> </li>
<li>Hand.usedRule: init not to None but to []. <a href='http://commits.kde.org/kajongg/d383d05bdb61f3c93055b6caac1b4ee41a29a601'>Commit.</a> </li>
<li>Hand ID in debug output: This should be repeatable yet helpful. <a href='http://commits.kde.org/kajongg/6396c40e9cd8839baf3ecb936235630c2839ce37'>Commit.</a> </li>
<li>Hand: indent not by hand hierarchy but dynamically in __init__. <a href='http://commits.kde.org/kajongg/74b105cd58c66576b688e7315799338c3a637805'>Commit.</a> </li>
<li>Hand: improve debug output. <a href='http://commits.kde.org/kajongg/76b3d7ed5986ab626116ca7afb949fcdc362d56d'>Commit.</a> </li>
<li>Log.fmt: _hideXXX variable names are not shown in output. <a href='http://commits.kde.org/kajongg/56f2a5ceff1eba197aea49c4ab78f7aa00844884'>Commit.</a> </li>
<li>Hand.__new__ does not output debug info anymore. <a href='http://commits.kde.org/kajongg/b6f1e2e3f7ca1dcdd8190d1c24e966d6ca1f0b2e'>Commit.</a> </li>
<li>Hand: change 2 docstrings. <a href='http://commits.kde.org/kajongg/d0336c04dfe28216753ca52911ab7ed4cf804a45'>Commit.</a> </li>
<li>Hand.__arrange: one more early abort for non winning long hands. <a href='http://commits.kde.org/kajongg/65d953c5cdf85b5fbaa1a8939d0da96aa7051fef'>Commit.</a> </li>
<li>Hand.__arrange(): if no winning arrangement is found, do not set hand.mjRule. <a href='http://commits.kde.org/kajongg/a427956e5b232775fd7846e583af4a1e28e305cd'>Commit.</a> </li>
<li>Hand.__NotWon exception: use msg argument, reducing duplicate code. <a href='http://commits.kde.org/kajongg/31eb5919990d99b3de497894564f4de0a4aad64f'>Commit.</a> </li>
<li>Hand: last debug output about fixed hand shows resulting hand, not its input string. <a href='http://commits.kde.org/kajongg/427ace1cc7aa8dbe03e81adc7de7fe34db494c6f'>Commit.</a> </li>
<li>Hand.__unicode__: new. <a href='http://commits.kde.org/kajongg/d4d4c8e7b45afeb6f0433a1c90ec51df4c6c6d43'>Commit.</a> </li>
<li>Debug-indent more clearly. <a href='http://commits.kde.org/kajongg/eb4f36135146f6dad587f784ebc62abe0f7d581a'>Commit.</a> </li>
<li>Server: clarify assertion message. <a href='http://commits.kde.org/kajongg/8a1e5cc7ed0bcc3bd47a9a0ffc6108421e6005ae'>Commit.</a> </li>
<li>Hand.mjStr removed. <a href='http://commits.kde.org/kajongg/4859ec25ba4fbcd27dfa90ddc2d8b9b798e8d520'>Commit.</a> </li>
<li>Hand.__add_ now uses newString instead of own building code. <a href='http://commits.kde.org/kajongg/b44e06807bdee9cea4b7a5a497b266bb9f604ab4'>Commit.</a> </li>
<li>Hand.__arrange uses newString() instead of mjStr. <a href='http://commits.kde.org/kajongg/df0fd9813019f6572c93fd3b708cb54378c0de1b'>Commit.</a> </li>
<li>Hand: use newString() instead of mjStr for __str__ and assertion. <a href='http://commits.kde.org/kajongg/e8ece754e8dec4f6c71aa60c7fb7652df947475f'>Commit.</a> </li>
<li>Hand.__parseString: add docstring. <a href='http://commits.kde.org/kajongg/7efba769a6fab59ec7767c55025e01a7502f168b'>Commit.</a> </li>
<li>New: Hand.newString, use in Hand.__add__, with assert. <a href='http://commits.kde.org/kajongg/fef39f555f92922f01df719837455d83efbf6fe1'>Commit.</a> </li>
<li>Hand.__sub__: Never set newHand.LastTile. <a href='http://commits.kde.org/kajongg/a82ad6fee401c4f9a02a3182bec9328a9681bb9a'>Commit.</a> </li>
<li>Hand.announcements now is a set. <a href='http://commits.kde.org/kajongg/56b71b6ed55a3e89efea4bf2c6727d5c9e7ae760'>Commit.</a> </li>
<li>Hand.__sub__: prepare for multiple announcements. <a href='http://commits.kde.org/kajongg/ed8540571c267f0b034bf8b8384463822f85b238'>Commit.</a> </li>
<li>Rename declaration to announcements. <a href='http://commits.kde.org/kajongg/69942725c061c262c64c6bdc1ead15c07899f89a'>Commit.</a> </li>
<li>Hand: __add__ produced wrong mjStr in presence of announcements. <a href='http://commits.kde.org/kajongg/d1f279fbe2c9295362f4a2c30771763937b74b56'>Commit.</a> </li>
<li>Hand.__sub__: never emit the "m" string part. <a href='http://commits.kde.org/kajongg/00400d9efe6493d8ce8f507909304aa19cb1692b'>Commit.</a> </li>
<li>Hand: integrate __setLastTile into __parseString. <a href='http://commits.kde.org/kajongg/614e26ac52f95b3e1350e43c85dd84da71ecb8ba'>Commit.</a> </li>
<li>Hand.__init__: small optimization. <a href='http://commits.kde.org/kajongg/2dd577a947b5115bfb05f7ce46ca1496beb6e94e'>Commit.</a> </li>
<li>Hand: re-order things in __init__ for better readability. <a href='http://commits.kde.org/kajongg/127466eb2aa1a56634b990a0fb96ac0825c45bda'>Commit.</a> </li>
<li>Hand: remove obsolete comments. <a href='http://commits.kde.org/kajongg/9ba67dd3044afca1d37385f250f6dd6c076d15b4'>Commit.</a> </li>
<li>Hand: extract code into __parseString. <a href='http://commits.kde.org/kajongg/994f16f909539d7ba10696c553dd70943b08fee7'>Commit.</a> </li>
<li>Make sure all MJ rule classes have method computeLastMelds. <a href='http://commits.kde.org/kajongg/f9285de1ab7c01a66e0474b1f87aa699dc5cdcc1'>Commit.</a> </li>
<li>Hand: small simplification. <a href='http://commits.kde.org/kajongg/efbd5e75c4889bc1933f4fcbc01759876e303adc'>Commit.</a> </li>
<li>Player: tighten an assertion. <a href='http://commits.kde.org/kajongg/bfce921bcbb79393951c623479272869088357b3'>Commit.</a> </li>
<li>Debug.mahJongg: more output. <a href='http://commits.kde.org/kajongg/ebccfaed28914abb7c2a86b2838e0e3aeed1c28b'>Commit.</a> </li>
<li>Player: remove a few asserts we do not need anymore. <a href='http://commits.kde.org/kajongg/9d85bd21bceff05f9c9e93df96d51e59f4a55807'>Commit.</a> </li>
<li>Player: rename arg withTile to withDiscard. <a href='http://commits.kde.org/kajongg/9706bfc948522403c7de1383c94d0a175b2ac2e5'>Commit.</a> </li>
<li>GatesOfHeaven.rearrange tried to do Tile("d1"). <a href='http://commits.kde.org/kajongg/9b0757c75d7866f4ac78a7298c3bc999d92ad633'>Commit.</a> </li>
<li>Fix output of Game.debug for non-ascii names. <a href='http://commits.kde.org/kajongg/e99b35d2828c9fa6eb6d098916b9b2ba5b96cc45'>Commit.</a> </li>
<li>Debug.scores: fix showing non-ascii player names. <a href='http://commits.kde.org/kajongg/3278b8a3828871f87ec932316da8112ab9c005c7'>Commit.</a> </li>
<li>HandId: better alignment for debug output. <a href='http://commits.kde.org/kajongg/001f09014854e2ada5d2835864b68087529331c7'>Commit.</a> </li>
<li>Add a few missing "from common import unicode". <a href='http://commits.kde.org/kajongg/e43315624320c88a65b0adb6cbca8b921ecc36f4'>Commit.</a> </li>
<li>Raise an exception if kde4-config is missing. <a href='http://commits.kde.org/kajongg/d017dd25da404edc640e47695e5da2ac54f25522'>Commit.</a> </li>
<li>M18n and friends expect args as unicode, not as utf-8. <a href='http://commits.kde.org/kajongg/fe8d3a391e563bd42fd491f9b8091d41316baf5d'>Commit.</a> </li>
<li>Log.callers: simplify, fixing bug: suppressed too much. <a href='http://commits.kde.org/kajongg/4d9d4213e486992bd218e75bcc942926a45e8b75'>Commit.</a> </li>
<li>Log: special-case display of callers: needs less output. <a href='http://commits.kde.org/kajongg/fc46ebed2a45051ce7734b64e137870cc839a92b'>Commit.</a> </li>
<li>Callers: exclude more methods from callstack, reverse display order. <a href='http://commits.kde.org/kajongg/3cba7cd7077373238c42054868a5a0dae2ace109'>Commit.</a> </li>
<li>Randoms now behave the same with python3 and python2. <a href='http://commits.kde.org/kajongg/22a7d0266b1e65b23c88f1d6ce766edac8d2e8fa'>Commit.</a> </li>
<li>Make Message classes sortable. Needed for kajonggtest python3 --log. <a href='http://commits.kde.org/kajongg/1cf22eac33b94ad60d2abb33d286b24203b2a2eb'>Commit.</a> </li>
<li>Debug / explain: show limit without decimals .0. <a href='http://commits.kde.org/kajongg/78e8340661e6f7d5b4ea397dafaa2d2f9e25c744'>Commit.</a> </li>
<li>Debug.neutral: setLanguage("en_US"). <a href='http://commits.kde.org/kajongg/274cd2b84767abfd0a90d6e60c24e25afe3223d0'>Commit.</a> </li>
<li>Kdestub/i18n: with Debug.neutral, do not translate. <a href='http://commits.kde.org/kajongg/f55ccdc03a35b9ce444f825a34782fe17940fc96'>Commit.</a> </li>
<li>Hand: make debug output more similar for Py2/Py3. <a href='http://commits.kde.org/kajongg/a15cb98099e47c5e84f7e49fd56474c465a31e86'>Commit.</a> </li>
<li>Change most "in set" to "in sorted(set", for better comparisons between test runs (Py2/Py3). <a href='http://commits.kde.org/kajongg/8e5a993794e34097104bfb9810a162c34f2044e9'>Commit.</a> </li>
<li>Common.unicodeString is now hopefully failsafe. <a href='http://commits.kde.org/kajongg/2709f4e802770af4b2182dcb67ee275e00011c0a'>Commit.</a> </li>
<li>Log: if git commit is wanted in prefix, also say if Python2 or Python3. <a href='http://commits.kde.org/kajongg/b84cd2bbdb2ddecaafdc87ba06db106d325ec1e0'>Commit.</a> </li>
<li>Kajonggtest: ignore spaces in tags field. <a href='http://commits.kde.org/kajongg/cc5f9a43d9d18eed50c22def3ee329be2901c2af'>Commit.</a> </li>
<li>Kajonggtest: show all difference details. <a href='http://commits.kde.org/kajongg/f3fd925aca2bbdec15a97df272b9a9d101bb1cf3'>Commit.</a> </li>
<li>Kajonggtest: accept any length for commit ids. <a href='http://commits.kde.org/kajongg/4161ddb31b8b8f89ed552ca02bf7ecce42663da2'>Commit.</a> </li>
<li>Kajonggtest: reduce unneeded output. <a href='http://commits.kde.org/kajongg/ad97a8dd315e084ebac3a36338cf888dd0d26e48'>Commit.</a> </li>
<li>Kajonggtest: remove output about common games, was faulty anyway. <a href='http://commits.kde.org/kajongg/cd956aeacccb825f9dfd75651e4182538f7cba00'>Commit.</a> </li>
<li>Kajonggtest --count=0: evaluate only once. <a href='http://commits.kde.org/kajongg/476a875bff791ca9c557cbce65edca4746ddd937'>Commit.</a> </li>
<li>Kajonggtest: new option --23 tests with both python2 and 3. <a href='http://commits.kde.org/kajongg/6de36f81411454e0ad87e91ed09365b4e1700382'>Commit.</a> </li>
<li>Kajonggtest: set LANG to en_US.UTF-8. <a href='http://commits.kde.org/kajongg/e517f6f8dc5dbc073904515d6e268e169fa8ef82'>Commit.</a> </li>
<li>Kajonggtest: correctly pass player name to client. <a href='http://commits.kde.org/kajongg/d3eda7933efdb7a17b663a962f925680f3c36131'>Commit.</a> </li>
<li>Kajonggtest: log directory subtree for 2 and 3. <a href='http://commits.kde.org/kajongg/012cf33e3ea9a16a0cf13879743e985f6f5895c5'>Commit.</a> </li>
<li>Csv: new first column with "2" or "3" for python dialect. <a href='http://commits.kde.org/kajongg/50329de838bc278652dbbc464a806e1bf25c1ee4'>Commit.</a> </li>
<li>Remove support for mixing python2 and python3 and merge version with defaultPort. <a href='http://commits.kde.org/kajongg/88f019b7f2480c95bb193f909920913975bfaedf'>Commit.</a> </li>
<li>Latest twisted supports sockets with python3. <a href='http://commits.kde.org/kajongg/f0b6970680fc8298b0d63e44e96d542cbd575096'>Commit.</a> </li>
<li>New: kajonggserver3. Use it when Options.server3. <a href='http://commits.kde.org/kajongg/71e0f56531d8c2904569e1392640d912e1571bf4'>Commit.</a> </li>
<li>Url.__findServerProgram says what it found when Debug.connections. <a href='http://commits.kde.org/kajongg/f8f61861c627ff415a23db04c2748dfbbcb18152'>Commit.</a> </li>
<li>Url.__findServerProgram(): rename local var args to result. <a href='http://commits.kde.org/kajongg/ffe654207b1131a6e4d2038200154d0ea8571590'>Commit.</a> </li>
<li>Refactor: create Url.__findServerProgram(). <a href='http://commits.kde.org/kajongg/a4bbc44f16e25e1d7a2b6a5f55aad0a02ea12bdb'>Commit.</a> </li>
<li>If kajongg is running under python3, use python3 as default for kajonggserver. <a href='http://commits.kde.org/kajongg/a95d3a1263587243a4fb7534f91f8312c748628b'>Commit.</a> </li>
<li>Kajonggtest: show detailled info about differences. <a href='http://commits.kde.org/kajongg/bbc982a44f357d6de19471d3296608dbb4d997d3'>Commit.</a> </li>
<li>SQLite query may fail with IntegrityError having no message attribute. <a href='http://commits.kde.org/kajongg/45f10077d9b5ba55df5029a8c0a8566d5a8e1529'>Commit.</a> </li>
<li>Kajonggtest: port subprocess calls to python3. <a href='http://commits.kde.org/kajongg/c0d80b6453f1b8cc4f8ad54d7d82de043e99733e'>Commit.</a> </li>
<li>Kajonggtest: pretty format output. <a href='http://commits.kde.org/kajongg/a7a195e175a2f0dc68ab987ce1758d96b2d6936d'>Commit.</a> </li>
<li>Kajonggtest can now allocate more than one server per commit. <a href='http://commits.kde.org/kajongg/b2de9da27a44cf752ff48dee9bab12d3a042fe2a'>Commit.</a> </li>
<li>Kajonggtest: show tmp source dir. <a href='http://commits.kde.org/kajongg/c8083ffb2eb628021a7abcceb9ce16b97d98dd5c'>Commit.</a> </li>
<li>Kajonggtest: with --log, do not write git commit ids into debug output, we want to compare logs. <a href='http://commits.kde.org/kajongg/55eeb3b52cc29a86017e2c03004869aa11498342'>Commit.</a> </li>
<li>Kajonggtest: if src/twisted is a symbolic link, copy it into the tempdir. <a href='http://commits.kde.org/kajongg/3df5ccf9ac8d678749d553ed7d43f132237a2f04'>Commit.</a> </li>
<li>Kajonggtest now finds git in /usr/local/bin too. <a href='http://commits.kde.org/kajongg/fdee4a6d00f2ecbe19d01ac7485d7fe251f5f7d3'>Commit.</a> </li>
<li>Kajonggtest: cleanly separate code for --port and --socket. <a href='http://commits.kde.org/kajongg/6c15ac9ae2948805f17bc434d31cccedaddfe92d'>Commit.</a> </li>
<li>Kajonggtest: always use a non-ascii player. <a href='http://commits.kde.org/kajongg/71ac80d06243fbe92be40cad3a975f154d79048e'>Commit.</a> </li>
<li>CSV: encapsulate differences between py2 and py3 in new compat.py. <a href='http://commits.kde.org/kajongg/72179021015e7a2ba7c7a483d9d75efc70e3ecda'>Commit.</a> </li>
<li>DBID is native string. <a href='http://commits.kde.org/kajongg/118a84030403f8692f5e4ba4693cc056c770cee4'>Commit.</a> </li>
<li>--game=555/ now gives a better error message. <a href='http://commits.kde.org/kajongg/7a643996dfa80d5b90d42fe81d7866a019aeedc1'>Commit.</a> </li>
<li>--nogui implies no sound. <a href='http://commits.kde.org/kajongg/62cbdaf3c3529d5ed8ade937e4642f02bac0b087'>Commit.</a> </li>
<li>Knitting rules delivered random results (another unsorted set). <a href='http://commits.kde.org/kajongg/4b7f8fea3752c54b86777d31609be342aaaf64e3'>Commit.</a> </li>
<li>Get rid of a small TODO. <a href='http://commits.kde.org/kajongg/33cd3aafe45d24f5ac5671b26de6701e0256c42c'>Commit.</a> </li>
<li>GatesOfHeaven/NineGates: ComputeLastMelds may only return existing melds. <a href='http://commits.kde.org/kajongg/dc266864f2263ee96ef11b2e963334d8b0683636'>Commit.</a> </li>
<li>NineGates and GatesOfHeaven: more precise descriptions. <a href='http://commits.kde.org/kajongg/b27cc5e6fc903a49b5d39b1838c4fed57e1f6745'>Commit.</a> </li>
<li>Fix a small copy&paste bug in TripleKnitting. <a href='http://commits.kde.org/kajongg/5caa47c7f5c3a687207b11feef2deb20f89df15c'>Commit.</a> </li>
<li>--rounds did not work for values>1. <a href='http://commits.kde.org/kajongg/1894caaa2c0b60492783c56ed1070ead0702f6d8'>Commit.</a> </li>
<li>Debug.dangerousGame: fix a small regression from fixing bug 355266. <a href='http://commits.kde.org/kajongg/1f67e96c53e756732b838f2bf8bc1ec8f2d8bac8'>Commit.</a> </li>
<li>Meld.isKong must be False for four hidden tiles, as the docstring already said. <a href='http://commits.kde.org/kajongg/0a40795f5b7b44590fe492fcfeac50edcc269097'>Commit.</a> </li>
<li>NineGates.rearrange is new. <a href='http://commits.kde.org/kajongg/2b4f513371492414294243a22f5df7016fcb704b'>Commit.</a> </li>
<li>Server.claimMahJongg: add missing return after abort(). <a href='http://commits.kde.org/kajongg/568e651740a0d08ab0a108bc76738587221be250'>Commit.</a> </li>
<li>When changing view angle, an assertion was sometimes triggered. <a href='http://commits.kde.org/kajongg/4748fb4496f7e734d3402f0dae9961f7e49641c0'>Commit.</a> </li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Show]</a></h3>
<ul id='ulkalarm' style='display: none'><li>New in this release</li></ul>
<h3><a name='kalarmcal' href='https://cgit.kde.org/kalarmcal.git'>kalarmcal</a> <a href='#kalarmcal' onclick='toggle("ulkalarmcal", this)'>[Show]</a></h3>
<ul id='ulkalarmcal' style='display: none'>
<li>Fix some clazy warning. <a href='http://commits.kde.org/kalarmcal/7e66f770f71828c5d42c430e0062deb711719e92'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/kalarmcal/3aabc7189e910c45c3d3179b9bbfa14763838277'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kalarmcal/31149f2dcbd8cb54af8a6b12ebb341b16d31f5d8'>Commit.</a> </li>
</ul>
<h3><a name='kalgebra' href='https://cgit.kde.org/kalgebra.git'>kalgebra</a> <a href='#kalgebra' onclick='toggle("ulkalgebra", this)'>[Show]</a></h3>
<ul id='ulkalgebra' style='display: none'>
<li>Port to QtQuick.Controls 2.0. <a href='http://commits.kde.org/kalgebra/eacb5bdaf852ffc744c3d12554bc21e28863617e'>Commit.</a> </li>
<li>Let the images' size depend on the page width. <a href='http://commits.kde.org/kalgebra/ec4e94bded544196196c68a0c9b0094b2eff34e4'>Commit.</a> </li>
<li>Neutral doesn't really have a reason to be anymore. <a href='http://commits.kde.org/kalgebra/d237a20575f4fef97b83854860d21cae02a11076'>Commit.</a> </li>
<li>Make it possible to export 3D plots to STL. <a href='http://commits.kde.org/kalgebra/4816fee60d41010316c823f97e056cb4045adfb1'>Commit.</a> </li>
<li>Improve Kirigami usage. <a href='http://commits.kde.org/kalgebra/89c6e000d1bcd18ed878eea6f8a5cebbe80e44d4'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kalgebra/8f585b2f6f2a598b00602b17411cedd25f17858d'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kalgebra/e655f035a395aa229c13fcbed596975e08cd2d93'>Commit.</a> </li>
</ul>
<h3><a name='kalzium' href='https://cgit.kde.org/kalzium.git'>kalzium</a> <a href='#kalzium' onclick='toggle("ulkalzium", this)'>[Show]</a></h3>
<ul id='ulkalzium' style='display: none'>
<li>Fix the soname of kalzium internal libraries. <a href='http://commits.kde.org/kalzium/e16b0159acc01efcdc8b51b04b8eb6303b16b0db'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373565'>#373565</a></li>
<li>Connect to correct application quit slot. <a href='http://commits.kde.org/kalzium/7c9e0221cd4a6d9d58c7ee0c9bac32f4e06b40da'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371963'>#371963</a></li>
<li>Update screenshot. <a href='http://commits.kde.org/kalzium/80cb092c3462d1e4c28593afef4f4035e4fc40ed'>Commit.</a> </li>
<li>Fix broken unit selection and update documentation. <a href='http://commits.kde.org/kalzium/5a30bec4acc10fc242604ccba56cf66711bac476'>Commit.</a> </li>
<li>Update molecule editor documentation. <a href='http://commits.kde.org/kalzium/ba357b4be0dbd58e66aa9ffdf8df77f1635592dd'>Commit.</a> </li>
<li>Fix .desktop Exec key for window title. <a href='http://commits.kde.org/kalzium/cd47ccb5f7b028546ca194b927e51ca09cbe6594'>Commit.</a> </li>
<li>Replace obsolete QStyleOption subclass. <a href='http://commits.kde.org/kalzium/07e2e47700e542f57980eb901c4aff7717157693'>Commit.</a> </li>
<li>Update screenshots for kf5. <a href='http://commits.kde.org/kalzium/b75387777e29378d27d60193d9e736aabcd41aa6'>Commit.</a> </li>
<li>Port away from KLocale/KDELibs4Support + make all about tabs translated. <a href='http://commits.kde.org/kalzium/c04108861a707d730cc8b3a35a154ef80711313c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129291'>#129291</a></li>
<li>Proofread/update Kalzium docbook for kf5. <a href='http://commits.kde.org/kalzium/856678c97f72e357bec5d52962b77389af7af627'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129290'>#129290</a></li>
<li>Proofread/update Kalzium man page for kf5. <a href='http://commits.kde.org/kalzium/cd2394048736ade066dd60a3fefb7afe43764994'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129292'>#129292</a></li>
<li>Bump development years. <a href='http://commits.kde.org/kalzium/79667a08db5b8fa8a6dba9132c69000b5da4e15f'>Commit.</a> </li>
<li>Time for bumping the version. <a href='http://commits.kde.org/kalzium/24a3d9ed20ebed94d3944e7f964c90946a74a561'>Commit.</a> </li>
<li>Minor cleanup. <a href='http://commits.kde.org/kalzium/4196fc71d8084249ae04aefddbc803dcda40aa51'>Commit.</a> </li>
<li>Add workaround for broken QtGui::Molecule copy constructor. <a href='http://commits.kde.org/kalzium/2665958aa5d6cf804f2e71fe2f452b5555f2805f'>Commit.</a> </li>
<li>Comment out optimization button. <a href='http://commits.kde.org/kalzium/877408c4ab8a6c2138322f5de5cec1acb8d02048'>Commit.</a> </li>
<li>Adapt molecule view UI for better Avogadro integration. <a href='http://commits.kde.org/kalzium/a8995abf114f7bf6958cf953829d4ef882e68e35'>Commit.</a> </li>
<li>Remove atom label combobox. <a href='http://commits.kde.org/kalzium/8fbc2d7c37cd3f5494b02adc0092786f02ca780c'>Commit.</a> </li>
<li>Minor cleanup. <a href='http://commits.kde.org/kalzium/59936cf7181dc4d367e4778235f0614e0d762c82'>Commit.</a> </li>
<li>Correctly set and update rendering plugins. <a href='http://commits.kde.org/kalzium/8d7af4899b3ae66af45e253aafb043315f0c6a54'>Commit.</a> </li>
<li>Make compatible with latest Avogadro2 release. <a href='http://commits.kde.org/kalzium/ac81d6b1e2014ab26b636f4af4f970dc3e2cf260'>Commit.</a> </li>
<li>Complete KUrl porting. <a href='http://commits.kde.org/kalzium/297d641f30d5487a4ddc98d996ce0c554bf2e064'>Commit.</a> </li>
<li>Use new connect mechanism. <a href='http://commits.kde.org/kalzium/fa49fe6ec90b7691f5c8b746841b08ef9c7feb27'>Commit.</a> </li>
<li>Update KXMLGUI rc file location. <a href='http://commits.kde.org/kalzium/f99c8eda2cb6ad94c91465293be4d7b2e82584c4'>Commit.</a> </li>
<li>Revisit glossary dialog porting. <a href='http://commits.kde.org/kalzium/b2e5193e3d93dcf687e4f562b8e7bf9ea3b738fb'>Commit.</a> </li>
<li>Revisit broken element info dialog port. <a href='http://commits.kde.org/kalzium/2d5de6b0fbecf677cb35db742d02d5aeb3438c40'>Commit.</a> </li>
<li>Adapt paths to KHTML behavior change for local URLs. <a href='http://commits.kde.org/kalzium/18fae5c3d61aaffbca45539030e39f6881c513c8'>Commit.</a> </li>
<li>Specify mime when calling URL opener. <a href='http://commits.kde.org/kalzium/5c152d375cabd73eb7757f1ae621f9fb7ba3733c'>Commit.</a> </li>
<li>Fix application init. <a href='http://commits.kde.org/kalzium/9f58befa3305a4b7294b6a38e5f010230f224226'>Commit.</a> </li>
<li>Fix port to QStandardPaths::locate(...). <a href='http://commits.kde.org/kalzium/1c09a84308bc808aff7baae95412e4e538b33685'>Commit.</a> </li>
<li>Fix minor typos. <a href='http://commits.kde.org/kalzium/ad24b0f296504a22f0c54d17eb3f6b3252eca143'>Commit.</a> </li>
<li>Update etymology of Y, Yb, Er, Tb, and Na, K, W. <a href='http://commits.kde.org/kalzium/2ab5ecdb2dbe6025e389aec6fa164f0e5629b02d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128483'>#128483</a>. Fixes bug <a href='https://bugs.kde.org/338679'>#338679</a></li>
<li>Update element symbols and entymology for newly-named elements: Nh, Mc, Ts, Og. <a href='http://commits.kde.org/kalzium/e08ebbb65991e30f55e24dc294baf5d4dffe9935'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128482'>#128482</a></li>
<li>Update etymology of element Hafnium. <a href='http://commits.kde.org/kalzium/62ad50f65ee3f731f5a31290095b2746ab467575'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128481'>#128481</a></li>
<li>Rename desktop file to use reverse dns naming standard. <a href='http://commits.kde.org/kalzium/842768de1f222a78e9025606e730fae51e9e3707'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128502'>#128502</a></li>
<li>Change "German" to "Norse" in description for Thorium. <a href='http://commits.kde.org/kalzium/9724d05298e23082236107859a8d39a10b13c60f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/338678'>#338678</a>. Code review <a href='https://git.reviewboard.kde.org/r/128474'>#128474</a></li>
<li>Use the new ECM variable for the appstream dir. <a href='http://commits.kde.org/kalzium/f02524fd2dfc1903e95a7c5c695cdc93fe7c82bb'>Commit.</a> </li>
<li>Kalzium master is still KDE4, so the ECM variables do not work. <a href='http://commits.kde.org/kalzium/2e826e8163bc5bd8f0c05967cd82ee374e9ae786'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kalzium/94ea9cb5c5d5d9aa3e0173d1852716e0073cdd5a'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kalzium/f120cba2e5c2388ece364923de4b70df8fe3ee54'>Commit.</a> </li>
<li>Use existing API. <a href='http://commits.kde.org/kalzium/9b1fe3393b63a2e0e6b4161abb154fe989ee3f6f'>Commit.</a> </li>
<li>Use QtQuick inUse QtQuick instead of QtDeclarative. <a href='http://commits.kde.org/kalzium/ee89aa4a7f33c1be07c53b2eda0e5c27ac9f8dcb'>Commit.</a> </li>
<li>Port Avogadro editor tools. <a href='http://commits.kde.org/kalzium/7fc6dcb26d0e716526e812cb6beba6ceb6ec1e3f'>Commit.</a> </li>
<li>Port loading of Avogadro tools. <a href='http://commits.kde.org/kalzium/3c95c89f6585903e21905c3c136697edcc915970'>Commit.</a> </li>
<li>Finish Avogadro porting of molecule view. <a href='http://commits.kde.org/kalzium/0b82a51ec33fbcf236439d14a86214ab5deb8bde'>Commit.</a> </li>
<li>Complete port of OpenBabel2Wrapper and rename to IoWrapper. <a href='http://commits.kde.org/kalzium/1a67cb2142dc2dbf7a540666c3cffab21f32ddf5'>Commit.</a> </li>
<li>Postpone port. <a href='http://commits.kde.org/kalzium/d0d765ecbe8d5ef796b02390439bddf4f0044546'>Commit.</a> </li>
<li>Load plugins. <a href='http://commits.kde.org/kalzium/bc63b50061d5c291b76d68f7163d832fbc09dbdb'>Commit.</a> </li>
<li>UndoStack not required anymore. <a href='http://commits.kde.org/kalzium/006dd0518bf59a89cb56a25777c287d523c362b1'>Commit.</a> </li>
<li>Cleanup and some straight forward Avogadro2 porting. <a href='http://commits.kde.org/kalzium/9cf9db11b1889419aba191c6655923779b31cda0'>Commit.</a> </li>
<li>Fix compile, comment out missing plugin header. <a href='http://commits.kde.org/kalzium/4bf130253b8be4b02b49171718a6994ad1ec3604'>Commit.</a> </li>
<li>Prefer const variable over define. <a href='http://commits.kde.org/kalzium/1eee121af8f734667143617ac677ca0807c512c5'>Commit.</a> </li>
<li>Merge KalziumConfigureChecks with main CMake configuration. <a href='http://commits.kde.org/kalzium/7ba7f72f14304887ab3f2700275a52af7e5a4ca1'>Commit.</a> </li>
<li>Compile porting to Avogadro2. <a href='http://commits.kde.org/kalzium/98edb9981f67a3f289e03bf903953c9f56cfea31'>Commit.</a> </li>
<li>Add include path. <a href='http://commits.kde.org/kalzium/465a40e4f2824163324fa2f0ce43f59a01767668'>Commit.</a> </li>
<li>Use correct CMake variables. <a href='http://commits.kde.org/kalzium/3d2493684888577262e302767c16c888c8976375'>Commit.</a> </li>
<li>Port away from KPixmapCache. <a href='http://commits.kde.org/kalzium/391857e7697727f684f745d123a24e673b377cbe'>Commit.</a> </li>
<li>Port away from KGlobalSettings. <a href='http://commits.kde.org/kalzium/a9e57b361393b083843f6b86f5fcb6a21cabf653'>Commit.</a> </li>
<li>Port away from KGlobal. <a href='http://commits.kde.org/kalzium/841d4175ae45abe362fbca1efa586120ba21ee07'>Commit.</a> </li>
<li>Fix porting of unit conversions in calculator. <a href='http://commits.kde.org/kalzium/3b0bcbf80c0a3636695a4d3d3a2a0e16e0acedbe'>Commit.</a> </li>
<li>Revert changes on OpenBabel2Wrapper. <a href='http://commits.kde.org/kalzium/1190ea4de7aaff1a016114c2c88ede127d55a3a4'>Commit.</a> </li>
<li>Sort and wrap CMakeLists. <a href='http://commits.kde.org/kalzium/b47c4da2fdddaac7178a35a37037430698239c8a'>Commit.</a> </li>
<li>Enable translations for UI files. <a href='http://commits.kde.org/kalzium/36dfb54d1fa79db80cf946e746a7f1c92d601d2d'>Commit.</a> </li>
<li>Cleanup CMakeLists file. <a href='http://commits.kde.org/kalzium/b7485856d7c7dd5d4b1852a72ce387d41eaceb7e'>Commit.</a> </li>
<li>Cleanup definitions. <a href='http://commits.kde.org/kalzium/6adad7e25ecd84111dd859923725718629820651'>Commit.</a> </li>
<li>Both AvogadroLibs and Eigen3 are optional. <a href='http://commits.kde.org/kalzium/6b9cf7154f5c41a23c96051a5e83e63ac0c33078'>Commit.</a> </li>
<li>Update doc data (release information, etc). <a href='http://commits.kde.org/kalzium/6e2bb3487238a4f636c791ebfe56a63c8b9ab2cb'>Commit.</a> </li>
<li>Use the new DTD, explicitly depend on KDocTools. <a href='http://commits.kde.org/kalzium/72730dfad6a89d9c83e81daf09b7a12e8353b95d'>Commit.</a> </li>
<li>Fix deprecated KCoreConfigSkeleton function calls. <a href='http://commits.kde.org/kalzium/32b96f5ddf69f6815af2dc994b33e069101ed7bb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125202'>#125202</a></li>
<li>Initialize m_elementInfo in Kalzium constructor. <a href='http://commits.kde.org/kalzium/91c050cd4bcabd1cfa6e4a226f873ba5e4948334'>Commit.</a> </li>
<li>Port K4AboutData -> KAboutData. <a href='http://commits.kde.org/kalzium/a8f7980bec385bd7f31ea9e43a2f59fefb56248d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125188'>#125188</a></li>
<li>Port KStatusBar -> QStatusBar. <a href='http://commits.kde.org/kalzium/f7e144b101d58eb6e04ce03d0f66bef68ac0bc68'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125181'>#125181</a></li>
<li>Port KDebug -> QDebug. <a href='http://commits.kde.org/kalzium/a2558fac17e48e291aebefa7f483bf0e0b7e1995'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125180'>#125180</a></li>
<li>Port KStandardDirs -> QStandardPaths. <a href='http://commits.kde.org/kalzium/19a8d84d382e9ea1cb8e9eb84876994b81f34065'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125173'>#125173</a></li>
<li>Port KTextBrowser -> QTextBrowser. <a href='http://commits.kde.org/kalzium/6e68d2750ecf9728f61a444bf7b52845ec51991f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125170'>#125170</a></li>
<li>Port KIcon -> QIcon. <a href='http://commits.kde.org/kalzium/a1fba062ec80e32dc286bc406a707a09b76caafa'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125169'>#125169</a></li>
<li>Port KLineEdit -> QLineEdit. <a href='http://commits.kde.org/kalzium/c7b48ea038cb94228c2e28cc0391bdf0c381ca59'>Commit.</a> </li>
<li>Port KTabWidget -> QTabWidget. <a href='http://commits.kde.org/kalzium/76bdcfe045c35aec33fa8792e35371822311018b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125157'>#125157</a></li>
<li>Port KUrl -> QUrl. <a href='http://commits.kde.org/kalzium/08ab3c35e50426962285401bdf152109b7f77cc2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125150'>#125150</a></li>
<li>Port KPushButton -> QPushButton. <a href='http://commits.kde.org/kalzium/70825cdc157f6bb2e708b71901e217ceed87f4b0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125149'>#125149</a></li>
<li>Fix QIcon port. <a href='http://commits.kde.org/kalzium/9c08b2f145ae5df85c5e4581774fbda2c8573285'>Commit.</a> </li>
<li>Add missing desktop file from rename. <a href='http://commits.kde.org/kalzium/8d71f707c393ea2dc6604d9b0789c67039646e18'>Commit.</a> </li>
<li>Remove empty variable. <a href='http://commits.kde.org/kalzium/4443007d1d3b36f85a6c75230c6c20d58354efbf'>Commit.</a> </li>
<li>Use reverse notation for desktop files. <a href='http://commits.kde.org/kalzium/b397d9447b1bf7c2c4dce5e27cf67ed24bb127ec'>Commit.</a> </li>
<li>Fix QMessageBox port. <a href='http://commits.kde.org/kalzium/8b8977f65602466c0ea560453d0fb2adcadcd286'>Commit.</a> </li>
<li>Revert changes to wrong Qt classes. <a href='http://commits.kde.org/kalzium/f6c7e5f579bc34e2b75f915f345ba3813ff05581'>Commit.</a> </li>
<li>Reverting changes. <a href='http://commits.kde.org/kalzium/f73f5ea045a2498f1b9a7a56c655c5c4a1222624'>Commit.</a> </li>
<li>Removing warnings from kalziumdataobject.cpp. <a href='http://commits.kde.org/kalzium/8c21b13bc9a231bc25ff084434e7bd51558ad732'>Commit.</a> </li>
<li>Removing warnings from gradientwidget_impl.cpp and detailinfodlg.cpp. <a href='http://commits.kde.org/kalzium/3c8c3678a03a4b4eaaf1ec2ff4af2c73e87d7c04'>Commit.</a> </li>
<li>Removing warnings from main.cpp. <a href='http://commits.kde.org/kalzium/5738d22874599295b8872cb7a5720cda79182629'>Commit.</a> </li>
<li>Removing warnings from kalzium.cpp. <a href='http://commits.kde.org/kalzium/66be2212a43f328da215317f8763c9ac393882f2'>Commit.</a> </li>
<li>Provide moc information for the GlossaryDialog::Private. <a href='http://commits.kde.org/kalzium/2ba297327953b4083b584c8f8a726973add25782'>Commit.</a> </li>
<li>Link to Qt5-based Avogadro. <a href='http://commits.kde.org/kalzium/b316e365250152e6cfa8906b53a9580c848df656'>Commit.</a> </li>
<li>Uncomment function, fix logic. <a href='http://commits.kde.org/kalzium/55e6bf6d4d8aa186b9bf842883f3146ff41b5171'>Commit.</a> </li>
<li>Include the CMake settings. <a href='http://commits.kde.org/kalzium/531f51ee04569952fcc83618dd6b5ed645142a1d'>Commit.</a> </li>
<li>Dealing with KDialog and CMakeLists. <a href='http://commits.kde.org/kalzium/94d37ad7fac627818009d6d8c807493a39882591'>Commit.</a> </li>
<li>Porting src :-1: error: cannot find -lQt5::Declarative. <a href='http://commits.kde.org/kalzium/5426d9030c9f3a2d9f66ca14492ca231e773dc19'>Commit.</a> </li>
<li>Porting src- KUnitConverion error. <a href='http://commits.kde.org/kalzium/c3a3248eb57d97af85bdb4ac262c26dc1d22e212'>Commit.</a> </li>
<li>Src kalzium.cpp and main.cpp ported. <a href='http://commits.kde.org/kalzium/97d4d053ce254b3e57ad10b484d178d3a6aad4e4'>Commit.</a> </li>
<li>Porting last subfolder of src -tools. <a href='http://commits.kde.org/kalzium/b01feabb497a05e1b2703b4a0d674e6aab7987c5'>Commit.</a> </li>
<li>Src/calculation/gasCalculator.cpp invalid conversion and casting errors. <a href='http://commits.kde.org/kalzium/75491289daf310bee957f27c0362a6bac4ca1473'>Commit.</a> </li>
<li>Porting src folder -I. <a href='http://commits.kde.org/kalzium/76f67c7302ec4e02c30f997bcf266fc28277ecb7'>Commit.</a> </li>
<li>Ported libscience, started porting src folder. <a href='http://commits.kde.org/kalzium/48a9708455d81619beaac2881e9e3451bbe6854c'>Commit.</a> </li>
<li>Give Anu a hand at porting. <a href='http://commits.kde.org/kalzium/56ccc3f70f550d71bd87ffbbf4e0e9d1f3c10ad8'>Commit.</a> </li>
<li>Porting :KalziumConfigureChecks.cmake and plasmoid/applet. <a href='http://commits.kde.org/kalzium/c3b298e1a51f1a9dac668248166ef59bbd1d8d33'>Commit.</a> </li>
<li>Porting: src CMakeLists.txt. <a href='http://commits.kde.org/kalzium/ce9292edb8ab9cbaddfe3f47ab783165fb4cfdeb'>Commit.</a> </li>
<li>Porting: plasmoid CMakeLists.txt. <a href='http://commits.kde.org/kalzium/12c0dc12b39cbff4eb65b16a89b7861079ddb330'>Commit.</a> </li>
<li>Ported doc and libscience. <a href='http://commits.kde.org/kalzium/768265a6f4462538c81d9be9fe46bdfb979ad2e4'>Commit.</a> </li>
<li>Porting: renamed png and svgz in  data/icons. <a href='http://commits.kde.org/kalzium/091a75c1d1f64e9284a60770e8804ecd267dfbb7'>Commit.</a> </li>
<li>Ported compoundviewer. <a href='http://commits.kde.org/kalzium/1004a6cdae28afc80aec46a29dc4f0b710463628'>Commit.</a> </li>
</ul>
<h3><a name='kamera' href='https://cgit.kde.org/kamera.git'>kamera</a> <a href='#kamera' onclick='toggle("ulkamera", this)'>[Show]</a></h3>
<ul id='ulkamera' style='display: none'>
<li>Revert "Fix i18n missing TRANSLATION_DOMAIN issue.". <a href='http://commits.kde.org/kamera/5b6550ccd5ad6c827e663de06f329ee79297842d'>Commit.</a> </li>
<li>Fix i18n missing TRANSLATION_DOMAIN issue. <a href='http://commits.kde.org/kamera/82b6e76a5b6ca69cc14a445c9a976bb8e007eec1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126442'>#126442</a></li>
<li>Revert "Fix i18n missing TRANSLATION_DOMAIN issue.". <a href='http://commits.kde.org/kamera/c5b29ab7ba33ecbbed7e6b95d5ce23d5c1ef1caa'>Commit.</a> </li>
<li>Fix i18n missing TRANSLATION_DOMAIN issue. <a href='http://commits.kde.org/kamera/8587a011f2d7525596149fc53f2957587ab84e5b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126442'>#126442</a></li>
<li>Move kamera kcm to the new subsection removable-storage. <a href='http://commits.kde.org/kamera/8277178cdfae15121607e48577d8cb2a367acab1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128447'>#128447</a></li>
</ul>
<h3><a name='kanagram' href='https://cgit.kde.org/kanagram.git'>kanagram</a> <a href='#kanagram' onclick='toggle("ulkanagram", this)'>[Show]</a></h3>
<ul id='ulkanagram' style='display: none'>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kanagram/14e00e418e988c56d8bffd447fd4591eb702d66d'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kanagram/65422f8c2bddd567368f7aab6637cbd2d11a8139'>Commit.</a> </li>
</ul>
<h3><a name='kapman' href='https://cgit.kde.org/kapman.git'>kapman</a> <a href='#kapman' onclick='toggle("ulkapman", this)'>[Show]</a></h3>
<ul id='ulkapman' style='display: none'>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kapman/1b68f4d1160ba0e2c590b11b1ecd599db59c1e83'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kapman/56d71f278e9bab33a2dbb1a6e3f263da8223212b'>Commit.</a> </li>
</ul>
<h3><a name='kapptemplate' href='https://cgit.kde.org/kapptemplate.git'>kapptemplate</a> <a href='#kapptemplate' onclick='toggle("ulkapptemplate", this)'>[Show]</a></h3>
<ul id='ulkapptemplate' style='display: none'>
<li>Add namespace to desktop and appdata files for KF5 templates. <a href='http://commits.kde.org/kapptemplate/0af1101cce78adc99053a86894c50b4954417896'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129360'>#129360</a></li>
<li>ECM prefers icon names to not have a theme prefix now. <a href='http://commits.kde.org/kapptemplate/4e9424907043164015b262c9a3bfa5649f139bf8'>Commit.</a> </li>
<li>Switch to to the new ECM template function and clean up the preview images. <a href='http://commits.kde.org/kapptemplate/0d1cd177f7477c0c362afffc535b4e39c650a09e'>Commit.</a> </li>
<li>Rename desktop file to use reverse dns naming standard. <a href='http://commits.kde.org/kapptemplate/55904c8ca6ca0c8b3ae4eaec0698c9f39196522c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128506'>#128506</a></li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kapptemplate/270ffb6ad8fea1a9025f425bf68feb3e4ff2c580'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kapptemplate/a7d72a5599c20f5516f8a5d8bc7fe0b7ee35f6c6'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>Fix SQL plugin combobox to actually show on the toolbar. <a href='http://commits.kde.org/kate/04a9ba13ebf279b8483377554cd1782661bbc5f8'>Commit.</a> </li>
<li>Fix: Kate crashes when trying to drag tabs between windows. <a href='http://commits.kde.org/kate/0b05000bfdde06aec2dc6528411ec24c9e20e672'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372025'>#372025</a></li>
<li>Do not crash when looking-up a word without the project tool-view. <a href='http://commits.kde.org/kate/8666aaab8eac67a2e377d80275e1c6a2860b5a6f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371976'>#371976</a></li>
<li>Fix segfault in ProxyItem::remChild by double-removing the same item. <a href='http://commits.kde.org/kate/1a14f27fd626451c3ac0c24b0ec594c0e2dbe7db'>Commit.</a> </li>
<li>Implemented Feature Request: Expand/Collapse all actions for items in the documents list. <a href='http://commits.kde.org/kate/0b2997addf31e4c68580445271c460c61c658342'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360195'>#360195</a></li>
<li>Update chocolatey installer script. <a href='http://commits.kde.org/kate/58d306871b2024fa15a08f2697f9ff3b43d14846'>Commit.</a> </li>
<li>Fix CMP0063 warnings from CMake. <a href='http://commits.kde.org/kate/54e232d37abb1ba3949c8df0e7e1822531943d50'>Commit.</a> </li>
<li>Update to Applications 16.08.2 release with KDE Frameworks 5.27. <a href='http://commits.kde.org/kate/bbd921df999af95be793d919764d09d3300cd0e3'>Commit.</a> </li>
<li>Session chooser: Ensure no unnecessary horizontal scroll bar. <a href='http://commits.kde.org/kate/d664483ac5e5e01cf13a3f8d9706cd64229b9d15'>Commit.</a> </li>
<li>Avoid that files are opened duplicated, we want canonical names, if possible. <a href='http://commits.kde.org/kate/cdd89582a7c22fdbb8709a0047f324df79e10684'>Commit.</a> </li>
<li>Turn the version into a integer number (rather thing of a revision). <a href='http://commits.kde.org/kate/92e71151321fd642e5dc14948a30d1f61edfcbe4'>Commit.</a> </li>
<li>Use {xxx} syntax for 1112121 replaces. <a href='http://commits.kde.org/kate/1593af0c1152dc0150d8f08d05f6df56bfe14619'>Commit.</a> </li>
<li>Bug 365124 - Regex capture groups for text fails beyond \9 (edit). <a href='http://commits.kde.org/kate/c06dc8d8d63f2c852a4b0adcd012bd2fa2b37e69'>Commit.</a> </li>
<li>Fix drag & drop of folder into Kate window. <a href='http://commits.kde.org/kate/fa5e37af33f44f6e721880042a48e162973c4840'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366568'>#366568</a></li>
<li>Support --encoding together with --stdin. <a href='http://commits.kde.org/kate/50c656517729bd6e2cbc21d20c7b640df6948eef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364169'>#364169</a></li>
<li>Bug 360860 - Filter text dialog max width problem. <a href='http://commits.kde.org/kate/f0b747a14be6edd90578dfed6e05e13154d26448'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360860'>#360860</a></li>
<li>Use last location of open file dialog instead of home if current document has no valid url (e.g. untitled). <a href='http://commits.kde.org/kate/2daae41a3738cc319f5925a96d3938779b3bd09b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364128'>#364128</a></li>
<li>Load plugins in right order to have preference for plugins first in plugin path. <a href='http://commits.kde.org/kate/f1e8c7a53950fdf78546869cc55dfdae5e5f983a'>Commit.</a> </li>
<li>Allow to activate elements via keypresses. <a href='http://commits.kde.org/kate/0a483167b98715181c72a3cfc0ffd37a1fa41cd8'>Commit.</a> </li>
<li>Kate sidebar does not appear with old sessions. <a href='http://commits.kde.org/kate/e1d70b2f59f901b9576cd146df8545a314e1f583'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/267618'>#267618</a></li>
<li>Fix shortcut clash, use shift-alt-left/right. <a href='http://commits.kde.org/kate/b45c597bd9968d4d20dedebef2a7e0b03938ee96'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/328170'>#328170</a></li>
<li>Add tooltip to symbolsviewer plugin. <a href='http://commits.kde.org/kate/0e1086254f2609b5fa6324af19a1d8451fb50e28'>Commit.</a> </li>
<li>Link typo fix htm->html. <a href='http://commits.kde.org/kate/d4a2f3d9e2e63d190ab9550c13aa4750fe72e442'>Commit.</a> </li>
<li>Fix wrong link to katepart highlight section. <a href='http://commits.kde.org/kate/8dbdf8df6bd86d5f489ec464674582e496be48f0'>Commit.</a> </li>
<li>Attempt to fix removing hidden view spaces. <a href='http://commits.kde.org/kate/a0c5008315fcc8564b61ca6acc3df83850ad12fe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367861'>#367861</a></li>
<li>Remove entities imgpath+rcfile and self-referencing link in katepart. <a href='http://commits.kde.org/kate/bebc8f1b868e97c7bbeaab826df85a8f33f78b05'>Commit.</a> </li>
<li>Fix wrong png names. <a href='http://commits.kde.org/kate/cb94856a76344cc654a6cc815e9c7e4704f530bc'>Commit.</a> </li>
<li>Fix minor issue. <a href='http://commits.kde.org/kate/2e94c9a10123d86aff36dd5da491856c905fa21c'>Commit.</a> </li>
<li>Sync find + replace bar from kate/menus.docbook to katepart/part.docbook. <a href='http://commits.kde.org/kate/b6e0089ac859df10da4a739d7836126c0e6acdb8'>Commit.</a> </li>
<li>Update kate handbook. <a href='http://commits.kde.org/kate/702f3af9b97029451974497c1d81a301c1d3d0de'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128776'>#128776</a></li>
<li>Tabs > "Open Containing Folder" now highlights the file in Dolphin. <a href='http://commits.kde.org/kate/5af3e012a20ad0db7ec8bb9e85335fe44bba2c30'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128729'>#128729</a></li>
<li>S&R: Only open the folder options when search-place is "Folder". <a href='http://commits.kde.org/kate/87d3562dea0003d492fe1c8e2e697676b1f77693'>Commit.</a> </li>
</ul>
<h3><a name='katomic' href='https://cgit.kde.org/katomic.git'>katomic</a> <a href='#katomic' onclick='toggle("ulkatomic", this)'>[Show]</a></h3>
<ul id='ulkatomic' style='display: none'>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/katomic/ca32bee2af646d6551fc5f502704b4548f06f4f3'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/katomic/9305968d1a0d5ba39b4d15c3c422a2aa4661baad'>Commit.</a> </li>
</ul>
<h3><a name='kblog' href='https://cgit.kde.org/kblog.git'>kblog</a> <a href='#kblog' onclick='toggle("ulkblog", this)'>[Show]</a></h3>
<ul id='ulkblog' style='display: none'>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kblog/039aefd9aec2e9c4ed4d4e22df8a7309f61459de'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kblog/44f9839b7fe49eecd44f934b564496f10a779267'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/kblog/7c718f9fdc9c0de7d49abdf3cece722c7b7b14ee'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/kblog/43213e6dd4ae2ca8288a8495b135bb26f7f56b57'>Commit.</a> </li>
<li>CLean up. <a href='http://commits.kde.org/kblog/ae3236b4fcb7d6ec2033746fe30a2cd92aa851cc'>Commit.</a> </li>
</ul>
<h3><a name='kbruch' href='https://cgit.kde.org/kbruch.git'>kbruch</a> <a href='#kbruch' onclick='toggle("ulkbruch", this)'>[Show]</a></h3>
<ul id='ulkbruch' style='display: none'>
<li>Q_FOREACH -> C++11 for. <a href='http://commits.kde.org/kbruch/0d4a267538169b1fadb8f7918cabe2ae879dede5'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kbruch/9625e73bf4e3e6f54975ff90aee56f6456751e71'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kbruch/2cb1972f6e8a29e143d5f1fbd7877c14881980ee'>Commit.</a> </li>
</ul>
<h3><a name='kcachegrind' href='https://cgit.kde.org/kcachegrind.git'>kcachegrind</a> <a href='#kcachegrind' onclick='toggle("ulkcachegrind", this)'>[Show]</a></h3>
<ul id='ulkcachegrind' style='display: none'>
<li>Compile ++. <a href='http://commits.kde.org/kcachegrind/3c9da24979d08659ceeb417ceb86e77db8002856'>Commit.</a> </li>
<li>QCachegrind View menu now same as in KCachegrind. <a href='http://commits.kde.org/kcachegrind/8e6ffb788cf2d87fbad02f11f31614483d44a6d0'>Commit.</a> </li>
<li>Fix crash on right click in flat profile without data. <a href='http://commits.kde.org/kcachegrind/446a8170b312e1673f475d11810f4af01c4cc608'>Commit.</a> </li>
<li>Enable icons for cycles. <a href='http://commits.kde.org/kcachegrind/e34e7a5b5b2044e960f644e2fab804b84ff25d4d'>Commit.</a> </li>
<li>Hide call counts if all counts from input are zero. <a href='http://commits.kde.org/kcachegrind/5fcda87e4cf8d4ae61fe5afb2bde605834d3eda0'>Commit.</a> </li>
<li>Call graph: be more robust against bogus input. <a href='http://commits.kde.org/kcachegrind/1eb4b19556bff1e83e42b81dae8f349683074a1a'>Commit.</a> </li>
<li>Fix for bug 368751. <a href='http://commits.kde.org/kcachegrind/4e5f940b57ae1089db29e0700d30cc309cf27805'>Commit.</a> </li>
</ul>
<h3><a name='kcalc' href='https://cgit.kde.org/kcalc.git'>kcalc</a> <a href='#kcalc' onclick='toggle("ulkcalc", this)'>[Show]</a></h3>
<ul id='ulkcalc' style='display: none'>
<li>Add appstream file. <a href='http://commits.kde.org/kcalc/5c5d76b11e62f1a470ba2ef9f6a33f71bd7cce41'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129320'>#129320</a></li>
<li>Update KCalc docbook to 16.08. <a href='http://commits.kde.org/kcalc/3a04e9ed27125a83d8c0c9e1eb0fec6b9614a38d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128521'>#128521</a></li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Show]</a></h3>
<ul id='ulkcalcore' style='display: none'>
<li>Provide compile error when compiling the ical test program fails. <a href='http://commits.kde.org/kcalcore/b2b0d944eab6937089e5156c7e1000618b9543ab'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/kcalcore/e62f2b5ca443136855dcf77505d491d0c4b6c6c7'>Commit.</a> </li>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kcalcore/93e75c4b7dcda11e0fb1ef9ecc7405d25ec97585'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kcalcore/f14e27099dfae7e962cdc904a2ee2a1d6e43de03'>Commit.</a> </li>
<li>Use Q_SIGNALS. <a href='http://commits.kde.org/kcalcore/687526d1e1cb519ce5236547feb771d8c5dbdf31'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/kcalcore/a1c405e451929d353a95fc6ebb5cbdfde99d92de'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/kcalcore/fd1ffecba22feee25df06ec9586efdb977a0b267'>Commit.</a> </li>
<li>Fix usage of qHash. <a href='http://commits.kde.org/kcalcore/f02be3572d2da2eeb60b1d6491500747e0c06178'>Commit.</a> </li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Show]</a></h3>
<ul id='ulkcalutils' style='display: none'>
<li>Remove unused variable. <a href='http://commits.kde.org/kcalutils/fc2822b95c4e630ba970e7031cbc00a534bfae88'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/kcalutils/4e8902d34dd0f4b34537f026447d6ef7553929de'>Commit.</a> </li>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kcalutils/d61a47cae56147df58dfe4cdeaf3008ffe8b7d58'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kcalutils/3cd042f8e6ca5a7b2f7a7352038f612f1dc15962'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/kcalutils/2b22f2865c7e542f3dce179c20ffb1df3edfd44b'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/kcalutils/aa3a8684b48912027d6c321a0c9e9ca038d7ebc9'>Commit.</a> </li>
</ul>
<h3><a name='kcharselect' href='https://cgit.kde.org/kcharselect.git'>kcharselect</a> <a href='#kcharselect' onclick='toggle("ulkcharselect", this)'>[Show]</a></h3>
<ul id='ulkcharselect' style='display: none'>
<li>Bump version for 16.12 releases. <a href='http://commits.kde.org/kcharselect/0ba43461bd46420a9d75252be8bda1a666cea1b9'>Commit.</a> </li>
<li>Remove kcharselect-generate-datafile.py. <a href='http://commits.kde.org/kcharselect/88a64d20c2cce50070fa11220eeed5def4b9378a'>Commit.</a> </li>
<li>Add bookmarks support. <a href='http://commits.kde.org/kcharselect/c25c21fa4415ff25db49b674ec3003bdcf4c565e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128522'>#128522</a>. Implements feature <a href='https://bugs.kde.org/242333'>#242333</a></li>
<li>Use new API to allow selecting characters from all planes. <a href='http://commits.kde.org/kcharselect/b4b8b5a383768fc8dea8914a98ab2b5a1b66e7bc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128453'>#128453</a>. See bug <a href='https://bugs.kde.org/142625'>#142625</a></li>
<li>Bump dependencies. <a href='http://commits.kde.org/kcharselect/84ef60aac8da782f7abf719bdb32164fc9a93094'>Commit.</a> </li>
<li>Update maintainers. <a href='http://commits.kde.org/kcharselect/db8aa158fe85baf91d3922bc74d90d7841d313ba'>Commit.</a> </li>
<li>Add symbol SMP blocks. <a href='http://commits.kde.org/kcharselect/5db1bcbd3c3ddf6d92ca6bbc9bc94bce7e4b5926'>Commit.</a> </li>
<li>Fix "See also" references. <a href='http://commits.kde.org/kcharselect/5415d07720286beb22962c30a01593c30a1cfce1'>Commit.</a> </li>
</ul>
<h3><a name='kcolorchooser' href='https://cgit.kde.org/kcolorchooser.git'>kcolorchooser</a> <a href='#kcolorchooser' onclick='toggle("ulkcolorchooser", this)'>[Show]</a></h3>
<ul id='ulkcolorchooser' style='display: none'>
<li>Call setApplicationDomain before KAboutData to make them translated. <a href='http://commits.kde.org/kcolorchooser/890aeb6d9137fbb367bf9128a001ad3957ffd856'>Commit.</a> </li>
<li>Fix crash if mimeData is null. <a href='http://commits.kde.org/kcolorchooser/a4c2986d2c8dbcac43cc7a8b49a3b03203372376'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371852'>#371852</a></li>
</ul>
<h3><a name='kcontacts' href='https://cgit.kde.org/kcontacts.git'>kcontacts</a> <a href='#kcontacts' onclick='toggle("ulkcontacts", this)'>[Show]</a></h3>
<ul id='ulkcontacts' style='display: none'>
<li>Start to look at parsing vcard2.1. <a href='http://commits.kde.org/kcontacts/14bb52d5e22c02d92b1c6a09050eacd2185c934a'>Commit.</a> </li>
<li>Add missing \n. <a href='http://commits.kde.org/kcontacts/72e2ed7189114f559a6b690c928cf4a893a0c802'>Commit.</a> </li>
<li>Look at to export as 2.1. <a href='http://commits.kde.org/kcontacts/c0dc9eb02b12583b449bdfabdf794048c8883506'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kcontacts/073770362c52932f4f8e417b150f082a73230905'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/kcontacts/a85e32da88da9611a72270a71a36e25b23b88f18'>Commit.</a> </li>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kcontacts/b2194dd9d806875122c76220f77660c9de09f14d'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kcontacts/8729e62572e4e6a0a5279e37a61db8c6c1c93840'>Commit.</a> </li>
<li>Rename variable. <a href='http://commits.kde.org/kcontacts/e18f1e06a46c6cd20da9f4706318dbde32bd966c'>Commit.</a> </li>
<li>Add autotest about member. <a href='http://commits.kde.org/kcontacts/987c3ba0fcdefb6974cfc6cdb9f138ea6a99d260'>Commit.</a> </li>
<li>Add since info. <a href='http://commits.kde.org/kcontacts/3e5abd978b973b80a79cb278c2b5c920e69d8c8d'>Commit.</a> </li>
<li>Fix since info. <a href='http://commits.kde.org/kcontacts/995ba8bf3edb61992bb34daf59ed531509260370'>Commit.</a> </li>
<li>Add more autotest (test without year). <a href='http://commits.kde.org/kcontacts/ff84acfe1637c8ac93feed20a8a54736d2411d83'>Commit.</a> </li>
<li>Test parsing birthday without year. <a href='http://commits.kde.org/kcontacts/47a93967380e6b9167952b0b12e29d5538da1f9b'>Commit.</a> </li>
<li>Add birthday autotest. <a href='http://commits.kde.org/kcontacts/798fae88b1764575eab50c36759f7e59348bc66f'>Commit.</a> </li>
<li>We need more autotests here too. <a href='http://commits.kde.org/kcontacts/a649e302584aa38ebc77bb2abf1e0e9ab9ad6399'>Commit.</a> </li>
<li>Remove debug. <a href='http://commits.kde.org/kcontacts/9e537d552dcbff830154606dbbfcab2932368d2f'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/kcontacts/73f3a62a85219767da38f955f4083d27fea71a96'>Commit.</a> </li>
<li>Add support for clientpidmap. <a href='http://commits.kde.org/kcontacts/bad818176d79b1379bd4d68364b81ccdb36a6ca0'>Commit.</a> </li>
<li>Store as qdate only. <a href='http://commits.kde.org/kcontacts/d6347ae7c27d3b895727a4124bed61dc241204d7'>Commit.</a> </li>
<li>Addressee: Determine if the birthday has been set with a time. <a href='http://commits.kde.org/kcontacts/bd9f439d46ec0ca21fdac7834c7f2d5a6e8e1560'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128743'>#128743</a></li>
<li>Use QT_REQUIRED_VERSION. <a href='http://commits.kde.org/kcontacts/9bd041a77656468228c56c13a92e1eb05d3cb0ea'>Commit.</a> </li>
<li>Use QString() directly. <a href='http://commits.kde.org/kcontacts/eb2e90f0a7188eb33f74c459c28e941f2e53081f'>Commit.</a> </li>
<li>Refactored VCardTool::parseDateTime and VCardTool::createDateTime. <a href='http://commits.kde.org/kcontacts/dc229047663cb9e7568159c4a4a9256db78747b4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128655'>#128655</a></li>
<li>Increase version. <a href='http://commits.kde.org/kcontacts/90552913525a1ddd290bc732ca0219196ff0bdeb'>Commit.</a> </li>
<li>Add maintainer. <a href='http://commits.kde.org/kcontacts/a714b40f8feab7ab70897e1955f38a5c48ff478a'>Commit.</a> </li>
<li>Enable docs generation. <a href='http://commits.kde.org/kcontacts/f84fcfac6ff2ff10085aaa9f8a290a7461694970'>Commit.</a> </li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Show]</a></h3>
<ul id='ulkde-dev-scripts' style='display: none'>
<li>Create_tarball: update svn URL. <a href='http://commits.kde.org/kde-dev-scripts/521a960e53b73c0d336551ab4bc6ed5af6960e2d'>Commit.</a> </li>
<li>Bump kio-gdrive version. <a href='http://commits.kde.org/kde-dev-scripts/54e0cdc7acc8588e956aa606e13c0b7dafb13990'>Commit.</a> </li>
<li>Allowing my contributions to be relicensed. <a href='http://commits.kde.org/kde-dev-scripts/2c1f5e125d41e4a821723760d3d4552559a05f7a'>Commit.</a> </li>
<li>Add myself to relicensecheck.pl. <a href='http://commits.kde.org/kde-dev-scripts/b3d8f87cb4b00e7e2efda398723dce76c81bf912'>Commit.</a> </li>
<li>Add myself to relicencecheck script. <a href='http://commits.kde.org/kde-dev-scripts/33959291b8b1e900d17965829de491558b169305'>Commit.</a> </li>
<li>Add myself to relicensecheck. <a href='http://commits.kde.org/kde-dev-scripts/3ced0ab03a72f5752778c1b0a85c76a565a60da4'>Commit.</a> </li>
<li>Add script to generate sieve rules for frameworks commits. <a href='http://commits.kde.org/kde-dev-scripts/3423d6f73ff0ee74157d1e9d5d2850a738b81bff'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129242'>#129242</a></li>
<li>Reviewboard-am: add Python 3 compatibility. <a href='http://commits.kde.org/kde-dev-scripts/a19ca72213586a05d6aff67c6562b3c60c5b012f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129155'>#129155</a></li>
<li>Reviewboard-am: add marker to ignore following text. <a href='http://commits.kde.org/kde-dev-scripts/6221fbec096fc31a16a8f644714342e7c52d3216'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129154'>#129154</a></li>
<li>Create_tarball: add sign option. <a href='http://commits.kde.org/kde-dev-scripts/deac48717dfdf474f7373bf7d6539d7ec1e7e6bd'>Commit.</a> </li>
<li>Add kio-gdrive. <a href='http://commits.kde.org/kde-dev-scripts/906aac3ea573ced830aed79594d2b85bb5b8e0a3'>Commit.</a> </li>
<li>Bump kronometer version. <a href='http://commits.kde.org/kde-dev-scripts/e7a1d91f3928d05258e849e0d523595f96c907e8'>Commit.</a> </li>
<li>Update Krusader data. <a href='http://commits.kde.org/kde-dev-scripts/3e3e6fd08b2f9801e075f15239bc3c3790453ef6'>Commit.</a> </li>
<li>Include useful tutorial. <a href='http://commits.kde.org/kde-dev-scripts/f820b7cf3737fba8950f9ba3b2f4b7a1e6874e27'>Commit.</a> </li>
<li>Include a file to help collect other useful emacs packages. <a href='http://commits.kde.org/kde-dev-scripts/aa82c52cf2bf07119bfdc53d814bffd1d78e2b03'>Commit.</a> </li>
<li>Simplify code using (beginning-of-defun) from c-mode. <a href='http://commits.kde.org/kde-dev-scripts/76d1b0af89f2265d774b3b73aee4aac7e584f971'>Commit.</a> </li>
<li>Provide a switch-to-function-def for GNU Emacs using dirty hacks. <a href='http://commits.kde.org/kde-dev-scripts/e1e5481eb518bb6307303ba2c55cbd80893fabde'>Commit.</a> </li>
<li>For the do keyword -- complete braces and also insert a while. <a href='http://commits.kde.org/kde-dev-scripts/acfedcf9a9a757527ba65d860790b02ffb1c8341'>Commit.</a> </li>
<li>Insert a close-brace when an open brace is inserted after 'else'. <a href='http://commits.kde.org/kde-dev-scripts/165859782144e453159c06909d5ddf8491ec79ff'>Commit.</a> </li>
<li>When a brace is inserted for a C++ lambda, insert ; after close-brace. <a href='http://commits.kde.org/kde-dev-scripts/e5247721d73dc1c43bbccafa1a33bb42aaa334c7'>Commit.</a> </li>
<li>Update to krita 3.0.1 beta 1. <a href='http://commits.kde.org/kde-dev-scripts/8fec1e36554992027468a4e2aaba158b578948f7'>Commit.</a> </li>
<li>Add Krita. <a href='http://commits.kde.org/kde-dev-scripts/9277a0e2a7b6252ee306e5db888866965da58724'>Commit.</a> </li>
<li>Don't insert \b into CMakeLists.txt. <a href='http://commits.kde.org/kde-dev-scripts/3e20aa7dffeb9d8d5b52a747f56e122e518a5c39'>Commit.</a> </li>
<li>Add info for kirigami in the ini. <a href='http://commits.kde.org/kde-dev-scripts/bbb0af7bba6170da243169659a74726d3cd2852f'>Commit.</a> </li>
<li>Fix syntax in previous commit. <a href='http://commits.kde.org/kde-dev-scripts/692af4f35420b3373b1f1f4452eb4664cfcc8e83'>Commit.</a> </li>
<li>Fix parsing of options.add("yes-label <text>"   (the '-' broke it). <a href='http://commits.kde.org/kde-dev-scripts/91bf543ed25a4ec433417cfbb08b4257e8b4fab6'>Commit.</a> </li>
<li>Don't match commented lines when looking for kdelibs4support stuff. <a href='http://commits.kde.org/kde-dev-scripts/722b12490d0c14eab6629042a4b117f967e80dda'>Commit.</a> </li>
<li>Use word boundaries to not match deprecated class names as part of variable names (e. g. m_bookmarkIcon). <a href='http://commits.kde.org/kde-dev-scripts/08722193e8340b35a33829f84b2151b8e1eb05c7'>Commit.</a> </li>
</ul>
<h3><a name='kde-runtime' href='https://cgit.kde.org/kde-runtime.git'>kde-runtime</a> <a href='#kde-runtime' onclick='toggle("ulkde-runtime", this)'>[Show]</a></h3>
<ul id='ulkde-runtime' style='display: none'>
<li>Compiling against gmgpe 1.7 requires c++11. <a href='http://commits.kde.org/kde-runtime/cf28801cd34730da07a2c01704ca3114630f4fe7'>Commit.</a> </li>
<li>Make building drkonqi optional. <a href='http://commits.kde.org/kde-runtime/a87fd4a4e4b768359bc71485f23f9f27a103de53'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129340'>#129340</a></li>
<li>Allow building kwalletd against gpgme++ from gpgme 1.7. <a href='http://commits.kde.org/kde-runtime/1b80d1d0b961f8e28186928ede2b87af292c3de4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129339'>#129339</a></li>
</ul>
<h3><a name='kdebugsettings' href='https://cgit.kde.org/kdebugsettings.git'>kdebugsettings</a> <a href='#kdebugsettings' onclick='toggle("ulkdebugsettings", this)'>[Show]</a></h3>
<ul id='ulkdebugsettings' style='display: none'>
<li>Remove it. <a href='http://commits.kde.org/kdebugsettings/4fd57808624edb132bc6a825a1d6a9952a4a44a3'>Commit.</a> </li>
<li>Add more rename categories. <a href='http://commits.kde.org/kdebugsettings/5de72571e1fe9c6f3ab99f0f0cc112fefec89de4'>Commit.</a> </li>
<li>Now kdebugsettings uses renamecategories. <a href='http://commits.kde.org/kdebugsettings/f50220d11fc4a52227e7cba8c69051642d0ce88e'>Commit.</a> </li>
<li>Add new files. <a href='http://commits.kde.org/kdebugsettings/6113a9213877d6d0de9fe9a94b907e7be509748f'>Commit.</a> </li>
<li>Allow to use renamed categories. <a href='http://commits.kde.org/kdebugsettings/bb1536706b96d9853e67961842c1d12b44523f38'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kdebugsettings/8be2e4d05cd5fd56d5edd5f340e268d0d31cf5b6'>Commit.</a> </li>
<li>Remove duplicate check. <a href='http://commits.kde.org/kdebugsettings/021fb855d978dd2f8459acaef2665bcda6cc5a4f'>Commit.</a> </li>
<li>Add renamecategories. <a href='http://commits.kde.org/kdebugsettings/2eee4f8f385901d47fccd0b1287f8af9964a8038'>Commit.</a> </li>
<li>Clear list to avoid to duplicate entries. <a href='http://commits.kde.org/kdebugsettings/b96ba95992c3cbf68b8fbc70e24deae686082ef7'>Commit.</a> </li>
<li>Load all rename files. <a href='http://commits.kde.org/kdebugsettings/2defba730357a5182cf3c6492f9f5c957570de0d'>Commit.</a> </li>
<li>Add TODO:. <a href='http://commits.kde.org/kdebugsettings/75741de5e6acf5d31f0dc8a2c507702dd434e7a2'>Commit.</a> </li>
<li>Load renameCategories. <a href='http://commits.kde.org/kdebugsettings/a82412ad36ea3d8de1117bb7d57a58dc064599d0'>Commit.</a> </li>
<li>Rename variable. <a href='http://commits.kde.org/kdebugsettings/9f78f5a03b3652c571d2f4c714c118ae7f779402'>Commit.</a> </li>
<li>Add rename categories list. <a href='http://commits.kde.org/kdebugsettings/3ede8f9a3122a5727687ee9627e396bcedab4bba'>Commit.</a> </li>
<li>Fix i18n. <a href='http://commits.kde.org/kdebugsettings/41c5964b2d4ef60ac9ff1293d8c951b86ff5bd54'>Commit.</a> </li>
<li>Rename method. <a href='http://commits.kde.org/kdebugsettings/d0b8795476a1a68789f5642a02411c594234bdf0'>Commit.</a> </li>
<li>Add method to load renamecategories files. <a href='http://commits.kde.org/kdebugsettings/b813820faf2fecbb8df703a23b7e4393fddccbf9'>Commit.</a> </li>
<li>Add autotest. <a href='http://commits.kde.org/kdebugsettings/66f4e32c6537a876f912e0ba255188f41ddea2fd'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/kdebugsettings/2aaa8cc2b34d17cb31a5ecf7ea4199ad91eede0e'>Commit.</a> </li>
<li>Fix parsing. <a href='http://commits.kde.org/kdebugsettings/6d203c3a07534bd0b8a167168ec51c6c3fee8f3c'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/kdebugsettings/382c28bef6eca4bb097e88026ce766582cb4a1a0'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kdebugsettings/289f25d8a79b2494099884e883fbab98982cf00b'>Commit.</a> </li>
<li>Return category. <a href='http://commits.kde.org/kdebugsettings/9a28ffc45b9727d0e8fbcf28c77ec3ab62bb2a19'>Commit.</a> </li>
<li>Start to implement rename categories. <a href='http://commits.kde.org/kdebugsettings/e7230b12f697f4f638880688f9f0d99dff31d6a9'>Commit.</a> </li>
</ul>
<h3><a name='kdegraphics-mobipocket' href='https://cgit.kde.org/kdegraphics-mobipocket.git'>kdegraphics-mobipocket</a> <a href='#kdegraphics-mobipocket' onclick='toggle("ulkdegraphics-mobipocket", this)'>[Show]</a></h3>
<ul id='ulkdegraphics-mobipocket' style='display: none'>
<li>Fix warning about uint vs int comparison. <a href='http://commits.kde.org/kdegraphics-mobipocket/efa85d7ff306b818c924816bca05674a2d145eff'>Commit.</a> </li>
<li>Remove Strigi references. <a href='http://commits.kde.org/kdegraphics-mobipocket/daa7341c2ba486a212232ad5a81996f158d9be93'>Commit.</a> </li>
<li>Dynamically generate the config file for CMake. <a href='http://commits.kde.org/kdegraphics-mobipocket/5cf97d4dd4c0a7d923da350d9babc36860c5e062'>Commit.</a> </li>
<li>Bump the so/version and allow co-installability. <a href='http://commits.kde.org/kdegraphics-mobipocket/9cf1978b47cf593618fb64c60c8b8e044ee45a35'>Commit.</a> </li>
<li>Revert "Use QStringLiteral in QRegExp constructors.". <a href='http://commits.kde.org/kdegraphics-mobipocket/6eb0209c7f4cc642923b3418645cf5b1aa0ea07f'>Commit.</a> </li>
<li>Re-enable Strigi analyzer. <a href='http://commits.kde.org/kdegraphics-mobipocket/69c91babe0c0f91a748529df3a66088e6f11cef7'>Commit.</a> </li>
<li>Use QStringLiteral in QRegExp constructors. <a href='http://commits.kde.org/kdegraphics-mobipocket/689b53d02a07837b4b84e94983e3e5b8b25652c7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127628'>#127628</a></li>
<li>Fix install of export header. <a href='http://commits.kde.org/kdegraphics-mobipocket/c2e1db60333e860aabab9c7ad754dfad07820aa2'>Commit.</a> </li>
<li>Remove kdelibs4support. <a href='http://commits.kde.org/kdegraphics-mobipocket/70ccd4c109e4d2b65674bdd0a2f8fe44b759fc1b'>Commit.</a> </li>
<li>Switch from KDE_EXPORT to Q_DECL_EXPORT. <a href='http://commits.kde.org/kdegraphics-mobipocket/5cd76ad35e0f2e2b037e02cd461681ba83f4cdfd'>Commit.</a> </li>
<li>Remove unused kdebug include. <a href='http://commits.kde.org/kdegraphics-mobipocket/f8fbaf34ee8407772594de24db26dab3afd73c81'>Commit.</a> </li>
<li>Let cmake generate export header. <a href='http://commits.kde.org/kdegraphics-mobipocket/da07949b7f65f05e89a924b0a6768b4b1b914b57'>Commit.</a> </li>
<li>Initial port of cmake files. <a href='http://commits.kde.org/kdegraphics-mobipocket/9cbec5550954f67d86dcf77fe25c6f318798928e'>Commit.</a> </li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Java: set names in permission dialog. <a href='http://commits.kde.org/kdelibs/bf0bd72dd2333c29c9cf5dea8cfa031b03676796'>Commit.</a> </li>
</ul>
<h3><a name='kdenetwork-filesharing' href='https://cgit.kde.org/kdenetwork-filesharing.git'>kdenetwork-filesharing</a> <a href='#kdenetwork-filesharing' onclick='toggle("ulkdenetwork-filesharing", this)'>[Show]</a></h3>
<ul id='ulkdenetwork-filesharing' style='display: none'>
<li>Mark variable as unused. <a href='http://commits.kde.org/kdenetwork-filesharing/dac4f312f146fbc2eddc7b656dc3b03ef4774df1'>Commit.</a> </li>
<li>Filter for current Arch so on multiarch systems it doesn't try to install several packages and get confused. <a href='http://commits.kde.org/kdenetwork-filesharing/c43b1761804e85ed723d167486eb1a609dcd95ea'>Commit.</a> </li>
<li>Definately hide unused widgets. <a href='http://commits.kde.org/kdenetwork-filesharing/7547ff98ce575ba67713edbeaa1d77602eee092f'>Commit.</a> </li>
<li>Detect if the install has failed and show message if so. <a href='http://commits.kde.org/kdenetwork-filesharing/f263eae81511855f4a108cda27bacb6f65d2622f'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Remove duplicate margin + extra widget. <a href='http://commits.kde.org/kdenlive/f0a92d7fb388bf1f3e8a850df9782da955c64942'>Commit.</a> </li>
<li>Fix signal/slot. <a href='http://commits.kde.org/kdenlive/5b85e93f400f9195c120de117f4bdd61d23c4b12'>Commit.</a> </li>
<li>Fix cancel/getpreview sound. <a href='http://commits.kde.org/kdenlive/3b6209b94045db76e3f18e29237b86c6a054401c'>Commit.</a> </li>
<li>* Fix crash on resize clip after removing keyframe effect. <a href='http://commits.kde.org/kdenlive/406b7674ae24a0a2cba1e2aff6feeebaaa04d6c8'>Commit.</a> </li>
<li>Fix split audio only working on first audio track on non automatic split mode. <a href='http://commits.kde.org/kdenlive/f922b5023f2f2ffdf0468d008e39932079ebc96b'>Commit.</a> </li>
<li>Add option in monitor options menu to display zoom toolbar. <a href='http://commits.kde.org/kdenlive/d39d1860ee3cde75c0b60ef22c45bb3acc94a4a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371871'>#371871</a></li>
<li>Fix broken generators (mimetype not added on non KDE Desktop). <a href='http://commits.kde.org/kdenlive/f8696c813c9952274c0f5c416688abab5c4a1193'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371798'>#371798</a></li>
<li>Use updated KDE Store category to download lumas. <a href='http://commits.kde.org/kdenlive/0ceed2ebad260c0179243021eb088558107da6e8'>Commit.</a> </li>
<li>Fix the label of the progress dialog when loading a new project. <a href='http://commits.kde.org/kdenlive/878737ae616aa1cb7951873338f6f6fb19dcb2c4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369211'>#369211</a></li>
<li>Remove gcc warnings. <a href='http://commits.kde.org/kdenlive/d945f8737ad033cbd1047176f7bc631b2c8b262b'>Commit.</a> </li>
<li>Fix clip loading on windows. <a href='http://commits.kde.org/kdenlive/388717238b03ca663187ec0e05209dcd0dfe5dfb'>Commit.</a> </li>
<li>Fix startup on windows. <a href='http://commits.kde.org/kdenlive/ae665d1607b54696bd174ab083360642af733108'>Commit.</a> </li>
<li>Downloadable Title templates and render profiles are back online, thanks to the KDE Store. <a href='http://commits.kde.org/kdenlive/26a7fafec6da88eaa8b87b05c21ace04d0e80659'>Commit.</a> </li>
<li>Add const'. <a href='http://commits.kde.org/kdenlive/07702138346a0f3ad65091e91d0573d1d738a3a1'>Commit.</a> </li>
<li>Don't execute code if we cancel savefile dialogbox. <a href='http://commits.kde.org/kdenlive/989dacdf466e494951057868477d9b1a605fdda9'>Commit.</a> </li>
<li>Fix crash when searching for missing clips, correctly look for missing playlists and clear effectstack when transition is unselected. <a href='http://commits.kde.org/kdenlive/d7e2372daa82f69b06d17546800a2e7f3a26a24c'>Commit.</a> </li>
<li>Ctrl+Mouse Wheel now zooms on mouse position. <a href='http://commits.kde.org/kdenlive/b753b3cfb55ce2eb3ad27a1c546d9b70fcd17265'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369198'>#369198</a></li>
<li>Make sure the document has a valid id on opening. <a href='http://commits.kde.org/kdenlive/12d1eb7ca5bd24ad88115c5cb6321f6a86cbe3d2'>Commit.</a> </li>
<li>Disable stem audio export if export audio is disabled. <a href='http://commits.kde.org/kdenlive/42701f3986ee99f4be470ddd4ab5d2fc8e731fc2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/102591'>#102591</a></li>
<li>Fix black frame at end when rendering full project. <a href='http://commits.kde.org/kdenlive/9869d0b0cd75958cf2de3b53a57d578887dd7ba2'>Commit.</a> See bug <a href='https://bugs.kde.org/373072'>#373072</a></li>
<li>Fix bug in ungroup (locked clip ). <a href='http://commits.kde.org/kdenlive/d212d92068e8e40ff26014043d1b7f73104a92fd'>Commit.</a> </li>
<li>Fix crash when closing a document that was still creating thumbs. <a href='http://commits.kde.org/kdenlive/336e3d2baf10d845cc7d14bccb3164ac1f8b6957'>Commit.</a> </li>
<li>Remove unused QLocale declarations. <a href='http://commits.kde.org/kdenlive/4e115e20d62303d539a2c5a3356ec392f4a31cf7'>Commit.</a> </li>
<li>Move 'Tracks' menus from Project to Timeline. <a href='http://commits.kde.org/kdenlive/0006b9c8e9bdf21e8b7252d2833f33849f67ca4a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359216'>#359216</a></li>
<li>Fix dissolve in slideshow clips broken. <a href='http://commits.kde.org/kdenlive/55d2e159ab53b32a280032dee65510027556ff70'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370337'>#370337</a></li>
<li>Don't check for missing resources in playlist if producer does not use a real file (like counter, noise, ...). <a href='http://commits.kde.org/kdenlive/67eb1aeaa5c3b50d4ebc41fc2775ccff168b3d6c'>Commit.</a> </li>
<li>Fix monitor scene not adapting to zoom (rotoscoping, composite, ...). <a href='http://commits.kde.org/kdenlive/baabfafe6fa546ce968a4eb523d1e1f3b2db2f81'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373113'>#373113</a></li>
<li>Fix button position. <a href='http://commits.kde.org/kdenlive/3254a73e8951f8a05ea42d5dca4956f2313b6210'>Commit.</a> </li>
<li>Fix many issues with volume keyframes. <a href='http://commits.kde.org/kdenlive/b348355e43285c37456bfc2625a055811b790b8f'>Commit.</a> </li>
<li>Reloading a playlist clip in a project now checks for missing files in it and allows fixing. <a href='http://commits.kde.org/kdenlive/2087c0236360e0bf3387f0117c7a9c273ba199e8'>Commit.</a> </li>
<li>Fix error in previous commit. <a href='http://commits.kde.org/kdenlive/ade0ce14193e4a2e62b9d683630a0deaa1d0876a'>Commit.</a> </li>
<li>When adding a playlist clip in a project, check for missing files inside it. <a href='http://commits.kde.org/kdenlive/7d0240c8a82ba4cdbbbf4fd07b7746c645cb35f9'>Commit.</a> </li>
<li>Fix several issues with effect keyframes behaving incorrectly. <a href='http://commits.kde.org/kdenlive/b2235e3ff5fea9c987752cdbd3899e87abedf4d0'>Commit.</a> </li>
<li>Fix timeline corruption when moving a clip by very small offset. <a href='http://commits.kde.org/kdenlive/13535766c1b1317f06bcc013c2f81472e691a081'>Commit.</a> </li>
<li>Fix error, we don't need to create new local variable. <a href='http://commits.kde.org/kdenlive/a05567066275632f40bcf302d67ccb770e4bb226'>Commit.</a> </li>
<li>Remove duplicate margin. <a href='http://commits.kde.org/kdenlive/84f218ede71cfdf4b4c6668289496efe0e483c38'>Commit.</a> </li>
<li>Cleanup ogg profile. <a href='http://commits.kde.org/kdenlive/7e8d9b81f87992445a46f651497182b7ac5d6444'>Commit.</a> </li>
<li>Several First run wizard fixes:. <a href='http://commits.kde.org/kdenlive/2719cc32779afbef53d60805f1083c9464c1b7cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372867'>#372867</a></li>
<li>Remove duplicate margin. <a href='http://commits.kde.org/kdenlive/cb9c8eef091f1552e7279696629ed5782dca9a0f'>Commit.</a> </li>
<li>Fix render sometimes incorrectly using proxies. <a href='http://commits.kde.org/kdenlive/69ea21c07165082d4cfd54747c6e55a33b214fdc'>Commit.</a> See bug <a href='https://bugs.kde.org/371064'>#371064</a></li>
<li>Fix razor icon disappearing after first cut. <a href='http://commits.kde.org/kdenlive/7075020d3bb6473bc1029264d1718f37dd90afcd'>Commit.</a> </li>
<li>Display warning when rendering using proxy clipy. <a href='http://commits.kde.org/kdenlive/8be12207b3024594b2651dd4f0ae485be2edd8a6'>Commit.</a> See bug <a href='https://bugs.kde.org/372660'>#372660</a></li>
<li>Fix moving tmp data when changing project tmp folder, fix location of global tmp data in widget when using custom location in current project. <a href='http://commits.kde.org/kdenlive/b992f5962a6e09a4dd0ef6cee90db0b83a0a1f08'>Commit.</a> </li>
<li>Fix random keyframe type when adding composite & transform transition. <a href='http://commits.kde.org/kdenlive/572b994fd3d1fef5822822e0bb1c077d5cfa2878'>Commit.</a> </li>
<li>Fix error preventing setting an interlaced default project profile. <a href='http://commits.kde.org/kdenlive/7481b6eae9df3872ad0cf3e4e72e0282936f7f61'>Commit.</a> See bug <a href='https://bugs.kde.org/372588'>#372588</a></li>
<li>Do not cache supported codecs / formats. <a href='http://commits.kde.org/kdenlive/64027bb069ceab0bfc0d80f8cdf52e5b24783e0c'>Commit.</a> </li>
<li>Fix missing codec detection in render widget. <a href='http://commits.kde.org/kdenlive/f49178380d1eee8e807b55deb6eb58f9983fd468'>Commit.</a> </li>
<li>Fix undo grouping broken. <a href='http://commits.kde.org/kdenlive/d25e1db8b7c7db083482bd2fb545ac7cfe571b2b'>Commit.</a> See bug <a href='https://bugs.kde.org/370653'>#370653</a></li>
<li>Fix ungrouped clips not saved. <a href='http://commits.kde.org/kdenlive/e4f09f85f9ae12d07ac30edbbc9f3d874c8ec9f5'>Commit.</a> See bug <a href='https://bugs.kde.org/372020'>#372020</a></li>
<li>Use breeze dark color theme on first start if available and default theme is breeze. <a href='http://commits.kde.org/kdenlive/00ff22d632e2d047af12ec570aba8bb91c31fb1a'>Commit.</a> See bug <a href='https://bugs.kde.org/346608'>#346608</a></li>
<li>* Fix moving keyframe moves parent clip. <a href='http://commits.kde.org/kdenlive/09d14df42c0a1ba76058c1bc82285a7331a12023'>Commit.</a> </li>
<li>Fix default path for titles. <a href='http://commits.kde.org/kdenlive/e97171d8d399574824a50fafd321b52b8a3d59f8'>Commit.</a> </li>
<li>Fix rendering crash on finish. <a href='http://commits.kde.org/kdenlive/f3002dd8d31de218d3f8797cfd8b93647760a9cb'>Commit.</a> See bug <a href='https://bugs.kde.org/371478'>#371478</a></li>
<li>Fix error message (cannot create directory) when opening archived project. <a href='http://commits.kde.org/kdenlive/d6a8dc1fa449cd6f6c5701e889fa024dd380a176'>Commit.</a> </li>
<li>Fix incorrect Url handling in archive feature. <a href='http://commits.kde.org/kdenlive/a8e87f2975913a627266332e424da87c6896834a'>Commit.</a> See bug <a href='https://bugs.kde.org/367705'>#367705</a></li>
<li>Fix screenshot link in appstream metadata. <a href='http://commits.kde.org/kdenlive/029c30e2e8270553314210edf897ef951ecc4dcb'>Commit.</a> </li>
<li>Fix path corruption on clip reload. <a href='http://commits.kde.org/kdenlive/41565402d462e0853241e822f51d056f12f6634a'>Commit.</a> See bug <a href='https://bugs.kde.org/371965'>#371965</a></li>
<li>Fix unwanted vertical scrolling. <a href='http://commits.kde.org/kdenlive/a6d10adf0c8df453cfbb765da4437132986a1b9d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371982'>#371982</a></li>
<li>Fix various regressions introduced with project folder change. <a href='http://commits.kde.org/kdenlive/1d1595e7d14ef6b0560dc93940f9c23ed96a4ce7'>Commit.</a> See bug <a href='https://bugs.kde.org/371965'>#371965</a></li>
<li>Fix blank screen on movit pause. <a href='http://commits.kde.org/kdenlive/4a3f660b885f5ba9eb606859ece709189e2189b4'>Commit.</a> See bug <a href='https://bugs.kde.org/371926'>#371926</a></li>
<li>Use relative path in .mlt files created by clip jobs. <a href='http://commits.kde.org/kdenlive/d56831ea499f82c4e604d2b39aa855781c66b9ee'>Commit.</a> </li>
<li>Cleanup. <a href='http://commits.kde.org/kdenlive/051741183d2e6c54fa50693ab0eab9deff8af034'>Commit.</a> </li>
<li>* timeline toolbar: add context menu to set icon size. <a href='http://commits.kde.org/kdenlive/c5f07dfc6a9f1268b1c0b3e00dd75742a5bbd12f'>Commit.</a> </li>
<li>Allow setting custom config file with --config option. <a href='http://commits.kde.org/kdenlive/fd09b024794d5670befb63c9a8723b19edb5ebc2'>Commit.</a> </li>
<li>* First steps towards using custom project folder to store tmp data (wip). <a href='http://commits.kde.org/kdenlive/8996c30e56b5ff6126adf828fdd7681320a5f7af'>Commit.</a> </li>
<li>Add ogg render profile, disable quality when required, patch by alcinos. <a href='http://commits.kde.org/kdenlive/aa9251299969aff2ffaaeae0edb81def5239a680'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129241'>#129241</a></li>
<li>Revert recent commit causing startup crashes. <a href='http://commits.kde.org/kdenlive/880344c32093885b1f98f418bedf00bcf0bc7070'>Commit.</a> See bug <a href='https://bugs.kde.org/371252'>#371252</a></li>
<li>Improve some effect names, capitalize first letter, patch by alcinos. <a href='http://commits.kde.org/kdenlive/9fcbba8bf03c918efce49ae3160ce23f8a085a18'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129218'>#129218</a></li>
<li>Fix CPU usage when idle. <a href='http://commits.kde.org/kdenlive/68e89f48be0ec2fe1767a6ea050fa87fb9b441c5'>Commit.</a> </li>
<li>Add proper UI for lut3d effect (avfilter), patch by alcinos. <a href='http://commits.kde.org/kdenlive/55f3848ceba2d80a43694bb2ca7522e315d2fb01'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129145'>#129145</a></li>
<li>Attempt to fix QOffscreenSurface thread crash. <a href='http://commits.kde.org/kdenlive/c71731d4919def0ebbffe31cf79d965eaa8cf320'>Commit.</a> See bug <a href='https://bugs.kde.org/357674'>#357674</a></li>
<li>Library widget: accept drops from Project Bin and Clip monitor. <a href='http://commits.kde.org/kdenlive/e20ef96051d98d0caf615c4dae0c40875b9c521a'>Commit.</a> </li>
<li>Add tripod parameter to vidstab. <a href='http://commits.kde.org/kdenlive/dece85bd3d6d7b4d612dd86a1b3db353fb62ccb9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370360'>#370360</a></li>
<li>Fix corrution when moving several clips (introduced in recent refactoring). <a href='http://commits.kde.org/kdenlive/29cd72966fd5f916f67e19eea07ba853f49cc146'>Commit.</a> </li>
<li>Remember track effect state when disabling timeline effects. <a href='http://commits.kde.org/kdenlive/670d54393d1d504032848553b82f4122353f3c9f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368245'>#368245</a></li>
<li>Fix scalable application icon. <a href='http://commits.kde.org/kdenlive/1ce6a9ae08b6081f0a1f2f42f8fabc44acec1f89'>Commit.</a> </li>
<li>Refactoring: continue moving tool functions out of customtrackview. <a href='http://commits.kde.org/kdenlive/867b056161b2da707b6899734cb8c18360eb3d74'>Commit.</a> </li>
<li>Updates for rolling trim: fix split view sometimes not working. <a href='http://commits.kde.org/kdenlive/5b7908a70b2fe1981aba1c11f093c592795e4297'>Commit.</a> </li>
<li>Some trim progress. Ctrl+T enters trim mode and cycles through the different modes (ripple, roll, ...). <a href='http://commits.kde.org/kdenlive/a805630da8699beb17551b79e45910dc4a5ac64a'>Commit.</a> </li>
<li>Fix auto transition icon coloring. <a href='http://commits.kde.org/kdenlive/6b3cb898b5f989938aff7b5afe8617b7c748855a'>Commit.</a> </li>
<li>Ensure we always have a keyframe at the end of a transition when resizing it to avoid artifacts. <a href='http://commits.kde.org/kdenlive/dcf45a03726a674425f65b98015a4712fa109f9f'>Commit.</a> See bug <a href='https://bugs.kde.org/369479'>#369479</a></li>
<li>Rename "Unset/Clear preview zone to Remove...". <a href='http://commits.kde.org/kdenlive/58270892e65c6044eadd2c20fe0cde9a190002f7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129056'>#129056</a>. Fixes bug <a href='https://bugs.kde.org/367448'>#367448</a></li>
<li>Fix duplicate producers created on library import, patch by Harald Albrecht. <a href='http://commits.kde.org/kdenlive/380e508f3e5712b75aebc22ae8dceb92efcf3a9f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129034'>#129034</a></li>
<li>Library: improve playlist expansion, patch by Harald Albrecht. <a href='http://commits.kde.org/kdenlive/e6561652db31de58ea4570da0edb4af3e7a02379'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129023'>#129023</a></li>
<li>Reintroduce the much requested rotoscoping effect, now ported to qml. <a href='http://commits.kde.org/kdenlive/71e11d7aa87b784a8e47c13b5211dcdcbc90930a'>Commit.</a> </li>
<li>Basic work to prepare rotoscoping porting. <a href='http://commits.kde.org/kdenlive/891fe44167e2f431d54dcfc133ff6d5775e3e679'>Commit.</a> </li>
<li>Allow proxy clips for slideshows. <a href='http://commits.kde.org/kdenlive/ab3efa5d5c1e4c3f30940c1e9a220e9599b1c010'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369243'>#369243</a></li>
<li>Make raising effect/transition properties panel configurable, patch by Harald Albrecht. <a href='http://commits.kde.org/kdenlive/435856a29f81881e4814b5cc26d5923de57f74d0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129000'>#129000</a></li>
<li>Rolling edit can now be triggered by selecting a timeline clip. <a href='http://commits.kde.org/kdenlive/8b78b8b9518ac98016e3844269daa3a00e785eb5'>Commit.</a> </li>
<li>Add new timeline action: Remove space from all tracks that works if there are grouped clips. <a href='http://commits.kde.org/kdenlive/d9d5d95a4565fbfbb890be20696fb424140943de'>Commit.</a> See bug <a href='https://bugs.kde.org/369123'>#369123</a></li>
<li>Various fixes for motion tracker. <a href='http://commits.kde.org/kdenlive/a0ee357c06ee529cecb9c1b1e03ba6d53a6fe5c6'>Commit.</a> </li>
<li>Use original clip, not proxy when extracting clip frame, patch by Harald Albrecht. <a href='http://commits.kde.org/kdenlive/16d894c61d4d6875b7bed4297a30a410c33bcb28'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128937'>#128937</a></li>
<li>New: Extract frame to project, patch by Harald Albrecht. <a href='http://commits.kde.org/kdenlive/e31ec9285daedbfd7cbdb0c2616b3e64c006a5fc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128929'>#128929</a></li>
<li>Fix missing css style attribute in auto transition icons. <a href='http://commits.kde.org/kdenlive/2a3a874882680e038fe95286540c8b6fff33970d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128928'>#128928</a></li>
<li>Automatically proxy playlist if enabled. <a href='http://commits.kde.org/kdenlive/f14fdfdd4ea7285eda64e3027936389efa3a9392'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368802'>#368802</a></li>
<li>Fix proxied playlist clips incorrectly detected as AV clips on document opening. <a href='http://commits.kde.org/kdenlive/7f17407ae5b728a28dd376a3e2b823f7e56cfc9c'>Commit.</a> See bug <a href='https://bugs.kde.org/368802'>#368802</a></li>
<li>Improve opacity widget and add size control in animated keyframe widget. <a href='http://commits.kde.org/kdenlive/246e684d6bd956e362a7d98d9f7e61405f6ce7e8'>Commit.</a> </li>
<li>Add deselect option, patch by Harald Albrecht. <a href='http://commits.kde.org/kdenlive/322a21e5057664a10046965eea1057c7139c27cd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128923'>#128923</a></li>
<li>Support file size display in clip properties display, patch by Harald Albrecht. <a href='http://commits.kde.org/kdenlive/da3539c449be1c261f290c31f3f91d99277e90e7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128921'>#128921</a></li>
<li>Fix deletion of source clip when using new speed clip job, add safeguard. <a href='http://commits.kde.org/kdenlive/6883a671792763da469dd08531378031b05f13da'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368836'>#368836</a></li>
<li>Add missing icons. <a href='http://commits.kde.org/kdenlive/aab0a31e67d9637a686747eebb0e4774bc0a2b16'>Commit.</a> </li>
<li>Allow defining automatic property of transitions, patch by Harald Albrecht. <a href='http://commits.kde.org/kdenlive/a4191ce71025e3c5e6dda12961f3404100ecd92d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128879'>#128879</a></li>
<li>Update "Reverse Clip" Bin job to handle any speed. <a href='http://commits.kde.org/kdenlive/cee1588f74b51c30b53f5a60217484751797c77f'>Commit.</a> See bug <a href='https://bugs.kde.org/368681'>#368681</a></li>
<li>Fix Recent regression - groups lost on project opening. <a href='http://commits.kde.org/kdenlive/6a6ad3742f3e28d82162f83ffdc1be94fda63f1e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368342'>#368342</a></li>
<li>Fix compilation with gcc6, the code was comparing a char* and a QChar. <a href='http://commits.kde.org/kdenlive/9f86e56335df43fd11fc1249002715640dd51517'>Commit.</a> </li>
<li>Add action to remove all preview zones. <a href='http://commits.kde.org/kdenlive/166a9dbdd309b93e333d1b2ae4a2908760295eda'>Commit.</a> </li>
<li>Move timeline cursor after insert point when using insert zone in timeline. <a href='http://commits.kde.org/kdenlive/839fc059e672cf389409e9716839d168fedbe64a'>Commit.</a> </li>
<li>Fix crash on rotoscoping, effect still need porting to qml to be usable. <a href='http://commits.kde.org/kdenlive/af25180d1c3cb832129d2a2901212d8dd1bf5e4f'>Commit.</a> </li>
<li>Add scriptable methods to add a bin/timeline clip and effect. <a href='http://commits.kde.org/kdenlive/5fdbdf94d63acb9c2410a4893c478a3c5afc4d66'>Commit.</a> </li>
<li>Bump master git version. <a href='http://commits.kde.org/kdenlive/8279fddb0d90557a2422266abbc8c5c9480d8982'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kdenlive/b5040d7c43158fd826fab324553cfbca3191fdad'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kdenlive/b39eae24f2779ca05fd87459fe94d57539ea71cb'>Commit.</a> </li>
<li>Fixes and updates preparing the new qtblend transition/effect. <a href='http://commits.kde.org/kdenlive/c4100bdd01ef02371c47aba128aa96fdfa420972'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Show]</a></h3>
<ul id='ulkdepim-addons' style='display: none'>
<li>It works fine in 5.8.0 => activate it with qt5.8.0. <a href='http://commits.kde.org/kdepim-addons/66f7143b6a43013c110cf1e580723227679fba11'>Commit.</a> </li>
<li>PimEventsPlugin: catch PayloadException. <a href='http://commits.kde.org/kdepim-addons/9b2176758ac1649a7090947795d6c62bc3ac4835'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372824'>#372824</a></li>
<li>Add missing description. <a href='http://commits.kde.org/kdepim-addons/64f0459b944d0fd354e42dc3ad8da9c56232048c'>Commit.</a> </li>
<li>Avoid to create several script with same name. <a href='http://commits.kde.org/kdepim-addons/36da994414c281cd4814d76ef50bb93e6cccac35'>Commit.</a> </li>
<li>UI strings: improve readability. <a href='http://commits.kde.org/kdepim-addons/781d3e7b645592dd357d856e9a0dcf23331e06ac'>Commit.</a> </li>
<li>Don't add empty list. <a href='http://commits.kde.org/kdepim-addons/cb76152becb85e0be8ec1f93dfeb9bf34c4eccc0'>Commit.</a> </li>
<li>Fix split emails. <a href='http://commits.kde.org/kdepim-addons/e31bba2f5c244acd9f3fd1205711da51c7f04619'>Commit.</a> </li>
<li>Disable it for release version. <a href='http://commits.kde.org/kdepim-addons/5415f4944e23517b90c509fb18451286ede8eef2'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-addons/a492229ff2d141ee168b01d1f8f7874d423899a7'>Commit.</a> </li>
<li>Fix some clazy warning. <a href='http://commits.kde.org/kdepim-addons/7dadd2361d81cbd14faaf4e5d8f627819e11bc45'>Commit.</a> </li>
<li>Fix minor typos. <a href='http://commits.kde.org/kdepim-addons/cc437440fe227b0488ab54971839aa0d3def4123'>Commit.</a> </li>
<li>Remove compile warning. <a href='http://commits.kde.org/kdepim-addons/f9effedd2c4e4d24418365b8c342eb849246c726'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-addons/3c1afccd662eb907996ebc16b53530ce3006975d'>Commit.</a> </li>
<li>Introduce GnuPG WKS and PGP keys formatter plugins. <a href='http://commits.kde.org/kdepim-addons/91b894103cc5104af8e624748f0da4d7011fdbe4'>Commit.</a> </li>
<li>Don't ask overwrite file twice. <a href='http://commits.kde.org/kdepim-addons/fed66bf18bfc0bb97f1c1d6c194b0bdea65c6e6c'>Commit.</a> </li>
<li>Use unique network manager. <a href='http://commits.kde.org/kdepim-addons/b180f26d345e3093fa339c1e6f069007adb5abe5'>Commit.</a> </li>
<li>USe hasHeader. <a href='http://commits.kde.org/kdepim-addons/cb71e7ffe30d4c49308539f661fc6fa05351f0b0'>Commit.</a> </li>
<li>Cache header pointer. <a href='http://commits.kde.org/kdepim-addons/7e1fd7a141c6e056726794a0315421c41607be6b'>Commit.</a> </li>
<li>Add parent. <a href='http://commits.kde.org/kdepim-addons/8b879db57b6093fffe84e5a707736fa794cbcacd'>Commit.</a> </li>
<li>Clean forward declaration. <a href='http://commits.kde.org/kdepim-addons/4a351884863d6b97d6fb4ded02138c9d5220ecca'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-addons/935eb9d2eaf0e39e50fb1183500b31f6eda01149'>Commit.</a> </li>
<li>Fix minor typo. <a href='http://commits.kde.org/kdepim-addons/fca2e3048d38dedf60befea0314174d50749c011'>Commit.</a> </li>
<li>UI messages: fixes (style, grammar). <a href='http://commits.kde.org/kdepim-addons/d19001cad38704497c0e017399fde95984e00b3f'>Commit.</a> </li>
<li>Emit finished when import is done. <a href='http://commits.kde.org/kdepim-addons/219b6d812783a538746222071270279b8d325e1b'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/kdepim-addons/effb2f673b5d06318cf4b26b71df74fd2ce9dec9'>Commit.</a> </li>
<li>Now we activate emoticon by default. <a href='http://commits.kde.org/kdepim-addons/1b73d7ee7297df4ca8ba42f33a28392e3d3e94b5'>Commit.</a> </li>
<li>Fix Bug 369376 - Show date in message view part. <a href='http://commits.kde.org/kdepim-addons/e2550ca1e3a0aea852f18849984713d98335c4cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369376'>#369376</a></li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-addons/71e965340cfe42f2595d9356880d8599cbf01f45'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/kdepim-addons/2b7dd973b007568f31678c5e86ee9e3c04022240'>Commit.</a> </li>
<li>Remove reset method. <a href='http://commits.kde.org/kdepim-addons/e61f933c230119fbd242c410a749f4e4c863c16a'>Commit.</a> </li>
<li>Use qCWarning + remove debug. <a href='http://commits.kde.org/kdepim-addons/2be43ecd0e5a4c2714d77708e16acd653785075c'>Commit.</a> </li>
<li>Fix autotests. <a href='http://commits.kde.org/kdepim-addons/a9fee31fae2b63c21d5b3695da05be2c7c1921cd'>Commit.</a> </li>
<li>Increase kleo version. <a href='http://commits.kde.org/kdepim-addons/6bc6f1615f0d1254a74b1b6bb61921d1b993b16b'>Commit.</a> </li>
<li>Try to fix autotest. <a href='http://commits.kde.org/kdepim-addons/224509bfd96418cab021f412cdc677f56d18c834'>Commit.</a> </li>
<li>Adjust buildsystem for gpgme-1.7. <a href='http://commits.kde.org/kdepim-addons/11199bb2bcd1dcc8b64218e9a323ecca89634f50'>Commit.</a> </li>
<li>Minor fix. <a href='http://commits.kde.org/kdepim-addons/98394329783f0a2b4733eedcee1eb93f734e48b0'>Commit.</a> </li>
<li>Fix progressbar. <a href='http://commits.kde.org/kdepim-addons/cbc0329559939ce6bf4c66261bbf503fdfe175b1'>Commit.</a> </li>
<li>Remove it. <a href='http://commits.kde.org/kdepim-addons/2f6483e9470b875558bee026947b652179069bac'>Commit.</a> </li>
<li>Fix export vcard. <a href='http://commits.kde.org/kdepim-addons/a62b88f2f593166f513215cb47d55eb5224a3885'>Commit.</a> </li>
<li>Fix categories name. <a href='http://commits.kde.org/kdepim-addons/434812dcd4ab39e27b8595f131bd55fe61ff1739'>Commit.</a> </li>
<li>Fix import/export. <a href='http://commits.kde.org/kdepim-addons/6d8c74093ef00ae5da7084ae279741bb79756218'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-addons/edfee3a1fb7b7dcfb08e24977a8c25aac5bf52cf'>Commit.</a> </li>
<li>Add export support. <a href='http://commits.kde.org/kdepim-addons/9130ef86aa523f6b02f3071ed7f1aaaf0ea76280'>Commit.</a> </li>
<li>Add export gmx support. <a href='http://commits.kde.org/kdepim-addons/05c26e54964ed984a2d8702db2b420af994d6df1'>Commit.</a> </li>
<li>Add code from gmx_xxport. <a href='http://commits.kde.org/kdepim-addons/39b3eadbc4ecc47be3b933c77677fbf8b320a286'>Commit.</a> </li>
<li>Add support for importFile. <a href='http://commits.kde.org/kdepim-addons/c395476a34b45e3475ee1726abc234e05ad19307'>Commit.</a> </li>
<li>Implement export. <a href='http://commits.kde.org/kdepim-addons/5cda489eb9e3091fe73eb5656accf9353dae554e'>Commit.</a> </li>
<li>Improve vcard. <a href='http://commits.kde.org/kdepim-addons/78a8c9b77309139160bad724256ad077a19bd212'>Commit.</a> </li>
<li>Improve plugins. <a href='http://commits.kde.org/kdepim-addons/2c6920cc78ccd3ec13284479a40dd471cfe47d34'>Commit.</a> </li>
<li>Improve plugins. <a href='http://commits.kde.org/kdepim-addons/dc473a0a6396f3fe131f93600c18db160b2478b6'>Commit.</a> </li>
<li>Continue to implement plugins. <a href='http://commits.kde.org/kdepim-addons/84fba0ea2db2d292ec7c6f2c95283c6915f412fa'>Commit.</a> </li>
<li>Port to new api. <a href='http://commits.kde.org/kdepim-addons/ed65673984db1c9f08377f5c7998cf4141a9a955'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-addons/6eeb71266d02028119b66296f96ae2ddb6ea6dd4'>Commit.</a> </li>
<li>Continue to implement it. <a href='http://commits.kde.org/kdepim-addons/2eed6f2f10caa5e41b41e0cdea43db5fc38fccd4'>Commit.</a> </li>
<li>Add import dialog for csv. <a href='http://commits.kde.org/kdepim-addons/42ba70ef6b69bf4152f1174c163186223951429a'>Commit.</a> </li>
<li>Add import csv. <a href='http://commits.kde.org/kdepim-addons/d978e647edb38a699e73de85f5a1cbd8a3f76a01'>Commit.</a> </li>
<li>Start to implement export. <a href='http://commits.kde.org/kdepim-addons/94b411c1c17eab3712ba6a61701596b75ac1a579'>Commit.</a> </li>
<li>Oops I didn't want to remove this file. <a href='http://commits.kde.org/kdepim-addons/2b78bee3d09a74bdfadaf26aa73e28427e515b63'>Commit.</a> </li>
<li>Add shared lib + start to define export ldif support. <a href='http://commits.kde.org/kdepim-addons/505e8a8423e7ae34127ca2ddbcea3f5292f3b195'>Commit.</a> </li>
<li>Port ldap plugin. <a href='http://commits.kde.org/kdepim-addons/ad0fb9468967e89dd8c72ea6b7711593dac5b2c7'>Commit.</a> </li>
<li>USe new API. <a href='http://commits.kde.org/kdepim-addons/4702ce496c712db778777185f6af080723a9c7a4'>Commit.</a> </li>
<li>Reimplement ldap plugins. <a href='http://commits.kde.org/kdepim-addons/4eaf7e6bf187b868bd54ff311266f2ad0cc2222e'>Commit.</a> </li>
<li>Add support for import. <a href='http://commits.kde.org/kdepim-addons/c19d88e129718f6acaf1d70efe05ebd2c5970c58'>Commit.</a> </li>
<li>Add shared lib. <a href='http://commits.kde.org/kdepim-addons/87e31e59a89e356f20c21e26f7acc12b75eaa741'>Commit.</a> </li>
<li>Add private shared lib. <a href='http://commits.kde.org/kdepim-addons/ada4c19fa0751a6dd40d8122738b9997b33f8ccc'>Commit.</a> </li>
<li>Add signal/slot. <a href='http://commits.kde.org/kdepim-addons/7d49e8fe594ebd98817da767379630e061f17170'>Commit.</a> </li>
<li>Add signal/slot. Import plugin. <a href='http://commits.kde.org/kdepim-addons/c84b8fecc3a895f3e5396d8d02d0286cd0de5c10'>Commit.</a> </li>
<li>Add actions. <a href='http://commits.kde.org/kdepim-addons/b59085751d854b9123ff78f2087470ea19300186'>Commit.</a> </li>
<li>Allow to translate. <a href='http://commits.kde.org/kdepim-addons/95c20b135ca5e0262b364ade83b204a57f63d070'>Commit.</a> </li>
<li>Add ldap plugin. <a href='http://commits.kde.org/kdepim-addons/c679a7b77c88cb5907d967536fd050f868ba1c18'>Commit.</a> </li>
<li>Add more plugins. <a href='http://commits.kde.org/kdepim-addons/86623cc734aa0255beb7cf51046f28316c218246'>Commit.</a> </li>
<li>Add windowtitle. <a href='http://commits.kde.org/kdepim-addons/157be4b5da42130469a192766cc6d66e67aad73d'>Commit.</a> </li>
<li>Allow to define exclude domain. <a href='http://commits.kde.org/kdepim-addons/6c50b9f0bfdf3a9e474c4808b7a680c5abeb821d'>Commit.</a> </li>
<li>We need all code here now. <a href='http://commits.kde.org/kdepim-addons/9fb78251a8fba0d67aa670ecb6f777ee13b670c0'>Commit.</a> </li>
<li>Start to implement as plugin. <a href='http://commits.kde.org/kdepim-addons/01fb3e4b6616f134f0277b99aded472628ebfdd1'>Commit.</a> </li>
<li>Prepare importexport plugins. <a href='http://commits.kde.org/kdepim-addons/a35b45a5e43e062b352b8715f717fb92e2cc2d12'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-addons/5297b4e9810b94f7e49c5a23cc5a530c87d35c2e'>Commit.</a> </li>
<li>Fix variable name. <a href='http://commits.kde.org/kdepim-addons/72de546d721caf68e0d0effa0ca1cc69e59e0b61'>Commit.</a> </li>
<li>I will move importexport kaddressbook contact as plugins. <a href='http://commits.kde.org/kdepim-addons/959d4129c924c47a9eb4757d569264f511674eeb'>Commit.</a> </li>
<li>Disable by default. <a href='http://commits.kde.org/kdepim-addons/1dcb7fb7ebd617fefda3d99bc83d725dd1db3ce5'>Commit.</a> </li>
<li>Disable it by default. <a href='http://commits.kde.org/kdepim-addons/c429910b4300f9daed8fe6d0261aebedf271759a'>Commit.</a> </li>
<li>Use new connect api. <a href='http://commits.kde.org/kdepim-addons/cdefa9850ad171154824495feb1cf6f88d1272c6'>Commit.</a> </li>
<li>Fix dialog layout. <a href='http://commits.kde.org/kdepim-addons/176a79cf93e53914bcb774c242fae158fa509375'>Commit.</a> </li>
<li>Fix dialog width. <a href='http://commits.kde.org/kdepim-addons/fc4c2ec2b17e01612c96e86e6ee7cb018bd21d2a'>Commit.</a> </li>
<li>Improve autotests. <a href='http://commits.kde.org/kdepim-addons/2b875f42d23da4fbe90d4200ecc1b635eb35b6ea'>Commit.</a> </li>
<li>This plugins doesn't have default button. <a href='http://commits.kde.org/kdepim-addons/6af7ca72887b45cb20bda8d469a0203cdc2be90c'>Commit.</a> </li>
<li>Now we can enable/disable plugin => we don't need to have another. <a href='http://commits.kde.org/kdepim-addons/e11ff25b73aff4d401115df1927bfe239b3b4498'>Commit.</a> </li>
<li>Update button at the beginning => fix enable/disable buttons. <a href='http://commits.kde.org/kdepim-addons/83c82e9d96a6898f9a4776b56f70b1ad36bbf8a3'>Commit.</a> </li>
<li>Fix minor typo. <a href='http://commits.kde.org/kdepim-addons/4e97e4d2d84a8d61eb2b162bd176842090fdefb8'>Commit.</a> </li>
<li>Adapt to new api. <a href='http://commits.kde.org/kdepim-addons/e90b4891bc1bbbba6e15d6fbb323db72c6bfadb2'>Commit.</a> </li>
<li>Add documentation. <a href='http://commits.kde.org/kdepim-addons/46d0dc249ed2e1de8b327d0399f25aaf2cbec6bb'>Commit.</a> </li>
<li>Allow to modify item when we double click. <a href='http://commits.kde.org/kdepim-addons/93044cf7cdc21f0e0c424eb311d3f7a6ecc0fcb1'>Commit.</a> </li>
<li>Add description. <a href='http://commits.kde.org/kdepim-addons/54c8c7474b89541ca0ef66d96b2967b850c6d20b'>Commit.</a> </li>
<li>Rename variable. <a href='http://commits.kde.org/kdepim-addons/717ad9d0c7f55d1fb00ea7bc88c3eef5c4afd1d1'>Commit.</a> </li>
<li>Fix i18n. <a href='http://commits.kde.org/kdepim-addons/6f2a8cd33f0b00e05235f6086dbd9df7d1d00d22'>Commit.</a> </li>
<li>Make sure that script is valid. <a href='http://commits.kde.org/kdepim-addons/a51663a888953b893bfed71f00b1da2d7bd9ff3f'>Commit.</a> </li>
<li>Allow to save as desktop file. <a href='http://commits.kde.org/kdepim-addons/ff4f28421e21b75183d8170f1a9cd2b612e28ca4'>Commit.</a> </li>
<li>Allow to enable/disable ok button when name is empty or not. <a href='http://commits.kde.org/kdepim-addons/9c1856de1351aaea8477bea8148cd10c7171f075'>Commit.</a> </li>
<li>Allow to save external script. <a href='http://commits.kde.org/kdepim-addons/04412532d5093fe127ffe7fbb7f34badc70c2026'>Commit.</a> </li>
<li>More autotest. <a href='http://commits.kde.org/kdepim-addons/d7f87431534b140667e410df3d5401fb51982ddb'>Commit.</a> </li>
<li>Add more test. <a href='http://commits.kde.org/kdepim-addons/7c0222f8aca77f8a9a130069d79692c816d95fe6'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/kdepim-addons/d09190cf1e9ce70b13b9aa0ba7053c86c51ae0b9'>Commit.</a> </li>
<li>Disable adblock by default. <a href='http://commits.kde.org/kdepim-addons/5c0fe7c9e2f4475b7d6845acca8662e6b2327c93'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kdepim-addons/883c9977a64867c26a42c38d1050953a2ea68de0'>Commit.</a> </li>
<li>Continue to implement it. <a href='http://commits.kde.org/kdepim-addons/104ec1b770106e67842448f8a3e2642f715b09fb'>Commit.</a> </li>
<li>Close dialogbox. <a href='http://commits.kde.org/kdepim-addons/9b9a934fcfe30621cd370fede2c43239672c4398'>Commit.</a> </li>
<li>Look at to create autotest here. <a href='http://commits.kde.org/kdepim-addons/0d22b1c83614b06a80a2f3ef56ead46fd01dcee6'>Commit.</a> </li>
<li>Now we have a configure dialog box. <a href='http://commits.kde.org/kdepim-addons/a3b521c2f3e33d091a0ae914320e431bb0a1880f'>Commit.</a> </li>
<li>Improve++. <a href='http://commits.kde.org/kdepim-addons/d72381e7afa4a0d7de20433a5776862b82b90a6f'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-addons/f8501f7dd124f0b34ec0545becab9e90a9c46170'>Commit.</a> </li>
<li>Time to increase version. <a href='http://commits.kde.org/kdepim-addons/af7c12f072653eec88541f38dd225273f6805d60'>Commit.</a> </li>
<li>Add mroe autotests. <a href='http://commits.kde.org/kdepim-addons/62743517a2a5e0857addb07f665c0582a79c93ee'>Commit.</a> </li>
<li>Improve dialog box. <a href='http://commits.kde.org/kdepim-addons/35cbd837655096f63f60a60db002e0960a71c8d7'>Commit.</a> </li>
<li>Use ViewerPluginExternalScriptInfo. <a href='http://commits.kde.org/kdepim-addons/e1f296ccc08fad51434c83146b37d274ae375d30'>Commit.</a> </li>
<li>Use a custom QListViewItem. <a href='http://commits.kde.org/kdepim-addons/c11988bb7c86643cba28db4e52c7aeb8f757dd55'>Commit.</a> </li>
<li>Continue to implement it. <a href='http://commits.kde.org/kdepim-addons/8b64581ac2db86a65f5ed4622df8722972e0fdd3'>Commit.</a> </li>
<li>Update buttons. <a href='http://commits.kde.org/kdepim-addons/47c8adad58b9a9e7a5d1f39a4cce18be9d2d050d'>Commit.</a> </li>
<li>Add future edit script dialog. <a href='http://commits.kde.org/kdepim-addons/386a19954b64ca38578dcff8cd0ecb279f477130'>Commit.</a> </li>
<li>Fill list. <a href='http://commits.kde.org/kdepim-addons/051ebcf4d1924920dc652400b2a7c7d6c9e22d3c'>Commit.</a> </li>
<li>Store filename path. <a href='http://commits.kde.org/kdepim-addons/9165d7fa382b0f1b888f79e413f843beece50289'>Commit.</a> </li>
<li>Improve configure dialog. <a href='http://commits.kde.org/kdepim-addons/345d55e169a77e71de58541a40636ec1926f2fe3'>Commit.</a> </li>
<li>Connect buttons. <a href='http://commits.kde.org/kdepim-addons/b43b20652ee512980b4dae8ae957402ab4806e81'>Commit.</a> </li>
<li>Continue to implement it. <a href='http://commits.kde.org/kdepim-addons/2e576a3888c80ce3a0b0db25420e9d0efdcee917'>Commit.</a> </li>
<li>Add window title here. <a href='http://commits.kde.org/kdepim-addons/2ef217414cc863d8b4d11602f7c0aa35852f7ae4'>Commit.</a> </li>
<li>Improve configure dialog. <a href='http://commits.kde.org/kdepim-addons/431143971ea60a4ada12c5094485a8ed7ca3e56b'>Commit.</a> </li>
<li>Continue to implement configure dialog. <a href='http://commits.kde.org/kdepim-addons/2af05597fb0524ef1d1237ab82ef7f9ce32a163e'>Commit.</a> </li>
<li>Adapt to new api. <a href='http://commits.kde.org/kdepim-addons/0f121b9c446db641e3cee4f271d669066343c991'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kdepim-addons/e75042a98015ac75664318870a3c7a5236471888'>Commit.</a> </li>
<li>Move in own directory. <a href='http://commits.kde.org/kdepim-addons/be366425f8ab3e4af2e2da08cfdcf5fae26d66c5'>Commit.</a> </li>
<li>Start to implement configure dialog. <a href='http://commits.kde.org/kdepim-addons/cabe6543b74a68af6fc6d4b0de4211cad6650101'>Commit.</a> </li>
<li>Add dialogbox to configure plugins. Clean up plugins. <a href='http://commits.kde.org/kdepim-addons/6929562ba3a03210863e2ddd83833d8f2b14b8ca'>Commit.</a> </li>
<li>Show dialogbox. <a href='http://commits.kde.org/kdepim-addons/5cae4a0c561f36a8ccbecb87a5f6a02626e2bb72'>Commit.</a> </li>
<li>Create dialogbox. <a href='http://commits.kde.org/kdepim-addons/aec745ca80899a4c037de3e831101481faa083ba'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/kdepim-addons/205eaf4c369891ca8120d44e9a6c1dddc24b17c4'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-addons/cab850ab6e253c56df3aa6aa7647bfea8e050c77'>Commit.</a> </li>
<li>Rename method. <a href='http://commits.kde.org/kdepim-addons/3c2a16fca5bcad58d75663ae994c22a4ddc57adb'>Commit.</a> </li>
<li>Adapt to new api. <a href='http://commits.kde.org/kdepim-addons/ec20a9cf7e3dbfee26a70da0fa9a529d507f3db1'>Commit.</a> </li>
<li>Adapt to new api. <a href='http://commits.kde.org/kdepim-addons/874345e9789f292bbf2fcac3ced9961e351555f7'>Commit.</a> </li>
<li>Adapt against new api. <a href='http://commits.kde.org/kdepim-addons/b4d27019657478381dbb735982f6f976a257afab'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-addons/9b8ccc61861492b6ce2181ca6cfdab0482189274'>Commit.</a> </li>
<li>Disable by default. <a href='http://commits.kde.org/kdepim-addons/1570e8fa4d23e4388f8fe5b3ad9c5544652b3eca'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/kdepim-addons/50a35995d0203e41765a70784fbb33419d962506'>Commit.</a> </li>
<li>USe kcoreaddons macros. <a href='http://commits.kde.org/kdepim-addons/5597dcb6d656f5133e81473a42de47ff5bf5ca3a'>Commit.</a> </li>
<li>Modernize Akonadi unittestenv. <a href='http://commits.kde.org/kdepim-addons/1702848f87825064ab83ca370914539b68699f10'>Commit.</a> </li>
<li>Move defaultgrantleeheaderstyleplugin in messageviewer directly. <a href='http://commits.kde.org/kdepim-addons/ac9c14d21c2d7270c2c66277fbd19f5c043f0235'>Commit.</a> </li>
<li>Add more descriptions. <a href='http://commits.kde.org/kdepim-addons/2a08af3d40b30db5e6ecbbbb148b46c14364f051'>Commit.</a> </li>
<li>Time to increase version. <a href='http://commits.kde.org/kdepim-addons/633a0ef388b2134dd88ad0479549d361fe3bc28e'>Commit.</a> </li>
<li>Use macro for shorturlplugin plugins. <a href='http://commits.kde.org/kdepim-addons/ef123d95b87596dd3adb656357ffed7e9814907c'>Commit.</a> </li>
<li>Use macro here too. <a href='http://commits.kde.org/kdepim-addons/d1af1afac44a638c1c8e8f7ee669fe02ac3d9b80'>Commit.</a> </li>
<li>Use kcoreaddons macro for plugins. <a href='http://commits.kde.org/kdepim-addons/a0bf5e084520eb7dd83aa10ac703d43ceb615289'>Commit.</a> </li>
<li>Port plasma to macro too. <a href='http://commits.kde.org/kdepim-addons/5aee3f02c1051d0cfe714b45e4074b4195d3851b'>Commit.</a> </li>
<li>Convert more plugin to macro. <a href='http://commits.kde.org/kdepim-addons/714e13e107382e926827ccbd68585277fb49ae93'>Commit.</a> </li>
<li>USe new macro. <a href='http://commits.kde.org/kdepim-addons/d445185c5ebf8b5648722e2c7cddb3687686a008'>Commit.</a> </li>
<li>Re-enable etm_usage, port from CR to Monitor. <a href='http://commits.kde.org/kdepim-addons/920531e6ddae836e3d1f83aefcd4c03f3dbff2c1'>Commit.</a> </li>
<li>Disable etm_usage folder from building. <a href='http://commits.kde.org/kdepim-addons/9aeade1fb60f7010d39b23349df374bc41947537'>Commit.</a> </li>
<li>Try (again!) to fix build. <a href='http://commits.kde.org/kdepim-addons/101ae0188a9386f7b5b7c142054eaef4329f8cc9'>Commit.</a> </li>
<li>Last (hopefully) batch of changes due to SIC change in Akonadi. <a href='http://commits.kde.org/kdepim-addons/e8a29bbdb4eb1191c05121326416b4d389811383'>Commit.</a> </li>
<li>Yet more fixes for BIC. <a href='http://commits.kde.org/kdepim-addons/4e4263d1a92601018e46d7577903bc67e4224442'>Commit.</a> </li>
<li>More fixes due to BIC change. <a href='http://commits.kde.org/kdepim-addons/ee6e0ae7f47cb1ce1e0bba4ea6bd1aa90f36120c'>Commit.</a> </li>
<li>One more change I forgot for BIC... <a href='http://commits.kde.org/kdepim-addons/ab9ea188edd9aeb531984d33d859d599dbd372d0'>Commit.</a> </li>
<li>Remove one more usage of Akonadi::ChangeRecorder for ETM. <a href='http://commits.kde.org/kdepim-addons/cab63f9eca54f55e658503144f82b2683c0b0414'>Commit.</a> </li>
<li>More fixes for the Akonadi BIC change. <a href='http://commits.kde.org/kdepim-addons/92940b705e5c5a0ed3f5e5533ce2e368438c70ba'>Commit.</a> </li>
<li>Remove include_directory. <a href='http://commits.kde.org/kdepim-addons/4ae436b9047146e41e736dab466791cc30ef860f'>Commit.</a> </li>
<li>Continue to migrate to kcoreaddons macros. <a href='http://commits.kde.org/kdepim-addons/9bcce7d5831e6931321cdac36a3340c3e0eb14fb'>Commit.</a> </li>
<li>Add description. <a href='http://commits.kde.org/kdepim-addons/8a5e3cae89e905bb7373709d17f1f64f2060055d'>Commit.</a> </li>
<li>Use kcoreaddons macro. <a href='http://commits.kde.org/kdepim-addons/e01c96a7df4a5a3ed8087dff88dea7f53a10d70d'>Commit.</a> </li>
<li>Add kcoreaddons macro. <a href='http://commits.kde.org/kdepim-addons/36d12e5d96d52779e7bcce4b64ba6d5b8ed6dd9d'>Commit.</a> </li>
<li>Port to kdecoreaddons plugins. <a href='http://commits.kde.org/kdepim-addons/e9bc132a7aac024a39ecf06f8e7016a702b3f8ee'>Commit.</a> </li>
<li>Port to new macro. <a href='http://commits.kde.org/kdepim-addons/4f2df12fe8bca9c566079d53fc05845aa4953025'>Commit.</a> </li>
<li>Use kcoreaddons macro for plugins. <a href='http://commits.kde.org/kdepim-addons/bf0eb6ae9aaeaf9822e1d93850d783e0a1fb3ccb'>Commit.</a> </li>
<li>Convert more CMakeLists. <a href='http://commits.kde.org/kdepim-addons/3c96313076d6158397cab51de6026debb7d0a765'>Commit.</a> </li>
<li>Convert more plugins. <a href='http://commits.kde.org/kdepim-addons/a8e8790b414eea5dd507ec127c40c5ade823d388'>Commit.</a> </li>
<li>Use macro to generate plugins. <a href='http://commits.kde.org/kdepim-addons/22077b86148dfe603c2c4d46dd4ca558cf5ef905'>Commit.</a> </li>
<li>Use kcoreaddons_add_plugin. <a href='http://commits.kde.org/kdepim-addons/11bf7ac7332427adcdd0f17b6fd93e937962ea6c'>Commit.</a> </li>
<li>UI strings: capitalize the acronym URL. <a href='http://commits.kde.org/kdepim-addons/6dcca06208464c6ae83d26c4bfb61d053015f676'>Commit.</a> </li>
<li>It's time to increase version. <a href='http://commits.kde.org/kdepim-addons/4bd1cd5c4489358bf3083ecda05f4cafe880ed99'>Commit.</a> </li>
<li>Add more description. <a href='http://commits.kde.org/kdepim-addons/b2e2221654217815ab4247dedb15d1e8f79c9d2d'>Commit.</a> </li>
<li>Fix "allows [somebody/something] to". <a href='http://commits.kde.org/kdepim-addons/b6d6c3c542986dc0046f5fb30d6b2fc70d36144d'>Commit.</a> </li>
<li>Fix minor typo. <a href='http://commits.kde.org/kdepim-addons/edd6cbe4590dcb509f86d1ed1f092c51914c7f83'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-addons/049613100fcfcb231f45b91762047b2175dcfbed'>Commit.</a> </li>
<li>Add more EnabledByDefault + description. <a href='http://commits.kde.org/kdepim-addons/9b3b0943c96b8710e598a319b1db8f1e514c825e'>Commit.</a> </li>
<li>Add identifier. <a href='http://commits.kde.org/kdepim-addons/32df22087c3de9f76818007299d2045dd5c2b330'>Commit.</a> </li>
<li>Fix syntax error in a JSON file. <a href='http://commits.kde.org/kdepim-addons/a1fcee1aa7049633cfc7c9352d335eac6844dfd7'>Commit.</a> </li>
<li>Add more description. <a href='http://commits.kde.org/kdepim-addons/eeef3679994ad5d1101414c23b88b61ce63a7af4'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kdepim-addons/62ccac62ad39876b1080d5f2efc2fdc9dcd09007'>Commit.</a> </li>
<li>Add description. <a href='http://commits.kde.org/kdepim-addons/130c35aa850ac5c78978d00197ae59880d4124f1'>Commit.</a> </li>
<li>Use EnabledByDefault. <a href='http://commits.kde.org/kdepim-addons/0dda1bdc9412b1d5bb8215de3aabfc5848a3d942'>Commit.</a> </li>
<li>Add EnabledByDefault. It will allow to load or not by defaut plugin. <a href='http://commits.kde.org/kdepim-addons/bc808e08affc823dd98eeb14b04276fee126af53'>Commit.</a> </li>
<li>I revert it as it's build when example is activate in build system and disable when we make a release. <a href='http://commits.kde.org/kdepim-addons/229a07f3fd467c18094be918be37aa17dddcff3c'>Commit.</a> </li>
<li>Don't install the rot13 example plugin. <a href='http://commits.kde.org/kdepim-addons/c0ea0945f4ce0395d738cf6b050f2e29dc17c5f7'>Commit.</a> </li>
<li>Use  QStandardPaths::setTestModeEnabled(true); when necessary. <a href='http://commits.kde.org/kdepim-addons/7c43eb659758661f61078b0188eff4fbca04d95a'>Commit.</a> </li>
<li>Minor. <a href='http://commits.kde.org/kdepim-addons/c8fad475f16b68ff833084047e7172feb3f045d4'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/kdepim-addons/8a7fb18b24087ee9a05f08f6a3dbc3ebd93af251'>Commit.</a> </li>
<li>Remove commented code. <a href='http://commits.kde.org/kdepim-addons/cb1c5b8b886d8fc8e258a9ece8bd58e89b73e55e'>Commit.</a> </li>
<li>Fix i18n. <a href='http://commits.kde.org/kdepim-addons/498daa62f4a0443d57fb136793b8e22545b6c491'>Commit.</a> </li>
<li>Add support for autocorrect selected text. <a href='http://commits.kde.org/kdepim-addons/95bf7797141f720f3e80e1cf5ba08807246cd9f5'>Commit.</a> </li>
<li>Use forceAutoCorrection. <a href='http://commits.kde.org/kdepim-addons/cd1f68534a2aba4577f7c1e5f73f1a06bf04605f'>Commit.</a> </li>
<li>Remove dependancy. <a href='http://commits.kde.org/kdepim-addons/455e8fd24250438b88478847c5f03e3e54229f15'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-addons/5cfd409bca9553b783279db306db221c8c105110'>Commit.</a> </li>
<li>Remove job we will use a method directly in composer. <a href='http://commits.kde.org/kdepim-addons/bd58337c7f3f4a1020df8cfbe7901d6c3d673a7d'>Commit.</a> </li>
<li>Add autocorrection. <a href='http://commits.kde.org/kdepim-addons/f927c5c37a016090849a525e742f73986333a41a'>Commit.</a> </li>
<li>Add a job. <a href='http://commits.kde.org/kdepim-addons/807ed6de61a629a0d298725c693d002a23e6fb8d'>Commit.</a> </li>
<li>Start to implement autocorrection tool. <a href='http://commits.kde.org/kdepim-addons/45b172617ba7544fb04ec81724aa66f783902ed7'>Commit.</a> </li>
<li>Create new plugin to autocorrect text. <a href='http://commits.kde.org/kdepim-addons/e284a3a4b4ff8fe69aed0c16d120f920954e06df'>Commit.</a> </li>
<li>Add messagebox to inform that emails was added. <a href='http://commits.kde.org/kdepim-addons/ca0118a5390677902909dc610bd4b6d48e191cad'>Commit.</a> </li>
<li>Fix enable/disable button. Emit signal when necessary. <a href='http://commits.kde.org/kdepim-addons/104516e0ed797e39f1b5b91bdaf45edfde83e9c6'>Commit.</a> </li>
<li>Typo fix (breaking). <a href='http://commits.kde.org/kdepim-addons/79bbebc4a1796798e58b7be0ee5d348e53673742'>Commit.</a> </li>
<li>Add insert breaking space as plugin. <a href='http://commits.kde.org/kdepim-addons/75191ad8695117bb586919ead4e3e5dea28ddf44'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-addons/62040f6e6ac5f0636ef2248196e4c161e2a9db0a'>Commit.</a> </li>
<li>Increase bersion. <a href='http://commits.kde.org/kdepim-addons/442db0b04f197c56d898d3a262d0b2a68966ab2e'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-addons/9d004f7442efe7c836a753f26870ca566dc648f3'>Commit.</a> </li>
<li>In master we are in pre version. <a href='http://commits.kde.org/kdepim-addons/b724886fd1f9eba050ae993b1d58d84f46822e1f'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-apps-libs' href='https://cgit.kde.org/kdepim-apps-libs.git'>kdepim-apps-libs</a> <a href='#kdepim-apps-libs' onclick='toggle("ulkdepim-apps-libs", this)'>[Show]</a></h3>
<ul id='ulkdepim-apps-libs' style='display: none'>
<li>Fix error about layouting. <a href='http://commits.kde.org/kdepim-apps-libs/221b620694613997070b356c5718f82ddfea25b3'>Commit.</a> </li>
<li>Add method for importing data. <a href='http://commits.kde.org/kdepim-apps-libs/0553c178f437c02aba631563e494cc25ff004520'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-apps-libs/a7d68a216b5515d69940e752d9c5e09cd1463c45'>Commit.</a> </li>
<li>Add method to import url. <a href='http://commits.kde.org/kdepim-apps-libs/062d29ad37ca2da8775f4e86f81c68054382fc15'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/kdepim-apps-libs/6e963afc8674f346c061204b151f89f77ee9daa8'>Commit.</a> </li>
<li>Allow to define item selection model. <a href='http://commits.kde.org/kdepim-apps-libs/5d8633b4d3b57df2bcd1832efe2eec809e3e8632'>Commit.</a> </li>
<li>Allow to set KAddressBookImportExportContactList. <a href='http://commits.kde.org/kdepim-apps-libs/a1e94cdfd833d41455e55ff5fdad2fcf76eb6e9b'>Commit.</a> </li>
<li>Move autotest here. <a href='http://commits.kde.org/kdepim-apps-libs/cbc69bfe28983b4cd7ef5a3a31247b48bda977a1'>Commit.</a> </li>
<li>We need contact fields in this library. <a href='http://commits.kde.org/kdepim-apps-libs/aca19ff99be98159588a146b2fd72c2ca24d3cd2'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-apps-libs/67c8a11ef12ede92b2f83c4f21255fe9239caa93'>Commit.</a> </li>
<li>Reactivate code. <a href='http://commits.kde.org/kdepim-apps-libs/cf4948cb94873e225dbf6f297197120678315939'>Commit.</a> </li>
<li>Add more widget. <a href='http://commits.kde.org/kdepim-apps-libs/ed27421a96327e7aa2e9825d4df23d84c2eda748'>Commit.</a> </li>
<li>Add dialogbox. <a href='http://commits.kde.org/kdepim-apps-libs/7887f9b3047498f9248eb79e27f8a40c4a132ff3'>Commit.</a> </li>
<li>Define default collection. <a href='http://commits.kde.org/kdepim-apps-libs/c652b89cebb6139dcf107e9dae2b1ddc65cac799'>Commit.</a> </li>
<li>Add enum about current action. <a href='http://commits.kde.org/kdepim-apps-libs/57d21f7388bd9e4b370b8a4a466267f37b4598e4'>Commit.</a> </li>
<li>Fix api, we store QList<QAction *> in interfacd. <a href='http://commits.kde.org/kdepim-apps-libs/29a78f0734b78553212dffded360031ae42bb0a6'>Commit.</a> </li>
<li>Add KAddressBookImportExportContactList. <a href='http://commits.kde.org/kdepim-apps-libs/3c858520876eee4735a7d35aecb8ad675c5f66ab'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-apps-libs/9577ae8c08fa085b7c9a81f189b7a542b292388e'>Commit.</a> </li>
<li>We can have several actions. <a href='http://commits.kde.org/kdepim-apps-libs/5187d72686c4ce0523e3bb3f7500e67ee77e7dfc'>Commit.</a> </li>
<li>Add import/export action. <a href='http://commits.kde.org/kdepim-apps-libs/8992766ded7b2d9fc742dab3b7a4334b6dcf42bf'>Commit.</a> </li>
<li>Fix Install header path. <a href='http://commits.kde.org/kdepim-apps-libs/8ae8e607e7ad97a8d18247d022407d986b963fa9'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-apps-libs/be5bc8337780ed5f5e242403c92123cdbd3fab78'>Commit.</a> </li>
<li>Fix plugin info. <a href='http://commits.kde.org/kdepim-apps-libs/be2939c4050ccf436c3d1002c20e74d03749ebd2'>Commit.</a> </li>
<li>Improve manager. <a href='http://commits.kde.org/kdepim-apps-libs/a3dbc4588bac722342edd5cc1cda23465753dbc1'>Commit.</a> </li>
<li>Add manager. <a href='http://commits.kde.org/kdepim-apps-libs/98c9858814ca5e3eed2a82322ed61d1609d1afa6'>Commit.</a> </li>
<li>Improve lib. <a href='http://commits.kde.org/kdepim-apps-libs/8eeeb0d48015db1c0184bbb7e46dcf4a8e4b1169'>Commit.</a> </li>
<li>Fix install header + soname. <a href='http://commits.kde.org/kdepim-apps-libs/8a323f61f9f9d08cf4b4339825a111d3633323a2'>Commit.</a> </li>
<li>Create lib. <a href='http://commits.kde.org/kdepim-apps-libs/ce7f4f09a2e5d65296cb3782e890bd8f35334ff4'>Commit.</a> </li>
<li>Start to create lib for import/export plugins. <a href='http://commits.kde.org/kdepim-apps-libs/6a74866b315dafaf6df157acdb5d2e96deacad57'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/kdepim-apps-libs/f645aa161e8aacfe7a96b3eea256b80ecabeddd7'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kdepim-apps-libs/2638bcf33d17135bf6d3f4275971603f7a2cab80'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/kdepim-apps-libs/1bbb25876eac0f006758ca0b0d0c981ae669737e'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-apps-libs/8af42949e9d4489874792936091f391de5a86670'>Commit.</a> </li>
<li>Lib prison uses kf5_version now. <a href='http://commits.kde.org/kdepim-apps-libs/56da80d267ebb20dc131be5b1858ffe740f87d23'>Commit.</a> </li>
<li>Port to new connect api. <a href='http://commits.kde.org/kdepim-apps-libs/ced72d381e0a954e7a5758077300e8fcf876343f'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Fix i18n. <a href='http://commits.kde.org/kdepim-runtime/2bd504441d7e24d2a1120d9a6226505d60779538'>Commit.</a> </li>
<li>Avoid to hide label when label uses wrapword settings. <a href='http://commits.kde.org/kdepim-runtime/5aefb383447aaca7525302ec43e8cffc047f806c'>Commit.</a> </li>
<li>Disable remove button when we don't have account. <a href='http://commits.kde.org/kdepim-runtime/324a34abf2a97b3db31b30263b0c58c7a9f17400'>Commit.</a> </li>
<li>It we don't have account clear listbox (for example after removing last. <a href='http://commits.kde.org/kdepim-runtime/b0d10ae718080863c0d1eb42bcab19e25a673dff'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/5f43427b0d75064890576cb3fbb8e86b5831a7db'>Commit.</a> </li>
<li>Fix some clazy warning. <a href='http://commits.kde.org/kdepim-runtime/26a3a299e93a1a1a4fe7f39be74a87ddb154efe4'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/kdepim-runtime/8a47f3d7cf44466e38fe9d01964a356f338bf6ff'>Commit.</a> </li>
<li>Use isEmpty. <a href='http://commits.kde.org/kdepim-runtime/22a3130eba1762fadead33c2067bf9ae25e9e0e2'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/kdepim-runtime/cb6f77ab54b31128908d7a12b29ae5a7ffb75d8f'>Commit.</a> </li>
<li>Use isEmpty. <a href='http://commits.kde.org/kdepim-runtime/e6c9bf03e4bbbc81debf61353ed0501d6ae5de52'>Commit.</a> </li>
<li>Minor. <a href='http://commits.kde.org/kdepim-runtime/bf9ab4022dcdb3648b17e0bc68dcfb22286a0c17'>Commit.</a> </li>
<li>Check if we must remove message on server if we didn't download message. <a href='http://commits.kde.org/kdepim-runtime/f54e84afa9ed217939ba9f99197a6ecc1e8973db'>Commit.</a> </li>
<li>We need to verify message to remove even if we do'nt download message. <a href='http://commits.kde.org/kdepim-runtime/b85ad65bfd594f90164b0cf725764f3049743bc3'>Commit.</a> </li>
<li>Unbreak keeponserver features. <a href='http://commits.kde.org/kdepim-runtime/6695e0ad434a9eccb56002ba269f535dc68ac357'>Commit.</a> </li>
<li>Rename variable. <a href='http://commits.kde.org/kdepim-runtime/c54b9879282456e69bb700d30e0690e7a2cbbf2b'>Commit.</a> </li>
<li>Use clear. <a href='http://commits.kde.org/kdepim-runtime/d3501210460da795b6001612a41634e151c0bafb'>Commit.</a> </li>
<li>Add more debug. <a href='http://commits.kde.org/kdepim-runtime/80b2802d963d241d4ae2d46c8889523588fa0c6d'>Commit.</a> </li>
<li>Const'ify. <a href='http://commits.kde.org/kdepim-runtime/beef1f2b2d817c37e73e0fd83eb01ccd10f65392'>Commit.</a> </li>
<li>USe 0. <a href='http://commits.kde.org/kdepim-runtime/a4b8d5056a2620e6f3324583311bae63c9461a47'>Commit.</a> </li>
<li>Don't use Q_NULLPTR here. <a href='http://commits.kde.org/kdepim-runtime/5b996d074bcb42c4c3e1f32832cbeea9b22b5507'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/kdepim-runtime/148596cad7390a5d1f31d96e354ca44b9e1beadb'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/kdepim-runtime/8b2beb57d79ec2cadeb4bee7499ef805add8ff0c'>Commit.</a> </li>
<li>Rename variable. <a href='http://commits.kde.org/kdepim-runtime/ba09263b1ee07655cc032320794229d84d69db8a'>Commit.</a> </li>
<li>USe new connect api. <a href='http://commits.kde.org/kdepim-runtime/2fb73cc10e809322d0283759b4062f1c0417bd27'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/5b60ead22c05f8b32a3cb66abc89398cf538b8bd'>Commit.</a> </li>
<li>MailDispatcher: honor the new silent flag on SentBehaviourAttribute. <a href='http://commits.kde.org/kdepim-runtime/5f6a1d085e0be03f350812f1ce6e5699645e1e5e'>Commit.</a> </li>
<li>Bug 370627: ignore temporary files created in calendar directory. <a href='http://commits.kde.org/kdepim-runtime/77f30c7471ce4f79e106cc91e8789c8c621fd1b7'>Commit.</a> </li>
<li>Formatting. <a href='http://commits.kde.org/kdepim-runtime/b2e40dd1edc4ab942fd64bc2fd60750959dd65c8'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/3b840b3260599b03b184c1d95c957e6a2bffc8d6'>Commit.</a> </li>
<li>Bug 370627: ignore temporary files created in calendar directory. <a href='http://commits.kde.org/kdepim-runtime/891b304dcf93f7d1b6b7745f2f0ff2b26cd418bb'>Commit.</a> </li>
<li>Use rejected boolean. <a href='http://commits.kde.org/kdepim-runtime/b28a7e83d0f871a2d0c924d132b10132737947bb'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/012feeee26b3f27aa65bd4ebcc55f941546c29e1'>Commit.</a> </li>
<li>Supress -Woverload-virtual warnings due to ResourceBase::retrieveItems(). <a href='http://commits.kde.org/kdepim-runtime/dc5c511f567d5b41d10fb6ea900bdb10c533937e'>Commit.</a> </li>
<li>Port from deprecated KIMAP::SearchJob API to KIMAP::Term API. <a href='http://commits.kde.org/kdepim-runtime/c4df9ce8c50f35b9ad9d5d66709c8a6c2d00a3e0'>Commit.</a> </li>
<li>Use the new KIMAP::MoveJob to move emails when server supports it. <a href='http://commits.kde.org/kdepim-runtime/78ea1208a3f008aca0cf52bc6fefb722547edae2'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/kdepim-runtime/c993c037b3002ecb90fcd33369a87a9a273dd307'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/kdepim-runtime/de8345399b2ff0a52f73e4ef66a088e8b317830b'>Commit.</a> </li>
<li>Remove outdated file. <a href='http://commits.kde.org/kdepim-runtime/c526cbefa724789b95788773c9bb9f6d62315bd7'>Commit.</a> </li>
<li>Time to increase version. <a href='http://commits.kde.org/kdepim-runtime/b4c90e1a42c426f9275b5c3e9b91209073f893bb'>Commit.</a> </li>
<li>SingleFileResource: port away from deprecated KGlobal::ref(). <a href='http://commits.kde.org/kdepim-runtime/a6fdccbfd261e0b5265dc02ebd4581af1dea78ae'>Commit.</a> </li>
<li>Unbreak compile by making the slots protected and using parent class. <a href='http://commits.kde.org/kdepim-runtime/b3cf451d9bc09d249b3dadc87d5cb91b065bdedb'>Commit.</a> </li>
<li>Remove unused variable. <a href='http://commits.kde.org/kdepim-runtime/badc3cd1a201c2f4a01a95b80b4c8da5e67ad30d'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kdepim-runtime/343b47b6f933d9073e5fb18183b1d2081351e94c'>Commit.</a> </li>
<li>Add parents. <a href='http://commits.kde.org/kdepim-runtime/987d7d592c94666c6fbb8f53333620869e9d8dcd'>Commit.</a> </li>
<li>Merge line. <a href='http://commits.kde.org/kdepim-runtime/05b410c8aa451d3bf8aa1f5e2d0a30d766d406a1'>Commit.</a> </li>
<li>Verify that we have engines. <a href='http://commits.kde.org/kdepim-runtime/7321cecac0a2372c140f58f4327fe2068dbc6da6'>Commit.</a> </li>
<li>Fix layout. <a href='http://commits.kde.org/kdepim-runtime/cd78f62ad3344b10dfcf549ba09186502ba7e1f7'>Commit.</a> </li>
<li>Fix i18n. <a href='http://commits.kde.org/kdepim-runtime/7825bcf8cbe925b1597a29532ebca16728728e50'>Commit.</a> </li>
<li>Use Q_SLOTS. <a href='http://commits.kde.org/kdepim-runtime/839b5153b32304a7d1b1935fc81154b2a54e74ab'>Commit.</a> </li>
<li>Time to increase version. <a href='http://commits.kde.org/kdepim-runtime/ad98694721b2c49cc5597d856748d886fa5f6e3c'>Commit.</a> </li>
<li>Port local mail resources to new retrieveItems() API. <a href='http://commits.kde.org/kdepim-runtime/e8a9d532884dc8af703bce75a437e44bc9af24c3'>Commit.</a> </li>
<li>Move qapplication. <a href='http://commits.kde.org/kdepim-runtime/d5cd0a4abd1b95ab4472c4d0f26a9654db0e86b8'>Commit.</a> </li>
<li>Move as q_slots. <a href='http://commits.kde.org/kdepim-runtime/ed4149a4052214e0694eda0fd672b4e4fc8e136b'>Commit.</a> </li>
<li>Remove not necessary private Q_SLOTS. <a href='http://commits.kde.org/kdepim-runtime/afdc8961e6e0a663bad66ad07bd1d07b365509d4'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/b6c5503f4e756375a5cd70589daf26c0d82e8846'>Commit.</a> </li>
<li>Never defined => remove it. <a href='http://commits.kde.org/kdepim-runtime/458bedea009266f6e577e66c0703f31a91748d3e'>Commit.</a> </li>
<li>It's not beta yet in master:). <a href='http://commits.kde.org/kdepim-runtime/3aee1bb4b485c95f17f577dbf57a33f835e9481c'>Commit.</a> </li>
<li>Not necessary to use private Q_SLOTS. <a href='http://commits.kde.org/kdepim-runtime/d40a9294687455c4fc811b04f650d3ec7f0325bf'>Commit.</a> </li>
<li>It's beta1. <a href='http://commits.kde.org/kdepim-runtime/a7a39b9f6f8360b7c1a53fd3298ee05c33bc18c8'>Commit.</a> </li>
<li>5.3.40. <a href='http://commits.kde.org/kdepim-runtime/d04e78c2c0384aca21b8edca346c764b69c058cd'>Commit.</a> </li>
</ul>
<h3><a name='kdialog' href='https://cgit.kde.org/kdialog.git'>kdialog</a> <a href='#kdialog' onclick='toggle("ulkdialog", this)'>[Show]</a></h3>
<ul id='ulkdialog' style='display: none'><li>New in this release</li></ul>
<h3><a name='keditbookmarks' href='https://cgit.kde.org/keditbookmarks.git'>keditbookmarks</a> <a href='#keditbookmarks' onclick='toggle("ulkeditbookmarks", this)'>[Show]</a></h3>
<ul id='ulkeditbookmarks' style='display: none'><li>New in this release</li></ul>
<h3><a name='kfilereplace' href='https://cgit.kde.org/kfilereplace.git'>kfilereplace</a> <a href='#kfilereplace' onclick='toggle("ulkfilereplace", this)'>[Show]</a></h3>
<ul id='ulkfilereplace' style='display: none'><li>New in this release</li></ul>
<h3><a name='kfind' href='https://cgit.kde.org/kfind.git'>kfind</a> <a href='#kfind' onclick='toggle("ulkfind", this)'>[Show]</a></h3>
<ul id='ulkfind' style='display: none'><li>New in this release</li></ul>
<h3><a name='kfourinline' href='https://cgit.kde.org/kfourinline.git'>kfourinline</a> <a href='#kfourinline' onclick='toggle("ulkfourinline", this)'>[Show]</a></h3>
<ul id='ulkfourinline' style='display: none'>
<li>Use non privileged ports for network games. <a href='http://commits.kde.org/kfourinline/1c50e784a44841d431f317de87ff559b694bf07c'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Show]</a></h3>
<ul id='ulkgpg' style='display: none'>
<li>Increase KF5 requirement. <a href='http://commits.kde.org/kgpg/fee6927a7396bbf1a6a8d1ad04f3fe6a81881d08'>Commit.</a> </li>
<li>Fix crash when GnuPG does not output curve fields for keys, take 2. <a href='http://commits.kde.org/kgpg/86b868474fb9b9eb1bf7092bc5cc7aae03f4090e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373408'>#373408</a>. See bug <a href='https://bugs.kde.org/371410'>#371410</a></li>
<li>Use new slot syntax for KStandardAction connections. <a href='http://commits.kde.org/kgpg/be2ea5a6415eeb87a715fe530770e03ffd1dae12'>Commit.</a> </li>
<li>Fix crash when GnuPG does not output curve fields for keys. <a href='http://commits.kde.org/kgpg/9f742e350e87618400a815e77b91fddb4220d9b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371410'>#371410</a></li>
<li>Remove write-only variable. <a href='http://commits.kde.org/kgpg/6ddd7920f06d2c8c9d584c5c024032cb1e5b1362'>Commit.</a> </li>
<li>Fix overlaping UI elements in key generate dialog. <a href='http://commits.kde.org/kgpg/d88076fd00e0e3eb1789c1ce2ae1bc843d37ef6a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129269'>#129269</a></li>
<li>Fix doc typo (name of the released bundle). <a href='http://commits.kde.org/kgpg/9450b9361249c6731e79cf009510686f3734f28c'>Commit.</a> </li>
<li>Get rid of KGpgItemModel::refreshKeyIds(). <a href='http://commits.kde.org/kgpg/d7f69b3df30b6b42995ef9b30a33496eb8e5c886'>Commit.</a> </li>
<li>Introduce KGpgItemModel::refreshAllKeys(). <a href='http://commits.kde.org/kgpg/4ce59c06857aeb40dbb1b2ea1bbc0479797d12af'>Commit.</a> </li>
<li>Remove KeysManager::refreshKeys(). <a href='http://commits.kde.org/kgpg/c736296b6d5709e7652a10babfa1ec5f4f55c025'>Commit.</a> </li>
<li>Only refresh keys after importing if at least one key was received. <a href='http://commits.kde.org/kgpg/f8d02bcb5768d58686110689030686afbc496c4e'>Commit.</a> </li>
<li>Only refresh keys after signing if at least one key was signed. <a href='http://commits.kde.org/kgpg/a30d9bd811a32bdb7d2d74774479fd511e425169'>Commit.</a> </li>
<li>Add documentation for KeyServer::importFinished(). <a href='http://commits.kde.org/kgpg/bd77e34edf0c05dbb425ff180135440e4bb6d691'>Commit.</a> </li>
<li>Make KGpgItemModel::refreshKey() implementations inline. <a href='http://commits.kde.org/kgpg/a3bc809c075c2d897f6ece5c13ed3f427bbe3b14'>Commit.</a> </li>
<li>Document the input expected by KGpgRootNode::addGroups(). <a href='http://commits.kde.org/kgpg/d73ddd585d42fdced964197e910b8e3bce2cab06'>Commit.</a> </li>
<li>Also refresh groups when refreshing all keys. <a href='http://commits.kde.org/kgpg/afe68e81f5c2d4a625f615b7e36457d37f811061'>Commit.</a> </li>
<li>Fix inconsistent interface specification of KGpgItemModel::changeGroup(). <a href='http://commits.kde.org/kgpg/55d77a14fedf9545a1f8792288fb922006acb404'>Commit.</a> </li>
<li>Use relay-only slots, directly connect to signal. <a href='http://commits.kde.org/kgpg/a28f31252aea596fce2ace75be73f68f9e98d715'>Commit.</a> </li>
<li>Use --list-config to request certain information from GnuPG. <a href='http://commits.kde.org/kgpg/d4916218686561997c29b75499a1cc20113750d8'>Commit.</a> </li>
<li>Tolerate group member ids to begin with 0x. <a href='http://commits.kde.org/kgpg/f33f8c759a13da6ab663bd9fd1baef17a238836d'>Commit.</a> See bug <a href='https://bugs.kde.org/371004'>#371004</a></li>
<li>Fix wrong sender checks. <a href='http://commits.kde.org/kgpg/4bf291cdbc7081cc8af728cda9653ea57edc38dd'>Commit.</a> </li>
<li>Use the Convert::toAlgo() overload that directly takes a QString. <a href='http://commits.kde.org/kgpg/a473a43dd26a10ef49f7624c5bce2ea0665af250'>Commit.</a> </li>
<li>Proofread + update KGpg docbook to kf5. <a href='http://commits.kde.org/kgpg/acb9c2fb93ebb5797d314301266e735c9ba4ea79'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129165'>#129165</a></li>
<li>Add missing Q_DECL_OVERRIDE keywords. <a href='http://commits.kde.org/kgpg/ba212ed86d563c900bb77c4fe39839ae04afebab'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129175'>#129175</a></li>
<li>Fix doxygen comments. <a href='http://commits.kde.org/kgpg/0a18c579b5f5a6af916f1db332ccd5fb894da996'>Commit.</a> </li>
<li>SlotGenerateKey: correctly invoke GPG for interactive key generation. <a href='http://commits.kde.org/kgpg/af3b40cbb204b69f30b33777b10606a6814276c6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369274'>#369274</a></li>
<li>Drop implemented line from TODO. <a href='http://commits.kde.org/kgpg/e1743170d44157424df0f29f7df89cf5411a6cb5'>Commit.</a> </li>
<li>Use isEmpty() instead of comparing count() to 0. <a href='http://commits.kde.org/kgpg/4349d04c305e81798eaeaa90a25c16cd9e7c90d0'>Commit.</a> </li>
<li>Rework the tracking if KGpgRootNode is currently deleting. <a href='http://commits.kde.org/kgpg/0499dca3914633d1ece028a187a1c8fa22d9422d'>Commit.</a> </li>
<li>Remove checks for invalid index in KGpgExpandableNode::getChild(). <a href='http://commits.kde.org/kgpg/db3f6c49e6830de3726415a7ff02bd50dca54171'>Commit.</a> </li>
<li>Drop needless friend declaration, KGpgSubkeyNode is a subclass. <a href='http://commits.kde.org/kgpg/b726585ae2efe39b4497842638062e64f2d6046c'>Commit.</a> </li>
<li>Kill kgpgcompiler.h. <a href='http://commits.kde.org/kgpg/986a76252f1f194f9cdeb7ac7b392accca8dfa73'>Commit.</a> </li>
<li>Port away from deprecated QWeakPointer usage. <a href='http://commits.kde.org/kgpg/40d33f67dfb6e34ccb7cfc984f915b82a6b0fc9b'>Commit.</a> </li>
<li>Put the KGpgApp object on the stack. <a href='http://commits.kde.org/kgpg/56b72f620c07d53be39fd1066a456e6be0677d2e'>Commit.</a> </li>
<li>Replace usage of QString::toAscii() with QString::toLatin1(). <a href='http://commits.kde.org/kgpg/01fe91f537380c7eed85a874dfa7ee52ef48c3bb'>Commit.</a> </li>
<li>Fix compilation with older versions of gpgme. <a href='http://commits.kde.org/kgpg/65514177e6b9d096b5f6a822a1699e474d791f03'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368462'>#368462</a></li>
<li>Fix broken HTML markup. <a href='http://commits.kde.org/kgpg/2162348b57df1b342c29d264223a5aef4f87d882'>Commit.</a> </li>
<li>Replace 'size' display with 'strength' display. <a href='http://commits.kde.org/kgpg/be42142a5adbc1271a77ada4d2c3efdb64b56a25'>Commit.</a> </li>
<li>Implement recognition of new ECC key types. <a href='http://commits.kde.org/kgpg/2c59f95f1d1ed64d61ad7c8572444d04ea875944'>Commit.</a> </li>
<li>Do not use obsolete Qt::WFlags. <a href='http://commits.kde.org/kgpg/abf0d45b16f6c98a2b068ae3749b3629bab33b62'>Commit.</a> </li>
<li>Do not use obsolete QChar::toAscii(). <a href='http://commits.kde.org/kgpg/acfab9eab78c9c7d6e92736bc0d45c58de556d38'>Commit.</a> </li>
<li>Drop KDELibs4Support. <a href='http://commits.kde.org/kgpg/a81ba38a2f0eaaf94eaa891f70f37795b4fda468'>Commit.</a> </li>
<li>Port away from kDebug(). <a href='http://commits.kde.org/kgpg/7aaf8b4b99a7b39214b12ad4cacbf8ae058bc73d'>Commit.</a> </li>
<li>Port away from most cases of Q_FOREACH to C++11 ranged for loop. <a href='http://commits.kde.org/kgpg/6e2654585568ec9528008022d83669996b6676c2'>Commit.</a> </li>
<li>Fix return type to avoid compiler warning. <a href='http://commits.kde.org/kgpg/aa39c9fdecec09d621f5ee38f48b71c8421aa74f'>Commit.</a> </li>
<li>Fix return type to avoid compiler warning. <a href='http://commits.kde.org/kgpg/58ca0e26d0aefe930fdfb5fe967461fbbd9ebfb9'>Commit.</a> </li>
<li>Add return values that should never be needed. <a href='http://commits.kde.org/kgpg/9512b7f821296468004dee092a1c5038e7ee4ad1'>Commit.</a> </li>
<li>Show only valid keys in search results. <a href='http://commits.kde.org/kgpg/95c6901392c17ef174a63188447f58d02338aeef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/254779'>#254779</a>. Code review <a href='https://git.reviewboard.kde.org/r/128701'>#128701</a></li>
<li>Encapsulate QModelIndex operations in search result model. <a href='http://commits.kde.org/kgpg/0779334e4cb13bed796b8f3c22617179972d6105'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128701'>#128701</a></li>
<li>Fix a crash on closing KGpgTextEditor window. <a href='http://commits.kde.org/kgpg/6d329a53390510b19695dd1f3e69349678078d98'>Commit.</a> </li>
<li>Convert some strings to KUIT semantic markup. <a href='http://commits.kde.org/kgpg/161729d3ade253e3657f64e20982e02443a5a479'>Commit.</a> </li>
<li>Bump version number for post Applications 16.08 development. <a href='http://commits.kde.org/kgpg/a9972483ad37636d4108a93f78986e69998338a6'>Commit.</a> </li>
<li>KTextEditor is actually not used. <a href='http://commits.kde.org/kgpg/2424d7cf1e16c726401c4d4ec8621656d1468674'>Commit.</a> </li>
<li>Show warning before deleting photo. <a href='http://commits.kde.org/kgpg/07f5a1b63b985d801342732d89fffb5948c9af4a'>Commit.</a> </li>
<li>Add HiDPI support. <a href='http://commits.kde.org/kgpg/b5778e9b0e93080e0cfa54dcaeb5ed7119bce0c2'>Commit.</a> </li>
<li>Add KCrash support. <a href='http://commits.kde.org/kgpg/05cf553058c8bdb1a5a8e87b765be43bae00aa05'>Commit.</a> </li>
<li>Port away from KStandardDirs::makeDir. <a href='http://commits.kde.org/kgpg/be86ce09418e86c91561c9ede97b6837391b98d6'>Commit.</a> </li>
<li>Port away from KIO::NetAccess. <a href='http://commits.kde.org/kgpg/0b5be818a994f09f9ee8b06be49542e23f45598e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128449'>#128449</a></li>
<li>Port away from KDateTime. <a href='http://commits.kde.org/kgpg/e0f2e9ee8cd7de6edc145104aeffab3f910f85d6'>Commit.</a> </li>
<li>Port away from KFileDialog. <a href='http://commits.kde.org/kgpg/f3ba1367e273974d553b4b87628f0ec1dc68dd2e'>Commit.</a> </li>
<li>Also link to KF5::Notifications, KF5::WindowSystems and Qt5::PrintSupport. <a href='http://commits.kde.org/kgpg/7070b23b6cce33594930ba208dcc6b580ea3b782'>Commit.</a> </li>
<li>Remove unnecessary include. <a href='http://commits.kde.org/kgpg/24a674c1ed02ab8de74dca4809c95aedf03c7de9'>Commit.</a> </li>
<li>KLocale->KLocalizedString. <a href='http://commits.kde.org/kgpg/9c273f8900dc65536457c8f7b70150406ef5867c'>Commit.</a> </li>
<li>Convert one more forgotten kapp to qApp. <a href='http://commits.kde.org/kgpg/4822b13b9cf04160156921e81611cbb01695b9c9'>Commit.</a> </li>
<li>Link to KDE Frameworks that we use. <a href='http://commits.kde.org/kgpg/0c67b9fd5157d35a12ddc44eb13a7e53758dceb0'>Commit.</a> </li>
<li>Migrate kde4 config files. <a href='http://commits.kde.org/kgpg/3319930c6b50f6d874cde9ddfeb2b7341175fb05'>Commit.</a> </li>
<li>Port away from K4AboutData. <a href='http://commits.kde.org/kgpg/f8f98b742006b77c5536251f5912d9d3042e0491'>Commit.</a> </li>
<li>Port away from KUniqueApplication to QApplication + KDBusService. <a href='http://commits.kde.org/kgpg/e14b55e783d8f86bc388be7837b0d9b491b52728'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128433'>#128433</a></li>
<li>Remove write-only member KGpgExternalActions::clipboardMode. <a href='http://commits.kde.org/kgpg/dc1008fe0439c6fd607ac51e43591e1f8ba889a8'>Commit.</a> </li>
<li>Fix include style. <a href='http://commits.kde.org/kgpg/7b857a52cd8ad18a14e3be263aeb0b33f967544d'>Commit.</a> </li>
<li>Renovate about data. <a href='http://commits.kde.org/kgpg/8fddba77f97a7fa07bad7f46aeb415521512fb4c'>Commit.</a> </li>
<li>Simplify setting KGPG_DEBUG_TRANSACTIONS compiler define. <a href='http://commits.kde.org/kgpg/ab594f86a7d5b5f93df52af2d25f36610f2ed880'>Commit.</a> </li>
<li>Set version number in CMakeLists.txt. <a href='http://commits.kde.org/kgpg/e4d2bc48ffe5c28f16f98305b7aaf566321aa4d1'>Commit.</a> </li>
<li>Fix tips installation folder. <a href='http://commits.kde.org/kgpg/b2cddf2058a7eb51f18ca3f88d337c5e487f5fb5'>Commit.</a> </li>
<li>Use new slot syntax for KStandardAction connections. <a href='http://commits.kde.org/kgpg/69a318b08109c37c78ba2cd2a68d5b5769bd2510'>Commit.</a> </li>
<li>Revert "Fix detailed console dialog layout.". <a href='http://commits.kde.org/kgpg/1316fea29ede941297b4b7659245ea3f437905cf'>Commit.</a> </li>
<li>Fix detailed console dialog layout. <a href='http://commits.kde.org/kgpg/3604dfa93e5b02db4d72cf4628f1c870bda742be'>Commit.</a> </li>
<li>Fix broken connection. <a href='http://commits.kde.org/kgpg/044c49a47a1cdc4409e57c29311249c83d3cf98e'>Commit.</a> </li>
<li>Fix appdata file name and destination variable. <a href='http://commits.kde.org/kgpg/a624b3a3dd93581b78b23332181592d87c0015e1'>Commit.</a> </li>
<li>Reenable double clicking on the public or private key to choose it. <a href='http://commits.kde.org/kgpg/c10b5898818c6ee818262ced720783d65a759494'>Commit.</a> </li>
<li>Tell DBus that this application can only be started once. <a href='http://commits.kde.org/kgpg/176adf8cd15d99cb7815b9a4fcdc3d8fcdf1ab53'>Commit.</a> </li>
<li>Caff: fix mail body showing up in the subject. <a href='http://commits.kde.org/kgpg/c8a90a6342e465351514d7e2b3f820f655191946'>Commit.</a> </li>
<li>Remove kgpgtransactionprivate.h from public header. <a href='http://commits.kde.org/kgpg/4b93ae4987f3be7b5302f4e0165a75d66c06c974'>Commit.</a> </li>
<li>Fix Q_ASSERTs. <a href='http://commits.kde.org/kgpg/66474c6460da5e56a7c9ebcea3e54bb874c1aa61'>Commit.</a> </li>
<li>KgpgDetailedInfo: bring back the "Details" button. <a href='http://commits.kde.org/kgpg/d8887e0931d75dc635e32ee664d463007ebe409a'>Commit.</a> </li>
<li>Detailedconsole: fix dialog layout. <a href='http://commits.kde.org/kgpg/abe6cabe39f36ee56484ceb224f59bc12c962c99'>Commit.</a> </li>
<li>Fix disconnecting KGpgRefNode::keyUpdate(). <a href='http://commits.kde.org/kgpg/e4007099c8090c79a87c3277872bf19033781054'>Commit.</a> </li>
<li>Some random cleanups. <a href='http://commits.kde.org/kgpg/b653010177870f19cbc1fada8c263b1a82912226'>Commit.</a> </li>
<li>Install recource files into KXMLGUI_INSTALL_DIR. <a href='http://commits.kde.org/kgpg/6d4973f9e7ec442564af510b968cd513af4419af'>Commit.</a> </li>
<li>Port to new qt5 slot signal syntax. <a href='http://commits.kde.org/kgpg/32aa5911623222455e2d1fd4e8a20b41537a4c17'>Commit.</a> </li>
<li>Let Qt handle deletion of QVBoxLayout pointers. <a href='http://commits.kde.org/kgpg/34082236e764b7f7cca34d84a2b91ef7bd79a5cb'>Commit.</a> </li>
<li>Port KeyServer from KDialog to QDialog. <a href='http://commits.kde.org/kgpg/8cb97c14e7984ea4401d6c055e18e44862f3ced1'>Commit.</a> </li>
<li>Port away from KComboBox to QComboBox. <a href='http://commits.kde.org/kgpg/ec44b760a7a548a3ea4e8e2c95ec9bb0bfd0d832'>Commit.</a> </li>
<li>Port KDialogs in KeysManager to QDialogs. <a href='http://commits.kde.org/kgpg/6abc51220ce7894afaf98bfafff788d5aaac7293'>Commit.</a> </li>
<li>Remove unnecessary includes from keyexport.cpp. <a href='http://commits.kde.org/kgpg/9764a4ab83c0003deb6140e9d088883c29973560'>Commit.</a> </li>
<li>Port KeyExport dialog to QDialog. <a href='http://commits.kde.org/kgpg/a0ac6de644a09c6cbc533ccfe2e20a57c3e7ecba'>Commit.</a> </li>
<li>Let Qt handle QNetworkConfigurationManager deletion. <a href='http://commits.kde.org/kgpg/e924f463845c7a2afb06f63f70785b4e0a7eb582'>Commit.</a> </li>
<li>Port to QNetworkConfigurationManager. <a href='http://commits.kde.org/kgpg/ce73e76861579826d2dc1f132fe548ffe1d1ff7d'>Commit.</a> </li>
<li>Port away from KGlobalSettings::fixedFont(). <a href='http://commits.kde.org/kgpg/3c117f274438fe2a33306d7d13316d535d93f174'>Commit.</a> </li>
<li>Finish port to QStatusBar. <a href='http://commits.kde.org/kgpg/42d05fedc25cbdf666b2f9bfc34d7b591c969474'>Commit.</a> </li>
<li>Finish shortcut port in KgpgSelectPublicKeyDlg. <a href='http://commits.kde.org/kgpg/60a9bbf891945a21d7a947cbc50ddc28b069aa24'>Commit.</a> </li>
<li>Port away from KToolInvocation::invokeBrowser(). <a href='http://commits.kde.org/kgpg/7c5fdc1b0ba00e384e59fa602a636d89f3fa3f5b'>Commit.</a> </li>
<li>Port remaining KHBoxes. <a href='http://commits.kde.org/kgpg/1b17286845a62e6f30f7f8324ebe8d2802aa9f97'>Commit.</a> </li>
<li>Port away from KLocale. <a href='http://commits.kde.org/kgpg/7889cdc8664e3d39fea7e779ad4b57d03f39f40a'>Commit.</a> </li>
<li>Port away from KPushButton. <a href='http://commits.kde.org/kgpg/4a75c3beb64112446c76f5c79bb6b0b9e241aeb4'>Commit.</a> </li>
<li>Fix ecm_install_icons and ecm_add_app_icon. <a href='http://commits.kde.org/kgpg/e85b266afa328d73a4abccf1865be4b959a76fe9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128111'>#128111</a></li>
<li>Port away from KInputDialog. <a href='http://commits.kde.org/kgpg/effc152bf737ff2a9aa47b5151b8c0c42d9aa218'>Commit.</a> </li>
<li>Port away from void KCoreConfigSkeleton::writeConfig(). <a href='http://commits.kde.org/kgpg/d4496b92f25889fd302066b19e76221295469fe8'>Commit.</a> </li>
<li>Port away from KToolInvocation::invokeMailer(). <a href='http://commits.kde.org/kgpg/2f9ec03fdb60244cbb33aa479032300d1b45c885'>Commit.</a> </li>
<li>Fix runtime warnings about QAction::setShortcut() being called directly. <a href='http://commits.kde.org/kgpg/54f4560964c62d309f91a8ba010839c8efc5932c'>Commit.</a> </li>
<li>Convert some KDialogs to QDialogs. <a href='http://commits.kde.org/kgpg/e438bf9b7ab5eb50633898a5c60059e4d2de2c6a'>Commit.</a> </li>
<li>Port away from KUrl to QUrl. <a href='http://commits.kde.org/kgpg/76e83d3be2a3bbe4c7c26f2aecb8c8448866eae5'>Commit.</a> </li>
<li>Remove most code from kgpgcompiler.h. <a href='http://commits.kde.org/kgpg/bb34b3d4ee6a64a35078bb965917c91343711d3d'>Commit.</a> </li>
<li>Cleanup .ui files after QTabWidget port. <a href='http://commits.kde.org/kgpg/4e4a8cf0447b6756d5f2981775378e5e60ff764f'>Commit.</a> </li>
<li>Port from KStandardDirs to QStandardPath. <a href='http://commits.kde.org/kgpg/869646ecb4eb74ca063f9f5401785a07849869fd'>Commit.</a> </li>
<li>KTabWidget->QTabWidget. <a href='http://commits.kde.org/kgpg/4e1e27a2fced2562390a1aa25548eead0769aef0'>Commit.</a> </li>
<li>Add cmake feature summary. <a href='http://commits.kde.org/kgpg/474309b4f0d44f1923df500893f28fe5e89bcebe'>Commit.</a> </li>
<li>Port away from KProgressDialog to QProgressDialog. <a href='http://commits.kde.org/kgpg/b2a69979a3e02bb7525d8fef27be38efa426fc4c'>Commit.</a> </li>
<li>KTemporaryFile->QTemporaryFile. <a href='http://commits.kde.org/kgpg/2490f061b9c5d954ab69e146bf43ab72abdb3ee0'>Commit.</a> </li>
<li>Remove some of the FIXME: KF5. <a href='http://commits.kde.org/kgpg/6d730b31c8f683fba0f705fc1c12f0a85e9296c0'>Commit.</a> </li>
<li>Port away from KTempDir to QTemporaryDir. <a href='http://commits.kde.org/kgpg/13223a903db7f1a7277b4883535d787b553b536e'>Commit.</a> </li>
<li>Port away from KMimeType. <a href='http://commits.kde.org/kgpg/23ab7fba0f49413fceb2717b11d016ef768ed174'>Commit.</a> </li>
<li>Cleanup some unused includes. <a href='http://commits.kde.org/kgpg/208f5d07d46d9d24e8306b175d8b6fba30f5ff89'>Commit.</a> </li>
<li>KLineEdit->QLineEdit. <a href='http://commits.kde.org/kgpg/e1d5a35feb0548ffef76fdac8cb2649b62347069'>Commit.</a> </li>
<li>Port away from KIcon. <a href='http://commits.kde.org/kgpg/669138638d0cb7faf023e338a08a43b58815e2e9'>Commit.</a> </li>
<li>Port away from KShortcut. <a href='http://commits.kde.org/kgpg/2ce8b49df16809eab683bddf444483b40423d7b5'>Commit.</a> </li>
<li>Add QMenuBar include. <a href='http://commits.kde.org/kgpg/1b212ef9c0a90638e380c99c10ca796f46c05f54'>Commit.</a> </li>
<li>Port KMenu to QMenu. <a href='http://commits.kde.org/kgpg/b48925552159bc818e9cfcefea3ff7833577c2cb'>Commit.</a> </li>
<li>Move docbook to 4.5. <a href='http://commits.kde.org/kgpg/d7aaa221b4faf99cb88c109de11618c141642ffc'>Commit.</a> </li>
<li>Fix search widget. <a href='http://commits.kde.org/kgpg/78a6b3e302fe634a72b3ef2f06872dd5dc54d372'>Commit.</a> </li>
<li>Add missing newline add end of file. <a href='http://commits.kde.org/kgpg/b8099504d3b340e487ffd1f1f709c32965761c42'>Commit.</a> </li>
<li>Drop CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS code. <a href='http://commits.kde.org/kgpg/9f47c3acaaa6eeed02246b7e3d15c306f86384c9'>Commit.</a> </li>
<li>Port away from KMD5. <a href='http://commits.kde.org/kgpg/7232885a3b8b7d3d1510641568b79754a7f14281'>Commit.</a> </li>
<li>Fix a typo. <a href='http://commits.kde.org/kgpg/82a51d27bf510fa0663c0a769001b9b38b26e0ab'>Commit.</a> </li>
<li>Use reverse DNS for desktop file. <a href='http://commits.kde.org/kgpg/c9716db904166176bf2e1ed4cddae2eece52e04c'>Commit.</a> </li>
<li>Uncomment more stuff in CMakeLists.txt. <a href='http://commits.kde.org/kgpg/3a7c979c835217d4a785487902c26997863c03ef'>Commit.</a> </li>
<li>Reenable building docs. <a href='http://commits.kde.org/kgpg/d4a39802519b63aefbffad7139f5c09a75490a21'>Commit.</a> </li>
<li>Commit files forgotten in the last commit. <a href='http://commits.kde.org/kgpg/5ea30d21efc2339406759e9938a8676271a4c055'>Commit.</a> </li>
<li>Inital port to frameworks. <a href='http://commits.kde.org/kgpg/0744416d4e712b7e3281e04952db18059759d09d'>Commit.</a> </li>
</ul>
<h3><a name='khangman' href='https://cgit.kde.org/khangman.git'>khangman</a> <a href='#khangman' onclick='toggle("ulkhangman", this)'>[Show]</a></h3>
<ul id='ulkhangman' style='display: none'>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/khangman/5f334769da80f100e3d6a2ad494c84bbf25e9f27'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/khangman/5c1135df27fac2c0906c3cf33e89f97e5b293d93'>Commit.</a> </li>
</ul>
<h3><a name='khelpcenter' href='https://cgit.kde.org/khelpcenter.git'>khelpcenter</a> <a href='#khelpcenter' onclick='toggle("ulkhelpcenter", this)'>[Show]</a></h3>
<ul id='ulkhelpcenter' style='display: none'>
<li>Bump version number. <a href='http://commits.kde.org/khelpcenter/8e69f9f0045bd9f1d5dbe3a2c22d62b2b1120b88'>Commit.</a> </li>
<li>Show part of the document ID for the pages found. <a href='http://commits.kde.org/khelpcenter/5c9ffc89f39b855432d3e6872d51d6ced0ca2bff'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129364'>#129364</a></li>
<li>Remove boolean search option, not implemented. <a href='http://commits.kde.org/khelpcenter/b86eaf776cede15455ceff7d980baf8c463902ff'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129164'>#129164</a></li>
<li>Glossary.cpp - fix misleading-indentation warning. add braces for clarity. <a href='http://commits.kde.org/khelpcenter/67037954a00dc24d3123b0335556e57fd7b7d23f'>Commit.</a> </li>
<li>Fix more wrong links. <a href='http://commits.kde.org/khelpcenter/6ef46204dbac1a8a41ff0223d9a001a6f82f7cce'>Commit.</a> </li>
<li>Fix wrong link, add some FIXME about checksum tab. <a href='http://commits.kde.org/khelpcenter/7c7a137448606d6ec1bf9d375b9f8131912e6e5f'>Commit.</a> </li>
<li>Fix minor typos. <a href='http://commits.kde.org/khelpcenter/5591fc9136c34a4724817616cd7316c5dde0842d'>Commit.</a> </li>
<li>Update Glossary docbooks. <a href='http://commits.kde.org/khelpcenter/8d24e662a4deeeed1cfeb9b2a5d07e83013daa73'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128705'>#128705</a></li>
</ul>
<h3><a name='kholidays' href='https://cgit.kde.org/kholidays.git'>kholidays</a> <a href='#kholidays' onclick='toggle("ulkholidays", this)'>[Show]</a></h3>
<ul id='ulkholidays' style='display: none'>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kholidays/6a4f277b0f26a85729de7aab263de32219b5de54'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/kholidays/6095de1c4ebd9e05dd8c9ab1a8138a2cceb88533'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/kholidays/c72cfc073dfa701fd2f049079f633dbc179d9bb5'>Commit.</a> </li>
</ul>
<h3><a name='kidentitymanagement' href='https://cgit.kde.org/kidentitymanagement.git'>kidentitymanagement</a> <a href='#kidentitymanagement' onclick='toggle("ulkidentitymanagement", this)'>[Show]</a></h3>
<ul id='ulkidentitymanagement' style='display: none'>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kidentitymanagement/1bf03d7237525bc3a57f3a67d16520a5ea9754ad'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/kidentitymanagement/969decf04244881af9dbfc9c925891e14ff0181a'>Commit.</a> </li>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kidentitymanagement/f7cd5ce8d20c944600d35859f9cdca2d56f775e2'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kidentitymanagement/8365605e941a78b63cc63a31a611ba59987eed20'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kidentitymanagement/fa8c6a544a5e0401e65d3f96e5e5eabf11ae615e'>Commit.</a> </li>
<li>Extend IdentityTest and fix all bugs it discovered. <a href='http://commits.kde.org/kidentitymanagement/2c4cf6d53c4e86d26b5409c137c6caab87c4d426'>Commit.</a> </li>
<li>Add per-identity auto-encrypt option. <a href='http://commits.kde.org/kidentitymanagement/5fc9962412a0f3d7475dde4b99391cef60a5f44f'>Commit.</a> </li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Show]</a></h3>
<ul id='ulkig' style='display: none'>
<li>Fix minor typo. <a href='http://commits.kde.org/kig/23b945164a3da1dd2e256ef9bceea4f02bdfa148'>Commit.</a> </li>
<li>Use the right ECM variable for the appstream dir. <a href='http://commits.kde.org/kig/61bf6b52a3c32ecb1c760f3bd9ed4e7bc023251c'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kig/1f2d5ab741f88dd405d3c69cc54d9e62560f0636'>Commit.</a> </li>
</ul>
<h3><a name='kimagemapeditor' href='https://cgit.kde.org/kimagemapeditor.git'>kimagemapeditor</a> <a href='#kimagemapeditor' onclick='toggle("ulkimagemapeditor", this)'>[Show]</a></h3>
<ul id='ulkimagemapeditor' style='display: none'><li>New in this release</li></ul>
<h3><a name='kimap' href='https://cgit.kde.org/kimap.git'>kimap</a> <a href='#kimap' onclick='toggle("ulkimap", this)'>[Show]</a></h3>
<ul id='ulkimap' style='display: none'>
<li>Fix some clazy warning. <a href='http://commits.kde.org/kimap/b73971f0955d09ab9893b9d7eb78e70b5f13c80d'>Commit.</a> </li>
<li>Introduce MoveJob which implements the MOVE extension (RFC6851). <a href='http://commits.kde.org/kimap/fa93d5df53c33b3c579a49424e58e191a30f16b0'>Commit.</a> </li>
<li>Fix kimap using only tls1.0. <a href='http://commits.kde.org/kimap/38b75709018699cd4de33ba48265f16d0125ce97'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129030'>#129030</a></li>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kimap/baae8a611f97eb4006f4aa91eb27a5a28c342b49'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kimap/f6b9224d12b8b29c9514b3bfca3650891dee6216'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/kimap/8097dc5ad5dca9bf93671c5283abdb7a8d903959'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/kimap/01464d191e431d51b5de097068e21f0b725bccbb'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Show]</a></h3>
<ul id='ulkio-extras' style='display: none'>
<li>[filenamesearch] Fix huge ram usage in kded module. <a href='http://commits.kde.org/kio-extras/4a1c479b24f1fdc17e96326be43d0398e610b16b'>Commit.</a> </li>
<li>We actually need 5.7 of QWebEnginePage. <a href='http://commits.kde.org/kio-extras/a00069f7d18562f20f5033bcc96e4537cf92eae1'>Commit.</a> </li>
<li>Remove trash kcm docbook from kio-extras. <a href='http://commits.kde.org/kio-extras/f7a6bc8721c90e992e7535436929cd9580d46efc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129377'>#129377</a></li>
<li>[htmlcore] Port to QtWebEngine. <a href='http://commits.kde.org/kio-extras/6dccc7739ba3d754b9209674a8985ce9d9546c2c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129348'>#129348</a></li>
<li>Kio_mtp: improve warning message. <a href='http://commits.kde.org/kio-extras/e5619efafdd9c316e6d3e609ddd73939dea3e270'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128793'>#128793</a></li>
<li>Readd X-DocPath again, we have an ioslave docbook. <a href='http://commits.kde.org/kio-extras/31bbc44bc51b6f1ee717eaa6ff22b4c7cfcc9dea'>Commit.</a> </li>
<li>Add docbook for recentdocuments ioslave. <a href='http://commits.kde.org/kio-extras/b4f67400793f596b3c55983fd733180dc6773a3e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129198'>#129198</a>. Fixes bug <a href='https://bugs.kde.org/370573'>#370573</a></li>
<li>Add the updated (and correct) mimetype names for comicbook archives. <a href='http://commits.kde.org/kio-extras/887b8e9a73d7373ef169497179dd1e9db58ca37e'>Commit.</a> </li>
<li>Comment out some ancient debug output from the thumbnail protocol handler. <a href='http://commits.kde.org/kio-extras/6ea0ad9aaccdec0c49bc2adb39462b3adf972546'>Commit.</a> </li>
<li>Remove DocPath entry from recentdocuments.protocol file. <a href='http://commits.kde.org/kio-extras/43c912d2067316f8d9329ed07972b15f0f94a7fe'>Commit.</a> See bug <a href='https://bugs.kde.org/370573'>#370573</a></li>
<li>Replace with KF5 equivalents. <a href='http://commits.kde.org/kio-extras/c643baf011bf377201db3c5da95fba53ef394bc5'>Commit.</a> </li>
<li>Fix suspicious condition (assignment + comparison). <a href='http://commits.kde.org/kio-extras/c7049eb74086986d7b59dedefc7036ce9cc7440b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366763'>#366763</a></li>
</ul>
<h3><a name='kjumpingcube' href='https://cgit.kde.org/kjumpingcube.git'>kjumpingcube</a> <a href='#kjumpingcube' onclick='toggle("ulkjumpingcube", this)'>[Show]</a></h3>
<ul id='ulkjumpingcube' style='display: none'>
<li>Use KStandardGuiItem::overwrite(). <a href='http://commits.kde.org/kjumpingcube/a2c25dbc83f386a2be330c3db29fadbed8735dec'>Commit.</a> </li>
<li>Remove extra =. <a href='http://commits.kde.org/kjumpingcube/e351665334b69361f1ed1d59be8cfe908efefd7f'>Commit.</a> </li>
<li>Add minimal appdata file for kjumpingcube. <a href='http://commits.kde.org/kjumpingcube/3db2b4b7c37d147f3e6430d895c6a3e93d1cf44b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128410'>#128410</a></li>
</ul>
<h3><a name='kldap' href='https://cgit.kde.org/kldap.git'>kldap</a> <a href='#kldap' onclick='toggle("ulkldap", this)'>[Show]</a></h3>
<ul id='ulkldap' style='display: none'>
<li>Rename categories. <a href='http://commits.kde.org/kldap/1698a6942f0db2842bdfa82523d3d50458edafdb'>Commit.</a> </li>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kldap/e4f6586096924e15328a3183eda9dc442ba97554'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kldap/63170ee6999719270929f629ede338ae5b4c971e'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/kldap/5b52493222ac752a683562838abbd5bd10185b56'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/kldap/6423231690265e5859b128a5dfac69ac8f1300b9'>Commit.</a> </li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Show]</a></h3>
<ul id='ulkleopatra' style='display: none'>
<li>Fix key generation. <a href='http://commits.kde.org/kleopatra/78c63320d294e640e1518daf9046a11b8e289887'>Commit.</a> </li>
<li>Fix some clazy warning. <a href='http://commits.kde.org/kleopatra/8737ae1c67ef13d9469676fc5d0f9a14db5fbf13'>Commit.</a> </li>
<li>Fix static_assert() syntax. <a href='http://commits.kde.org/kleopatra/782ae3b75c9361e184b3d52a3261671a4b2f3068'>Commit.</a> </li>
<li>Port away from boost where possible. <a href='http://commits.kde.org/kleopatra/da4c0eb2677cd38bbaaa43dbc904f9328528e81c'>Commit.</a> </li>
<li>Port Kleopatra to new gpgme 1.7. <a href='http://commits.kde.org/kleopatra/920027ccdaad7420c37057cbb244095e898ca7d8'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/kleopatra/252cac8f325bd5bbf73ab719d773cbef0c5d8391'>Commit.</a> </li>
<li>Remove KDSignalBlocker, port to QSignalBlocker. <a href='http://commits.kde.org/kleopatra/0fec4e68d2ceae34867224fd5c81176dc759876d'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kleopatra/b228ae5d9eb8c2679c285844f988692668d74e6c'>Commit.</a> </li>
<li>Fix encrypt / sign of single directory. <a href='http://commits.kde.org/kleopatra/2f7c88639578c2dd11e298f911a0ed7604338755'>Commit.</a> </li>
<li>Do heavy init work only after uniqueservice call. <a href='http://commits.kde.org/kleopatra/dbb1f8b0216db246039bedbe11009b9d2637bc85'>Commit.</a> </li>
<li>Look for paperkey executable in the install dir. <a href='http://commits.kde.org/kleopatra/dd39faa6f4c5b4d534c03546cc7a60e1d68c7dcd'>Commit.</a> </li>
<li>Remove keep open when done from resultpage. <a href='http://commits.kde.org/kleopatra/781fc0393303435c91a4510f78e0c11e352515cc'>Commit.</a> </li>
<li>Fix ok btn behavior for tasks without output. <a href='http://commits.kde.org/kleopatra/c47c6c2bf5f729f1c9dc95aa2a2ca8525a5181d7'>Commit.</a> </li>
<li>Fix progress handling (for gnupg >= 2.1.15). <a href='http://commits.kde.org/kleopatra/fab9fd5f804bdb54db16a1e51b3528b6036d0364'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365931'>#365931</a></li>
<li>Fix minor typos: and or -> or. <a href='http://commits.kde.org/kleopatra/98fe7602a1e9c5ea570eaa44dd808bf252193aa6'>Commit.</a> </li>
<li>Enfore modern sigencfileswizard style on Windows. <a href='http://commits.kde.org/kleopatra/6305064e85c2a945f9ed93e2ba2ca2508ce7c679'>Commit.</a> </li>
<li>Save / Restore Sigencfileswizard geometry. <a href='http://commits.kde.org/kleopatra/c24d6945965bc27e15fefca2e137bc852dc03305'>Commit.</a> </li>
<li>Fix certificatelineedit removal in sigencwidget. <a href='http://commits.kde.org/kleopatra/2c20f98c198364862e1340e3fa5076c1f8edd3c2'>Commit.</a> </li>
<li>Use setDefaultKey in KeySelectionCombo. <a href='http://commits.kde.org/kleopatra/90873cc04db49e1555a6e38c22deefc734921cd1'>Commit.</a> </li>
<li>Make ASCII Armor for files configurable. <a href='http://commits.kde.org/kleopatra/05f3e9901772bfad8e9b54d5adec6d75ea237300'>Commit.</a> </li>
<li>Add support for symmetric encryption. <a href='http://commits.kde.org/kleopatra/0c94da233d620bfd48f5728450052fdb69aafaee'>Commit.</a> </li>
<li>Initialize members in SignEncryptFilesWizard. <a href='http://commits.kde.org/kleopatra/c29b7d771d07e0f760083e6e10f33643a2ffd686'>Commit.</a> </li>
<li>Move utils/classify to libkleo. <a href='http://commits.kde.org/kleopatra/58cd95577c4c16f303d0481e1cd3a59a18307590'>Commit.</a> </li>
<li>Add missing space. <a href='http://commits.kde.org/kleopatra/7700169686a8597ba8b59f9d6c3fd9ece9d27ef3'>Commit.</a> </li>
<li>Store last selected own keys in SigEnc Widget. <a href='http://commits.kde.org/kleopatra/01b1022b289f2f841be939a574b7bfc9df040e13'>Commit.</a> </li>
<li>Fix sign only for files. <a href='http://commits.kde.org/kleopatra/20570ce63677578d761d22204dc0ffd61b2a123a'>Commit.</a> </li>
<li>Fix sigencpage validation. <a href='http://commits.kde.org/kleopatra/448617fbfe0a612d141533d43dcaf3b26753d61b'>Commit.</a> </li>
<li>Improve Layout of new signencryptwidget. <a href='http://commits.kde.org/kleopatra/ab451eafcf37b31da8f5bd883631f1d7f3344177'>Commit.</a> </li>
<li>Let certificatelineedit inherit QLineEdit. <a href='http://commits.kde.org/kleopatra/4bd90c0c554c80101300b656cddf88c788b5e40d'>Commit.</a> </li>
<li>Use EncryptSelfCertificateFilter again for own key. <a href='http://commits.kde.org/kleopatra/9b853c1498758149b05c2bafe3f38de5d3d88a5d'>Commit.</a> </li>
<li>Replace layout hack by proper margins. <a href='http://commits.kde.org/kleopatra/143b274ae84d01cd02947b3c2381e17c1406eae6'>Commit.</a> </li>
<li>Read ArchiveDefintion from config and handle names. <a href='http://commits.kde.org/kleopatra/cc582f99622877823d70bc4876ab712f39a329ff'>Commit.</a> </li>
<li>Make ArchiveCommand configurable. <a href='http://commits.kde.org/kleopatra/e2fbd723826a1edab3dfbf584d635828613d4b04'>Commit.</a> </li>
<li>Replace certificatecombobox with KeySelectionCombo. <a href='http://commits.kde.org/kleopatra/ca707754b5d5cf6f3f4f6a9ed289f9e40729ed4b'>Commit.</a> </li>
<li>Use full type signature in Q_DECLARE_METATYPE. <a href='http://commits.kde.org/kleopatra/b291f882ec9c0a4b65cf00bcbe1b442c01bf841f'>Commit.</a> </li>
<li>Do not encode PGP Mail Addresses. <a href='http://commits.kde.org/kleopatra/27c76863817243e19582ac1d7fb0bc7e54ff3438'>Commit.</a> </li>
<li>Replace broken EMail validator by KEmailAddress. <a href='http://commits.kde.org/kleopatra/d21ccf3659023f21b62e52e741ec83faae12bdd6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361677'>#361677</a></li>
<li>Add command line option --gen-key. <a href='http://commits.kde.org/kleopatra/7ffea143f14dfd3f50b8b2a250b42213803c401d'>Commit.</a> </li>
<li>Fix startup on UI-Server errors. <a href='http://commits.kde.org/kleopatra/42ff7ff278ee2f03325eee4e09eaad0602b06c2b'>Commit.</a> </li>
<li>Remove splashscreen code from selftestcommand. <a href='http://commits.kde.org/kleopatra/36ff83c012335bfec009353c412e96ed38854f05'>Commit.</a> </li>
<li>Remove Assuan and minimalized Qt ifdefs in main. <a href='http://commits.kde.org/kleopatra/d025eb8cf71451276c6164a493c133aa158b911c'>Commit.</a> </li>
<li>Mark Assuan2 as required. <a href='http://commits.kde.org/kleopatra/91792b78ed51792c6b2a1bc5a51434a1169c5dcb'>Commit.</a> </li>
<li>Proofread KWatchgnupg docbook for 16.08. <a href='http://commits.kde.org/kleopatra/d295090bd46e5d96437d611f2ef72e720b22ed03'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128534'>#128534</a></li>
<li>Move models and some utility classes to Libkleo. <a href='http://commits.kde.org/kleopatra/66a2fe6c99eee44435fc74589d8790f95ec79ba3'>Commit.</a> </li>
<li>Fix archiving and output file selection. <a href='http://commits.kde.org/kleopatra/7c6bfdaf8359e62a1fe6bbd86da0b08a9f9caa12'>Commit.</a> </li>
<li>Enable clear button in CertificateLineEdit. <a href='http://commits.kde.org/kleopatra/e35bbe917ce9bd3fb89596d06b5e3a0ff8115b1f'>Commit.</a> </li>
<li>Remove duplicated assuan2 include directive. <a href='http://commits.kde.org/kleopatra/d8e6657ccdd831862f88e6acbd37acddb49142a1'>Commit.</a> </li>
<li>Add ${ASSUAN2_INCLUDES} to include_directories. <a href='http://commits.kde.org/kleopatra/4257748d4e7f3b2e6a9815cefad692e8f8d1d71a'>Commit.</a> </li>
<li>Include Assuan2 directly in Source. <a href='http://commits.kde.org/kleopatra/38f3fcf2d6c199e32153b2b9e69ff43c30d1da19'>Commit.</a> </li>
<li>Explicitly look for Assuan2 in CMakeLists. <a href='http://commits.kde.org/kleopatra/33797271eb6697e8a081340222f70663a9097e29'>Commit.</a> </li>
<li>Add some FIXMEs and reduce translations. <a href='http://commits.kde.org/kleopatra/8466330425f7dea442084b12cbbb0dd0073f7594'>Commit.</a> </li>
<li>Rework certificateselectionwidget to lineedit. <a href='http://commits.kde.org/kleopatra/4ab1c5cd5363916374edebf7585797af4be788b0'>Commit.</a> </li>
<li>Add Icon for UID helper to formatting. <a href='http://commits.kde.org/kleopatra/e001135eab16bdd2f5393a795d869a50e37d9933'>Commit.</a> </li>
<li>Add support for output file name changing. <a href='http://commits.kde.org/kleopatra/7e19bcc30084028fd2d344b6299ed09899dcbc12'>Commit.</a> </li>
<li>Handle operation changed from the sig/enc widget. <a href='http://commits.kde.org/kleopatra/60e9f5d25b75e222ab8350aa9199a2267d097a00'>Commit.</a> </li>
<li>Add recipient support to signencryptwidget. <a href='http://commits.kde.org/kleopatra/1c351b7a75833b73ddf5931058b727c2f27ec86a'>Commit.</a> </li>
<li>Rewrite of the signencryptfileswizard. <a href='http://commits.kde.org/kleopatra/ac2487e944d99626f6fe8209240490b0717f8b01'>Commit.</a> </li>
<li>Add new widget for certificate selection. <a href='http://commits.kde.org/kleopatra/e10e15be16d65566f27c62e10a0d3d90814a8c3d'>Commit.</a> </li>
<li>Factor out combobox into certificatecombobox. <a href='http://commits.kde.org/kleopatra/eeb042f1febebdefe4836f4e88a2ae02659780c6'>Commit.</a> </li>
<li>Refactor Line class into certificateselectionline. <a href='http://commits.kde.org/kleopatra/cdead29b6bfaa8bd6c5a938073e84bd324854fcb'>Commit.</a> </li>
<li>Respect custom filter column in sort filter model. <a href='http://commits.kde.org/kleopatra/b2c8f4d901abc0d140ece2c321bc5defa4fb9a07'>Commit.</a> </li>
<li>Add function to get model based on keycache. <a href='http://commits.kde.org/kleopatra/1ca721ca8d983726aeeb58f07347f5ef59737dd1'>Commit.</a> </li>
<li>Add new KeyModel based on KeyRearangeColumnsProxy. <a href='http://commits.kde.org/kleopatra/1e20795fc16b18ee0da2abc6b05e0793b49d0884'>Commit.</a> </li>
</ul>
<h3><a name='klettres' href='https://cgit.kde.org/klettres.git'>klettres</a> <a href='#klettres' onclick='toggle("ulklettres", this)'>[Show]</a></h3>
<ul id='ulklettres' style='display: none'>
<li>Use cmake variable from ECM for appstream directory. <a href='http://commits.kde.org/klettres/7a742c55947a229fc6e05a6470559a5e0b0bce90'>Commit.</a> </li>
</ul>
<h3><a name='klinkstatus' href='https://cgit.kde.org/klinkstatus.git'>klinkstatus</a> <a href='#klinkstatus' onclick='toggle("ulklinkstatus", this)'>[Show]</a></h3>
<ul id='ulklinkstatus' style='display: none'><li>New in this release</li></ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Show]</a></h3>
<ul id='ulkmail' style='display: none'><li>New in this release</li></ul>
<h3><a name='kmail-account-wizard' href='https://cgit.kde.org/kmail-account-wizard.git'>kmail-account-wizard</a> <a href='#kmail-account-wizard' onclick='toggle("ulkmail-account-wizard", this)'>[Show]</a></h3>
<ul id='ulkmail-account-wizard' style='display: none'><li>New in this release</li></ul>
<h3><a name='kmailtransport' href='https://cgit.kde.org/kmailtransport.git'>kmailtransport</a> <a href='#kmailtransport' onclick='toggle("ulkmailtransport", this)'>[Show]</a></h3>
<ul id='ulkmailtransport' style='display: none'>
<li>Remove some clazy warning. <a href='http://commits.kde.org/kmailtransport/eea75465e65b922c4c31b0613b596661e74b89e8'>Commit.</a> </li>
<li>Use isEmpty here too. <a href='http://commits.kde.org/kmailtransport/24ad911dec51e8547927d82235ea8b10bfb87fe5'>Commit.</a> </li>
<li>Detele a here too. <a href='http://commits.kde.org/kmailtransport/f213f5230caf7974bc3f3d209484b52e89abbd54'>Commit.</a> </li>
<li>Fix mem leak. <a href='http://commits.kde.org/kmailtransport/0127678a87be802ae575ef97072f06ddc01662d4'>Commit.</a> </li>
<li>Compare clone. <a href='http://commits.kde.org/kmailtransport/f16009ca8d5f281ab846a57426b90148f1fac338'>Commit.</a> </li>
<li>Test copy. <a href='http://commits.kde.org/kmailtransport/1dbf2f02d2db25ad22fb489e9b88a4e87c48ab0e'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kmailtransport/d5948bc4f03fcfd337497d130f6a82e1f66ad7b0'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/kmailtransport/4e5c0984944a3d184b7b8315aaded5a1e08d312e'>Commit.</a> </li>
<li>Fix deserialize attribute :) It's good to execute autotest :) It was. <a href='http://commits.kde.org/kmailtransport/1d99fef42ff4b0c6e4e17fee3af9db456dd30b8a'>Commit.</a> </li>
<li>Add autotest for send silently. <a href='http://commits.kde.org/kmailtransport/5f8d4e488ddd3479552868cf4e6428ccc8550979'>Commit.</a> </li>
<li>SentBehaviourAttribute: clone mSilent flag too. <a href='http://commits.kde.org/kmailtransport/fc35d720ae012a8e3fb498ff58863937836843c1'>Commit.</a> </li>
<li>Fix assert in SentBehaviourAttribute::seralize(). <a href='http://commits.kde.org/kmailtransport/3f58621127785ad8a89b94f2739495e35dade5e5'>Commit.</a> </li>
<li>Add silent mode to SentBehaviourAttribute. <a href='http://commits.kde.org/kmailtransport/1f34e48610033a307856587b10728cc18c98de86'>Commit.</a> </li>
<li>Export SmtpJob. <a href='http://commits.kde.org/kmailtransport/a150b51de74df6a9bb073e4b722f094c1de4958a'>Commit.</a> </li>
<li>Install SmtpJob header. <a href='http://commits.kde.org/kmailtransport/f6b174ead6bb4505bfa78d2a39f15d1bf42effe6'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/kmailtransport/9426bf71354005ae3b7e816cbf76ccd84295f0d0'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/kmailtransport/81240c41470a7457dae10b7c274443f3e142b288'>Commit.</a> </li>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kmailtransport/3f2ace681ed08f326541556b4e163b2061237af8'>Commit.</a> </li>
<li>Fix Bug 368538 - mailtransport uses only tls1.0. <a href='http://commits.kde.org/kmailtransport/c644c58bb57406f8b20ab0fc1396d92b03c40f32'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368538'>#368538</a></li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kmailtransport/60b63a16c794c6f211ffbc2f0d89a5315af4f8ff'>Commit.</a> </li>
<li>KF5::TextWidgets is required to build the tests. <a href='http://commits.kde.org/kmailtransport/b1dfef6933e8827050203a8e79080f3494e94bac'>Commit.</a> </li>
<li>Fix compile with Qt 5 without deprecated features:. <a href='http://commits.kde.org/kmailtransport/49ade0d9b3539e433342537a54ae51b7d9eaf0dd'>Commit.</a> </li>
<li>Use non-deprecated QSsl::TlsV1_0. <a href='http://commits.kde.org/kmailtransport/1dff5d5aee9a8ebdc6b38faa9791b6cdb60674a2'>Commit.</a> </li>
<li>QUrl has no addQueryItem, use QUrlQuery instead for that. <a href='http://commits.kde.org/kmailtransport/f0689cea597e9131ef12a138bf112829bff80bee'>Commit.</a> </li>
<li>KIO is required, fixes compile errors. <a href='http://commits.kde.org/kmailtransport/31ec73a231c5ffe00f0433e338b59cd005edc0e6'>Commit.</a> </li>
</ul>
<h3><a name='kmbox' href='https://cgit.kde.org/kmbox.git'>kmbox</a> <a href='#kmbox' onclick='toggle("ulkmbox", this)'>[Show]</a></h3>
<ul id='ulkmbox' style='display: none'>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kmbox/11630d351ff289d30054ed4f4a9f25f03ba18a89'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kmbox/03bd6c6577098e0a22f8cddc78dce383bd02e700'>Commit.</a> </li>
<li>Improve the documentation markup. <a href='http://commits.kde.org/kmbox/ca0ab3d590ccc27b43e56215197ff33f058ad7bb'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/kmbox/5239048c9b6abdbba38562be60fdb46977bab2de'>Commit.</a> </li>
</ul>
<h3><a name='kmime' href='https://cgit.kde.org/kmime.git'>kmime</a> <a href='#kmime' onclick='toggle("ulkmime", this)'>[Show]</a></h3>
<ul id='ulkmime' style='display: none'>
<li>Add info about invalid address. <a href='http://commits.kde.org/kmime/bacb2c706c89b2601e6cc0cd48a6325ece10fd58'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kmime/afd30748d85ab3955a0be29d970502b3fa2ca761'>Commit.</a> </li>
<li>Enable docs generation on api.kde.org. <a href='http://commits.kde.org/kmime/05a76bb3b0dad1e4b0f38461871d431372c620f5'>Commit.</a> </li>
</ul>
<h3><a name='kmplot' href='https://cgit.kde.org/kmplot.git'>kmplot</a> <a href='#kmplot' onclick='toggle("ulkmplot", this)'>[Show]</a></h3>
<ul id='ulkmplot' style='display: none'>
<li>Use KStandardGuiItem::overwrite(). <a href='http://commits.kde.org/kmplot/1969e97bd24d8e9508a4afc7d3dc11e315bc4a8e'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kmplot/2c7a0d084ef4b8b2dff7a03c1a9963f1f528b011'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kmplot/a101686be7c7d1245dd81db9f5143accdb144cb4'>Commit.</a> </li>
</ul>
<h3><a name='knotes' href='https://cgit.kde.org/knotes.git'>knotes</a> <a href='#knotes' onclick='toggle("ulknotes", this)'>[Show]</a></h3>
<ul id='ulknotes' style='display: none'><li>New in this release</li></ul>
<h3><a name='kollision' href='https://cgit.kde.org/kollision.git'>kollision</a> <a href='#kollision' onclick='toggle("ulkollision", this)'>[Show]</a></h3>
<ul id='ulkollision' style='display: none'>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kollision/5ab91371ad483d8cd9fac8c11adce77c22346296'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kollision/77048963774aa55476634a381dbd60a019ec5891'>Commit.</a> </li>
</ul>
<h3><a name='kolourpaint' href='https://cgit.kde.org/kolourpaint.git'>kolourpaint</a> <a href='#kolourpaint' onclick='toggle("ulkolourpaint", this)'>[Show]</a></h3>
<ul id='ulkolourpaint' style='display: none'>
<li>Use KStandardGuiItem::overwrite(). <a href='http://commits.kde.org/kolourpaint/20b9078c683df3207ad811f7bcd7879918f1556b'>Commit.</a> </li>
<li>New slot for clearing recent files action. <a href='http://commits.kde.org/kolourpaint/d652f727ad9ddb86655eac4242d3426ebe89c920'>Commit.</a> </li>
<li>Make sure clearing the recent files also removes it from the config file. <a href='http://commits.kde.org/kolourpaint/d06ed0a4d404234548ee13c460c3b0f4f7404cd0'>Commit.</a> </li>
<li>Respect EXIF rotation information on loading an image. <a href='http://commits.kde.org/kolourpaint/a149002600d8d697fa7136488f0d4703054a3a4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/252003'>#252003</a></li>
<li>Fix desktop file name in KAboutData. <a href='http://commits.kde.org/kolourpaint/2cd4e5f7847e1b4a28ff25895f401ffaa68be3e1'>Commit.</a> </li>
<li>Fix handling of commandline filename argument when given relative path. <a href='http://commits.kde.org/kolourpaint/333e1d16efe138d3618f5ec4ecfd6008c7b6a848'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368131'>#368131</a></li>
<li>Add a "make confidential" action which blurs the image/selection. <a href='http://commits.kde.org/kolourpaint/015d25f5db41b06e27227372917564b413a46b14'>Commit.</a> Implements feature <a href='https://bugs.kde.org/366436'>#366436</a></li>
<li>Port away from kDebug. <a href='http://commits.kde.org/kolourpaint/23d779e4e202e2ea7b986e0a872deeaa1a7e95fe'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128622'>#128622</a></li>
<li>Remove unused entities kappname + package. <a href='http://commits.kde.org/kolourpaint/936754cbc2cb9ac5c4b908cd95abaa6844813016'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128516'>#128516</a></li>
<li>Update screenshots to kf5. <a href='http://commits.kde.org/kolourpaint/06fa0e28a569af5b54b3bc968e29168d59655be7'>Commit.</a> </li>
</ul>
<h3><a name='kommander' href='https://cgit.kde.org/kommander.git'>kommander</a> <a href='#kommander' onclick='toggle("ulkommander", this)'>[Show]</a></h3>
<ul id='ulkommander' style='display: none'><li>New in this release</li></ul>
<h3><a name='konqueror' href='https://cgit.kde.org/konqueror.git'>konqueror</a> <a href='#konqueror' onclick='toggle("ulkonqueror", this)'>[Show]</a></h3>
<ul id='ulkonqueror' style='display: none'><li>New in this release</li></ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>Set default TERM=xterm-256color. <a href='http://commits.kde.org/konsole/d5cac9d2f1a1225d19c0be6b51e3db6e4aaed3d4'>Commit.</a> See bug <a href='https://bugs.kde.org/371919'>#371919</a>. Fixes bug <a href='https://bugs.kde.org/212145'>#212145</a></li>
<li>Sync value order with how 16.08 writes them. <a href='http://commits.kde.org/konsole/cbc20416f77ab13b7e37de2a1030fb4bad143a57'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129369'>#129369</a></li>
<li>Fix Save Output as -> HTML. <a href='http://commits.kde.org/konsole/019dde3e04b792f6643aa2993ed293aadbe4cd55'>Commit.</a> </li>
<li>Only use foreground process info if valid. <a href='http://commits.kde.org/konsole/d28afb6664459cf28d73d7617b74c49711ce7a86'>Commit.</a> See bug <a href='https://bugs.kde.org/372401'>#372401</a>. See bug <a href='https://bugs.kde.org/372620'>#372620</a>. See bug <a href='https://bugs.kde.org/372619'>#372619</a>. See bug <a href='https://bugs.kde.org/372593'>#372593</a></li>
<li>Don't crash/assert on trying to load session with invalid ID. <a href='http://commits.kde.org/konsole/6416009561ce800cde2824bbf369a32687b6fdfe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372173'>#372173</a></li>
<li>Fix crash with combining characters after several cursorRight() calls. <a href='http://commits.kde.org/konsole/76453a7df8427048a8ce92169c3dbd172f89798c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372530'>#372530</a></li>
<li>Add COLORTERM=truecolor to the environment. <a href='http://commits.kde.org/konsole/8d32afabb564ae124e06e233be5c5300da906aea'>Commit.</a> Implements feature <a href='https://bugs.kde.org/371919'>#371919</a></li>
<li>Breeze color scheme uses better contrasting color for "black" color value. <a href='http://commits.kde.org/konsole/3f79ec70e5f1d7eb4f8bd85d827632afef30e29f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371068'>#371068</a>. Code review <a href='https://git.reviewboard.kde.org/r/129368'>#129368</a></li>
<li>Silence ubsan warnings about invalid pointer casts. <a href='http://commits.kde.org/konsole/66e85acbc9db67b1f57e3dd6da0e9523e831728f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129343'>#129343</a></li>
<li>Add setting for using multiple instances. <a href='http://commits.kde.org/konsole/a30be88f3bd62b84b1c4760cf7fa3fbef32ef23e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129345'>#129345</a></li>
<li>Fix randomization in color scheme. <a href='http://commits.kde.org/konsole/3ff022527e53cd5daa6fe8589de81b5988457945'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129342'>#129342</a></li>
<li>Correct handling %u when changing tab title via Rename Tab dialog. <a href='http://commits.kde.org/konsole/a5ca90345378262825b2450ff89db7addcf89309'>Commit.</a> </li>
<li>Change close activate view to Ctrl+Shift+X to avoid duplicate. <a href='http://commits.kde.org/konsole/f2358180683f96b1c47e1a441f45c88460fc23b5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129305'>#129305</a>. Fixes bug <a href='https://bugs.kde.org/354954'>#354954</a></li>
<li>Add info about shortcut for menubar and DnD options. <a href='http://commits.kde.org/konsole/72ec5f6819dda101d6c4a32cb8648b07aa34aa63'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129300'>#129300</a></li>
<li>Fix memory leak when building with a11y support. <a href='http://commits.kde.org/konsole/d95cc48ae17e1cae4d067cc0ed8d795b244b5299'>Commit.</a> </li>
<li>Silence warning from QCommandLineParser. <a href='http://commits.kde.org/konsole/b410e7f08ced8741ca8c83bb45083e944b9fb278'>Commit.</a> </li>
<li>Remove debug output I forgot to remove. <a href='http://commits.kde.org/konsole/a9b0878b56ccea354bb97218272a6f55d85d026a'>Commit.</a> </li>
<li>Another missing receiver, spotted by sandsmark. <a href='http://commits.kde.org/konsole/ce3013d9dfcb0169fb39c128569d7099252723f7'>Commit.</a> </li>
<li>Fix compilation with KF < 5.23, patch by Aniketh Girish. <a href='http://commits.kde.org/konsole/efeaa73fe17c4a19ca47d536bd72b3066eacc737'>Commit.</a> </li>
<li>Pass receiver as 3rd arg in qt5-style-connect, to avoid crashes when the receiver is deleted. <a href='http://commits.kde.org/konsole/ec172b8f6b39a056326751f26ce81af664f0837f'>Commit.</a> </li>
<li>Fix reparsing of command line arguments. <a href='http://commits.kde.org/konsole/4ec9b565800b9e9bdbf45dbb5983530fb95791e7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129217'>#129217</a></li>
<li>Adjust a UI string to make its meaning clearer. <a href='http://commits.kde.org/konsole/bb7561862488bf8f1dbe95c85eba6361ada42fb9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129232'>#129232</a></li>
<li>Make the config of file filters and url filters separate. <a href='http://commits.kde.org/konsole/b2b044cf2ad3b7a400fd032650acfa37c4e440b9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128828'>#128828</a>. Fixes bug <a href='https://bugs.kde.org/368234'>#368234</a></li>
<li>Remove last remains of bold in comments. <a href='http://commits.kde.org/konsole/e2d85494e27b57c421e191ea924bff0c5f26786d'>Commit.</a> </li>
<li>Update manual for nofork and selecting non mono fonts. <a href='http://commits.kde.org/konsole/e8422251f7a5765b66808a464d0eb1ada21f0a44'>Commit.</a> </li>
<li>Fix build with g++ 4.8.5. <a href='http://commits.kde.org/konsole/12c701747c8c35a354cf66e195ed92641b63647e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129124'>#129124</a></li>
<li>Validate initial working directory Profile entry. <a href='http://commits.kde.org/konsole/6e2d3f4e7e952a342ed91bf93d4487bd51a331ea'>Commit.</a> </li>
<li>A QSet is sufficient here. QSet is also hash-based. <a href='http://commits.kde.org/konsole/1b8c9ca40ac7aff0d37eb3c26ade41c74d44a5c9'>Commit.</a> </li>
<li>A loop can run zero times without another check. <a href='http://commits.kde.org/konsole/35fdfb2fcbdc7aca64639cf18e9ac40a42d59873'>Commit.</a> </li>
<li>Update link for Qt syle sheet to qt.io and qt5. <a href='http://commits.kde.org/konsole/53c7d22873286be70d91f14ab17056007da0e2ea'>Commit.</a> </li>
<li>Remove ColorEntry::FontWeight as it hasn't been used for a long time. <a href='http://commits.kde.org/konsole/191b1ce83d6d030a59fbdb912af33c260e530053'>Commit.</a> </li>
<li>Fix updating of tab title. <a href='http://commits.kde.org/konsole/3747ecfde0800804717203d7f5fa0f96c0791052'>Commit.</a> See bug <a href='https://bugs.kde.org/368785'>#368785</a></li>
<li>Allow profile to set margin and center flag. <a href='http://commits.kde.org/konsole/8e5a5b1ec1fb68e359319c9ce23a43c0434b836a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346450'>#346450</a></li>
<li>Use QPointer for QDrag widget. <a href='http://commits.kde.org/konsole/7a97832d9a02af74f179f761adbce704af761f3f'>Commit.</a> </li>
<li>Use QPointer for KeyBindingEditor dialog. <a href='http://commits.kde.org/konsole/4f15c586019027d87a06645a12844aceaa8ffd94'>Commit.</a> </li>
<li>Const & foreach loop. <a href='http://commits.kde.org/konsole/edcb028d299c4dcf152116e05f4adfb0c35198bd'>Commit.</a> </li>
<li>Fix scrolling down if user presses something which is not just a modifier. <a href='http://commits.kde.org/konsole/54ecd560b8d76121256a0bd653999fb8d4acb363'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128827'>#128827</a></li>
<li>Use explicit for single argument classes. <a href='http://commits.kde.org/konsole/624e6ecdf7f062d8823d7bdacb66c802a62112f3'>Commit.</a> </li>
<li>Don't show URL hints if no URL hint modifiers are set. <a href='http://commits.kde.org/konsole/e7d7069f12fa3b22cbdd88ad81ce538659c75a9f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367945'>#367945</a></li>
<li>Only send EOF to known shells. <a href='http://commits.kde.org/konsole/ce4ab922496cb9e133f96db78c5b67bc01429512'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128791'>#128791</a>. Fixes bug <a href='https://bugs.kde.org/367746'>#367746</a></li>
<li>Warn when trying to close window with open tabs. <a href='http://commits.kde.org/konsole/e21fc13c473cff47654f02b1e5ecd6ff8e3636f4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128668'>#128668</a>. Fixes bug <a href='https://bugs.kde.org/333023'>#333023</a></li>
<li>Add KCrash code so krkonqi crash dialog works again. <a href='http://commits.kde.org/konsole/30fe2a0b9d93617c80f6e3d5a61d845a5fb526ab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367006'>#367006</a></li>
<li>Fix checking of foreground process. <a href='http://commits.kde.org/konsole/79ca76a94696eda408927b122b0b531676d4ba93'>Commit.</a> See bug <a href='https://bugs.kde.org/367746'>#367746</a>. Code review <a href='https://git.reviewboard.kde.org/r/128789'>#128789</a></li>
<li>Avoid polling processes if they report what we need with OCS7. <a href='http://commits.kde.org/konsole/e7770aba9f6af26cba3dbecc1ac085acb214c6cc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128784'>#128784</a>. Fixes bug <a href='https://bugs.kde.org/325442'>#325442</a></li>
<li>Fix display sometimes randomly scrolling down. <a href='http://commits.kde.org/konsole/eea5ecfc5ed93515afaae5b9b938154eeaaf0ba8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128786'>#128786</a></li>
<li>Make the URL hint keyboard modifiers configurable. <a href='http://commits.kde.org/konsole/04dce67996a9318494c79c2617e3c88620e8e19e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128778'>#128778</a></li>
<li>Prevent 2 windows from opening upon session restore. <a href='http://commits.kde.org/konsole/5b11fd593b954504343bdb418c54a8e7913d9c7c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367447'>#367447</a></li>
<li>Don't re-read process information that should be static. <a href='http://commits.kde.org/konsole/392ee33cf8ffec54d41d342d7cba8fe82a13adc0'>Commit.</a> See bug <a href='https://bugs.kde.org/325442'>#325442</a>. Code review <a href='https://git.reviewboard.kde.org/r/128777'>#128777</a></li>
<li>Fix issues where --tabs-from-file doesn't exist or is invalid. <a href='http://commits.kde.org/konsole/d73270a470680e4b796372a3196d15053d50029b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367855'>#367855</a></li>
<li>Fix crash when closing session. <a href='http://commits.kde.org/konsole/b2b852cfd7b10fc2ffb668ac87f8ca9236a134e3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128667'>#128667</a>. Fixes bug <a href='https://bugs.kde.org/366706'>#366706</a></li>
<li>Remove unused environment reading code. <a href='http://commits.kde.org/konsole/e07a62ede32467089c1e4c57f7fed141ea009c49'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128674'>#128674</a>. See bug <a href='https://bugs.kde.org/325442'>#325442</a></li>
<li>Nofork option removed with https://quickgit.kde.org/?p=konsole/27dec8d02f705c77eef45a2533bed203eec9454f. <a href='http://commits.kde.org/konsole/a9a8db2cf4b7a9c063445d1505b8ee410f9949a8'>Commit.</a> </li>
<li>Only try to send EOF to the shell, not other processes. <a href='http://commits.kde.org/konsole/c6c09ce7a38fccdc1cbbc847a80b98f107349fc7'>Commit.</a> </li>
<li>Fix regexpfilter matching with an empty regex. <a href='http://commits.kde.org/konsole/9175663f216a85f7c81c8080adbab872c74b6bd4'>Commit.</a> </li>
<li>Avoid asking the user a second time for confirmation of closing an active tab. <a href='http://commits.kde.org/konsole/6c23f43ec314ea196c8a42682f91467778a67e54'>Commit.</a> </li>
<li>Fix transparent backgrounds. <a href='http://commits.kde.org/konsole/a28902e9120798c67844864a11bbe3f1c2549b74'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128669'>#128669</a>. Fixes bug <a href='https://bugs.kde.org/366368'>#366368</a></li>
<li>Fix crash when failing to restore session. <a href='http://commits.kde.org/konsole/13684bebe2a421157032bc3cfaf28e468d0c1eef'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128670'>#128670</a>. Fixes bug <a href='https://bugs.kde.org/366217'>#366217</a></li>
<li>Add option to use the selected font's line characters. <a href='http://commits.kde.org/konsole/a4de3feb16df7f18559587ebf78f115d88acc022'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364992'>#364992</a></li>
<li>Allow non monospaced fonts to be selected. <a href='http://commits.kde.org/konsole/fa5f32f13fae38e8cee15dd2d87db30fa6dc6822'>Commit.</a> See bug <a href='https://bugs.kde.org/349350'>#349350</a></li>
<li>Add simple script to test/show sgr 2 8 9 53. <a href='http://commits.kde.org/konsole/19804bc2dffe87e2eb80a40fd837152488479aaa'>Commit.</a> </li>
<li>Add rendition flags SGRs 2, 8, 9, 53. <a href='http://commits.kde.org/konsole/84b43dfb2108eab47fa1dfcafbf1b94a410d6cbf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128405'>#128405</a>. Fixes bug <a href='https://bugs.kde.org/362171'>#362171</a></li>
<li>Improve filter updating. <a href='http://commits.kde.org/konsole/f3ce85b9effbb16d8cd08f5dc60c09c986c4607a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128393'>#128393</a></li>
</ul>
<h3><a name='kontact' href='https://cgit.kde.org/kontact.git'>kontact</a> <a href='#kontact' onclick='toggle("ulkontact", this)'>[Show]</a></h3>
<ul id='ulkontact' style='display: none'><li>New in this release</li></ul>
<h3><a name='kontactinterface' href='https://cgit.kde.org/kontactinterface.git'>kontactinterface</a> <a href='#kontactinterface' onclick='toggle("ulkontactinterface", this)'>[Show]</a></h3>
<ul id='ulkontactinterface' style='display: none'>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/kontactinterface/97aaea8e06db1c4cecced7cf7e52a056dad7bf38'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kontactinterface/1e11f7bb3f3cf628df83482d93c87d232213958b'>Commit.</a> </li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Prepare for 16.12.0. <a href='http://commits.kde.org/kopete/c44bd4588c332d348d8bc82659ce4800fc4b007f'>Commit.</a> </li>
<li>Update URL of jabber server list. <a href='http://commits.kde.org/kopete/472ed51dcef188e1d8b4c946c99f6f48c2216385'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356102'>#356102</a></li>
<li>Fix crash after OTR plugin generates key. <a href='http://commits.kde.org/kopete/15579ddcf34ad04bf190a189b670f62c3796c057'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349596'>#349596</a></li>
<li>When we receive encrypted OTR message with different instance tag then update it in current ChatSession. <a href='http://commits.kde.org/kopete/9bdf83a9a20a0dcc69c21b060b6ce26272646ba8'>Commit.</a> </li>
<li>Do not show empty internal messages by OTR plugin. <a href='http://commits.kde.org/kopete/966e8170b04812369307a2471596e433662e8f79'>Commit.</a> </li>
<li>Fix memory leak in OTR plugin. <a href='http://commits.kde.org/kopete/0b49f29662de4d4f5a5bbacc90e9791ae794a8dd'>Commit.</a> </li>
<li>When updating OTR GUI icon properly set OTR instance tag. <a href='http://commits.kde.org/kopete/19957f9324a5ae45bcb1479f1bb017efa77d0aa7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362535'>#362535</a></li>
<li>Prepare for 16.11.90. <a href='http://commits.kde.org/kopete/552c1ebb342c9c9da096214b608c2cccbe85cfc9'>Commit.</a> </li>
<li>Prepare for 16.11.80. <a href='http://commits.kde.org/kopete/f8b7bf3397787c1004723893fe595870b4247c9d'>Commit.</a> </li>
<li>Add support for X-OAuth2 authentication in Jabber protocol. <a href='http://commits.kde.org/kopete/3bff188483fd2ee01bb8310a511e8cc9a4808d22'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129239'>#129239</a>. Fixes bug <a href='https://bugs.kde.org/354473'>#354473</a></li>
<li>Update copyright year for Kopete Development Team. <a href='http://commits.kde.org/kopete/9b48acb5551f205bec4ac1b5bcb32f00a59d13b9'>Commit.</a> </li>
<li>Update libiris from https://github.com/psi-im/iris.git commit 1db95f9af245e0dd8d33fd7e686e960b59d0cb9d. <a href='http://commits.kde.org/kopete/76eb903dd96532e96ce3b7113a7489f2f5a88cd0'>Commit.</a> </li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Show]</a></h3>
<ul id='ulkorganizer' style='display: none'><li>New in this release</li></ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Show]</a></h3>
<ul id='ulkpimtextedit' style='display: none'>
<li>Fix some clazy warning. <a href='http://commits.kde.org/kpimtextedit/606e0fc2731100d8f334e2feb1cfb272c25bad53'>Commit.</a> </li>
<li>Remove this include. <a href='http://commits.kde.org/kpimtextedit/61f7e82565fc67c056b76fc0d0d1155bfeef76b9'>Commit.</a> </li>
<li>Now we use ksyntaxhightlighting. <a href='http://commits.kde.org/kpimtextedit/97b53f7d690aa1ced7ee7c0e94ae78e7a43d519a'>Commit.</a> </li>
<li>Update comment. <a href='http://commits.kde.org/kpimtextedit/d5023b17d1cdf9e705c72a1bd97d551d615acc3b'>Commit.</a> </li>
<li>Step 1 before to remove option. <a href='http://commits.kde.org/kpimtextedit/c2a988db06a79a0ece393cbfffc8a6dc3034bcc5'>Commit.</a> </li>
<li>Port after renaming. <a href='http://commits.kde.org/kpimtextedit/89a162f39a55cb7d43ee024ef538560802c88331'>Commit.</a> </li>
<li>Minor optimization to get linkcolor. <a href='http://commits.kde.org/kpimtextedit/918141c9766e23ef13c0164352352f50a2cd419a'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/kpimtextedit/f83c63ac884b0986e586d8e2c8a232d63b41fc3b'>Commit.</a> </li>
<li>Adapt to new api. <a href='http://commits.kde.org/kpimtextedit/61b8955f9a5908a297ce38ad3c54beeb847fd206'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/kpimtextedit/830b9d1f0624dcbcc31cd1d6b09d2d254c6e0d42'>Commit.</a> </li>
<li>Use syntaxhighlighting. <a href='http://commits.kde.org/kpimtextedit/082e3b59261a442c3971040deea9b25a577ac873'>Commit.</a> </li>
<li>Don't install files twice. <a href='http://commits.kde.org/kpimtextedit/1cc31f5e7bc4d6cdab3cf66d011ca886a0cf0978'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/kpimtextedit/a80fdc547f089bcc1c9ada10ccce92b4f8e380e4'>Commit.</a> </li>
<li>Improve autotest and migrate to QRegularExpression. <a href='http://commits.kde.org/kpimtextedit/c3ee531f33e97ca02c16de1cab7ca7f2207ea5d9'>Commit.</a> </li>
<li>Use new connect api. <a href='http://commits.kde.org/kpimtextedit/60e128f577da3f26b68876eeba3432366c7f270f'>Commit.</a> </li>
<li>Don"t show action when not necessary. <a href='http://commits.kde.org/kpimtextedit/a5009e6c0a261a7d1107a9c4db658a94854da14d'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/kpimtextedit/22f64930ed7190a3f3f08e11668a6f9e3eb64a43'>Commit.</a> </li>
<li>Add support for selected text. <a href='http://commits.kde.org/kpimtextedit/b8d30a730c0a1031d92ec8b746c7fde181cab568'>Commit.</a> </li>
<li>Add new method to force autocorrection. Which will reimplement in messagecomposer. <a href='http://commits.kde.org/kpimtextedit/01cecd873a3187f8874b977887551bef4635280e'>Commit.</a> </li>
<li>Move as public QSLOT. <a href='http://commits.kde.org/kpimtextedit/00fa78980f08d11bfbf7a09687cf4d8a2c90ee50'>Commit.</a> </li>
<li>Port to new connect api. <a href='http://commits.kde.org/kpimtextedit/568ce484a01183d49527b04801a02b1f54180c37'>Commit.</a> </li>
</ul>
<h3><a name='kqtquickcharts' href='https://cgit.kde.org/kqtquickcharts.git'>kqtquickcharts</a> <a href='#kqtquickcharts' onclick='toggle("ulkqtquickcharts", this)'>[Show]</a></h3>
<ul id='ulkqtquickcharts' style='display: none'>
<li>LegendItem: Fix Reference Errors. <a href='http://commits.kde.org/kqtquickcharts/9f44dbcf08eb396de8e1bdf4fe430abf492ad33b'>Commit.</a> </li>
<li>LegendItem: Fix behavior in when used in a layout. <a href='http://commits.kde.org/kqtquickcharts/2e4c0a96eca311da4e7335f9ee06fa77a819ca05'>Commit.</a> </li>
<li>Merge Branch 'frameworks'. <a href='http://commits.kde.org/kqtquickcharts/2cd0baf7eea44c9ee25f454b9ecced65346f987a'>Commit.</a> </li>
<li>Minor README Update. <a href='http://commits.kde.org/kqtquickcharts/21f89e481c926de6e65e399d11f20931772c1715'>Commit.</a> </li>
<li>Fix compilation. <a href='http://commits.kde.org/kqtquickcharts/c2d83603006f893522f27481ffa9953fd6fd5a10'>Commit.</a> </li>
<li>Update README.md For Qt 5. <a href='http://commits.kde.org/kqtquickcharts/b9bad28df92ef30360eafa6703b93c1d3374770e'>Commit.</a> </li>
<li>Compile in Mac?. <a href='http://commits.kde.org/kqtquickcharts/7c589d017053d90a235346034754c1ae234b58f5'>Commit.</a> </li>
<li>Add New Chart Type For Two-Dimensional Data. <a href='http://commits.kde.org/kqtquickcharts/4162048589abae0a7b42e316f9f8bf80c81ddc33'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123734'>#123734</a></li>
<li>Port to KF5. <a href='http://commits.kde.org/kqtquickcharts/3f22c00d2343616c9caeed32f6cf41f675c859eb'>Commit.</a> </li>
</ul>
<h3><a name='krdc' href='https://cgit.kde.org/krdc.git'>krdc</a> <a href='#krdc' onclick='toggle("ulkrdc", this)'>[Show]</a></h3>
<ul id='ulkrdc' style='display: none'>
<li>REVIEW:129142. <a href='http://commits.kde.org/krdc/0f217bbf339b9d1d0efa30a84faf41ca7097b533'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129142'>#129142</a></li>
</ul>
<h3><a name='krfb' href='https://cgit.kde.org/krfb.git'>krfb</a> <a href='#krfb' onclick='toggle("ulkrfb", this)'>[Show]</a></h3>
<ul id='ulkrfb' style='display: none'>
<li>Needed to simplify the creation of reviews via the rbt tool. <a href='http://commits.kde.org/krfb/f1a3e2b31ce2b4f8653427a7b099004b3abb46d5'>Commit.</a> </li>
<li>Fix build by undefining the max macro. <a href='http://commits.kde.org/krfb/51957837575210517f5917d1cebcda1fe90f509b'>Commit.</a> </li>
<li>Fix minor glitches. <a href='http://commits.kde.org/krfb/1b6702167db7ddfe797b089c30e7ae7f7d06ae8c'>Commit.</a> </li>
<li>Update KRFB docbook to 16.08. <a href='http://commits.kde.org/krfb/8b7fcb023d2125ab6a60022f7bbbded525683462'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128528'>#128528</a></li>
</ul>
<h3><a name='kruler' href='https://cgit.kde.org/kruler.git'>kruler</a> <a href='#kruler' onclick='toggle("ulkruler", this)'>[Show]</a></h3>
<ul id='ulkruler' style='display: none'>
<li>Add appstream file to kruler. <a href='http://commits.kde.org/kruler/d05fab8bed69f073c3ec25afb2d87046eb7ad09a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129322'>#129322</a></li>
</ul>
<h3><a name='kstars' href='https://cgit.kde.org/kstars.git'>kstars</a> <a href='#kstars' onclick='toggle("ulkstars", this)'>[Show]</a></h3>
<ul id='ulkstars' style='display: none'>
<li>Use QDialogButtonBox. <a href='http://commits.kde.org/kstars/42eee1ef450633d87a9699afcb09caab04a7eaf5'>Commit.</a> </li>
<li>Polish INDIHostConf UI. <a href='http://commits.kde.org/kstars/b1be6c240dfa08bfe9419795f37a876f18d1fac7'>Commit.</a> </li>
<li>Add missing ifdef to fix build. <a href='http://commits.kde.org/kstars/f2cd5da550b29afe5a144a2448ee9d3c34db7c62'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129387'>#129387</a></li>
<li>Add support to dithering every X frames as requested. <a href='http://commits.kde.org/kstars/1e083f0c3d44321a403a8a215a56b36dfbc7dae3'>Commit.</a> </li>
<li>Do not set time as it is already set in SimClock ctor. <a href='http://commits.kde.org/kstars/fcbacdf52b8f6500f1dabb57bb7b0411d1f5b208'>Commit.</a> </li>
<li>KStars now depends on INDI v1.3.1. <a href='http://commits.kde.org/kstars/cb99a35f6e6b56e3bbb5e9012af263c0f23d266b'>Commit.</a> </li>
<li>Simple fix by Rob to show WCS Labels after switch. <a href='http://commits.kde.org/kstars/7e9dccd87456fa48953667f22336e7e49cecf77e'>Commit.</a> </li>
<li>Bump to 2.7.2. <a href='http://commits.kde.org/kstars/cfb6d384b19860a814d1f918cb6cc96fa0235749'>Commit.</a> </li>
<li>Set all connection to UniqueConnection in Align and fix mismatched signal/slot. <a href='http://commits.kde.org/kstars/7b474dc97b7bae3d5605e2aa146e4aab53c9f728'>Commit.</a> </li>
<li>Improve visibility of the magnitude number in the labels. <a href='http://commits.kde.org/kstars/5ec1c8c947120f20bc6ab3b08256aa934d3ac271'>Commit.</a> </li>
<li>Warn users if BLOB was disabled in the camera due to usage of external guiding applications. <a href='http://commits.kde.org/kstars/9e82b30fe779d138741c38826acd5e9b4ca3069a'>Commit.</a> </li>
<li>Set the appropiate FITS view type. <a href='http://commits.kde.org/kstars/3b7d6e9472988dd55b03a2be3b0f36553ba4a3cd'>Commit.</a> </li>
<li>OSX Bundle improvements. <a href='http://commits.kde.org/kstars/09efca463b23dc4f9da6404711dfed0f9ae1001e'>Commit.</a> </li>
<li>Fix today calender icon not showing up. <a href='http://commits.kde.org/kstars/3b23570cf79bde3a1989b609fd1234dae6799162'>Commit.</a> </li>
<li>Not cancel slot. <a href='http://commits.kde.org/kstars/80d70f5a1a5a47d3d10f57696a617c605f15870c'>Commit.</a> </li>
<li>Only reset file on startup. <a href='http://commits.kde.org/kstars/a73a386db19075994a298b5e00e407ce0d07a734'>Commit.</a> </li>
<li>Fix ifdefs to compile without cfitsio/lite. <a href='http://commits.kde.org/kstars/c29ef8466cdca80e4b1a34245a48dd3389e2b14f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129387'>#129387</a></li>
<li>XPlanet Fixes and Bundle Defaults. <a href='http://commits.kde.org/kstars/eb2453ac7954c5c0a8da5749591f2d37faf497e9'>Commit.</a> </li>
<li>Added functionality for Update button. Now, the AltvsTime widget will recompute all the graphs points when the Update button is pressed. <a href='http://commits.kde.org/kstars/5b4cc61cfce1cdce9ecd863ebda662446ba06afc'>Commit.</a> </li>
<li>Night theming for OSX. <a href='http://commits.kde.org/kstars/8bf445ee4b10edcf488f88cd2b69699bb3165648'>Commit.</a> </li>
<li>KStars Lite - add sand clocks into the first startup. <a href='http://commits.kde.org/kstars/3606db6f376946e63ce14917448d6cd9912d10b7'>Commit.</a> </li>
<li>Apply capture filter for dark frames. <a href='http://commits.kde.org/kstars/9ca83ff28ecfdd671924a343bb9931877f4ded5e'>Commit.</a> </li>
<li>Check index do not crash. <a href='http://commits.kde.org/kstars/3b922408e2058f32ca4fd893c7aa251052d3e984'>Commit.</a> </li>
<li>New Lite Android version. <a href='http://commits.kde.org/kstars/b71de874ad7f8453bb50c0b1f32bd5d294cd7f98'>Commit.</a> </li>
<li>KStars Lite - added splash screen right after startup. <a href='http://commits.kde.org/kstars/3ef929f2b24781026e510d9ae57e743c09fc6705'>Commit.</a> </li>
<li>Change Ekos icon size to 48x48. Reset image progress value to zero on abort. <a href='http://commits.kde.org/kstars/68f277153c5539bf4b049e2ea6afad522a3583dc'>Commit.</a> </li>
<li>2.7.1 release. <a href='http://commits.kde.org/kstars/bbf323c25d01e35f19787f8a879b714774126a93'>Commit.</a> </li>
<li>Watch out for negative boundary values. <a href='http://commits.kde.org/kstars/36c93a9f34c55e8117000c15c5217df1846a6006'>Commit.</a> </li>
<li>Expand profile Pixmap to cover more area. Only submit state changes once per action, GUIDE_DITHERING_SUCCESS was being emitted twice. <a href='http://commits.kde.org/kstars/bf3040dd58e253d5fbb7afe932e9a49c92ec21e4'>Commit.</a> </li>
<li>If no star is selected while waiting, change state to IDLE. <a href='http://commits.kde.org/kstars/390e58cc688c42e80c34f0cbb8eda393cfec5ceb'>Commit.</a> </li>
<li>Add new progress bar for current image being captured in the summary. <a href='http://commits.kde.org/kstars/9ad587c34f38bfb30f5d9626a1f8599e7d3c97db'>Commit.</a> </li>
<li>Retry failed tasks before giving up. Add new signal for image time progress. <a href='http://commits.kde.org/kstars/594cd91c274e52f623ef5e2689357c6f833eff6f'>Commit.</a> </li>
<li>Disable too verbose logging until necessary. <a href='http://commits.kde.org/kstars/a3dcdb295e8c59bd0f814cc5c3a4b2306bf5bd28'>Commit.</a> </li>
<li>Fix warning and add clarification message. <a href='http://commits.kde.org/kstars/4042778288810363dc1917c1492a575d0b7625e3'>Commit.</a> </li>
<li>Improve handling of sync and slew failure in align module. <a href='http://commits.kde.org/kstars/341b6e77742a4912a0af412ba65378088719d8c8'>Commit.</a> </li>
<li>Fixing problems with saving configuration on Android. <a href='http://commits.kde.org/kstars/64a2d1ed533ece24e6d85dfe6f960a9c56acb7c0'>Commit.</a> </li>
<li>Ensure box size combo index >= 0. <a href='http://commits.kde.org/kstars/cead4ef99dbe3d17cf0db5c00f3820a353abeb6d'>Commit.</a> </li>
<li>OSX and XPlanet fixes. <a href='http://commits.kde.org/kstars/1934a83957815df635d428294eadce809d113753'>Commit.</a> </li>
<li>Fix function name in namespace. <a href='http://commits.kde.org/kstars/ca6c401863b95a2d1577d37fb54679f72ecc73f7'>Commit.</a> </li>
<li>If user canceled dark frame capture then we set the options to off. <a href='http://commits.kde.org/kstars/4bd27900f17ff2becb9079494b8f7119ad5ddbd0'>Commit.</a> </li>
<li>If loadSlew is active, we do not check targetDiff value. <a href='http://commits.kde.org/kstars/e1beafd01a2f0ead08e162e6d03473dd1c18c074'>Commit.</a> </li>
<li>Adding support to other FITS data types. <a href='http://commits.kde.org/kstars/9ca7f7c39e3ba57db9b405ff8a68b1892197732a'>Commit.</a> </li>
<li>Make it compile on Android. <a href='http://commits.kde.org/kstars/ea574b6181da119e170a14614dcb58b991970a37'>Commit.</a> </li>
<li>Make moonless night fallback default. <a href='http://commits.kde.org/kstars/194102eac605c1f6bc27cedf182dbff2c9421732'>Commit.</a> </li>
<li>Save color scheme file to config. <a href='http://commits.kde.org/kstars/e7271785ddcbde7a73ef536591fc6608e113b92b'>Commit.</a> </li>
<li>Show splash first thing. <a href='http://commits.kde.org/kstars/53fbf26570bf052601c1114a71b60afa62fee2dd'>Commit.</a> </li>
<li>Do not include histogram in lite. <a href='http://commits.kde.org/kstars/329eb0fbf6382094adba978bcfbd1f8c8c503203'>Commit.</a> </li>
<li>More OSX bundle work to copy data if it does not exist. <a href='http://commits.kde.org/kstars/e93c4bdceeffee13f20aeb5f3db6a392a763634b'>Commit.</a> </li>
<li>Add KStars version to log. <a href='http://commits.kde.org/kstars/61f2c7bc41c0f179499465c64a0963ce9f4559a4'>Commit.</a> </li>
<li>Bump to 1.0.1. <a href='http://commits.kde.org/kstars/9cc29431e93680321e54c16a25c0f0978003b591'>Commit.</a> </li>
<li>Fix compile. <a href='http://commits.kde.org/kstars/12c581547ccfed9b1912b95d0f1320b6d11ee566'>Commit.</a> </li>
<li>Various fixes for KStars Lite. <a href='http://commits.kde.org/kstars/362394db85cd66bd556f62803493280dd43023b5'>Commit.</a> </li>
<li>Adapting FITSData to work for KStars Lite and adding a static function to create an autostreched QImage from a FITS File. <a href='http://commits.kde.org/kstars/7b5afc70b2fdd82640642d6ea40a6bd95a8f50ec'>Commit.</a> </li>
<li>QtConcurrent for milkyway seems to crash it sometimes to disabling it for now. <a href='http://commits.kde.org/kstars/9182ea136b1982120cd8680e333b57676db92039'>Commit.</a> </li>
<li>New notification system that can work for both desktop and mobile version. <a href='http://commits.kde.org/kstars/e37c3ee4c166fdcf3ad856da6bd37fbd1254cbc1'>Commit.</a> </li>
<li>KStars Lite - Change version of apk. <a href='http://commits.kde.org/kstars/a6d6d54a449fcafdaf7b75213ce62cb3967d1d4a'>Commit.</a> </li>
<li>SIGNIFICANT boost in startup speed from using a combination of delayed image loading, which also saves RAM, and QtConcurrent. The usage of QtConcurrent has to be carefully examined since we do want out-of-sequence loading where data cannot be read properly. So far it seems to work well but have to try it on Raspberry PI and also on Android. <a href='http://commits.kde.org/kstars/a4751fe5550c3b678c8df8e073112088183bd905'>Commit.</a> </li>
<li>Connect streamWindow when there is a valid object. Make it unique. <a href='http://commits.kde.org/kstars/09f7929799b17d2655029a5bdcd9bd83834d15b9'>Commit.</a> </li>
<li>Reset buttons fixes. <a href='http://commits.kde.org/kstars/b6c7370087936461364518a18ed80c1a8641866d'>Commit.</a> </li>
<li>Add more debug logging. Change status to progress when slewing is over. <a href='http://commits.kde.org/kstars/3de1e6f807975a823a582a957c9b88efc2170de8'>Commit.</a> </li>
<li>Break outside of else. <a href='http://commits.kde.org/kstars/b1ba2bcadc8eabc7a9e548ece860032d5458fd77'>Commit.</a> </li>
<li>Autostretch fixes. <a href='http://commits.kde.org/kstars/723b756011282f288c7bfb9288d53133ec989c42'>Commit.</a> </li>
<li>Use templated functions to reduce memory footprint of FITS operations. <a href='http://commits.kde.org/kstars/b2c8887e2f408c4dedf20b1f1b4e7cda468ce59a'>Commit.</a> </li>
<li>Solve bug https://bugs.kde.org/show_bug.cgi?id=371998. <a href='http://commits.kde.org/kstars/0e8ed22342fc0d2fa9ef629f1bc874fb4be82586'>Commit.</a> </li>
<li>OSX bundle fixes. <a href='http://commits.kde.org/kstars/61df10526344c10f489edff66cdf4e023a3bdefc'>Commit.</a> </li>
<li>Make auto debayering as an option so that it can be disabled if not desired. <a href='http://commits.kde.org/kstars/3b5c56479078084c7f949a6e5245fb8164c48167'>Commit.</a> </li>
<li>Compile without WCS take 2. <a href='http://commits.kde.org/kstars/a7603e2120a56cb10e1e3057c780e84b69fb47d4'>Commit.</a> </li>
<li>Compile without WCS. <a href='http://commits.kde.org/kstars/8e84022f58b84155a45eb6c818472fed11a52c14'>Commit.</a> </li>
<li>Treat empty drivers as invalid as well. <a href='http://commits.kde.org/kstars/3435bcf6025f5bb016e62f7e9a5ec3cf4f937fdd'>Commit.</a> </li>
<li>OSX bundle fixes. <a href='http://commits.kde.org/kstars/87e676a61853d60a85dd4a92cadd80b56c7f11d0'>Commit.</a> </li>
<li>Improve file downloading to emit cancelled signal and show optionally a progress dialog. Also can directly save to file and now saves progressivly as not to consume a lot of RAM. <a href='http://commits.kde.org/kstars/65d0badca750abcdb352a6703d89a4935c59b7fd'>Commit.</a> </li>
<li>OSX Bundling patch. <a href='http://commits.kde.org/kstars/05b8f16b749b9f1637e31a10ed7c0a1aab80112f'>Commit.</a> </li>
<li>KStars Lite - Fix bug with INDI in Android version. <a href='http://commits.kde.org/kstars/2f928fbfa9441d5969121321731e1d6e3f9fb2e5'>Commit.</a> </li>
<li>KStars Lite - Fix size of icons in top drawer. <a href='http://commits.kde.org/kstars/3b3e27ea8c377f96d1cc95b15fb7abe0d3843dcf'>Commit.</a> </li>
<li>KStars Lite - Location name can be now automatically retrieved from internet while setting coordinates from GPS. <a href='http://commits.kde.org/kstars/6b9087d736598f4df5fef18fc5338cc431896119'>Commit.</a> </li>
<li>Cannot use UniqueConnection with labdas so going back to regular signals/slots. <a href='http://commits.kde.org/kstars/8c481c8f39662204e48f1f2838fba8a9e3c6b84e'>Commit.</a> </li>
<li>Make group as unknown if it is empty. <a href='http://commits.kde.org/kstars/31240e60138af9890001b38b330ff7bcc4a10498'>Commit.</a> </li>
<li>Disable debug comments that litter the logs. <a href='http://commits.kde.org/kstars/ff744aca3747e0b36d63b48637bbb542ee5704e3'>Commit.</a> </li>
<li>Various fixes for guiding. <a href='http://commits.kde.org/kstars/8450f009c2976ca7df915414855cd74eec172bb6'>Commit.</a> </li>
<li>Dithering success should keep busy signal. <a href='http://commits.kde.org/kstars/a4bb1d7b38d80ee5757a9e6806a96e4a43424f6c'>Commit.</a> </li>
<li>Increase edge limit to 15% for focus and guide frame to avoid edge star detection. <a href='http://commits.kde.org/kstars/85ac071561f8caa5d6cb106ede1f822ce87a495d'>Commit.</a> </li>
<li>Km --> AU. <a href='http://commits.kde.org/kstars/4bc9e9991175a5c6cfbff1252fa860cabaeeae4b'>Commit.</a> </li>
<li>Deprecess an empty point when we add a flag, otherwise use ra0/de0 from the object. <a href='http://commits.kde.org/kstars/2052307d9e427c7ebe795de634cabfd849cf4de5'>Commit.</a> </li>
<li>Fix manual focus dialog calculating wrong sky point based on bogus epoch. <a href='http://commits.kde.org/kstars/e1ecb2133ad21b2a3afbb13e493af06b64b95ed0'>Commit.</a> </li>
<li>Preliminary support to solving captured non-FITS images in Align module. Needs testing. <a href='http://commits.kde.org/kstars/cd598e8a0e1023c7c7ec62dc9c329c4dcd7b12cf'>Commit.</a> </li>
<li>Do not enable Align module if there is no scope. <a href='http://commits.kde.org/kstars/41aa2eb88d2a550ac83b8af970d3f1882d7e7b0c'>Commit.</a> </li>
<li>Set currentGoto mode value since the connect in the ctor gets only activated for explicit clicks. <a href='http://commits.kde.org/kstars/b3dc10d1475b0c5bec0a6bfac705409a21b2224f'>Commit.</a> </li>
<li>Set tab order. <a href='http://commits.kde.org/kstars/845d077a7a0084ba2a5109cc4823e028df200eaf'>Commit.</a> </li>
<li>Fixuifiles. <a href='http://commits.kde.org/kstars/137b2e0e977cbc840c08d08ce968a57b3fc78ee0'>Commit.</a> </li>
<li>Create file URL from Local File. <a href='http://commits.kde.org/kstars/6bda418a722e1f070d89ae22a9cea6ccf8b670c7'>Commit.</a> </li>
<li>Fix few problems that appeared after removing build_kstarslite. <a href='http://commits.kde.org/kstars/831d1d98fcd7bc9fbabef394e32b4b9730879316'>Commit.</a> </li>
<li>Removing build_kstarslite from repo and moving it to Github where it is maintained there. <a href='http://commits.kde.org/kstars/0327f0f6bc6f83090e011284a400c5e006b509dd'>Commit.</a> </li>
<li>Use the old-style connect for the deprecated function to make it compile on Qt < 5.6. <a href='http://commits.kde.org/kstars/40760c3c26b8693cf1eb606b733b3144443976b0'>Commit.</a> </li>
<li>ErrorOccured was introduced in 5.6 so account for that. <a href='http://commits.kde.org/kstars/316e19a9068724b06953d715e0ac1ab902d354ba'>Commit.</a> </li>
<li>Use local file otherwise it is broken under Windows. <a href='http://commits.kde.org/kstars/807467ead5cec5211b17946156aa5c152eca3131'>Commit.</a> </li>
<li>Invalid directory was being set since it was storing a file and not a path, fixed. <a href='http://commits.kde.org/kstars/a867d71f9b270e487d5b898ed5ad99956f0b1b21'>Commit.</a> </li>
<li>Fix Epoch range as it was limited to 99.99. <a href='http://commits.kde.org/kstars/f087747ac45dfbce3b76677b00035c8cb6eeaac9'>Commit.</a> </li>
<li>Use std::abs. Thanks to Christian Dersch for finding this issue. <a href='http://commits.kde.org/kstars/df74f9444dea8b78a3d0ef3508abdd9aea70f77d'>Commit.</a> </li>
<li>Call checkCCD if CCD_TEMPERATURE permission is not RO otherwise no need. Reset frame to zero in case we have invalid limits. <a href='http://commits.kde.org/kstars/c027f6849a6ed67dc258a2e0078a112314d2b713'>Commit.</a> </li>
<li>Set pulseTimer to be oneShot. <a href='http://commits.kde.org/kstars/51270088d1d51570867cefffd2c34d60c188756b'>Commit.</a> </li>
<li>Clear temperature out when switching cameras. <a href='http://commits.kde.org/kstars/f1da6f6e79e6970b0e5ef613116538a1245b0e2a'>Commit.</a> </li>
<li>Add 2.7.0 changelog from GIT. <a href='http://commits.kde.org/kstars/154a795ae4ac43b0a678af4dcd7ab72273fb9b8b'>Commit.</a> </li>
<li>Adding missing list icons. <a href='http://commits.kde.org/kstars/13c199346861e9483c0bc1d0ae6234ff0c443703'>Commit.</a> </li>
<li>Add two missing icons required for KNewStuff. <a href='http://commits.kde.org/kstars/70c3d290ecd11ec0798805c0b80ac3ebeaa243dd'>Commit.</a> </li>
<li>Bump version to 2.7.0 and arrange developers. <a href='http://commits.kde.org/kstars/524d99a19492c221693fe6913dd83425bfdd4d33'>Commit.</a> </li>
<li>Change start to stop for preview as well. <a href='http://commits.kde.org/kstars/3ec33f0f0649c7900c5b99edf33c4d99b31f1f46'>Commit.</a> </li>
<li>No need to unpack full image when we only need thumbnails. <a href='http://commits.kde.org/kstars/34d59206f9ef15981dad16b2e3443f50f8c15395'>Commit.</a> </li>
<li>Also disable temperature checkbox if there is no cooler. <a href='http://commits.kde.org/kstars/523dfedb6333420c40f6e3e0d698ccd109b4f3cd'>Commit.</a> </li>
<li>Fix reading raw files bugs. <a href='http://commits.kde.org/kstars/dc9fb7ea530356adb5953233b397cf6f58c63609'>Commit.</a> </li>
<li>Always reset frame when doing calibration. Use pulseTimer instead of single shot so that we can stop it on abort. <a href='http://commits.kde.org/kstars/eaafa9317b3a7c31c711217521c531aabeba2d4d'>Commit.</a> </li>
<li>Add LibRaw as optional dependency to handle reading of RAW files recieved from cameras. This enables multiplatform support for handling RAW files as libraw is available in major platforms. <a href='http://commits.kde.org/kstars/bef4d86e2a7762f1fbe3bdd25c52fb54fd557790'>Commit.</a> </li>
<li>KLauncher handling on OSX. <a href='http://commits.kde.org/kstars/0dbb399d00316aa8af3eae9020a88849f60a3cc8'>Commit.</a> </li>
<li>Add support to NEF handlign. I probably need to utilize libraw instead of dcraw so that this can work multiplatform. <a href='http://commits.kde.org/kstars/f27f7b4b58c8d40fa547a8444b6fccbe121b1079'>Commit.</a> </li>
<li>Update icons in KStars Lite. <a href='http://commits.kde.org/kstars/ed8bc05be1c44da583e4995c8dbd03ad4f3f1ae2'>Commit.</a> </li>
<li>More OSX window fixes to stay on top. <a href='http://commits.kde.org/kstars/cc5fc1c2b6499fc7df317126eb69a45ae07a1359'>Commit.</a> </li>
<li>Stop align timer on solve fail. <a href='http://commits.kde.org/kstars/40ad4565091677416d6c52b2f21632e0929d5352'>Commit.</a> </li>
<li>Put connect statements when we run solver since we disconnect the slots upon exist. <a href='http://commits.kde.org/kstars/f7586602344cb7bb3e4c4ec9dfb508e3fbd27724'>Commit.</a> </li>
<li>Fix GUI of add deep sky. <a href='http://commits.kde.org/kstars/db63f1a530cb391796ae0402c6cc0b4250215546'>Commit.</a> </li>
<li>Retain frame value when receving updated CCD_INFO property unless they are out of bounds. <a href='http://commits.kde.org/kstars/4e245ad02ebb2887dfc6f96a13684cef60e4b04a'>Commit.</a> </li>
<li>Always use the POSIX INDI client under Linux to match what users use by default. <a href='http://commits.kde.org/kstars/4031709511427971711f2fe9a8cbf748241ea25f'>Commit.</a> </li>
<li>Add log comment when timing out. Must find solution for loadAndSlew condition on timeout. <a href='http://commits.kde.org/kstars/3b0bf48ad42c63742052247b102c737618d24459'>Commit.</a> </li>
<li>Add parity detection to astrometry modules and it is reused on next captures unless a solver fails then it gets reset to both. Adding alignment solver timer as well to watch for timeout. <a href='http://commits.kde.org/kstars/6dfa89afc8171d045af5374e1513fd1ccceabe01'>Commit.</a> </li>
<li>Reset the guide state on calibrate. Show detected stars when doing auto-star selection. <a href='http://commits.kde.org/kstars/931a353b98211a0f253ff396b7441f99d4b7b5a2'>Commit.</a> </li>
<li>Fix buttons in OSX. <a href='http://commits.kde.org/kstars/f9846aaebea554b4d8a2da68b20037048d128d82'>Commit.</a> </li>
<li>Make Windows as tools on OSX to workaround some Qt bug where they get behind main window. <a href='http://commits.kde.org/kstars/de60e6f54ddc1f3b96344f468c583f0325395be0'>Commit.</a> </li>
<li>Added new splash screen and icon in KStars Lite. <a href='http://commits.kde.org/kstars/fe65e034f20c6d90446fb950af83a4ba7adb5cb5'>Commit.</a> </li>
<li>Fixing more issues with guiding and subframing in auto-calibrate-guide mode. <a href='http://commits.kde.org/kstars/684cb8290cc3b59938654d53116ea001914ffb7f'>Commit.</a> </li>
<li>Use flag to know if calibration was ever completed instead of checkin for prior status. Fix dark frame bug. <a href='http://commits.kde.org/kstars/65db5ceb2d5d669bf3c6d31e04c7bc22d7126c34'>Commit.</a> </li>
<li>Some fixes to guiding with dark frames. <a href='http://commits.kde.org/kstars/c9930ea268c9667b9368d98f4ecd0d7f128fa927'>Commit.</a> </li>
<li>Use continue cancel dialog. <a href='http://commits.kde.org/kstars/fc982bf67c311ca1aa1bd418f2215f1380859d18'>Commit.</a> </li>
<li>Add button to display INDI logs. <a href='http://commits.kde.org/kstars/d26bea32d853d593d66337aea0ccff05d6394dad'>Commit.</a> </li>
<li>Fix context menu location in FITSViewer. <a href='http://commits.kde.org/kstars/043e2e6d247786c3a5005a480c76144c1c29a2d6'>Commit.</a> </li>
<li>For dithering, just set initial state and let the workflow handle it. <a href='http://commits.kde.org/kstars/3ec82ddde7089b6c8df8849b5a216a8d6dab4a1a'>Commit.</a> </li>
<li>Better use QDir::tempPath() directly. Also monitor if process was started at all or if an error occured and handle it. <a href='http://commits.kde.org/kstars/b3633c3dcf767d97eeeafa0782c4a571b3410a9e'>Commit.</a> </li>
<li>Add timeout to each stage of the startup/shutdown and restart procedure in case it timeouts. <a href='http://commits.kde.org/kstars/77352b6e5b0e45db5912b385e68484355ade41ee'>Commit.</a> </li>
<li>Use system temp path from QStandardPaths. <a href='http://commits.kde.org/kstars/3e0de03371c5ddb21febee52ead7e608c9ef1686'>Commit.</a> </li>
<li>Adding context menu to FITSViewer just like SkyMap. Label fixes. New center telescope icon. <a href='http://commits.kde.org/kstars/9e0e602c56e7d0fdd5ca787b505ce679ebc0e71c'>Commit.</a> </li>
<li>Fix crash due to activeJob pointer not being reset. Make sure all queue icons are disabled during sequence execution. <a href='http://commits.kde.org/kstars/09e751f6f7415dc0281d612284975b4601760a48'>Commit.</a> </li>
<li>Use a timer to check if guiding deviation are still not resolved after 1 minute. If that happens then complete abort of the sequence occurs. <a href='http://commits.kde.org/kstars/36c6899a433c295bb36a79cbd12c4d92c8fdac7e'>Commit.</a> </li>
<li>Gradient algorithm should only be run on subframes as running on full frame images would make it impossible to run. <a href='http://commits.kde.org/kstars/8d5cca1e553a7e101b1aef517e8d4506d43d72b2'>Commit.</a> </li>
<li>Set ST4 default driver. <a href='http://commits.kde.org/kstars/e5abf7c1e2e527a98be197d310dba199d6a0abc5'>Commit.</a> </li>
<li>Fix setting of ST4 driver. <a href='http://commits.kde.org/kstars/1545e480dc33c9483d952dbbc0d1edfa65d893fa'>Commit.</a> </li>
<li>Enable guide log file. <a href='http://commits.kde.org/kstars/2632b0c81d2db12cd85f3ee8864e831bab082ef1'>Commit.</a> </li>
<li>Added support for dark theme in KStars Lite. <a href='http://commits.kde.org/kstars/abce52fc4b7605fb53afad1c03b59c8ff3c0fc9d'>Commit.</a> </li>
<li>Using QtConcurrent to parallelize some aspects of KStars. Now WCS is running in a seprate thread and this brought opening time from 5 seconds to less than 1 second in WCS-enabled frames. <a href='http://commits.kde.org/kstars/b1404a252196a4459b59446a89f138a0d94f40b1'>Commit.</a> </li>
<li>Separating object info and gridlines. New telescope center icon. Speed improvements. <a href='http://commits.kde.org/kstars/099173ed040faea8568877bf93b6dda69d4f1bec'>Commit.</a> </li>
<li>Fix currentZoom being set to 0 if view width is a lot smaller than image width due to using floor(). <a href='http://commits.kde.org/kstars/699a1b7113221fceedad40abb0963236188d6851'>Commit.</a> </li>
<li>Reset default focus square to 64 and guide square to 32. <a href='http://commits.kde.org/kstars/4b6d88b9f9b7529ca2598c2b99a2ac43fba3ab0f'>Commit.</a> </li>
<li>Immediately send heart beat upon connection, do not rely on timeout. <a href='http://commits.kde.org/kstars/f1e5848f26d46ec16872d579bafa95bf71776d23'>Commit.</a> </li>
<li>If null is sent to any param, ignore it. <a href='http://commits.kde.org/kstars/0a4bb6bea350f7a4d0f2cbd8f3c50176f0df0df6'>Commit.</a> </li>
<li>Set binning in cmath to calculate final frame w and h. <a href='http://commits.kde.org/kstars/69c6da9c170872da03adb83a40bb3936215ae519'>Commit.</a> </li>
<li>Use correct timeout for INDI and Ekos timeouts. <a href='http://commits.kde.org/kstars/dc272db82feb13543a38ed7a3edf6fadcf2d2ab0'>Commit.</a> </li>
<li>Increase subframe capture size since star can sometimes move a bit around and we do not want to lose it. Also reset first load upon subframing so that the frame is adjusted properly. <a href='http://commits.kde.org/kstars/d7d72589b6dcb9849b7f240a99ee8f03b0d1bfaa'>Commit.</a> </li>
<li>Remove space. <a href='http://commits.kde.org/kstars/139cc1cf9e5b91297aab75922f2bbc7558893500'>Commit.</a> </li>
<li>Always use full width & height in Align module. <a href='http://commits.kde.org/kstars/51210154c36ee28ad803099a85bebedcfca49051'>Commit.</a> </li>
<li>Fix building error. <a href='http://commits.kde.org/kstars/6f8ef3e2e46f510ebf85f3cb903f6a6deb5a811a'>Commit.</a> </li>
<li>Add support to identifying objects within a WCS enabled image. <a href='http://commits.kde.org/kstars/816026d08b41b8c04cec5b27521aaab77b4df9ae'>Commit.</a> </li>
<li>Sync job to gui should include remote dir and upload mode combos. <a href='http://commits.kde.org/kstars/c5862ad311343ff0f56fc7742dc58c1011cf2749'>Commit.</a> </li>
<li>Add upload mode and remote dir options to capture jobs. <a href='http://commits.kde.org/kstars/918e01dcb3d3232e785a6ca56be38d8f077a2e61'>Commit.</a> </li>
<li>Do not update remote astrometry binary file automatically. <a href='http://commits.kde.org/kstars/944a3c7cadb6f1d6a316953887f821d2a81f5f26'>Commit.</a> </li>
<li>Fix few issues with guiding restart and stop. <a href='http://commits.kde.org/kstars/9e8cb4a931891275d62a880fd4e94ed130fc0b94'>Commit.</a> </li>
<li>Reduce massRatio threshold to 1.5 to include stars in noisy images. <a href='http://commits.kde.org/kstars/25296476ddf684d602035d68aad427c20335ec55'>Commit.</a> </li>
<li>If we are in autofocus, we need to proceed with capture after autostar is selected without subframe. <a href='http://commits.kde.org/kstars/8bd4d477681550a1b6cd8d2398e725b6bdd9f4ea'>Commit.</a> </li>
<li>Reset state to GUIDE_GUIDING after a successful dithering. <a href='http://commits.kde.org/kstars/38d88a10185b51e4fd234b6f7741df2ace6c1766'>Commit.</a> </li>
<li>If guide status > GUIDE_GUIDING set aborted state as suspended. <a href='http://commits.kde.org/kstars/2c7170381efddc5a20b66253d4d0e4c413d29830'>Commit.</a> </li>
<li>Fixing multiple focus issues. <a href='http://commits.kde.org/kstars/6033a718c648670509360c974f4acf3d7194b652'>Commit.</a> </li>
<li>Commenting out test case. <a href='http://commits.kde.org/kstars/4098038f612ca8c61e164f0871b7568769df15c0'>Commit.</a> </li>
<li>Update focus DBus function calls. <a href='http://commits.kde.org/kstars/60590ccec6ec1b592e904df5779b59defad29ddb'>Commit.</a> </li>
<li>Restrict search area for stars also in focus mode as to avoid edge cases. In canny detector, calculate total mass ratio among potential regions and select one with the highest mass ratio. If the ratio is small then it is most likely just noise. <a href='http://commits.kde.org/kstars/f05f6fd325a22415c7e1caaa7a8e329a77f49177'>Commit.</a> </li>
<li>Set default focus box size to 32. <a href='http://commits.kde.org/kstars/01ec4640f1f1635aa11198ccbc152eb7f7ff42f4'>Commit.</a> </li>
<li>Connect focus newHFR to capture module. <a href='http://commits.kde.org/kstars/349557d75b1e538fda561be09078f57d5b57253a'>Commit.</a> </li>
<li>Only abort due to guiding errors if we are taking a light frame. Also check for focus status failure flag. <a href='http://commits.kde.org/kstars/f442343498f08cfde9a96507bcdb34460a6a3031'>Commit.</a> </li>
<li>Use colors from current color scheme in KStars Lite pages, drawers and context menus. Change the way INDI client is built and included in Android version. <a href='http://commits.kde.org/kstars/5e935c6519ec7df67c63fc99768d33012c683263'>Commit.</a> </li>
<li>Fixing tab order. <a href='http://commits.kde.org/kstars/f6022ae7d7ff3e82060ac956bf4eb856602ad265'>Commit.</a> </li>
<li>New gradient-based sobel algorithm is mostly working now, need mass testing. <a href='http://commits.kde.org/kstars/9817e1b4980ca942bd1f05fe973343b342d0863e'>Commit.</a> </li>
<li>Algorithm mostly working but would fail if multiple stars within same frame. Trying out region partitioning later on. <a href='http://commits.kde.org/kstars/c971a38fe660b159061445d06f27c9a41b7fb9ae'>Commit.</a> </li>
<li>Libnova documentation interfers with KStars, so removing it. <a href='http://commits.kde.org/kstars/84fcffa3247fe9df4d8e437e959538779c5acec9'>Commit.</a> </li>
<li>More progress on gradient star detection. Adding option to select which algorithm to use in Focus module. <a href='http://commits.kde.org/kstars/b661e4423140090375fa3e2bc0e131a144d39f8c'>Commit.</a> </li>
<li>Reduce debug output. <a href='http://commits.kde.org/kstars/a12e12fb9c1e1f967e3c53af7b829423a4b7bce2'>Commit.</a> </li>
<li>More work on canny star. <a href='http://commits.kde.org/kstars/a663435ecf8c6fd22ca0d2af801a5fe8777f0ef5'>Commit.</a> </li>
<li>Always make generated file sequence number last so we can easily parse it and lift artificial limit of 999. <a href='http://commits.kde.org/kstars/80b6a2ad16f3c1067a10a3014d77520d841842da'>Commit.</a> </li>
<li>Improving the GUI a bit and grouping comming settings togeather. <a href='http://commits.kde.org/kstars/53b3eeca5e2763099307cd99471be69a2ff7162e'>Commit.</a> </li>
<li>Create android_libs folder on startup of build-kstarslite.sh. <a href='http://commits.kde.org/kstars/7c266f16be9d0910687ba8b0a41f4549e74d7367'>Commit.</a> </li>
<li>Added KStars Lite building script. <a href='http://commits.kde.org/kstars/5686e05ec8054ede67445fa5f8476466b1f67b7f'>Commit.</a> </li>
<li>More progress on the gradient based detector. <a href='http://commits.kde.org/kstars/6486d96f8cb8ee4eba1ca37ed794bc3af3248c1f'>Commit.</a> </li>
<li>Starting on canny based single star detector. <a href='http://commits.kde.org/kstars/46999cdea88435870d1af6401b3371b477f9ec3c'>Commit.</a> </li>
<li>If there is no focuser still then return. <a href='http://commits.kde.org/kstars/42391b18110ff8aefdc2dd2589f701cf596f4e14'>Commit.</a> </li>
<li>Adding support to panning and trackpad gestures for panning and zooming plus other improvements. <a href='http://commits.kde.org/kstars/5934307746b68cd09810cb0ec4a4f0226ce12688'>Commit.</a> </li>
<li>Adding initial canny edge detector. <a href='http://commits.kde.org/kstars/c63569a7fdf1aacd87378df2ce956f7072e71306'>Commit.</a> </li>
<li>Add necessary getStatus functions. <a href='http://commits.kde.org/kstars/4ad48a3b9b7725fc5cce2d6afaa6991fc7cc3b3c'>Commit.</a> </li>
<li>Add getStatus function. <a href='http://commits.kde.org/kstars/b57daae071dac84d340feb6c8a724cdb22457d28'>Commit.</a> </li>
<li>Update the scheduler to use commands of updated Ekos modules. <a href='http://commits.kde.org/kstars/4c66d46786b5ccd78a0760607140e24cc288e77b'>Commit.</a> </li>
<li>Fix crash in observing list whenever catalog configuration was changed. <a href='http://commits.kde.org/kstars/f11601a3f2a7dd99f1fba49425372baf8cc462fc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345348'>#345348</a></li>
<li>Add utility method to create a QList<T *> from QList<QSharedPointer<T>>. <a href='http://commits.kde.org/kstars/42d4149a44d26f92be59de57cce5fa484cd980c6'>Commit.</a> </li>
<li>More FIXMEs. <a href='http://commits.kde.org/kstars/e108f42e4467070221ff33a83695349cbdb8328f'>Commit.</a> </li>
<li>Updating icons to latest and fixed Breeze icons. <a href='http://commits.kde.org/kstars/c1247ca1c7c230eb63886c3488a0f99900fe9cad'>Commit.</a> </li>
<li>Use QStandardPaths to find INDI paths. <a href='http://commits.kde.org/kstars/ab6be7bbd4a22fec4796530d1f17be3534487c72'>Commit.</a> </li>
<li>A commit full of FIXMEs and hardly anything else. <a href='http://commits.kde.org/kstars/ddb83b14269784f9ac22f262f1a33eeb190821fd'>Commit.</a> </li>
<li>Avoid truncating the wishlist if there's a crash while saving it. <a href='http://commits.kde.org/kstars/30862b948a5afce39a3e3d85bd6964220993444f'>Commit.</a> </li>
<li>More work on porting Altvstime to QCP 2.0. <a href='http://commits.kde.org/kstars/1daa4f735b49943625f9420b1a92455841ed5ddc'>Commit.</a> </li>
<li>GUI changes, simplifyig interface and operations. Lots of fixes given multiple options. <a href='http://commits.kde.org/kstars/5ca9db728cf13b892afe3b59d76ab508809baedf'>Commit.</a> </li>
<li>Start working on porting Altvstime to QCP 2.0. <a href='http://commits.kde.org/kstars/a47364f40ea29fe0104788d686049b3670dc011f'>Commit.</a> </li>
<li>Focus uses its own fits frame like guide module. Improve GUI. <a href='http://commits.kde.org/kstars/7958708473376a3610daf92a7c89c69834d942e7'>Commit.</a> </li>
<li>Improve GUI a bit. <a href='http://commits.kde.org/kstars/60755c23c186d1b0e8d0f46e282ccb2c2aab4db6'>Commit.</a> </li>
<li>Remove auto park since it is done in scheduler. Improve GUI. <a href='http://commits.kde.org/kstars/183aca085c9f1286fc4ba0c747dfb719d42df398'>Commit.</a> </li>
<li>Improve GUI. Fix resume/suspend action. <a href='http://commits.kde.org/kstars/b19391ed6a6106a048ae0fbcaea80eda286add69'>Commit.</a> </li>
<li>Add missing overload for CachingDms::setD(). <a href='http://commits.kde.org/kstars/682bf87410a62ed580c45d3f4f7326c9a0fcdfc0'>Commit.</a> </li>
<li>Adding zoom to fit and central crosshair. Grid lines TODO. <a href='http://commits.kde.org/kstars/f40ddf7208ae3a0f23b04fb880db708d325d8af0'>Commit.</a> </li>
<li>FIx issues when switching guiders and connecting to guider with existing guide state. <a href='http://commits.kde.org/kstars/6d23b0c93639ef2f9244a5bf6c8f9f07a2748e57'>Commit.</a> </li>
<li>Few fixes. <a href='http://commits.kde.org/kstars/144b2f3f8ce6064e39858a1b86c20fbff5a52ffc'>Commit.</a> </li>
<li>LinGuider is now supported. Needs more testing of course. <a href='http://commits.kde.org/kstars/de269d387142b6b6bae6c6820df7f7b5462ebb6b'>Commit.</a> </li>
<li>Display logs only when guide logging is enabled. <a href='http://commits.kde.org/kstars/deaf6c7335520547334c598b33bf73c6423eeca3'>Commit.</a> </li>
<li>More progress on linguider. small fix in phd2. <a href='http://commits.kde.org/kstars/22b6eadd58c0cf18541e247c656bbcb4d8528c95'>Commit.</a> </li>
<li>Use new signals for guide. <a href='http://commits.kde.org/kstars/90c09b9dba0b1f007e81e63c1613cc49e660464a'>Commit.</a> </li>
<li>Fix signal/slots connections. <a href='http://commits.kde.org/kstars/b7b5b9ac087c2cc5b2417ce69e57af9ee7cc4ac2'>Commit.</a> </li>
<li>Moving post capture script to sequence queue where it belongs. <a href='http://commits.kde.org/kstars/25c6335a04785ac6a92a5ac2320065d1b9a94512'>Commit.</a> </li>
<li>Fix wrong state names. Need to change them all anyway. <a href='http://commits.kde.org/kstars/e9a3db10dfdcddb90825a7b3f081b5826bc8c908'>Commit.</a> </li>
<li>Should be at first tab. <a href='http://commits.kde.org/kstars/8652a42730897d59caf18ae03baf1e718e29c25e'>Commit.</a> </li>
<li>Adding initial skeleton for linguider support based on PHD2. <a href='http://commits.kde.org/kstars/095e5e8fff3d7b4c1479303e5d058626ec8b86a7'>Commit.</a> </li>
<li>PHD2 is mostly working now in the Ekos updated guide module. <a href='http://commits.kde.org/kstars/0a7a4d87ae6bcd94596a674d963951aae615fee0'>Commit.</a> </li>
<li>Adding option to enable/disable guide images coming from external guide applications. <a href='http://commits.kde.org/kstars/d56d662db3c30832d3c9fdd5699258f92d82302e'>Commit.</a> </li>
<li>Add missing breeze icon. <a href='http://commits.kde.org/kstars/be7dc226deac69b39635fe55c411f37d816f6d97'>Commit.</a> </li>
<li>Search by name and then by label and only if found add it to pi exec drivers. <a href='http://commits.kde.org/kstars/e774127ffca03dab4adad276070036f4998441d5'>Commit.</a> </li>
<li>Fix zoom to center to correctly reflect coordinates of the viewport. <a href='http://commits.kde.org/kstars/eb99d58da4ec759f5f9ef353af9852c950cdc3e3'>Commit.</a> </li>
<li>Init FITS url path to be the user home directory. <a href='http://commits.kde.org/kstars/dfd322c407df8c397d9326e9ad932034691de5ae'>Commit.</a> </li>
<li>More progress on internal guider implementation. <a href='http://commits.kde.org/kstars/9d9d7490c14479c5c99cd0130feea64f9a39527d'>Commit.</a> </li>
<li>Use fixed size for all buttons. <a href='http://commits.kde.org/kstars/484f8f599fb09da4abba301431b6c28b2015e04a'>Commit.</a> </li>
<li>Refactor name to be more standard. <a href='http://commits.kde.org/kstars/a1c4eeefe26736fa7f8ca14f8fd2f664a996d28a'>Commit.</a> </li>
<li>Zoom to center by default. <a href='http://commits.kde.org/kstars/d208d824f48b8e6de62680eb35e535850e050b92'>Commit.</a> </li>
<li>Set button policies to fixed. <a href='http://commits.kde.org/kstars/367b94cd51582de0d749edd4a9c36b3c9f897a6f'>Commit.</a> </li>
<li>Checking for timeout in both Ekos starting phase and when connecting INDI devices. Better fail than wait forever. <a href='http://commits.kde.org/kstars/ab1f93612a42eb6efefd285fd483a1af6a619f3a'>Commit.</a> </li>
<li>More reliable way of checking if Ekos devices are connected. <a href='http://commits.kde.org/kstars/08f535009a8530e07274d08abb334e132b057b46'>Commit.</a> </li>
<li>More progress on guide graph. <a href='http://commits.kde.org/kstars/c146cc950e84bf9d9cc4e95b5403e297995dae76'>Commit.</a> </li>
<li>Update slots. <a href='http://commits.kde.org/kstars/a3baa10339d9b927805b7ed00f7518214961bbe9'>Commit.</a> </li>
<li>More progress on guide module. <a href='http://commits.kde.org/kstars/fcc87c1b3c8fa672af7f9ef4a7845200d1e7827d'>Commit.</a> </li>
<li>Disabling a few calls uncompatible with the new QCustomPlot. Need to update it to use QCPAxisTickerTime later on. <a href='http://commits.kde.org/kstars/fb55ed2fb7a84d468b8e94d5181c80b733d7e376'>Commit.</a> </li>
<li>Update to 2.0 beta of QCustomPlot. <a href='http://commits.kde.org/kstars/4e3fd8cb5a2187ab0c61e10ad99b2d0ffc927215'>Commit.</a> </li>
<li>Fixing slot issue with observer manage. <a href='http://commits.kde.org/kstars/83e46709c1209d3aaed257c22a18976dbe25334d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370061'>#370061</a></li>
<li>Disabling some icons which are missing. <a href='http://commits.kde.org/kstars/82eb8423c9e95f4ff1551a77da7d7161384146b6'>Commit.</a> </li>
<li>More progress on guide operation. Now internal guiding basically works. Need to use QCustomPlot for plotting instead of legacy one. <a href='http://commits.kde.org/kstars/40b5cc74d58befa4e0b0aa5c120df232efba561f'>Commit.</a> </li>
<li>Histogram object is not strictly necessary in star finding. <a href='http://commits.kde.org/kstars/8e306483e9baf50482dff753e4a6ccadc87e7037'>Commit.</a> </li>
<li>Various fixes for OSX and icons. <a href='http://commits.kde.org/kstars/e78a098148ae1cdf061cc213adc68503c6f72e7f'>Commit.</a> </li>
<li>Fix bug with horizontal grid (revert to old way of calculating cosAlt if new way produced 0 value). <a href='http://commits.kde.org/kstars/68523926371d99c8427654fff1dcf12b0a5cf263'>Commit.</a> </li>
<li>Starting work on guide module after calibration is almost done in internal guider. <a href='http://commits.kde.org/kstars/bbd21bdf708ba5796a6ab5c9d241773daa822c4f'>Commit.</a> </li>
<li>If no crosshair is present, center around the tracking box on mouse wheel events. <a href='http://commits.kde.org/kstars/05f6fa0dc5cd09a1a493cef8d8f0dbcd47a36584'>Commit.</a> </li>
<li>Master sync. <a href='http://commits.kde.org/kstars/373c3ebeac6e75dacc84f5298cbcfb0f321726aa'>Commit.</a> </li>
<li>Fix issue with lines not extended to horizon in KStars Lite. <a href='http://commits.kde.org/kstars/fe69ba4fdae4fbfdbe2671cddd204467097f82f2'>Commit.</a> </li>
<li>Fixed issue when lines were not extended up to horizon. <a href='http://commits.kde.org/kstars/94b829b2497daee7c32a10ce7246ece74e027314'>Commit.</a> </li>
<li>Fix minor typo. <a href='http://commits.kde.org/kstars/3bccca1c952ed678859b51944447dbb9214e7a5e'>Commit.</a> </li>
<li>Prevent unsafe connect by accepting slot in KSDssDownloader constructor. <a href='http://commits.kde.org/kstars/0bdaf2dcadcd0b014d2789dfae5481a8a4a9026c'>Commit.</a> </li>
<li>Fix crashes in KSDssDownloader. <a href='http://commits.kde.org/kstars/55a83c05d27c22c68a84b2e217da26c0b50c2b29'>Commit.</a> </li>
<li>Show some basic info about the object in a easy-to-access label. <a href='http://commits.kde.org/kstars/f5a4b8cce5fe6d2aaead5da6a6f2abbb7c4123c3'>Commit.</a> </li>
<li>Allow to demote objects in the Dobsonian hole. <a href='http://commits.kde.org/kstars/5f9bd2ef3b6609f3e2c5854c3da5da4a13854482'>Commit.</a> </li>
<li>Fix missing columns in Session View. Also show J2000 coords instead. <a href='http://commits.kde.org/kstars/cec7c8f99cbfcd1561a5feeda626dc9861c96081'>Commit.</a> </li>
<li>Finish up restructuring of Observing List UI. <a href='http://commits.kde.org/kstars/c990d40965b423bce2f80cdb86cb6b64c4f36372'>Commit.</a> </li>
<li>Restruct. Obs. Planner: More vertical real estate for the Table Views. <a href='http://commits.kde.org/kstars/cf1767bc7fc296500026f7b66547ab5d88657fca'>Commit.</a> </li>
<li>Fix custom catalog handling regression introduced by b32c9417353. <a href='http://commits.kde.org/kstars/81e301153e6e89ce14bfad8944f9b87025a3fad3'>Commit.</a> </li>
<li>Master sync. <a href='http://commits.kde.org/kstars/98649c60807d1635eb8d1c7a50261331fe8efe3f'>Commit.</a> </li>
<li>Robert fixes to to use only svg fallback all over instead of png oxygen. <a href='http://commits.kde.org/kstars/3ff4e3428c9f504bf20d20a4e0091c211340a507'>Commit.</a> </li>
<li>Fix the altitude % display in the observation planner. <a href='http://commits.kde.org/kstars/9c7736df8c283638fa39238792550fee8a058430'>Commit.</a> </li>
<li>Change behavior of deprecess to update RA0, Dec0 only if they are J2000. <a href='http://commits.kde.org/kstars/64ff47cec51687a8916b2b8c5a47754c22411aac'>Commit.</a> </li>
<li>Convenience method: recomputes both equatorial and horiz. coordinates. <a href='http://commits.kde.org/kstars/b1adcc89190e7dae6e9e697b2a6a493c323588c6'>Commit.</a> </li>
<li>Show the correct epoch in Details Dialog, and also the correct coords. <a href='http://commits.kde.org/kstars/7cb8bdab107b30eb4bfae5d542220071806b9c72'>Commit.</a> </li>
<li>Fix typo. <a href='http://commits.kde.org/kstars/79fac9a3a3fad916a64a38ded9f351a615ef6576'>Commit.</a> </li>
<li>Add some missing icons to the QRC. <a href='http://commits.kde.org/kstars/5497c1e271a77bc14c6a3a7df7af34962e681868'>Commit.</a> </li>
<li>Syncing with master. <a href='http://commits.kde.org/kstars/e252eeac05980be37d841dd2c4bd65f64ae83ff0'>Commit.</a> </li>
<li>Adding missing icons. <a href='http://commits.kde.org/kstars/bc1183697909df05033519b05a32e57ee3f1e01b'>Commit.</a> </li>
<li>Fixes icons on systems that cannot load icons from system. <a href='http://commits.kde.org/kstars/5b821a293398f43ad482ff7f8842b034c466ccc4'>Commit.</a> </li>
<li>New workflow in the Observation Planner: Sort by % max. alt achieved. <a href='http://commits.kde.org/kstars/b10766db6dd714337905394e4c240964381b1616'>Commit.</a> </li>
<li>Add methods: SkyPoint::maxAlt() and SkyPoint::minAlt(). <a href='http://commits.kde.org/kstars/94dc45f9511279d66f1923a2dc334b228803eeb7'>Commit.</a> </li>
<li>Replace includes by forward delcarations, change include order etc. <a href='http://commits.kde.org/kstars/922657abf4471555088d953578356d800f98d1df'>Commit.</a> </li>
<li>Avoid polluting the global namespace with Eigen stuff. <a href='http://commits.kde.org/kstars/a10ba5ed81e43545b4398dbb09047389940517d9'>Commit.</a> </li>
<li>Calibration mostly works now in internal guider. Need to test all cases after guiding and dithering are complete. <a href='http://commits.kde.org/kstars/29f66bff03d27e1a5b5ec958fa7cccf82ee7613c'>Commit.</a> </li>
<li>Minor GUI update. <a href='http://commits.kde.org/kstars/e191630a1685e7c0d361c3a8315f74d58bb2308f'>Commit.</a> </li>
<li>Minor GUI update to utilize space. Refactor sync. <a href='http://commits.kde.org/kstars/a8a8b94e76b0bdaa786ee4e87dbf382d378f754b'>Commit.</a> </li>
<li>Minor GUI change. Need to update to reflect guiding changes. <a href='http://commits.kde.org/kstars/c67acebdad5cdca7317249872659505d6ef7957b'>Commit.</a> </li>
<li>Refactor update. <a href='http://commits.kde.org/kstars/354950761b1aeb733f43510a6ead46894b272cce'>Commit.</a> </li>
<li>Loading guide frame in its own separate view without need for FITSViewer window. <a href='http://commits.kde.org/kstars/68711ba2a27f5a8d5f714e691e8d9f3c2cb80396'>Commit.</a> </li>
<li>Rearranging Guide states. <a href='http://commits.kde.org/kstars/12a6c78f4edd707530a8b862b954e4f5daa9e0ea'>Commit.</a> </li>
<li>Refactor changes. <a href='http://commits.kde.org/kstars/b291c293dd126aa91aeb032f4c95b0353e46adb6'>Commit.</a> </li>
<li>Streamline focus GUI in a grid. <a href='http://commits.kde.org/kstars/f06e813f22a5722cb40e1164d4e5838b18df89f6'>Commit.</a> </li>
<li>Fix headers <---> items in Observation Planner. <a href='http://commits.kde.org/kstars/4d0ae872e95efb865423d0f4b48b74686a938373'>Commit.</a> </li>
<li>Make profile images scalable without keeping aspect ratio, keep star images expandable with aspect ratio set. Use icons for Ekos options. <a href='http://commits.kde.org/kstars/18580ad8f114a018cdc5f5b8aed4edf151641cd3'>Commit.</a> </li>
<li>CachingDms test case for implicit conversion from dms. <a href='http://commits.kde.org/kstars/765ac46acc4aad48d18ebbfeb19dae5378561ba5'>Commit.</a> </li>
<li>New colum in the Wish List view for the new workflow: Current Altitude. <a href='http://commits.kde.org/kstars/74541e1bf7052ea403ec4d5ae3c717cc687d8292'>Commit.</a> </li>
<li>Refactor and simplify code in Observation Planner. <a href='http://commits.kde.org/kstars/e2a39fe8c13a48274a2b09f8994fa1373d03e486'>Commit.</a> </li>
<li>Cosmetic: Reorganize code in a more readable manner. <a href='http://commits.kde.org/kstars/99841a79284eaba6254d45b3219e7c86027cc425'>Commit.</a> </li>
<li>Warnings--, fix compile error on Mac OS X. <a href='http://commits.kde.org/kstars/de5463654e306a8b52db46c7d76571afd2ba1213'>Commit.</a> </li>
<li>Right-click now removes the crosshairs. Also, if there is a crosshair present then zoom in or out will try to center the view on the crosshair location. <a href='http://commits.kde.org/kstars/cce4f3016357362cac2e977a3afa5312155d2e18'>Commit.</a> </li>
<li>Fixing http://indilib.org/forum/general/1549-qt-error-when-building-kstars-from-source.html. Made Qt::AA_EnableHighDpiScaling used only in KStars Lite. <a href='http://commits.kde.org/kstars/531ec87a003e8a2c8c470696061c0ff4abc11502'>Commit.</a> </li>
<li>Disable dms and CachingDms counting instrumentation. <a href='http://commits.kde.org/kstars/14dc85d8322cc46bcb0d4b54f1f797935b54757e'>Commit.</a> </li>
<li>Revert "Experiment: Make Azimuth CachingDms". <a href='http://commits.kde.org/kstars/5a978fde16329dd13d41b376b3f5042861642bfc'>Commit.</a> </li>
<li>Constify CachingDms setters. <a href='http://commits.kde.org/kstars/b85333ab7c877f4dba627caf307a1f872240d39b'>Commit.</a> </li>
<li>Experiment: Make Azimuth CachingDms. <a href='http://commits.kde.org/kstars/fdf1cac2eb57430b86b8c63ffe842574467f6a77'>Commit.</a> </li>
<li>Revert "Experiment: Explicitly write out the computations of precession". <a href='http://commits.kde.org/kstars/4fd4a0067604c52b7a52da90fbbf2646daf53304'>Commit.</a> </li>
<li>Experiment: Explicitly write out the computations of precession. <a href='http://commits.kde.org/kstars/66a7ea2f3829bff896128836479bdf5fb365df48'>Commit.</a> </li>
<li>Disable time-measuring instrumentation in SkyPoint and StarObject. <a href='http://commits.kde.org/kstars/743cba78b14e7ec002484faa4e3e124deccf782b'>Commit.</a> </li>
<li>Reduce number of multiplications in SkyPoint::aberrate() by factoring. <a href='http://commits.kde.org/kstars/c5e709bd919f55d2335f22070b8d31ed5868efbb'>Commit.</a> </li>
<li>Update comments in SkyPoint::precess(). <a href='http://commits.kde.org/kstars/c1dc0b46e51424ffcc11b0cdf822852381442556'>Commit.</a> </li>
<li>Experiment: Is using Eigen::Matrix3d faster than 2D array and for loop?. <a href='http://commits.kde.org/kstars/d9158d4d986b29da2950a402612f01a919480fc6'>Commit.</a> </li>
<li>Improve efficiency of SkyPoint::angularDistanceTo() etc. <a href='http://commits.kde.org/kstars/32de27498abdaf3d001fee75c5a0b11a2e056178'>Commit.</a> </li>
<li>Reduce coordinates to the range properly. <a href='http://commits.kde.org/kstars/d4705745c916777eaa82606d156c23bc79d8bdef'>Commit.</a> </li>
<li>FIXME comments and cosmetic changes. <a href='http://commits.kde.org/kstars/7a5cca47ec521c39330a912e05c505ed2852495a'>Commit.</a> </li>
<li>Add tests to check SkyPoint::precess() and related routines. <a href='http://commits.kde.org/kstars/aa7dda063f1221b472f321dbc3c53af5e2dfbac3'>Commit.</a> </li>
<li>Reduce unnecessary include dependencies, use forward declarations. <a href='http://commits.kde.org/kstars/5ca4464f247eb3fed442eeb03bcac2f3ad558a60'>Commit.</a> </li>
<li>Virtualize dms setters to prevent wrong results if we convert to dms*. <a href='http://commits.kde.org/kstars/1a6d50b0553763c2c3418952f3e85b12359ab699'>Commit.</a> </li>
<li>Improve CachingDms use in SkyPoint. <a href='http://commits.kde.org/kstars/5c0232bf4518319350648efdb9e538c15a0209ed'>Commit.</a> </li>
<li>Fix accounting of bad cache uses. <a href='http://commits.kde.org/kstars/b3d5b616ed04dfe48f1c67c49243eab636816735'>Commit.</a> </li>
<li>Change some KSNumbers values to CachingDms to improve re-use. <a href='http://commits.kde.org/kstars/27d32e1ccedf1400e6cf5a2d34e542450ffa0eaa'>Commit.</a> </li>
<li>Improve use of CachingDms. <a href='http://commits.kde.org/kstars/6b9ddf72f775d45ff720858395ce316457714a25'>Commit.</a> </li>
<li>Inline inlinable methods in KSNumbers. <a href='http://commits.kde.org/kstars/2029a90da256d5b8e4795520a18a848320311a78'>Commit.</a> </li>
<li>A dms::reduceToRange() method. <a href='http://commits.kde.org/kstars/8fd7073964885ff9b8d21a509fa8a77cda49de52'>Commit.</a> </li>
<li>Profiling code to estimate the time spent in StarObject::udpateCoords(). <a href='http://commits.kde.org/kstars/6e4fff45e0f4e4db691da7ccb00d4932e9eb74a5'>Commit.</a> </li>
<li>FIXME! dms::reduce() doesn't modify the dms, it returns a new one. <a href='http://commits.kde.org/kstars/f838f2623d70d835a878a31f97cdc62833c7edd7'>Commit.</a> </li>
<li>Some minor optimizations. <a href='http://commits.kde.org/kstars/e01f22a4d3f5ea611dff9ae2f3bd37c31ccc64d5'>Commit.</a> </li>
<li>Make EquatorialToHorizontal faster by a number of improvements. <a href='http://commits.kde.org/kstars/786e4fcc5397fa8a9dcbb54c670410b687f000db'>Commit.</a> </li>
<li>Avoid resetting "catalog" coordinates if no proper motion was applied. <a href='http://commits.kde.org/kstars/69caa284c81f86204ff4b37b4670c5bb927cb223'>Commit.</a> </li>
<li>Make KSNumbers::obliquity() return CachingDms. <a href='http://commits.kde.org/kstars/284282fc64a0a5bf4f2b0e52a466a968c19ec113'>Commit.</a> </li>
<li>Change LST and latitude to CachingDms -- this saves a lot of trig calls!. <a href='http://commits.kde.org/kstars/13ac9e9eaadf61845fe54c13ba72fa23c01ddb56'>Commit.</a> </li>
<li>Show global number of bad CachingDms uses. <a href='http://commits.kde.org/kstars/5a9c6a424624b50fb071e171508cda5e4d565bab'>Commit.</a> </li>
<li>Add profiling code to catch bad uses of CachingDms. <a href='http://commits.kde.org/kstars/d9ec631bf8e4e5e2e09b991e82288499b954113f'>Commit.</a> </li>
<li>Profiling: Show caching information. <a href='http://commits.kde.org/kstars/62b1e6acf87b087aa3a3871b8f8bfe7f25ff603b'>Commit.</a> </li>
<li>Tests for CachingDms. <a href='http://commits.kde.org/kstars/da8bcf7b52407bf8b1bbb35e303cb77399e723c0'>Commit.</a> </li>
<li>A CachingDms class that caches trigonometric values. <a href='http://commits.kde.org/kstars/8c2dffafcf756f6216f0213bf1517d3e197110fd'>Commit.</a> </li>
<li>Calculate trig function redundancy within DeepStarComponent::draw(). <a href='http://commits.kde.org/kstars/37ba58d59222b77c663a460dfeb2fd7611119f4d'>Commit.</a> </li>
<li>Revert "Experiment 1: Can we cache trig values during first computation?". <a href='http://commits.kde.org/kstars/33b036190697d614c9f822f700350022461bd089'>Commit.</a> </li>
<li>Experiment 1: Can we cache trig values during first computation?. <a href='http://commits.kde.org/kstars/2d4f65a20b3da0bc7c8123a11da316c07d8b4b5c'>Commit.</a> </li>
<li>Optimizations in StarObject::getIndexCoords(). <a href='http://commits.kde.org/kstars/5da196c1adefe88e697a72cd23f4af4e5a998d9c'>Commit.</a> </li>
<li>Cosmetics. <a href='http://commits.kde.org/kstars/e7e88e17a0c70393a6409fe5546ad4f54e95c058'>Commit.</a> </li>
<li>Profile trigonometric function calls to dms objects. <a href='http://commits.kde.org/kstars/b91a0c3768d58c3c6b62fc0b3c8d50043d180add'>Commit.</a> </li>
<li>Profiling Code: Determine time spent in EquatorialToHorizontal. <a href='http://commits.kde.org/kstars/0860ef61054d5ceee931aa90806e16d99ad12b9a'>Commit.</a> </li>
<li>Profiling Code: Count trigonometric calls on dms, and profile them. <a href='http://commits.kde.org/kstars/25b07e4593ce0c0a41cdb3b706a16a2418fed826'>Commit.</a> </li>
<li>Fix include order; and cosmetic changes. (++krazy::happiness;). <a href='http://commits.kde.org/kstars/4549930403dbf94f891accb86139bf779b4ff326'>Commit.</a> </li>
<li>Use KStarsDateTime::stringToEpoch etc. in FocusDialog. <a href='http://commits.kde.org/kstars/f1be36f39a00d3c86c5a192dba28136191ba2ddc'>Commit.</a> </li>
<li>Fix epoch problems in KStarsDateTime. <a href='http://commits.kde.org/kstars/3aedbf074e715bd233c584f5c0161dc824c8c1ae'>Commit.</a> </li>
<li>Fix the definition of Julian Epoch in KStarsDateTime. <a href='http://commits.kde.org/kstars/90e7e72037b029f61d922ec2fc60e2f9135ec090'>Commit.</a> </li>
<li>More progress in guide module. Load & Save settings now work. <a href='http://commits.kde.org/kstars/22eefeb1cf928919bf9836a9d4279d99a763f47a'>Commit.</a> </li>
<li>More progress on guide and calibration options. <a href='http://commits.kde.org/kstars/ac085a82d59a55054fc5b2919d27bfb805b4d2ae'>Commit.</a> </li>
<li>Send heartbeat imediately upon reception of watchdog device. Then set the timer only when receiving back the heartbeat value. <a href='http://commits.kde.org/kstars/535189077de67e929a19bd9bbaa588f5628aaccc'>Commit.</a> </li>
<li>Fix typo that was causing a crash. <a href='http://commits.kde.org/kstars/e06921bc798c3bb8cab6ae4d9daf033f5d5f4112'>Commit.</a> </li>
<li>Working on implementing calibration and guiding options. <a href='http://commits.kde.org/kstars/642bbc648558124c9bd46b538dd1f9940acac9bb'>Commit.</a> </li>
<li>Updated splashscreen from Yuri. <a href='http://commits.kde.org/kstars/8a971e377efbdd8992957e95c23f0ebe1396634c'>Commit.</a> </li>
<li>Now it compiles but nothing works as most functions disabled. Need to work next on calibration and guiding options setup. <a href='http://commits.kde.org/kstars/cb0af5516e7c92e5ea55a40bc99e5cc7f0aeff3e'>Commit.</a> </li>
<li>More re-organization to the Ekos code base. Should be ready in a few days. <a href='http://commits.kde.org/kstars/e90f2e5566a827d0a2b0d0bc4fef4095c7dd07b0'>Commit.</a> </li>
<li>More progress of migrating guide stuff to new architecture, still a lot of way to go. Nothing compiles yet. <a href='http://commits.kde.org/kstars/5b5277427a6a23a5d9c86d9ff112816beab43192'>Commit.</a> </li>
<li>More progress on migrating guide stuff. <a href='http://commits.kde.org/kstars/120af427cfaa1bb4e9ecc4668eea5b6259abab14'>Commit.</a> </li>
<li>Add new guide interface files. Organizing Ekos hierarchy. <a href='http://commits.kde.org/kstars/e901f7d04d188540b5bafe1bbe9d4f2a9aacfed1'>Commit.</a> </li>
<li>Ekos GUI updates. <a href='http://commits.kde.org/kstars/9ee9a4ced09721754974ff1dc7ca4c015bce0518'>Commit.</a> </li>
<li>Ekos re-organization. <a href='http://commits.kde.org/kstars/8a9983e2d73ec2d30f87a88e719315b38da24d1a'>Commit.</a> </li>
<li>Moving all mount module files to their own dedicated directly. <a href='http://commits.kde.org/kstars/e1f76e51011e71bf0f92ee097663f0861c574e59'>Commit.</a> </li>
<li>Moving all scheduler module files to their own dedicated directly. <a href='http://commits.kde.org/kstars/c35aeefeec960297c5083dab56f48f5eb678a3fb'>Commit.</a> </li>
<li>Moving all guide module files to their own dedicated directly. Separating external vs internal guiding tools. Started on creating internal to unify all guiding commands so that they can be used for either internal or external guiding tools equally. Major redesign of guide module tab to make all information available in one place. Still lots of work in progress. <a href='http://commits.kde.org/kstars/abcbcd87736bcd2a76e20fe37f4982f0a775e7c9'>Commit.</a> </li>
<li>Moving all focus module files to their own dedicated directly. <a href='http://commits.kde.org/kstars/aeb023954713b066e43f7e85d4c44cdf50255915'>Commit.</a> </li>
<li>Moving all align module files to their own dedicated directly. GUI is simplified even more. <a href='http://commits.kde.org/kstars/f19e281f7d0d928ffe53c9592b46bab173d7a253'>Commit.</a> </li>
<li>Moving all capture module files to their own dedicated directly. <a href='http://commits.kde.org/kstars/37fecee9cca8503903c8e6e60ae74274bf5ad685'>Commit.</a> </li>
<li>Moving common Ekos files to the auxiliary directory for better organization. <a href='http://commits.kde.org/kstars/030e4855899a29a859f6681048986a3d8aa09313'>Commit.</a> </li>
<li>Removing polar alignment mode since it can be done directly so now less user hassle to switch modes. <a href='http://commits.kde.org/kstars/ab7cc640d6fbf0d969b2c97fc59c908833c572b1'>Commit.</a> </li>
<li>Draw FOV even while alignment is in progress to display mid progress results on the sky map. <a href='http://commits.kde.org/kstars/4212cea000d99a88bf0afda6ac43841482ead28a'>Commit.</a> </li>
<li>Several changes to the guiding GUI to make it more compact, still needs further improvements maybe with embedded FITS View. <a href='http://commits.kde.org/kstars/82083ae2781efd7fbfc19688c8ff2d21178e2418'>Commit.</a> </li>
<li>Move and rename startup and shutdown procedure to make GUI more compact. Reduce spacings and margins. <a href='http://commits.kde.org/kstars/fcdf2f94822e771b972a812516cb71e574f5d635'>Commit.</a> </li>
<li>Re-order groups to make it more compact. <a href='http://commits.kde.org/kstars/685791eb39cdaa42f08f3a0c1f0bb7d181a0c9c6'>Commit.</a> </li>
<li>GUI improvement to make Ekos viewable on low resolution displays. <a href='http://commits.kde.org/kstars/c808d927c2f3cc4b809f4adff06fe245b4d3b434'>Commit.</a> </li>
<li>Remove unused functions, compact GUI. <a href='http://commits.kde.org/kstars/cbb8de7a63e7096c94f253ac6a67e5ed8f952598'>Commit.</a> </li>
<li>Compact align UI, needs further improvement in layout. <a href='http://commits.kde.org/kstars/880bbb63632879ba3369915a4f5cfa585cfeefe2'>Commit.</a> </li>
<li>Fix EBN issues. <a href='http://commits.kde.org/kstars/cee6680c40740b8a687563553a9200e49b538d73'>Commit.</a> </li>
<li>Added author and credits. Added lock for centering in KStars Lite (if. <a href='http://commits.kde.org/kstars/6db7a79a16fe2b57b1640b1f7a5b7b9e49176ecd'>Commit.</a> </li>
<li>Get rid of unnecessary signals since it is all being tracked now by the state. <a href='http://commits.kde.org/kstars/270a2b74cda3ac1384cbdb4a00a4f9f9cc88bab6'>Commit.</a> </li>
<li>Use state change to signal actual events instead of different signals for all various status changes. <a href='http://commits.kde.org/kstars/1a460219cdcd179dc8a90185de82cc8baf320da3'>Commit.</a> </li>
<li>Use refactored name. <a href='http://commits.kde.org/kstars/6c580c2fe9432e8b775dfec393f8ef40aa20ef7b'>Commit.</a> </li>
<li>Remove unused signals since we rely mostly on module states now. <a href='http://commits.kde.org/kstars/8840a60a33df05e39d82ef660478d64f65d54651'>Commit.</a> </li>
<li>Get rid of obsolete signals and use lambdas where possible. keep track of modules states. <a href='http://commits.kde.org/kstars/bd278462b79727ab1666354bc060181cc5cf77c1'>Commit.</a> </li>
<li>Use align status to indicate actual changes in status. Fix order of solving and delays to announce the solution. <a href='http://commits.kde.org/kstars/0585d044da8537383675f41294511f88e34ccc06'>Commit.</a> </li>
<li>Set new splash screen in regular KStars. <a href='http://commits.kde.org/kstars/06e7889a0ff69afa18dafb416f747dcb9d43e560'>Commit.</a> </li>
<li>Fix regression where INDI version check is ignored. <a href='http://commits.kde.org/kstars/bbd70f646ece7f512eb4964b8884aa399e002ed2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128523'>#128523</a></li>
<li>Probable fix of bug with disappearing and re-appearing labels in regular KStars. Need to test on other devices. <a href='http://commits.kde.org/kstars/d0ff31b443de6b7de21e2e75cd370eefa89b8985'>Commit.</a> </li>
<li>KStars Lite. Added "Find Object" icon to bottom drawer. <a href='http://commits.kde.org/kstars/7d0b855e0cc2c9229441ca2749525f8ab01222c1'>Commit.</a> </li>
<li>Fix minor typo. <a href='http://commits.kde.org/kstars/950a62696ffc456c51d279ea814969b20c64627e'>Commit.</a> </li>
<li>Drastically speed up binary file -> SQLite database generation. <a href='http://commits.kde.org/kstars/548a0acf1e749f7c9b5641c187a4e97f091fdae4'>Commit.</a> </li>
<li>Final commit before merging to master. Added comments to the rest of. <a href='http://commits.kde.org/kstars/b32c941735317a4380ea615ef173ec25646db7d9'>Commit.</a> </li>
<li>All modules should use status to indicate what they are doing. <a href='http://commits.kde.org/kstars/67128cab72e3a1f60c38198bb38d85d6ad4268bc'>Commit.</a> </li>
<li>Add more logging to isSequenceFileComplete to diagnose any future issues. <a href='http://commits.kde.org/kstars/1deb1f256accac062e41d2c58966020038a943ce'>Commit.</a> </li>
<li>Disable and enable start button based on number of jobs. <a href='http://commits.kde.org/kstars/55f164cf1b8690c19508de77c969aa235cd5f172'>Commit.</a> </li>
<li>Added comments to all SkyItem derrived classes. Started to add comments. <a href='http://commits.kde.org/kstars/353d5f865d35e3b36319bb9459662ae99eee73e4'>Commit.</a> </li>
<li>Disable all queue edit buttons while sequence is in progress. <a href='http://commits.kde.org/kstars/31a7b6d5c03c04faacdf687bb436d3254d07c4d1'>Commit.</a> </li>
<li>Start sequence as well if state is capture complete. <a href='http://commits.kde.org/kstars/d210c125ceaf7e97f343eec7a58cb1f7824b8f44'>Commit.</a> </li>
<li>Always set filterReady to true if the frame type is either bias or dark. <a href='http://commits.kde.org/kstars/e52c841c6c7631a99ccdecbb26c5d7e6e2f9d506'>Commit.</a> </li>
<li>Fix typo. <a href='http://commits.kde.org/kstars/020015c4be6d85a06b362e68cdfdbd4a914eb8cd'>Commit.</a> </li>
<li>Set dome device in capture module. <a href='http://commits.kde.org/kstars/d40a345fd27d6047a75031027ead8092970552d6'>Commit.</a> </li>
<li>On guide abort, abort the capture completely. <a href='http://commits.kde.org/kstars/630156c49947ad3331791dbe352a52b649aaa4a4'>Commit.</a> </li>
<li>Fix typo. <a href='http://commits.kde.org/kstars/4d33442348a37e3ceb667e20101155ca6913d384'>Commit.</a> </li>
<li>Debug tracking box. <a href='http://commits.kde.org/kstars/2c94014423e1b4de66b21ffd40dfc83bd4682820'>Commit.</a> </li>
<li>Fix signal/slot connection. Disabled selecting autostar after calibration as guide would do it anyway. <a href='http://commits.kde.org/kstars/ba36ebdd43b36cd9da0d76afe54a99a4496cd44a'>Commit.</a> </li>
<li>Using lambda to signal alignment startup after configured delay. <a href='http://commits.kde.org/kstars/c547d10afc7021343a0b43c6fdea47160a3c96e2'>Commit.</a> </li>
<li>Do not process a NULL BLOB. <a href='http://commits.kde.org/kstars/7ae00e2ebc1637bfd75e496c7b5ba0007d710a7b'>Commit.</a> </li>
<li>Fix setWCS enable/disable notification. <a href='http://commits.kde.org/kstars/3642c4b76efb7620da8b86419092a50c5f1c589f'>Commit.</a> </li>
<li>If we cannot load the file, do not signal we have a BLOB. <a href='http://commits.kde.org/kstars/898be9d3eead2b90868fb12f55a10b8067423a0c'>Commit.</a> </li>
<li>Get rid of obsolete signals. Move selectAutoStar to guide module instead of calibration. <a href='http://commits.kde.org/kstars/91b5bfea488668c2f8bbe6f3c50d8101a67ef37f'>Commit.</a> </li>
<li>Add the day offset if we are past dawn instead of hardcoded 12. <a href='http://commits.kde.org/kstars/83a192a736bbb46c4806623ecb6c7b5b39a764c1'>Commit.</a> </li>
<li>If focus fails at post-alignment stage, we need to set state back to post-alignment so that it proceeds correctly. <a href='http://commits.kde.org/kstars/40f1ec370d73c6d3e5dbeaa4420cb261d5553fed'>Commit.</a> </li>
<li>Fix comments. <a href='http://commits.kde.org/kstars/bda8ab1bd40f791265fedf960cbb3bb6055fa03e'>Commit.</a> </li>
<li>Disable call to updateCoords since it creates problem when ra0/dec0 are not valid for some objects. <a href='http://commits.kde.org/kstars/ab4b451cdf203c82c4b05ff15d3fd5d488bf1728'>Commit.</a> </li>
<li>Major overhaul of guiding module to make it work better with the FITSView tracking box that is used by all modules now. Use Dark Library to capture darks whenever necessary. Tweaked GUI a bit. Subframing on different binning and box size variations work a lot more reliably now. <a href='http://commits.kde.org/kstars/ee5669cbf03ac496a0441546d482e6ae95526f29'>Commit.</a> </li>
<li>Improved scheduler GUI. Add ability to pause the scheduler. Fix bug with jobs being re-shuffled after scoring is complete. Fix single click vs double click issue when it comes to editing a job and resetting its status. Add KDE event in case scheduler is aborted due to whatever reason. <a href='http://commits.kde.org/kstars/18e7dcecf60ce76adaf6b107c5cc4bc00ee28448'>Commit.</a> </li>
<li>Fix slot name. <a href='http://commits.kde.org/kstars/dde684742e5c4f45eb24faaf6ed860768a2b6278'>Commit.</a> </li>
<li>Fix astronomy.net online solving issues. Use asynchronous transfer since sync. causes a lot of event loop issues. <a href='http://commits.kde.org/kstars/b75dd9e00f34a70727c411bae3bea534d1c5839e'>Commit.</a> </li>
<li>Use options verbose to check for verbosity settings. <a href='http://commits.kde.org/kstars/ab78d9717b4f0ae7d03f2b3c933c2d9c25f4b6d7'>Commit.</a> </li>
<li>Add dark library options. Add options taken from Ekos align module. <a href='http://commits.kde.org/kstars/542f789c13e1210b3620fdf1d84cd5c8fe17951d'>Commit.</a> </li>
<li>Add capture paused state. <a href='http://commits.kde.org/kstars/e29969e8b2fa7c714a30ffb9550d8e0bcfe10ba8'>Commit.</a> </li>
<li>Distinguish between shutterful and shutterless CCDs. Ask the user if the CCD has a shutter or not as to avoid asking the user to cover the telescope if it already has a shutter. <a href='http://commits.kde.org/kstars/1e647c49ede654d290189251c262adbda8323853'>Commit.</a> </li>
<li>Focus module no longer have to actively revert to whatever frame settings it initially started with since this created more unnecessary complexities. Each module is separately responsible for their own frame settings. Subframing works correctly with dark frames. <a href='http://commits.kde.org/kstars/a05de10c430555794fb32138b6d7bd471b09c7dc'>Commit.</a> </li>
<li>Added ability to pause sequences and then resume them later. The module now accepts an optional target name which it will append to the root directory of the sequence file. This way the scheduler or other scripts can still use the same sequence files but with different targets. <a href='http://commits.kde.org/kstars/3954632f283e9cd5ebce3dc73cc1ca5c06a9039f'>Commit.</a> </li>
<li>Reworked GUI, reduced clutter and darks are now taken from the Dark Library. <a href='http://commits.kde.org/kstars/32fe363e0778a46112bb3da92b4c1e747acd915a'>Commit.</a> </li>
<li>Add FITS Align support to the CCD interface. <a href='http://commits.kde.org/kstars/81d59496252c1ffdc868cb6c227a2a37d8724407'>Commit.</a> </li>
<li>Add align tab so it can be inspected separately from preview or batch images. <a href='http://commits.kde.org/kstars/2b77d52d20bd9d9760f298b46599cd68a55c52fe'>Commit.</a> </li>
<li>Add Dark Library options and moved some seldom used options away from Ekos to reduce clutter to the settings screen instead. <a href='http://commits.kde.org/kstars/61283b4455c40648878616c6d3cd4aab98993665'>Commit.</a> </li>
<li>Add Ekos abort event. <a href='http://commits.kde.org/kstars/24f50076a223ab7cdfc2f8d41e2423bccb9a58eb'>Commit.</a> </li>
<li>More progress on dark frame library. Now it works across capture, guide, and focus modules. <a href='http://commits.kde.org/kstars/94999566b67737e9c625a9cde709c0da6ce5bddb'>Commit.</a> </li>
<li>More progress on dark frame library. <a href='http://commits.kde.org/kstars/4e290affbce9378a055180cd9b062ab63a2361dd'>Commit.</a> </li>
<li>+ Dark Library working in capture module. <a href='http://commits.kde.org/kstars/38e52bb4d5e57c7d438537931d6052cceaab2029'>Commit.</a> </li>
<li>Use FOVManager to handle how FOVs are stored. This fixes a number of memory related bugs. Need to migrate this to SQLite instead of file. <a href='http://commits.kde.org/kstars/842b5341cc5a1877f696e72559f12a89b539f006'>Commit.</a> </li>
<li>Code to put USNO NOMAD binary data file contents into a SQLite database. <a href='http://commits.kde.org/kstars/b524e15775b79004390905f79cd268724d395264'>Commit.</a> </li>
<li>Complete the sentence. <a href='http://commits.kde.org/kstars/3c994128444a3e1543ef70cd0778d01bb8ef9f43'>Commit.</a> </li>
<li>Initial work for dark library. <a href='http://commits.kde.org/kstars/7f7d2d208a038ec0bef9828bbe485dd557bd43d0'>Commit.</a> </li>
<li>Fixing some issues with memory as reported by Valgrind, need to check more. Please double check. <a href='http://commits.kde.org/kstars/0acf553c250d3498be85bed26746369aacaf684b'>Commit.</a> </li>
<li>Fix few focus box selection issues especially with different bin settings. <a href='http://commits.kde.org/kstars/eeb3f9e1806f4dbef7a5e7e2d71f3b80c61226dc'>Commit.</a> </li>
<li>Added tutorial mode. Fixed bug when context window for object was appearing while zooming. <a href='http://commits.kde.org/kstars/66106b0027b59dcfae90a0e4111a539f41166639'>Commit.</a> </li>
<li>Reject invalid frame requests. <a href='http://commits.kde.org/kstars/d53f6ffbc527479c3b9971a64f83a5f489e455d3'>Commit.</a> </li>
<li>Save the frame settings for the current session for all the CCDs. Settings are saved when an image is captured. Needs testing. <a href='http://commits.kde.org/kstars/4b23f1ed87c5bee786e1324eb5036e272b0fc933'>Commit.</a> </li>
<li>No need to be redundant here. <a href='http://commits.kde.org/kstars/3518013b052ea57ffd5a96b5851aa8cd3f141cf9'>Commit.</a> </li>
<li>Introduce 1500ms delay between slew motion completion and start of capturing for next astrometry image to avoid residual motion from the mount. <a href='http://commits.kde.org/kstars/5bae8f50f712554a89f3c5d09c4a4506a0b4f214'>Commit.</a> </li>
<li>1. Making INDI Control Panel independent as an option. <a href='http://commits.kde.org/kstars/46cfa929fa84dec93e9d0d6f207a7eb7a202bc4b'>Commit.</a> </li>
<li>Deps-- to Qt 5.1: use a lambda wrapper to replace a Qt5.5 method. <a href='http://commits.kde.org/kstars/a79f4667b59a25a3f35f2b3da3e210d7de545f47'>Commit.</a> </li>
<li>Double check ccdNum value. <a href='http://commits.kde.org/kstars/042cf321fb0834c9c66ca6fae3dc94212a490260'>Commit.</a> </li>
<li>Add option to make Ekos and FITSViewer windows as separate Windows from KStars. <a href='http://commits.kde.org/kstars/187e93a259c5cc984c0ffc619b631290b5b9f5ae'>Commit.</a> </li>
<li>Use QTimer instead of single shot stuff. <a href='http://commits.kde.org/kstars/701a50583f38d09d566be576807855fd714a4a99'>Commit.</a> </li>
<li>Fix comets and asteroids not being included despite qualifying for magnitude limit. Always use curly brackets!. <a href='http://commits.kde.org/kstars/b214f5b90411b30bfd39d0b6ab11a8080334ac73'>Commit.</a> </li>
<li>Add horizontal spacer. <a href='http://commits.kde.org/kstars/c9ef30e497c7328f5048d77c67d4483d688945dd'>Commit.</a> </li>
<li>Refactor variable and make the GUI a grid layout. <a href='http://commits.kde.org/kstars/023d91bbad80755fbb92537c6da74a34f71e1e0f'>Commit.</a> </li>
<li>Add temporary post capture script. <a href='http://commits.kde.org/kstars/22e7fe356e902bc69b1e3b3e988677bb04be6ca1'>Commit.</a> </li>
<li>Make target bold. <a href='http://commits.kde.org/kstars/13c10740057b9a8feddacf51d8e8f16b09263fef'>Commit.</a> </li>
<li>Add scope target name, if available either from direct sky map interaction or via the scheduler. <a href='http://commits.kde.org/kstars/78d8e70a6f3a440647fe74c85853da67d2e7fe73'>Commit.</a> </li>
<li>Increase timeout for alignment after meridian flip to about 10 minutes maximum. Only send status updated if in batch mode, not preview. <a href='http://commits.kde.org/kstars/7ff6d45093e495d5ca2c6855975a2fca6c8ecdf1'>Commit.</a> </li>
<li>Only perform autofocus on filter change IF and only IF the frame is LIGHT. <a href='http://commits.kde.org/kstars/0fd113e228d00b6fcdce8d297dcc93510eb9ef0a'>Commit.</a> </li>
<li>Remove incorrect name "McLeish's Object" for an NGC galaxy. <a href='http://commits.kde.org/kstars/8250f7d575dd15f4cbad5bf41a4b0aed44d328cc'>Commit.</a> </li>
<li>Change DSS / ref. image file-naming conventions! (Breaks some files). <a href='http://commits.kde.org/kstars/72b99242b391af788242e4a39b6871989afdbe3a'>Commit.</a> </li>
<li>Rename CurrentImage -> m_currentImageFileName, uniformize image paths. <a href='http://commits.kde.org/kstars/249df869349218c89d8a92892ce4c6d38197fefc'>Commit.</a> </li>
<li>Remove DSSUrl and SDSSUrl; use KSDssDownloader::getDSSURL() instead. <a href='http://commits.kde.org/kstars/98e1b54d748440b139c47725de0c77b1bafb0588'>Commit.</a> </li>
<li>Rename ObservingList::FileName to ObservingList::m_listFileName. <a href='http://commits.kde.org/kstars/ac6e68f39b1b0eaf31c31f05f6c78a8793b0b0ca'>Commit.</a> </li>
<li>Make the Image Preview label when there is no object selected meaningful. <a href='http://commits.kde.org/kstars/92293ef5eba5a1cca75b8f1b790b829ca179b836'>Commit.</a> </li>
<li>Rename ObservingListUI::TableView to ObservingListUI::WishListView. <a href='http://commits.kde.org/kstars/b4928bccf5b65dfd6ad6f9bc3a389e72e35a0171'>Commit.</a> </li>
<li>Simplify the greek letter code using a C++1x lambda. <a href='http://commits.kde.org/kstars/a4c7ca342a3d48005222bfb47c00b2c945cdc7c6'>Commit.</a> </li>
<li>Add a solid (non-bitmap) colored star mode. <a href='http://commits.kde.org/kstars/3f74b17d5f79d761aecb4c999a8047da7d86ffd7'>Commit.</a> </li>
<li>Fix bug: White stars must result in white stars. <a href='http://commits.kde.org/kstars/2ea4c3c71dcdc88f46367adbd727216be7e73041'>Commit.</a> </li>
<li>[EXPERIMENTAL] In the projector, don't reduce an angle twice. <a href='http://commits.kde.org/kstars/b8bc83de4b36164dc15f142dca2d14f97288c9dd'>Commit.</a> </li>
<li>Take the antialiasing on/off more seriously!. <a href='http://commits.kde.org/kstars/41fc26a668080126ec0ce9349ff22195c26dc5b4'>Commit.</a> </li>
<li>Use standard atlas symbol for galaxy clusters, which is a dashed circle. <a href='http://commits.kde.org/kstars/7bad74209ca2d398b854bbf471313afb4b41f7a0'>Commit.</a> </li>
<li>Simplify code for SkyQPainter::drawDeepSkySymbol() using C++11 lambdas. <a href='http://commits.kde.org/kstars/c52877c024eaf4e4dc681948a666689eb3b77079'>Commit.</a> </li>
<li>Improvements to the name resolver type interpreter. <a href='http://commits.kde.org/kstars/f4d2b6a176ff6228af87a785a07a8a232c1c989c'>Commit.</a> </li>
<li>Be more leniant when guessing RA/Dec from text. <a href='http://commits.kde.org/kstars/7d1c80961416ec227f2a0b24aa79e258241cbcdb'>Commit.</a> </li>
<li>Simplify code for drawing open cluster symbol using C++11 lambda. <a href='http://commits.kde.org/kstars/e6002ffa647ceeff34a35c30952f182608a90159'>Commit.</a> </li>
<li>Fix include order. <a href='http://commits.kde.org/kstars/a70b293dc8a79a142463344e1f4ed1816de1f274'>Commit.</a> </li>
<li>Fix Mosaic URL issue and add tool tips. <a href='http://commits.kde.org/kstars/02a4a440e8cbff859f1e3224968af4a167999744'>Commit.</a> </li>
<li>Use proper wizard icon. <a href='http://commits.kde.org/kstars/4c94638ce73765fd7cb7afaebf3321b5d7c50a10'>Commit.</a> </li>
<li>Fix signal slot names. <a href='http://commits.kde.org/kstars/16df4d5c089ebe7feab9bdc78ba77c0abdfd79d9'>Commit.</a> </li>
<li>Fix capture progress report on initial startup. <a href='http://commits.kde.org/kstars/f4d8c79642557897e4cc4e4d97699aa6399dc8a8'>Commit.</a> </li>
<li>Started to work on adopting DSO resolver. Fixed some compilation errors of regular KStars. Changed the way tap and pinch recognizers work. <a href='http://commits.kde.org/kstars/ba79cf5c998341b2743c037ece64ead06594eb48'>Commit.</a> </li>
<li>Added "Set Geolocation" page. User can set location either manualy or by. <a href='http://commits.kde.org/kstars/e498f19a762b431787dd2f5a0d0c9f4a502bcec9'>Commit.</a> </li>
<li>Fix misspelling and API doc fixes. <a href='http://commits.kde.org/kstars/120139a2f418628aac9b4a2635d5f9d4cd9bfc90'>Commit.</a> </li>
<li>Fix warning and remove OpenGL stuff. OpenGL is used in KStars Lite only. <a href='http://commits.kde.org/kstars/82c89591f42091033bbaf4a667145b81737bb53f'>Commit.</a> </li>
<li>Removing unused code and warning. <a href='http://commits.kde.org/kstars/6881b86a17dca197bde01de5aea292e83febda43'>Commit.</a> </li>
<li>Cleanup. Also make a star edge passes if only 5 point are on the potential circle. <a href='http://commits.kde.org/kstars/e20612bddb1239baf4bfb1d7dfae9b3f47b4880f'>Commit.</a> </li>
<li>Add threshold and average frame options to improve focusing verstality. <a href='http://commits.kde.org/kstars/e9fc373200beabc5c9e9c15001b41174c66e91ec'>Commit.</a> </li>
<li>Added support for FOV symbols. Solved the bug with incorrect display of EllipseNode. <a href='http://commits.kde.org/kstars/a520f0eca43caa9ffbe06271d2f063cd8bcc6486'>Commit.</a> </li>
<li>Changed the way images (icons, splash screen, arrow) are loaded for screens with different DPI. Finished Details page. Modified Top Menu and Projection and Color Scheme Popups so that it is clear what option is currently switched on/off. <a href='http://commits.kde.org/kstars/6c9ddf1bf51053b3ed38e10b218683bf84212ea2'>Commit.</a> </li>
<li>Change SOURCE_DUSTCAP to SOURCE_FLATCAP and SOURCE_DARKCAP in Ekos capture. <a href='http://commits.kde.org/kstars/a29536ce3a2e40ef08794915c203bf0dfc4c9d07'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128718'>#128718</a></li>
<li>Fix mismatched signal/slots. <a href='http://commits.kde.org/kstars/a57eadaf5454c489876e9893ddc51824417bfb0c'>Commit.</a> </li>
<li>Update screenshots for Catalogs tab. <a href='http://commits.kde.org/kstars/b30008485b2bd1dd10495166c4e483fec7564602'>Commit.</a> </li>
<li>Updated documentation for Finding Objects in order to include Akarsh internet searching feature. <a href='http://commits.kde.org/kstars/c5547a11454be8f4ff1a74d9cd329ef7eb997574'>Commit.</a> </li>
<li>Misc. improvements to add deep sky object GUI. <a href='http://commits.kde.org/kstars/a1b96416e4324f943760f0086919cae8adddf81b'>Commit.</a> </li>
<li>Improve guessing of DSO parameters from text. <a href='http://commits.kde.org/kstars/7349ff2dfa78a7dbe20ad745a9cb8af48343ef23'>Commit.</a> </li>
<li>Make sure RA input is HMS!. <a href='http://commits.kde.org/kstars/7e0471becb8543cca22d4613e35254e8582bb42d'>Commit.</a> </li>
<li>Use a boolean to know whether ekos is running or not besides communication status flag. <a href='http://commits.kde.org/kstars/67859e5a1632bab93bd03c252cf23a736d0f551c'>Commit.</a> </li>
<li>Change ordering of menu items. <a href='http://commits.kde.org/kstars/ef6adfc39283bb21fc81469b6b14dc509fffd8ff'>Commit.</a> </li>
<li>Introducing a new way of adding deep-sky objects into KStars. <a href='http://commits.kde.org/kstars/0a7cb3af6a92f02aac27438273ea170cfba995a5'>Commit.</a> </li>
<li>Include a SkyObject::NUMBER_OF_KNOWN_TYPES enum entry + Cosmetics. <a href='http://commits.kde.org/kstars/247a6e1dee9ddb48512c21e757f1aa69e508efb9'>Commit.</a> </li>
<li>Rename miscObjectComponent to internetResolvedComponent. <a href='http://commits.kde.org/kstars/67bf490da3e13d8636193c05c527949a90ba2725'>Commit.</a> </li>
<li>Fix crash in SyncedCatalogComponent. I'm an idiot. <a href='http://commits.kde.org/kstars/dbcc3756aba9901b6bbf0b29ef0bd7a5a168fcd2'>Commit.</a> </li>
<li>Update a comment... <a href='http://commits.kde.org/kstars/52d368d09c896b6a1cab32e46b2fb06935b9fdd2'>Commit.</a> </li>
<li>No need for separator here it is already included in the writable location return string. <a href='http://commits.kde.org/kstars/d8442c5a55f9771c209a1e1f05c078e7ae1c71a2'>Commit.</a> </li>
<li>Use hardcoded / to avoid issues on Windows. <a href='http://commits.kde.org/kstars/278a1731f847bd842b356a5d18afd0e367e78fc4'>Commit.</a> </li>
<li>Use GenericDataLocation. <a href='http://commits.kde.org/kstars/31790263f2f1adf4c80ec214c4b8660eda56a0e4'>Commit.</a> </li>
<li>Send target chip info for newImage and do not send guide image to the summary screen. Thanks to Robert for noting this. <a href='http://commits.kde.org/kstars/9658f6bd53870ab8c7c2a0894b35741a099b81aa'>Commit.</a> </li>
<li>Cosmetic. <a href='http://commits.kde.org/kstars/9095118860409ea4ebab6b2a382b3ad089aa4b72'>Commit.</a> </li>
<li>Add an assert. <a href='http://commits.kde.org/kstars/b844859fc5d79ef2ca60df10cab0c0ffdf31a171'>Commit.</a> </li>
<li>Only add "kstars" directory if using QStandardPaths::GenericDataLocation. <a href='http://commits.kde.org/kstars/ff5332eb8263a57867b16931982dae53f077a940'>Commit.</a> </li>
<li>Remove fail criteria and reduce the threshold a bit. This perhaps needs to be configurable. <a href='http://commits.kde.org/kstars/cb070e1a77b6a0aa16542cfa3d124befad222c9e'>Commit.</a> </li>
<li>Refactor function name to distinguish it from newStatus. Always connect box selection from focus image in any mode. <a href='http://commits.kde.org/kstars/7eb464868d3603d011675263ca84c446b2ae2d74'>Commit.</a> </li>
<li>Nope do not remove the file at all, let image viewer handle it. <a href='http://commits.kde.org/kstars/5f48904575910e69cd156cc14479b8f816bf5851'>Commit.</a> </li>
<li>Remove temporary files. If origin is local files, ask for confirmation first. <a href='http://commits.kde.org/kstars/0dc41aa88888ca8d28edcd50ed287a627c47fb80'>Commit.</a> </li>
<li>Only remove image file if it was temporary. <a href='http://commits.kde.org/kstars/f2d0c31e15b14c45cae76ad64eb7e5583d77abb6'>Commit.</a> </li>
<li>Fix slot. <a href='http://commits.kde.org/kstars/650cdea77a3e0ea6bfdc9120cb11d13ace46dc9b'>Commit.</a> </li>
<li>Construct URL from local file. <a href='http://commits.kde.org/kstars/f689472fd73f70a32bb15571c1961de55b388002'>Commit.</a> </li>
<li>Parse magnitudes correctly from Sesame XML result. <a href='http://commits.kde.org/kstars/d51147bce4544b5ece28407d17fbbe4bb2498a9e'>Commit.</a> </li>
<li>Initialize magnitudes in DeepSkyObject c'tors. <a href='http://commits.kde.org/kstars/234259d760a446ca7f1edf07672b7f8c24e6472f'>Commit.</a> </li>
<li>Add asserts to deliniate crashes from not setting the parent customCat. <a href='http://commits.kde.org/kstars/3f0e2d2f094bc33c1e989b83484dc3f7da7c3ffb'>Commit.</a> </li>
<li>Set the pointer to the CatalogComponent from DeepSkyObject c'tor. <a href='http://commits.kde.org/kstars/a759b43ac20d7375fda62286960234744559e68b'>Commit.</a> </li>
<li>HFR calculation more stable now and works OK under simulation, need to test it today under real conditions. <a href='http://commits.kde.org/kstars/9842e1be25d22e6f654b27fa49f66657f0bccd6f'>Commit.</a> </li>
<li>On first reverse direction, recapture to hopefully avoid erronous measurement. <a href='http://commits.kde.org/kstars/fe67b8e5c265aa2a4eec4a8a85fee58d219fed35'>Commit.</a> </li>
<li>Enable resolution of object names by default. <a href='http://commits.kde.org/kstars/c6a8a3187d4d35589cccf779c98dbf89b43e2155'>Commit.</a> </li>
<li>Add the name "The Eyes" to the pair of galaxies in the Markarian Chain. <a href='http://commits.kde.org/kstars/8b86c6f01fae0f81c920d242c3bbb9b4d148a2e5'>Commit.</a> </li>
<li>Add a new button to the Find Dialog, that resolves using internet. <a href='http://commits.kde.org/kstars/9541af7cfd5fae8a753804ef8341c8c567763bdb'>Commit.</a> </li>
<li>Move the processing of acceptance of FindDialog to a separate method. <a href='http://commits.kde.org/kstars/4493f67f381559570c706db1c5b22a838d680381'>Commit.</a> </li>
<li>Improve type identification. <a href='http://commits.kde.org/kstars/91f4c22e0fe3fd4fc093e218f7ff65502470c15a'>Commit.</a> </li>
<li>Cosmetic. <a href='http://commits.kde.org/kstars/18676731e788d1c87837da2739d21a5fe837c1b9'>Commit.</a> </li>
<li>Parse object name returned by SIMBAD and put it in CatalogEntryData. <a href='http://commits.kde.org/kstars/58c15dea8824115c24e0432c9ecd9ce381346300'>Commit.</a> </li>
<li>Prevent showing _Internet_Resolved # names for internet-resolved objects. <a href='http://commits.kde.org/kstars/4148ddf859057d9eefb83c2b837ee0313fdc41f9'>Commit.</a> </li>
<li>Enable newly created SyncedCatalogComponent catalogs by default. <a href='http://commits.kde.org/kstars/b43e6ecead87fac98e9fb5a494bafd6668c8bde9'>Commit.</a> </li>
<li>Rename "Misc" to  "_Internet_Resolved" and allow showing/hiding it. <a href='http://commits.kde.org/kstars/f9fdbbbcff8e1a3908cb9f9eee25afbfbbf3b7e9'>Commit.</a> </li>
<li>Enable/disable automatic rsolution of unknown objects using the internet. <a href='http://commits.kde.org/kstars/c6f15b3a1f416c20f377c774ec8f7a7638c63435'>Commit.</a> </li>
<li>Fix crash when a resolved object could not be entered in the database. <a href='http://commits.kde.org/kstars/4eef9eb85388c0155377911a231f59e93da3b48a'>Commit.</a> </li>
<li>Initialize CatalogEntryData::catalog_name to empty. <a href='http://commits.kde.org/kstars/433369d3a6d8078a04adb97b43e6f3d1160752e0'>Commit.</a> </li>
<li>Sort custom catalog names in ascending order. Makes them easier to find. <a href='http://commits.kde.org/kstars/b25452cf0871ec16a149ae216f2c51f9a93f9257'>Commit.</a> </li>
<li>Draw objects of unknown type as a circle with a "?" symbol. <a href='http://commits.kde.org/kstars/e1e6f0016c98aa92941196de740942c749a549d5'>Commit.</a> </li>
<li>Fix failing connect() in KSDssDownloader (used in EyepieceView). <a href='http://commits.kde.org/kstars/9ad052d2af472effea40d761d32acf5515565721'>Commit.</a> </li>
<li>Remove unused / unimplemented methods from CatalogComponent. <a href='http://commits.kde.org/kstars/a8b3c83d1f129d042926262c214c07891de8b580'>Commit.</a> </li>
<li>Use inline for inline methods in CatalogComponent. <a href='http://commits.kde.org/kstars/4dda309097745454318e77d8268d7d31559a290d'>Commit.</a> </li>
<li>Draw objects of unknown type as a circle with a "?" symbol. <a href='http://commits.kde.org/kstars/d7c9f8dae0772d118b4980db4536315262bd3b14'>Commit.</a> </li>
<li>Fix failing connect() in KSDssDownloader (used in EyepieceView). <a href='http://commits.kde.org/kstars/99ee92c70ed4d6a35976dcde249be5879f5d3ccf'>Commit.</a> </li>
<li>Use the miscObjectComponent() SyncedCatalogComponent in the Find Dialog. <a href='http://commits.kde.org/kstars/51eea36e8907fba23eeaffd80088ce4628366af6'>Commit.</a> </li>
<li>Cosmetics. <a href='http://commits.kde.org/kstars/0dc3bfa9fba29ab23ccb7bc0b405d15c0bc75bff'>Commit.</a> </li>
<li>Add a SyncedCatalogComponent called miscObjectComponent to SkyComposite. <a href='http://commits.kde.org/kstars/eff0e53162bbcf17b56976cd7c2356a4615727ca'>Commit.</a> </li>
<li>A SkyComponent which loads a custom catalog, but can also add objects. <a href='http://commits.kde.org/kstars/ad55f21c5a4f7b64dcc77f5fdc677e71536607b2'>Commit.</a> </li>
<li>Prepare CatalogComponent to be sub-classed. <a href='http://commits.kde.org/kstars/3aedf672b17e009c9c82ba0f51f884e0a1e7c45f'>Commit.</a> </li>
<li>Make CatalogDB::FindCatalog and CatalogDB::AddCatalog public. <a href='http://commits.kde.org/kstars/26d87871499f1e230803ca16aaec84423b972aeb'>Commit.</a> </li>
<li>Allow for fake catalogs where prefix+number designation makes no sense. <a href='http://commits.kde.org/kstars/54ed399e4dbe3975951157774c380b68a43768fa'>Commit.</a> </li>
<li>Use _AddEntry() when adding entries in a batch. <a href='http://commits.kde.org/kstars/9cd7ced15ab6aecdaab41dc3545e3129661a2c3d'>Commit.</a> </li>
<li>Simplify CatalogDB::AddEntry -- it should now be given a valid catalog. <a href='http://commits.kde.org/kstars/d187919e1ede4df017a916406865e3a6a6f50d58'>Commit.</a> </li>
<li>Changes to CatalogDB::AddEntry(). <a href='http://commits.kde.org/kstars/ac46a17dcf2173e152938b1885d9474cbf7340e8'>Commit.</a> </li>
<li>Reverting to prior algorithm until issues with new one are resolved. <a href='http://commits.kde.org/kstars/06cfa9e20062fe825199b8a839744daed4f49328'>Commit.</a> </li>
<li>Fix warning. <a href='http://commits.kde.org/kstars/7f464a69b1a435b8eb14eb7565532361a0562804'>Commit.</a> </li>
<li>Remove unused / unimplemented methods from CatalogComponent. <a href='http://commits.kde.org/kstars/4dbb0be1ed57ff3e542eacc93855feb225d599f9'>Commit.</a> </li>
<li>Use inline for inline methods in CatalogComponent. <a href='http://commits.kde.org/kstars/613284dd21a623d2cd2fb58677711d81bf2fbcaa'>Commit.</a> </li>
<li>Add missing files and missing build list line to DSO Resolver branch. <a href='http://commits.kde.org/kstars/628cdbea1f34adb63c5bcce33faf261b459688cc'>Commit.</a> </li>
<li>[NOT TESTED] This commit creates a special Misc catalog in the DB. <a href='http://commits.kde.org/kstars/d2f79ff590fab7cfa6653a8f4d3d735646c4191d'>Commit.</a> </li>
<li>New struct for Deep Sky Object data [might not be necessary]. <a href='http://commits.kde.org/kstars/93803460705c392943794f11654d9a08c9b65090'>Commit.</a> </li>
<li>[NOT TESTED] UI to add / edit DSO entries into/from the database. <a href='http://commits.kde.org/kstars/84b3d4fe12ec9bec6b54fa8b8243cb66beaeb8cf'>Commit.</a> </li>
<li>Experimental testing: using Name Resolver in Find Dialog. <a href='http://commits.kde.org/kstars/d36b326d6b87b3c4df1220f637aa4a0eeb68df07'>Commit.</a> </li>
<li>Add constructor to construct DeepSkyObject from CatalogEntryData. <a href='http://commits.kde.org/kstars/39fc1940e5cb0f274347d7025cfe4137dead64e6'>Commit.</a> </li>
<li>Use smooth scaling for DSS / SDSS images in the Image Viewer. <a href='http://commits.kde.org/kstars/0b725a0ec6fee51a37cce9d7e2e08aef76103605'>Commit.</a> </li>
<li>Use bounded star detection for HFR focusing. Keep track of subframed focus. <a href='http://commits.kde.org/kstars/a959e81105e346cc427604af45bb71deb874a455'>Commit.</a> </li>
<li>Add HFR calculation to weighted detection function. Need to test it under real conditions. Need to run median filter perhaps followed by high contrast filter to obtain better results. <a href='http://commits.kde.org/kstars/a7f0496c553ed37a670c8acfe24ff6f06ee1c07d'>Commit.</a> </li>
<li>Added support for projection systems and color schemes. Deleted Kirigami from the project. Moved UI to QtQuickControls 2.0 and Qt 5.7. Added support for simulation time and setting the date/time. Started to work on details dialog. <a href='http://commits.kde.org/kstars/a39c596a104c85e5168e7fefe4241fd13489b8ce'>Commit.</a> </li>
<li>Use setDestination to center scope crosshairs. <a href='http://commits.kde.org/kstars/27f34e7dd50dfd1df8ac7626dc7b6cf678616061'>Commit.</a> </li>
<li>Adding preliminary testing algorithm for 1 star bounded detection. <a href='http://commits.kde.org/kstars/c9e0cfca0dcc378e9be3eda567ec7801e245f16b'>Commit.</a> </li>
<li>Check if temp diff is within limit on startup. <a href='http://commits.kde.org/kstars/5befb46689c29d685b23c9e2813a641bc440f595'>Commit.</a> </li>
<li>Updated FAQ. <a href='http://commits.kde.org/kstars/ecf86445fe611b2258b15bbe6e87aa3c74c314ed'>Commit.</a> </li>
<li>Reset startup condition to file startup condition on abort/reset. <a href='http://commits.kde.org/kstars/5726adf01b09cbc8e65080a080ca0588305f83a4'>Commit.</a> </li>
<li>Fix focus signal with new status updates. Include number of captured and total images. <a href='http://commits.kde.org/kstars/d9c71c2dde2d5cc527e573d07f32783a7a494d63'>Commit.</a> </li>
<li>Fix remaining time as the delay is in ms. <a href='http://commits.kde.org/kstars/1dc7d87f68e4b93c6ad325ac20faa3127e99bb14'>Commit.</a> </li>
<li>Only emit prepareComplete() if we need to. <a href='http://commits.kde.org/kstars/88b83f61f88af2f011a5568340112e5f8429afcb'>Commit.</a> </li>
<li>Do not abort if guiding stopped while we are in the middle of a meridian flip. <a href='http://commits.kde.org/kstars/43b042440b382c73ffc0c634a990040e1f979727'>Commit.</a> </li>
<li>Recenter tracking box around max HFR star in autofocus mode. <a href='http://commits.kde.org/kstars/31a227f70c31a12ce9f70968b1ddcd82b52a07f4'>Commit.</a> </li>
<li>No need to explicitly apply autostretch. <a href='http://commits.kde.org/kstars/694ef7f46647ffab0f7ae06bf0e79e5a520d17ad'>Commit.</a> </li>
<li>Apply autostretch if it is set by the user to all displayed images. Data is left untouched. <a href='http://commits.kde.org/kstars/9b07e931c9ea2edbb5924e7065c6d04aa0d48c93'>Commit.</a> </li>
<li>No need to apply autostretch here, it is done in FITSView. <a href='http://commits.kde.org/kstars/047c5f00330163c5f8ab47741c62d302a8c067cf'>Commit.</a> </li>
<li>Fix bit -> byte. <a href='http://commits.kde.org/kstars/2bbff8827bc3463bc91bf2c7419da9d9d23ddb24'>Commit.</a> </li>
<li>Set dirty after writing the changes. <a href='http://commits.kde.org/kstars/9716aae1fced997545b93f4b600483e8c1b49e4c'>Commit.</a> </li>
<li>Include missing header. <a href='http://commits.kde.org/kstars/4d9484481c1d3fe20746bdc19f113b6b550c749f'>Commit.</a> </li>
<li>Accept URL as is without making assumptions. The Image Exporter expects a fully qualified URL to the desired file path. <a href='http://commits.kde.org/kstars/8bf6681fecc2df061c64141cd77b0ae9b594af1c'>Commit.</a> </li>
<li>Use toLocalFile(). <a href='http://commits.kde.org/kstars/6777b501d29f01861b94b230afa9ff5c23ddae0d'>Commit.</a> </li>
<li>Use fromUserInput to properly parse the file input. <a href='http://commits.kde.org/kstars/9acd8748217b2080e81af41895139218f3fb8b68'>Commit.</a> </li>
<li>+ Using toLocalFile() instead of path() to avoid cross-platform file location issues. <a href='http://commits.kde.org/kstars/d2ad5cc2deaa2c664966a1923f5e50d6412fa110'>Commit.</a> </li>
<li>Fix minor typo. <a href='http://commits.kde.org/kstars/fbd8f8892924ff6ce4a198a1a80f730fb698e215'>Commit.</a> </li>
<li>Fix minor glitches. <a href='http://commits.kde.org/kstars/18fbabff7a2a02e3562c604cb579db3d5e8908ba'>Commit.</a> </li>
<li>Update tutorial. <a href='http://commits.kde.org/kstars/2ef514ca98c311701ccbb3187b1ea6e77f78cf3d'>Commit.</a> </li>
<li>Update catalogues tutorial. <a href='http://commits.kde.org/kstars/0699e337da7b9bdf417b9935f174239fb047884e'>Commit.</a> </li>
<li>Fix xml error. <a href='http://commits.kde.org/kstars/70c39526c472013e0ab6f1a1a8c53102a1eaa981'>Commit.</a> </li>
<li>Improve tutorial. Needs additional work. <a href='http://commits.kde.org/kstars/9276c4cd3a86e0e8bb255c57b777736468017a0c'>Commit.</a> </li>
<li>Updated FAQ. <a href='http://commits.kde.org/kstars/0931d969d28ce5909de68a318a6624ceffcffc56'>Commit.</a> </li>
<li>New screenshot for pointing out KStars Horizon, Celestial Equator and Ecliptic curves on the Sky Map. <a href='http://commits.kde.org/kstars/9eb41515081a03a5cf33b1d5098a6f32a6febd33'>Commit.</a> </li>
<li>Documentation fixes. <a href='http://commits.kde.org/kstars/f2b23d2dbeeb7258021f55893ab7d99d021b1859'>Commit.</a> </li>
<li>Fix subframing under some conditions. <a href='http://commits.kde.org/kstars/655cf001121529ee3bc7dd0a4b5be296d47f94be'>Commit.</a> </li>
<li>Check if ccd and telescope have been established. <a href='http://commits.kde.org/kstars/0dc1825f432e4ec5f28c37711560b37326f4f9b2'>Commit.</a> </li>
<li>Added search to KStars Lite. In KStars Lite we don't need to call. <a href='http://commits.kde.org/kstars/eecc521f6e3a37144e2270d0e0be37faa70eaf0e'>Commit.</a> </li>
<li>Added support of "Light" properties to INDI. Eliminated a few bugs with. <a href='http://commits.kde.org/kstars/1ec33812527b7ccc0c5b3bb2026994c362ef389c'>Commit.</a> </li>
<li>Added folder with Android (ARM) versions of some libraries (INDI, CFITSIO, LibNova, LibRaw) and changed folder with kirigami. <a href='http://commits.kde.org/kstars/d03d3fcc178442f053c91913f1c812b069dc6168'>Commit.</a> </li>
<li>Added image previewer (FITS and CR2), animation for tap and fixed some. <a href='http://commits.kde.org/kstars/32b61546363013993a4eaaeef522f84e1067c774'>Commit.</a> </li>
<li>Updated documentation and added Eyepiece View. <a href='http://commits.kde.org/kstars/2db1484a5953d5961c7498bc5e465d529afb4468'>Commit.</a> </li>
<li>Update image links. <a href='http://commits.kde.org/kstars/90ef682003f9b49c4a9ebcf2834e830018296bc5'>Commit.</a> </li>
<li>Opssupernovae - minor code cleanup - removing useless slots. <a href='http://commits.kde.org/kstars/6179506a1cba6ca68ad4a1055a5b1fae25b669da'>Commit.</a> </li>
<li>Updated info links for Solar system objects. <a href='http://commits.kde.org/kstars/2dff34982a95c73b8da2825244e4549c7cc43ef1'>Commit.</a> </li>
<li>Added Wiki info links for IC objects. <a href='http://commits.kde.org/kstars/036337d678fca623fc8f36e63b1504ebd5a567fa'>Commit.</a> </li>
<li>All Wiki links for NGC objects. <a href='http://commits.kde.org/kstars/990379515beb77d8b74eb33115da9bb57df3a2d9'>Commit.</a> </li>
<li>Added Wiki info links for NGC objects. <a href='http://commits.kde.org/kstars/c7534c884bfdff7ff9e86efad3ea7a776e818b01'>Commit.</a> </li>
<li>Added Wiki info links for Messier objects. <a href='http://commits.kde.org/kstars/8de3d90bc666aeaad947c7411e7e34235cd35cef'>Commit.</a> </li>
<li>Added INDI Client for KStars Lite. Currently working - telescope control. <a href='http://commits.kde.org/kstars/7cd2ae86cc875247528cf4e1235147f0864e101e'>Commit.</a> </li>
<li>Added HST Images links for Messier objects. <a href='http://commits.kde.org/kstars/6ebcabd16ada3b019d7a9f9e93bf01dd0907ccea'>Commit.</a> </li>
<li>Added Spitzer Images links for Messier objects. <a href='http://commits.kde.org/kstars/24381884fef8cfa898d1a76df5011211751b750f'>Commit.</a> </li>
<li>Add Lynds catalog of Dark Nebulae example. <a href='http://commits.kde.org/kstars/12e16c9ac58344f753a9a047793124b6c3f6601a'>Commit.</a> </li>
<li>Handle bin regardless of which valid ccd and mount values we currently have. <a href='http://commits.kde.org/kstars/031907ff7d737d7a7cfcae4520900cad04ddc6f3'>Commit.</a> </li>
<li>Altvstime - fix warnings. <a href='http://commits.kde.org/kstars/fe83ee8524d355b46ca29e79679cf63834daead7'>Commit.</a> </li>
<li>Update to use FocusState. <a href='http://commits.kde.org/kstars/fb5dc06a671ba6fb0ab66069bb5f0d953fe0037e'>Commit.</a> </li>
<li>DO not crash if dark image is not properly captured, try again. <a href='http://commits.kde.org/kstars/a62fdae0b9c9caed63ca161524d049397849debe'>Commit.</a> </li>
<li>Use FocusState. <a href='http://commits.kde.org/kstars/1cebe1db144649119047daddcb15f93b396b6bed'>Commit.</a> </li>
<li>Trackbox instead of Center and Size. <a href='http://commits.kde.org/kstars/02dbf4ea7cf1d38ac573aedea5df69e5abd7bdae'>Commit.</a> </li>
<li>No need to set tracking box size twice. <a href='http://commits.kde.org/kstars/bfe3ef4090a45875798e9bff905718d3087a9174'>Commit.</a> </li>
<li>Add FocusState. <a href='http://commits.kde.org/kstars/635296e1239b7a55b8beaadd523f8a5157e8e41d'>Commit.</a> </li>
<li>Limit star search to the user supplied QRect. <a href='http://commits.kde.org/kstars/addba85c74984b894bbb707d00abedb10c42667a'>Commit.</a> </li>
<li>Use trackingbox RECT instead of center and size variables. <a href='http://commits.kde.org/kstars/046ede826d764a9a2b936932334de7944a8e9d5f'>Commit.</a> </li>
<li>Remember last opened URL. <a href='http://commits.kde.org/kstars/5898796c69a0067d078ad464a053739e2d2c8bd0'>Commit.</a> </li>
<li>Limit focus star search to the tracking box center and size. Use median filter on initial exposure to reduce noise before searching for candidate stars. <a href='http://commits.kde.org/kstars/b462fde893330f4f13628d7205c7a3f130a08a6e'>Commit.</a> </li>
<li>Add ability to limit star search to a specific boundary. <a href='http://commits.kde.org/kstars/442fc5589737400e405061350119157a68f8d5f1'>Commit.</a> </li>
<li>Reset startup condition on reset and fix string. <a href='http://commits.kde.org/kstars/a07d2b543b340fd8f43155ce71584602b54276e6'>Commit.</a> </li>
<li>DetailDialog - minor code cleanup. <a href='http://commits.kde.org/kstars/65edfe79d0b35c0881d8a29f82961eb947a989d6'>Commit.</a> </li>
<li>DetailDialog - minor code cleanup. <a href='http://commits.kde.org/kstars/fa79e0db31fe46d4cd494a9c39ee23df976ae79a'>Commit.</a> </li>
<li>DetailDialog - minor code cleanup. <a href='http://commits.kde.org/kstars/c77bd8fcae8905b577b6508813d3ddf6f0ba0414'>Commit.</a> </li>
<li>Use the right ECM variable for the appstream path. <a href='http://commits.kde.org/kstars/f4e60e33eda286944db904e05597cae506a65131'>Commit.</a> </li>
<li>Make it easier to translate. <a href='http://commits.kde.org/kstars/f8e93e527e6308c44e47356d75a1709e362f9a67'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kstars/4eccedaf9363468b504927bafe695bd3bbfa7acd'>Commit.</a> </li>
<li>Fix XML. <a href='http://commits.kde.org/kstars/8cce8ac2a5d24c1d57fe0718e70f6d3fa2fca5aa'>Commit.</a> </li>
<li>New tutorial for importing custom catalogs. <a href='http://commits.kde.org/kstars/4843e8f2282e11c571ff67aa0ef8e6082f8ba91b'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kstars/23e9f7671d92c0e3412377099c4f8f4c5ff950e9'>Commit.</a> </li>
<li>Save last opened URL in FITSViewer. <a href='http://commits.kde.org/kstars/c7446afb050b3ad517c2e94b49875904e380b3c6'>Commit.</a> </li>
<li>Make logs organized by date and time. <a href='http://commits.kde.org/kstars/718630cd9d1554ef52fbab471343d48a200ac092'>Commit.</a> </li>
<li>XML and factual fixes. <a href='http://commits.kde.org/kstars/16d0f262531f5c369f25b61adaaf94406dce6409'>Commit.</a> </li>
<li>Branch sync. <a href='http://commits.kde.org/kstars/947c7416121ca454b1f8619e50c2cb73d9067f6a'>Commit.</a> </li>
<li>Improve summary screen. <a href='http://commits.kde.org/kstars/2b20c994a3ae695aadc005103b7a667d4747f931'>Commit.</a> </li>
<li>Use tracking square instead of specific guide square. Return pixmap for tracking square. <a href='http://commits.kde.org/kstars/72d38401448b21ce051c68e96d5afa4189951dab'>Commit.</a> </li>
<li>Added mechanism for deleting unused nodes to reduce memory consumption. <a href='http://commits.kde.org/kstars/fc3f9cd7d614edb1d11a8dcf633f6053bd9e12cd'>Commit.</a> </li>
<li>Added support for Constellation Art, Satellites and Supernovae. Fixed. <a href='http://commits.kde.org/kstars/dcf4bfd9b35e6947a2c6d6502f53fb4782e518ae'>Commit.</a> </li>
<li>DSO images are now included in KStars Lite package for Android. Added. <a href='http://commits.kde.org/kstars/037bb68e09f7a4d38fbf646c8ab01c33f6499a56'>Commit.</a> </li>
<li>Added support for DSOs (Images and symbols). <a href='http://commits.kde.org/kstars/0f31a955d6d38ba5b6ff8bbe043049b898841ad3'>Commit.</a> </li>
<li>Added support for unnamed static stars. Started working on dynamic. <a href='http://commits.kde.org/kstars/e782b2201100e67bb0514fd4b719f4e363a6df22'>Commit.</a> </li>
<li>Added support for named stars. Added SkyOpacityNode (wrapper for. <a href='http://commits.kde.org/kstars/7fd5cf743a9a47255d635ebc48dd83a70cff86d9'>Commit.</a> </li>
<li>Added support for zoom-dependent labels. Added comments to all SkyItems. <a href='http://commits.kde.org/kstars/923bdceb41d845f1aa28042e279594830414c7fd'>Commit.</a> </li>
<li>Added constellation names (ConstellationNamesItem) to SkyMapLite. <a href='http://commits.kde.org/kstars/13562153080dbd1a5b9d0a54129a20d64cac0152'>Commit.</a> </li>
<li>Added labels for planets, asteroids, comets and stars. <a href='http://commits.kde.org/kstars/4c02dbca790e031774b44a0ce1f41981842f21b6'>Commit.</a> </li>
<li>Optimized the structure of SkyMapLite so that now there is only one node. <a href='http://commits.kde.org/kstars/1aa79365420ac3374183e1945a939e4391d4d8d9'>Commit.</a> </li>
<li>Use QStringList removeDuplicates to remove any duplicates for now until a re-work is done for DSO classes. <a href='http://commits.kde.org/kstars/36098121e5f07792b4d98d37c41e93e2f4a2c50b'>Commit.</a> </li>
<li>Remove checking for duplicate strings since it is extremely inefficient. <a href='http://commits.kde.org/kstars/8b21b252ec48c6ef1979bc0b6831414a6f3be7e8'>Commit.</a> </li>
<li>Fix FITSViewer support under Windows, and include all actions. <a href='http://commits.kde.org/kstars/16eab86d4065be6d71f33d84daed8cacdcce32ce'>Commit.</a> </li>
<li>Use QStringList removeDuplicates to remove any duplicates for now until a re-work is done for DSO classes. <a href='http://commits.kde.org/kstars/63ff1e825f658ad5b5353701cd244d95603130ee'>Commit.</a> </li>
<li>Remove checking for duplicate strings since it is extremely inefficient. <a href='http://commits.kde.org/kstars/3fb113b19138ec79fef270d987947eb5f0022d34'>Commit.</a> </li>
<li>Fix FITSViewer support under Windows, and include all actions. <a href='http://commits.kde.org/kstars/9f28b0f80843aa816cad89cae08debfd9baebf37'>Commit.</a> </li>
<li>Fixed incorrect displaying of lines. <a href='http://commits.kde.org/kstars/0a80604d28ac564c35b71efe20ef3d320a6609f6'>Commit.</a> </li>
<li>Added HorizonItem to SkyMapLite. To render the polygon I used. <a href='http://commits.kde.org/kstars/42bb14021e7537a345b4b7b59dd5e12ecefc55a0'>Commit.</a> </li>
<li>Added support of planet moons in SkyMapLite. SkyMapLite now can handle. <a href='http://commits.kde.org/kstars/762487c3f9bea82d17e9a7e66a1dd7317165fe2e'>Commit.</a> </li>
<li>Changed splash screen so that it looks better on Android (no black screen at the startup, better fits to Android style). <a href='http://commits.kde.org/kstars/f6e2d72aca9f89d3eb597589c6ab25ff0e0f1bfe'>Commit.</a> </li>
<li>Android build compiles and works well. Introduced new class KPaths that. <a href='http://commits.kde.org/kstars/dff369e15c0cc9f14546c7c31fd73757ab591144'>Commit.</a> </li>
<li>Excluded KDECompilerSettings from CMakeLists.txt because it causes the. <a href='http://commits.kde.org/kstars/6395454f744d99b92b6bdd2b3ea44047596e6866'>Commit.</a> </li>
<li>[NOT TESTED] This commit creates a special Misc catalog in the DB. <a href='http://commits.kde.org/kstars/65c7ce93ff1afbf386fb8249e857ee7c59f651cd'>Commit.</a> </li>
<li>New struct for Deep Sky Object data [might not be necessary]. <a href='http://commits.kde.org/kstars/1d1680a68afa0260c99725175ed06253efd851e6'>Commit.</a> </li>
<li>[NOT TESTED] UI to add / edit DSO entries into/from the database. <a href='http://commits.kde.org/kstars/49d1c3d7e05383646eca973a21833a0bb8a0570a'>Commit.</a> </li>
<li>Experimental testing: using Name Resolver in Find Dialog. <a href='http://commits.kde.org/kstars/b7f66ee4301b95898f5965c0542d9d0eae45bedd'>Commit.</a> </li>
<li>Add constructor to construct DeepSkyObject from CatalogEntryData. <a href='http://commits.kde.org/kstars/c086b6ceb9b715b415010f921b36ee6e6d8942f2'>Commit.</a> </li>
<li>Added rotation to planet images. SkyNodes now changes their position. <a href='http://commits.kde.org/kstars/f3cbe01c7593f02609945694d1d37aa74fe79548'>Commit.</a> </li>
<li>Added AsteroidsItem and CometsItem. Changed the structure of nodes:. <a href='http://commits.kde.org/kstars/0a1a6b9a8b98ad08b42da2feed6ff1aad9c0bcc5'>Commit.</a> </li>
<li>Setup KStars Lite build for Android. Except for a pair of classes, only. <a href='http://commits.kde.org/kstars/5a45a75d35a29ffad1cb579df46a1c73e92e2265'>Commit.</a> </li>
<li>Moved Projection enum from SkyMap to Projector. Introduced RootNode. <a href='http://commits.kde.org/kstars/ee72b1758783f73ea0894b1250a0ccca71b0293a'>Commit.</a> </li>
<li>Started setting up build for Android. Edited CMakeLists.txt so that. <a href='http://commits.kde.org/kstars/a7fd3b67ecd10dc936b19d79cb2da10cc3aaa982'>Commit.</a> </li>
<li>Commited using QtCreator and didn't add images. Added signal in. <a href='http://commits.kde.org/kstars/ea7ea2d07b25b8bcec4dc2068b8aa4c03e1ad158'>Commit.</a> </li>
<li>Added functions for saving config files upon exit. Added call to. <a href='http://commits.kde.org/kstars/83b44f4ff555ea513b80a9d5032c02ade2db6018'>Commit.</a> </li>
<li>Added new classes KStarsLite, SkyMapLite, SkyItem, PlanetsItem and nodes. <a href='http://commits.kde.org/kstars/c0fcc1e421e62b3c64c017ab3046d61009b40b7e'>Commit.</a> </li>
</ul>
<h3><a name='ksudoku' href='https://cgit.kde.org/ksudoku.git'>ksudoku</a> <a href='#ksudoku' onclick='toggle("ulksudoku", this)'>[Show]</a></h3>
<ul id='ulksudoku' style='display: none'>
<li>Master is still KDE4 based, so the extra-cmake-modules macros will not work. <a href='http://commits.kde.org/ksudoku/169da336398549e8e6e9f2aa13009b673ef5b719'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/ksudoku/da00e76553f2200ce9af793fbc99854354524165'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/ksudoku/a73e1e08fefb434840c6994c2fdada0bc167c557'>Commit.</a> </li>
</ul>
<h3><a name='ksystemlog' href='https://cgit.kde.org/ksystemlog.git'>ksystemlog</a> <a href='#ksystemlog' onclick='toggle("ulksystemlog", this)'>[Show]</a></h3>
<ul id='ulksystemlog' style='display: none'>
<li>Fix wrong menu item. <a href='http://commits.kde.org/ksystemlog/bcd654a75da8df62bf45afafc64e5f8b7cb86eca'>Commit.</a> </li>
</ul>
<h3><a name='kteatime' href='https://cgit.kde.org/kteatime.git'>kteatime</a> <a href='#kteatime' onclick='toggle("ulkteatime", this)'>[Show]</a></h3>
<ul id='ulkteatime' style='display: none'>
<li>Q_FOREACH -> C++11 for. <a href='http://commits.kde.org/kteatime/d4aa0fc394a6bdefceb841b1edab67e2175b3501'>Commit.</a> </li>
<li>Reimplement support for progress indicator. <a href='http://commits.kde.org/kteatime/12368fd24be1a16e9f388bca131a7e9a23cb9eb3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128380'>#128380</a>. See bug <a href='https://bugs.kde.org/279222'>#279222</a></li>
<li>Use new signal/slot syntax. <a href='http://commits.kde.org/kteatime/dcf6c7bed1d7d2e9de9aa1dcda1fc2d01a0a7bb3'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kteatime/091efb5541da9feeb7d4c30b94df12c634ee3c19'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kteatime/5bfd150bf7e619d4bde3543d38efd56b3a9cb342'>Commit.</a> </li>
</ul>
<h3><a name='ktnef' href='https://cgit.kde.org/ktnef.git'>ktnef</a> <a href='#ktnef' onclick='toggle("ulktnef", this)'>[Show]</a></h3>
<ul id='ulktnef' style='display: none'>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/ktnef/8aeea19017bbf4310e963bd218311475e9d7fbaf'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/ktnef/1a87e1d138a5af8b2cff36a3f61cd2bc8d2f3830'>Commit.</a> </li>
<li>Allow to create doc. <a href='http://commits.kde.org/ktnef/6026008d041a12ff299d36e9442d597a0f3480aa'>Commit.</a> </li>
</ul>
<h3><a name='ktouch' href='https://cgit.kde.org/ktouch.git'>ktouch</a> <a href='#ktouch' onclick='toggle("ulktouch", this)'>[Show]</a></h3>
<ul id='ulktouch' style='display: none'>
<li>Fix typo, thanks for spotting. <a href='http://commits.kde.org/ktouch/5caa6751bcf21e0bedf4ea449a4ea3fa32ddd4ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373367'>#373367</a></li>
<li>Course Editor: Fix Course Duplication. <a href='http://commits.kde.org/ktouch/6def386ea3fade0f12950b06552a15c7c6725933'>Commit.</a> </li>
<li>Fix Issues In Course de.neo.xml. <a href='http://commits.kde.org/ktouch/816c550bae1e0169e64c4285f1afcfe22e759a61'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347505'>#347505</a></li>
<li>Fix Typo In Course de.neo.xml. <a href='http://commits.kde.org/ktouch/c919d08b6968b3300842708f36aedf3264ece574'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349567'>#349567</a></li>
<li>Course Editor: Fix Lesson Selection. <a href='http://commits.kde.org/ktouch/367b90b9157cdfb35e3be10f0b9cf86d878d86c6'>Commit.</a> </li>
<li>Remove Exec line parameters from desktop file. <a href='http://commits.kde.org/ktouch/feba894ee4bcf3674f8a0f9ff75ad7f09c0d2f2c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129523'>#129523</a></li>
<li>Score Screen: Resurrect Learning Progress Legend. <a href='http://commits.kde.org/ktouch/4cb4b32d869acd6ca95b4dd833c996d6998d2444'>Commit.</a> </li>
<li>Balloon Component: Fix Reference Error. <a href='http://commits.kde.org/ktouch/cb61cf6538ef08c29d28283d6bfa430bc99176e6'>Commit.</a> </li>
<li>Fix Typo In Course 'de1'. <a href='http://commits.kde.org/ktouch/71ab9a88b1240fb95dccfdbc6cf5d37e7f1f6593'>Commit.</a> </li>
<li>Training Screen: Set Initial Keyboard Focus. <a href='http://commits.kde.org/ktouch/d9fdabbee3ea36e604540a0410bb90f2111bf04f'>Commit.</a> </li>
<li>Training Screen: Use A Loader For The Menu. <a href='http://commits.kde.org/ktouch/01a6ed83821e0af7b7b27e6f67898ae67785424c'>Commit.</a> </li>
<li>Training Widget: Fix Scroll Bar Mouse Interaction. <a href='http://commits.kde.org/ktouch/7cca7d0d97034db48bf8d02dbf59052767c7ce8d'>Commit.</a> </li>
<li>Fix Vertical Alignment On Training Screen. <a href='http://commits.kde.org/ktouch/24f41455a4c0fd00a99ea1cd5311df896813a2e5'>Commit.</a> </li>
<li>Update screenshots. <a href='http://commits.kde.org/ktouch/82cc640abe0f3f4e566f244b4ec1f98d2d3dd477'>Commit.</a> </li>
<li>Update/proofread KTouch docbooks for kf5. <a href='http://commits.kde.org/ktouch/1c5373cbb14dbd5c9c2efa388f2b353286afa621'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129310'>#129310</a></li>
<li>Update README. <a href='http://commits.kde.org/ktouch/c285f7d2cb2cad3d383bba0922dd0d56d2a615f7'>Commit.</a> </li>
<li>Update some screenshots. <a href='http://commits.kde.org/ktouch/eb567a6eb1aacc3a45f32e56c53f390bc06d4217'>Commit.</a> </li>
<li>Use ki18n_wrap_ui as required using xgettext for translation. <a href='http://commits.kde.org/ktouch/c225ee47e7e91a27c76459faa5b8b3228554547d'>Commit.</a> </li>
<li>Merge Branch 'frameworks'. <a href='http://commits.kde.org/ktouch/f26c9dd5e1f89b132da4af26fa36f23203875d59'>Commit.</a> </li>
<li>Fix Installation Of Mongolian Keyboard Layout. <a href='http://commits.kde.org/ktouch/5a0b0ff46aa2347c0a65d16109d1957998f0dec0'>Commit.</a> </li>
<li>Add Mongolian Cyrillic keyboard. <a href='http://commits.kde.org/ktouch/62e5ba0a056ce28594a4598ee941ca33122f56b8'>Commit.</a> </li>
<li>Home Screen: Fix Course Switching. <a href='http://commits.kde.org/ktouch/3c9193c969cbcc4b8e58f1a25e3007e19f760df0'>Commit.</a> </li>
<li>Training Screen: Activate Menu With Escape. <a href='http://commits.kde.org/ktouch/6c65872ee25eb7082723d6280f451b8d75a0597b'>Commit.</a> </li>
<li>Use QQuickView To Render The Trainer QtQuick Scene. <a href='http://commits.kde.org/ktouch/9fe18509b9090561b56b87f4bbdc77c11f0147f0'>Commit.</a> </li>
<li>Specify A Minimum Qt Version. <a href='http://commits.kde.org/ktouch/8fa790e23a9e1a5c4711f98d79505a0eeb42c091'>Commit.</a> </li>
<li>Fix Keyboard Control Issues On Training Screen. <a href='http://commits.kde.org/ktouch/be44b176fa51fb6fadf5963b571a36ecb9762257'>Commit.</a> </li>
<li>Deemphasize Training Screen Background. <a href='http://commits.kde.org/ktouch/6126bb1b23ad5ae1441bc821cd0fefd83b41ea91'>Commit.</a> </li>
<li>KeyItem: Fix Radius Of Shadow Element. <a href='http://commits.kde.org/ktouch/c7b7da4a8086c687159e8fa2454e85a5efcb3f62'>Commit.</a> </li>
<li>KeyItem: Remove Old Tinting Hack. <a href='http://commits.kde.org/ktouch/303915efbed2719712ec161ce411d68af7304804'>Commit.</a> </li>
<li>Fix Crash On Exit With Resource Editor. <a href='http://commits.kde.org/ktouch/a04275a4d3ca7e83f2592fea59920addfc317049'>Commit.</a> </li>
<li>Fix Crash On Exit. <a href='http://commits.kde.org/ktouch/0e9b3907861120f61bcb8cbd891e970c5738f2b2'>Commit.</a> </li>
<li>Fix Course Selection On Start. <a href='http://commits.kde.org/ktouch/b51ab8575bf37e9fa6d7af272cd93845d3287c1f'>Commit.</a> </li>
<li>Fix Crash Due Parentless QObjects. <a href='http://commits.kde.org/ktouch/1ce4b332dfb361c90cb41b6376c8ec6b019d895f'>Commit.</a> </li>
<li>Fix Context Variable Access In main.qml. <a href='http://commits.kde.org/ktouch/2bed2ed84cd038973c6b894bbf33e2bf40849223'>Commit.</a> </li>
<li>Qt5: Fix The X11 Helper. <a href='http://commits.kde.org/ktouch/e06b1c086ba375e27f859f56de0f423f3bedece9'>Commit.</a> </li>
<li>Bind KTouch Version To KDE Applications Version. <a href='http://commits.kde.org/ktouch/8cecca6d52387106a2fab707f5bc09993057ecd3'>Commit.</a> </li>
<li>Remove Debug Variables. <a href='http://commits.kde.org/ktouch/7ab9e93d17cb211b2cc25950784e7b0019062777'>Commit.</a> </li>
<li>Fix Button Icon In Profile Selector. <a href='http://commits.kde.org/ktouch/9fe7b630cc337cbc6caafb66ad340d44be93ba38'>Commit.</a> </li>
<li>Migrate profiles.db From KDE4 Location. <a href='http://commits.kde.org/ktouch/e8e1a109ff494d8da044a328c6e12cb2eede1590'>Commit.</a> </li>
<li>Unload QML engine when layout object gets deleted. <a href='http://commits.kde.org/ktouch/cbb065ade17493f563d0e47a2e699e4ab69c2c5d'>Commit.</a> </li>
<li>Clean Up Image Ressources. <a href='http://commits.kde.org/ktouch/590ae75bb07da81d1c299ad501ba93fc49826e3e'>Commit.</a> </li>
<li>Qt5: Port KeyboardLayout.qml And Friends. <a href='http://commits.kde.org/ktouch/dd390d9a99bf19a6812e1866cff999aa147fde9f'>Commit.</a> </li>
<li>Fix KeyChar::value Handling. <a href='http://commits.kde.org/ktouch/5519b0ec5da7e78430f5deb6042195961af49e1c'>Commit.</a> </li>
<li>Balloon.qml: Small Fixes And Improvements. <a href='http://commits.kde.org/ktouch/a99a1cc845e1db69f1b86601c0bde2a20c3b1936'>Commit.</a> </li>
<li>Qt5: Port ScoreScreen.qml. <a href='http://commits.kde.org/ktouch/9045bd318d945f308369522eb8f6cc722a8ee54c'>Commit.</a> </li>
<li>HomeScreen: Use A Button For Profile Selection. <a href='http://commits.kde.org/ktouch/e081cae79c1866b718c56c73f44b06b56beba4b5'>Commit.</a> </li>
<li>Qt5: Port Balloon.qml. <a href='http://commits.kde.org/ktouch/9b3d7ca0cfd4618bcc58dbc80b9a9b11c115dc77'>Commit.</a> </li>
<li>Use the ECM variable for the metainfo (appdata) directory. <a href='http://commits.kde.org/ktouch/9f74db1af586f43d5bc1b177b6e9dc90704b26a9'>Commit.</a> </li>
<li>Port away from removed slots. <a href='http://commits.kde.org/ktouch/5282569fe23cec583e93c06a7eb2b758ade198e8'>Commit.</a> </li>
<li>Only handle actual course changes. <a href='http://commits.kde.org/ktouch/69de35c675054c206b1d6347afc9cbe629811cfc'>Commit.</a> </li>
<li>Correctly initialize member pointers. <a href='http://commits.kde.org/ktouch/0b395ebcdaf621fa5b89c0d19b03e343942b9159'>Commit.</a> </li>
<li>Silence CMake policy CMP0063 warning. <a href='http://commits.kde.org/ktouch/7695642ad69f5dc47fc4b2e6773e1f1910f23c2d'>Commit.</a> </li>
<li>Basic updates for documentation (doc and man). <a href='http://commits.kde.org/ktouch/e675c836f141b82bb0d6ef6ef02e824dcc6597f8'>Commit.</a> </li>
<li>Use correct library name to fix the build. <a href='http://commits.kde.org/ktouch/6bc3ede380de6cf2977528765f62d5d4e267c43c'>Commit.</a> </li>
<li>Migration of Ktouch to kde4 settings. <a href='http://commits.kde.org/ktouch/237529a837d43b33fa3821de797acf19eb3fe4c6'>Commit.</a> </li>
<li>Fix build. <a href='http://commits.kde.org/ktouch/1fd50a43ee92359d0b4ec6ae3e9c4c26adeef7aa'>Commit.</a> </li>
<li>Make it build. <a href='http://commits.kde.org/ktouch/ffd02474e34abbc0800d5d8de3805bba73b384ff'>Commit.</a> </li>
<li>Rename icons and fix src/CMakeLists.txt so cmake can configure. <a href='http://commits.kde.org/ktouch/96346a6081db4f8f7e802f158973cd396943b245'>Commit.</a> </li>
<li>Qt5: Port TrainingScreenMenuOverlay. <a href='http://commits.kde.org/ktouch/21572d960420523e4ca8e19bb7600cd6b553c16d'>Commit.</a> </li>
<li>Qt5: Remove Reference To `theme`. <a href='http://commits.kde.org/ktouch/8d0c4fc8ef114ed47badd63e0b76d37dc55dce15'>Commit.</a> </li>
<li>Qt5: Port StatBox and friends. <a href='http://commits.kde.org/ktouch/eabbca36b73508d8394d0e4c51b9d59fc34be4b3'>Commit.</a> </li>
<li>Qt5: Port TrainingWidget. <a href='http://commits.kde.org/ktouch/69db84d929577877a25035ad01e4a6c8c565bbc2'>Commit.</a> </li>
<li>Fix MessageBox Icon. <a href='http://commits.kde.org/ktouch/9345e9d58aa55f1692c365b1a4a8d063ca17a110'>Commit.</a> </li>
<li>Fix Update Logic In Lesson Painter. <a href='http://commits.kde.org/ktouch/9f1bbc100e8ae59a76efed9927a5299b3649f085'>Commit.</a> </li>
<li>Qt5: Port Keyboard.qml and friends. <a href='http://commits.kde.org/ktouch/228a381591793507f8b73d8cc41e95ef2e8f35ee'>Commit.</a> </li>
<li>Qt5: Port TrainingScreenToolbar.qml. <a href='http://commits.kde.org/ktouch/feaca83a6e885e8c841475c217596c16f03b71b3'>Commit.</a> </li>
<li>Remove Deleted File From CMakeLists.txt. <a href='http://commits.kde.org/ktouch/4d7e764349512d4fae16f779fc804e69a19b6b24'>Commit.</a> </li>
<li>Qt5: Port TrainingScreen.qml. <a href='http://commits.kde.org/ktouch/8e0bf167200ae4de8716e21b7f7f3ba1d2e8b244'>Commit.</a> </li>
<li>Remove Unused Component. <a href='http://commits.kde.org/ktouch/9d52d514099709568f72929b692b4d5dec58f198'>Commit.</a> </li>
<li>Qt5: Blur Background Of Lesson Locked Notice. <a href='http://commits.kde.org/ktouch/2c15ae1696737683f40b800400ab9374ca58e4a7'>Commit.</a> </li>
<li>Qt5: Port ProfileSelector.qml And Friends. <a href='http://commits.kde.org/ktouch/a5000ea6af2f0a8abd67410688a8eea33c52aecd'>Commit.</a> </li>
<li>Qt5: Add A Command Line Option For QML Import Dirs. <a href='http://commits.kde.org/ktouch/1d1ff6c5651bb95a61363628dc2ac3ca18b02e70'>Commit.</a> </li>
<li>Qt5: Custom Lesson Selector Fixes. <a href='http://commits.kde.org/ktouch/c81b1d693abfc423e7c650986abb522ee58b3523'>Commit.</a> </li>
<li>Qt5: Fix Layout Of Custom Lesson Editor Dialog. <a href='http://commits.kde.org/ktouch/50dd985e525c0489b9de3895445eb02d04e6d5a4'>Commit.</a> </li>
<li>Qt5: Port CourseSelector.qml And Friends. <a href='http://commits.kde.org/ktouch/148765963503077c1033345a032df84c63d02e25'>Commit.</a> </li>
<li>Qt5: Fix DB File Access. <a href='http://commits.kde.org/ktouch/442b70c94757cb1ef162120f7009e2761b5520c5'>Commit.</a> </li>
<li>Qt5: Port InitialProfileForm.qml And Friends. <a href='http://commits.kde.org/ktouch/594617c89af8abca238f9e77c7b9b1ab0ec4e8c0'>Commit.</a> </li>
<li>Qt5: Port main.qml To Qt Quick 2. <a href='http://commits.kde.org/ktouch/00d90c30371310cd1aef0c0ac1ecac8d37d504d1'>Commit.</a> </li>
<li>Qt5: Fix Command Line Parsing. <a href='http://commits.kde.org/ktouch/6a2217b2a5d941faa8c14549461a725650d27ec5'>Commit.</a> </li>
<li>Port to Qt5/KF5: It builds!. <a href='http://commits.kde.org/ktouch/570d74cbf76da96e18ecc715acf4bbb8053a2bf7'>Commit.</a> </li>
</ul>
<h3><a name='ktp-auth-handler' href='https://cgit.kde.org/ktp-auth-handler.git'>ktp-auth-handler</a> <a href='#ktp-auth-handler' onclick='toggle("ulktp-auth-handler", this)'>[Show]</a></h3>
<ul id='ulktp-auth-handler' style='display: none'>
<li>Ported away from deprecated setProgramIconName(). <a href='http://commits.kde.org/ktp-auth-handler/be85071f0bb735e1b6ab90dcc0049db40cdc3fe8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129017'>#129017</a></li>
<li>Fixed uninitialized variable usage. <a href='http://commits.kde.org/ktp-auth-handler/03508d05afe02c17ed7066d78d6f42283c2e14e5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129016'>#129016</a></li>
</ul>
<h3><a name='ktp-common-internals' href='https://cgit.kde.org/ktp-common-internals.git'>ktp-common-internals</a> <a href='#ktp-common-internals' onclick='toggle("ulktp-common-internals", this)'>[Show]</a></h3>
<ul id='ulktp-common-internals' style='display: none'>
<li>[otr-proxy] Fixed incorrect check of adaptee method existance (channel). <a href='http://commits.kde.org/ktp-common-internals/2c75666285464592f5ee8d8d9ea9780e51d9dfb3'>Commit.</a> </li>
<li>[declarative] Various style fixes that have accumulated here through time. <a href='http://commits.kde.org/ktp-common-internals/cf4fac5c8b6570adc94baa108f638cc4050ec3a8'>Commit.</a> </li>
<li>[message-processor] Introduce separate logging category. <a href='http://commits.kde.org/ktp-common-internals/769e7a06ddfbc9adce044105b1af8e9c548c6080'>Commit.</a> </li>
<li>[declarative] Slightly more informative warning message when sending emtpy message. <a href='http://commits.kde.org/ktp-common-internals/7e11a99b4f096a4857ab842e4da9eb162c48950d'>Commit.</a> </li>
<li>[declarative] Remove an (years-)outdated qWarning. <a href='http://commits.kde.org/ktp-common-internals/f226dc0a128a3bfe4cd2624499994d1208a62fbc'>Commit.</a> </li>
<li>[declarative] Make sure the Private is constructed with 2nd ctor too. <a href='http://commits.kde.org/ktp-common-internals/f36d316e34f292879da4398fb67e3a04deb0c61c'>Commit.</a> </li>
<li>[declarative] Conversation::isValid() needs to be const. <a href='http://commits.kde.org/ktp-common-internals/98111fa320a00eded781ad979091beebc10e7cac'>Commit.</a> </li>
<li>[declarative] Return 0 unread messages if channel is null. <a href='http://commits.kde.org/ktp-common-internals/a0492908262fe1ae1fa3d6e8e2cf7b9a229a3368'>Commit.</a> </li>
<li>Add missing copyright. <a href='http://commits.kde.org/ktp-common-internals/d58fd6386a4cfc19e14b371c4b0af1a7acf79a19'>Commit.</a> </li>
<li>[declarative] Guard for empty channel. <a href='http://commits.kde.org/ktp-common-internals/294e1b91813ae6862efb834a884d41cc11e6d95e'>Commit.</a> </li>
<li>[declarative] Emit Conversation::validityChanged at the end. <a href='http://commits.kde.org/ktp-common-internals/2f406bd139bf2f02a286d10998b8611f59aca2b6'>Commit.</a> </li>
<li>[declarative] Add property hasUnreadMessages directly to Conversation. <a href='http://commits.kde.org/ktp-common-internals/a63c02dc22b6271382c8d33fdca8165426642595'>Commit.</a> </li>
<li>[kpeople] Add dedicated property for account display name. <a href='http://commits.kde.org/ktp-common-internals/c0bd88ef554105375d4f8a12a32cf963c0aefb00'>Commit.</a> </li>
<li>[declarative] Guard the MessagesModel::data from out-of-bounds indexes. <a href='http://commits.kde.org/ktp-common-internals/f053d76283b1955c4f37bf8fb5ce571af5bb9d27'>Commit.</a> </li>
<li>[declarative] Guard against null stuff. <a href='http://commits.kde.org/ktp-common-internals/1a0b7e2843e15beb543fda4d954e50417f4a7228'>Commit.</a> </li>
<li>[declarative] Create MessagesModel on setting new text channel. <a href='http://commits.kde.org/ktp-common-internals/46e4a5b716bf3a0d69dc8e9dc41d6fcbd5ff7911'>Commit.</a> </li>
<li>[declarative] Move the channel setup to setTextChannel. <a href='http://commits.kde.org/ktp-common-internals/1c6ebe036bfd641c7f147faabc1b76fe2d32c762'>Commit.</a> </li>
<li>[declarative] Init the ConversationPrivate members. <a href='http://commits.kde.org/ktp-common-internals/58a0da25ce92f8ee6f8bf67a35caf160e7db8aae'>Commit.</a> </li>
<li>[Declarative/MessagesModel] Fixed type of modelIndex in onMessageSent(). <a href='http://commits.kde.org/ktp-common-internals/2706d8d60cedcdcc31067122c909797d506ff629'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128961'>#128961</a></li>
<li>[Declarative/MessagesModel] Implemented message sort by sent timestamp. <a href='http://commits.kde.org/ktp-common-internals/cbd7bb27f5caa776c3a41bcb5d35bcf4cb2376b2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128954'>#128954</a></li>
<li>[debugger] TelepathyProcess extracted from DebugMessageView. <a href='http://commits.kde.org/ktp-common-internals/33af28091656d4b39412c1a0064b83f28e3064ac'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128847'>#128847</a></li>
<li>[KTp/Message] Direction of received message now depends on sender. <a href='http://commits.kde.org/ktp-common-internals/431db458de1417764ecdef00974fca4c412e19f8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128867'>#128867</a></li>
<li>[debugger] Cleanup and reformat, no any change of logic. <a href='http://commits.kde.org/ktp-common-internals/17b0f5e4a7600487b9d92e766c6f1f44b8aa014a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128845'>#128845</a></li>
<li>[otr-proxy] Fixed incorrect check of adaptee method existance. <a href='http://commits.kde.org/ktp-common-internals/ccdf6a3c006a0f385a1224ee45f89a3776aa0fb9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128844'>#128844</a></li>
<li>[debugger] Fixed timestamp format (ms width). <a href='http://commits.kde.org/ktp-common-internals/b814213ac6cfdcdbfab3986fed15ed4732168e95'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128809'>#128809</a></li>
<li>[kpeople] Final initialization step moved from loadCache to constructor. <a href='http://commits.kde.org/ktp-common-internals/3989778fafde7fbf8bf2e4b7b439a0b401050187'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128807'>#128807</a></li>
</ul>
<h3><a name='ktp-contact-list' href='https://cgit.kde.org/ktp-contact-list.git'>ktp-contact-list</a> <a href='#ktp-contact-list' onclick='toggle("ulktp-contact-list", this)'>[Show]</a></h3>
<ul id='ulktp-contact-list' style='display: none'>
<li>Avoid using QCoreApplication before it was created. <a href='http://commits.kde.org/ktp-contact-list/e0541440b45fc5a53324bc764c1a99bc284fcaa5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128711'>#128711</a></li>
</ul>
<h3><a name='ktp-desktop-applets' href='https://cgit.kde.org/ktp-desktop-applets.git'>ktp-desktop-applets</a> <a href='#ktp-desktop-applets' onclick='toggle("ulktp-desktop-applets", this)'>[Show]</a></h3>
<ul id='ulktp-desktop-applets' style='display: none'>
<li>[Quick Chat] Remove unused popupSide property. <a href='http://commits.kde.org/ktp-desktop-applets/0a981e934f37a47fb2ac69c19233a4d24c2c9a3f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129069'>#129069</a></li>
<li>[Chat List] Activate chat also on Enter press. <a href='http://commits.kde.org/ktp-desktop-applets/9f56556e72a79e4499b945ed180fa206c2e9e17e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129052'>#129052</a></li>
<li>[Quick Chat] Add context menu for copying text and links. <a href='http://commits.kde.org/ktp-desktop-applets/8328a1295a7bded16549998411af2446d774dc79'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128089'>#128089</a></li>
<li>[Quick Chat] Unset PlasmaComponents.Label default height. <a href='http://commits.kde.org/ktp-desktop-applets/6a4906090db93923b203dfba6bd2a9f681e03e49'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128135'>#128135</a></li>
<li>[Quick Chat] Allow cycling through chats with Ctrl+(Shift)+Tab. <a href='http://commits.kde.org/ktp-desktop-applets/d9c85361b248d36f4fafaf054e332871e47eb49c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128138'>#128138</a></li>
<li>[Contact List Applet] Drop custom compact representation. <a href='http://commits.kde.org/ktp-desktop-applets/2da2d251e26be335afd4df1877aaf503c24518dd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128976'>#128976</a></li>
</ul>
<h3><a name='ktp-text-ui' href='https://cgit.kde.org/ktp-text-ui.git'>ktp-text-ui</a> <a href='#ktp-text-ui' onclick='toggle("ulktp-text-ui", this)'>[Show]</a></h3>
<ul id='ulktp-text-ui' style='display: none'>
<li>[lib] Disable QWebView's object cache. <a href='http://commits.kde.org/ktp-text-ui/6b93b6fdf3bc0d095e4746815623571506f47c77'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128884'>#128884</a></li>
<li>[lib] Rename "KopeteStyleDebug" to "KTpStyleDebug". <a href='http://commits.kde.org/ktp-text-ui/4c35e126d176a5dae1b5013e6b3b5a54a96e7194'>Commit.</a> </li>
<li>[lib] Insert chat text edit's history as plain text. <a href='http://commits.kde.org/ktp-text-ui/02a5ed8f5fa324ed12e1d4dab978e33a8e9fb4b3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128842'>#128842</a></li>
<li>[lib] Fix message classes for history items. <a href='http://commits.kde.org/ktp-text-ui/7e429544a6f5530365c22ca5f83b4fc4e97de658'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348929'>#348929</a>. Code review <a href='https://git.reviewboard.kde.org/r/128841'>#128841</a></li>
<li>[filters] Added Geopoint filter plugin. <a href='http://commits.kde.org/ktp-text-ui/78a720b656a84340d026dc9085864a325c431e37'>Commit.</a> </li>
<li>App: Cleanup headers. <a href='http://commits.kde.org/ktp-text-ui/f8f3729be03aee93352a3dbd65f45df8acf6aec9'>Commit.</a> </li>
</ul>
<h3><a name='kturtle' href='https://cgit.kde.org/kturtle.git'>kturtle</a> <a href='#kturtle' onclick='toggle("ulkturtle", this)'>[Show]</a></h3>
<ul id='ulkturtle' style='display: none'>
<li>Remove existance checks on saving. <a href='http://commits.kde.org/kturtle/acb376348fa0baa5d1eab39f08d0a55d2baaf0b0'>Commit.</a> </li>
<li>Update screenshots to kf5. <a href='http://commits.kde.org/kturtle/00f861e6050b11597e14c7895abb734b53f1ac39'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kturtle/2abadda25d5c1e805a81b7848e00ab6fb0251093'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kturtle/576d37c0b1e655d7badde806c0ab24d3c25d9724'>Commit.</a> </li>
</ul>
<h3><a name='kwalletmanager' href='https://cgit.kde.org/kwalletmanager.git'>kwalletmanager</a> <a href='#kwalletmanager' onclick='toggle("ulkwalletmanager", this)'>[Show]</a></h3>
<ul id='ulkwalletmanager' style='display: none'>
<li>Doc: fix typo. <a href='http://commits.kde.org/kwalletmanager/4113482f2326be544d48f5b17b308f600619cf5b'>Commit.</a> </li>
<li>Don't give the user an empty error dialog on permission denied. <a href='http://commits.kde.org/kwalletmanager/f655d13830df362427c7accd01e12ac07e17558d'>Commit.</a> </li>
<li>Fix icon names. <a href='http://commits.kde.org/kwalletmanager/5d35eb2c3b82979994b75c116d8c57ac8c8e398a'>Commit.</a> </li>
<li>Fix icon installation path. <a href='http://commits.kde.org/kwalletmanager/7bb9e92f26074ec892df4fe0a790567097221a9f'>Commit.</a> </li>
<li>Remove KDELibs4Support. <a href='http://commits.kde.org/kwalletmanager/9a7897eed5473009f444f52d9c27165c0e776506'>Commit.</a> </li>
<li>Port KWMapEditor to QTextEdit. <a href='http://commits.kde.org/kwalletmanager/ed3bd2e8ca8daceef54106ec8bcac3531f443011'>Commit.</a> </li>
<li>Fix minor glitches. <a href='http://commits.kde.org/kwalletmanager/fef888c25f32002dc0c463b2d7fe1b3d78ef7452'>Commit.</a> </li>
<li>Update KWalletmanager docbook to 16.04. <a href='http://commits.kde.org/kwalletmanager/176decc75c86922aad91102273d24539e6e06a31'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128378'>#128378</a></li>
<li>Port away from kinputdialog. <a href='http://commits.kde.org/kwalletmanager/ced980e4694962af7c348e2f269f8cac2b9a134c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128414'>#128414</a></li>
<li>Port away from deprecated Qt methods. <a href='http://commits.kde.org/kwalletmanager/ac61e7597ac45a4301b3bd1b8f5eab124e3e5c9b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128415'>#128415</a></li>
</ul>
<h3><a name='kwave' href='https://cgit.kde.org/kwave.git'>kwave</a> <a href='#kwave' onclick='toggle("ulkwave", this)'>[Show]</a></h3>
<ul id='ulkwave' style='display: none'><li>New in this release</li></ul>
<h3><a name='kwordquiz' href='https://cgit.kde.org/kwordquiz.git'>kwordquiz</a> <a href='#kwordquiz' onclick='toggle("ulkwordquiz", this)'>[Show]</a></h3>
<ul id='ulkwordquiz' style='display: none'>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/kwordquiz/86aa32897d6f13e9fb6fb32d429f4c4298c6e727'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/kwordquiz/4c134281790b71663c91c835fde9a4fddd7a7d52'>Commit.</a> </li>
</ul>
<h3><a name='libgravatar' href='https://cgit.kde.org/libgravatar.git'>libgravatar</a> <a href='#libgravatar' onclick='toggle("ullibgravatar", this)'>[Show]</a></h3>
<ul id='ullibgravatar' style='display: none'>
<li>Use unique network configure manager. <a href='http://commits.kde.org/libgravatar/81bc266939033144ad736acfc7a73bb7dadd4366'>Commit.</a> </li>
<li>Don't settings https. <a href='http://commits.kde.org/libgravatar/670996d0a5105446b594019841d147c3b9a918d2'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/libgravatar/d0c06d5d4f2a8b6b54d72d1cb5133e2b99eff3fb'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/libgravatar/1c3d1ec31f59d44ac45c796ca27c1a8c9eab8a9d'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/libgravatar/244cb19765ab3ec073fee62f9a28d2a8aaa7d52a'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/libgravatar/35abb9eba2173069d82ca5628dcc8a15e9192c85'>Commit.</a> </li>
<li>Fix find_package version: these are KF5 packages. <a href='http://commits.kde.org/libgravatar/d4e04797f5919598ef09fe3c5db295600739c29c'>Commit.</a> </li>
<li>Fix compile: KTextWidgets and KIO are required. <a href='http://commits.kde.org/libgravatar/f8169fbb640373dfbef3f73d1f637818a1d33ef6'>Commit.</a> </li>
<li>Add metainfo. <a href='http://commits.kde.org/libgravatar/0641c8ac0f31e368397430771c41d6e436a2dda5'>Commit.</a> </li>
</ul>
<h3><a name='libkcddb' href='https://cgit.kde.org/libkcddb.git'>libkcddb</a> <a href='#libkcddb' onclick='toggle("ullibkcddb", this)'>[Show]</a></h3>
<ul id='ullibkcddb' style='display: none'>
<li>Do not follow KDE Application for the library version. <a href='http://commits.kde.org/libkcddb/705b0e737d257f18fe191fda6978d204d79a4f8d'>Commit.</a> </li>
<li>Fix for the fix: properly look for libmusicbrainz5. <a href='http://commits.kde.org/libkcddb/bb30765a44df9ada47953477533b0a07ffabfc0d'>Commit.</a> </li>
<li>Fix linking to MusicBrainz5. <a href='http://commits.kde.org/libkcddb/f787e70fd528bf7620ca6714b800c566de98bb97'>Commit.</a> </li>
<li>[cmake] Move call to feature_summary to the end. <a href='http://commits.kde.org/libkcddb/d69b92d1fd9d5ab4e5aaea31a4e974c54a244f5a'>Commit.</a> </li>
<li>Use QDialogButtonBox. <a href='http://commits.kde.org/libkcddb/327e77eb6c2f2bc1bbc08b48ae19c98434d4115c'>Commit.</a> </li>
<li>Load translation catalog for kcm + library. <a href='http://commits.kde.org/libkcddb/df41315cc50e2275f83404e20005903d3e3fc3e4'>Commit.</a> </li>
<li>Fix file conflicts with kdelibs4-version issue. <a href='http://commits.kde.org/libkcddb/d260f55955ce13891a681b34d344157c08445846'>Commit.</a> </li>
<li>Fix build for testcase. <a href='http://commits.kde.org/libkcddb/d1b7836cbac7240fb4224a126514fbdd61353d99'>Commit.</a> </li>
<li>Follow KF5 style header generation and keep the original project version. <a href='http://commits.kde.org/libkcddb/aeaf3341989fedca65b18d1dcd0e5a006a811d61'>Commit.</a> </li>
<li>Update docbook dtd. <a href='http://commits.kde.org/libkcddb/1b6ba68d7e773591c14cba2b4bf80229547339a1'>Commit.</a> </li>
<li>Make tests optional. <a href='http://commits.kde.org/libkcddb/4aacbc05c1f7c8f171b3e4ce76f54073f969264c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122547'>#122547</a></li>
<li>Ensure i18n() are connected to proper catalogs. <a href='http://commits.kde.org/libkcddb/be53b333e6405cbcd34ef2b8d99debe88bfe8f1d'>Commit.</a> </li>
<li>Splitted library into two: kccdb and kcddbwidgets. <a href='http://commits.kde.org/libkcddb/bfe7453628786247801f9c008ab172edbe726a07'>Commit.</a> </li>
<li>Export also catogires.h header. <a href='http://commits.kde.org/libkcddb/162b9c8e9dcbdbff60f9e6586338423ab5f1d7bb'>Commit.</a> </li>
<li>Set correct library version based on the current version of KF5. <a href='http://commits.kde.org/libkcddb/af905c92f3333442b2d67eb11e02c8bde8eb4858'>Commit.</a> </li>
<li>Use Musicbrainz library as imported target. <a href='http://commits.kde.org/libkcddb/f31cfdd44820561a7f73235d0a29912696c34aca'>Commit.</a> </li>
<li>Kcmshell4 -> kcmshell5. <a href='http://commits.kde.org/libkcddb/d5986df3f66bdb464ae009f86e8abec98fae6636'>Commit.</a> </li>
<li>Ported leftovers from KDE3 to Qt5. <a href='http://commits.kde.org/libkcddb/c586a7ab60c65720d23c3304ea827210da450944'>Commit.</a> </li>
<li>Cleaned up header inclusion. <a href='http://commits.kde.org/libkcddb/73cc9990f2982d0259eba5b7b3ce65cc8d4037c3'>Commit.</a> </li>
<li>Removed dependency on KDELibs4Support. <a href='http://commits.kde.org/libkcddb/1d4f4777318e4c464accb0c33a91fcdc753b5022'>Commit.</a> </li>
<li>Commented out KGlobal::locale()->insertCatalog() usage and showing. <a href='http://commits.kde.org/libkcddb/0a0deb48888561290bb29fabb13f8292f06d2a77'>Commit.</a> </li>
<li>Ported KSocketFactory to a plain QTcpSocket. <a href='http://commits.kde.org/libkcddb/d02cc713a9cfe46221ad7045f2d3855d20c666a0'>Commit.</a> </li>
<li>Ported KUrl to QUrl. <a href='http://commits.kde.org/libkcddb/c7feb4e99a9f75c5e9f9a9201fcb12bd6a9e7f94'>Commit.</a> </li>
<li>Ported straightforward cases to KF5. <a href='http://commits.kde.org/libkcddb/ec37677e234994a92d31b9de745881bddd34bbe3'>Commit.</a> </li>
<li>First port to KF5 with the help of KDELibs4Support. <a href='http://commits.kde.org/libkcddb/380e90cbd39ecea86a4e384d4ebff4688c88049d'>Commit.</a> </li>
</ul>
<h3><a name='libkcompactdisc' href='https://cgit.kde.org/libkcompactdisc.git'>libkcompactdisc</a> <a href='#libkcompactdisc' onclick='toggle("ullibkcompactdisc", this)'>[Show]</a></h3>
<ul id='ullibkcompactdisc' style='display: none'>
<li>Do not follow KDE Application for the library. <a href='http://commits.kde.org/libkcompactdisc/a6f253301ca25b88493829bb13649add5e6748e6'>Commit.</a> </li>
<li>[cmake] Move call to feature_summary to the end. <a href='http://commits.kde.org/libkcompactdisc/b33e0fe7e063e2d862251b33a736a490a1fef74d'>Commit.</a> </li>
<li>Fic Messages.sh, code was moved into subdir src. <a href='http://commits.kde.org/libkcompactdisc/60135da39af0ee03b8600a084cc5a079e3a8407e'>Commit.</a> </li>
<li>Update CMakeLists and testcase. <a href='http://commits.kde.org/libkcompactdisc/c0f4fb1f115b73b733bd9970d96b95a1204d9fb7'>Commit.</a> </li>
<li>Update CMakeLists to follow KF5 v5.24.x style. <a href='http://commits.kde.org/libkcompactdisc/c5ebed95fe503c6f67d9765aa429e38dab85a081'>Commit.</a> </li>
<li>Fix abs wrong overload for GCC 6.x. <a href='http://commits.kde.org/libkcompactdisc/4c1b42b9252b430288af46cecc650fe846c07482'>Commit.</a> </li>
<li>Use KLocalizedString instead of klocale.h. <a href='http://commits.kde.org/libkcompactdisc/eaa16c4e88d67bcf14fc112b45f4fd45045320bc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127732'>#127732</a></li>
<li>Silence CMake policy CMP0063 warning. <a href='http://commits.kde.org/libkcompactdisc/ac661522c6b85d9309437676b3be2f1bf7aebe00'>Commit.</a> </li>
<li>KF5CompactDiscConfig.cmake.in: Fix typo. <a href='http://commits.kde.org/libkcompactdisc/73199f84b84fd62c36eb382d61f803a08f47048a'>Commit.</a> </li>
<li>Add call to stateChanged to set initial disc state in phonon interface. <a href='http://commits.kde.org/libkcompactdisc/13f7a3e35d6d05296f067c84e887317fb4f95bdd'>Commit.</a> </li>
<li>Improve test. <a href='http://commits.kde.org/libkcompactdisc/8f89fbe888c7e00089738877360a160a7bca9017'>Commit.</a> </li>
<li>Generate export headers through CMake. Bumps CMake req to 3.0.2. <a href='http://commits.kde.org/libkcompactdisc/8de9e3158624292243e10ab64bd14652e0dd472a'>Commit.</a> </li>
<li>Re-enable WorkManLib. <a href='http://commits.kde.org/libkcompactdisc/8da63fa7814aeecb9262ff028733d689e85ce579'>Commit.</a> </li>
<li>Add qtcreator gitignore. <a href='http://commits.kde.org/libkcompactdisc/1475e38606ac04d3594e7213b2318c63dc960f4a'>Commit.</a> </li>
<li>Search for KDELibs4Support to fix the build. <a href='http://commits.kde.org/libkcompactdisc/3f2f2e0ce721609a85f9ad48e380c4b3fe8bb830'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125403'>#125403</a></li>
<li>Add discinfo test. <a href='http://commits.kde.org/libkcompactdisc/4c1a84e03e766fa942a34b11117447964b7d6afe'>Commit.</a> </li>
<li>Drop Qt requirements to 5.4. <a href='http://commits.kde.org/libkcompactdisc/0ad727fb51f049852b407c1ee4d39ef1c52bd79f'>Commit.</a> </li>
<li>Add gitignore for QtCreator. <a href='http://commits.kde.org/libkcompactdisc/7085c4bbb623288f253213bd17ed008138550749'>Commit.</a> </li>
<li>New-style header includes. <a href='http://commits.kde.org/libkcompactdisc/f8374a1ae9a76a55a682be42c46b55cdf81b52f4'>Commit.</a> </li>
<li>ECM-ise CMake. WMLib backend disabled. <a href='http://commits.kde.org/libkcompactdisc/4a465e259a62ebd6b1c4993e5821968777aeda6d'>Commit.</a> </li>
<li>Set library name to libKF5CompactDisc and set VERSION/SOVERSION to 5.0.0. <a href='http://commits.kde.org/libkcompactdisc/494ac110a52a64aa7b7913fc75bdfc71b27cdad4'>Commit.</a> </li>
<li>Change from KUrl to QUrl. <a href='http://commits.kde.org/libkcompactdisc/2fc0667f7ec2ed3e386a39cd1abe42c4351e5d46'>Commit.</a> </li>
<li>Initial port to Qt5/KF5 and Phonon4Qt5. <a href='http://commits.kde.org/libkcompactdisc/619b57c56e583f8b0f5db0cc50449b63de91c4bf'>Commit.</a> </li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Show]</a></h3>
<ul id='ullibkdepim' style='display: none'>
<li>Use isEmpty here. <a href='http://commits.kde.org/libkdepim/9314489de88d33d195ac4d4f33113afca798a912'>Commit.</a> </li>
<li>Rename it. <a href='http://commits.kde.org/libkdepim/1da7735b84a75a5e4d2c29308df414901335820b'>Commit.</a> </li>
<li>Use Qt::darkGreen here. <a href='http://commits.kde.org/libkdepim/daebc77e766c14f08d855537296e972311bfbc7f'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/libkdepim/b08a94b9a28de81dc9f4f74a1adfd004fe9f5b71'>Commit.</a> </li>
<li>Convert categories to org.kde.pim.*. <a href='http://commits.kde.org/libkdepim/eb6cd194f85a4ce62a330ec0b62ae6391959d574'>Commit.</a> </li>
<li>Don't install files twice. <a href='http://commits.kde.org/libkdepim/b320add491c42cedd93b3f7a2f37a484af32ed08'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/libkdepim/f76e10fec67d384d38bb971cbeeebfd6f8d8318d'>Commit.</a> </li>
<li>Port to qregularexpression. <a href='http://commits.kde.org/libkdepim/88246c7f6416861ead323f63826f6c02eb40dd34'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/libkdepim/6fece88c8a7ce0fb6ed27a67561c6837be97fff5'>Commit.</a> </li>
<li>Add autotest to migrate to QRegularExpression. <a href='http://commits.kde.org/libkdepim/c2b51f1b8d22c455c17d8bb2ae5eecaf59e6e433'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/libkdepim/d7ce6efa81f74e7d6f5e749807676c01c7832e01'>Commit.</a> </li>
<li>New API, bump version. <a href='http://commits.kde.org/libkdepim/617a59de3785deafa0c048dc4d7811bc7d83fb1d'>Commit.</a> </li>
<li>AddresseeLineEdit: allow disabling completion from Akonadi. <a href='http://commits.kde.org/libkdepim/f848121d7b062e1cb320b56f31e51164e91d3ed2'>Commit.</a> </li>
<li>AddresseeLineEdit: create Akonadi::Session on-demand. <a href='http://commits.kde.org/libkdepim/e093a7ebb5817d956c7e3355d61c113f8f06c1b8'>Commit.</a> </li>
<li>Remove not necessary private Q_SLOTS. <a href='http://commits.kde.org/libkdepim/80bc0007e68ad778009f104bf7453d22b6c46e9e'>Commit.</a> </li>
</ul>
<h3><a name='libkeduvocdocument' href='https://cgit.kde.org/libkeduvocdocument.git'>libkeduvocdocument</a> <a href='#libkeduvocdocument' onclick='toggle("ullibkeduvocdocument", this)'>[Show]</a></h3>
<ul id='ullibkeduvocdocument' style='display: none'>
<li>Fix faulty assignment and equal to operator of KEduVocText. <a href='http://commits.kde.org/libkeduvocdocument/e410bd83ea425a43686394a38c881d5cfb341697'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128975'>#128975</a></li>
</ul>
<h3><a name='libkface' href='https://cgit.kde.org/libkface.git'>libkface</a> <a href='#libkface' onclick='toggle("ullibkface", this)'>[Show]</a></h3>
<ul id='ullibkface' style='display: none'>
<li>Add opencv 3.1 support. <a href='http://commits.kde.org/libkface/ec6e82328cac37c9d59b49245b18c3f0abee4d8f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126833'>#126833</a></li>
</ul>
<h3><a name='libkgeomap' href='https://cgit.kde.org/libkgeomap.git'>libkgeomap</a> <a href='#libkgeomap' onclick='toggle("ullibkgeomap", this)'>[Show]</a></h3>
<ul id='ullibkgeomap' style='display: none'>
<li>Add Marble also as dependency in the KGeoMap CMake Config file. <a href='http://commits.kde.org/libkgeomap/8c158f952cac8b83416ccb4b4e8881f5a948bb96'>Commit.</a> </li>
<li>Re-add Marble to public link interface of KF5KGeoMap. <a href='http://commits.kde.org/libkgeomap/311a25a2fac4fe36f00d1eef8238c084714328d4'>Commit.</a> </li>
<li>Use QTEST_GUILESS_MAIN. <a href='http://commits.kde.org/libkgeomap/ddf27f679765845a22b3f9909f3890b5f738a0da'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129370'>#129370</a></li>
<li>Load Marble translations. <a href='http://commits.kde.org/libkgeomap/953e832b6d364f85e62ad481b466fece6cc2c0ba'>Commit.</a> </li>
<li>Remove code variants for outdated Qt4 Marble versions. <a href='http://commits.kde.org/libkgeomap/41a46ee8003891977794625c6040710dc384d896'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://commits.kde.org/libkgeomap/099e160c8a4840c3ef6134a5ef9265ea5b7398b3'>Commit.</a> </li>
<li>Find Marble by its CMake Config files. <a href='http://commits.kde.org/libkgeomap/18ab26fb9474178ea330fcb43c2cc0801971786e'>Commit.</a> </li>
<li>Clear mapbackend.h from marble includes. <a href='http://commits.kde.org/libkgeomap/180805657176a8b30d6be421b720722d90e27b64'>Commit.</a> </li>
<li>Remove unneeded explicit listing of Qt5::Gui & Qt5::Core for mapbackends. <a href='http://commits.kde.org/libkgeomap/ed55cd83eb431a9c76f549c3dd90c12333f1781b'>Commit.</a> </li>
<li>Explicitely include marble/GeoDataLatLonAltBox.h for future compatibility. <a href='http://commits.kde.org/libkgeomap/17ffbdf033fe985717e538a781cf32faa4d41ef2'>Commit.</a> </li>
</ul>
<h3><a name='libkipi' href='https://cgit.kde.org/libkipi.git'>libkipi</a> <a href='#libkipi' onclick='toggle("ullibkipi", this)'>[Show]</a></h3>
<ul id='ullibkipi' style='display: none'>
<li>Apply patch to use find_dependency as one dependency per call. <a href='http://commits.kde.org/libkipi/8dce607daa8dc6722c56087a5eff6f5b0f02e3a5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129555'>#129555</a></li>
<li>Use kipi qrc. <a href='http://commits.kde.org/libkipi/0251c37850b0ce7623981f0697a02b221d7ead38'>Commit.</a> </li>
<li>Fix ABI/API ID since pure virtual method fingerprints have changed. <a href='http://commits.kde.org/libkipi/593cf8bf10349ce459fdf26c800f9b5b3301a4b3'>Commit.</a> </li>
<li>Apply patch #100422 to make implemented methods not pure virtual. <a href='http://commits.kde.org/libkipi/860f11a91c9398397246bf554e24ff9b8b3adc6d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366361'>#366361</a></li>
</ul>
<h3><a name='libkleo' href='https://cgit.kde.org/libkleo.git'>libkleo</a> <a href='#libkleo' onclick='toggle("ullibkleo", this)'>[Show]</a></h3>
<ul id='ullibkleo' style='display: none'>
<li>CMakeLists.txt: Remove unused variable. <a href='http://commits.kde.org/libkleo/b092fd56ca2d3c8e8d5b977668237024bb565b80'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129403'>#129403</a></li>
<li>Fix build with g++ 4.8.x. <a href='http://commits.kde.org/libkleo/8973c75c9427d2fe53227303a081951ecea39b9a'>Commit.</a> </li>
<li>KeyCache: fix crash when KeyCache dies while RefreshKeysJob is running. <a href='http://commits.kde.org/libkleo/e69f0b08cb8d907df868159e2d90341de5330406'>Commit.</a> </li>
<li>Fix a crash in stlutil. <a href='http://commits.kde.org/libkleo/75a00ec959d198f8d5de02ba5708b539f7d32a41'>Commit.</a> </li>
<li>Fix Messages.sh, subdir backengs was deleted. <a href='http://commits.kde.org/libkleo/15796386f562b0ea09a02a1cc0b6339c15854fe6'>Commit.</a> </li>
<li>Port to new connect() syntax where possible. <a href='http://commits.kde.org/libkleo/a0ccf4fc33ff90409ae182739fe3d819131574a7'>Commit.</a> </li>
<li>Port from cassert to Q_ASSERT. <a href='http://commits.kde.org/libkleo/b222e4591be27a8ba840ed985ffc473d9f54d580'>Commit.</a> </li>
<li>Port away from boost in public API. <a href='http://commits.kde.org/libkleo/4cca867c9d89beb484164ef6e9a67ac0abab4ba8'>Commit.</a> </li>
<li>Rebase on master and port additional classes. <a href='http://commits.kde.org/libkleo/9dac631ad7d1528d5cfc6faf5b0b06811d6ec717'>Commit.</a> </li>
<li>Use QGpgME protocol.h instead of qgpgmebackend.h. <a href='http://commits.kde.org/libkleo/009db62cab7164a1fadaee882abe90a5bef3518d'>Commit.</a> </li>
<li>API Break: Remove backends from libkleo. <a href='http://commits.kde.org/libkleo/6ac7ef85362ee76c3a4926fd4d93f6cde487e6f9'>Commit.</a> </li>
<li>Fix GnupgKeyParams syntax in DefaultKeyGenerationJob. <a href='http://commits.kde.org/libkleo/896cac636a3e3c4dad4f4d3cf5707621e3b352c7'>Commit.</a> </li>
<li>KeySelectionCombo: fix key pre-selection after applying filter. <a href='http://commits.kde.org/libkleo/956b7f064e7dac515c3cf440b13f6b5fce349f9e'>Commit.</a> </li>
<li>DefaultKeyGenerationJob: add API to specify custom passphrase. <a href='http://commits.kde.org/libkleo/7ad4d6dc2a0b3a3b0ea604f9ad2c8dd3a4191310'>Commit.</a> </li>
<li>Now we depend against 5.6.0. <a href='http://commits.kde.org/libkleo/10889c17e3058737545c08a14437e637a90e2657'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/libkleo/9da95afd712785e3aeb2fdd030a576846b9bc7ca'>Commit.</a> </li>
<li>Don't install files twice (part 2). <a href='http://commits.kde.org/libkleo/c1ccaa18073a0fb4ac61ac253a7696692ebd2a8a'>Commit.</a> </li>
<li>Don't install files twice. <a href='http://commits.kde.org/libkleo/2c0f3187ab5315022c7926083948a3532b87a133'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/libkleo/27f22c26176469b5ac0279d5b2e252e23bb513a0'>Commit.</a> </li>
<li>Fix compilation with GCC 4.8. <a href='http://commits.kde.org/libkleo/5e666673e372c92b3a51818153d61504602bd86c'>Commit.</a> </li>
<li>Tests/test_keylister.cpp - add overrides. <a href='http://commits.kde.org/libkleo/bc2b4d78df7d1b61469b1d3e8689dcee4da59a2d'>Commit.</a> </li>
<li>Models/keylistmodel.cpp -Wpedantic. <a href='http://commits.kde.org/libkleo/50dae068813bdfb9f19d149bec13f0eb525cfe94'>Commit.</a> </li>
<li>Fix default key selection for initalized keycache. <a href='http://commits.kde.org/libkleo/510c82fdac8024ff9acc84ad26ffba9219f8d227'>Commit.</a> </li>
<li>Introduce Kleo::DefaultKeyGenerationJob. <a href='http://commits.kde.org/libkleo/f49b7157e55a25da325eb4c35ac4134e20bdb2f1'>Commit.</a> </li>
<li>Add support for EncryptJobs with generic flags. <a href='http://commits.kde.org/libkleo/f93f14e544c8248612b91471600a99c6dc9bb3a5'>Commit.</a> </li>
<li>Import Classify utilities from Kleopatra. <a href='http://commits.kde.org/libkleo/a0fc9f8ca19718cc5d521538b91ad5b8a68d0b7c'>Commit.</a> </li>
<li>KeySelectionCombo: add id filter, manual key refreshing and UI feedback. <a href='http://commits.kde.org/libkleo/6bbd8b1059328f5396cfdb2f17cbc90f75c9c9f6'>Commit.</a> </li>
<li>Make sure all public classes are exported. <a href='http://commits.kde.org/libkleo/d6a71a4ecf0455f1c02e2749ad32b751d64794c1'>Commit.</a> </li>
<li>Handle existing keycache in keyselectioncombo. <a href='http://commits.kde.org/libkleo/bc2d2a986b131f483eaee5773609917abb998dac'>Commit.</a> </li>
<li>Silence unused variable warning. <a href='http://commits.kde.org/libkleo/a76396774f49d4bdbbe97ac5e374b0283b605e13'>Commit.</a> </li>
<li>Fix filtering for multiple uids again. <a href='http://commits.kde.org/libkleo/bdc575a44d9aa49bf226a05f9c890f6e9f3c365d'>Commit.</a> </li>
<li>Bump version for new KeySelectionCombo API. <a href='http://commits.kde.org/libkleo/bafe34e3c5aee70bc2f250435e65125d6e36fa0d'>Commit.</a> </li>
<li>Make keyFilter mechanism work again. <a href='http://commits.kde.org/libkleo/95d5051c1ef42c27c665369f48048e02a4c61f91'>Commit.</a> </li>
<li>Use full type signature in Q_DECLARE_METATYPE. <a href='http://commits.kde.org/libkleo/b3b0c02117307716a9db5c35bdcd92f9056b2a01'>Commit.</a> </li>
<li>KeySelectionCombo: don't show keyID. <a href='http://commits.kde.org/libkleo/ab1fe0f502604f9d59d705b09c4e8ad648f1891b'>Commit.</a> </li>
<li>Introduce KeySelectionCombo class. <a href='http://commits.kde.org/libkleo/ea628492350015675d8131c5347cdc4e1cf39d92'>Commit.</a> </li>
<li>Fix filtering for multiple uids. <a href='http://commits.kde.org/libkleo/69fa56358784f3793129dc8021cd7dc8f70668a9'>Commit.</a> </li>
<li>Fix Formatting::prettyEMail. <a href='http://commits.kde.org/libkleo/22fa0adc71b231f686b5e48104ef0c407304199e'>Commit.</a> </li>
<li>Import models and some utility classes from Kleopatra. <a href='http://commits.kde.org/libkleo/411863304ce47ea6fbb5a996eb215ac0fa7a42d1'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Show]</a></h3>
<ul id='ullibksieve' style='display: none'>
<li>Fix clazy warning. <a href='http://commits.kde.org/libksieve/0d0aafde899eecc25fe319e6caf478ad7502c549'>Commit.</a> </li>
<li>Enable with qt5.6 too. <a href='http://commits.kde.org/libksieve/48692e42d7fd105ec20d22c3ef078f9f491f085f'>Commit.</a> </li>
<li>USe qobject_cast. <a href='http://commits.kde.org/libksieve/80f88fd3026e10afa057be969db734486be6cd1b'>Commit.</a> </li>
<li>Fix open url in current webengineview. <a href='http://commits.kde.org/libksieve/babf02527b6af13a5ee6a41d18f2fdc67415fce8'>Commit.</a> </li>
<li>Add more help page. <a href='http://commits.kde.org/libksieve/0a5b1da3e5556da49de13c6d9f12d8129bd5b797'>Commit.</a> </li>
<li>Add help info about imapflags. <a href='http://commits.kde.org/libksieve/05f0147d9c53be2700eb91fb6f8cd6deb5633798'>Commit.</a> </li>
<li>Use pimcommon api. <a href='http://commits.kde.org/libksieve/1e305793ecc0fb7eeb90bb093445799df06fb731'>Commit.</a> </li>
<li>Use an unique qnetworkconfigurationmanager. <a href='http://commits.kde.org/libksieve/a0ada1fecc4cc60f83ead4c0134609c716654ec9'>Commit.</a> </li>
<li>Add an unique networkmanager in all sieve apps. <a href='http://commits.kde.org/libksieve/8e07e4bb83c560ca6c39e73c93075eb79111c0e1'>Commit.</a> </li>
<li>Increase version (need for sieveeditor). <a href='http://commits.kde.org/libksieve/825f8213566efd6fbe34fdb377a66bf12693c9d0'>Commit.</a> </li>
<li>USe new class. <a href='http://commits.kde.org/libksieve/f906bc46bf744fbb479a165b9b0f82f4897d708a'>Commit.</a> </li>
<li>Add autotest. <a href='http://commits.kde.org/libksieve/79d81ab74493ec7c3eea4b7fd5b1cb9d40587abe'>Commit.</a> </li>
<li>Extract check script method. <a href='http://commits.kde.org/libksieve/0557b7fd35111d725f82cdf8a4498c763ced1551'>Commit.</a> </li>
<li>Minor coding style. <a href='http://commits.kde.org/libksieve/3bf3931583fd6daf9610bec498ddfdde362da9bf'>Commit.</a> </li>
<li>Emit signal when renamed was a success. <a href='http://commits.kde.org/libksieve/631fa74628e42baae516643aff4903b99181c185'>Commit.</a> </li>
<li>Signal that we renamed old script. <a href='http://commits.kde.org/libksieve/882e57b8d0f16e3cbef061501a6be1c1e8649d5c'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/libksieve/f8bf62d67e2782512442fb2565ae74cc2e45bc28'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/libksieve/d9098e59b1638fd07878b3cc2bc4d05db63f48f3'>Commit.</a> </li>
<li>Use old filename here. <a href='http://commits.kde.org/libksieve/a4e94ee57cd823250114872838fd4d2bea8a76a9'>Commit.</a> </li>
<li>Avoid to rename as empty filename. <a href='http://commits.kde.org/libksieve/37c47d5d020316b1090a04412cf152940568e797'>Commit.</a> </li>
<li>Now we can return error. <a href='http://commits.kde.org/libksieve/d2b6045714588a008d3e37ab93b9fb65cee284ac'>Commit.</a> </li>
<li>Add separator. <a href='http://commits.kde.org/libksieve/125c4630a939ced16a4bd54ab8223783f867dcd6'>Commit.</a> </li>
<li>Clean up code. <a href='http://commits.kde.org/libksieve/c523b0d5e58f87776215aacb985ac2abfc885ffd'>Commit.</a> </li>
<li>Allow to rename script. <a href='http://commits.kde.org/libksieve/c53debe3a4e4d19dbf48a917d8b7da8851d6b261'>Commit.</a> </li>
<li>Add job to rename script. <a href='http://commits.kde.org/libksieve/c856b69e695d98e1a8b6e240be544b727fefb260'>Commit.</a> </li>
<li>Clean up CMakeLists.txt. <a href='http://commits.kde.org/libksieve/d4125576ef025af069ea703239606f45bf59c0f3'>Commit.</a> </li>
<li>We can activate new method. <a href='http://commits.kde.org/libksieve/4475a62794be62575f7ab21747d259af5632f43d'>Commit.</a> </li>
<li>Add method to renamescript + check script. <a href='http://commits.kde.org/libksieve/93c937e8313e87fe1f7538d4e891ed5b566b1068'>Commit.</a> </li>
<li>We have a checkscript command (look at to implement it). <a href='http://commits.kde.org/libksieve/ce1d6dbdd89d60b7afe03e32ff2f01f6d5006297'>Commit.</a> </li>
<li>Allow to rename file. <a href='http://commits.kde.org/libksieve/1acb0b96c42c62362328116dc9f698bef01bfa8e'>Commit.</a> </li>
<li>Add job for renaming script. <a href='http://commits.kde.org/libksieve/e9d264720e630e12df6f9bdc8be84c888224a2fd'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/libksieve/50b38658ce59f577b1f2c321b71ecd05ace7bc87'>Commit.</a> </li>
<li>Start to implement rename script. <a href='http://commits.kde.org/libksieve/c62dd4794741fb2c52b75e21a7b064ada9b616b9'>Commit.</a> </li>
<li>USe new connect api. <a href='http://commits.kde.org/libksieve/d9d83a1388028fe311487e6b8321368f342eaa73'>Commit.</a> </li>
<li>Fix parsing error when we have < or > in comment. <a href='http://commits.kde.org/libksieve/f046f24edb49e2c668fa6ce3bd5c27b639bad479'>Commit.</a> </li>
<li>Add failed script. <a href='http://commits.kde.org/libksieve/9655108a4524c394d73765dba12267f0afe88570'>Commit.</a> </li>
<li>Disable some features here too. <a href='http://commits.kde.org/libksieve/923fe09eb7aa0a46c3ac6a8de76c9c5316fbb930'>Commit.</a> </li>
<li>Fix managesieve using only tls1.0. <a href='http://commits.kde.org/libksieve/98c3d5326a510dfd5ddc10f2ff95b1bf690342e2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129029'>#129029</a></li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/libksieve/4f99a7b6dbd81dc9cfbbbd423ce5dae577e192ce'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/libksieve/660f9e8d541a731fdf285472c7d235bc04be218c'>Commit.</a> </li>
<li>Use new connect api. <a href='http://commits.kde.org/libksieve/223808ce7ced8d9f9805c90bfd76c7d0d7934f9b'>Commit.</a> </li>
<li>Fix warning. <a href='http://commits.kde.org/libksieve/8f1d9f8d4108943d0c3403fab373685ab3e602ba'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/libksieve/7f542bb3447a0c6f1ebe8d989dc08f56ab3efe47'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/libksieve/f8ea2f6c868862c9e125d0bda07e35c999395037'>Commit.</a> </li>
<li>Add parent to widget, remove duplicate line. <a href='http://commits.kde.org/libksieve/1317b9f77f8a1680fb334914e3a79cadce65c6f3'>Commit.</a> </li>
<li>Merge 2 lines. <a href='http://commits.kde.org/libksieve/e0b65c70fd461d2fd5b7c9f5f9547cead8c51d45'>Commit.</a> </li>
<li>Fix compile when deprecated Qt features are disabled. <a href='http://commits.kde.org/libksieve/cf68a15c3dccfc11d1fb0a4aeef13908706f55f8'>Commit.</a> </li>
<li>Fix compile: DocTools and KIO are required. <a href='http://commits.kde.org/libksieve/31a9940a617ccfc0cb922ea676ccd515dc5b64e0'>Commit.</a> </li>
<li>KApidox: do not redefine group_info. <a href='http://commits.kde.org/libksieve/b9842bb98d1e9be91366cb5f3e914c8b58bec1a5'>Commit.</a> </li>
<li>Fix button text. Allow to unable/disable button. <a href='http://commits.kde.org/libksieve/dbf0c0cfc46e373b66f6f52705d436b668a52b0f'>Commit.</a> </li>
<li>Add more info. <a href='http://commits.kde.org/libksieve/c778ba1cf25d6ecd0d3d1f37bb3ab9a30b8bbdcf'>Commit.</a> </li>
<li>It's a public lib. <a href='http://commits.kde.org/libksieve/7a6bbf4876f3fa8105885d63c1327a58bf5982cd'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/libksieve/6b9bd794d8c7c808041de3b78acd3a4661f1c4a4'>Commit.</a> </li>
<li>Don't create new kidentitymanager when not necessary. <a href='http://commits.kde.org/libksieve/2c355f0faacb6cb016edcbabba173875db6b04ac'>Commit.</a> </li>
<li>Remove more private Q_SLOTS. <a href='http://commits.kde.org/libksieve/c06a7e3a6d5003615befb65977e231eb62685247'>Commit.</a> </li>
<li>Private Q_SLOTS is not necessary. <a href='http://commits.kde.org/libksieve/5cae5d79c417ee46ad114685688aa4b3c94e23df'>Commit.</a> </li>
<li>Add metainfo. <a href='http://commits.kde.org/libksieve/874d66a462b9098618ffe56ab7244fbb626544be'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Show]</a></h3>
<ul id='ullokalize' style='display: none'>
<li>CCBUG: 368842. <a href='http://commits.kde.org/lokalize/bf393c36847f0198668953da97831872433ab04a'>Commit.</a> See bug <a href='https://bugs.kde.org/368842'>#368842</a></li>
<li>Appdata: use the screenshot from www.kde.org. <a href='http://commits.kde.org/lokalize/feb56a609593ad4aa57b198ba9881f9f8eb9fd44'>Commit.</a> </li>
<li>Rename the appdata file (namespace) and install it. <a href='http://commits.kde.org/lokalize/0334d5338979d27152a2985afa1491fb5ed7602f'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Show]</a></h3>
<ul id='ulmailcommon' style='display: none'>
<li>Fix some clazy warning. <a href='http://commits.kde.org/mailcommon/7d95a5ba45da8d82b2a52bb7919240787acf6c8c'>Commit.</a> </li>
<li>Use qobject_cast. <a href='http://commits.kde.org/mailcommon/255473f20374e5c510c06f8250c6d22427b98d09'>Commit.</a> </li>
<li>Convert to std::shared_ptr. <a href='http://commits.kde.org/mailcommon/48d76cf72d27b8b4f3344aad8d14ce38ba82a09d'>Commit.</a> </li>
<li>Clean up api. <a href='http://commits.kde.org/mailcommon/5db62cbb0d7ec853eda13ef7847e4c3ec4d289e6'>Commit.</a> </li>
<li>Rename method. <a href='http://commits.kde.org/mailcommon/02077a93e2be1257cc11ff15921ac341f9f7bff7'>Commit.</a> </li>
<li>Add method to get resource pop3 collection target. <a href='http://commits.kde.org/mailcommon/973ac0e85921268fa367adbf42f50a4eb4e64eb1'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/mailcommon/0228ac5a9ceb93419d62cf42cd70d5c45ed2d314'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/mailcommon/984cfcac26054c91e2a5418d0be95ff8ac0c77dc'>Commit.</a> </li>
<li>Not necessary to use this settings now. <a href='http://commits.kde.org/mailcommon/6130edd2e6f83f5cfe2c68a1f063937c1dae95ec'>Commit.</a> </li>
<li>We depend against 5.6.0. <a href='http://commits.kde.org/mailcommon/ef0b7b0aea08bb99acc5626ebffb486172b57855'>Commit.</a> </li>
<li>Remove unused includes. <a href='http://commits.kde.org/mailcommon/e9d0adb0eb28df6180ba0fa4d8c50b94b87799cb'>Commit.</a> </li>
<li>Remove settings. <a href='http://commits.kde.org/mailcommon/83756a68b1db8b174a4cdaa601820f04182b7118'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/mailcommon/c0112f7762ceeb101d3d1bb773830ea173cabc0c'>Commit.</a> </li>
<li>Modernize Akonadi unittestenv. <a href='http://commits.kde.org/mailcommon/83bb73f7d3ff312a6a03548052aa7ffd269282f0'>Commit.</a> </li>
<li>Use setFlat. <a href='http://commits.kde.org/mailcommon/05a60bd16563b23b94dddc34f1f17957e8be9357'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/mailcommon/c5407a28e50e03ae92a745f6bdde1c9b394169b9'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/mailcommon/23d750f8ca5eb9b99ea4cf82680ea724bb8184b7'>Commit.</a> </li>
<li>Prevent double delete of FolderSelectionDialog. <a href='http://commits.kde.org/mailcommon/f0e19f4b6b4752c27ac2c9b5fd850b5349b63755'>Commit.</a> </li>
<li>Add setObjectName. <a href='http://commits.kde.org/mailcommon/7d3cf093bd451d77ac6ccd57f39870b48a8a778f'>Commit.</a> </li>
<li>Allow to remove account identifier. <a href='http://commits.kde.org/mailcommon/85ab78ead32750eb790d5be9066d486015d975a7'>Commit.</a> </li>
<li>Fix compile when deprecated Qt features are disabled. <a href='http://commits.kde.org/mailcommon/01b61c2fb7b6538b0a9c3cb256cf23a451afde43'>Commit.</a> </li>
<li>Fix compile when Qt keywords are disabled. <a href='http://commits.kde.org/mailcommon/f06ee7b7fc30a3ceb0564a4f7a8d12258e1610fe'>Commit.</a> </li>
<li>Fix compile: DBusAddons is linked against. <a href='http://commits.kde.org/mailcommon/02cf3eb08272cf3e96f2dabedc814737af4a67ec'>Commit.</a> </li>
<li>Remove not necessary private Q_SLOTS. <a href='http://commits.kde.org/mailcommon/116109e09834d0ffa3de90df460f8d801ad4608f'>Commit.</a> </li>
<li>Add meta info file. <a href='http://commits.kde.org/mailcommon/a2160ef3ce11df8b2da79ef227b70ddde097bcea'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/mailcommon/f24ba25fdcae43c0c049fa69a5b0d6fc44204a78'>Commit.</a> </li>
</ul>
<h3><a name='mailimporter' href='https://cgit.kde.org/mailimporter.git'>mailimporter</a> <a href='#mailimporter' onclick='toggle("ulmailimporter", this)'>[Show]</a></h3>
<ul id='ulmailimporter' style='display: none'>
<li>Fix categories. <a href='http://commits.kde.org/mailimporter/92e6083a27533d1d4070d527a2d26bdf3a258188'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/mailimporter/b6ad71b2115cdf6fc71eb26b61af2428e41ede27'>Commit.</a> </li>
<li>Don't install files twice. <a href='http://commits.kde.org/mailimporter/fb21eabf247f9e25df0f7f19960080142f5f3459'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/mailimporter/824c203fa14370f7c07a5a428d7996656c634996'>Commit.</a> </li>
<li>Fix layout. Remove duplicate line. <a href='http://commits.kde.org/mailimporter/f6e51af8a083a4c91530f5a096f2571f9b07944a'>Commit.</a> </li>
<li>Add metafile. <a href='http://commits.kde.org/mailimporter/19536f0934f73d492fa4b6e22936816c5cafeb1f'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Need Q_OBJECT in classes that call tr(). <a href='http://commits.kde.org/marble/d38b1d196a064e1c32068f5c42edf62b0bb40637'>Commit.</a> </li>
<li>Add qmake pro file to example hello-marble. <a href='http://commits.kde.org/marble/7fdcdc764278cc0af8882f5519063e7f133c7a95'>Commit.</a> </li>
<li>Don't enable SENTINEL2_MAP per default, not ready for broader consumption. <a href='http://commits.kde.org/marble/8a749e70ecf4c16ef5fb3b526965207fbcb46e98'>Commit.</a> </li>
<li>Remove dep on Qt5::Location, not used. <a href='http://commits.kde.org/marble/670a24a87adab6d7b76de60ce67c8eef8433c24c'>Commit.</a> </li>
<li>Version bump to 0.26.0 (stable release). <a href='http://commits.kde.org/marble/ba0306cfe35dc4adb30d98e1a7e4cfde9d3b3d74'>Commit.</a> </li>
<li>Center on placemark when tapping on its coordinates. <a href='http://commits.kde.org/marble/06c3888c81b722634e08503aed1d6eaa0b7035c2'>Commit.</a> </li>
<li>Graciously add http:// if a website tag lacks it. <a href='http://commits.kde.org/marble/7164dc98cfd39387fb6c17b8a4a171bfdfb84bd5'>Commit.</a> </li>
<li>Increase level of detail in levels < 10. <a href='http://commits.kde.org/marble/ef9310e726e28b730546911ccb55e9611b5a05b6'>Commit.</a> </li>
<li>Keep category and visibility info. Remove wrong check. <a href='http://commits.kde.org/marble/84026e1b2c64e018ebc452bcb1bd08b53ae36a55'>Commit.</a> </li>
<li>Install a pri file, so qmake-based projects just need: QT += Marble. <a href='http://commits.kde.org/marble/312ae71d42e534cd4a5a36619b12c0b7f03b3adc'>Commit.</a> </li>
<li>Sane defaults, less debug output. Add zoom/tile levels option. <a href='http://commits.kde.org/marble/cf096489feaea2a975157e7989d844a9296fab56'>Commit.</a> </li>
<li>Do not write tags that are not used for rendering or elsewhere. <a href='http://commits.kde.org/marble/ab58a6293e2929d64f97aaf857e5c6f6183ebbc1'>Commit.</a> </li>
<li>Need for LD_LIBRARY_PATH no longer an issue. <a href='http://commits.kde.org/marble/5de15d9100ee87e034cd6797567bbc01e684b227'>Commit.</a> </li>
<li>Only consider valid osm ids. <a href='http://commits.kde.org/marble/49e66c96c371e46e4b9fec203a30dd0bc3c8bc08'>Commit.</a> </li>
<li>Filter polygon holes by their size as well. <a href='http://commits.kde.org/marble/32fda9a929bc839515047a1efde9a9260f1f50e9'>Commit.</a> </li>
<li>Set RPATH to custom dirs of own and foreign libs; no linking to build dir. <a href='http://commits.kde.org/marble/c816a2bce3c6f748f0f546b6398a1485337a3245'>Commit.</a> </li>
<li>Ignore kde-style translation system folders. <a href='http://commits.kde.org/marble/68d303e2e37cdc2393187b9a34f5c0e8f1a02216'>Commit.</a> </li>
<li>Remove outdated tools/qt-l10n/marble-l10n.sh. <a href='http://commits.kde.org/marble/f02dcf661d1c14916df00f15d5c0638e81bebfc6'>Commit.</a> </li>
<li>When e.g. calling createWayStyle then NEVER do this:. <a href='http://commits.kde.org/marble/6c4dc2ff09e95223e5adf5df1383956b6bc43794'>Commit.</a> </li>
<li>Remove write-only, unitialized variable. <a href='http://commits.kde.org/marble/0c338b2036f9174e400ba3ba5c09250251c0269a'>Commit.</a> </li>
<li>Minor tweaks to the position marker. <a href='http://commits.kde.org/marble/34b666e1b756a4b55a71c649207fd1705fe23192'>Commit.</a> </li>
<li>Display a circle instead of an arrow for the position marker is speed. <a href='http://commits.kde.org/marble/f1a7db2fa7e635bba9a6517fede36c545d689c72'>Commit.</a> </li>
<li>Bump Android version code. <a href='http://commits.kde.org/marble/6e181e2dea6a06f395469011bc8d86abde8647ed'>Commit.</a> </li>
<li>Tame dialog height for now. Needs a proper solution though. <a href='http://commits.kde.org/marble/297087626788faa2130c69fa3072ab9d6a6d1fc8'>Commit.</a> </li>
<li>- Clean up position marker code. <a href='http://commits.kde.org/marble/859d12735508336f47e277a26f308ac7e92cec4c'>Commit.</a> </li>
<li>Provide more space for bookmark entries. <a href='http://commits.kde.org/marble/53ee4580a24229d5e4f3728804338304fe946615'>Commit.</a> </li>
<li>Patch across tiles using original osm id. <a href='http://commits.kde.org/marble/1a713c5c883ef20882a557ccd570706d84914f5b'>Commit.</a> </li>
<li>Provide a oid() method to return the original osm id, if present. <a href='http://commits.kde.org/marble/994f283eb593724b9e6b1b43f7b7f983ca198f69'>Commit.</a> </li>
<li>Use mx:oid tags if available. <a href='http://commits.kde.org/marble/c188ca3b4cbf17488560921e3cec9b27e528c0c6'>Commit.</a> </li>
<li>Simplify. <a href='http://commits.kde.org/marble/d9b3ee197f479316e3863fe2dc30bdb421051790'>Commit.</a> </li>
<li>Simplify less. <a href='http://commits.kde.org/marble/660c528cb5683506107fb129ab958964b270539a'>Commit.</a> </li>
<li>Use more details in level 3/4. <a href='http://commits.kde.org/marble/40c0e937df2c75b9ca9651e6026ffe1483cf071e'>Commit.</a> </li>
<li>Assign to the correct osm placemark data. Do not re-use original ids. <a href='http://commits.kde.org/marble/e2ed541fe4fe43cb5b7976534e3f353df759dfe2'>Commit.</a> </li>
<li>Avoid overflows (?) for large (?) values. Fixes date line appearance. <a href='http://commits.kde.org/marble/9d845f69cdd8cdfde9d4cb0a0aa469aa5651d733'>Commit.</a> </li>
<li>Align D/P epsilon between low and med/high tiles. <a href='http://commits.kde.org/marble/e2c59ba1c5f08921d8115b1424064072eb72adeb'>Commit.</a> </li>
<li>Simplify, generalize and improve performance. <a href='http://commits.kde.org/marble/ae56edcc0f27c22cc7c73cb7195335c650571f29'>Commit.</a> </li>
<li>Fix inner ring indices and containment of multipolygons. <a href='http://commits.kde.org/marble/fe4bc8d26ce3d9bfd07a8aa6d73c3d91934678a4'>Commit.</a> </li>
<li>Correct nat earth city names at level 7/9 with geonames database. <a href='http://commits.kde.org/marble/850e124bf85f9c568df6d5cca5bb6f27c4fd7371'>Commit.</a> </li>
<li>ConfigurePainter refactoring:. <a href='http://commits.kde.org/marble/9e11b578f18ea3d905e26d7c2d2ec68563f69e44'>Commit.</a> </li>
<li>Stars and coordinate grid in low zoom, adjust colors. F-up to 0db4ede. <a href='http://commits.kde.org/marble/ef5c0ba82c514798b3040dd22d662400d468ba31'>Commit.</a> </li>
<li>Show labels on top of graticule. Follow-up to 0db4ede. <a href='http://commits.kde.org/marble/0b03928c7a6dd17862389261efd377a79d02018d'>Commit.</a> </li>
<li>Fix typo. <a href='http://commits.kde.org/marble/7d611dd03fb777e7568f7bf5678c7610c915cd58'>Commit.</a> </li>
<li>Save some space by not installing unused files (part of tile data now). <a href='http://commits.kde.org/marble/5408668d9730f1ef41da08117ba12611f019e226'>Commit.</a> </li>
<li>Ship OSRM with Android again. <a href='http://commits.kde.org/marble/1f27b564443affb0d124336ce40518308845f065'>Commit.</a> </li>
<li>Fix average calculation for n > 1 :-P. <a href='http://commits.kde.org/marble/acd2d10b552f393578766cbe02013073ab2074bf'>Commit.</a> </li>
<li>Adjust OSRM plugin to new API. Turn instructions tbd. <a href='http://commits.kde.org/marble/926082c918cc2a0824c550efb2d1d157e9f9a3f5'>Commit.</a> </li>
<li>Take an average of the currently available screen sizes into. <a href='http://commits.kde.org/marble/a6f946ed2ea3652213afa1586e81834623ec550b'>Commit.</a> </li>
<li>Condense elevation information into placemark description of peaks. <a href='http://commits.kde.org/marble/1258fe101cef289b4d6b45517f27151161d00839'>Commit.</a> </li>
<li>Reduce variable scope. <a href='http://commits.kde.org/marble/3490be25a1be8465e4aa0fea8f0cef3b3850ed52'>Commit.</a> </li>
<li>Take screen size into account when adjusting tile zoom level. <a href='http://commits.kde.org/marble/0621ff5286d3e511fb9d630b9576eb04c0783c95'>Commit.</a> </li>
<li>Add a mid() method just as QVector provides. <a href='http://commits.kde.org/marble/45f9db6dcdc7a3a9415eb0d643ae4ebbf96cd123'>Commit.</a> </li>
<li>Drop explicit last node in a ring if it is a duplicate one. <a href='http://commits.kde.org/marble/fd535e6b3e11be92f9ca88fc12a3346a34a45af3'>Commit.</a> </li>
<li>Insert a tag with a list of clip point indices (points on tile border). <a href='http://commits.kde.org/marble/1426a625e23c6e103b4700d7d5617fb1f8c999cb'>Commit.</a> </li>
<li>Minor fixes in the Marble Maps UI qml code. <a href='http://commits.kde.org/marble/841628cb8873c2e7033e26c692d3bd4efe8cd848'>Commit.</a> </li>
<li>Refactoring of the Routing element:. <a href='http://commits.kde.org/marble/9d90adc92799eb12ade6280b7ec65caf7ba01cd1'>Commit.</a> </li>
<li>This commit brings performance on my tablet from 3fps to about. <a href='http://commits.kde.org/marble/e2091b15cebe0d7de5e4d1f8df25ef1c9112d58a'>Commit.</a> </li>
<li>Remove unused includes. <a href='http://commits.kde.org/marble/1dbc21b5f27ce88898b264ebd804f4793cc91a51'>Commit.</a> </li>
<li>Give swimming pool polygons a place in painting order. <a href='http://commits.kde.org/marble/b658c69cb6ac6facec27da447d68d81811cffae2'>Commit.</a> </li>
<li>No more need for nodeType() in private class of GeoDataFeature & subclasses. <a href='http://commits.kde.org/marble/a874f22f3362c8185f18b1d7666ebc734bfcfa8b'>Commit.</a> </li>
<li>Paint TransportPlatform linestrings at similar level as polygons. <a href='http://commits.kde.org/marble/3ae26b99d5ff23507da046d4cf9bcaf3cee35222'>Commit.</a> </li>
<li>Paint ManmadePier below streets/ways and buildings. <a href='http://commits.kde.org/marble/e259aefef084b9f4fa379eef5acb538fbf16a2c1'>Commit.</a> </li>
<li>Remove unused include. <a href='http://commits.kde.org/marble/30fab9070128e5847f6a33f2d91017a9b7d740f2'>Commit.</a> </li>
<li>Remove implicit sharing from GeoDataFeature & subclasses. <a href='http://commits.kde.org/marble/7bcd0676d2853375d630c8e42df35458923c4a5b'>Commit.</a> </li>
<li>Fix assert logic. <a href='http://commits.kde.org/marble/426b984468b6e5514a1180e75ad1d06a54cfb52f'>Commit.</a> </li>
<li>Re-add qDeleteAll + clear to get proper rendering again. <a href='http://commits.kde.org/marble/73b95db97f7abdfb05bca2aea6c089d3e4009963'>Commit.</a> </li>
<li>Code style fixes: Curly braces also for oneliners. <a href='http://commits.kde.org/marble/f9262d857f9986dcc46550e870f78b23a735634f'>Commit.</a> </li>
<li>DPI aware switch between fake 3D modes. <a href='http://commits.kde.org/marble/207765fd428ec313bd2a5a87eaca57f5adaf9cc9'>Commit.</a> </li>
<li>Minor building rendering optimization:. <a href='http://commits.kde.org/marble/4f89f8ea3c72354e3814a36a1076d62f3ef982f5'>Commit.</a> </li>
<li>Remove superfluous parameter. <a href='http://commits.kde.org/marble/73dcb3cb8cfc3bb2341aa7fc57178866b7ff0036'>Commit.</a> </li>
<li>Don't attempt to draw empty labels. <a href='http://commits.kde.org/marble/80c3aa76823727b1a48307064889830b2a85fb59'>Commit.</a> </li>
<li>Ensure matching calls of qDeleteAll/clear. Code style fixes. <a href='http://commits.kde.org/marble/bc1ce46fdabb0b30459cb1f7cda9fe225fa798ab'>Commit.</a> </li>
<li>Add developer options for inertial rotation and batch debug rendering. <a href='http://commits.kde.org/marble/b58558ccd379258d3259e16cfe8fe3fe80fbaf66'>Commit.</a> </li>
<li>More efficient street rendering:. <a href='http://commits.kde.org/marble/620499fdb830ced95e0ab3b4b8f635aa59db13d6'>Commit.</a> </li>
<li>Add batch rendering visualization for the desktop:. <a href='http://commits.kde.org/marble/070c60b5d9901d61a1758459b51095e5355da113'>Commit.</a> </li>
<li>Preliminary rough fix for the graticule:. <a href='http://commits.kde.org/marble/0db4edeb1116464bec680b33d68cac091ed27cf4'>Commit.</a> </li>
<li>Align fonts between polygon and placemark debug drawing. <a href='http://commits.kde.org/marble/84f6ab0ef5f72b1bcca8bf2fbed61850609aa20d'>Commit.</a> </li>
<li>Keep the color for displaying batch rendering of placemarks constant. <a href='http://commits.kde.org/marble/83f32cd936f30ff23ccdfa2863ea5fbbcb070bd1'>Commit.</a> </li>
<li>Adapt number of tile border support points to tile level. <a href='http://commits.kde.org/marble/867ec8cb24a7d57daed41e73949c5441ead5e64d'>Commit.</a> </li>
<li>Fix newly downloaded vector tiles being loaded and rendered twice. Doh. <a href='http://commits.kde.org/marble/0c5a33442693a9fa2e175fc6e6502821da0ec6cb'>Commit.</a> </li>
<li>Initial zoom level needs no cache clear. <a href='http://commits.kde.org/marble/31cc33fad5313e64565623e417c869f91d855312'>Commit.</a> </li>
<li>Batch Rendering for placemark symbol pixmaps:. <a href='http://commits.kde.org/marble/0d3aedbb8a37a40d64ec158823604bbd0e0ace36'>Commit.</a> </li>
<li>No need for explicit mention of GeoDataGeometry class when calling detach(). <a href='http://commits.kde.org/marble/76edbd38835e4a81cf55cdaa628374a9c439a300'>Commit.</a> </li>
<li>Use consistently GEODATA_EXPORT in geodata/data. <a href='http://commits.kde.org/marble/0e8a7b92d5dc51e9bc7a9b11ddd451c22470cd30'>Commit.</a> </li>
<li>Keep API dox and actual class together. <a href='http://commits.kde.org/marble/322f9c816ae3eb9f6b0fcc4ebd1fce569174f729'>Commit.</a> </li>
<li>Remove done FIXME. <a href='http://commits.kde.org/marble/ae4350c63f26624247f96d99218ee4afd68c2841'>Commit.</a> </li>
<li>Add some empty API dox to GeoData classes, so doxygen picks them up. <a href='http://commits.kde.org/marble/61057316917a3a1e8c13c8f46a6d3d656b0b40bc'>Commit.</a> </li>
<li>Keep more node details for strings on level 17. <a href='http://commits.kde.org/marble/f1ed206c80c4928d0f88421431bd9e0287046513'>Commit.</a> </li>
<li>Merge line strings with equal osm ids that are split across tiles. <a href='http://commits.kde.org/marble/3e8d79cd9fa5e15678fb0b281fd306b704f60b76'>Commit.</a> </li>
<li>Bump Android version code. <a href='http://commits.kde.org/marble/a094ca0764777fb52f51740934f924ae82a113f2'>Commit.</a> </li>
<li>No need for a protected member. <a href='http://commits.kde.org/marble/312134352779c17cff5dfcaddc5fd9da33462733'>Commit.</a> </li>
<li>Remove osm id sharing logic: Needs a rewrite for the new clip logic. <a href='http://commits.kde.org/marble/7f16581074ea8c90dd2ada585cb66e1aef777e79'>Commit.</a> </li>
<li>Keep osm ids of ways and areas. <a href='http://commits.kde.org/marble/1c51fa07f4845b3b0a9adee30fde78499aa6ee33'>Commit.</a> </li>
<li>Prevent empty runtime trace items. <a href='http://commits.kde.org/marble/a37f54add5c53e09afd0d3ff11cc492f56f99f83'>Commit.</a> </li>
<li>Two columns. Remove bitmap tiles option, split debug painting options. <a href='http://commits.kde.org/marble/2d12a652a3774f7bbe5228d8f1977a7bcb7bf994'>Commit.</a> </li>
<li>Fix tab order of elements in Stars config dialog. <a href='http://commits.kde.org/marble/0ecf28af090264f9747a8bccbc9ff16c8e19172f'>Commit.</a> </li>
<li>Remove explicite duplicate friend class declaration, now done by Q_DECLARE_PRIVATE. <a href='http://commits.kde.org/marble/9883c55506f61b2f8a9344ed519b4ef5b3c0a79c'>Commit.</a> </li>
<li>Use a consistent default zoom value. <a href='http://commits.kde.org/marble/4df750adb0a170df607613be5f258c77ef5b0d35'>Commit.</a> </li>
<li>Typo fixes. <a href='http://commits.kde.org/marble/c29df33b130e65cafcb11f116b258b7113b3619b'>Commit.</a> </li>
<li>Add notes what to do when updating/adding/removing bundle translations. <a href='http://commits.kde.org/marble/b6c27d388d3017de34194949cbd1952d7956b738'>Commit.</a> </li>
<li>Require Qt 5.7.0 for Android builds (Maps uses QuickControls2). <a href='http://commits.kde.org/marble/ec882377d66b4805b89af743f735afdffa464b69'>Commit.</a> </li>
<li>Use COMPONENTS when searching for Qt5 modules. <a href='http://commits.kde.org/marble/d059a6e31083568b464f5b0f80eede1e03e082a3'>Commit.</a> </li>
<li>Remove redundant find_package/cmake_minimum_required calls in tools. <a href='http://commits.kde.org/marble/3fd4c294e67694d7a522a46305920325ad0de6d8'>Commit.</a> </li>
<li>Let the marbleZoomLevel tag have the last word about the zoom level of an OSM node. <a href='http://commits.kde.org/marble/f76b677d4bec06bbeec95b7bf122f146008d1af3'>Commit.</a> </li>
<li>Establish invariant: set a popularity in any case also for OSM nodes. <a href='http://commits.kde.org/marble/8483e5f322babb7f7f4a0eb723c3a5de76a02eb1'>Commit.</a> </li>
<li>Whitelist settlement categories that have no icon. <a href='http://commits.kde.org/marble/c65a2f35314f9f81658c121cd0713cff79f858bb'>Commit.</a> </li>
<li>Preserve more details. <a href='http://commits.kde.org/marble/e6b0b00ae8c7c9d8c413b89e8a7a4528d2cc2d80'>Commit.</a> </li>
<li>Condense placemark dialog information. Add recycling amenity details. <a href='http://commits.kde.org/marble/872536f1c1129693877573f4ccb5e705913555b4'>Commit.</a> </li>
<li>Let placemark layout render the name (but not the house name or number). <a href='http://commits.kde.org/marble/b5dc06f15a453c6bbc264e04f7c148625a83a8a8'>Commit.</a> </li>
<li>Duplicate default min zoom levels to StyleBuilder. <a href='http://commits.kde.org/marble/e908fbc82c2f7a36309c29927e7b142049e5041b'>Commit.</a> </li>
<li>Reject non-render placemarks even earlier. <a href='http://commits.kde.org/marble/52e9fbd1e82f72a83c5aa735638fac5882af69c2'>Commit.</a> </li>
<li>Pass a reference rather than a pointer. <a href='http://commits.kde.org/marble/429811c2d14ec85ef42f91ec834ac738279e4cf5'>Commit.</a> </li>
<li>Remove unused method. <a href='http://commits.kde.org/marble/2f1b6b940b4a1f4f861825824d888102159f1b32'>Commit.</a> </li>
<li>No need for a dynamic style to always hide the label. <a href='http://commits.kde.org/marble/bbd333570bf9e3b5962fe24f7dfd477f7b532d79'>Commit.</a> </li>
<li>Provide a render preference for osm placemarks. <a href='http://commits.kde.org/marble/72b2815f5d8b8c895d178ee27d0596e9c4edd7df'>Commit.</a> </li>
<li>Draw zoom level and popularity in debug mode. Both are sort key. <a href='http://commits.kde.org/marble/0d28f0098791d922c4ca8a68cbaec48b9be41eed'>Commit.</a> </li>
<li>Do not create lots of placemarks that will never be rendered. <a href='http://commits.kde.org/marble/e94d8fa7d327e1d48d9a1d409247845787f673a4'>Commit.</a> </li>
<li>Extract hospitals on level 13 already. <a href='http://commits.kde.org/marble/acf8a3e8f43708b748622c9ed40b9cf296f90058'>Commit.</a> </li>
<li>Improve visible placemark caching. Lazy label pixmap creation. <a href='http://commits.kde.org/marble/09ecae215464886d7cc5896ac263227013b830f8'>Commit.</a> </li>
<li>Add a comment that loss of precision is intended. <a href='http://commits.kde.org/marble/63dc14328a38e901ae4d86af3babbde5c1c78415'>Commit.</a> </li>
<li>Avoid flickering: Use a floating point position for collision detection. <a href='http://commits.kde.org/marble/94a289443f792285cf5f8448d7bb19ce047b976d'>Commit.</a> </li>
<li>Add a debug mode for placemark layer. <a href='http://commits.kde.org/marble/934dfd760ce613d3cdabe78b944f70982f1c98a9'>Commit.</a> </li>
<li>Treat icons as part of the placemark used for collision detection. <a href='http://commits.kde.org/marble/8955569d0b65bd63bc367c31a8f5d97b2f0a8471'>Commit.</a> </li>
<li>Follow-up to 461dc11: Icons are rendered by PlacemarkLayer. <a href='http://commits.kde.org/marble/7de7cf170957ee9836f8361d66b64e00c15e24f1'>Commit.</a> </li>
<li>Minor low level optimizations. <a href='http://commits.kde.org/marble/b1b8f0fd691abb789d8464202049de1ac9c85748'>Commit.</a> </li>
<li>Avoid repeated svg loading. <a href='http://commits.kde.org/marble/fd19557c891f1867797c81d8e1483078156d0ef7'>Commit.</a> </li>
<li>Improve render speed for tile level < 17. <a href='http://commits.kde.org/marble/973fa9918fd815d9aa1c52fbd552609918e9f931'>Commit.</a> </li>
<li>Avoid intersects() call when the result can be determined easier. <a href='http://commits.kde.org/marble/53a9f2b61c0569a5a8baa3d1a737251d6ed4d725'>Commit.</a> </li>
<li>Revert "Do not create additional placemarks when converting file formats". <a href='http://commits.kde.org/marble/116108b7c33e6b5c500ce230bae1408b3104db63'>Commit.</a> </li>
<li>Reduce traffic light icon size. <a href='http://commits.kde.org/marble/b574746e74eda52dd6c52da20d58cdb7702aa01f'>Commit.</a> </li>
<li>Tweak tag extraction for level 11. <a href='http://commits.kde.org/marble/4113bb2977f823000aa3f95f57ebf0f424b9d273'>Commit.</a> </li>
<li>Cluster mountain peaks at level 11 and only show the highest. <a href='http://commits.kde.org/marble/c22ac0db3cdbe5ecfac5694ce484f0c5deb96429'>Commit.</a> </li>
<li>Parse marbleZoomLevel=<int> osm tags as placemark zoomLevel values. <a href='http://commits.kde.org/marble/c22ed98e26ef126f456672bad9706916e1a39f61'>Commit.</a> </li>
<li>Extract altitude for all nodes from ele tag. <a href='http://commits.kde.org/marble/2d0441f947d58223d023a46a0d32336fd179c349'>Commit.</a> </li>
<li>Use smart pointers to fix memory leaks. <a href='http://commits.kde.org/marble/3908eedf274fd9250c9fb142f7dfebd20662833d'>Commit.</a> </li>
<li>Use Douglas-Peucker algorithm for line/area simplification. <a href='http://commits.kde.org/marble/78b8313d55355066396b0688130a0bc0db7811f9'>Commit.</a> </li>
<li>Don't crash on (invalid, but still possible) empty ways. <a href='http://commits.kde.org/marble/10611fac637ce56c6b9e35eb5c0fb7a1937db39f'>Commit.</a> </li>
<li>Only consider ways with non-local (positive) IDs. <a href='http://commits.kde.org/marble/433547895e131acd7e6b5b113343bd92660a1620'>Commit.</a> </li>
<li>Have polygon and linear ring icons rendered by PlacemarkLayer. <a href='http://commits.kde.org/marble/461dc1122bebbf5d7e5a84b4d03764e1eb1e6b75'>Commit.</a> </li>
<li>Merge rivers as well. <a href='http://commits.kde.org/marble/9350556f36a600ca3f5a6feec90cf96d5cfda307'>Commit.</a> </li>
<li>Don't show streams in levels 11 and 13. <a href='http://commits.kde.org/marble/aeabc117d1214c67a2fa6376a1bcca15d31fc294'>Commit.</a> </li>
<li>Add translation, remove unused code. <a href='http://commits.kde.org/marble/4cda537568057361eb55e164419f6973348bd1d3'>Commit.</a> </li>
<li>Further level tweak: Default back to 18, bench and waste basket to 19. <a href='http://commits.kde.org/marble/de1ea4c57fad4bbe06e895cff9c74e2ea6cb97f4'>Commit.</a> </li>
<li>Fix build with Qt 5.5. <a href='http://commits.kde.org/marble/3a572a24d3947e0307249148dd12b143929845a4'>Commit.</a> </li>
<li>Integrate way merging into the vector tile creation toolchain. <a href='http://commits.kde.org/marble/8ab1bc70a0adcc7822d86fdce0c9d92247432358'>Commit.</a> </li>
<li>Keep node meta data (id and tags) during clipping. <a href='http://commits.kde.org/marble/edc02a8eb9d49eada1f85b9039777e768b5ddd21'>Commit.</a> </li>
<li>Fix tile creation with extensions other than o5m. <a href='http://commits.kde.org/marble/dc93df25478d0b61f8d9d7d82be26e8181f809ab'>Commit.</a> </li>
<li>Commit 3b9aa31 was intended to reveal peaks; whitelist them instead. <a href='http://commits.kde.org/marble/6e53a7877daffdbeca077b6df8644e2781ff882b'>Commit.</a> </li>
<li>Do not truncate large id differences. <a href='http://commits.kde.org/marble/84ec936926710c305a9c3d86e12dd01e51a64454'>Commit.</a> </li>
<li>Avoid copy. <a href='http://commits.kde.org/marble/462f1abe85ee18c4db50e12075e9ff47f8d801bd'>Commit.</a> </li>
<li>Set the QQuickPaintedItem opaque and don't render the application. <a href='http://commits.kde.org/marble/75d5925bd995ddb9f4539b7af60235a6b1f99ae5'>Commit.</a> </li>
<li>Change the render target for Marble's QQuickPaintedItem to FBO. <a href='http://commits.kde.org/marble/3b54783554040eec8df01508ec965dce60f9e6fd'>Commit.</a> </li>
<li>Filter small areas (less than 1 percent of tile area) on levels 11-15. <a href='http://commits.kde.org/marble/c34a13f8f7c050fc2254b7fde0e51b64b7465bd5'>Commit.</a> </li>
<li>Use placemark zoom level to restrict area icon visibility. <a href='http://commits.kde.org/marble/0979657e38f8473c8abf0c32b29db23ecdf62e71'>Commit.</a> </li>
<li>Do not render a random collection of placemarks from polygons. <a href='http://commits.kde.org/marble/47ea631f263a326c115f0289643fb111e1caa734'>Commit.</a> </li>
<li>Reduce station icon size. <a href='http://commits.kde.org/marble/9064488612b27aee6e80ed6efd2c2345d090a47a'>Commit.</a> </li>
<li>Extract peaks in level 11 as well. <a href='http://commits.kde.org/marble/1285ad1a6e6fd9bfd822a81d0001ac529f0517eb'>Commit.</a> </li>
<li>Have osm placemarks appear at level 11 onwards by default. <a href='http://commits.kde.org/marble/4a15b4ca8897097ab19c888ab9d0d7b738a0b022'>Commit.</a> </li>
<li>Bump Android version code. <a href='http://commits.kde.org/marble/3ba3d3e626ee626d16ab38ff1f4554eba100f298'>Commit.</a> </li>
<li>Tweak image size. <a href='http://commits.kde.org/marble/ffb6fd65f931eb3d57161d84a47c96c9c45a0c96'>Commit.</a> </li>
<li>Prefer QStringLiteral and cache repeatedly used strings. <a href='http://commits.kde.org/marble/9f91a521d50120db58c0f50aaf1340aa2c42f8b3'>Commit.</a> </li>
<li>Paint highway label on top of railways. <a href='http://commits.kde.org/marble/ad81bea2d2e3573836cc1a81a725da63b2c2e914'>Commit.</a> </li>
<li>Use private member directly. <a href='http://commits.kde.org/marble/adc69b23d9017c0bf669f7de61c3f0563219e5dd'>Commit.</a> </li>
<li>Simplify. <a href='http://commits.kde.org/marble/17a784b808bb9934930aebccfb14e5fa8720ed9f'>Commit.</a> </li>
<li>Fix unitialized value (used when canIterateTags is 0). <a href='http://commits.kde.org/marble/fae02a12482805a6bfe6af116fb0b41bf014b572'>Commit.</a> </li>
<li>Minor performance improvements (cppcheck). <a href='http://commits.kde.org/marble/4b66ae4ff8ab925faf0fa0b11ff8a667570c1103'>Commit.</a> </li>
<li>Minor low-level optimizations. <a href='http://commits.kde.org/marble/ff43bd553b58db2928acae9e5cee1ceef0d48152'>Commit.</a> </li>
<li>Provider faster implementations of the default case (radian unit). <a href='http://commits.kde.org/marble/1d5a7eb190deaefcd51689aa089aea8ac8667ff4'>Commit.</a> </li>
<li>Cache one of the two expensive gdInv calls. <a href='http://commits.kde.org/marble/ac2660dd2fc7ef85db47f9a06fe9b144d51c7536'>Commit.</a> </li>
<li>Simplify bounds check. <a href='http://commits.kde.org/marble/e697a6824a516d584272df7be3381cb26a41512a'>Commit.</a> </li>
<li>Prefer QVector over QList. <a href='http://commits.kde.org/marble/4dd642a49e3b9fd725b433485d0b9a5916858c07'>Commit.</a> </li>
<li>Use a pixmap cache for texture caching. <a href='http://commits.kde.org/marble/5213c02e8779309aee1d7575d751c010d283ff09'>Commit.</a> </li>
<li>Do not create geographics items from invisible placemarks. <a href='http://commits.kde.org/marble/f2ef75f7dccedeb5b519273b0760b0850c553c9c'>Commit.</a> </li>
<li>Create proper link from OSM wikipedia tag. <a href='http://commits.kde.org/marble/7bce2565109f6cb8bea8bfd897ec72d2f9b50cc5'>Commit.</a> </li>
<li>Revert "set the bboxe's height to the building height and use that for building rendering". <a href='http://commits.kde.org/marble/2e5a6e3443437057a54978b8c0047bcc9b199fc8'>Commit.</a> </li>
<li>Revert "have polygon and linear ring icons rendered by PlacemarkLayer". <a href='http://commits.kde.org/marble/91d07d77a3726c7ece10c9d7770fdd7ed0f01797'>Commit.</a> </li>
<li>Revert "have house entries rendered by PlacemarkLayer". <a href='http://commits.kde.org/marble/39cfe9432fe9e73fc533d45c044c87e835424f11'>Commit.</a> </li>
<li>Fix broken check for name & note field existance in ShpRunner. <a href='http://commits.kde.org/marble/0b84b2f04600a14cfe836e8242e6ca16d9319212'>Commit.</a> </li>
<li>Prevent unwanted direct quit from search dialog. <a href='http://commits.kde.org/marble/e49eae6925f342185f891cd9faec0f8e1b4b5b26'>Commit.</a> </li>
<li>Allow showing bicycle and foot routes with Yours service. <a href='http://commits.kde.org/marble/f5319d7e4329a5654486364fe6895b69f10c4199'>Commit.</a> </li>
<li>Adjust dialog height to content. <a href='http://commits.kde.org/marble/75330ca1072aa7e0f15077a3c85b4d1627739d00'>Commit.</a> </li>
<li>Harmonize place font sizes. <a href='http://commits.kde.org/marble/4f64afc4b3409485821c597f5f702ff8fe205d8a'>Commit.</a> </li>
<li>Update links to wiki pages. <a href='http://commits.kde.org/marble/3e4849d5b48c35d789c14831995fcbcc3ff7976b'>Commit.</a> </li>
<li>Fix TestGeoDataCoordinates: adapt to changed string contexts. <a href='http://commits.kde.org/marble/0aa8ad0eb7c6132952f4a4f1b3b0f98ec2ae666a'>Commit.</a> </li>
<li>Do not treat closed line strings as polygons during clipping. <a href='http://commits.kde.org/marble/3cc478ed1d8f2ed558581b3276f502f155a495c9'>Commit.</a> </li>
<li>Fix clip path calculation. <a href='http://commits.kde.org/marble/3199b4f490bd4260aeba21d18e46f6427b38c79e'>Commit.</a> </li>
<li>Bump Android version code to 14. <a href='http://commits.kde.org/marble/42d1c5068ce22ee385a03abab8b173dbd5fc4372'>Commit.</a> </li>
<li>Add an about (and a settings stub) dialog. <a href='http://commits.kde.org/marble/ed28e81c2464c73a65f1ea874bcffd0327b5be41'>Commit.</a> </li>
<li>Increase default zoom value. <a href='http://commits.kde.org/marble/b6845732c6a03de4fbe74c851846f91911b793ea'>Commit.</a> </li>
<li>Switch to Yours while OSRM and ORS are not working. <a href='http://commits.kde.org/marble/fe7682cefd6ce587799360d4469213cdaa584b4f'>Commit.</a> </li>
<li>Avoid file/dir name clashes when using local files. <a href='http://commits.kde.org/marble/6a78fc91e01c0ffc97496ecb6235024a7dbe849c'>Commit.</a> </li>
<li>Merge rectangular boundary tiles of adjacent polygonal input files. <a href='http://commits.kde.org/marble/fdac3b5b8c187601d9ff5ab64f50284592019cd8'>Commit.</a> </li>
<li>Filter some landuse tags (e.g. farmland) on level 11. <a href='http://commits.kde.org/marble/1a62251551071f7868aee693f1211ad2c1ecca06'>Commit.</a> </li>
<li>Return nullptr when file cannot be opened/parsed, not old state. <a href='http://commits.kde.org/marble/c5ce456b24d2d6d825d897a383b0b40470dd3909'>Commit.</a> </li>
<li>Fix refactoring issue introduced in 5c1022d. <a href='http://commits.kde.org/marble/0646a5e26e2137944a51632639588ccd0298eeb8'>Commit.</a> </li>
<li>Fix popupitem sizes to match (hardcoded layout sizes of) content. <a href='http://commits.kde.org/marble/cc707b9afaed81038b5c378a54cca5e6b3ee9771'>Commit.</a> </li>
<li>Support for translations with bundled apps, for now Marble Maps. <a href='http://commits.kde.org/marble/87c0182d3b0a38c58c11249870d5202f944d9acf'>Commit.</a> </li>
<li>Use Qt5 comment system to pass url with more info to translators. <a href='http://commits.kde.org/marble/ef7f429151b9ae07fa245663d595c6e0a6164cfd'>Commit.</a> </li>
<li>Align title of Earthquakes plugin configuration dialog with other dialogs. <a href='http://commits.kde.org/marble/d57d2ece26690fde0925e5247e40437dc5eeb038'>Commit.</a> </li>
<li>For strings extracted from DGML note location with "file:line". <a href='http://commits.kde.org/marble/fb5a3bbba59d64037768b66509912204235ccc9e'>Commit.</a> </li>
<li>Make zoom level adoptions be the last operation on styles. <a href='http://commits.kde.org/marble/5189a1be168d2fa686ba61953e176b2d0df751e3'>Commit.</a> </li>
<li>No need to inherit from GeoDataCoordinatesPrivate. <a href='http://commits.kde.org/marble/7463cfd00e53d8e0a89f933efd1755ba11084689'>Commit.</a> </li>
<li>Don't compare level of detail values for GeoDataPlacemark::coordinate(). <a href='http://commits.kde.org/marble/882d42a3e9bb74edb02cc709413a6182ffce51c7'>Commit.</a> </li>
<li>Compare against values, not strings. <a href='http://commits.kde.org/marble/fe028258f0e57b3523cb5884bf280a49a497e86a'>Commit.</a> </li>
<li>Initialize w/ GeoDataCoordinates members. <a href='http://commits.kde.org/marble/e5aca74adeed990b4cd6586311488b1f9f12fe28'>Commit.</a> </li>
<li>Const'ify. <a href='http://commits.kde.org/marble/a60803b2bea03a3236231e3196e86637f1c7ea0d'>Commit.</a> </li>
<li>Fix string puzzle with labels in statusbar. <a href='http://commits.kde.org/marble/1a22b2ce5446ad26f2f739e3c2c281681e624510'>Commit.</a> </li>
<li>Remove code of past-end-of-line marble-mobile. <a href='http://commits.kde.org/marble/6a4f1d5cd4b173e63a41e31dd0336b899cfeddd0'>Commit.</a> </li>
<li>Revert "do not install extra license files". <a href='http://commits.kde.org/marble/8f9a314e23d53a74346a1a9255370771a792048b'>Commit.</a> </li>
<li>Extract UI string also from PluginInterface.h (& set non-QObject context). <a href='http://commits.kde.org/marble/fad162d3da468f3fe6be4edee9443b1396236245'>Commit.</a> </li>
<li>Make labels in statusbar localizable, use localized ones for size calculation. <a href='http://commits.kde.org/marble/19222df231d65c7193499a5acbfda42a3e726a96'>Commit.</a> </li>
<li>Align elevation edit widget to the top in EditPlacemarkDialog. <a href='http://commits.kde.org/marble/861e5adb6ad8331a9fa4f56f2533eb4b54fc76c5'>Commit.</a> </li>
<li>Fix memory leak: delete OsmRelationEditorDialog also on cancelling. <a href='http://commits.kde.org/marble/757ca724e27df8acc2b37ba845f5e31a5d214209'>Commit.</a> </li>
<li>Fix: make Cancel button work in OsmRelationEditorDialog. <a href='http://commits.kde.org/marble/dd9ae7ee4112e9a1aa1ffe968951b6b8f63ade09'>Commit.</a> </li>
<li>Remove unused include. <a href='http://commits.kde.org/marble/51e13644d02ce400f55aa2c0547ba0c339a4e8b8'>Commit.</a> </li>
<li>Fix: use currentIndex instead of comparing currentText against english version. <a href='http://commits.kde.org/marble/33078d2f99b52f2fc46b906909a76b7b1ff99f08'>Commit.</a> </li>
<li>Make more strings in Maps localizable. <a href='http://commits.kde.org/marble/e94666fe77c9e3d3d16a97ca93280fa0026f81d9'>Commit.</a> </li>
<li>Put also strings from marble-maps into marble_qt catalog. <a href='http://commits.kde.org/marble/e186d795addd54f1c41570babc7207467bf42bba'>Commit.</a> </li>
<li>Cache known layers outside loop in a data structure with faster lookups. <a href='http://commits.kde.org/marble/a7bbae347d94b4de5cac143f1f952fc0e52794ad'>Commit.</a> </li>
<li>Extract rivers on level 11 also, and include waterway=riverbank. <a href='http://commits.kde.org/marble/237fc5cafc29e523da2b0fcbac53b079e40500ae'>Commit.</a> </li>
<li>Initialize GeoDataPolyStyle colorIndex. <a href='http://commits.kde.org/marble/291e449089332ad4ff7f3bcd1ebceb069c3521d3'>Commit.</a> </li>
<li>Prevent copies of MarbleGraphicsItem already, not just on subclasses. <a href='http://commits.kde.org/marble/d6884522f26cec481d9cce49dad90365378829d7'>Commit.</a> </li>
<li>Avoid adjusting style when it can be done during construction. <a href='http://commits.kde.org/marble/df10d176d238d70fb40b828257e8d43376811741'>Commit.</a> </li>
<li>Introducing GeoSceneAbstractTileProjection for tile x/y <-> lonlat. <a href='http://commits.kde.org/marble/5c1022d13a3b68fd3251cf0b57d03087957dc9eb'>Commit.</a> </li>
<li>Qt'fy access to pimpl objects for GeoDataFeature & subclasses. <a href='http://commits.kde.org/marble/c85227a4ccda5f7a6595102f67cf73de0e6791b1'>Commit.</a> </li>
<li>Qt'fy access to pimpl objects for MarbleGraphicsItem & subclasses. <a href='http://commits.kde.org/marble/cafdd1e7322b96af4f155a6e1d433bdd2a44de8c'>Commit.</a> </li>
<li>Qt'fy access to pimpl objects: use macros Q_DECLARE_PRIVATE & Q_D. <a href='http://commits.kde.org/marble/3e280f9061d48eeeb97a29e213d6931a88dbd53e'>Commit.</a> </li>
<li>Explicitely clear a QVector from its items, qDeleteAll does not do that. <a href='http://commits.kde.org/marble/92e6692ca703bba1e6f2f145b4a6b59fcdd75491'>Commit.</a> </li>
<li>Initial fix for webpopup appearance. <a href='http://commits.kde.org/marble/fa8ed7041dd3b74dddf089baa8ca7275483a6e24'>Commit.</a> </li>
<li>No range check is needed here, and operator[] is faster than at(). <a href='http://commits.kde.org/marble/e34e2833aa6a7555bd4fe9aa370686868d1b96f2'>Commit.</a> </li>
<li>Inline manually (hotspot). <a href='http://commits.kde.org/marble/bfc6837b24018cedba5bbb015ab3efb781a2c106'>Commit.</a> </li>
<li>Remove annotations tags (comment, note, fixme, todo, source, ...). <a href='http://commits.kde.org/marble/5dc9ba8bf7bca2a5f699949a751394cb53d29b80'>Commit.</a> </li>
<li>Re-enable node reduction for levels below 17. <a href='http://commits.kde.org/marble/55e24cdac3d0daf79389562975e718dc8801dace'>Commit.</a> </li>
<li>Do not create additional placemarks when converting file formats. <a href='http://commits.kde.org/marble/98d069f88f2da028d10e70ecf989c08d98a4e3ae'>Commit.</a> </li>
<li>No need for an icon on tags. <a href='http://commits.kde.org/marble/74b35067566f9b48f591e2f65d176e39381a402f'>Commit.</a> </li>
<li>Simplify. <a href='http://commits.kde.org/marble/b5672254104e974ab4fc8cb8e8bcff7116e3c709'>Commit.</a> </li>
<li>Improve precision. <a href='http://commits.kde.org/marble/c2e83a463664c370ff83b4d6632615c8b96d80c9'>Commit.</a> </li>
<li>Keep nodes with tags. <a href='http://commits.kde.org/marble/ad0515bd2782e38e9560a02326dd9600b5285721'>Commit.</a> </li>
<li>No need to create a copy. <a href='http://commits.kde.org/marble/e64268225ea673555060cbc6aa725398aa5f430e'>Commit.</a> </li>
<li>Always use o5m file format for cache tiles. <a href='http://commits.kde.org/marble/5acfffe3696927966f5c030277bad15abd0be20c'>Commit.</a> </li>
<li>Have house entries rendered by PlacemarkLayer. <a href='http://commits.kde.org/marble/c5aff6a993e0f17d5dc62b636b41adcbec291c5f'>Commit.</a> </li>
<li>Have polygon and linear ring icons rendered by PlacemarkLayer. <a href='http://commits.kde.org/marble/b1a507ba5d9e2386bc2dd4b2432569910a5e030e'>Commit.</a> </li>
<li>Set the bboxe's height to the building height and use that for building rendering. <a href='http://commits.kde.org/marble/e2e3e1989783c8b5640ad703e2d7001d87f05dcc'>Commit.</a> </li>
<li>Move GeoDataVisualCategory enum from GeoDataFeature to GeoDataPlacemark. <a href='http://commits.kde.org/marble/620949187746a4a2368e81e77fb8d6360e996b7c'>Commit.</a> </li>
<li>Let visual categories only be defined for placemarks. <a href='http://commits.kde.org/marble/405038106b41ba30da316641bef8231d7316fddd'>Commit.</a> </li>
<li>Accept GeoDataPlacemark rather than GeoDataFeature. <a href='http://commits.kde.org/marble/493ece18986ff3ac9e1904ec3c5f82c91dc645b0'>Commit.</a> </li>
<li>Pass the point upon construction. <a href='http://commits.kde.org/marble/fabf916f54939db7ef5a851bdeab7bc97bfd84bd'>Commit.</a> </li>
<li>Create directories only if they are actually needed. <a href='http://commits.kde.org/marble/5e3ee3cbcf73bc0712e7da1e8be537e7614759c5'>Commit.</a> </li>
<li>Use progress bar output for high level tiles as well. <a href='http://commits.kde.org/marble/f4530d81d984b02df492fa66a1bc4446e9b70706'>Commit.</a> </li>
<li>Disable node reduction on polygons/rings for now. Needs more work. <a href='http://commits.kde.org/marble/081d2950f7e7a7f587b76191386e4b1047e798a7'>Commit.</a> </li>
<li>Avoid repeated function calls inside loop. <a href='http://commits.kde.org/marble/69fdb9bc11b303bbfc38274cc56f3ab38f17d6bd'>Commit.</a> </li>
<li>Use a tile pyramid to speed up clipping. <a href='http://commits.kde.org/marble/de1208749510aff11ce00d715ad075c8cd38ae0b'>Commit.</a> </li>
<li>Reduce number of comparisons from 2n to 1.5n in average. <a href='http://commits.kde.org/marble/9636c8c18d6166dbe10898f7ba5267bfde053824'>Commit.</a> </li>
<li>Manually implement the default parameter value case for performance. <a href='http://commits.kde.org/marble/35c33f18b7d50834a78cfbdb11d946040c501ea2'>Commit.</a> </li>
<li>Split GeoPolygonGraphicsItem class into subtypes Building & Normal. <a href='http://commits.kde.org/marble/ba2a3c9c6ea54aa00b9ad42f515f2f05c441669f'>Commit.</a> </li>
<li>Add optional support for writing to a mbtile database. <a href='http://commits.kde.org/marble/2729488eefabd85724c66e4c079009268a35675e'>Commit.</a> </li>
<li>Rely on building visual category and nothing else. <a href='http://commits.kde.org/marble/62c88c811469365bca0f1a788451dbc987e7ad6c'>Commit.</a> </li>
<li>Prefer building to other competing tags that set a visual category. <a href='http://commits.kde.org/marble/6dc481ad0c5f9dac3bb78f2e92e42a4cfacda42d'>Commit.</a> </li>
<li>Grant osmconvert more time to finish. <a href='http://commits.kde.org/marble/1fbaba73adc21a338872f2ecf7c6baa322499c52'>Commit.</a> </li>
<li>Extract landmass file if needed. <a href='http://commits.kde.org/marble/7360d426783a5f9d581593717afd937e563ee8ad'>Commit.</a> </li>
<li>Less dominant road style in medium levels. <a href='http://commits.kde.org/marble/6bf2fd045fec880cfd4aa989135da2cce7a98059'>Commit.</a> </li>
<li>Use urls as input and download data as needed. Use bounding polygon. <a href='http://commits.kde.org/marble/cc9a2d9ab974e35d9addc1a9e196148dc7e42c85'>Commit.</a> </li>
<li>Fix total number of tiles calculation. <a href='http://commits.kde.org/marble/8e1d8e36da9e200328204c6b1c3dd25d1682c013'>Commit.</a> </li>
<li>Introduce a cache directory, pre-tiling is done internally now. <a href='http://commits.kde.org/marble/a429adaf09016ae3bb6e294e465fda557338d1fd'>Commit.</a> </li>
<li>Remove redundant name specifier. <a href='http://commits.kde.org/marble/464996fbb3e190c1deef176b0787ca8ff01053c4'>Commit.</a> </li>
<li>Reduce function call overhead. <a href='http://commits.kde.org/marble/3c141903adddbf5ae2fa440ae4800ae8cdfc2a0a'>Commit.</a> </li>
<li>Reduce geometry call overhead. <a href='http://commits.kde.org/marble/58dee2489ae6464dda4c4d4d5bc827ec943f5a19'>Commit.</a> </li>
<li>Store tiles in local Marble path by default. <a href='http://commits.kde.org/marble/293df21652d0a244c3cc1f8d21f07265f69b0547'>Commit.</a> </li>
<li>Add an option to skip recreating existing tiles. <a href='http://commits.kde.org/marble/8b7a5fc44c392af8ca6e0e1d6f1113d95c8824ed'>Commit.</a> </li>
<li>Remove debug levels. <a href='http://commits.kde.org/marble/3c7e15306c8cb401253f0c04939b31fbbea1c006'>Commit.</a> </li>
<li>Traverse by input map file to improve speed. <a href='http://commits.kde.org/marble/84d0710ae43fcda79ad36c70615d545f93a35557'>Commit.</a> </li>
<li>Use tiling approach for input map data as well. <a href='http://commits.kde.org/marble/c2803467097480d1e0f2d8ae0ad08ba36b557f3c'>Commit.</a> </li>
<li>Do not write empty files. <a href='http://commits.kde.org/marble/2a26eab3f1fcf0c18d0ad070a134a74e1e59a376'>Commit.</a> </li>
<li>Remove unneeded static attribute. <a href='http://commits.kde.org/marble/8bb2d7884e57fed42a03ee639a4ff811c493810b'>Commit.</a> </li>
<li>Check file existance/readability before parsing. <a href='http://commits.kde.org/marble/132dd389b2861433f63b98462c72507b8a0e727e'>Commit.</a> </li>
<li>Move LandmassLoader to its own file, rename TileDirectory. <a href='http://commits.kde.org/marble/ec804223cd061d6b4b33cb5f4fe8924002cd2aec'>Commit.</a> </li>
<li>Add an option to pre-tile data using osmconvert (for large input). <a href='http://commits.kde.org/marble/3078a3cb8af2d24ac6e60ee035e42bae48fe120e'>Commit.</a> </li>
<li>Zoom level is always needed. <a href='http://commits.kde.org/marble/2f642ecfa7328afa93e22f0ce49f65107dfbbf48'>Commit.</a> </li>
<li>Remove overlap between route-editor button and bookmark button. <a href='http://commits.kde.org/marble/cc8406238d29477478c0ade0672123d510bc5050'>Commit.</a> </li>
<li>When user opens app and lands on a portion of map which contains. <a href='http://commits.kde.org/marble/4e862bbfc4bbe432ecc981e5a5008ad59a7adb27'>Commit.</a> </li>
<li>Load landmass data from tiles. <a href='http://commits.kde.org/marble/ac95285731f09b4e416aab2ac84fa18a129ea4cd'>Commit.</a> </li>
<li>Re-use tile clipping for landmass. <a href='http://commits.kde.org/marble/1f1204a5ef49c9c20fba47361c550de0c13c006a'>Commit.</a> </li>
<li>Switch to o5m as default extension. <a href='http://commits.kde.org/marble/0eccdde0c2f9fbeb70560ca8f769e8d22ac99a4a'>Commit.</a> </li>
<li>Correct indentation. <a href='http://commits.kde.org/marble/220ce8f3c112ec5403df653489d453cd67e9347d'>Commit.</a> </li>
<li>Center on current position when application starts,. <a href='http://commits.kde.org/marble/b9e355c87a995ee751d75f5c941479a2b51e9fa5'>Commit.</a> </li>
<li>Fix building on Ubuntu 16.04 (Qt 5.5). <a href='http://commits.kde.org/marble/830aee230b404f5e418f715f16d9dc6e99dd1558'>Commit.</a> </li>
<li>Fix link library call scope. <a href='http://commits.kde.org/marble/e19e8be89917c5e1c9d32885850776cb7a35f2aa'>Commit.</a> </li>
<li>Less prominent roads in level 7 and 9. <a href='http://commits.kde.org/marble/3c5448b5215b0dd257fd198ee139d1feab8d59c1'>Commit.</a> </li>
<li>Bring back some placemarks we do not (yet) extract from natural earth. <a href='http://commits.kde.org/marble/34e71ddb0ff60eca0bee9a9c5706bfe430be9f03'>Commit.</a> </li>
<li>Enable icons for osm city placemarks. Icons from osm-carto like before. <a href='http://commits.kde.org/marble/f94b7ac1636ee5f5cc47542ebf62a2b6a5db1b57'>Commit.</a> </li>
<li>Draw horizontally centered labels on top of their icons, not below. <a href='http://commits.kde.org/marble/721ee30a107f47e62ce276a62f92b1d2586e9090'>Commit.</a> </li>
<li>Stronger node reduction in higher levels. <a href='http://commits.kde.org/marble/e101518d1697756ef209c7ccbd94425bae46e6bd'>Commit.</a> </li>
<li>Tweak highway extraction in level 13/15. <a href='http://commits.kde.org/marble/524b57fce26230093a1ee4e0cd3380ca2a2a41f6'>Commit.</a> </li>
<li>Use city placemarks from tiles. <a href='http://commits.kde.org/marble/8aa6c785c059ed8e52f5882004e628b62fceedd9'>Commit.</a> </li>
<li>Extract city placemarks. <a href='http://commits.kde.org/marble/082609727fe61ebec86ca9c6e623227566a98b89'>Commit.</a> </li>
<li>Revert "Kml_scaleTagHandler{.cpp, .h} -> KmlScaleTagHandler{.cpp, .h}". <a href='http://commits.kde.org/marble/37fc703d310866985014b6d88c4af0c8817432f1'>Commit.</a> </li>
<li>Kml_scaleTagHandler{.cpp, .h} -> KmlScaleTagHandler{.cpp, .h}. <a href='http://commits.kde.org/marble/1368ec36ded88665dc113d619698bcb7a68e8547'>Commit.</a> </li>
<li>Allow implicit linking to qtnetwork for build to work even without webkit. <a href='http://commits.kde.org/marble/a8f0f4b72ff236c170acded452c692e01e1686aa'>Commit.</a> </li>
<li>Create multiple placemarks for multipolygons with several outer ways. <a href='http://commits.kde.org/marble/bacea90c3ef5bf5605d28e70568a86df3cab1403'>Commit.</a> </li>
<li>Extract more landuse areas in tile levels 11 and 13. <a href='http://commits.kde.org/marble/996e761ad772c63ce8226a5db2d10024649f1c66'>Commit.</a> </li>
<li>Support multiple zoom levels. <a href='http://commits.kde.org/marble/7e9f14f8709b2f05a7bdc46d8c9f24866c6da9ad'>Commit.</a> </li>
<li>Cut landmass to input map region. <a href='http://commits.kde.org/marble/4e7b699ecf10f59378fd23161b1e8ca3f04d201c'>Commit.</a> </li>
<li>Fix output. <a href='http://commits.kde.org/marble/411dd30243c83148d91b32f7f17013351f3b37c1'>Commit.</a> </li>
<li>Simplify memory management. <a href='http://commits.kde.org/marble/b1ee2d8cd9a09640a54b10c95fbcec8863bb860f'>Commit.</a> </li>
<li>Always extract elevation. <a href='http://commits.kde.org/marble/453200a86fd04d085f2640b67b1f177038844af7'>Commit.</a> </li>
<li>Resolve compiler warnings of uninitialized variables. <a href='http://commits.kde.org/marble/61cfd05382e8d4c8a273ad329cfbde713193df1f'>Commit.</a> </li>
<li>Remove marble/global.h, deprecated since 2012. <a href='http://commits.kde.org/marble/0026e64a35cd5cc0ed7c4c3b0f9ea328d9fe72fb'>Commit.</a> </li>
<li>Pass more arguments of enum type by value. <a href='http://commits.kde.org/marble/8948afb91b52b237cb2c49d47c595490f853617a'>Commit.</a> </li>
<li>Big cleanup of Marble classes includes in headers of libmarblewidget. <a href='http://commits.kde.org/marble/56c850054debe68a920d94fe712af2f69867d071'>Commit.</a> </li>
<li>Use mDebug() for no menu entry created for plugin with unknown render type. <a href='http://commits.kde.org/marble/735cc620419da6f9eec99bbfa72181c7590d133d'>Commit.</a> </li>
<li>GeoPolygonGraphicsItem: query properties/objects in methods only once. <a href='http://commits.kde.org/marble/662c2baad5d16474f0d11d9c9d6e7f7019ec0214'>Commit.</a> </li>
<li>GeoPolygonGraphicsItem::configurePainter(): also consider brushStyle. <a href='http://commits.kde.org/marble/d596845458c7aa4afb490bce1644ca987e24d3e0'>Commit.</a> </li>
<li>Pass arguments of simple type (enum, bool, int, float) by value. <a href='http://commits.kde.org/marble/4c20d68f025a04eeea8614a6603a7ef267929092'>Commit.</a> </li>
<li>Preserve collinear points to keep artificial clip border points. <a href='http://commits.kde.org/marble/9d3bc52f0bfae57d2ce54ca22e33e91e2684ecc0'>Commit.</a> </li>
<li>Improved progress output. <a href='http://commits.kde.org/marble/232ef7aa54cba696660dfa6b5e7a217ea619da3b'>Commit.</a> </li>
<li>No tag filtering on level 17+. <a href='http://commits.kde.org/marble/8390a968688d6ba803afd24b216a0970f2cd30dd'>Commit.</a> </li>
<li>Set Qt::AA_UseHighDpiPixmaps true for marble desktop apps. <a href='http://commits.kde.org/marble/de6d8f35ac494c5007d47b40a67cf947ce5e0fba'>Commit.</a> </li>
<li>Scale pixmap cache of MarbleGraphicsItem. <a href='http://commits.kde.org/marble/17c73b120bfe418a0e854fd7f2846014ef400b9c'>Commit.</a> </li>
<li>Apply suggestions from cppcheck --enable=performance. <a href='http://commits.kde.org/marble/900d04b18bebe32efca88f481d22b760aa3383a5'>Commit.</a> </li>
<li>Use the clipper library as an alternative clipping algorithm. <a href='http://commits.kde.org/marble/89a0e9010523f48d4ebebd289e1c9fef9140e079'>Commit.</a> </li>
<li>Provide higher level of detail. <a href='http://commits.kde.org/marble/0e3df3294fc1b36120bac6cf58dca7bee7ce431a'>Commit.</a> </li>
<li>Do not create invalid tile IDs for longitude = +180 degree values. <a href='http://commits.kde.org/marble/4f42b7231fc0ce51d9b0e81c3897dea60e69d562'>Commit.</a> </li>
<li>Replace gray background with blue water background. <a href='http://commits.kde.org/marble/cbf676abb19a10ecf022e123df7b9c05860be98b'>Commit.</a> </li>
<li>Render building parts. <a href='http://commits.kde.org/marble/66b09fb977008844906c332796221e3681b7dc6a'>Commit.</a> </li>
<li>Return building tags rather than building values. <a href='http://commits.kde.org/marble/2ba44684ca805f3631efac570119e8789d8763c6'>Commit.</a> </li>
<li>Remove redundant code. <a href='http://commits.kde.org/marble/da611f66129294ee6ce8dc6fae95ea020a6f92da'>Commit.</a> </li>
<li>Avoid possible deletion of just created placemark. <a href='http://commits.kde.org/marble/b8d82b4fd1acec32d5e838ad569779fa4d5eb353'>Commit.</a> </li>
<li>Open URLs in external browser. <a href='http://commits.kde.org/marble/f6e27008765c8023be8f124403b21a4b1a3377f0'>Commit.</a> </li>
<li>Add Marble namespace to KDescendantsProxyModel copy. <a href='http://commits.kde.org/marble/7321f7fcf1d89f015659edecf32956eccd8442a0'>Commit.</a> </li>
<li>Remove unneeded includes from TileId.h. <a href='http://commits.kde.org/marble/387d8fe88537aa856f0835ab4adfb8742c5bb3dc'>Commit.</a> </li>
<li>Add more API docs to the AbstractProjection class. <a href='http://commits.kde.org/marble/32872cf4da95c1c7e567c3f343a4e271bff5e906'>Commit.</a> </li>
<li>Always fall back to "ref" tag when no name is found (not only for highways). <a href='http://commits.kde.org/marble/7fc3301af5cffc0be860e7f0008294e3e909db6f'>Commit.</a> </li>
<li>Simplify. <a href='http://commits.kde.org/marble/315ab418be7f159aa42ebd383c89769b3bc2bb83'>Commit.</a> </li>
<li>Opencaching: Fix missing closing bracket (to silence lupdate). <a href='http://commits.kde.org/marble/7ce610a50e70d70470671d26836f6556bcd9ad23'>Commit.</a> </li>
<li>Worldclock: default locationLongitude to 0 if not available. <a href='http://commits.kde.org/marble/87edf909c16bdbd3e6776475fcb8e8b7bb532176'>Commit.</a> </li>
<li>Port DeclarativeDataPlugin from QtScript to QJson* -> no more Qt5Script dep. <a href='http://commits.kde.org/marble/4e97f5be53e8981c626a8ac9caa74f3406a040fd'>Commit.</a> </li>
<li>Port JsonPlugin from QtScript to QJson*. <a href='http://commits.kde.org/marble/d11cdb14ab2d672538cdc051ec97a2f66def5281'>Commit.</a> </li>
<li>Port FoursquarePlugin from QtScript to QJson*. <a href='http://commits.kde.org/marble/aa3e0ffe312ed796f7cfb716438a6faa2038f6d5'>Commit.</a> </li>
<li>Port OSRMPlugin from QtScript to QJson*. <a href='http://commits.kde.org/marble/c97daf2039ac42b70ab776f139d7c82a21c59099'>Commit.</a> </li>
<li>Probably bring back photo rendering. <a href='http://commits.kde.org/marble/a2b74101acf6e89ece07830952938eed9751189d'>Commit.</a> </li>
<li>Reduce memory footprint of GeoDataPlacemark in the style of d7f36cb. <a href='http://commits.kde.org/marble/df9b850576a0d4b72b533ca4d6c454ca500a0758'>Commit.</a> </li>
<li>Fix memory leak. <a href='http://commits.kde.org/marble/6bf6d8881cfeed5f3d6152433bc9e1fcf821d94d'>Commit.</a> </li>
<li>Improve heuristic (do not exit too early). <a href='http://commits.kde.org/marble/f5b876f2a8386a705c1748276d614a67457a4507'>Commit.</a> </li>
<li>Free memory early when opened files are not needed anymore. <a href='http://commits.kde.org/marble/5a455698469e9238e174bd2c77bad6681f4e9325'>Commit.</a> </li>
<li>Reduce memory footprint of GeoDataFeature instances. <a href='http://commits.kde.org/marble/d7f36cbeb6588db4d38175ad0b4025eb78f4f6aa'>Commit.</a> </li>
<li>Do not assume first == last. <a href='http://commits.kde.org/marble/5f33d108b4f53669923a223da23783ed05c530f1'>Commit.</a> </li>
<li>Make GeoGraphicsItem::latLonAltBox() purely virtual. <a href='http://commits.kde.org/marble/f72396cb4cb1879559c42e84431cb20591df2110'>Commit.</a> </li>
<li>No need for StyleBuilder::Private:: prefix. <a href='http://commits.kde.org/marble/c78b2f09485ab9cb2aaae8bca2db7f75dd9396df'>Commit.</a> </li>
<li>Remove manual processing steps. <a href='http://commits.kde.org/marble/21045945f7e08992c6bc21ed7030a4f286b0b2b3'>Commit.</a> </li>
<li>Add landmass tag when merging backdrop. <a href='http://commits.kde.org/marble/65eb67f9b39f830c74b896a5728561bbcb610de5'>Commit.</a> </li>
<li>Add filter tags for levels 13 and 15. <a href='http://commits.kde.org/marble/8e561af10a56c55b4eadfa916272d6852fa67aca'>Commit.</a> </li>
<li>Don't enforce a pure virtual method that does not fit well. <a href='http://commits.kde.org/marble/a5b4a52012715ff01015e20df74500733779ebe9'>Commit.</a> </li>
<li>Rename TinyPlanetProcessor VectorClipper. <a href='http://commits.kde.org/marble/87638c5d8c364774864d0c4ccf71e3074374b9d4'>Commit.</a> </li>
<li>Remove PlacemarkFilter class which serves no purpose. <a href='http://commits.kde.org/marble/b2e184c04e552739c5cf0249264dc25a6c643538'>Commit.</a> </li>
<li>Prevent copies. <a href='http://commits.kde.org/marble/b3dce195aae05d0c1708ccb6c743728762384102'>Commit.</a> </li>
<li>Rename cutToTiles() to clipTo(). <a href='http://commits.kde.org/marble/6450b503eaad0f01f6044d48920f0bfa6382a3e3'>Commit.</a> </li>
<li>ShpCoastlineProcessor is superseded by TinyPlanetProcessor. <a href='http://commits.kde.org/marble/0df741d697c7d68d10a3c693ad5f2ea804a86385'>Commit.</a> </li>
<li>Processing chain for tile level 11. <a href='http://commits.kde.org/marble/939554605ddb02436a1941dff54954dd249452bc'>Commit.</a> </li>
<li>Add a landmass file conversion option. <a href='http://commits.kde.org/marble/7744b947ca351368a0344ad1c661580ff5fbba56'>Commit.</a> </li>
<li>Fix placemark ownership problems. <a href='http://commits.kde.org/marble/e258fe0bf2ec44e31e51663bf8e08cb1691cce77'>Commit.</a> </li>
<li>Extract method clipTo(GeoDataLatLonBox). <a href='http://commits.kde.org/marble/792a609668e8e34a6cd2bdeb021e9f36316b3948'>Commit.</a> </li>
<li>Move inverse methods tileX2lon, tile2Ylat to TileId as well. <a href='http://commits.kde.org/marble/e929bbcb89d64aec0dd3487d88e3f771d0ca340b'>Commit.</a> </li>
<li>Remove wrong factor 90/85.0511. <a href='http://commits.kde.org/marble/5ffca170843876ae99a181f7d27e5cd498a62560'>Commit.</a> </li>
<li>Fix generated tile IDs for medium/high levels. <a href='http://commits.kde.org/marble/4c0533d747f4569df8adfa455f08a6ce3fafb855'>Commit.</a> </li>
<li>Move lon2tileX, lat2TileY methods to TileId class. <a href='http://commits.kde.org/marble/cc98be13cf2a13bd4ed16c8f37569f97cbe46e4d'>Commit.</a> </li>
<li>Port OpenCachingComPlugin from QtScript to QJson*. <a href='http://commits.kde.org/marble/2fbf41d031b734ee2bb1046aca2e25ed15406450'>Commit.</a> </li>
<li>Port Weather plugin from QtScript to QJson*. <a href='http://commits.kde.org/marble/243317e06dcdf963b581b79b98d9c65373717c2c'>Commit.</a> </li>
<li>Port PostalCode plugin from QtScript to QJson*. <a href='http://commits.kde.org/marble/63137bfa68c53b0621f0cceaababb2c6d7fafe80'>Commit.</a> </li>
<li>Port marblewidget from QtScript to QJson*. <a href='http://commits.kde.org/marble/6fc39be28efede63c67a18de1fad180be257cf40'>Commit.</a> </li>
<li>Port OpenDesktopPlugin from QtScript to QJson*. <a href='http://commits.kde.org/marble/4915d5894b8808b7328317cea9a6895a0764e43d'>Commit.</a> </li>
<li>Port EarthquakePlugin from QtScript to QJson*. <a href='http://commits.kde.org/marble/2242019ccaed1b48d4884eb20825a8fb367a89dd'>Commit.</a> </li>
<li>Use localized values for display of EarthquakeItems. <a href='http://commits.kde.org/marble/881e5d0c8f8a614f2b36b41d8059690bb90c9648'>Commit.</a> </li>
<li>Make EarthquakeItem tooltip translatable. <a href='http://commits.kde.org/marble/5208de325ec74c6adc959957d32698d2dc01923f'>Commit.</a> </li>
<li>Const'ify. <a href='http://commits.kde.org/marble/d27bcd539398d3a37df29f13c664732ad5153aba'>Commit.</a> </li>
<li>Do not hardcode font for street styles to Arial, but use generic map font. <a href='http://commits.kde.org/marble/c3dee96ba6725f94c39681c67aa5281f121eaeaf'>Commit.</a> </li>
<li>Revert "Note KF5 version where SERVICES_INSTALL_DIR, DATA_INSTALL_DIR are needed". <a href='http://commits.kde.org/marble/7ed790f7c59cf51f3ee8387a6f98de31c30e4bcb'>Commit.</a> </li>
<li>Fix vectorosm/legend.html, align with the others. <a href='http://commits.kde.org/marble/494da6a6c8a40aeda47c88c888e2f1b1b6e90e2f'>Commit.</a> </li>
<li>Fix: legends item either have a pixmap or a color, not both. <a href='http://commits.kde.org/marble/729a84e51e15a55187a066bd91aafe7cc516fb87'>Commit.</a> </li>
<li>Remove outdated AbstractDataPlugin::setDelegate(QtQuick stuff). <a href='http://commits.kde.org/marble/567b7c1002e4525c6a843f568ce996ca36b7c85c'>Commit.</a> </li>
<li>WayConcatenator::getWayChunk(...) -> WayConcatenator::wayChunk(...) const. <a href='http://commits.kde.org/marble/6f21c01ffb7d5bd05d9dd94844c855fd4894d7a5'>Commit.</a> </li>
<li>Note KF5 version where SERVICES_INSTALL_DIR, DATA_INSTALL_DIR are needed. <a href='http://commits.kde.org/marble/fb427298b1d31734b86ebdca4e6be04d7ff570d1'>Commit.</a> </li>
<li>Update some comments & notes to Qt5/KF5. <a href='http://commits.kde.org/marble/af95f5f04b6eacb06973a3227faa7366e82bd657'>Commit.</a> </li>
<li>Remove deprecated QTONLY cmake flag. <a href='http://commits.kde.org/marble/2522847856a96d18a31da248be86d224bfe04ef6'>Commit.</a> </li>
<li>Libmarblewidget: Make map legend translatable. <a href='http://commits.kde.org/marble/2f33671aca406a35214d5ca3577ea46645f663d5'>Commit.</a> </li>
<li>Pass a GeoDataPlacemark rather than a visual category, so in the future more magic can happen behind the scene. <a href='http://commits.kde.org/marble/1d9e20ecdee086c654fe9642c446f2c22f54a5e9'>Commit.</a> </li>
<li>Remove unneeded includes. <a href='http://commits.kde.org/marble/b4a96f1c08f2de8fbf520ca1f6d94e6c1a0b9815'>Commit.</a> </li>
<li>Tag new methods (or changed with incompatible signature) added for 0.26.0. <a href='http://commits.kde.org/marble/8c9aad1b292c20d243f98ed37764e5d94b07a951'>Commit.</a> </li>
<li>Add and use OsmPlacemarkData::findTag(const QString& key). <a href='http://commits.kde.org/marble/6a5e0d810d43dab354784f2dc877e8243a395bf0'>Commit.</a> </li>
<li>Pass const reference to lambda. Might fix build with GCC 4.8. <a href='http://commits.kde.org/marble/c04efe5a74cc1d5e64562216d7d82adcec5e1add'>Commit.</a> See bug <a href='https://bugs.kde.org/368219'>#368219</a></li>
<li>Use foreach. <a href='http://commits.kde.org/marble/3eae87c251a65bce7a88e3b601ee776e17cdd903'>Commit.</a> </li>
<li>Simplify & align MarbleWidgetPopupMenu & Placemark code for osmdata usage. <a href='http://commits.kde.org/marble/981a59aa12df9acb3d18bc10b450e7431d42122b'>Commit.</a> </li>
<li>Move o5mreader sources to 3rdparty directory. <a href='http://commits.kde.org/marble/900a199dbe6851be1e20c42a73c44b2745bdad43'>Commit.</a> </li>
<li>Use const ref instead of deep copies of OsmPlacemarkData. <a href='http://commits.kde.org/marble/5a45888ad8a1629eed92f459b729ab731ff32f0a'>Commit.</a> </li>
<li>Remove Hillshading from VectorOSM, not compatible. <a href='http://commits.kde.org/marble/ae0bc841c02e9fce7fb00e587eef38bc9edc5433'>Commit.</a> </li>
<li>More meaningful node reduction statistics. <a href='http://commits.kde.org/marble/3439995ac12215e62a007aa2179bff6ab1c21333'>Commit.</a> </li>
<li>Error out earlier (in debug mode, problem in question still exists). <a href='http://commits.kde.org/marble/cbc023b24f8f9ae11d3f87eb30c91e8ddcf4073c'>Commit.</a> </li>
<li>Free memory early. <a href='http://commits.kde.org/marble/960680d0fe166cde05ae726effea7eccffb5c5a4'>Commit.</a> </li>
<li>Prefer QVector over QList. <a href='http://commits.kde.org/marble/a07275bdc68d185b14a952f31e7d104a2b0ee813'>Commit.</a> </li>
<li>Fix memory leaks. <a href='http://commits.kde.org/marble/6ec9b30e7ab001964d0e8f71e0aaf57d5a4f3b11'>Commit.</a> </li>
<li>Favor nodeType and static_cast over dynamic_cast (faster). <a href='http://commits.kde.org/marble/a6b115517b23943b2b5868d7e3473c051b7d2394'>Commit.</a> </li>
<li>Use QCoreApplication instead of QApplication. <a href='http://commits.kde.org/marble/36319184f908f4dc436643571cef5327383d98ef'>Commit.</a> </li>
<li>Allow construction with only QCoreApplication (not QApplication). <a href='http://commits.kde.org/marble/f644c48a9109420363810fa74b94c647ec2ef87f'>Commit.</a> </li>
<li>Use the Marble based tooling instead of osmconvert/osmfilter. <a href='http://commits.kde.org/marble/7e767314ced51c9e5212d77235bc907427691938'>Commit.</a> </li>
<li>Add processing steps for lower level tiles (0-9). <a href='http://commits.kde.org/marble/7eee1fb78880a9267ddffc88db88d2ae51cea5b6'>Commit.</a> </li>
<li>Rename osm-simplify (marble-)vectorosm-tilecreator. <a href='http://commits.kde.org/marble/9aa7bce70fef178a0ab260a9cd761edfb5e35d85'>Commit.</a> </li>
<li>Remove unused class. <a href='http://commits.kde.org/marble/f52af133e3f88a30dbfde761a1a05516dde8a909'>Commit.</a> </li>
<li>Simplify. <a href='http://commits.kde.org/marble/50e1269a4c6165c67db3aac867d03f052460eab5'>Commit.</a> </li>
<li>Cache iterator end. <a href='http://commits.kde.org/marble/38e91e8eeddbab817b37f38e7b69bb623cc52035'>Commit.</a> </li>
<li>Use Marble namespace consistently. <a href='http://commits.kde.org/marble/017a680a06c7e110c3ed9a6ac65930d9a7340745'>Commit.</a> </li>
<li>Simplify. <a href='http://commits.kde.org/marble/a3891570eb191c9ceccb53a8630be178979447f9'>Commit.</a> </li>
<li>Avoid static_casts and redundant code. <a href='http://commits.kde.org/marble/df015efc2e70b80d23bcad683d7f819904009af4'>Commit.</a> </li>
<li>Normalize headers. <a href='http://commits.kde.org/marble/61e174ee6913582f4584a664e8e00abead867a6c'>Commit.</a> </li>
<li>Use qDeleteAll. <a href='http://commits.kde.org/marble/1f5dc1b577eb5d8f1a42f7e035a8717916fb357d'>Commit.</a> </li>
<li>Don't repeat yourself. <a href='http://commits.kde.org/marble/89c5847ca2e455cb92f8e3df330992c6d7967df8'>Commit.</a> </li>
<li>Even more whitespace fixes. <a href='http://commits.kde.org/marble/e8e5e4290fc90effa296e439c69568e99b0a7e3b'>Commit.</a> </li>
<li>More whitespace fixes. <a href='http://commits.kde.org/marble/a4135f7eb85d125ec4197a91ad7885b7c34c0898'>Commit.</a> </li>
<li>Use initializer list, fix whitespace. <a href='http://commits.kde.org/marble/55b28190ab538697d4a977bd1ff2a11e213b9aac'>Commit.</a> </li>
<li>Compile. <a href='http://commits.kde.org/marble/85ff00bcdd42dc1667349cc6d2d56941dd8863a8'>Commit.</a> </li>
<li>Fix potential use of uninitialized variable reducedLine. <a href='http://commits.kde.org/marble/247b9531954c6677f5467d42846a826c7c680f44'>Commit.</a> </li>
<li>Do not reuse flag for different objects and make it const. <a href='http://commits.kde.org/marble/78bad5baab40eaea0f69c5cecce3ae1a75ff2aae'>Commit.</a> </li>
<li>Avoid code duplication. <a href='http://commits.kde.org/marble/dc4526e9c65fe71f3e8b73430935e61547b3875b'>Commit.</a> </li>
<li>This is redundant. <a href='http://commits.kde.org/marble/642c4af60534bc8ed7f81e66a75365aac1f8ef8b'>Commit.</a> </li>
<li>Version bump to 0.25.20 (0.26 development version). <a href='http://commits.kde.org/marble/c66c27c71c12096d56913b750315bc5698b3939a'>Commit.</a> </li>
<li>Fix "QObject::disconnect: Unexpected null parameter" warning in stderr. <a href='http://commits.kde.org/marble/68de7e50892b61f952161236026485c4166a5052'>Commit.</a> </li>
<li>Fix "libpng warning: iCCP: known incorrect sRGB profile" warnings. <a href='http://commits.kde.org/marble/b6546aa95666b20f4789fdf67e685d16756acd8c'>Commit.</a> </li>
<li>Run optipng on all png files. <a href='http://commits.kde.org/marble/d8c4e7ee72897bc8732a1bc2748ad4ae152c0cad'>Commit.</a> </li>
<li>Fix "iCCP: profile 'ICC Profile': 1000000h: invalid rendering intent" warning. <a href='http://commits.kde.org/marble/97e47523ab704d16c50fd5540b74161569c0b952'>Commit.</a> </li>
<li>Use QStringLiteral with setName(QString). <a href='http://commits.kde.org/marble/762a7bde707315094f74a94a369f88b48d13960e'>Commit.</a> </li>
<li>Use QStringLiteral with elementsByTagName/firstChildElement. <a href='http://commits.kde.org/marble/1ccc01975e229d3956d1351865ba9113f5840411'>Commit.</a> </li>
<li>Use QStringLiteral with setStyleUrl. <a href='http://commits.kde.org/marble/d3e078acad59c523ba81b73a9624988b7537b772'>Commit.</a> </li>
<li>Use QStringLiteral with GeoDataData constructor. <a href='http://commits.kde.org/marble/24b06b04059b55018a84ca9423d528c8a09e320d'>Commit.</a> </li>
<li>Use QStringLiteral with some paintlayer ids. <a href='http://commits.kde.org/marble/cc3bcb4a2efc192ecb8b6d3b44c39356d9fc4426'>Commit.</a> </li>
<li>Use QStringLiteral with OsmPlacemarkData::addTag. <a href='http://commits.kde.org/marble/d032a563b9d875d79b35bf9da5cb4e7b31068386'>Commit.</a> </li>
<li>Use QStringLiteral with OsmPlacemarkData::tagValue/containsTagKey/containsTag. <a href='http://commits.kde.org/marble/342a17a31b94d8c97f6ef511f11537c97125bb7f'>Commit.</a> </li>
<li>Use QStringLiteral with MarbleMap::propertyValue/setPropertyValue. <a href='http://commits.kde.org/marble/aba0b56fa291e0c9f7cb39a7237ad53570f5567c'>Commit.</a> </li>
<li>Use QStringLiteral with all example apps. <a href='http://commits.kde.org/marble/40cbcf1a2b03ffea333a897936e783f70718403a'>Commit.</a> </li>
<li>Examples: Remove default values from overriden methods, unneeded here. <a href='http://commits.kde.org/marble/77a4ce271b6780361a4fb43971f8f27f880ef6dd'>Commit.</a> </li>
<li>Use QStringLiteral with PlanetFactory::construct. <a href='http://commits.kde.org/marble/a4cd48aa6eb32d4bddd9342aaf1af24b90325877'>Commit.</a> </li>
<li>Use QStringLiteral with styleMap methods. <a href='http://commits.kde.org/marble/1c2ef0e7151ebb018432f4db418d8356cd2b3fd5'>Commit.</a> </li>
<li>Use QStringLiteral with setId. <a href='http://commits.kde.org/marble/60d4eabde653b017f3ed6387b59e628822775bfe'>Commit.</a> </li>
<li>Use QStringLiteral with setMapThemeId. <a href='http://commits.kde.org/marble/fdc5a869854baf05635cdba9c352855561df874f'>Commit.</a> </li>
<li>Use QStringLiteral with all QString qrc ids. <a href='http://commits.kde.org/marble/05705f4f68b53e58fb5da0ffc34617435b398ef9'>Commit.</a> </li>
<li>Use QStringLiteral with MarbleDirs::path(). <a href='http://commits.kde.org/marble/b527a095ea7b9e66cb2435a41ce9cf56619c323c'>Commit.</a> </li>
<li>Use QStringLiteral with QString keys for setting methods. <a href='http://commits.kde.org/marble/531bebed03f7a80593e72eb9680d80dc17d33d9e'>Commit.</a> </li>
<li>Use QStringLiteral with more namedItem(QString) methods. <a href='http://commits.kde.org/marble/9c43b4403851b16829ce32533f9aa4be7ae99438'>Commit.</a> </li>
<li>Use QStringLiteral with more contains(QString) methods. <a href='http://commits.kde.org/marble/5afd12d10b9b4d6ad9c279346429814f64b9eb32'>Commit.</a> </li>
<li>Use QStringLiteral for errorstring templates. <a href='http://commits.kde.org/marble/5ab8e573f1d5613437d62aa63efa294529900df2'>Commit.</a> </li>
<li>Use QStringLiteral with more value(QString) methods. <a href='http://commits.kde.org/marble/08609a15d8fdad2b41bc8a9494bc9d873c8f22db'>Commit.</a> </li>
<li>Use QStringLiteral for runtimeTrace expressions. <a href='http://commits.kde.org/marble/81a296d856c156626172b028efedfbc1d1849003'>Commit.</a> </li>
<li>Use QStringLiteral for RenderState ids. <a href='http://commits.kde.org/marble/0fceeff076e15a42ad19c7d8a63d1a11d8824e45'>Commit.</a> </li>
<li>Don't use physical widths if zoom level becomes < 0 (for whatever reason). <a href='http://commits.kde.org/marble/12d11ab73819d1f48e95416e02c003056200bf60'>Commit.</a> </li>
<li>Use the full power of StyleBuilder::determineVisualCategory() to determine visual category. <a href='http://commits.kde.org/marble/88dc5fb3608961b6cd17876ea92f3915c36f2df3'>Commit.</a> </li>
<li>Use override keyword consistently. <a href='http://commits.kde.org/marble/e6e148ac2ab7094b42a01f9a18537446bc1f3014'>Commit.</a> </li>
<li>Avoid creating multiple string copies of new styleurls. <a href='http://commits.kde.org/marble/1a11520817ffeebbfc6c482eff1a5c5a019a9d81'>Commit.</a> </li>
<li>Use a string pool for sharing some string data on loading cache & osm/o5m. <a href='http://commits.kde.org/marble/690fcf380985c5fefbb8531fbeb54a1432b49044'>Commit.</a> </li>
<li>Use QLatin1String with QXmlStreamAttributes::value. <a href='http://commits.kde.org/marble/8b24749079ec9981c617f861cb7f3811e01d2cd2'>Commit.</a> </li>
<li>Osm reader: Use QStringLiteral for creating QString objects from raw strings. <a href='http://commits.kde.org/marble/468adca632813265130a7e37399ff20a7b6b0a41'>Commit.</a> </li>
<li>Kml reader: Use QStringLiteral for creating QString objects from raw strings. <a href='http://commits.kde.org/marble/dd5e1550ee69708a89c3601ab1577a7aa43fe729'>Commit.</a> </li>
<li>Gpx reader: Use QStringLiteral for creating QString objects from raw strings. <a href='http://commits.kde.org/marble/a14cab5883010d9e251de26e96cbe079550369fe'>Commit.</a> </li>
<li>Set min versions for ECM & KF5. <a href='http://commits.kde.org/marble/4901ab6fd295dfa379d4adf24de7c1efd86a139a'>Commit.</a> </li>
<li>Let ECM not mess with normal install path, use KDE_INSTALL_DIRS_NO_DEPRECATED. <a href='http://commits.kde.org/marble/01d583e719c247a43b71624c3f658d2656f949a6'>Commit.</a> </li>
<li>Use setDesktopFileName for marble-qt. <a href='http://commits.kde.org/marble/ee6ccd8fa2f1ca273269c1b4f02999ee716b0af6'>Commit.</a> </li>
<li>Add reversed dns naming prefix to marble-qt.desktop. <a href='http://commits.kde.org/marble/173cd3929007d49805009c053200ba8dbfc543ed'>Commit.</a> </li>
<li>Fix setting the min Qt version + set also for non-Android builds to 5.3.0. <a href='http://commits.kde.org/marble/2ac4cf6fb0bfa685ee961682ca7eda2b32039186'>Commit.</a> </li>
<li>SetDesktopFileName for marble-maps. <a href='http://commits.kde.org/marble/0779aa8abcd32e3e34eed90fdaf97943aa33d23f'>Commit.</a> </li>
<li>Remove unused and wrong windowTitle property. <a href='http://commits.kde.org/marble/51b2c49ae8594f390ec76b15c944982f663bc851'>Commit.</a> </li>
<li>Set proper titles to config dialogs of Overview Map & Earthquackes plugins. <a href='http://commits.kde.org/marble/48389cf666961d2735257a30932b288e9719dc53'>Commit.</a> </li>
<li>Fix more dialog window titles: do not prepend ourselves the appname. <a href='http://commits.kde.org/marble/cb42ad2af6f9fc923dad34f08b689825009312d3'>Commit.</a> </li>
<li>Make marble-qt find the KDE-translator/packaging based qm catalog files. <a href='http://commits.kde.org/marble/7bd7321e2f6a55cccb51b13bcb642ce0093972d1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367781'>#367781</a></li>
<li>Marble-qt: Make more main submenu titles translatable. <a href='http://commits.kde.org/marble/daf43a0f9d03193e3e920c1b920783292a87ec78'>Commit.</a> </li>
<li>Qt::transparent instead of string "transparent" to create transparent color. <a href='http://commits.kde.org/marble/b60e65a85134ef52d4ec8da3a539b6c41cb608d6'>Commit.</a> </li>
<li>GeoPainter::drawPolyline: Use painter font property for labels, not some arg. <a href='http://commits.kde.org/marble/a50d67340bf1ae9314e7c27eb23506d21e1277e7'>Commit.</a> </li>
<li>Add GeoPainter::drawPolyline overload for linestrings without labels. <a href='http://commits.kde.org/marble/fb24641f79c48609c6dcc879f55184447e4b23b7'>Commit.</a> </li>
<li>Fix regression: some groundlayers were being painted over structures. <a href='http://commits.kde.org/marble/56fcd260014ddcc7b9b62116bbd11bd8340d6c90'>Commit.</a> </li>
<li>Normalize default/dummy sans serif font name to "Sans Serif". <a href='http://commits.kde.org/marble/d094e377dc78bf7107d25e74b8977b355709541c'>Commit.</a> </li>
<li>Remove declaration of unimplemented method. <a href='http://commits.kde.org/marble/eef771a4f967513045e13faaedd0864cacffc847'>Commit.</a> </li>
<li>Concentrate OSM styling in StyleBuilder by merging OsmPresetLibrary. <a href='http://commits.kde.org/marble/1ae9675bea36b120d2244f84cc737627e47e1f87'>Commit.</a> </li>
<li>Adapt to size of parent widget. <a href='http://commits.kde.org/marble/71e501c4b781f4c9ddcb901882355d220f4e4702'>Commit.</a> </li>
<li>Return OsmPlacemarkData rather than suitable tag. <a href='http://commits.kde.org/marble/98a8173678a6f1dd8c63315584e9d1c7d60e5de8'>Commit.</a> </li>
<li>--code duplication. <a href='http://commits.kde.org/marble/bc13e500f7a12d14e6d5300591964882e1d53a9b'>Commit.</a> </li>
<li>Remove unused variable. <a href='http://commits.kde.org/marble/8938f293ff132e9a680f4715b7edd36b9750c3c5'>Commit.</a> </li>
<li>Remove unneeded includes. <a href='http://commits.kde.org/marble/dbae5a410cb08ffdb0ca8b00bc4ca9378175df41'>Commit.</a> </li>
<li>Use QLatin1String with QXmlStreamAttributes::value. <a href='http://commits.kde.org/marble/12e77e9929aa9758a830ab46d333510e43df4bd8'>Commit.</a> </li>
<li>QString::replace operates on the object itself, no need to reassign to object. <a href='http://commits.kde.org/marble/b154432d567f9917d4a86159ebddf3abc9378057'>Commit.</a> </li>
<li>Fill turnTypePixmap map only once. <a href='http://commits.kde.org/marble/bf0f04500555c4ad33116a582fb84150cd53c19c'>Commit.</a> </li>
<li>Use type const char[] instead of const char* for const string "variable"s. <a href='http://commits.kde.org/marble/6bb24934a6cdb3212f35c078087ec2004549b517'>Commit.</a> </li>
<li>Use QLatin1String with QString::contains/startsWith/endsWith/indexOf/compare. <a href='http://commits.kde.org/marble/39d2b3d5e31425813c1ac0e159325bc70f397665'>Commit.</a> </li>
<li>Use QHash, not QMap, for quicker access to OSM visual categories by tag. <a href='http://commits.kde.org/marble/2192d61c101bee9da808005398bc9f7fd1f2e38c'>Commit.</a> </li>
<li>Use QLatin1Char with simple chars. <a href='http://commits.kde.org/marble/676288ab8749a2966eb3d487ec76ce58ff8b6f2d'>Commit.</a> </li>
<li>Use QLatin1Char in string calculation expressions. <a href='http://commits.kde.org/marble/598dc83ea6c51ad172088e150c7bfd538aedcb9e'>Commit.</a> </li>
<li>Make use of OsmPlacemarkData. <a href='http://commits.kde.org/marble/5c53d7fc46c6dbd0de2aa2839d60f03ffb27d582'>Commit.</a> </li>
<li>Use QLatin1String in string calculation expressions. <a href='http://commits.kde.org/marble/5b6f30c7d24825c30cbab7b015ccbc06ebb3728f'>Commit.</a> </li>
<li>Use QStringLiteral with QIcon ids. <a href='http://commits.kde.org/marble/e25f8b7b92e3c5ade103d99c8e5c9b7878c6f041'>Commit.</a> </li>
<li>CompassFloatItem: avoid multiple assignment, "" -> QString(). <a href='http://commits.kde.org/marble/61da9616b85a53203a11417c96ff0028d6d4880b'>Commit.</a> </li>
<li>No need to pass empty string to QColor constructor to get invalid color. <a href='http://commits.kde.org/marble/c888313dbc82487619862e1e80664d4bded9791e'>Commit.</a> </li>
<li>No need to explicitely set empty title in QMenu constructor call. <a href='http://commits.kde.org/marble/2e87fe9535d393935dc910282345b185148607b7'>Commit.</a> </li>
<li>No need to initialize QStrings with "". <a href='http://commits.kde.org/marble/6547e5a1102414552701035a35a163a44c7342cb'>Commit.</a> </li>
<li>Use QString(), not "". <a href='http://commits.kde.org/marble/bed44d4640a452924bb6c103963204b5d456df9b'>Commit.</a> </li>
<li>Fix missing init of GeoDataPolygon::renderOrder, use 0. <a href='http://commits.kde.org/marble/a0e5ecab0e4f820efbebde207705b772fb07fe26'>Commit.</a> </li>
<li>In GEODATA_DEFINE_TAG_HANDLER use QLatin1String for the raw strings. <a href='http://commits.kde.org/marble/c220d895afff57bc30aa3a68fbcf04fd3d69eced'>Commit.</a> </li>
<li>No need to construct string of whitespaces manually. <a href='http://commits.kde.org/marble/00cece9fa534c10f7e6fcc54838b81e6ef2469b6'>Commit.</a> </li>
<li>AzimuthalProjection: Avoid duplicated globeHidesPoint calculation in iteration. <a href='http://commits.kde.org/marble/64efed1e3ba2c645e3d322ff846901019e0b0f20'>Commit.</a> </li>
<li>Fixed: marble-kde app commandline arg "latlon" had help text mixed up. <a href='http://commits.kde.org/marble/deaf4ab53b686694e5dae324cc58b1c6860ce037'>Commit.</a> </li>
<li>Map to OSM visual categories. <a href='http://commits.kde.org/marble/ce8c3943ed5f82e8af12a2f3ba3a6362a392eb6e'>Commit.</a> </li>
<li>No else statement after an if statement with return. <a href='http://commits.kde.org/marble/88ed905c59bec5c587336725489bca2a662b37ee'>Commit.</a> </li>
<li>Preserve the "ele" tag. <a href='http://commits.kde.org/marble/21034cb5cb86e8e5ec245043737177cd57da18e8'>Commit.</a> </li>
<li>Have OSM styling determine the visibility of OSM placemarks. <a href='http://commits.kde.org/marble/9bb72adddb37dc709e1804feec891bd4fdcb05a0'>Commit.</a> </li>
<li>Proper parsing for polygons and their inner rings when cutting to tiles. <a href='http://commits.kde.org/marble/ff63d11da85af11795977c5154f9580f5ef0d534'>Commit.</a> </li>
<li>Change !x.isEmpty() instead of x != QString(""). <a href='http://commits.kde.org/marble/b52b6e3d3d7f5c31c2a0097084ff2215979ded34'>Commit.</a> </li>
<li>Use QLatin1String and QCOMPARE for raw string tests. <a href='http://commits.kde.org/marble/5212b98574194ee349d2444cc04f6f6f5b58ab5a'>Commit.</a> </li>
<li>Use startsWith() instead of mid() and complete comparison. <a href='http://commits.kde.org/marble/0823d5a71d4c4591ae6e876539b9a248bdf00b8f'>Commit.</a> </li>
<li>Use more QLatin1String for comparing raw strings to QStrings. <a href='http://commits.kde.org/marble/688f32eae1bf32f64de70e78406940dae4403574'>Commit.</a> </li>
<li>Use QLatin1String with assert on tag in GeoTagHandler::parse() reimplementations. <a href='http://commits.kde.org/marble/76bc83e65ae395f26a0ae5b6950787d58a6198c6'>Commit.</a> </li>
<li>Use more QStringLiteral for plugin metedata strings. <a href='http://commits.kde.org/marble/9ea3604dd4dd952d1f1d9d8256073a0911894220'>Commit.</a> </li>
<li>Use existing QVariant constructors instead of QVariant::fromValue. <a href='http://commits.kde.org/marble/4b3a1b84c44293c63a62252259b7f3f39d33f931'>Commit.</a> </li>
<li>Use more QStringLiteral for raw strings. <a href='http://commits.kde.org/marble/4eb9d675faba8cc872c13c8f6f611fc48364985f'>Commit.</a> </li>
<li>Use more QLatin1String for comparing raw strings to QStrings. <a href='http://commits.kde.org/marble/ebc5572a3f7b4fc4491f8950c75a934499708482'>Commit.</a> </li>
<li>Adding more points to the edges of the generated tiles. <a href='http://commits.kde.org/marble/65afbafa914a0e98b73d7ad24e5383ab4e4793ea'>Commit.</a> </li>
<li>Use -DQT_NO_CAST_TO_ASCII. <a href='http://commits.kde.org/marble/df03e7af538da9c1194f24fc0fdd63580ac60754'>Commit.</a> </li>
<li>Use -DQT_NO_CAST_FROM_BYTEARRAY (and adapt some code). <a href='http://commits.kde.org/marble/0e144849eb2f8918c3a2d6e91aee2c84aea787a8'>Commit.</a> </li>
<li>Add TileIterator to simplify traversing region extracts in the future. <a href='http://commits.kde.org/marble/570a430b12409a5462554c35fda2bcf51576d14d'>Commit.</a> </li>
<li>Fix confusing parameter name. <a href='http://commits.kde.org/marble/f006fcb9b2d928b7cab555d595cbcc14f1bc0d47'>Commit.</a> </li>
<li>Restore GeoDataFeature context for translations. <a href='http://commits.kde.org/marble/e2adf41777f898ca82c2fea2fb424d012ab85b42'>Commit.</a> </li>
<li>Group OpenStreetMap categories together. <a href='http://commits.kde.org/marble/b1edb9c3efccedf75820979e508ea6d609fd61d5'>Commit.</a> </li>
<li>No need for indirection. <a href='http://commits.kde.org/marble/5d68df254ea95bbc09d2eea62bb2f1391870b7cb'>Commit.</a> </li>
<li>GeoLineStringGraphicsItem: query properties/objects in methods only once. <a href='http://commits.kde.org/marble/6f4e8cc757b6cf19dba652d1e6b775fad26bda47'>Commit.</a> </li>
<li>Local static variable was meant to be const, of course. <a href='http://commits.kde.org/marble/6a69c19b2d39392ecba99bea63735a6a0781184e'>Commit.</a> </li>
<li>Move additional OSM tags to less prominent place. <a href='http://commits.kde.org/marble/7f47e23a53ab93e19a8368bf92d9b4f699e09241'>Commit.</a> </li>
<li>Remove convenience method that was used only once. <a href='http://commits.kde.org/marble/daf5d5405c4f944c0f64fd1d1eb8c2e9be3842e7'>Commit.</a> </li>
<li>Remove convenience method that was used only once. <a href='http://commits.kde.org/marble/2f6f22988b14810b25973e4be57de5f930bef8c9'>Commit.</a> </li>
<li>Use OsmTag rather than plain QString (which needed to be in a certain format). <a href='http://commits.kde.org/marble/815379faeb5fd788ea41f60e605d08c48cf3b4a4'>Commit.</a> </li>
<li>No need to compute category twice. <a href='http://commits.kde.org/marble/2c8f7e90ef215b85fdd70f868419a69cc5c14cc7'>Commit.</a> </li>
<li>Simplify. <a href='http://commits.kde.org/marble/21e3f103ca388e7925ea42500e386e6d6f32fbe4'>Commit.</a> </li>
<li>Avoid redundancy. <a href='http://commits.kde.org/marble/edff1b3a14c15d4dc6e7590d2718fa20b82367ea'>Commit.</a> </li>
<li>Render aeroway apron areas. <a href='http://commits.kde.org/marble/147e58f103ddfc139914f72db230cb1b34735ca9'>Commit.</a> </li>
<li>Improve node only vs node or ring vs node or area category styles. <a href='http://commits.kde.org/marble/32f677a37c20c429cfa1bff4b7b638c1887d519b'>Commit.</a> </li>
<li>Developer oriented tools need to translation. <a href='http://commits.kde.org/marble/df848e57be6b442681faf4377541fd48d49b75af'>Commit.</a> </li>
<li>Allow choosing a different file format than osm xml. <a href='http://commits.kde.org/marble/b139ca1a8f6e6d9be385b17b80ea6022ac5fb410'>Commit.</a> </li>
<li>Remove unused method. <a href='http://commits.kde.org/marble/9d00e02fb961c0803e5e091d2b0826362e8619b2'>Commit.</a> </li>
<li>Remove unused methods. <a href='http://commits.kde.org/marble/0d5f226eea0a93e81ae4650590c7f0cc529d1225'>Commit.</a> </li>
<li>Mark ctors with single argument explicit. <a href='http://commits.kde.org/marble/813ce13210a084bb9d371c48a5bbb100dd133a94'>Commit.</a> </li>
<li>Unify ctors, make explicit. <a href='http://commits.kde.org/marble/7b63a2e6b444c53153e4ba6aaeeacb49dc519949'>Commit.</a> </li>
<li>Add copyTags method to avoid redundancy. <a href='http://commits.kde.org/marble/ab548999e35929d570f268ef924ef67bad3e06e9'>Commit.</a> </li>
<li>Unbreak Android build: set VERSION/SOVERSION on Android, needed for cmake config. <a href='http://commits.kde.org/marble/bc4e5df8650eebc8938e077d42cf9110469d451b'>Commit.</a> </li>
<li>Do not set (wrong) visible and josm specific action attributes. <a href='http://commits.kde.org/marble/ae284e7446ac2bb5feb7d298f8992425196b6e58'>Commit.</a> </li>
<li>Add missing category name. <a href='http://commits.kde.org/marble/3f5a4bc4b9da2f603193827ba7fe6b60ea3af5a5'>Commit.</a> </li>
<li>Highways do not need icons, so remove them altogether. <a href='http://commits.kde.org/marble/04a1e23c2a21d2b25cf6ed1ee016da71b5201bd0'>Commit.</a> </li>
<li>SunLightBlending: calculate longitudes iteratively in loop, not from scratch. <a href='http://commits.kde.org/marble/b1f68ad53f92ea64321f8ff4197d80b101c9d199'>Commit.</a> </li>
<li>Do bound check on other side only if still needed. <a href='http://commits.kde.org/marble/a92c7254e1a6d13bb8cebe5f73b7f430ee53b008'>Commit.</a> </li>
<li>Speedup EmbossFifo by using the CPU for shifting the queue. <a href='http://commits.kde.org/marble/ad5d3fc8990d002c56b4d3477e4ccd04359cebb9'>Commit.</a> </li>
<li>SphericalProjection::screenCoordinates(): query viewport properties only once. <a href='http://commits.kde.org/marble/06dbad8b4398bb6b8c6c37d6a96ccf47ca9ca867'>Commit.</a> </li>
<li>Implemented polygon cutting based on the Weiler-Atherton polygon clipping algorithm. <a href='http://commits.kde.org/marble/342aa8041cce2e3f5aa38eab00b62934b313b02c'>Commit.</a> </li>
<li>Fix typo. <a href='http://commits.kde.org/marble/dc4bf4b2b7b11a63379d3b41c6a068c460c29f5a'>Commit.</a> </li>
<li>Render aeroway taxiways. <a href='http://commits.kde.org/marble/bcf1f53b06214a45d27e731d951d14a4fd29dc3d'>Commit.</a> </li>
<li>Extract width of areoway runways also. <a href='http://commits.kde.org/marble/83ee05e30e0f6d6ce4799194e20feca782b0a365'>Commit.</a> </li>
<li>Extract explicit width values for highways. <a href='http://commits.kde.org/marble/f3fce78606547afdb5dcd83edbc0305a2023c709'>Commit.</a> </li>
<li>Do not fill POI styles. <a href='http://commits.kde.org/marble/8ec326688e2fd68b9179bb5da538560515dae340'>Commit.</a> </li>
<li>Use "else" with alternatives on fine-tuning zoomlevel in FileLoader. <a href='http://commits.kde.org/marble/c2664c90bd331b76a64174951cb8255a52f4e3b0'>Commit.</a> </li>
<li>FileLoader: cache placemark->role() and use QLatin1String for comparing. <a href='http://commits.kde.org/marble/8a7167afce107843e44bfa7d767e26d0241d494b'>Commit.</a> </li>
<li>Have SunLocator cache twilightZone instead of estimating it on every call. <a href='http://commits.kde.org/marble/7675aceee7fe7bf6545483647f0ff268ee6bb87e'>Commit.</a> </li>
<li>Plasma wallpaper/worldclock: add option to center on current location. <a href='http://commits.kde.org/marble/44d080ff9b218e22e745ff002c4474b9bea1ceed'>Commit.</a> </li>
<li>Plasma wallpaper/worldclock: rename fixedLongitude <- centerLongitude. <a href='http://commits.kde.org/marble/36c94290337a4fc3682f36986bfc5223714f2dec'>Commit.</a> </li>
<li>Plasma applet/wallpaper only need KF5::Plasma (for cmake macros at least). <a href='http://commits.kde.org/marble/29a0207f884448913924092aebc99dd46b79bb77'>Commit.</a> </li>
<li>Plasma wallpaper/worldclock: add option to center on longitude. <a href='http://commits.kde.org/marble/9b6dcacf3277d1f3d9eff5548d03f77cbd41600a'>Commit.</a> </li>
<li>Plasma wallpaper/worldclock: ensure map centered on equator vertically. <a href='http://commits.kde.org/marble/2523d9b37b22f0f7db1c5ea5e52583052b7f1272'>Commit.</a> </li>
<li>Fix build-ability of installed examples. <a href='http://commits.kde.org/marble/690b4afe03c8e3828d2367f89e0f4ab43ddb313c'>Commit.</a> </li>
<li>Work on the correct placemark data instance. <a href='http://commits.kde.org/marble/32abd75af82bb668aea837930185f9aae4398a66'>Commit.</a> </li>
<li>Also initialize LinearRing type geometry. <a href='http://commits.kde.org/marble/ea73532eb1ca715d57cc184dcc5fabda9adaab6e'>Commit.</a> </li>
<li>Disable ATM wrong note in build log about Marble's Qt declarative plugins. <a href='http://commits.kde.org/marble/0e5fcbef96ca3cb6c3f60c17d669809030f8a1bf'>Commit.</a> </li>
<li>Remove sources of old plasma4 worldclock plasmoid. <a href='http://commits.kde.org/marble/858d80c6f62dbf38c10fa0d4d29679d9122d4065'>Commit.</a> </li>
<li>Add a visual category for aeroway runways, extract and render them in Vector OSM. <a href='http://commits.kde.org/marble/f4ae0754dead3039f4663b664dda64301faad0ec'>Commit.</a> </li>
<li>Metainfo.yaml: Declare Marble cmake config and target names. <a href='http://commits.kde.org/marble/2af89ea6a9f6ea35906c78025f3f4344b88460b0'>Commit.</a> </li>
<li>Remove unneeded use of MARBLE_EXPORT in example marble-game. <a href='http://commits.kde.org/marble/2fce765065a3367830de6940eb1d9f7851904591'>Commit.</a> </li>
<li>Do not install marbleQuick2 example while marbledeclarative is private. <a href='http://commits.kde.org/marble/7ed5da2ee223a61e3d279ae9f40688089b3232bc'>Commit.</a> </li>
<li>Remove unused lib from marbleQuick2 example link list. <a href='http://commits.kde.org/marble/42f0f37b88e70e01351af80856467079f405d42d'>Commit.</a> </li>
<li>Use "marblewidget" target again instead of var ${MARBLEWIDGET}. <a href='http://commits.kde.org/marble/67e3229152d55488d4af58010f1e65b0dc5570c7'>Commit.</a> </li>
<li>Use -DQT_USE_QSTRINGBUILDER. <a href='http://commits.kde.org/marble/56615a95a71cd9140cfe06aa2b420a91bdeb98f1'>Commit.</a> </li>
<li>Sample FindMarble.cmake no longer needed. <a href='http://commits.kde.org/marble/ac08536d260454f952dba2e0d1bd726855c4c655'>Commit.</a> </li>
<li>Use -DQT_STRICT_ITERATORS. <a href='http://commits.kde.org/marble/3099e6122fa4539bcc2d07790c6021493754e726'>Commit.</a> </li>
<li>Update elevplacemarks.kml: newer heights & latlon, Mount McKinley->Denali. <a href='http://commits.kde.org/marble/5a62ac9d5cc2c89d89526564ff83f7cbef5719b9'>Commit.</a> </li>
<li>Create and install CMake Config file for marble & astro libraries. <a href='http://commits.kde.org/marble/b40c2cba738dc1a659440587939791b7ffcbb138'>Commit.</a> </li>
<li>Basic O5M writer. <a href='http://commits.kde.org/marble/a6ce72f9d6002a644c9e684cde6e617cf79b623b'>Commit.</a> </li>
<li>Separate the amenity and shop details in the Android version's placemark dialog. <a href='http://commits.kde.org/marble/9cf50760efcea2c3ecb75a81b868424b91782f62'>Commit.</a> </li>
<li>Use GeoDataDocumentWriter instead of GeoWriter. <a href='http://commits.kde.org/marble/0448670d4325ab96cc24ae24b5b3fea1d90328e4'>Commit.</a> </li>
<li>Generalize GeoWriter to support non-XML formats. <a href='http://commits.kde.org/marble/99c25c19efbb3080a3d8b2dff4010eafc8ae310e'>Commit.</a> </li>
<li>Do not sneak in a name tag on writing. <a href='http://commits.kde.org/marble/fc42208f6d7cded4a314c6cdd809f86979f4ed0e'>Commit.</a> </li>
<li>Initial rewrite of worldclock plasmoid and a new worldmap plasma "wallpaper". <a href='http://commits.kde.org/marble/cf4f8af7476b725aacf32f9443c0e971e8b08ab8'>Commit.</a> </li>
<li>Show Shop info, or if not present, amenity info, in the placemark dialog. <a href='http://commits.kde.org/marble/5752f751d64fa31515610115951c1127377a9a6f'>Commit.</a> </li>
<li>Do not fill place areas, just show their label. <a href='http://commits.kde.org/marble/db22e5607162da786e29146d2e690c79850d5359'>Commit.</a> </li>
<li>Less prominent power towers. <a href='http://commits.kde.org/marble/2f8e6aae7cfbeb5f032e479d25ee4145171b7246'>Commit.</a> </li>
<li>Config option for transaction commit interval. <a href='http://commits.kde.org/marble/1079ee4911712504c2fdffca29db25a21c1dcc54'>Commit.</a> </li>
<li>Update KDescendantsProxyModel::setSourceModel to KF5 pre-5.25. <a href='http://commits.kde.org/marble/0957c91931e53e2449ba8388c273519d9959ddb8'>Commit.</a> </li>
<li>Remove some unneeded includes. <a href='http://commits.kde.org/marble/e0090a2ad72e51252b5eac7b03b378595516408e'>Commit.</a> </li>
<li>First half-done approach to Satellites settings mess. <a href='http://commits.kde.org/marble/61816f122208315d208cd1d02930d39c0c830603'>Commit.</a> </li>
<li>Reduce progress output frequency. <a href='http://commits.kde.org/marble/513c946f2a61fabe4e57ef6ce5c836fed1511ad9'>Commit.</a> </li>
<li>Commit each 10000 tiles. <a href='http://commits.kde.org/marble/eada1f585ce2db374e51d27e269ca924075d9c52'>Commit.</a> </li>
<li>Report tile count in progress. <a href='http://commits.kde.org/marble/be65f00925d916ed794e5bdea9996446f2c8f537'>Commit.</a> </li>
<li>Add a tool to convert z/x/y tile directories to a .mbtile database. <a href='http://commits.kde.org/marble/0696a4c5d3b665895b0c2c6db28b822bbd290b4d'>Commit.</a> </li>
<li>Add airport gate translation. <a href='http://commits.kde.org/marble/a03f06194b8686361a2a01e5b8747b316c831ef8'>Commit.</a> </li>
<li>Only export symbols from the libraries that should be exported. <a href='http://commits.kde.org/marble/b5dc725832839986048f64ced12c6491cdb954a1'>Commit.</a> </li>
<li>Remove methods which were deprecated since 2011/2012. <a href='http://commits.kde.org/marble/6aab8e9ddfe7698f685058310c56120005d95f83'>Commit.</a> </li>
<li>Show gates at airports. <a href='http://commits.kde.org/marble/23fb2b7e1dc8e6285185582abf18d15e9eff25bb'>Commit.</a> </li>
<li>For clothes shops, show which clothes they sell as well,. <a href='http://commits.kde.org/marble/a7ff38fe01b94bf6aa97bc4d3d37e271a26d5439'>Commit.</a> </li>
<li>More generic namespace for plugins:  org.kde.marble <- org.kde.edu.marble. <a href='http://commits.kde.org/marble/e32dad6580146e0c34ac25ccdcd443b6b9a9c134'>Commit.</a> </li>
<li>Show elevation of peaks in the placemark dialog in the Android version. <a href='http://commits.kde.org/marble/b73527b1f6e513996fddbdb1f2ea51611d958de3'>Commit.</a> </li>
<li>Export MarbleWidgetInputHandler symbols. <a href='http://commits.kde.org/marble/ba93df952bc831f39b5b85e14c0be6fced1de634'>Commit.</a> </li>
<li>Satellites: Do not trigger default setting in constructor to avoid bad data. <a href='http://commits.kde.org/marble/5db42c6455fc9bfaf32242dc897ee654f906c030'>Commit.</a> </li>
<li>Fix dialog window titles: do not append ourselves the appname. <a href='http://commits.kde.org/marble/af52863dea16e9255d0173e024847b59f7309f4b'>Commit.</a> </li>
<li>Pass QHash<QString,QVariant> as const ref argument. <a href='http://commits.kde.org/marble/6fee682460dcd5197c4867f0d721a8dedb3562ee'>Commit.</a> </li>
<li>Warn about out-dated content. <a href='http://commits.kde.org/marble/a111b7ee89986d87f2d0d0519fe0222c035f49eb'>Commit.</a> </li>
<li>Remove left-over mentioning of MarbleControlBox and NavigationWidget. <a href='http://commits.kde.org/marble/8fde5cbb88e5fac0da821937cd1361451afc87e4'>Commit.</a> </li>
<li>Move categoryName() out of declarative/Placemark.cpp into GeoDataFeature.cpp. <a href='http://commits.kde.org/marble/f82e3a66842a493ca1c49666572ab5ceef6c4f97'>Commit.</a> </li>
<li>Add metainfo.yaml for kapidox/api.kde.org. <a href='http://commits.kde.org/marble/9a0f9a1837f81e1472db99aa9dfd5d55a36719ec'>Commit.</a> </li>
<li>Use CamelCased SharedMimeInfo_FOUND. <a href='http://commits.kde.org/marble/098fc9394ee217858c9296ac4c40b6d6b354d9b5'>Commit.</a> </li>
<li>GeoPainter: consistent qreal-based API, separate regionFromPixmapRect(). <a href='http://commits.kde.org/marble/c18308456d909ef576b53ab69da890ab8b32ab5e'>Commit.</a> </li>
<li>Marble-qt: warn about render plugins whose actions were not added to a menu. <a href='http://commits.kde.org/marble/f7cf8cf819bf8c044eb6b793985f31e642a4f664'>Commit.</a> </li>
<li>Unbitrot TestPlugin. <a href='http://commits.kde.org/marble/f01d9a3e887c1d86bbaf9b0d87471f770537f9b7'>Commit.</a> </li>
<li>StarsPlugin: fix states of context menu. <a href='http://commits.kde.org/marble/524293b025dc1339ed58d4115d428fbd07cbe1e2'>Commit.</a> </li>
<li>Avoid that labels repeat extremely often when the text is short. <a href='http://commits.kde.org/marble/15f981159cdf90b6c7b4a5b54996db9142d26bb8'>Commit.</a> </li>
<li>When a highway has no name, use ref instead. <a href='http://commits.kde.org/marble/f9efd262efff5390012788fc8bbf9e5f4319e001'>Commit.</a> </li>
<li>Don't use protected member variables. <a href='http://commits.kde.org/marble/32c48f393b2ccd412295b612dc7b02966ebe3635'>Commit.</a> </li>
<li>Use QVector<GeoDataPlacemark*> instead of QList<GeoDataObject*>. <a href='http://commits.kde.org/marble/8b0a09cb1bc0391949db8e740775a7c254878f3b'>Commit.</a> </li>
<li>Fix order of node, way and relation blocks generated by shp2osm script. <a href='http://commits.kde.org/marble/8907b7e525ccb926c3fbaac1cccd3498cdd224d0'>Commit.</a> </li>
<li>Make a special dedicated popup for OpenStreetMap data in the Desktop version. <a href='http://commits.kde.org/marble/4213c996dba39106b6b0b05208c362efb2846f2e'>Commit.</a> </li>
<li>Bump Android version number. <a href='http://commits.kde.org/marble/0f61ad6bd9c22cf92288b6cda3e926f4db35d7b7'>Commit.</a> </li>
<li>Do not package unused plugins. <a href='http://commits.kde.org/marble/1930127f2687cd5b13d8882dee180a9b977befb2'>Commit.</a> </li>
<li>Fix plugin upgrade problem on Android. <a href='http://commits.kde.org/marble/ce202433b9255b32f69511ef5f69c2596bdaf3ec'>Commit.</a> </li>
<li>Bump Android version number and target SDK. <a href='http://commits.kde.org/marble/381aab379e5a95698f906748ad28602f0aa7c043'>Commit.</a> </li>
<li>Improve area vs linestring classification. <a href='http://commits.kde.org/marble/6f7e3fbd0b9e7f99882fde9155871ff46870efff'>Commit.</a> </li>
<li>Improve rendering of pedestrian areas. <a href='http://commits.kde.org/marble/afab421dc32c429484ff2cf46fc66bbbe4683441'>Commit.</a> </li>
<li>Simplify area determination: All landuse is area. <a href='http://commits.kde.org/marble/84f64ecc3216eef582a4896b50e4e80df1eb2b9a'>Commit.</a> </li>
<li>Render amenity=shelter. <a href='http://commits.kde.org/marble/59fb06dbcbe697c5515818e85dc40dba737e42b7'>Commit.</a> </li>
<li>Hide unknown polygons. <a href='http://commits.kde.org/marble/942cd301687de253911460c67a8eb55069c578d8'>Commit.</a> </li>
<li>Recreation grounds are areas. <a href='http://commits.kde.org/marble/5a221b82395a3f1447afbc78f601696c92e55467'>Commit.</a> </li>
<li>Nicer wall barrier styling. <a href='http://commits.kde.org/marble/f6629face828d374598800b306d72c2e46f02cd0'>Commit.</a> </li>
<li>Hide unknown ways. <a href='http://commits.kde.org/marble/c1fab1897e2362841f14e03ba330d103668b39b9'>Commit.</a> </li>
<li>Improve dialog height. Always show bookmark toggle icon. <a href='http://commits.kde.org/marble/0079084a100fd1f3892b7d4ccbc39be886390d3a'>Commit.</a> </li>
<li>Fix URL opening. Condensed mode toggle on any tap. <a href='http://commits.kde.org/marble/999a0ad04fa9575cd4e89f1dcaa1b61efb216bc3'>Commit.</a> </li>
<li>Enable selection of buildings. <a href='http://commits.kde.org/marble/70200bbeb53f7620e4138171e89e3ca34fd819c6'>Commit.</a> </li>
<li>Const correct osm placemark data access. <a href='http://commits.kde.org/marble/47b59f9f029eabb60342a166f6621b8c040abda0'>Commit.</a> </li>
<li>Add whichBuildingAt with a similar purpose to whichFeatureAt. <a href='http://commits.kde.org/marble/0a8a426f05e364501ff3feb1580a4d31b138da75'>Commit.</a> </li>
<li>Basic support for creating bookmarks. <a href='http://commits.kde.org/marble/37a937f791d892728838068212d9bd4c85ee1ee5'>Commit.</a> </li>
<li>Fix rad/deg confusion. <a href='http://commits.kde.org/marble/4901acb72ff16233b349cd5909525159140d3653'>Commit.</a> </li>
<li>Add 200m and 4000m bathymetries, level 3 onwards. <a href='http://commits.kde.org/marble/5c59300a756a0a1f60d9a7bafe3dba9d36519776'>Commit.</a> </li>
<li>Fix KeyError: 'depth' in shp2osm script. <a href='http://commits.kde.org/marble/849f4bfbefcd42c2f43afc7f884252c91ed97365'>Commit.</a> </li>
<li>Show coordinate and address if there is no name. <a href='http://commits.kde.org/marble/f995bb5a36b4f942a4315e512fd0d6a6245c64d0'>Commit.</a> </li>
<li>Update bookmarks model when a different kml file is loaded. <a href='http://commits.kde.org/marble/3471752a13dfb9ce4c368f6437911ed854f949d1'>Commit.</a> </li>
<li>Expose longitude and latitude properties to QML. <a href='http://commits.kde.org/marble/52144d888c6f4bcdcec4bcd258e1722160cb4f9e'>Commit.</a> </li>
<li>Ensure bookmarks have a name and icon. Use StyleBuilder internally. <a href='http://commits.kde.org/marble/77902db9a1a00c0043e811afc97ef3f513c1ff4f'>Commit.</a> </li>
<li>Enable bookmark support (load and show bookmarks). <a href='http://commits.kde.org/marble/a306e47ceb7b48ca48a1a3b28d0caade6994a81c'>Commit.</a> </li>
<li>Use a svg bookmark icon by default. <a href='http://commits.kde.org/marble/baab654c79faceedddd89298d22cae5d8c1d5419'>Commit.</a> </li>
<li>Return data for icon path role. <a href='http://commits.kde.org/marble/30c7eee07501688c0f32de3eeeaff63a9209e364'>Commit.</a> </li>
<li>Report additional role names. <a href='http://commits.kde.org/marble/93f97f5c3a20a54fe2ad4ec7eee994060c7bd2e3'>Commit.</a> </li>
<li>Fix anchor syntax. <a href='http://commits.kde.org/marble/4f54177f83d4847bbfb1e5ff34df59fe2b5973ef'>Commit.</a> </li>
<li>Trim whitespace in coordinate. <a href='http://commits.kde.org/marble/4515b913fecf5f38fa76abd25ac1e96181a9293f'>Commit.</a> </li>
<li>Set OSM tags from nominatim reverse geocoding results. <a href='http://commits.kde.org/marble/53eb1046b3647a9346c27f309c22e5bf9f5d4402'>Commit.</a> </li>
<li>Use reverse dns naming for appdata/deskop files for KDE Marble app. <a href='http://commits.kde.org/marble/843eb5d37f5ecb913b1bd74a9b4a048e031a2e97'>Commit.</a> </li>
<li>Modified vector rendering support for bathymetry. <a href='http://commits.kde.org/marble/d67eb06fc35d6678e84d5e022a1365c8c7c0af2b'>Commit.</a> </li>
<li>Keep niceIntervals as static list instead of creating it every time. <a href='http://commits.kde.org/marble/65c4af4a2a456767df247574b4803095c558a0bf'>Commit.</a> </li>
<li>QList<AxisTick> -> QVector<AxisTick>. <a href='http://commits.kde.org/marble/e22fc53e26db49788dde4f74ced3ffd851a6080a'>Commit.</a> </li>
<li>Group boolean members for improved byte layout. <a href='http://commits.kde.org/marble/357c2f923f5ba694e4f25c2da13db3ecb60bb19f'>Commit.</a> </li>
<li>QList<QImage> -> QVector<QImage>. <a href='http://commits.kde.org/marble/019df53b4e5aed978f686bda588c8383e71b950a'>Commit.</a> </li>
<li>QList<PolylineNode> -> QVector<PolylineNode>. <a href='http://commits.kde.org/marble/2d27a6eac09f5eaf5dfdabe6d82819b1d424fbce'>Commit.</a> </li>
<li>QList<MovieFormat> -> QVector<MovieFormat>. <a href='http://commits.kde.org/marble/671256d72267b88790fef1ecf94a8cf8cd6f6cca'>Commit.</a> </li>
<li>QList<NamedEntry> -> QVector<NamedEntry>. <a href='http://commits.kde.org/marble/50972f43c6fcca5d46a359ae971261d317b108c2'>Commit.</a> </li>
<li>QList<QRegion> -> QVector<QRegion>. <a href='http://commits.kde.org/marble/b84479c51ce875ca1be615a50d4afbad4defc491'>Commit.</a> </li>
<li>QList<QDateTime> -> QVector<QDateTime>. <a href='http://commits.kde.org/marble/e220d1365195e651e229f539a01ff2cc6992f261'>Commit.</a> </li>
<li>Remove unused var. <a href='http://commits.kde.org/marble/2cafd5626289defebff936ff3689a2beba87b7b4'>Commit.</a> </li>
<li>QList<QColor>  -> QVector<QColor>. <a href='http://commits.kde.org/marble/97664768ef88ef06c6e48056f7ca8b2e917c93c6'>Commit.</a> </li>
<li>QList<QSize> -> QVector<QSize>. <a href='http://commits.kde.org/marble/90621c451d55463d0b516f0340b3fabad8438008'>Commit.</a> </li>
<li>QList<QRectF> -> QVector<QRectF>. <a href='http://commits.kde.org/marble/096107254f80e56a7442716ca42ec6164fb5d0f9'>Commit.</a> </li>
<li>QList<QPointF> -> QVector<QPointF>. <a href='http://commits.kde.org/marble/640cf6e66f71859632e21da131e5546876487954'>Commit.</a> </li>
<li>QList<PluginAuthor> -> QVector<PluginAuthor>. <a href='http://commits.kde.org/marble/6a935c95acfe2e4d0f718131e9bf643b918d5c84'>Commit.</a> </li>
<li>Remove dead Q_EXPORT_PLUGIN macros, no longer used in Qt5. <a href='http://commits.kde.org/marble/cae89b95ef6c6f71a978a1db342dc0b525604a6f'>Commit.</a> </li>
<li>Partly revert 43f3168 to keep memory usage low. Needs a proper fix. <a href='http://commits.kde.org/marble/913caa81077781479f6eae0bcd70b00f79c2891b'>Commit.</a> </li>
<li>InitializeOsmData sets default values, so ignore these. <a href='http://commits.kde.org/marble/0fe7cd6cc082a524b9c59916ead14b3e16293f64'>Commit.</a> </li>
<li>Adding a preliminary template osm web popup file for fewcha. <a href='http://commits.kde.org/marble/cd3fb3492c438745295473bd401c38e64d0a8f01'>Commit.</a> </li>
<li>Added vector rendering support for bathymetry. <a href='http://commits.kde.org/marble/a70e7d5b96e451ec9ca81cd01a50d4d32fcca685'>Commit.</a> </li>
<li>Enable Vector OSM by default for non-stable releases. <a href='http://commits.kde.org/marble/c136126c70de748843bb819751cbf53946f4183a'>Commit.</a> </li>
<li>Enable sentinel 2 by default for non-stable releases. <a href='http://commits.kde.org/marble/14ce559e317462771e978b09f5ec7064e34553cc'>Commit.</a> </li>
<li>Added way-concatenating module to osm-simplify. <a href='http://commits.kde.org/marble/9c6b3d0e42da0438a54798fda192034f6694ba72'>Commit.</a> </li>
<li>Create placemarks with point geometry from non-trivial way nodes. <a href='http://commits.kde.org/marble/43f3168e3ed8fb73d331cff3f6f2f7b022c9b095'>Commit.</a> </li>
<li>Teach AutoNavigation to center on the route-fixed position. <a href='http://commits.kde.org/marble/61c2384729acc6e1a5a39f3af3b8dcdd72024151'>Commit.</a> </li>
<li>Revert "use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name". <a href='http://commits.kde.org/marble/0c0cdfbb6f607fdf7e39177ac4a0a9693947a626'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/marble/5da206565fa2d311073752b6b4c0c99a2fe8e61c'>Commit.</a> </li>
<li>Render "leisure=marina" (see e.g. Yachthafen Fahrensodde). <a href='http://commits.kde.org/marble/25811c29977c8f1bc6a07218e8f1edf544e7a6ed'>Commit.</a> </li>
<li>Make the range for detection of deviation from route speed-dependent. <a href='http://commits.kde.org/marble/c3d7ab14c2953fcbabf42fb7605ae230fa9220cc'>Commit.</a> </li>
<li>Don't render Osm relations with tag key "area:highway". <a href='http://commits.kde.org/marble/693ceec0681a6a42a39c0adb18d164d96e020530'>Commit.</a> </li>
<li>Don't render the area:highway tagkeys and the tag. <a href='http://commits.kde.org/marble/79d448d307fde6617e78cf929015e10c2f119427'>Commit.</a> </li>
<li>Introduce FloatItemsLayer, a dedicated layer for rendering screen-positioned, 2D float items. <a href='http://commits.kde.org/marble/025808394790ea6413a0585f899cf7bb2073c3ee'>Commit.</a> </li>
<li>Added Bunker and Shelter. <a href='http://commits.kde.org/marble/d2436d382bb98892fcf72bc611ebf06cc02a4492'>Commit.</a> </li>
<li>Adding AmenityKindergarten. <a href='http://commits.kde.org/marble/02684a87bf018c7e672d2539130673edc9ca5b3c'>Commit.</a> </li>
<li>No Small Caps for areas (e.g. Continents). <a href='http://commits.kde.org/marble/6a50e1c259c6811be956af238c3e8b848f84d5a5'>Commit.</a> </li>
<li>Reuse cache files from different levels. <a href='http://commits.kde.org/marble/584dd684e474def76eff0a6d8ef0032e8e36ae15'>Commit.</a> </li>
<li>Another cache directory fix. <a href='http://commits.kde.org/marble/0fc8bd3168b8ba5390955d6e365360332a4e5d97'>Commit.</a> </li>
<li>Fix cache directory usage. <a href='http://commits.kde.org/marble/4f3641515abf053748f5f1d453f85803acbc54a4'>Commit.</a> </li>
<li>Revert "Fix cache directory usage". <a href='http://commits.kde.org/marble/5154d7e805692ac11e2c0aa39410e63f34667127'>Commit.</a> </li>
<li>Fix cache directory usage. <a href='http://commits.kde.org/marble/06a8ef68e9ccbdde6154fbe0766a421b2b6c9d52'>Commit.</a> </li>
</ul>
<h3><a name='mbox-importer' href='https://cgit.kde.org/mbox-importer.git'>mbox-importer</a> <a href='#mbox-importer' onclick='toggle("ulmbox-importer", this)'>[Show]</a></h3>
<ul id='ulmbox-importer' style='display: none'><li>New in this release</li></ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Detect passphrase errors robustly. <a href='http://commits.kde.org/messagelib/38761d68c5ff35e6388c9239784e3bb827966b98'>Commit.</a> </li>
<li>Used accidently a header that is not available in gpgme 1.7.1. <a href='http://commits.kde.org/messagelib/e978f4d266846c74200424149214b968789f0485'>Commit.</a> </li>
<li>Fix store correct value in cache. <a href='http://commits.kde.org/messagelib/fa6013e087cbbc95024e5d580de0f070feb8dd59'>Commit.</a> </li>
<li>Add Fix for Kontact creates broken pgp signed mails. <a href='http://commits.kde.org/messagelib/d10544b77348ca48bebe1c5c94f2a07f807b8ac9'>Commit.</a> </li>
<li>I18n: do not extract dummy strings. <a href='http://commits.kde.org/messagelib/292d86c46feef40f4a43caa2de9d2e7dd1938e3b'>Commit.</a> </li>
<li>Add friend class to create new interface. <a href='http://commits.kde.org/messagelib/1a7304128c228267dfe60f6a8acb6f45591c5784'>Commit.</a> </li>
<li>Add EncryptedMessagePart::isDecryptable. <a href='http://commits.kde.org/messagelib/8e54231d5e7c507ed519167600cb21bf403b6ae9'>Commit.</a> </li>
<li>Make mimetreeparser to build without libkleo, for gpgme > 1.7.1. <a href='http://commits.kde.org/messagelib/532c1daf21edbd433ec993f73d5e3c98e30dc11a'>Commit.</a> </li>
<li>Set older versions for MIMETREEPARSER_ONLY_BUILD. <a href='http://commits.kde.org/messagelib/1a6974a3c0737c130ccb1a9475aca31d516267f0'>Commit.</a> </li>
<li>Get rid of kleo in mimetreeparser. <a href='http://commits.kde.org/messagelib/11a7769a01b0b155ae62b209c7cc24aa1b65cdbd'>Commit.</a> </li>
<li>Increase privacy. <a href='http://commits.kde.org/messagelib/c090cc139f02ba54624bf93da6adc26de161234d'>Commit.</a> </li>
<li>Improve localfile support. <a href='http://commits.kde.org/messagelib/d9ac1d0877d844a793e4a0fccdf08dda311bef62'>Commit.</a> </li>
<li>Improve localdatabasefile. <a href='http://commits.kde.org/messagelib/c9d5c0a1637074d58a46807ee414c87d023d3d57'>Commit.</a> </li>
<li>Reactivate some code. <a href='http://commits.kde.org/messagelib/c97f4b28277468b8d8748b41240fc4b8644f074e'>Commit.</a> </li>
<li>Remove unused createTable. <a href='http://commits.kde.org/messagelib/92d07b3dc7c084aa1c2be5550268c43a013caa67'>Commit.</a> </li>
<li>We will use a mmap file. <a href='http://commits.kde.org/messagelib/22771596775663157e0e6e7b42b803365a163bde'>Commit.</a> </li>
<li>Improve test apps. <a href='http://commits.kde.org/messagelib/0a2601b993e9f97912815e1aa43b199a4cb3b3f1'>Commit.</a> </li>
<li>Improve request. <a href='http://commits.kde.org/messagelib/650ddbe06f8c3a9e88a26d01cc03461e3123e948'>Commit.</a> </li>
<li>Improve autotests. <a href='http://commits.kde.org/messagelib/bac3d30cf261ac45f2503d930412c8d39ebe4012'>Commit.</a> </li>
<li>Fix request json. <a href='http://commits.kde.org/messagelib/d733c0cd61e390f4e5e6a2192fa63466cb57b926'>Commit.</a> </li>
<li>Improve searchfullhash job. <a href='http://commits.kde.org/messagelib/48069042eaccc944e98a7ae63d5a22c71a074cf0'>Commit.</a> </li>
<li>Add info about return value. <a href='http://commits.kde.org/messagelib/c6e9cce694840ff0a7592d74d062e73c1703b36b'>Commit.</a> </li>
<li>We will use mmap file as discussed with David. <a href='http://commits.kde.org/messagelib/6c8379d9377bee937afacbd2d99498f894da4fd0'>Commit.</a> </li>
<li>Don't use QTimer as discussed with David. <a href='http://commits.kde.org/messagelib/76afabac6c8ff1682d27ffad0b99d41436450cb7'>Commit.</a> </li>
<li>Improve generate json. <a href='http://commits.kde.org/messagelib/ebe771fa2d32f433823a87f612c27dec137b0de4'>Commit.</a> </li>
<li>Add test apps. <a href='http://commits.kde.org/messagelib/8094abcc664a3e0c324fabab268cab3fc0b47624'>Commit.</a> </li>
<li>Add new autotest. <a href='http://commits.kde.org/messagelib/cb62494470bc56b85b16592228891bb437d4bc22'>Commit.</a> </li>
<li>Add full search method. <a href='http://commits.kde.org/messagelib/3b52378d13d2166e426aa7ae73d82939aa617136'>Commit.</a> </li>
<li>Add new class to search full job. <a href='http://commits.kde.org/messagelib/222b5a2388355c0bc2166e92a741e2130a96690a'>Commit.</a> </li>
<li>Add comment. <a href='http://commits.kde.org/messagelib/33328f720d0549fce6ea73756787b36fbfb01d90'>Commit.</a> </li>
<li>Continue to implement search in local database. <a href='http://commits.kde.org/messagelib/79ac0ea8d98e7fc7ba2119469fa2af962ed021eb'>Commit.</a> </li>
<li>Add more debug. <a href='http://commits.kde.org/messagelib/7166458b37d021e826254aaabcd70e66a6aacaaa'>Commit.</a> </li>
<li>Use qCDebug(...). <a href='http://commits.kde.org/messagelib/f4d827aa88d9d7483a933cab54df54ae1335fa71'>Commit.</a> </li>
<li>Use qCWarning. <a href='http://commits.kde.org/messagelib/2c0e33bb07986a8c333b1856101037c2366b528d'>Commit.</a> </li>
<li>Remove no used DEFINE. <a href='http://commits.kde.org/messagelib/ae3fff46ca5a00ffbafe93ad724c011627ba1a59'>Commit.</a> </li>
<li>Remove enum. <a href='http://commits.kde.org/messagelib/aaeb12d5b1212cbbf80a8445657fdcd357b79e71'>Commit.</a> </li>
<li>Improve localmanager. <a href='http://commits.kde.org/messagelib/135da872ec67ada1aa4fda327c67f5cbf9421d35'>Commit.</a> </li>
<li>Move method to localdatabasemanager directly. <a href='http://commits.kde.org/messagelib/41a2bb46c39ef2c415697dd9852a94fa43c9796f'>Commit.</a> </li>
<li>Forward signal. <a href='http://commits.kde.org/messagelib/05dfc61523176fb124e9912d6f28d30f67852fcd'>Commit.</a> </li>
<li>Don't check phishing url when we have low bandwidth. <a href='http://commits.kde.org/messagelib/2a5a261a77c95ed0a97e6fa1e316016545d26672'>Commit.</a> </li>
<li>Save new database state. <a href='http://commits.kde.org/messagelib/688c94c21492202df14d400ac0829a1b7ae96ef3'>Commit.</a> </li>
<li>Add info about rice support. <a href='http://commits.kde.org/messagelib/4d3029e1c96ad806d6534423bb5623013654d4f1'>Commit.</a> </li>
<li>Use qCDebug. <a href='http://commits.kde.org/messagelib/3271688d4f3e35a74ebd5fb8de4f517395a3c8ca'>Commit.</a> </li>
<li>Improve localdatabase creation. <a href='http://commits.kde.org/messagelib/02a8e79b474caacf55af24317249d9101a63a5b4'>Commit.</a> </li>
<li>Add missing include. <a href='http://commits.kde.org/messagelib/c9c9cdeb6c3f861da2c8334fa844e2806dce18aa'>Commit.</a> </li>
<li>Add new autotest. <a href='http://commits.kde.org/messagelib/164d3bdb7d419fddd39309cff4ddfafa97e5eea9'>Commit.</a> </li>
<li>Add comment. <a href='http://commits.kde.org/messagelib/fa8296b2bdc6bd564d3e97b0641ba8468195ca75'>Commit.</a> </li>
<li>Test if we need to clear database. <a href='http://commits.kde.org/messagelib/ff3935b32abc606e87625807fd0b5630ce725a3a'>Commit.</a> </li>
<li>Use a enum type. <a href='http://commits.kde.org/messagelib/e7d0883ed18a89b1f67b290d4d40ce3690e08390'>Commit.</a> </li>
<li>Check if we must update database. <a href='http://commits.kde.org/messagelib/6e144aec8af7eb7cb18f977bc8e6c6406f98a425'>Commit.</a> </li>
<li>Add timer to update database. <a href='http://commits.kde.org/messagelib/efd80c513b30dc2fb7bd3d92b8fb5130ede5b4b7'>Commit.</a> </li>
<li>Don't call two fullUpdate if we still are in progress. <a href='http://commits.kde.org/messagelib/9919a40f3217f42f9155231d9c21a30e6ca083fd'>Commit.</a> </li>
<li>Rename methods. <a href='http://commits.kde.org/messagelib/6a813662fd111a9aa5b3f054ed4c53172dbd4e17'>Commit.</a> </li>
<li>Implement update database support. <a href='http://commits.kde.org/messagelib/03c7cefcaa4104d5d5fc30b5d78e0d0838852ca5'>Commit.</a> </li>
<li>Add clear method. <a href='http://commits.kde.org/messagelib/2b5b01085939739f0e35fc40c01709fa803fe503'>Commit.</a> </li>
<li>Use new inline method. <a href='http://commits.kde.org/messagelib/8c449d6a07a5da76080da9222e09c222eeac40ad'>Commit.</a> </li>
<li>Improve download + create database. <a href='http://commits.kde.org/messagelib/067206792343932ffdb670561d5e9587c6163f25'>Commit.</a> </li>
<li>Allow to test load partial download. <a href='http://commits.kde.org/messagelib/84bca161c3cafc26a962c61b0f954ab0816d62d9'>Commit.</a> </li>
<li>Verify proto pointer. <a href='http://commits.kde.org/messagelib/3afd8dfafb9fc09fa52bc7a16fb2369ef36a7620'>Commit.</a> </li>
<li>Improve database management. <a href='http://commits.kde.org/messagelib/b6fef0529619a40642724deff4f088cbc066bf4e'>Commit.</a> </li>
<li>Improve local database. <a href='http://commits.kde.org/messagelib/00c89a0e3d961dcf3c4e9040bf0a6a35746cfc7a'>Commit.</a> </li>
<li>Add new test apps. <a href='http://commits.kde.org/messagelib/a11336eaf194485c33813082043edf6a13137ca6'>Commit.</a> </li>
<li>Improve create database. <a href='http://commits.kde.org/messagelib/e358ef84a032813a61642c8ba28921a1eaa84cfe'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/messagelib/336e8ca490ef5c6d50a65806b675264b48250d45'>Commit.</a> </li>
<li>Look at to define sqlite database for phishing url database. <a href='http://commits.kde.org/messagelib/c09a8f0929eef87299374f691db6b3bed1148f7c'>Commit.</a> </li>
<li>Add a util class. <a href='http://commits.kde.org/messagelib/b34448bfbca4ccf5c41184c7cdb194c912c8308e'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/messagelib/2903fd09a4af6633b402d4dfc055a46d812b6237'>Commit.</a> </li>
<li>Implement clear method. <a href='http://commits.kde.org/messagelib/e85794b78461498cb610fa4914cafbfad3f00b32'>Commit.</a> </li>
<li>Parse removal index. <a href='http://commits.kde.org/messagelib/47f41e3b296850f3670cc5a189760959394342c0'>Commit.</a> </li>
<li>Add debug info. <a href='http://commits.kde.org/messagelib/f8d9c9e291be6a484cc688f705721548bd4b999f'>Commit.</a> </li>
<li>Continue to fix parsing. <a href='http://commits.kde.org/messagelib/31eba380348209feeb0a4e323d1b99e29bd2be09'>Commit.</a> </li>
<li>Improve test. <a href='http://commits.kde.org/messagelib/195019a3cea52eb8bcf6a929a0daed86355e647b'>Commit.</a> </li>
<li>Improve autotest. <a href='http://commits.kde.org/messagelib/3765adbcf236cd76c33889c8632e6dab8aead4ac'>Commit.</a> </li>
<li>Add more check. <a href='http://commits.kde.org/messagelib/b19c6ed8d760ef9c6a2b8eaff1fa95798d22e9f5'>Commit.</a> </li>
<li>Improve autotest. <a href='http://commits.kde.org/messagelib/5f14004230b4e75edb9833c9931a762a2f6b836e'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/messagelib/549224ca465d59d94e40e6af547c2e171579ecce'>Commit.</a> </li>
<li>Return parsing result. <a href='http://commits.kde.org/messagelib/24d9949bb419ac95590d82e2f2401488a3589280'>Commit.</a> </li>
<li>Improve parsing. <a href='http://commits.kde.org/messagelib/6e5a492d6efcfbe5f23f1ffc0e8bc1dc1d99578c'>Commit.</a> </li>
<li>Initialize database. <a href='http://commits.kde.org/messagelib/9038d4382e75c0f46d3de38e5501bbee61033eec'>Commit.</a> </li>
<li>Create hash. <a href='http://commits.kde.org/messagelib/bfb84a6527ed10324a8d50f5c921e7737dbc134e'>Commit.</a> </li>
<li>Add singleton. <a href='http://commits.kde.org/messagelib/30ac86ea47b3060ee0bc5669fcd934ee0c5bf4fb'>Commit.</a> </li>
<li>Add localdatamanager class. <a href='http://commits.kde.org/messagelib/cd400c20fc6d740d3d44f58032927e8cbbfc160b'>Commit.</a> </li>
<li>Add more enum value. <a href='http://commits.kde.org/messagelib/a82c604ebcafd4941cfdd6e6f19332a0bce1c152'>Commit.</a> </li>
<li>Emit signal when we finished to parsing data. <a href='http://commits.kde.org/messagelib/1b465059b1d843b5df92db0cca117e73be390d16'>Commit.</a> </li>
<li>Improve parsing. <a href='http://commits.kde.org/messagelib/3baeca18c5c029a5be865a2ce9fe0949de6972ee'>Commit.</a> </li>
<li>Continue to parse json document. <a href='http://commits.kde.org/messagelib/2cca5b73a7364c47337345e0c02dabce394735c5'>Commit.</a> </li>
<li>Add more info. <a href='http://commits.kde.org/messagelib/680b887ae28b91500076a74deef76387f2fe753b'>Commit.</a> </li>
<li>Improve createphishingurldatabasejob. <a href='http://commits.kde.org/messagelib/3adaa075aa6284b026ea6fb706e4e50949b725f8'>Commit.</a> </li>
<li>Add class to check url from local database. <a href='http://commits.kde.org/messagelib/40d0a5adf74717a5244b33d416bbf1e618d72a2e'>Commit.</a> </li>
<li>Improve test apps. <a href='http://commits.kde.org/messagelib/514191218cee42f6c2a9e9e3d00b66a2e537a323'>Commit.</a> </li>
<li>Improve. <a href='http://commits.kde.org/messagelib/8e2f230ebb062643a5cb1e734d73ad345793a9eb'>Commit.</a> </li>
<li>Generate requests. <a href='http://commits.kde.org/messagelib/530c8afa7057724c8bea88d4737a51df4406ec8d'>Commit.</a> </li>
<li>Improve job. <a href='http://commits.kde.org/messagelib/4a66bf75eec7fa7d0829dbf9ca5d7c7175d5b023'>Commit.</a> </li>
<li>Add test apps for the future. <a href='http://commits.kde.org/messagelib/201a577cac7557f386cdc08201e59960dacf5cc9'>Commit.</a> </li>
<li>Look at how to implement local database. <a href='http://commits.kde.org/messagelib/7d158746a86db91aa436ad1b802f3df9cdb68b3a'>Commit.</a> </li>
<li>Fix some clazy error. <a href='http://commits.kde.org/messagelib/42c5e7884cd82a6eb087c441d0aab431f0a51706'>Commit.</a> </li>
<li>Use const_ref. <a href='http://commits.kde.org/messagelib/43728e53dc29607b7dc6661352ce92b3064c59c2'>Commit.</a> </li>
<li>Use variable. <a href='http://commits.kde.org/messagelib/3aec6cd91a7bb28d73674bd56b90751a2f08fe21'>Commit.</a> </li>
<li>It was an old api. Remove it. <a href='http://commits.kde.org/messagelib/43f28af04b44930693a371d3a140f25cb08ae3f0'>Commit.</a> </li>
<li>Fix some clazy warning. <a href='http://commits.kde.org/messagelib/b7b1ffd6f24f3d632aba3512c1ad81cc1887f4d9'>Commit.</a> </li>
<li>Disable dnd in some case. <a href='http://commits.kde.org/messagelib/82277b9c7d284367c76ae1013161869529ab03c0'>Commit.</a> </li>
<li>Use isEmpty. <a href='http://commits.kde.org/messagelib/e8e7d21bdca274070762dc19f469f974a6f8c4f7'>Commit.</a> </li>
<li>Use WebEngineViewer::CheckPhishingUrlCache::MalWare too. <a href='http://commits.kde.org/messagelib/9ba38b28fbb381e9f2acda7987ced3b93f07dfd6'>Commit.</a> </li>
<li>USe cache. <a href='http://commits.kde.org/messagelib/d832310a02b9263b216c45275f185984880348c5'>Commit.</a> </li>
<li>Add a cache for check phishing url. <a href='http://commits.kde.org/messagelib/a20268fb6f40580d59ad367175f7548e4d4a9002'>Commit.</a> </li>
<li>Block other type. <a href='http://commits.kde.org/messagelib/f111a3ef8b531e859012d5b58a50ec467f9b9a7e'>Commit.</a> </li>
<li>Make it false by default. <a href='http://commits.kde.org/messagelib/34805b872860fe7bb040fa1ae0cc4feb1b387480'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/f23078a0c3bfccf517116b398102d3a43e1f3097'>Commit.</a> </li>
<li>Remove debug. <a href='http://commits.kde.org/messagelib/538fc991a0e4c0a01669389bd132745156f16a6c'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/fec663f38742ca1e5b32ed0a49a4eff45670f825'>Commit.</a> </li>
<li>We can't implement as it. => remove it for the moment. <a href='http://commits.kde.org/messagelib/c2b0b2d8db3f1af755bd7eb986c8eec3048af74b'>Commit.</a> </li>
<li>Fix Bug 366652 - Drag and drop attachments in Kmail has stopped working. <a href='http://commits.kde.org/messagelib/42a5c00667e6440fcf3879a1dfdc9f31088f78d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366652'>#366652</a>. Fixes bug <a href='https://bugs.kde.org/368378'>#368378</a></li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/messagelib/a2478015a40a00ea0443d65f705aa3e0c52c334c'>Commit.</a> </li>
<li>Improve autotest. Clean up. <a href='http://commits.kde.org/messagelib/72246d52b1fd5add44ca8c56f762560577b9e396'>Commit.</a> </li>
<li>Use ksyntaxhightling as default. <a href='http://commits.kde.org/messagelib/0996f81c43f704cab95f0b49042497b1be8631d2'>Commit.</a> </li>
<li>Activate check phishing url. <a href='http://commits.kde.org/messagelib/90a674d19c631441366cdc84b17bbe06f88e5461'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/9e8beaad47a696a6b5b7ef7860227575adb9d21e'>Commit.</a> </li>
<li>Const'ify. <a href='http://commits.kde.org/messagelib/500fe72f295c0b43fccfd36d094eed72d9466f56'>Commit.</a> </li>
<li>Parse answer. <a href='http://commits.kde.org/messagelib/cc7c23837beba4a7a6f2caf8c02584e5462c9837'>Commit.</a> </li>
<li>Start to parse answer. <a href='http://commits.kde.org/messagelib/5fb54f0da8ef1bbe2210b59a61b203a098e06191'>Commit.</a> </li>
<li>Add parsing. <a href='http://commits.kde.org/messagelib/e8742e84b855eee90a0126eaa786dd3990d8f5bd'>Commit.</a> </li>
<li>Use return pressed to check url. <a href='http://commits.kde.org/messagelib/ee5016d76ab4dedcf0e956a2e76e1f34e893964b'>Commit.</a> </li>
<li>Extract method which generates json request + use qCDebug. <a href='http://commits.kde.org/messagelib/d608d21f78c8b561ada1aa0d2e9c6e59cb431035'>Commit.</a> </li>
<li>Fix cmake warning. <a href='http://commits.kde.org/messagelib/2dd8be99e85d5117e8c61f8eda98c6d7b1ccb839'>Commit.</a> </li>
<li>Allow to make it works. <a href='http://commits.kde.org/messagelib/c4da1ae1c0108d06e652b03a325b210355313e01'>Commit.</a> </li>
<li>Increase test apps. <a href='http://commits.kde.org/messagelib/b387f6a1627f8685766392521e8df2cf996e8f92'>Commit.</a> </li>
<li>Add option to only build mimetreeparser. <a href='http://commits.kde.org/messagelib/f1cc2c8c75b6bd452027a34c8c92ec61bd6a5a48'>Commit.</a> </li>
<li>Delete not used FindGpgme.cmake file. <a href='http://commits.kde.org/messagelib/5e197abb437fdfae66c80e6a1377c70589ddc68d'>Commit.</a> </li>
<li>Fix Bug 372085 - KMail 5.3.2: forwarding or replying to html mails do not preserve links. <a href='http://commits.kde.org/messagelib/ce24fc860ee444147070573cda42fe2d1369d75c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372085'>#372085</a></li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/e9c579ea85ba3d4f98384c2033a3b86a6f709101'>Commit.</a> </li>
<li>Step 1 before to remove option. <a href='http://commits.kde.org/messagelib/41c43a51894505d32b4a7697b04a23e55e7201a4'>Commit.</a> </li>
<li>Fix build with new ksyntaxhighlighting. <a href='http://commits.kde.org/messagelib/12ba93d14b4a42d6d898dd5db811460f8db7ba5e'>Commit.</a> </li>
<li>Allow selection when we forward message. <a href='http://commits.kde.org/messagelib/da7acca24ee4feadef84423b65ec7310c1716ca0'>Commit.</a> See bug <a href='https://bugs.kde.org/372085'>#372085</a></li>
<li>Fix mem leak. <a href='http://commits.kde.org/messagelib/36f439a3c225905c999875166144f4858673d54d'>Commit.</a> </li>
<li>Remove old settings. <a href='http://commits.kde.org/messagelib/7983a237ebb3c598c8564031f07804172d079aaa'>Commit.</a> </li>
<li>Add settings to check or not url. <a href='http://commits.kde.org/messagelib/ed494bfdb7ab34d049a73ad4208977c8869b2ba4'>Commit.</a> </li>
<li>Fix mem leak when we print (need new kmail too). <a href='http://commits.kde.org/messagelib/95896d8864831c476de579bff47cc5646cf160f3'>Commit.</a> </li>
<li>Fix emit signal here too. <a href='http://commits.kde.org/messagelib/9f15f1590606805e28ecf9dffedb89c08f6916ae'>Commit.</a> </li>
<li>Fix Bug 359964 - "Kmailleaks", or what to improve to make Kmail more privacy friendly. <a href='http://commits.kde.org/messagelib/6296818e9c7003bec9911c0ee702dc1851ab33e1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359964'>#359964</a></li>
<li>Remove include when not necessary. <a href='http://commits.kde.org/messagelib/2b775153489021db19a33b6a178bb8322078ca62'>Commit.</a> </li>
<li>Fix message. <a href='http://commits.kde.org/messagelib/75107c331e7ac4a15d61cf393bca7cba50296786'>Commit.</a> </li>
<li>Add reply to all action. <a href='http://commits.kde.org/messagelib/669eb1dbf4fc6e87ee56cf7655c55365822b6c68'>Commit.</a> </li>
<li>Remove unused enum value. <a href='http://commits.kde.org/messagelib/0d00bc76ab1ec362894d098df75f3bd7af8bda3d'>Commit.</a> </li>
<li>Add specific  mailcheckphishingurljob. <a href='http://commits.kde.org/messagelib/b80c3450a29aa5b874470d7e4a8624ae4eac942d'>Commit.</a> </li>
<li>Exclude some other types. <a href='http://commits.kde.org/messagelib/44bff4e0b28280c8b6ecbb7e5d8373c25d2d71ae'>Commit.</a> </li>
<li>Add more informations. <a href='http://commits.kde.org/messagelib/c4348722ce821da1fae53cf7397439f380194126'>Commit.</a> </li>
<li>Fix check url. <a href='http://commits.kde.org/messagelib/706d59208f68bf3656398f8838ae63270e81faaa'>Commit.</a> </li>
<li>Fix signal/slot. <a href='http://commits.kde.org/messagelib/acc68dc7ccac0dd9c1db757b5d9d8ed894374840'>Commit.</a> </li>
<li>Return url too. <a href='http://commits.kde.org/messagelib/265824fcfe2d9d39169b6c68b8bab3bd9b6ba17b'>Commit.</a> </li>
<li>Start to implement reply action against encapsuled message. <a href='http://commits.kde.org/messagelib/2ecf5b5ae66fe32c68662a0133a72b1a1222b883'>Commit.</a> </li>
<li>Use 5.8.0 for it. <a href='http://commits.kde.org/messagelib/86fe837ecb2863e7c6120a651be6a07ec4da6ed7'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/129a159684ac8e062d3d26c27c7aeaaf3e30c956'>Commit.</a> </li>
<li>Clean forward declaration. <a href='http://commits.kde.org/messagelib/45d21c2df34b74f24ec626a60d47fb730029e31d'>Commit.</a> </li>
<li>Test dnd. <a href='http://commits.kde.org/messagelib/c46819349aa0ebc82b84ef8e153882c398f2d456'>Commit.</a> </li>
<li>Don't strip my identity in this case. <a href='http://commits.kde.org/messagelib/cff84cbca1f0fdfba99ebd2fab289d15355a2486'>Commit.</a> </li>
<li>Clean up class. <a href='http://commits.kde.org/messagelib/9b0babc955a92785954a743b0ade07363ebb9558'>Commit.</a> </li>
<li>Fix autotests. <a href='http://commits.kde.org/messagelib/156c34565de3f8bcde7a0a2a2026a9d78bce3424'>Commit.</a> </li>
<li>Add new autotest. <a href='http://commits.kde.org/messagelib/0976cf3112ca6ffbfae78b3fef02474815120b61'>Commit.</a> </li>
<li>Use unique network manager. <a href='http://commits.kde.org/messagelib/0e07b10191e454706d63ff17b704e666bd192738'>Commit.</a> </li>
<li>Make it compiles. <a href='http://commits.kde.org/messagelib/a2db3dd1c93260977d46b3780a5cb340425d7711'>Commit.</a> </li>
<li>We don't need libkleo in public interface anymore. <a href='http://commits.kde.org/messagelib/b2ec973f28ad458386b84835a5a7d3d826be6e82'>Commit.</a> </li>
<li>Messageviewer actually using QGpgme and not Gpgmepp for building. <a href='http://commits.kde.org/messagelib/be65fae6aa0dd34e43d69eb31f6fa5070a0fc6e0'>Commit.</a> </li>
<li>Search for importjob correctly in gpgme++. <a href='http://commits.kde.org/messagelib/750c8addd5496cf52f2495f06aa3a7566a7a0a0e'>Commit.</a> </li>
<li>Make it more secure. <a href='http://commits.kde.org/messagelib/fb6b70c4d7b9f1b7f384e189e11425ed2576675e'>Commit.</a> </li>
<li>Return value. <a href='http://commits.kde.org/messagelib/730b8725b48ae7ee235d7212a2b3766bf0d19f9d'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/messagelib/dca534e6c2f14d7980c32702623834ca1dfa0504'>Commit.</a> </li>
<li>Reduce linking. <a href='http://commits.kde.org/messagelib/315da0e562d03c1b3bbc09656c7badae3d132a3e'>Commit.</a> </li>
<li>Move in webengineviewer so we can use it in akregator too. <a href='http://commits.kde.org/messagelib/84625acd5a45fb73c785c1ca9cc564a2616f4e34'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/messagelib/260110ecbd92f98a5561ed21f9387cd8621ab24d'>Commit.</a> </li>
<li>Look at to using QWebEngineContextMenuData. <a href='http://commits.kde.org/messagelib/642a9d6051d5f42dd8dd1205d2b19887e659decf'>Commit.</a> </li>
<li>Whatthis is always empty. <a href='http://commits.kde.org/messagelib/fa5bb7d57500501213a8ff504066ebad73efdcd7'>Commit.</a> </li>
<li>Add code to check url. <a href='http://commits.kde.org/messagelib/b4f47b8d56348c31f3ddae959e768afbbb9686f0'>Commit.</a> </li>
<li>Continue to implement check url job. <a href='http://commits.kde.org/messagelib/ace882f213f3319fb66891124664741454a4df30'>Commit.</a> </li>
<li>Test if network is online. <a href='http://commits.kde.org/messagelib/c0f73a906f38d05440883f51f2a25bd391d7f73f'>Commit.</a> </li>
<li>Add method to create postrequest. <a href='http://commits.kde.org/messagelib/d7fba5bbdcf80a7bb1ba083cc2b5af26df62e69d'>Commit.</a> </li>
<li>Don't add sender. <a href='http://commits.kde.org/messagelib/b8b421756da80d8df6d8413e697132ba62495eef'>Commit.</a> </li>
<li>Fix Bug 368498 - Kmail does not open links with target="_blank". <a href='http://commits.kde.org/messagelib/70173a8245c6f0401619f5be9cc4770451feeb21'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368498'>#368498</a></li>
<li>Add QNetworkAccessManager class. <a href='http://commits.kde.org/messagelib/ae04a1a6cff7e600672d3d9a3acb38fe519b7f57'>Commit.</a> </li>
<li>Remove unused include. <a href='http://commits.kde.org/messagelib/7d9182fd1e02976d861317bed43fc979d6bb1e8b'>Commit.</a> </li>
<li>Improve tests apps. <a href='http://commits.kde.org/messagelib/3ef824512ecb89551ec73ee27c59b5ac8e28b0c2'>Commit.</a> </li>
<li>Disable more feature for mailer. <a href='http://commits.kde.org/messagelib/4659635afd71784dee7b83839b4edcada004ca71'>Commit.</a> </li>
<li>Add TODO. <a href='http://commits.kde.org/messagelib/572715ad6f6bd776922579295408263189eb9419'>Commit.</a> </li>
<li>Improve test apps. <a href='http://commits.kde.org/messagelib/d5e74dce99599f41b520749f27fe8505f19475dd'>Commit.</a> </li>
<li>Add missing CMakeLists.txt. <a href='http://commits.kde.org/messagelib/e2805c46ef9e915e8ba93954f53b341dd70d521e'>Commit.</a> </li>
<li>Add test application. <a href='http://commits.kde.org/messagelib/b9b37534c27266092b92e220b33bcaab9d068c89'>Commit.</a> </li>
<li>Add info about url state. <a href='http://commits.kde.org/messagelib/c139cf8fdc3d51bfc706fbaa9d5a65ab0d73f419'>Commit.</a> </li>
<li>Make it compiles. auto keyword is not correct all the time. <a href='http://commits.kde.org/messagelib/22bf8f2a9ecfe4c6686348511285d7cc522d0d08'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/messagelib/f534ff26af4c77dfeb960d4819b7825202c7a528'>Commit.</a> </li>
<li>Add check url job. <a href='http://commits.kde.org/messagelib/1518beca32293b8518008532102f25f862321479'>Commit.</a> </li>
<li>Fix autotest. <a href='http://commits.kde.org/messagelib/f2e6e3c4a32157ce570f52549528676feabb16a3'>Commit.</a> </li>
<li>Fix reply to all when we use specific identities. <a href='http://commits.kde.org/messagelib/b507a9b650b934806376f913dd692f656e0b26b6'>Commit.</a> </li>
<li>Close viewer source when we close kmail. <a href='http://commits.kde.org/messagelib/fece1ec3662732f846e06ce8933a8fbe78f32f3c'>Commit.</a> </li>
<li>Block other object type. <a href='http://commits.kde.org/messagelib/15382290683dfa469e64574b18ee6598d37433bb'>Commit.</a> </li>
<li>Add warning widget about submitted form. <a href='http://commits.kde.org/messagelib/2f2ff89f2d3058c0f50225ef27b611cc5ea94315'>Commit.</a> </li>
<li>Remove debug. <a href='http://commits.kde.org/messagelib/9597ca21eb5f32d7fbac2c80efae1f2a3b381d36'>Commit.</a> </li>
<li>Forbidd to submit form. <a href='http://commits.kde.org/messagelib/660f74b66051bbe1c540d39155a7e7e1ae638053'>Commit.</a> </li>
<li>Make compile by default. <a href='http://commits.kde.org/messagelib/550ef410cd9f1f53b6367e31d78b371b13bc116b'>Commit.</a> </li>
<li>Add new interceptor. Make more secure. <a href='http://commits.kde.org/messagelib/5279f877497353b1b9aa9a457d4ebe4901d84b26'>Commit.</a> </li>
<li>Comment unused method. <a href='http://commits.kde.org/messagelib/d85cabbe06cce7fdcbd98136c80e12ab84a9497f'>Commit.</a> </li>
<li>Other optimization. <a href='http://commits.kde.org/messagelib/fd9e51a0b67f10f3946dc83b57a7514fa4b2d2af'>Commit.</a> </li>
<li>Cache hearder pointer. <a href='http://commits.kde.org/messagelib/d5ccb8624ef337b8d26905860e00bbf01d0d0ea3'>Commit.</a> </li>
<li>Cache header pointer. <a href='http://commits.kde.org/messagelib/033c2920a26c7b729604c140ab32983ce26d454c'>Commit.</a> </li>
<li>Cache header pointer. <a href='http://commits.kde.org/messagelib/09c2b0e61c580b7023fea546e72f38b8d9bd1f49'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/messagelib/da03d3c3b00352e6f7a86b25c43406cd5fbff86b'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/4b271a1d7d130fee03e1f23bbe62b621fa334015'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/5e1a25641a9996f155dd09ae2550ec2eec41a71c'>Commit.</a> </li>
<li>Fix new css. <a href='http://commits.kde.org/messagelib/8de178617bcec1376518efcee47264e8d8381fec'>Commit.</a> </li>
<li>Fix autotests. <a href='http://commits.kde.org/messagelib/37ff0ee5741f0314854835deaffc71c3716a39eb'>Commit.</a> </li>
<li>Update autotest. <a href='http://commits.kde.org/messagelib/7a2de14c2cf403cfca2d927eccd669509d70f9f7'>Commit.</a> </li>
<li>Add autotest for bug-370452. <a href='http://commits.kde.org/messagelib/db32210ef9a7ddba69fc5d82b262d67d83a7cf10'>Commit.</a> </li>
<li>Improve autotest. <a href='http://commits.kde.org/messagelib/144a531e56273d2dcb1165b289aca3ebb76471ad'>Commit.</a> </li>
<li>Add mbox file. <a href='http://commits.kde.org/messagelib/e1927f87c690168d9a190b439819e7949a772e67'>Commit.</a> </li>
<li>I need a new autotest for specific bug. <a href='http://commits.kde.org/messagelib/bf68849e1740fab4c2eb0fa78fb26e3652d4c591'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/messagelib/445590f403c5cc456ac7d4dbd79bb6794b70d417'>Commit.</a> </li>
<li>Fix Bug 369072 - Regression kMail 5.3.x: Plain-text quoting display is messed up. <a href='http://commits.kde.org/messagelib/c1d1b7fb6ebeb6c060b172567aa6df9104a6aeed'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369072'>#369072</a></li>
<li>Not necessary to store in private class. <a href='http://commits.kde.org/messagelib/4419745bc0fa72f3fdc0d3e54dbbbbda85f7d976'>Commit.</a> </li>
<li>Use path and not data for icons. It's more easy to create autotest. <a href='http://commits.kde.org/messagelib/1723bdd827fb48caf61f6e95b3abe258bad8158b'>Commit.</a> </li>
<li>We need to implement reply to list. <a href='http://commits.kde.org/messagelib/0544b566ea62c09860099c4e51a22893fbf961ea'>Commit.</a> </li>
<li>Add reply to author autotest. <a href='http://commits.kde.org/messagelib/0f5709bf42b6df0910c86d0ae3532a85c2ecf0e0'>Commit.</a> </li>
<li>Implement autotest for testing reply to all. <a href='http://commits.kde.org/messagelib/5b2b8d9425127698e7e2573d3627d2d0c10b623e'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/messagelib/bd0daec51ca02f28f5d22f76db1e2c5b65142d0d'>Commit.</a> </li>
<li>Add autotest when we select text. <a href='http://commits.kde.org/messagelib/4fc620d8cd91f7c8f0f4df9ce87e3cbe422bedb3'>Commit.</a> </li>
<li>Add autotest with mutli emails. <a href='http://commits.kde.org/messagelib/1a254f8e8f049e0cc24780d239fbba6b131ceaa5'>Commit.</a> </li>
<li>Add more autotest about resend method. <a href='http://commits.kde.org/messagelib/a0cd09de11895e41922b39cc8db2a8184a63bf4d'>Commit.</a> </li>
<li>Reorder method. <a href='http://commits.kde.org/messagelib/430da7db55bc91591d097f8755173163b4d5aa8d'>Commit.</a> </li>
<li>Rename method. <a href='http://commits.kde.org/messagelib/2d969703fa58d69be5fb951d7b25fc16cfcc7e5b'>Commit.</a> </li>
<li>Add identity. <a href='http://commits.kde.org/messagelib/3fbb06c246cc947177003d2bcf0dbe69e9fca3ce'>Commit.</a> </li>
<li>Debug--. <a href='http://commits.kde.org/messagelib/5177e58109127954e2f348a1d9a1dcb632798452'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/messagelib/87b1a6d6d71f91d3d9c5ca9b7c9889a4c3fc5e63'>Commit.</a> </li>
<li>Now we don't skip autotest. <a href='http://commits.kde.org/messagelib/6c2feb62387b8f2ec3bffee47e01188da4f6ae90'>Commit.</a> </li>
<li>Fix this autotest too. <a href='http://commits.kde.org/messagelib/e8287a0ce253adfe70e4850cb10f3e10cb78d833'>Commit.</a> </li>
<li>Fix autotest. <a href='http://commits.kde.org/messagelib/0ca1f3b647799f5aba4a817dedb5fecf25e47a6f'>Commit.</a> </li>
<li>Make it compiles. <a href='http://commits.kde.org/messagelib/e4efa7e85770c62299f2c39dff78dafd523c70b7'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/6248e12c38ca4beb91e9f8d4bcbba6a73d1283fb'>Commit.</a> </li>
<li>Make sure to clean up identities. <a href='http://commits.kde.org/messagelib/a19ba4d1ac691a2c24284599f90351ac23dd03f7'>Commit.</a> </li>
<li>Not necessary to change KDEHOME. Create some identities. <a href='http://commits.kde.org/messagelib/54b2a3dfe34520517edb87ff70d9760ece454446'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/0fee5f264f540363be2766fbdc069531a404c263'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/messagelib/1daedaa2ff26b2d79b4beb71fdeb25b04fd93a61'>Commit.</a> </li>
<li>Use Q_NULLPTR. <a href='http://commits.kde.org/messagelib/e838ae2ce7303ba77bf7d98c3d0ab6a05612842c'>Commit.</a> </li>
<li>Activate by default emoticon. Add action to disable it if necessary. <a href='http://commits.kde.org/messagelib/64ab2483009bd6d68bf8d1e9f9efadec7084b3e0'>Commit.</a> </li>
<li>Add const'ref. <a href='http://commits.kde.org/messagelib/8cfc00ad42d30a4999adb0234f0ca3ea4e630675'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/be6f0d08832394a1574bf8ee2e477676c2fc2b5e'>Commit.</a> </li>
<li>Fix don't show warning when we print message. <a href='http://commits.kde.org/messagelib/c64616221277a76613913c84ddc690cb7a6812a8'>Commit.</a> </li>
<li>Printing is never used. Remove it. <a href='http://commits.kde.org/messagelib/52a1b2a62beaa95d9c7557062323d73daa667c52'>Commit.</a> </li>
<li>Fix Bug 340435 - Printing HTML e-mail w/external references incorrectly adds warning message to top of print-out. <a href='http://commits.kde.org/messagelib/706ec225ece7d732615917cef2dd000de95690ce'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340435'>#340435</a></li>
<li>Not necessary to show error page for an email. <a href='http://commits.kde.org/messagelib/2fa342ba2ad6f9b60a1e836067bca6183dffd39a'>Commit.</a> </li>
<li>Remove not necessary methods. <a href='http://commits.kde.org/messagelib/b441cce2837a8aa0c4c0862641cb88386cbb0941'>Commit.</a> </li>
<li>Remove commented line. <a href='http://commits.kde.org/messagelib/f63069dfcd2f15b8e1a4f52b0e243abfed4ddafb'>Commit.</a> </li>
<li>Remove not necessary old scripts. <a href='http://commits.kde.org/messagelib/c1af9bb147d56009ad36f1c56a1b6873d69c5e79'>Commit.</a> </li>
<li>Port messagelib to GpgME 1.7 and new LibKleo. <a href='http://commits.kde.org/messagelib/277cbdcbfbce200957c107a96bc6f6627bdafe3e'>Commit.</a> </li>
<li>Use script for show/hide attachment, same for hide/show CC/To. <a href='http://commits.kde.org/messagelib/75b1bd97fd877f7f200b86bab6ce4cb36bcd3097'>Commit.</a> </li>
<li>Now we have just one quote font. <a href='http://commits.kde.org/messagelib/da070943c4fc6e8a52ae64d8a87659d5d3c15553'>Commit.</a> </li>
<li>Show close button by default. <a href='http://commits.kde.org/messagelib/711930d831964515536de61e33e25996fcca7a60'>Commit.</a> </li>
<li>Store linkcolor. <a href='http://commits.kde.org/messagelib/23115fedf9971a6d4c67253d7946fbe8172b40d0'>Commit.</a> </li>
<li>Optimization. <a href='http://commits.kde.org/messagelib/f1978e83ea45316d96c874e74791f7ebe0d4b662'>Commit.</a> </li>
<li>Remove debug. <a href='http://commits.kde.org/messagelib/843e1e8bdbae71e0ed450646c75610a0a93204f8'>Commit.</a> </li>
<li>Rename variable. <a href='http://commits.kde.org/messagelib/05feb8b022121cc6625c59172685de2f0a2c5fa3'>Commit.</a> </li>
<li>Try to reduce number of KColorScheme usage. <a href='http://commits.kde.org/messagelib/a2ad080bef8bd0e4c7a00647739942c4f6269a89'>Commit.</a> </li>
<li>Fix Bug 369146 - Reading email advancing with "space" doesn't jump to next unread at end of current email. <a href='http://commits.kde.org/messagelib/6b8674631b71e7ebb67ad7cfec3bf0aa4ea640de'>Commit.</a> See bug <a href='https://bugs.kde.org/369146'>#369146</a></li>
<li>Fix return info about we are at bottom of page. <a href='http://commits.kde.org/messagelib/f9cd0bbae68f61b1be24ce809c318bbf6780f468'>Commit.</a> </li>
<li>Use new connect api. <a href='http://commits.kde.org/messagelib/f90bb1875c8cf693b93e8e92ef7be55856c0efe5'>Commit.</a> </li>
<li>Improve test apps. <a href='http://commits.kde.org/messagelib/179f6e9ac1c9b4a7902e791ad1e8d105179fe4da'>Commit.</a> </li>
<li>Show javascript message. <a href='http://commits.kde.org/messagelib/ffc3973cbdf1e7942037583bb89ba53722a6b862'>Commit.</a> </li>
<li>Improve test. <a href='http://commits.kde.org/messagelib/fad04718e33a47552ee3ead9f4de4ef957b04752'>Commit.</a> </li>
<li>Add new script. <a href='http://commits.kde.org/messagelib/ddd3bb07cdd371a04c22eabcaf9b4fa16563132e'>Commit.</a> </li>
<li>Allow to show message from javascript. <a href='http://commits.kde.org/messagelib/76703af546c7cd83e6268f6ea41cf70a99525c32'>Commit.</a> </li>
<li>Add data/jquery-ui.js. <a href='http://commits.kde.org/messagelib/a053d23bc7810cfe58f65b82c07f7738c3ead47b'>Commit.</a> </li>
<li>Look at if we are at bottom. <a href='http://commits.kde.org/messagelib/69d08f7708e7bd0d683f0d0f52140f4c4051662e'>Commit.</a> </li>
<li>Remove unused variable. <a href='http://commits.kde.org/messagelib/cc674647ac886d8ac7bbab43ec2e1d007b75768f'>Commit.</a> </li>
<li>RecipientLine: notify on focus change. <a href='http://commits.kde.org/messagelib/3beb253925ca46db7d235d58c9be94d9dc0942e1'>Commit.</a> </li>
<li>Remove comment. <a href='http://commits.kde.org/messagelib/f635e8f54f2a5c26e97391641b18579dde8e91a1'>Commit.</a> </li>
<li>Use all the time https. <a href='http://commits.kde.org/messagelib/d9501198386e673bd29bf08593350f5fb024c0c4'>Commit.</a> </li>
<li>Remove this settings. <a href='http://commits.kde.org/messagelib/caff582d472d47d311121fc21469bc99048355c6'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/messagelib/1e1e7572f174b389177818eeb51398081e30abef'>Commit.</a> </li>
<li>Clean up api. <a href='http://commits.kde.org/messagelib/de8d51d55d1ac56b6e1ba5065eeade0df31c8907'>Commit.</a> </li>
<li>Remove script from mail directly. <a href='http://commits.kde.org/messagelib/111feeb5c1c144e89d345eb5789445d2a1d9f041'>Commit.</a> </li>
<li>Add slot when we start loading. <a href='http://commits.kde.org/messagelib/85ebc5a815f7080e95d297df0b262b55351df8d5'>Commit.</a> </li>
<li>Disable some js feature. <a href='http://commits.kde.org/messagelib/dfc6a86f1b25f1da04b8f1df5320fcdd7085bcc1'>Commit.</a> </li>
<li>Add data test. <a href='http://commits.kde.org/messagelib/f0d8b1d6442c338a35c823dc3231f95f31bb6d11'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/8e69c157255520fff70b1cf95c7a84da5d233229'>Commit.</a> </li>
<li>Add new test apps. <a href='http://commits.kde.org/messagelib/6b283cfdd1e3f9bd63789cf5948c757babd2b696'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/98146a8d22b932b2541023bcc8b496cbbbdd7941'>Commit.</a> </li>
<li>Remove unused method. <a href='http://commits.kde.org/messagelib/e4114ae4b6b06ab516b0c01b90ea1a640374a9f0'>Commit.</a> </li>
<li>Forward event. <a href='http://commits.kde.org/messagelib/4faf450d323d9ae43a4cba9bcf70bdb0056bd5c4'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/9579bfa4d19552ed9904b209bd954c929bb26372'>Commit.</a> </li>
<li>Rename method. <a href='http://commits.kde.org/messagelib/7d5aac0f81b2ba1e5d23d037487364cef24fb840'>Commit.</a> </li>
<li>Fix scroll to position. <a href='http://commits.kde.org/messagelib/938a6e999c63fdc2d9b5f546a2c4c35efecd2754'>Commit.</a> </li>
<li>It works. <a href='http://commits.kde.org/messagelib/892cb8d1a00b216e5215205209c9145650b140ba'>Commit.</a> </li>
<li>Add debug info. <a href='http://commits.kde.org/messagelib/b6c6c3a2ebe52cc848b3ebb9581db9d846aaedd6'>Commit.</a> </li>
<li>Continue to implement configure dialog support. <a href='http://commits.kde.org/messagelib/73d920554d0edf03b411743e0aee4b84b3441a1f'>Commit.</a> </li>
<li>Add info about configure dialog support. <a href='http://commits.kde.org/messagelib/e1109e507ae4d45fcb1618008e40c5fbcf02d106'>Commit.</a> </li>
<li>Add support for configure dialogbox. <a href='http://commits.kde.org/messagelib/08a3302918460ed88641097f9bc02fb05162ed48'>Commit.</a> </li>
<li>Remove debug. <a href='http://commits.kde.org/messagelib/68ed22fc3e2344fc87b8835066f079f81d514856'>Commit.</a> </li>
<li>Adapt to new api. <a href='http://commits.kde.org/messagelib/07b9ee5f15670232280d1f813e5807dd03034e92'>Commit.</a> </li>
<li>Clean up plugins api. <a href='http://commits.kde.org/messagelib/a11cab3e7aacdf34b03dc59d92b7d273c91425ff'>Commit.</a> </li>
<li>Improve configure dialog plugin support. <a href='http://commits.kde.org/messagelib/1b05d8012a4e1399f401a8716d6f282df8f9e1e5'>Commit.</a> </li>
<li>Fix build with Qt 5.6. <a href='http://commits.kde.org/messagelib/04c992a972c28dd3efdeed9b3b6e2938826d67fa'>Commit.</a> </li>
<li>Allow to show or not viewerplugin. <a href='http://commits.kde.org/messagelib/713468d395efb7865e7f7857f2f0401f2732a64e'>Commit.</a> </li>
<li>Enable or not header style plugins. <a href='http://commits.kde.org/messagelib/7e8711123e00712a8494a2f699231dbcc5ace0a7'>Commit.</a> </li>
<li>Show or not networkpluginurlinterceptor. <a href='http://commits.kde.org/messagelib/3bc1cccc689756e94a449c176a34067e918224b9'>Commit.</a> </li>
<li>Allow to define if plugin is enable or not. <a href='http://commits.kde.org/messagelib/3887f1b38a8ed20801545f64fbcebc20e54a9ae8'>Commit.</a> </li>
<li>Add info about configure dialog support. <a href='http://commits.kde.org/messagelib/32323cd0e854469267f1833e8afcdf5d57563437'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/messagelib/8ac8a9852aa3d8814146a123b2b86f40de7c8b5f'>Commit.</a> </li>
<li>Rename method. <a href='http://commits.kde.org/messagelib/2e2c6087aaa2f70366a9b0d4472628cb28370fb9'>Commit.</a> </li>
<li>Adapt to new api. <a href='http://commits.kde.org/messagelib/33888e345a649990c200727e7c77e5588dac2914'>Commit.</a> </li>
<li>Move method to plugins directly. <a href='http://commits.kde.org/messagelib/a43aa28413227322cc74209cc19fbcaf55c33c74'>Commit.</a> </li>
<li>Add plugin to interface. <a href='http://commits.kde.org/messagelib/ae36c72023b5728be1a00ae325371433ad93443e'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/d88a60e5b10f95691faece868b34c9c616173658'>Commit.</a> </li>
<li>Use htmlhighlighter when necessary. <a href='http://commits.kde.org/messagelib/67685ec3877c45d06ba31de9cce71eab64aef219'>Commit.</a> </li>
<li>Remove unused include. <a href='http://commits.kde.org/messagelib/eb6ff67d51b76dd9afd9df064362a4d095ae0ca3'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/messagelib/8641f14c8bd5612c434fd1c7a49cd4b1efe81c8b'>Commit.</a> </li>
<li>Remove change spell checking color. <a href='http://commits.kde.org/messagelib/a55aad3fd2aefeed0b09be66c29064fa35e5c5a5'>Commit.</a> </li>
<li>Minor. <a href='http://commits.kde.org/messagelib/f3596416a66d4cf1f437ce209e9453fdbf7f6111'>Commit.</a> </li>
<li>Fix crash + activate theme. Repository instance must be keep alive. <a href='http://commits.kde.org/messagelib/91b27db9063f5565048fc71b5efb9aac58163a0a'>Commit.</a> </li>
<li>Add code from syntax-highlighting but it crashs for the moment. I need to investigate it. <a href='http://commits.kde.org/messagelib/2847f399e9adf6692db16334e0ea82a0963b591b'>Commit.</a> </li>
<li>Remove unused method. <a href='http://commits.kde.org/messagelib/48af0c033cc376beb3d26e15c8368d9a7fe6b8dd'>Commit.</a> </li>
<li>Remove AutoHideTabBarWithSingleTab as discussed. <a href='http://commits.kde.org/messagelib/a9b5c3127f83e8927934664266022ca0452174f1'>Commit.</a> </li>
<li>USe new connect api. <a href='http://commits.kde.org/messagelib/51c2a7ecbd98c40f60d89313a96936e56babf3ca'>Commit.</a> </li>
<li>Modernize Akonadi unittestenv. <a href='http://commits.kde.org/messagelib/5a9b8c55157862fc3d44c328674a302356a3bdc2'>Commit.</a> </li>
<li>We need config-messageviewer.h. <a href='http://commits.kde.org/messagelib/78de12de99964b8ea2cc8016d303921e4b7b196e'>Commit.</a> </li>
<li>As discussed last week move defaultgrantleeheaderstyleplugin here so we can live without kdepim-addons. <a href='http://commits.kde.org/messagelib/466efd8580f2abdea73bb4d6cfdd723d13f3e609'>Commit.</a> </li>
<li>Look at to use new syntax highlighting module. <a href='http://commits.kde.org/messagelib/17a97ef1e91c3f535b7820461f47cea082dae478'>Commit.</a> </li>
<li>Use kdecoreaddons macro. <a href='http://commits.kde.org/messagelib/956ac2dae9252be6a4a33d0917314711af774cc1'>Commit.</a> </li>
<li>Remove duplicate code. <a href='http://commits.kde.org/messagelib/21706a35f648a256ea62e44a8ad2d48e480cbaaa'>Commit.</a> </li>
<li>Use new PimCommon::PluginUtil::createPluginMetaData. <a href='http://commits.kde.org/messagelib/a31bf6f0c6690c697e42771c531ceb8fb1bac29b'>Commit.</a> </li>
<li>Add more specific message. <a href='http://commits.kde.org/messagelib/9472e661a6c38ffaa8151d30bf7c7d8f9f97d17a'>Commit.</a> </li>
<li>Remove warning. <a href='http://commits.kde.org/messagelib/a0bcfbb7bd850627b24c041fe79360623599525f'>Commit.</a> </li>
<li>Remove unused code. <a href='http://commits.kde.org/messagelib/529c6e5d22c18a6bc16c556d285aa0c1cbf42575'>Commit.</a> </li>
<li>Add enablebydefault. <a href='http://commits.kde.org/messagelib/7d81f8b35942cd2cc8997b9926fcbfde1c29a2ab'>Commit.</a> </li>
<li>Allow to enable/disable header style. <a href='http://commits.kde.org/messagelib/9056704d18c72afaf107126fe237de576864a318'>Commit.</a> </li>
<li>Remove new line. <a href='http://commits.kde.org/messagelib/52543b91f66ba18b753375516d7a32c9c8dff2c3'>Commit.</a> </li>
<li>Fix check order. <a href='http://commits.kde.org/messagelib/cea6420fb92e940f34805f4dd915d9f9b10cf9f8'>Commit.</a> </li>
<li>Rename class as discussed with david. <a href='http://commits.kde.org/messagelib/ce78ecf7a67ed0011d948c1be55e67835a70d974'>Commit.</a> </li>
<li>Fix typo. <a href='http://commits.kde.org/messagelib/1a42d6c9f8f32198f536f6c7044236302306433f'>Commit.</a> </li>
<li>Allow to disable webengine plugins too. <a href='http://commits.kde.org/messagelib/10636a37b7932140c0d3dc083beb5d27b8acd7f7'>Commit.</a> </li>
<li>Allow to enable/disable NetworkUrlInterceptorPlugin. <a href='http://commits.kde.org/messagelib/e00683f25de08f101bce7eb73210e49613f59596'>Commit.</a> </li>
<li>Port to new api. <a href='http://commits.kde.org/messagelib/dbdd09a329d8c7082b7ef0d2ef23c9f589dd15fc'>Commit.</a> </li>
<li>Load Plugin settings directly. <a href='http://commits.kde.org/messagelib/3fdbbaf1072f08a66da11ee1996bd9bddd2aacb2'>Commit.</a> </li>
<li>Use PimCommon::PluginUtil::isPluginActivated. <a href='http://commits.kde.org/messagelib/37c1120d70426a9e62dd9ae35faf946415a09ade'>Commit.</a> </li>
<li>Remove unused method. <a href='http://commits.kde.org/messagelib/7d1a6188146ccc07806064c16ffbfde385aa7351'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/messagelib/ee46041babbb546922a7b75467cea11272819f4a'>Commit.</a> </li>
<li>Use pluginId it's better than name. <a href='http://commits.kde.org/messagelib/f48c6cb2fcb5c674cbf82a398f6e91f2c57a5c66'>Commit.</a> </li>
<li>Add ViewerPluginData. <a href='http://commits.kde.org/messagelib/3ffb32a5a0b6eb1290508b390c4cea8fd3ed65b5'>Commit.</a> </li>
<li>Fix plugin group name. <a href='http://commits.kde.org/messagelib/3de6dfae970653a0dac5711aa7565bd210145554'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/messagelib/997a564063d492e5bbc3714689e1c6fe52064a01'>Commit.</a> </li>
<li>Get description from json file directly. <a href='http://commits.kde.org/messagelib/f9726198c0406d1afa51e18bee3bce5200fe5f77'>Commit.</a> </li>
<li>No need to link against KCalCore and thus KDELibs4Support anymore. <a href='http://commits.kde.org/messagelib/ff988d86926bc68dc80c0c938a7206244f7cbeb3'>Commit.</a> </li>
<li>Don't use includes from KDELibs4Support. <a href='http://commits.kde.org/messagelib/72293556a7c0c3be085ab48d46db9fca48838033'>Commit.</a> </li>
<li>Enable/disable by settings (We will configure it in future dialogbox settings in kmail). <a href='http://commits.kde.org/messagelib/9a2d66d25c6ec923680adda2ec2f0a1d7c6de9b1'>Commit.</a> </li>
<li>Load when plugin "isEnabledByDefault". <a href='http://commits.kde.org/messagelib/355fbd2bf8089da23eccd1e61c8a249c3ee90034'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/messagelib/7d0e004952d05e88c8babb3aa4522156ad532b48'>Commit.</a> </li>
<li>Add autotest when signature is based on command. <a href='http://commits.kde.org/messagelib/399ba6702b3d94945763b8e765eca8604235b12a'>Commit.</a> </li>
<li>Debug-. <a href='http://commits.kde.org/messagelib/ad11f0abda88242733f92dd491628371ba74b64d'>Commit.</a> </li>
<li>Add setPrintElementBackground. <a href='http://commits.kde.org/messagelib/04e47871451f1cb1502140a2309ea016724b194f'>Commit.</a> </li>
<li>Prepare autotest for signature based on command line. <a href='http://commits.kde.org/messagelib/cf4246e8c8be76a8f81a53e43856f4aa8bb3b5c2'>Commit.</a> </li>
<li>Forward method. <a href='http://commits.kde.org/messagelib/6e467dffdf11bd0c962b2fc8a5b835e64ccecea7'>Commit.</a> </li>
<li>Add support for printing PrintElementBackgrounds. <a href='http://commits.kde.org/messagelib/bc38aa480d412e498e6f0561305bae0bf51fbce9'>Commit.</a> </li>
<li>Add more autotests about signature. <a href='http://commits.kde.org/messagelib/bb7f48138f0ba14a8cdda8926147a4c1f3ff4cbc'>Commit.</a> </li>
<li>Coding style here. <a href='http://commits.kde.org/messagelib/c083d76bcd58a0d4279f6402b42c08f737702c29'>Commit.</a> </li>
<li>Remove old code. <a href='http://commits.kde.org/messagelib/c3779e6d73fec91d4f43f799fe6560263a94014d'>Commit.</a> </li>
<li>Add autotest on signature which loads contain from file. <a href='http://commits.kde.org/messagelib/3123bf27f973f14855db158a53f799e3cd98d145'>Commit.</a> </li>
<li>Remove dead code. <a href='http://commits.kde.org/messagelib/df496ed80059612b7f69b47615aa4d3d1eb75599'>Commit.</a> </li>
<li>Add support for printing based on qt5.8. <a href='http://commits.kde.org/messagelib/45e588ed7198a4e26924eaca25a968bde4d461cd'>Commit.</a> </li>
<li>Add parent. <a href='http://commits.kde.org/messagelib/6abc5ebb6b1611fd61de5480d5738a4c4751e49b'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/a8e8ee55ddba5d993821d5143ccda6bf63cd66a8'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/messagelib/f7aab82001798742d60224c2a3fc75b5369fa799'>Commit.</a> </li>
<li>Use Q_DECLARE_METATYPE for future autotest. <a href='http://commits.kde.org/messagelib/da237e3da20bf9cbc320c4efca0756952bb76839'>Commit.</a> </li>
<li>Remove commented code. <a href='http://commits.kde.org/messagelib/40cca23691acc916b2ddbf704b4bfa06a19245a1'>Commit.</a> </li>
<li>Add more autotest but it doesn't work. I need to investigate it. <a href='http://commits.kde.org/messagelib/a6cd5c38522022377520ad03aa8b361d80796396'>Commit.</a> </li>
<li>Fix signature + newline. <a href='http://commits.kde.org/messagelib/899acdb254e45ffa15682697176bf225e31ecc56'>Commit.</a> </li>
<li>Add autotest for testing AddNewLines (need to fix it). <a href='http://commits.kde.org/messagelib/ebf27857f025551de0c82d8162f3db84c900ed68'>Commit.</a> </li>
<li>Now autotest works. <a href='http://commits.kde.org/messagelib/1e9559073fafd8cfc894e5c2b0670a9bb49e2a81'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/messagelib/5d96d2ff7864dc415f61f0d0c86fbe21f80c22c7'>Commit.</a> </li>
<li>Fix forceautocorrection + selection. <a href='http://commits.kde.org/messagelib/a399dc8ae555aa17eb261cd35ff56e71d4bbea41'>Commit.</a> </li>
<li>Remove old code. <a href='http://commits.kde.org/messagelib/f53d5d275cff69e24756d353896546afb98bd5c7'>Commit.</a> </li>
<li>Allow to define body text. <a href='http://commits.kde.org/messagelib/dc647f3e4b2978f0e73c7180077c7b45d505af1a'>Commit.</a> </li>
<li>Continue to add autotest. <a href='http://commits.kde.org/messagelib/58daed3935bb492f05e2f7fbf58e72016daf5b71'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/messagelib/ec9ac81b6dc1232f03d2af78c9358c881060f740'>Commit.</a> </li>
<li>It doesn't reproduce my bug yet. <a href='http://commits.kde.org/messagelib/74e2278ac6225498f51bc6b5fdb256bdef6c0af6'>Commit.</a> </li>
<li>Improve autotests (fix some bugs about signature). <a href='http://commits.kde.org/messagelib/406f488a9cef1e33c604e4f78875b7db6fa038d7'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/messagelib/ea8b4163cd62787964316d33c67e804d87d0d986'>Commit.</a> </li>
<li>Add more comment. <a href='http://commits.kde.org/messagelib/efbbea3a0769084dbc90d360f3f1e3528e4178b9'>Commit.</a> </li>
<li>Test KIdentityManagement::Signature::End. <a href='http://commits.kde.org/messagelib/8cb90cd0a9592cd63c394dc6438545792baf4127'>Commit.</a> </li>
<li>Improve autotests. <a href='http://commits.kde.org/messagelib/20a43b9a69402b8efb3c0c7444ab22ae76a83c8f'>Commit.</a> </li>
<li>Add autotests. <a href='http://commits.kde.org/messagelib/a101d9fbab9c231417cce886503d5370a5c4bf43'>Commit.</a> </li>
<li>Add more autotest (fix signature addseparator support). <a href='http://commits.kde.org/messagelib/11bb694587a053b81b289db4d8db586abef42818'>Commit.</a> </li>
<li>Add more autotests for signature. <a href='http://commits.kde.org/messagelib/83c26ec4ca66eaf8f94aaa56cac20393949fee6c'>Commit.</a> </li>
<li>Start to add signature autotest. <a href='http://commits.kde.org/messagelib/9ed10fea0bf357abe0ad04b593c27094cd47ab49'>Commit.</a> </li>
<li>We need to reimplement webengineviewer print support here. <a href='http://commits.kde.org/messagelib/489fe768e831b9547b9b75146d19e1468d53b116'>Commit.</a> </li>
<li>We need to have this info here too. <a href='http://commits.kde.org/messagelib/7f3a9d3eae69d1aa9afe7c94788ec89c9a8d2481'>Commit.</a> </li>
<li>Fix variable name. <a href='http://commits.kde.org/messagelib/7d025c5f77586e994e874201a6ad0d1c2d9977ac'>Commit.</a> </li>
<li>Prepare to autotest add signature. <a href='http://commits.kde.org/messagelib/48421e57933bb143364979a48aad5d40fc6ae01c'>Commit.</a> </li>
<li>Fix end edit block. <a href='http://commits.kde.org/messagelib/6973109b20ad614cd6aa63c410d67dbafd06e1b9'>Commit.</a> </li>
<li>Add more autocorrection. <a href='http://commits.kde.org/messagelib/469742b86e6e88f70f5a6a1b709b9d0ea73d8bce'>Commit.</a> </li>
<li>Validate forceautocorrection with selection. <a href='http://commits.kde.org/messagelib/2130dfbcca036e9e0d467aaca9b89ce455075661'>Commit.</a> </li>
<li>Improve autotest. <a href='http://commits.kde.org/messagelib/a93e423bf8f54d21f51b1082612688d4e155b5e9'>Commit.</a> </li>
<li>Add test for selection text. <a href='http://commits.kde.org/messagelib/f1ae45fd034765df019081a9d54dfaed35155a26'>Commit.</a> </li>
<li>Comment for the moment selected text method. <a href='http://commits.kde.org/messagelib/5daaca99fc12cecf4c7976577817bbc8b4b395f0'>Commit.</a> </li>
<li>Add more autotests. <a href='http://commits.kde.org/messagelib/67a78e2c01b435027ec7b36dace33e6024b1acfa'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/messagelib/16cb2f107e5cf2acf0711ca9b4279bc846de904b'>Commit.</a> </li>
<li>Add autotests. <a href='http://commits.kde.org/messagelib/3c5e28c88cfceffb3cafb94258d3898310dec3c2'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/7fd28c37ddd4905c74323ef59427de6178d40c58'>Commit.</a> </li>
<li>Add edit begin/end. <a href='http://commits.kde.org/messagelib/153a79d3f72c51a072a651d658fdda0650b5665e'>Commit.</a> </li>
<li>Add support for selected text. <a href='http://commits.kde.org/messagelib/2ede9b2f2abc3e7e259d1a2520df238c27146ea4'>Commit.</a> </li>
<li>Don't autocorrect quoted line. <a href='http://commits.kde.org/messagelib/5668c06c060ba59274c55d93ff54ca39b77fd265'>Commit.</a> </li>
<li>Implement forceAutocorrect. <a href='http://commits.kde.org/messagelib/717e316c8da211e9b117d5c0e968ec4c1fc73425'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/5b38184696f93231c99097a79f35a9cc6493f030'>Commit.</a> </li>
<li>Make sure that we can use autocorrection. <a href='http://commits.kde.org/messagelib/f46c6efeddbb28ba464ce8e5d9b3930e989b3d06'>Commit.</a> </li>
<li>Add forceautocorrection. <a href='http://commits.kde.org/messagelib/9712cac51c4c7a9066a045401fb34addd5c602e8'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/messagelib/7d4043364cbf2adac508c6302dcbdec120f8d9c1'>Commit.</a> </li>
<li>Remove this code. <a href='http://commits.kde.org/messagelib/c2d9397b7ca4422dfe043c6f2f56e690e1dde48e'>Commit.</a> </li>
<li>Finally we need this variable. <a href='http://commits.kde.org/messagelib/757437d211cf176a20ee25e73cad59e6bf47f9ab'>Commit.</a> </li>
<li>Remove it (not used and in qt5.8 we will have real printdialog support). <a href='http://commits.kde.org/messagelib/d8c8042b88c57527aaf66a85bc5f25f4c377cacd'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/messagelib/4407b175a140a9e9dbed55ffc7718f4a9a0dc0b7'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/messagelib/0d3880497a77d6c2bf5fc43e07cd1a05c7149b9a'>Commit.</a> </li>
<li>Fix merging. <a href='http://commits.kde.org/messagelib/eac4edf1721dd0e529f431c4ef24b4c78e5a6278'>Commit.</a> </li>
<li>Clean up includes. <a href='http://commits.kde.org/messagelib/0c5e64ed193141edd63de7eb01a10e6d353e1856'>Commit.</a> </li>
<li>Add source to jquery.min.js to be free software compliant. <a href='http://commits.kde.org/messagelib/80c5569f4a05fb38aea79167566e90a14355a9a5'>Commit.</a> </li>
<li>Add charset so we can open correctly in browser. <a href='http://commits.kde.org/messagelib/ba0c250bd5ad295ec0a6e6397580727a5140568f'>Commit.</a> </li>
<li>Allow only one recipient per line in RecipientsEditor. <a href='http://commits.kde.org/messagelib/a84cfe5d03185ccf6cec0e6358fa3225d336d0ac'>Commit.</a> </li>
<li>Remove identitymanager. <a href='http://commits.kde.org/messagelib/42d2ecb22f57e242f783d9c478f8b0660df29f5f'>Commit.</a> </li>
<li>Allow to use kidentitymanager. <a href='http://commits.kde.org/messagelib/9f26a5d448ede4660e821b9e4932e17275d829fc'>Commit.</a> </li>
<li>Fix memory leak when deleting MessageViewer and its NodeHelper. <a href='http://commits.kde.org/messagelib/28b2cb85b596c0a112949b3ae2134fe5a6ea7c40'>Commit.</a> </li>
<li>Fix memory leak when deleting MessageViewer and its NodeHelper. <a href='http://commits.kde.org/messagelib/00283cb1f050fba17b5603038496271afc5277f9'>Commit.</a> </li>
<li>Set identitymanager for plugins. <a href='http://commits.kde.org/messagelib/aa1c25e29c2b3383206fbcfc4ae3aa40028e248c'>Commit.</a> </li>
<li>Do not render parts that are rendered by plugins several times. <a href='http://commits.kde.org/messagelib/7a390be1796480bfa5ee2d390f551527be056b65'>Commit.</a> </li>
<li>Render multipart/alternative with a invitation correctly. <a href='http://commits.kde.org/messagelib/d8d0f65a88ccf524b8d2fb88fe9d5ad07409fd52'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362958'>#362958</a></li>
<li>Remove global pgp-auto-encrypt config option. <a href='http://commits.kde.org/messagelib/01df5060d7771b60fb4923da495e1f44f2df42fc'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/messagelib/062461f11c67047bb05cecde6bf5b40722d15d37'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/bc4317433c4ecdc647f2558b1658595392770023'>Commit.</a> </li>
</ul>
<h3><a name='minuet' href='https://cgit.kde.org/minuet.git'>minuet</a> <a href='#minuet' onclick='toggle("ulminuet", this)'>[Show]</a></h3>
<ul id='ulminuet' style='display: none'>
<li>Fix message extraction. <a href='http://commits.kde.org/minuet/fab5fefa22eebd9a6c5617fadf0ebdf6feb99db3'>Commit.</a> </li>
<li>Fix translation issues in Android. <a href='http://commits.kde.org/minuet/b8e67e89d236016c8cbb5da3c57380eb9f9a4970'>Commit.</a> </li>
<li>Add documentation back. <a href='http://commits.kde.org/minuet/03ca0ac1112d1ed4e35070f57194e1d4424d8579'>Commit.</a> </li>
<li>Add some more android polishing. <a href='http://commits.kde.org/minuet/c268535d3959e21aa3e4b3e51933285a8efa0c8a'>Commit.</a> </li>
<li>Manually merge convergence branch. <a href='http://commits.kde.org/minuet/561406275f67de9567004860d00bcb9ff7174a49'>Commit.</a> </li>
<li>Pitch, Tempo and Volume using FluidSynth. <a href='http://commits.kde.org/minuet/e65e0a073c4d17f8155b7843bde4907fb858d474'>Commit.</a> </li>
<li>Fix master version. <a href='http://commits.kde.org/minuet/8022f65256bce4c8172d95bf18cf3db4ecbe04e0'>Commit.</a> </li>
<li>Use ecm variable for appstream metadata location to pick up new install path. <a href='http://commits.kde.org/minuet/2b32588db0f65d3bbe233e2771f9df1a6bfeae25'>Commit.</a> </li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Add encoding="ebcdic" to string structures. <a href='http://commits.kde.org/okteta/b455b93cb15fb3af078c48e70f326aa6efe6275e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/367863'>#367863</a>. Code review <a href='https://git.reviewboard.kde.org/r/128795'>#128795</a></li>
<li>Oktetapart.desktop: use better/proper MimeType=. <a href='http://commits.kde.org/okteta/6f1fc9b9ea42682bb980251dd7f21834b7b19aa7'>Commit.</a> </li>
<li>Typo fix. <a href='http://commits.kde.org/okteta/324cb928a137dcd81ea03913bc34c9f8baf9c491'>Commit.</a> </li>
<li>Proofread Okteta Handbook for 16.08. <a href='http://commits.kde.org/okteta/863ff7c95173348654f7c0fe67254f843eaf9c7c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128553'>#128553</a></li>
<li>Bump version to 0.20.60. <a href='http://commits.kde.org/okteta/eeb05c362fc737e8e81d4f1ffa007096cac13c98'>Commit.</a> </li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/okteta/f5e6359668d10c93949f920e7576bcd0c19b86d8'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/okteta/e3e131247a113ce08b4bbb18ea72411cc1cb6a8f'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Test: Be really patient. <a href='http://commits.kde.org/okular/964e3eb2e541ee5168734ef1ea1507799539b173'>Commit.</a> </li>
<li>Try other generators if opening fails with one generator. <a href='http://commits.kde.org/okular/ecf54e42035e7c96a0c0a435ba01d1b3483d032a'>Commit.</a> </li>
<li>Check all supported mime types the generator supports. <a href='http://commits.kde.org/okular/e81fb3d639400df03d47749e4c5532b32394b45c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129610'>#129610</a></li>
<li>Match the spring actually push things up. <a href='http://commits.kde.org/okular/1747b8b662557b8a0bf8e3806a104364045d7107'>Commit.</a> </li>
<li>Fix comparision between signed and unsigned. <a href='http://commits.kde.org/okular/4520ac7192f5c505e8fed5a14f66df85214f6620'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129285'>#129285</a></li>
<li>UI messages: fix the capitalization. <a href='http://commits.kde.org/okular/97b41df9357584d0bfce189a3ebbd7e958143819'>Commit.</a> </li>
<li>UI string: fix spelling. <a href='http://commits.kde.org/okular/82f1a728f9176ad0da518598be3b87a03edf3849'>Commit.</a> </li>
<li>Require Qt 5.6. <a href='http://commits.kde.org/okular/fa680b6c047b05a458bf9516fe7bc18f0d723ce6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372339'>#372339</a></li>
<li>Update/proofread Okular docbooks to kf5. <a href='http://commits.kde.org/okular/3f8d9f760c9dbd5c4969e07805da25f0fde3f826'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129326'>#129326</a></li>
<li>[KApiDox] update logo name in metainfo. <a href='http://commits.kde.org/okular/ae6adf2390e36d15754133fdf9f9a16c7bce7680'>Commit.</a> </li>
<li>Add metadata for kimgio, mobipocket, ooo, plucker, poppler, spectre, txt, xps. <a href='http://commits.kde.org/okular/4b7455396ef70eb40f3ccae93604f4d118cc8609'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129347'>#129347</a>. Fixes bug <a href='https://bugs.kde.org/363842'>#363842</a></li>
<li>Remove no longer existing include dir. <a href='http://commits.kde.org/okular/37aafeba93745167cecfb0c625e45ed2f1d40d49'>Commit.</a> </li>
<li>Add missing metainfo.xml files. <a href='http://commits.kde.org/okular/4c1a44bc40f6b66a5de82f4ffe467d19a8f009d4'>Commit.</a> </li>
<li>Use reverse domain-name scheme for metainfo.xml files. <a href='http://commits.kde.org/okular/9ae16afd978b5d173a8290c7bbdbc598aa7bfd94'>Commit.</a> </li>
<li>Use reverse domain-name for component ID. <a href='http://commits.kde.org/okular/1c9a9ce9477ee9ce00899b7f2d77cb8523fd47c1'>Commit.</a> </li>
<li>Add appdata file + metadata for chm, comicbook, djvu, dvi, epub, fax, fictionbook. <a href='http://commits.kde.org/okular/8666cacb926ab473bb1dc9025f2e9ebb5786c3af'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128404'>#128404</a></li>
<li>Update screenshots to kf5. <a href='http://commits.kde.org/okular/622cd794dbba0e7658e931aadc93e36e24bfeb56'>Commit.</a> </li>
<li>Fix minor typos. <a href='http://commits.kde.org/okular/e1535db5a05728ddd11f5490351c1b68476c93df'>Commit.</a> </li>
<li>Cleanup Messages.sh files for mobile. <a href='http://commits.kde.org/okular/ce4057c8b69ceab208ba35de9d24fff296d803aa'>Commit.</a> </li>
<li>Kill pre-release warning. <a href='http://commits.kde.org/okular/3c8464a4a0f7e104dd4318314393a630404320e0'>Commit.</a> </li>
<li>Test pixel pushing. <a href='http://commits.kde.org/okular/2aa006fa87240a89ff8446744ccd9f86a48c8dd0'>Commit.</a> </li>
<li>Fix autotest. <a href='http://commits.kde.org/okular/5db87d4d6fd4f02ffa6514639b95ac950432804d'>Commit.</a> </li>
<li>Fixed epub generator not being able to load resources on some files. <a href='http://commits.kde.org/okular/c8742b3f378e665a0c4e7a5baf7ccd22c236838e'>Commit.</a> </li>
<li>Fix xps url resolving porting regression. <a href='http://commits.kde.org/okular/aa38e8d1b78f2de63c317040151640ec514660df'>Commit.</a> </li>
<li>Remove threading in plucker. <a href='http://commits.kde.org/okular/6007168aa0f6bae49679e1d84d22519972ae435a'>Commit.</a> </li>
<li>Fix LibSpectre look-up. <a href='http://commits.kde.org/okular/505ef44479a5cbafc418f58ed2b98345a70701b4'>Commit.</a> </li>
<li>Use QMobipocket >= 2 (Qt5 based). <a href='http://commits.kde.org/okular/58b2f0e1f06c28377b506f564185431444e114a4'>Commit.</a> </li>
<li>Fix ms-its protocol lookup in kf5. <a href='http://commits.kde.org/okular/34ea79c2c31810ba0ce3ba9b17e03b33a28e6175'>Commit.</a> </li>
<li>Added missing overrides found by clang-tidy. <a href='http://commits.kde.org/okular/3c93b2523e46b645f2d4f130ebeb56863bdff9aa'>Commit.</a> </li>
<li>Improve Sidebar minimum size. <a href='http://commits.kde.org/okular/109c4a4090ac5550c167709a485f1aad85e5af6d'>Commit.</a> </li>
<li>Fix moc mess. <a href='http://commits.kde.org/okular/9e87989adebe6596bd26c23053f3b30a805e327d'>Commit.</a> </li>
<li>Fixes suggested by Clazy. <a href='http://commits.kde.org/okular/c71f3082521d17275e562ad296457681640bcf26'>Commit.</a> </li>
<li>Use Qt to detect the paper type and name. <a href='http://commits.kde.org/okular/dd126149d67a3a28c7040752e3bc65bfdc6c68fa'>Commit.</a> </li>
<li>Fix recoloring of inverted colors. <a href='http://commits.kde.org/okular/ff3cc05b6b0f1a7d6cbd8d80b5832667f973132d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368229'>#368229</a></li>
<li>Set the version to 1.0. <a href='http://commits.kde.org/okular/ebeeac8d72b258b516929344b43ceec3a57e06e7'>Commit.</a> </li>
<li>Make context menu on a bookmark menu action work again. <a href='http://commits.kde.org/okular/c75a2e52d96b559d7ab5f3a9ed4654887212aebb'>Commit.</a> </li>
<li>Remove declared but not implemented function. <a href='http://commits.kde.org/okular/fe18ff0ecd26a26fc0c16411b1c1efe30d77e181'>Commit.</a> </li>
<li>Fix wrong c&p. <a href='http://commits.kde.org/okular/32149802b9426729a20210491f90b5afdb76c9cc'>Commit.</a> </li>
<li>Remove the !this checks. <a href='http://commits.kde.org/okular/f943f9ee8b5cc5c2e2b049cf223040608891aa64'>Commit.</a> </li>
<li>Implement zooming with a pinch gesture on a touch screen. <a href='http://commits.kde.org/okular/9d90a08ef5e22d682a5a5004128f72d1a5a84aff'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128821'>#128821</a>. Fixes bug <a href='https://bugs.kde.org/366781'>#366781</a></li>
<li>Make the QML component plugin not crash when trying to be loaded without a QApplication. <a href='http://commits.kde.org/okular/afb5e01aed442c862b053fbb8cb9323fb20c2dbe'>Commit.</a> </li>
<li>Increase version in master. <a href='http://commits.kde.org/okular/75c387c62c8a1132f4101a1964c05dc8b034433c'>Commit.</a> </li>
<li>Rename method slotAutoScoll to slotAutoScroll. <a href='http://commits.kde.org/okular/c7bbf6f77c0128800e841b2e661d5c72e73b73b4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128938'>#128938</a></li>
<li>Fix build with Visual Studio 2015. <a href='http://commits.kde.org/okular/bfa28f1f64bfb8225db29d3f90c561b6e0603842'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128805'>#128805</a></li>
<li>Remove a spurious trailing semicolon that issued g++ warnings. <a href='http://commits.kde.org/okular/62f2cb99f689c01571b1f753af60b736f502f75d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128673'>#128673</a></li>
<li>Don't show mobile version in menus. <a href='http://commits.kde.org/okular/5e77764ac5608e87baf20043836ee0710f292734'>Commit.</a> </li>
<li>Remove kdelibs4support. <a href='http://commits.kde.org/okular/ff274fdecc9d872ba8a58ed57cc29c36c30b1e89'>Commit.</a> </li>
<li>Port away from obsolete QTest functions. <a href='http://commits.kde.org/okular/cbe859eeec6938140c32a213f98587350ed0f48e'>Commit.</a> </li>
<li>Fix misuse of QWeakPointer. <a href='http://commits.kde.org/okular/f2e9633ea7603df965eb7fdddf875fa8e950fb4d'>Commit.</a> </li>
<li>Use normal KDE compiler settings. <a href='http://commits.kde.org/okular/85078160fa34b1a04d663ebfc2ba7bf6dacf2ec9'>Commit.</a> </li>
<li>Use Okular's own print preview dialog for all documents. <a href='http://commits.kde.org/okular/decd4849a80ddd77addfd35013ac2ca0e6b00e7a'>Commit.</a> </li>
<li>Extend print preview dialog to handle .pdf previews. <a href='http://commits.kde.org/okular/2639b1e8ed45d1c21099debe5ab6cbdf37a279ef'>Commit.</a> </li>
<li>Stop leaking qfiledialogs. <a href='http://commits.kde.org/okular/57fd20e90e3e13a6fd7fdeb638a4d06316f5e809'>Commit.</a> </li>
<li>Ignore mimetypes without known globbing patterns. <a href='http://commits.kde.org/okular/1cd0bb269744013083a9c0ce8699842729cfcc30'>Commit.</a> </li>
<li>Merge mimetypes with same name, sort by name. <a href='http://commits.kde.org/okular/2eb0e01796de7a7e54568e9355e7b92f6c50e511'>Commit.</a> </li>
<li>Fix typo in previous commit. <a href='http://commits.kde.org/okular/e6e0d90a84a037dc20911cb9041327895da67437'>Commit.</a> </li>
<li>Make the open dialog a bit more usable without the Plasma QPA. <a href='http://commits.kde.org/okular/8f86818b5cf4bffb38ce9b3f725b4c38f20df598'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128435'>#128435</a></li>
<li>Open relative path again. <a href='http://commits.kde.org/okular/411bdee0ebaf67d603b2c8c9eab81768939e1bd5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128507'>#128507</a></li>
<li>Update docs for the new RTL direction option. <a href='http://commits.kde.org/okular/519fddd3a58233616c673b39bb1706282002f1c7'>Commit.</a> </li>
<li>Adding RTL reading mode feature to okular. <a href='http://commits.kde.org/okular/04908dba2f93842a0008ef1ba8c966621c615f9e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125397'>#125397</a>. Fixes bug <a href='https://bugs.kde.org/325650'>#325650</a></li>
<li>Port away from obsolete Qt functions. <a href='http://commits.kde.org/okular/09a99ba96bd7aafaf9d377d34ab52e2ea3d64eb6'>Commit.</a> </li>
<li>We need doctools. <a href='http://commits.kde.org/okular/68deb82a6fba64fcc0c75a5234a64144e7d57e9d'>Commit.</a> </li>
<li>Fix inhibiting of sleep on modern linux systems, port away from deprecated Solid methods. <a href='http://commits.kde.org/okular/c4287096a3226f63851be5be03f38acfb52ec32b'>Commit.</a> </li>
<li>Port away from deprecated KConfig API. <a href='http://commits.kde.org/okular/6875dc88cfcabf1092730d46b63afd6244a04a98'>Commit.</a> </li>
<li>Port away from deprecated Phonon API. <a href='http://commits.kde.org/okular/d8fdb149417ee88c5d098dc3b006785f46fd71b6'>Commit.</a> </li>
<li>Port away from deprecated KArchive method. <a href='http://commits.kde.org/okular/fc09e52fc30d3ddb6799310e4036ece314073da7'>Commit.</a> </li>
<li>Silence some warnings. <a href='http://commits.kde.org/okular/962af72cc1bf1234d274779c02bbb4e6d9ebc757'>Commit.</a> </li>
<li>Fix font color parsing in Plucker. <a href='http://commits.kde.org/okular/d9a31fa02ad92c663b3c5bd0fc5be38b09bc761f'>Commit.</a> </li>
<li>Stop using deprecated way to open browser. <a href='http://commits.kde.org/okular/5e78ca3df5cef47f109af3666bb8ce521d22b63c'>Commit.</a> </li>
<li>Fix warning about incompatible pointers in synctex parser. <a href='http://commits.kde.org/okular/f87a306ec1ae541a7e183d1468a81ba8bdd04f70'>Commit.</a> </li>
<li>Fix usage of string constants. <a href='http://commits.kde.org/okular/868bf74410decfe1fc71bb7289e8975fd87ce713'>Commit.</a> </li>
<li>Fix misuse of bool. <a href='http://commits.kde.org/okular/e7697a35d1d3be3f2e4f0ce87cc0d45678051965'>Commit.</a> </li>
<li>Re-add web shortcuts to context menu. <a href='http://commits.kde.org/okular/fe8e85f404d88f1d4e02a54e488b4b2ead4e049a'>Commit.</a> </li>
<li>Remove outdated compiler message. <a href='http://commits.kde.org/okular/d2f4f1c2d7114a8650d5d77845a5feb19a5bde4e'>Commit.</a> </li>
<li>No need to set component data anymore, we set component name. <a href='http://commits.kde.org/okular/f2dc74fc1981e22dbfdc69f0d822ae33b9d397d1'>Commit.</a> </li>
<li>Remove unnecessary and deprecated calls. <a href='http://commits.kde.org/okular/95bdeea2d793c6529c28f0df53eb020359769a2f'>Commit.</a> </li>
<li>(re-)Implement recoloring. <a href='http://commits.kde.org/okular/077258f2357126be0242fa79c58d45b9b25266ad'>Commit.</a> </li>
<li>Fix warnings about using struct for classes. <a href='http://commits.kde.org/okular/1e32012649d1843397fc713973946252b87e7c16'>Commit.</a> </li>
<li>Fix parsing of lilypond URLs. <a href='http://commits.kde.org/okular/a6ec5ea9de36a24f44998887d34f631bebd59864'>Commit.</a> </li>
<li>Fix sizing of properties dialog. <a href='http://commits.kde.org/okular/3bcfb63d5c0153bc1578c0df4f094ceb63274b7c'>Commit.</a> </li>
<li>Remove unnecessary build warning. <a href='http://commits.kde.org/okular/82cc716f709390a988aee23565070ba5cca8dc44'>Commit.</a> </li>
<li>Remove unnecessary build warning. <a href='http://commits.kde.org/okular/3a00f4e9c080e02f78c67c782ae921d876e19dd3'>Commit.</a> </li>
<li>Remove unnecessary .moc include. <a href='http://commits.kde.org/okular/dcba48cbd338e4408a2a07dbadc15e1f623190dd'>Commit.</a> </li>
<li>Silence unused member variable warning. <a href='http://commits.kde.org/okular/622fce954dcd035ae126bfd704ed7d24b31cf211'>Commit.</a> </li>
<li>Remove dead and unused code after porting to Qt5 QUrl. <a href='http://commits.kde.org/okular/ba1aeef06fc2c4c3c2aecc2b39552882628ef231'>Commit.</a> </li>
<li>Remove dead code that was added in 2004 but never used. <a href='http://commits.kde.org/okular/ab2fe5f17875aae70f5b4922eb4dae9bfba06a60'>Commit.</a> </li>
<li>Fix setting of icon in backend about dialog. <a href='http://commits.kde.org/okular/9014ef75cb2756040b75d665dcbedc5fb5629b71'>Commit.</a> </li>
<li>Clean up Part autotest, proper porting etc. <a href='http://commits.kde.org/okular/2e2432308f7508d41a418669eac803b4f8e7eaf0'>Commit.</a> </li>
<li>Remove usage of KGlobal from Part autotest. <a href='http://commits.kde.org/okular/b9b2c3bfae69e83e7042c52ab8c89f8c4ea3985e'>Commit.</a> </li>
<li>Remove KGlobalSettings include in sidebar. <a href='http://commits.kde.org/okular/2896b9c9a9ceb7a0b5ae5bc29f0cd248eb1372f7'>Commit.</a> </li>
<li>Fix updating of palette in sidebar. <a href='http://commits.kde.org/okular/ad8b35a69008d708c7163c76f7f0170c2c8d5373'>Commit.</a> </li>
<li>Don't use the toolbar font for the sidebar, toolbar != sidebar. <a href='http://commits.kde.org/okular/06edec49fd681cb6abe024df65d66b06f45dbf8f'>Commit.</a> </li>
<li>Use correct palette update signal in minibar. <a href='http://commits.kde.org/okular/76b976f87e806b7eb9f7dc56da4efe7e79832694'>Commit.</a> </li>
<li>Fix i18n issue missing setApplicationDomain. <a href='http://commits.kde.org/okular/e90b836fee88e92492c6b0c73c4f56d7a7cc0f98'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126445'>#126445</a></li>
<li>Revert "Fix i18n issue missing setApplicationDomain". <a href='http://commits.kde.org/okular/c3d2c8e1c45ab0c4d8394ed5ea4b65db074586c5'>Commit.</a> </li>
<li>Preview functions again. <a href='http://commits.kde.org/okular/3b608a66c94b61975b29c5665fafe01d6f7b5b9e'>Commit.</a> </li>
<li>Porting more files out of KDialog. <a href='http://commits.kde.org/okular/ef6f5ebc3b7063abdf8328777d17fd1d2482982d'>Commit.</a> </li>
<li>Uncomment printing option pdf2ps. <a href='http://commits.kde.org/okular/33122dd8252cb1a87f1708142332bc15b6c6f0d1'>Commit.</a> </li>
<li>Print preview is working again. <a href='http://commits.kde.org/okular/dfae933ae926226c78decda7654057afb56afa9e'>Commit.</a> </li>
<li>More ports out of KDialog. <a href='http://commits.kde.org/okular/630269a400bd2fd1f5a49388d2d37b44f70b8d3b'>Commit.</a> </li>
<li>Remove leftover from intermediary tests. <a href='http://commits.kde.org/okular/3d7bea0f806770f2a5bab5dd414a9328148f7299'>Commit.</a> </li>
<li>Port away from KButtonGroup. <a href='http://commits.kde.org/okular/6ff419ecba3a98f4460cbcdcf3c2185d4c965eaf'>Commit.</a> </li>
<li>This is Qt5 based, remove condition. <a href='http://commits.kde.org/okular/327b560e4e6aa6e889111ae26131d3cb672bf770'>Commit.</a> </li>
<li>Port most of part.cpp out of KDELibsSupport. <a href='http://commits.kde.org/okular/741c616c7a0483f8b232bfd756dcd8fcbf6ba314'>Commit.</a> </li>
<li>Port GotoPageDialog away from KDialog, KIntNumInput. <a href='http://commits.kde.org/okular/1f69f5fdcc038ed74e94b8c3f37db4da80a81487'>Commit.</a> </li>
<li>Revert "Improve page breaks and batch up cursor edits in epub generator". <a href='http://commits.kde.org/okular/1fc80ab58fa069122f379f3cfddab32772ff0c8b'>Commit.</a> </li>
<li>Fonts are scanned again by poppler5. <a href='http://commits.kde.org/okular/dd8fa351ce87be2b46243e5de641b9c104a024cc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128441'>#128441</a></li>
<li>Update deprecated KArchive method + remove unused variable in. <a href='http://commits.kde.org/okular/ec0b68a75672ad2413fefbed81ebe08c46080bd7'>Commit.</a> </li>
<li>Improve page breaks and batch up cursor edits in epub generator. <a href='http://commits.kde.org/okular/c110c65401a97877eb8b1cd3c6be04e2bdcbc5ec'>Commit.</a> See bug <a href='https://bugs.kde.org/359932'>#359932</a></li>
<li>Fix loading of embedded resources in epubs. <a href='http://commits.kde.org/okular/7490f37291b3424ee08d2a19f330e173792803bf'>Commit.</a> </li>
<li>Improve matching of generators. <a href='http://commits.kde.org/okular/89e5f175426aea6139c118d11e26386f60572c1b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/323213'>#323213</a></li>
<li>Fix MiniBar's adjusting to size changes. <a href='http://commits.kde.org/okular/5d9d98f46f67a0eff62e13ef3147acc712ac045d'>Commit.</a> See bug <a href='https://bugs.kde.org/357579'>#357579</a></li>
<li>Unbreak fetching of supporting generators. <a href='http://commits.kde.org/okular/3985577c908cbb8d1d9646cb2b127036b76635de'>Commit.</a> </li>
<li>Fix duplicate mimetype elimination. <a href='http://commits.kde.org/okular/f0323c26d0a5a1a494b3ed5d1ca6df452df3507e'>Commit.</a> </li>
<li>Make querying for KPart supported mimetypes a bit less broken. <a href='http://commits.kde.org/okular/99b5688725343321aad1caa232cca97066681bdc'>Commit.</a> </li>
<li>Add config migration. <a href='http://commits.kde.org/okular/39bff653a5d7a9c965bb534c7dcb6303bf1c208a'>Commit.</a> </li>
<li>Register Okular to DBus to get back the "open in new tab" feature. <a href='http://commits.kde.org/okular/e3e2b7c6e30073acd54766cba8a744058dfd06c1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128424'>#128424</a></li>
<li>Port part away from deprecated KArchive methods. <a href='http://commits.kde.org/okular/c8e3c0eb0159fdc0acf34fd5fa1775d40180ffd3'>Commit.</a> </li>
<li>Port part away from NetAccess. <a href='http://commits.kde.org/okular/b51d495930bdeeafd75a338abfe598f97f536ecd'>Commit.</a> </li>
<li>Port away from KStandardPaths. <a href='http://commits.kde.org/okular/4189304934bb49157307d7d477cd71d4c10ecb13'>Commit.</a> </li>
<li>Replace obsolete typedefs. <a href='http://commits.kde.org/okular/638fedb2eac6c573ce36952f5c4b1b28b5e0f519'>Commit.</a> </li>
<li>Port part away from KFileDialog. <a href='http://commits.kde.org/okular/9ac90274674300e61646e1a526d804f58735545a'>Commit.</a> </li>
<li>Fix low hanging unused parameters warnings. <a href='http://commits.kde.org/okular/d5bdca64d2c0df5950d2c6491df34dee16fcb4aa'>Commit.</a> </li>
<li>Fix source file encoding. <a href='http://commits.kde.org/okular/25a1915daf2a10d29e5f89bec6a8d369e5d02da6'>Commit.</a> </li>
<li>Add missing override declarations. <a href='http://commits.kde.org/okular/48d52d9bd189ce4aecb15a996055db26f13fed04'>Commit.</a> </li>
<li>Remove invalid null pointer checks. <a href='http://commits.kde.org/okular/d29ec080077962affda48e3518e5a49210dfc961'>Commit.</a> </li>
<li>Use proper qvariant cast. <a href='http://commits.kde.org/okular/004f67410ddc23d5e1e36d7a09fa872b5497b9d4'>Commit.</a> </li>
<li>Port away from obsolete QTabletEvent member. <a href='http://commits.kde.org/okular/d700bc626eb893c896d43be3566dbcd13478a937'>Commit.</a> </li>
<li>Port away from obsolete QRect/QRegion members. <a href='http://commits.kde.org/okular/ad6e3fe8503c050ae983e6220b1fe63d421ae878'>Commit.</a> </li>
<li>Get correct error message when failing to create temporary file. <a href='http://commits.kde.org/okular/44624bd52d1a39034575526a1fcb3964422e2f2a'>Commit.</a> </li>
<li>Fix build without implicit cast to/from ascii. <a href='http://commits.kde.org/okular/67e7ada3117a43ffae831e172b5b061888da0e4e'>Commit.</a> </li>
<li>Core: Fix build without implicit cast to/from ascii. <a href='http://commits.kde.org/okular/f05d42eac36a34e60487fbe207d64d0d9d771708'>Commit.</a> </li>
<li>Ui: Fix build without implicit cast to/from ascii. <a href='http://commits.kde.org/okular/ba4b9eb1a67e71bbe2af8ab7ca68bcdda9a0bc6b'>Commit.</a> </li>
<li>Autotests: Fix build without implicit cast to/from ascii. <a href='http://commits.kde.org/okular/3fcd69889622b21ca828536c05d9e79516ae0698'>Commit.</a> </li>
<li>Generators: Fix build without implicit cast to/from ascii. <a href='http://commits.kde.org/okular/2b473fedbdcb04d8cfb8d07a465456cfa2c9089c'>Commit.</a> </li>
<li>Chm, dvi: Fix build with compiler flags after removing kdelibs4support. <a href='http://commits.kde.org/okular/b512e668e003a35201c0dac01a06a0d98973186f'>Commit.</a> </li>
<li>Undo mistake in last commit. <a href='http://commits.kde.org/okular/a1d2d8cac7e1f1b1241b87e6574fcf53a592c8fa'>Commit.</a> </li>
<li>Add some missing explicit dependencies. <a href='http://commits.kde.org/okular/243fc02cb910d7fdde903c2441709c661e6c521e'>Commit.</a> </li>
<li>Port away from signal/slot keyword. <a href='http://commits.kde.org/okular/439ced872dadc566012725b30ef31e5745e22796'>Commit.</a> </li>
<li>Somehow this still needs KDELibs4Support. <a href='http://commits.kde.org/okular/6fd750973083e37185bee225d72ea7f07a15119b'>Commit.</a> </li>
<li>Fix build of mobipocket backend. <a href='http://commits.kde.org/okular/b2ffcab0433b40d5bd9c5afdd63ea3fbf45c2ebf'>Commit.</a> </li>
<li>Remove most KDELibs4Support linking. <a href='http://commits.kde.org/okular/32b62e919a6493cca9d5d2b272872b53f9b5e41d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127021'>#127021</a></li>
<li>Change kf5-warning text. <a href='http://commits.kde.org/okular/18b4cababc796ba0c8bde4fc6f6f977678510956'>Commit.</a> </li>
<li>Go at beginning of page when switching. <a href='http://commits.kde.org/okular/954c46ec46c348146321d8a791d0b4aba87b293f'>Commit.</a> </li>
<li>Joined buttons style on plasma style. <a href='http://commits.kde.org/okular/7fe208892dfe051549ce579fffefbd2fa9894d6c'>Commit.</a> </li>
<li>Move progressbar in MainView. <a href='http://commits.kde.org/okular/f4de5d1befa6ed2634bb8e5ed2e5e02c9feb1506'>Commit.</a> </li>
<li>Adjust look of thumbnails. <a href='http://commits.kde.org/okular/3b34e223c8c55b99fdf88e6bca866a3fcba2cfc5'>Commit.</a> </li>
<li>Minor layout fixes. <a href='http://commits.kde.org/okular/3c74f1acb0a59b0abc985c9390fad12d0bbd271f'>Commit.</a> </li>
<li>Better behavior for wheel zoom. <a href='http://commits.kde.org/okular/1d5853c2cb15dffd045996df83eb10e0815dfec4'>Commit.</a> </li>
<li>Restore horiz flicking. <a href='http://commits.kde.org/okular/5cc122f0214e0d5e64a73571f83eb57ab7779d4f'>Commit.</a> </li>
<li>Support zoom by ctrl+wheel. <a href='http://commits.kde.org/okular/4bb05056cea8daf6660b5a8cb4a0424496c38453'>Commit.</a> </li>
<li>Make sure horiz scrollbar is off when not needed. <a href='http://commits.kde.org/okular/6fe6e884d1b4133e92e35dc34fb3db71e1c5d6b7'>Commit.</a> </li>
<li>Port to kirigami. <a href='http://commits.kde.org/okular/eb1f73800e83b11e4a5ae93ac069893aa88f0eaa'>Commit.</a> </li>
<li>Mark this as not official release. <a href='http://commits.kde.org/okular/f9917aa9fe4274d8bce3418437ae8db917e121ec'>Commit.</a> </li>
<li>Avoid a crash occurring when pushing the components a little hard. <a href='http://commits.kde.org/okular/d01a5704bf14d0b1b9795512ce235a8abb680fc1'>Commit.</a> </li>
<li>Actually fix fax generator compilation with msvc2015. <a href='http://commits.kde.org/okular/8bcfc5d0b4d803275a301357bebc94eb2d09dedb'>Commit.</a> </li>
<li>Fix build with MinGW on Windows. <a href='http://commits.kde.org/okular/df0c41212fad2ade6f73b103e57062f99ae20de9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127328'>#127328</a></li>
<li>Use kcmshell5 for configure web shortcuts action. <a href='http://commits.kde.org/okular/1335d67259191d8b2949be3e1816f65075ea50ca'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126852'>#126852</a></li>
<li>Cmake: Port to FindPoppler from extra-cmake-modules. <a href='http://commits.kde.org/okular/568079625dd01ae70ac9c1073946830da4b9d7b6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127049'>#127049</a></li>
<li>Fix QUrl string encoding issue. <a href='http://commits.kde.org/okular/154c98fdaab2e593091fe644cfaeed96dd9fefaf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127042'>#127042</a></li>
<li>Make presentation widget tool bar follow the lineedit height. <a href='http://commits.kde.org/okular/db212c805fd5f8363806f24c0a33aecab6353dd3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126050'>#126050</a></li>
<li>Forms: Let checkboxes be unchecked. <a href='http://commits.kde.org/okular/aa2c28bc51b715433a6ad18fe08b748ba55d9a4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357743'>#357743</a></li>
<li>Fix for CUPS printing. <a href='http://commits.kde.org/okular/14c936ea1b0a9094b476da45160ebb23614f3a39'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127036'>#127036</a></li>
<li>Fix porting QUrl issues. <a href='http://commits.kde.org/okular/a704fbce94b6949c3a583ca8f28c63cb86638966'>Commit.</a> </li>
<li>Fix KTabWidget porting issue. <a href='http://commits.kde.org/okular/cbf47d1bb8540a3e2007ff2dacdb04bd91c97714'>Commit.</a> </li>
<li>Fix installation of kimgio plugin. <a href='http://commits.kde.org/okular/b6d3854e66717e7d73edb8e12120f5afe5aa3ab8'>Commit.</a> </li>
<li>Drop libkscreen dependency. <a href='http://commits.kde.org/okular/f42a3bad65200267cfe04cf584c203e70a3a6ec0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126913'>#126913</a></li>
<li>Fix mainshelltest (and as a side-effect docdata saving). <a href='http://commits.kde.org/okular/1b2de0d1d074769005db01b8de47e26c7e310057'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126192'>#126192</a></li>
<li>Remove kdelibs4support from the generator plugins. <a href='http://commits.kde.org/okular/37d562310266a85b21896cbe8ab333f0e7f680d5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126375'>#126375</a></li>
<li>Need new KF5 to get KPluginMetaData::mimeTypes. <a href='http://commits.kde.org/okular/db1c8ad75174e0eaf17f2202c915639bb5794e4e'>Commit.</a> </li>
<li>Let KDE*CompilerSettings change the policies in the project. <a href='http://commits.kde.org/okular/2c6cf868cf71f5e6dd7a1f8bc6c1abfe711145aa'>Commit.</a> </li>
<li>Make sure we don't pick up the new FindPoppler from e-c-m yet. <a href='http://commits.kde.org/okular/4930045ff1534b6d192d36786c0747f69107e1e7'>Commit.</a> </li>
<li>A message if no docs present. <a href='http://commits.kde.org/okular/5b5f0a0c8a4ee08372fd9e54286c55fb8782fcd4'>Commit.</a> </li>
<li>Fix parttest by increasing the wait duration. <a href='http://commits.kde.org/okular/449d727707be4b2d8e06cf41bedf489327889414'>Commit.</a> </li>
<li>Fix OkularDrawer appearance. <a href='http://commits.kde.org/okular/0772330f79fd9c5622c2326f019d22a1b7a353b2'>Commit.</a> </li>
<li>Add the Copyright information back to the JSON files. <a href='http://commits.kde.org/okular/00b15528a40531fc63cfa888436fe531052449e9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126193'>#126193</a></li>
<li>Bring back the about backend dialog. <a href='http://commits.kde.org/okular/93b94161f27e4fd23efc2d6643cd07c716c57676'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126193'>#126193</a></li>
<li>Port to new JSON plugin loading system. <a href='http://commits.kde.org/okular/2f9246ae42ce6276002641ac02d9c6b880bcf7a3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126193'>#126193</a></li>
<li>Fix i18n issue missing setApplicationDomain. <a href='http://commits.kde.org/okular/71950016aaaadf1a5f296b25e589400b71f4e9ff'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126445'>#126445</a></li>
<li>Add logging categories file for kdebugsettings. <a href='http://commits.kde.org/okular/b939800fb79f6781fd0c1ff8e4bcb537b735349f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126589'>#126589</a></li>
<li>Unify the naming of logging categories. <a href='http://commits.kde.org/okular/4c7c7dbd8c9f0954cd4d9975d389fc73e6440f6e'>Commit.</a> </li>
<li>Windows string fixes. <a href='http://commits.kde.org/okular/d0ecab47368288ec00318dccb6dfcd9fc79a8e8b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126379'>#126379</a></li>
<li>Add missing file. <a href='http://commits.kde.org/okular/9689a8eccd39469fa6c7ecd37b7ea12f8498f6e6'>Commit.</a> </li>
<li>Fix compilation of fax and comicbook generators with MSVC 2015. <a href='http://commits.kde.org/okular/355d592bfb6a817ce537f1b39a82ba7b0c4e99ca'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125791'>#125791</a></li>
<li>Build okularpart as SHARED library when using MSVC, as using MODULE causes errors when linking to okularpart.dll. <a href='http://commits.kde.org/okular/0928298a74087f1a42fc53f5eca35d0315d36c22'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125792'>#125792</a></li>
<li>Adapt to new components API. <a href='http://commits.kde.org/okular/2b4486a00de34cf0102997db8bb88462d4959de6'>Commit.</a> </li>
<li>Remove tests/data/file2.pdf as we have it in autotests now. <a href='http://commits.kde.org/okular/9b749abbc02f852ca6c66e84392084f7777ac850'>Commit.</a> </li>
<li>Fix PartTest::testSelectText(). <a href='http://commits.kde.org/okular/de6443d106b87c0036dc32adc8727af37b2f2683'>Commit.</a> </li>
<li>Make parttest pass by adding the file that it wants to open. <a href='http://commits.kde.org/okular/19c2c170bbe5833c6b17db7ac348496a8f42aec8'>Commit.</a> </li>
<li>Fix crash in parttest by failing the test if the file couldn't be opened. <a href='http://commits.kde.org/okular/5c566b35430fc33b21c3cdaf11e67870a0b107e9'>Commit.</a> </li>
<li>Set the KXMLGUI component name to okular. <a href='http://commits.kde.org/okular/ab1d3a36cce914e8a831cc702d4eda84ec014f99'>Commit.</a> </li>
<li>Set objectName for 'm_name'. <a href='http://commits.kde.org/okular/e5383ea2d6c25f628871a7af68022e95b62e8550'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126158'>#126158</a></li>
<li>Fix crash on close. <a href='http://commits.kde.org/okular/c793fb2703bbeaf2847663013b62850fe6d970d3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125890'>#125890</a></li>
<li>Make ViewerInterface to be exported unconditionally to fix linking errors with MSVC. <a href='http://commits.kde.org/okular/6fdc6e22a955d160690ed9ebd0230d0247bef9a5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125741'>#125741</a></li>
<li>Fix okularplugin linking on VS 2015. <a href='http://commits.kde.org/okular/c339bc5d0a28d21990ea4eb876703ddc8217a22f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125742'>#125742</a></li>
<li>Use QStringLiteral. <a href='http://commits.kde.org/okular/466eb79615ba313ff6ef418177a15a6961fbfb36'>Commit.</a> </li>
<li>Fix build: PlayOnce -> PlayLimited. <a href='http://commits.kde.org/okular/4087e54727554add7f3274dc14de132dcd157285'>Commit.</a> </li>
<li>Revert "Use QFileDialog from Qt 5 for "File -> Open" dialog.". <a href='http://commits.kde.org/okular/f8870e60f493dc31c004f17dc9e1e9121598eb7b'>Commit.</a> </li>
<li>Use QFileDialog from Qt 5 for "File -> Open" dialog. <a href='http://commits.kde.org/okular/2bbdb762eb7b57f89cd89140c55ff746b61b3451'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125683'>#125683</a></li>
<li>Implement qHash() for Okular::NormalizedRect. <a href='http://commits.kde.org/okular/b8d22401f26eba647e86706a1c88f912e42b5b5a'>Commit.</a> </li>
<li>Remove references to non existent id. <a href='http://commits.kde.org/okular/9d0f5e63121bb3d3176f13ec3db07c2078e9be7f'>Commit.</a> </li>
<li>Directly scale the content item when pinching. <a href='http://commits.kde.org/okular/c8838e94c9c6baba9ccc3bee0d8b5f8817ddb7d6'>Commit.</a> </li>
<li>Port to Documentview. <a href='http://commits.kde.org/okular/be805e906ea9dae6c31af65e57618d1c8f694b70'>Commit.</a> </li>
<li>A touchscreen optimized view for a document. <a href='http://commits.kde.org/okular/79622ee9a34930f156a7716cdd9a17e65adf5ec7'>Commit.</a> </li>
<li>Better pinch behavior. <a href='http://commits.kde.org/okular/709121c37cea63479e752362e5b09abff1b8fb66'>Commit.</a> </li>
<li>Fix and port the Table of Contents browser. <a href='http://commits.kde.org/okular/bb5c4291e28123f48519261327176e93642b06f5'>Commit.</a> </li>
<li>Tiny progressbar at bottom of the screen. <a href='http://commits.kde.org/okular/c8db5970524eb9676e6f87a1dcc030ec35294e17'>Commit.</a> </li>
<li>Better sizing of thumbnails. <a href='http://commits.kde.org/okular/883a55f92500f20dbd68d0f2b978cfa9f3623a95'>Commit.</a> </li>
<li>By default fit to page. <a href='http://commits.kde.org/okular/884b40287e3234e073727ca4ca01dd7fc4820ae5'>Commit.</a> </li>
<li>Toolbar to open sidepanels. <a href='http://commits.kde.org/okular/8d8665582c278f99d2627032ff3004c9c0c22569'>Commit.</a> </li>
<li>Browser in left panel. <a href='http://commits.kde.org/okular/933e72ec03ad9f3c1cd4b6baef576dc5be168219'>Commit.</a> </li>
<li>Restore the URL parsing behaviour in from before the KF5 port. <a href='http://commits.kde.org/okular/ab700b245355a2d42f7fcea1e62923fb2e65b880'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124738'>#124738</a></li>
<li>Fix picking of different view modes. <a href='http://commits.kde.org/okular/cf513f3dd61ceb01069bf8f0614ecb5401452a71'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124842'>#124842</a></li>
<li>Implement eraser tool for presentation view. <a href='http://commits.kde.org/okular/b44d70a6f2d7ab1b508c203764be4170567a4d83'>Commit.</a> Implements feature <a href='https://bugs.kde.org/343774'>#343774</a>. Code review <a href='https://git.reviewboard.kde.org/r/124689'>#124689</a></li>
<li>Use correct target name. <a href='http://commits.kde.org/okular/2a7fda155b749338d1951508cd52f27069d1f66c'>Commit.</a> </li>
<li>Make the drawing tools be actions instead of tool buttons. <a href='http://commits.kde.org/okular/d8f9800d2834b00dd4b6c824834453149dea3fe0'>Commit.</a> </li>
<li>Implement drawing quick selection tool for presentation mode. <a href='http://commits.kde.org/okular/81f80ac3a4c7211fb0a8cd89dc8da27f6e47780e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124687'>#124687</a></li>
<li>Add basic support for RichMedia annotations in PDF files. <a href='http://commits.kde.org/okular/8b603c174dc288dcd83e19afb075853e459a82a7'>Commit.</a> Implements feature <a href='https://bugs.kde.org/326230'>#326230</a>. Code review <a href='https://git.reviewboard.kde.org/r/124612'>#124612</a></li>
<li>Remove check for Qt < 4.7. <a href='http://commits.kde.org/okular/e60890896146118973e1b0b824433f1b16527193'>Commit.</a> </li>
<li>Port away from KLocale. <a href='http://commits.kde.org/okular/64afe09e883f09a320f789db415524bd5325a242'>Commit.</a> </li>
<li>K_GLOBAL_STATIC -> Q_GLOBAL_STATIC. <a href='http://commits.kde.org/okular/81d4b984a4b88bb8ffcaddebe62d21adb456fa75'>Commit.</a> </li>
<li>Use QTRY_VERIFY/QTRY_COMPARE instead of qWait() loop. <a href='http://commits.kde.org/okular/461c963cb8a63c68beb463a87f3944d0a59e98e7'>Commit.</a> </li>
<li>Don't add "Speak Text" when we don't have support. <a href='http://commits.kde.org/okular/ad333314bce6b54acdcb1e813d52243fbccc36b1'>Commit.</a> </li>
<li>Use explicit. Q_DECL_OVERRIDE. <a href='http://commits.kde.org/okular/014b712359db87d37d5061de56a3aaf93825d0cf'>Commit.</a> </li>
<li>Use Q_NULLPTR. <a href='http://commits.kde.org/okular/6fab218de945c0493cf1337f5322bfa419d13ead'>Commit.</a> </li>
<li>Make sure we run the okular executable from the build directory. <a href='http://commits.kde.org/okular/771fb0de56250ff9171c110d986b6c97d5909725'>Commit.</a> </li>
<li>Remove copy of qpagesize.cpp. <a href='http://commits.kde.org/okular/22b6d4551de18367e0c390f1051ba9b9edc42a77'>Commit.</a> </li>
<li>Make generatorstest work without installing first. <a href='http://commits.kde.org/okular/437b2d4a3abdb7ddfbd69d19ca275583bc184258'>Commit.</a> </li>
<li>Don't rotate images with Qt 5.4.0 and 5.4.1. <a href='http://commits.kde.org/okular/bb22f7d3f1307ec8eff837a13bcaa930c38ab08a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123895'>#123895</a></li>
<li>Ensure KImageIO test always finds the currently built generator. <a href='http://commits.kde.org/okular/824a35ced051842a1518b95146042d3567a89ba1'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123895'>#123895</a></li>
<li>Rename all the active specific desktop files. <a href='http://commits.kde.org/okular/1dd033d91450cc8708d3b4b42cbaea23bdb90cbc'>Commit.</a> </li>
<li>Rename from active to mobile. <a href='http://commits.kde.org/okular/5ba5e2189c67a115a708d3e9b0933dcae938b8c4'>Commit.</a> </li>
<li>Revert needless KF5 dep increase. <a href='http://commits.kde.org/okular/c05f26c204b7572d414633c029e0db06261c3e38'>Commit.</a> </li>
<li>Use Q_DECL_OVERRIDE. <a href='http://commits.kde.org/okular/e03e6f6863a5771bb244ff2c9296c00a58308adc'>Commit.</a> </li>
<li>Add Q_DECL_OVERRIDE. <a href='http://commits.kde.org/okular/a8c13a9e1dd58e2a13e98a8b966bedc41cea1ffd'>Commit.</a> </li>
<li>Use QVector here too. <a href='http://commits.kde.org/okular/26e4f8de60075d7ea93c3a0dde511f26613316e6'>Commit.</a> </li>
<li>Use QVector. <a href='http://commits.kde.org/okular/3c8daf3068a7049e438650644edc81db32558fd2'>Commit.</a> </li>
<li>Add explicit. <a href='http://commits.kde.org/okular/3102cd86657172bea61486bfa7a50fd361d1f0c0'>Commit.</a> </li>
<li>Restore page shadows. <a href='http://commits.kde.org/okular/2d21f8a6b5bfcded99c9af273d1de2df7568225a'>Commit.</a> </li>
<li>Set the contents size as the full size. <a href='http://commits.kde.org/okular/511054c4232873705ca8430148de6aed42add8e8'>Commit.</a> </li>
<li>Fix crash on startup when okularpart can't be found. <a href='http://commits.kde.org/okular/d5bad89a51e0f957e3f6bc0764038ce4dec68e05'>Commit.</a> </li>
<li>Uncomment add_subdirectory(doc). <a href='http://commits.kde.org/okular/25335614180db72ab62c58c056bcc36043c20039'>Commit.</a> </li>
<li>Remove nonexistent property. <a href='http://commits.kde.org/okular/73d1d3bea2ebb9b6b0c73b2a8014e0ea1734215b'>Commit.</a> </li>
<li>Port away from ViewSearch. <a href='http://commits.kde.org/okular/f3698b6d69e4a2b66a17a6f7e30075d5a12ae958'>Commit.</a> </li>
<li>Add Q_DECL_OVERRIDE. <a href='http://commits.kde.org/okular/13ad9aa53b8e9629658f2656b3ff3480eb103117'>Commit.</a> </li>
<li>Add ';'. <a href='http://commits.kde.org/okular/70159dc61ff93a5a9adf2956a8fe4044a400b4e2'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/okular/adde882203468caab005a026c1ba0dadcac82171'>Commit.</a> </li>
<li>Port QCursor. Use QStringLiteral. <a href='http://commits.kde.org/okular/4a1c9687cff7ac7bb0521e526183d9b801070f4e'>Commit.</a> </li>
<li>Print out better error message if a plugin can't be loaded. <a href='http://commits.kde.org/okular/94b95d8fdaa085ff58c8611ede5c947ee3c2374a'>Commit.</a> </li>
<li>Fix url creation in 'Document::processSourceReference'. <a href='http://commits.kde.org/okular/f98eb936d51142408999173290797601573be407'>Commit.</a> </li>
<li>Generate settings.cpp from the kcfgc file. <a href='http://commits.kde.org/okular/991e11e81b9d3d48f45c021527b5f9bd0b38dffb'>Commit.</a> </li>
<li>A simple file browser ui. <a href='http://commits.kde.org/okular/43e6d63e9621c618f77367d3d6bd3828db42c118'>Commit.</a> </li>
<li>Directly use needed files from ui. <a href='http://commits.kde.org/okular/9c9a928e12bf42943d0b3a83712654c6d5ccb82a'>Commit.</a> </li>
<li>S/auto_ptr/unique_ptr/. <a href='http://commits.kde.org/okular/abd30fb774f6d0cd5abf93019f652c0ca54778f4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124027'>#124027</a></li>
<li>Set the CMake target name from Okular::okularcore to Okular::Core. <a href='http://commits.kde.org/okular/5ecffc246b6e00441386cbd0c107a19432ffe3b9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124381'>#124381</a></li>
<li>Make the KF5 okular library coinstallable with the KDE4 version. <a href='http://commits.kde.org/okular/f9b90348ec3684c2ca3a51386bf31d8696f3b2f5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124381'>#124381</a></li>
<li>Install Okular5Config.cmake instead of OkularConfig.cmake. <a href='http://commits.kde.org/okular/ed352a06fb918f778e022e6c17d919921f204707'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124381'>#124381</a></li>
<li>Use KPluginFactory::createInstance<Okular::Generator>(). <a href='http://commits.kde.org/okular/c2fc0199b8f7cf1f0a4bfee0568fa68ec8d1d6ab'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123466'>#123466</a></li>
<li>Add Q_INTERFACES( Okular::Generator ) to all plugins. <a href='http://commits.kde.org/okular/1049dc188a6138b0b28cb526bb7e513d61e95277'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123466'>#123466</a></li>
<li>Add a test to see whether generators can be loaded. <a href='http://commits.kde.org/okular/8059e5b686cef4b4a8aab1f62d92255556a3ec09'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123466'>#123466</a></li>
<li>Use ecm_add_test correctly. <a href='http://commits.kde.org/okular/0ef5903c73fbf074317620ef739cf6fb026969a6'>Commit.</a> </li>
<li>Try to fix Mac/clang build. <a href='http://commits.kde.org/okular/3f55cc1e9084ae39941d4ba3030e1dd96463acb9'>Commit.</a> </li>
<li>Fix porting mistake on next/prev page shortcuts. <a href='http://commits.kde.org/okular/8452e86b657f05d92f5e2a8dae6e179b5dd2ceb3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347109'>#347109</a></li>
<li>Make KActivities a required dependency. <a href='http://commits.kde.org/okular/c46b08c23f12f9819e93bd67b7147b1964cd7e8a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123249'>#123249</a></li>
<li>Fix the export header not being found in interfaces/*.h. <a href='http://commits.kde.org/okular/dcf4d93abb79b3f8c38f32a4d1e32e672e2dff4c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123363'>#123363</a></li>
<li>Use generated version and CMake config files. <a href='http://commits.kde.org/okular/ac258e49f1d7efe5fe950776ce334a9107923b07'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123314'>#123314</a></li>
<li>Remove unneeded target_include_directories(). <a href='http://commits.kde.org/okular/4a1c199e8eb687f6df8de6a4e65e9368db1c9ddf'>Commit.</a> </li>
<li>Fix INTERFACE_INCLUDE_DIRECTORIES for okularcore library. <a href='http://commits.kde.org/okular/475be2073adf71d70ded4c961fe638e988027fd5'>Commit.</a> </li>
<li>Be explicit about the target dependencies. <a href='http://commits.kde.org/okular/cfc82ee9b0684da2281db0ac81583c77f4bac74f'>Commit.</a> </li>
<li>Use QThread::msleep() instead of platform-specific sleep functions. <a href='http://commits.kde.org/okular/0436b34a985c3440efdd28c6ab5b9098ead89aef'>Commit.</a> </li>
<li>Use the new kpackage-based runtime for the QML app. <a href='http://commits.kde.org/okular/af3b4cba17a25fd866f15886eb52a04ef21bae2f'>Commit.</a> </li>
<li>Fix include dir and library search. <a href='http://commits.kde.org/okular/9e603cc9239fc26879cb7a95548572aa35162f53'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123176'>#123176</a></li>
<li>Fix includes. <a href='http://commits.kde.org/okular/5d727ee9657058e275c256706fa9d34655e62885'>Commit.</a> </li>
<li>Port to QCDebug. <a href='http://commits.kde.org/okular/25684630616bd3d9dc2fac70ad7cf3c59876651a'>Commit.</a> </li>
<li>Rename icons. <a href='http://commits.kde.org/okular/629882497b9584ebba6de5834e524f2026cec35f'>Commit.</a> </li>
<li>Kdelibs4support--. <a href='http://commits.kde.org/okular/080dba35b2b29446605b7f873b8ed2e7790ddb2a'>Commit.</a> </li>
<li>USe new connect api. <a href='http://commits.kde.org/okular/ae30a7fc5124cdb88979b1f2c94228152357c39d'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/3580f0089b30ed5916b769d23029abcab6c60ad1'>Commit.</a> </li>
<li>Reactivate it. <a href='http://commits.kde.org/okular/778f5bb9cb8faa81f981ead802f10f2a66953e44'>Commit.</a> </li>
<li>Rename icons. <a href='http://commits.kde.org/okular/855d2df586d385a319bea1760f915d7f7adbb262'>Commit.</a> </li>
<li>Fix rename icons. <a href='http://commits.kde.org/okular/6e01127177e52cd49705bad0e04f6723d68f90aa'>Commit.</a> </li>
<li>Rename icons. <a href='http://commits.kde.org/okular/ffbf0b82f6cac4a5fc9d807dc56e6e03ef0232e7'>Commit.</a> </li>
<li>Fix warning. <a href='http://commits.kde.org/okular/840355dc780e58810a4c5798b3c958dbc35f3047'>Commit.</a> </li>
<li>Kdelibs4support--. <a href='http://commits.kde.org/okular/c50c3d5332bdeab9ce4d53dddefdfd5963e6ad9a'>Commit.</a> </li>
<li>Don't create action when we don't have qtspeech. <a href='http://commits.kde.org/okular/8fad6ebe09048cbcfe2bec3d365d026ab1a1ddf8'>Commit.</a> </li>
<li>Fix build of qml component. <a href='http://commits.kde.org/okular/587e25fa8f3c2bad3c1b76b596d82d9e24f839e1'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/da040cbd46a52eccfecf3dcb2e5b711783222105'>Commit.</a> </li>
<li>Use new KDE_ macro. <a href='http://commits.kde.org/okular/5e984303a49911db651c3601f461172e9a3f6410'>Commit.</a> </li>
<li>Fix build with Qt 5.5 branch. <a href='http://commits.kde.org/okular/f847d716a00a597c53d0f75b8668a34294f4440e'>Commit.</a> </li>
<li>Fix install icons. <a href='http://commits.kde.org/okular/90b2f6c62c63799d7b21c9da748c07dbda1a2006'>Commit.</a> </li>
<li>Fix format of zoom level percent. <a href='http://commits.kde.org/okular/0b59e83530a6c8893a5d810ba8c3704627583615'>Commit.</a> </li>
<li>Reenabled text to speech using QtSpeech as an optional dependency. <a href='http://commits.kde.org/okular/61554c833ed0c009cf977de7d9b0984221cc9e17'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122553'>#122553</a></li>
<li>Fix includes. <a href='http://commits.kde.org/okular/5c85b83a8f45503a5f08f24814da45adc60d11a4'>Commit.</a> </li>
<li>Port to new connect api. <a href='http://commits.kde.org/okular/c4f224f1e8f31c374da33f83c4998641be7e7769'>Commit.</a> </li>
<li>Use co-installable qca-qt5 version. <a href='http://commits.kde.org/okular/e176acdc4013ee0d4b44c2d58404a7aebf073160'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122155'>#122155</a></li>
<li>Fix build and an unintended KScreen change. <a href='http://commits.kde.org/okular/dc06e7265721b1ca6f91be4ee42ce17b74ec8da3'>Commit.</a> </li>
<li>Port away from KLocale. <a href='http://commits.kde.org/okular/e9ae1d99afc16d62faa27a2b5dbcdb4eed58548e'>Commit.</a> </li>
<li>Port KUrl -> QUrl. <a href='http://commits.kde.org/okular/fd6a35486e60f5f727fb70e05403b650fc13163c'>Commit.</a> </li>
<li>Fix/restore the Selection Tools functionality in the toolbar. <a href='http://commits.kde.org/okular/ec8c2900ff518330a85f67f9f13bde50db224540'>Commit.</a> </li>
<li>Make it link. <a href='http://commits.kde.org/okular/413bb725f5b82e3735c6c1d5c9d51cc12f9ca09d'>Commit.</a> </li>
<li>Lik to okularcore and okularpart instead of including the settings code multiple times, which breaks the exports. <a href='http://commits.kde.org/okular/49c28f52dfc8391781a2b262092ed8e7a01e9111'>Commit.</a> </li>
<li>Fix connect on windows. <a href='http://commits.kde.org/okular/dee5b8b00634c9293b292c959ecb83b3e448f40e'>Commit.</a> </li>
<li>Fix build. <a href='http://commits.kde.org/okular/71dd075437a4ddb6fa60e23ee6806fd9501e9d0b'>Commit.</a> </li>
<li>Port qca support. <a href='http://commits.kde.org/okular/f6df6731625979ac74a19aa276a70fdc91b53d54'>Commit.</a> </li>
<li>Fix cmakelists.txt. <a href='http://commits.kde.org/okular/73ccfdefb72b7e671004e5a0cfb95330e2634e78'>Commit.</a> </li>
<li>Remove include moc. <a href='http://commits.kde.org/okular/0cbd97ced63f2abd9de219d44a7ba41af03b64ba'>Commit.</a> </li>
<li>Port the QML part. <a href='http://commits.kde.org/okular/78d8b1f6050205e0bd6c79a2a07788613c0fa961'>Commit.</a> </li>
<li>Initial port of QML components to QtQuick2. <a href='http://commits.kde.org/okular/3d50fec102acd5d902bf40029fe20084ad7ecc5e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/121326'>#121326</a></li>
<li>Fix build. <a href='http://commits.kde.org/okular/aad0211c10ec50add500c85bacf61acd09ec4410'>Commit.</a> </li>
<li>Fix make install. <a href='http://commits.kde.org/okular/6924fa4ef0b1f14975ded6d072447e34c535f2d4'>Commit.</a> </li>
<li>It uses QUrl. <a href='http://commits.kde.org/okular/f95258ec1db72e06f6496eddadad14dec8490b57'>Commit.</a> </li>
<li>Port to qCDebug. Autogenerate export headers. <a href='http://commits.kde.org/okular/5da7c5f77d50b08fd3e2dac020de41e0813d92fc'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/8457b66b38dd329ac3f3ecda8a5aa3408ea36cfb'>Commit.</a> </li>
<li>Fix icons. <a href='http://commits.kde.org/okular/98eb2833642a83372c5def6bfb372f21b566e51d'>Commit.</a> </li>
<li>Rename to autotests. <a href='http://commits.kde.org/okular/5eea4fd07389674482df0684a57ed29ec4214212'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/1f5ffad2218dd507efc6f504f3fee21c974f8530'>Commit.</a> </li>
<li>Port to QDialog. <a href='http://commits.kde.org/okular/127175c3ea1a88aacc0014eec2040d44499ef3a8'>Commit.</a> </li>
<li>Kdelibs4support--. <a href='http://commits.kde.org/okular/8e7429ddcc9767a3b215a99e4ac7f338ef77c7d3'>Commit.</a> </li>
<li>Port to QSpinBox. <a href='http://commits.kde.org/okular/2910f82a04ab994379ab60c5860e526f93da8456'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/603d8b19c950503cb03d71ec2cd3fa69b1b09c81'>Commit.</a> </li>
<li>Kdelibs4support--. <a href='http://commits.kde.org/okular/16f13c04d20f4981957d4bbd332666e13e275782'>Commit.</a> </li>
<li>Fix debug, QStandardpath. <a href='http://commits.kde.org/okular/f59383196de140c87e4158c62c848f03dcf62ee9'>Commit.</a> </li>
<li>Kdelibs4support--. <a href='http://commits.kde.org/okular/8f8b70d893f29a8ac4015a02037deed0375fb660'>Commit.</a> </li>
<li>Kdelibs4support--. <a href='http://commits.kde.org/okular/2d8df4584513cba75d5be49748a5fbe59696bd4f'>Commit.</a> </li>
<li>Port from KTempDir to QTemporaryDir. <a href='http://commits.kde.org/okular/b98cea80a97e48c1d13a3b96bb78f854d8090e29'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/4ee83e6a7719da5a9c84f97b193b9155e13e1c6b'>Commit.</a> </li>
<li>Port to qCDebug(). <a href='http://commits.kde.org/okular/f762d8a8c495c9f0d85c795b68b02bf0d93ea88b'>Commit.</a> </li>
<li>Remove not necessary fule. <a href='http://commits.kde.org/okular/953a731c4bb172ba6db5a7bccbdba121f4d61d1a'>Commit.</a> </li>
<li>Remove not necessary file. <a href='http://commits.kde.org/okular/0d96400041ab72b9c0382af23d9644e61996ad3e'>Commit.</a> </li>
<li>Port to QStandardPath. <a href='http://commits.kde.org/okular/c0e625605ddc84a3397bdc13fd84c9afcdd5ffbb'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/60f8d7b7123f42f3a2eaac473c824a1c8ca14646'>Commit.</a> </li>
<li>Port KSharedPtr. <a href='http://commits.kde.org/okular/7098b044c9ffee7f9d4451103580e61af65f2416'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/21360d6b7af16fae566782e2cf99c1c6b0d82441'>Commit.</a> </li>
<li>Not necessary. <a href='http://commits.kde.org/okular/e11caac3965b029a593ed95315c0ea4f52473216'>Commit.</a> </li>
<li>Port to new connect api. <a href='http://commits.kde.org/okular/0a8b1ca098aba7155aa93608d4469be29e712431'>Commit.</a> </li>
<li>Make it compile. <a href='http://commits.kde.org/okular/657c07ed2028cbd073b933b80675d41d7aa5ef01'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/17b7049c69cb783131941c2e7dc2dadcb0124614'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/3b4ee788b8b25183ab61882d379c0fe818d1d6a0'>Commit.</a> </li>
<li>Fix shortcut conflict between "Save as" and "Save copy as". <a href='http://commits.kde.org/okular/ef8d018a721eeff205740f271cc8e83a14bb0fb0'>Commit.</a> </li>
<li>Remove %i from .desktop files which added --icon to exec line. <a href='http://commits.kde.org/okular/83299b95328d9f3021666c8793994ec553cd8c73'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/d3a3b2df25d14dbfd68fa8f12cead8f82ba58fbd'>Commit.</a> </li>
<li>Port to QStandardPaths. <a href='http://commits.kde.org/okular/08013f87d0a498ce00dc6c6613db70354ec70f65'>Commit.</a> </li>
<li>Reactivate it. <a href='http://commits.kde.org/okular/49129b788a5e0981697e8a45cb9415327f05ed32'>Commit.</a> </li>
<li>Use modern macro. <a href='http://commits.kde.org/okular/4ec78fa5df29bdf4da9e258aa816e8fcc2063d41'>Commit.</a> </li>
<li>Use new Q_OS_* macros. <a href='http://commits.kde.org/okular/d852a7bee096850a698c79dbb41a38d47332a442'>Commit.</a> </li>
<li>Fix cmakelists.txt. <a href='http://commits.kde.org/okular/6300c30fea213953fa2a052db7cdbfe09bdcab0e'>Commit.</a> </li>
<li>Minor. <a href='http://commits.kde.org/okular/67614e453f653fed67fee01e4f96b7903cd4694a'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/26fc592e7db0518fa70fd39c3e620847b62c1c32'>Commit.</a> </li>
<li>Fix includes². <a href='http://commits.kde.org/okular/0045b384e300921053683d545d29dc7647b699ef'>Commit.</a> </li>
<li>Port to QSpinBox. <a href='http://commits.kde.org/okular/493467ec64299f5097a8d6da1f0b0e5a5cbf954e'>Commit.</a> </li>
<li>Port to QDialog. <a href='http://commits.kde.org/okular/5557598b470e9a68f2b33e7c33673fb45d399220'>Commit.</a> </li>
<li>Get rid of to/fromAscii methods. <a href='http://commits.kde.org/okular/a92c9c31a582e831b38edaf5964441d5871f8277'>Commit.</a> </li>
<li>Use QPushButton instead of KPushButton. <a href='http://commits.kde.org/okular/50f2ee6951901cc768543fc7c9e614f9138f9120'>Commit.</a> </li>
<li>Make the generators build. <a href='http://commits.kde.org/okular/7e3e82693e2849f0c6231b0cf2adf6dda2d96de2'>Commit.</a> </li>
<li>Still compile error to reactivate it. <a href='http://commits.kde.org/okular/596f84fe17b40df4a511434fe237514850686c94'>Commit.</a> </li>
<li>Reactivate code. <a href='http://commits.kde.org/okular/406ee919bfaa0b08aa976b032cd05fdd97c77996'>Commit.</a> </li>
<li>Use new connect api. Remove not necessary include moc. <a href='http://commits.kde.org/okular/a94b09684a1d7eb8734b39a846ba830602fe6571'>Commit.</a> </li>
<li>Remove not necessary include moc. <a href='http://commits.kde.org/okular/57adb81a031ecf8aeae3b9c03fd402ba7b4124ec'>Commit.</a> </li>
<li>Replace deprecated QAction::setShortcut with KStandardShortcut. <a href='http://commits.kde.org/okular/48300404cb4599f36e6830a697877be1d33bb887'>Commit.</a> </li>
<li>Reactivate djvu generator. <a href='http://commits.kde.org/okular/fc13e5c14817093b26e00d7e822c39837142cb4e'>Commit.</a> </li>
<li>Reactivate some generator. <a href='http://commits.kde.org/okular/89bb5562ac2f8488c32d9d86fb9746e3ef6f5d98'>Commit.</a> </li>
<li>Reverse dns desktop. <a href='http://commits.kde.org/okular/5b88e93891391ce620976659ddbf643f4dfa0dff'>Commit.</a> </li>
<li>Port to new connect api. <a href='http://commits.kde.org/okular/2eacdea022d04b7b1a52bc41d6ce2f715b820f13'>Commit.</a> </li>
<li>Port to new connect api. <a href='http://commits.kde.org/okular/d386ea257eb1a22b900b9e8ed4b5f14ac408b9b0'>Commit.</a> </li>
<li>Remove executable attribute. <a href='http://commits.kde.org/okular/aee3d941616043b0eba1f9d1115e01e864b0335b'>Commit.</a> </li>
<li>Remove some deprecated kdelibs4support. <a href='http://commits.kde.org/okular/378f6847a2cf6820c395b34b1850a28e5fd8d65d'>Commit.</a> </li>
<li>Increase ecm version. <a href='http://commits.kde.org/okular/9cc0f6bc39ff51a1f8fcf4b6c06e10a9d7fb15fb'>Commit.</a> </li>
<li>Port away from KGlobalSettings::desktopGeometry(). <a href='http://commits.kde.org/okular/dc74543bac21819f79ef01e00b39d49e3815e9d6'>Commit.</a> </li>
<li>Port from KInputDialog to QInputDialog. <a href='http://commits.kde.org/okular/a6db7a2655846740a22888a3d2cb68c8e3d697a2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/120378'>#120378</a></li>
<li>Make constants const, avoids a few extra symbols in the .data section. <a href='http://commits.kde.org/okular/146e585f0298a4c68596f7e332946459d0e388c7'>Commit.</a> </li>
<li>Port away from KGlobal::caption(). <a href='http://commits.kde.org/okular/f8f90b15b307d7c67438a8eba68173e960ade8f8'>Commit.</a> </li>
<li>Port the aboutData to the new methods. Next would be to bring back. <a href='http://commits.kde.org/okular/f4badbc117bf4e801c333e84b1390a9892b3cf51'>Commit.</a> </li>
<li>Fix building epub generator. <a href='http://commits.kde.org/okular/6bb0668d9d397961b993af4b1485fee5e1c413bb'>Commit.</a> </li>
<li>Port to QTemporaryFile. <a href='http://commits.kde.org/okular/3581fb624593bd7bbdd9a6b173050a7cf9413394'>Commit.</a> </li>
<li>Add .gitignore. <a href='http://commits.kde.org/okular/4e15c70d91ebdf216dd4c5b520fdba98a119d800'>Commit.</a> </li>
<li>Remove outdated debug area int. <a href='http://commits.kde.org/okular/9e804397ef6066db218b41aaed2e920cd07964a7'>Commit.</a> </li>
<li>Bring back the txt generator. <a href='http://commits.kde.org/okular/dad6936896b4edc549d8ad20a10d3e181ea30b83'>Commit.</a> </li>
<li>Install libokularGenerator_txt.desktop again. <a href='http://commits.kde.org/okular/61c5bba91945a90f33d9e937b9cbec7bc0f0b81a'>Commit.</a> </li>
<li>Use KSharedConfig::openConfig instead of KGlobal::config. <a href='http://commits.kde.org/okular/1e01a5cfc9dbbf881f236dae92cc1fe2307317a7'>Commit.</a> </li>
<li>Convert some remaining kWarning/kDebug. <a href='http://commits.kde.org/okular/a146b5b307e03356825421fd671d9fb704f7341f'>Commit.</a> </li>
<li>Remove -caption %c from .desktop files. <a href='http://commits.kde.org/okular/f7d1dd9312135e3f1820d98d38db5195b863fad9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/338264'>#338264</a></li>
<li>Add missing debug_comicbook.h. <a href='http://commits.kde.org/okular/be5af7304f2e9da67536bd4cd970f6e58c93bb19'>Commit.</a> </li>
<li>Use categorized logging even more. <a href='http://commits.kde.org/okular/6c602bf0e74a2f4fe0d80e51759e2c6023bf2cea'>Commit.</a> </li>
<li>Use categorized logging. <a href='http://commits.kde.org/okular/9d8973997253a338ca8878dbd27f53de5c4c196f'>Commit.</a> </li>
<li>Use qApp instead of kapp. <a href='http://commits.kde.org/okular/50e3c1478a6fe064dd3a965bdcb31b45b1bfa153'>Commit.</a> </li>
<li>Make it possible to open URLs on the command-line again. <a href='http://commits.kde.org/okular/d98b4d920037422fe052ffa2633349d41fdbe02e'>Commit.</a> </li>
<li>Port to QMime. <a href='http://commits.kde.org/okular/e28dbc6c00243442f6278546d19d1cf1b38b4ba9'>Commit.</a> </li>
<li>Fix command line parsing and DBUS registration in effect. <a href='http://commits.kde.org/okular/33f6862a94bf224ac465c4e27fb45fba4140b74a'>Commit.</a> </li>
<li>Revert "Some more kDebug->qDebug". <a href='http://commits.kde.org/okular/83d3f1f6b73a0ab44864d2f786c34f33b4a8d689'>Commit.</a> </li>
<li>Set translation domain. <a href='http://commits.kde.org/okular/c5819eec4dd888f06ba5cf8160ba58b4ffa325c1'>Commit.</a> </li>
<li>Some more kDebug->qDebug. <a href='http://commits.kde.org/okular/34fbdf8c85a75fb6666f07323625e38bbaabd4ba'>Commit.</a> </li>
<li>Use tabBar()->hide() instead of setTabBarHidden. <a href='http://commits.kde.org/okular/374592256a91f657008ff2eb5f7fb66cba12ce23'>Commit.</a> </li>
<li>Fix a typo in docs. <a href='http://commits.kde.org/okular/90e6f73e261f7b413e231248892b08080a97821b'>Commit.</a> </li>
<li>Compile DVI, TIFF and XPS generators. <a href='http://commits.kde.org/okular/75c8acee60f5ca3d780df86e994b1a84fbd78d36'>Commit.</a> </li>
<li>Assume Poppler 0.22 or newer (0.22 was the first one with qt5 support). <a href='http://commits.kde.org/okular/0b662faf35ffe021405a1f73098e3cd76cd10b75'>Commit.</a> </li>
<li>Port the kimgio generator. <a href='http://commits.kde.org/okular/e395735820dedcd8bb704a01981057a747656784'>Commit.</a> </li>
<li>Fix another rc install dir. <a href='http://commits.kde.org/okular/1fc4c747a69375d2335911b30361028e55c20d21'>Commit.</a> </li>
<li>Remove unused/commented out code from kimgio generator. <a href='http://commits.kde.org/okular/559ba0f01ed99d35c9cf01618e49cd6d458dbc5e'>Commit.</a> </li>
<li>Ported tests to QtTest, added missing trailing / to KDESRCDIR. <a href='http://commits.kde.org/okular/2a52e0c4e143249644697cf534806135cd34e409'>Commit.</a> </li>
<li>Re-enable busyPixmap, so that tests don't crash. <a href='http://commits.kde.org/okular/12810d361dfb8701463a851da266e376b4de6302'>Commit.</a> </li>
<li>Reenable doc generation. <a href='http://commits.kde.org/okular/4298b6ebd5a23712d4c282c7f45ae4df2487a1a7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/119804'>#119804</a></li>
<li>Do not add json for txt generator. <a href='http://commits.kde.org/okular/9350061dfce6e2ea8a8cff7f79410087143fa13f'>Commit.</a> </li>
<li>Port away from KUrl, pretify headers. <a href='http://commits.kde.org/okular/bd36521c3631f50002613ac73c59178f57896f39'>Commit.</a> </li>
<li>Port Shell away from KUrl to QUrl. <a href='http://commits.kde.org/okular/d5b9a2e07809f665b4c7b6615ba8c1e827a29025'>Commit.</a> </li>
<li>Fix build. <a href='http://commits.kde.org/okular/f8b9546afff0695d27cecb4b1e9fd74c3a862917'>Commit.</a> </li>
<li>Bring back txt generator. <a href='http://commits.kde.org/okular/24f7054c9e0ee0313687287538614dbc8e3960d6'>Commit.</a> </li>
<li>Port away from KUrl::List. <a href='http://commits.kde.org/okular/46cf7e46a5c406ebe6e20c77105d5daf0ca94f5d'>Commit.</a> </li>
<li>Port KAction and KMenu. <a href='http://commits.kde.org/okular/077689c4d3efec0c84c8bf85dcf4c8a4d9513d7d'>Commit.</a> </li>
<li>KIcon -> QIcon. <a href='http://commits.kde.org/okular/e038950f50d186dcdc94746da5a7545aa89c27c8'>Commit.</a> </li>
<li>Port the EPubGenerator. <a href='http://commits.kde.org/okular/fb330dd45c0af77510e38a0f8e385c1ab68a1978'>Commit.</a> </li>
<li>Port comicbook generator. <a href='http://commits.kde.org/okular/eee26dd72f40953d45f41e421e4b7bfba30925f6'>Commit.</a> </li>
<li>Kde4_add_ui_files -> ki18n_wrap_ui. <a href='http://commits.kde.org/okular/a2e353d3cb2c487dcf98476c0b3aed7a3b09f795'>Commit.</a> </li>
<li>Port the CHM generator. <a href='http://commits.kde.org/okular/2bfa3ccff000b945818182a4e962ba77323441bb'>Commit.</a> </li>
<li>Port the poppler generator. <a href='http://commits.kde.org/okular/e4a4d400c35b3e41a60d490a7f56cd43c8707372'>Commit.</a> </li>
<li>Port the tests. <a href='http://commits.kde.org/okular/7123b4d9d26feedb12caf326d50fd4b986a6ee97'>Commit.</a> </li>
<li>Okular Config: Use qWarning otherwise the compile will fail. <a href='http://commits.kde.org/okular/ead067d329feb6914cf23e4294b4a059d5eb383d'>Commit.</a> </li>
<li>Disable txt generator. <a href='http://commits.kde.org/okular/bf8f942bacffa48965bafaef2e84c2ae9456f30f'>Commit.</a> </li>
<li>Remove old KPart factory. <a href='http://commits.kde.org/okular/08840a9168b4e100bae16c8e397dda19d155fb9a'>Commit.</a> </li>
<li>Welcome back Okular::Part. <a href='http://commits.kde.org/okular/9398df0a06953ba889f59b42970da0e0315a865b'>Commit.</a> </li>
<li>More QUrl porting. <a href='http://commits.kde.org/okular/ac15a7075e9d10710f6edc1937c397e0a361c016'>Commit.</a> </li>
<li>Make cmake more kf5 compliant. <a href='http://commits.kde.org/okular/56eb2ae952ac4cbc128a90ac58689227f8d2c09f'>Commit.</a> </li>
<li>Fix rc install dir. <a href='http://commits.kde.org/okular/99db5d0157229e9400ce6f2de3df7d0a60cdb98f'>Commit.</a> </li>
<li>Fix some typos. <a href='http://commits.kde.org/okular/a2f6a136b25a07c15793a3dc9e92151db2245dc8'>Commit.</a> </li>
<li>Look for KF5 versions of libkscreen and kactivities. <a href='http://commits.kde.org/okular/b32306463cad7f73b584ff2042d11e0032ef4544'>Commit.</a> </li>
<li>Disable lots of stuff and compile. <a href='http://commits.kde.org/okular/0c7a45c7a53486028cf1d8515a3005293ae8e5ad'>Commit.</a> </li>
<li>Port UI: dialog buttons and friends. <a href='http://commits.kde.org/okular/fc73cbf8869e60a1089a01f4bd0717d523fc74f9'>Commit.</a> </li>
<li>A few action and include fixes. <a href='http://commits.kde.org/okular/b32d8153013ab7b744e00ef999957621bf85554a'>Commit.</a> </li>
<li>QAction, more includes fixed. <a href='http://commits.kde.org/okular/96b0e27c44ce8a4b5c6ca19aed518a27e4953350'>Commit.</a> </li>
<li>Qt5 porting. <a href='http://commits.kde.org/okular/4ce837a7ac50f97654990f13a028d18cecd44b33'>Commit.</a> </li>
<li>Fix settings namespace. <a href='http://commits.kde.org/okular/4851e8a792c413ddd526974afba5a527d76e8f4b'>Commit.</a> </li>
<li>Disable text to speech. <a href='http://commits.kde.org/okular/c4eedb68ead5b2bfdc72e79b956d4b72f4a48e5b'>Commit.</a> </li>
<li>Link Qt Svg. <a href='http://commits.kde.org/okular/19a73981eafe7d35b198ed2d92121205e12f4066'>Commit.</a> </li>
<li>Generator porting (mostly kaboutdata). <a href='http://commits.kde.org/okular/7261d5d45afc1b6e6b973c07a21d009f27fbfa5f'>Commit.</a> </li>
<li>Phonon fix, good bye klocalsocket. <a href='http://commits.kde.org/okular/51e5589e71801f99defc5143df6999adf05e017a'>Commit.</a> </li>
<li>Poppler-qt4 -> poppler-qt5. <a href='http://commits.kde.org/okular/26251d91a493a46eca9f11fdfca940d39575b8fd'>Commit.</a> </li>
<li>Port to new threadweaver. <a href='http://commits.kde.org/okular/efbde6600927ef41df5b7e139048202205d50404'>Commit.</a> </li>
<li>I need it down here for cmake to suceed. <a href='http://commits.kde.org/okular/20d687cac5d28b42afcb107bb5fe8e380b6a7dbf'>Commit.</a> </li>
<li>Clean up some have_poppler defines. <a href='http://commits.kde.org/okular/d1fa3b869f7c399c73dd46de638db0297000570a'>Commit.</a> </li>
<li>Poppler-qt4 -> 5. <a href='http://commits.kde.org/okular/bc155c6842249ee90ff470a6055e605bb34d72c6'>Commit.</a> </li>
<li>Disable plugins for now. <a href='http://commits.kde.org/okular/643026fa650e07c7123ded900692275f625fb714'>Commit.</a> </li>
<li>Some more linking KF5. <a href='http://commits.kde.org/okular/a3fb02b881d4980206a28d84db019ca0e6ee450a'>Commit.</a> </li>
<li>Fix linking. <a href='http://commits.kde.org/okular/5ec298c10c4a9179b281f8d7084c623fc4f6dc19'>Commit.</a> </li>
<li>Port KAboutData. <a href='http://commits.kde.org/okular/1703f2df7c49e24307a9af9a752d66ba4b835b26'>Commit.</a> </li>
<li>Bump version. <a href='http://commits.kde.org/okular/0de101fdde887daa99f72dbcbae97597ef0536ee'>Commit.</a> </li>
<li>Port to QCommandLineParser. <a href='http://commits.kde.org/okular/c7e003d7bce9980de51856527de227c439200379'>Commit.</a> </li>
<li>Fix wrong usage of pragma. <a href='http://commits.kde.org/okular/7a9036ec2bc83196297537182c24b6640b2ffdec'>Commit.</a> </li>
<li>Use QKeySequence instead of KStandardShortcut. <a href='http://commits.kde.org/okular/272e8eb556ca66ddba393f1cae99ec4ea29ec98a'>Commit.</a> </li>
<li>Enable building of all parts again. <a href='http://commits.kde.org/okular/cf6de9bbd8836ec14476206d90476de77bfb9e73'>Commit.</a> </li>
<li>KDialog::setCaption removed. <a href='http://commits.kde.org/okular/49c6f33356026a814bd7e9888dd7c0a0b36d63de'>Commit.</a> </li>
<li>Fix linking to KF5 libs. <a href='http://commits.kde.org/okular/cebdf978ceace041dc8b6725dccdf7d5b47c7720'>Commit.</a> </li>
<li>Fix bug: when opening look at available mime types. <a href='http://commits.kde.org/okular/e638e59c15ae6099634706e125611314d569b054'>Commit.</a> </li>
<li>Disable dynamic translation loading. <a href='http://commits.kde.org/okular/9eec8ece731507c70a9de566f8fa1d0645dc3e49'>Commit.</a> </li>
<li>Fix wrong namespace for settings. <a href='http://commits.kde.org/okular/935a3fdcf811d0c4b8d90b80086f2f3aa9e208f6'>Commit.</a> </li>
<li>Disable PostScript printing. <a href='http://commits.kde.org/okular/4efd43253764caa133a0b24f90167cb6ec0b2e56'>Commit.</a> </li>
<li>Fix includes and KQAction. <a href='http://commits.kde.org/okular/4cf3df32e349c361dbe8304ae3cdd683bbc2da48'>Commit.</a> </li>
<li>Use kconfig_add_kcfg_files for kcfgc. <a href='http://commits.kde.org/okular/39787e4324e3a7560e44a39408183dab97ff90b9'>Commit.</a> </li>
<li>Use moc include style dictated by automoc. <a href='http://commits.kde.org/okular/0ba233788c56abd91e24f80f92a00081c7470875'>Commit.</a> </li>
<li>Include more kde4support. <a href='http://commits.kde.org/okular/4f196da176029cbc132d6b7641b62bb46b51585e'>Commit.</a> </li>
<li>Disable qimageblitz usage. <a href='http://commits.kde.org/okular/4f74ed01cfeef288b0d481a4cae8ab81d953386a'>Commit.</a> </li>
<li>Fix some QUrl usage. <a href='http://commits.kde.org/okular/d16ac66ab37ba5d5802ce8e754580eb4541539fd'>Commit.</a> </li>
<li>Fix includes. <a href='http://commits.kde.org/okular/75031eee7e4196a04bd41ab294c1f0b14f5ecd6f'>Commit.</a> </li>
<li>Disable subdirs while porting core. <a href='http://commits.kde.org/okular/5ab5212e21c7e5401c503fdcc101f5a34358daa8'>Commit.</a> </li>
<li>Add Qt5 printsupport module. <a href='http://commits.kde.org/okular/0ec27dda741a3d7dd56bb2e9f2d5275963cab6ca'>Commit.</a> </li>
<li>Fix moc includes. <a href='http://commits.kde.org/okular/fdf223494daf658961085425c7ade0fecc65d299'>Commit.</a> </li>
<li>Basic cmake kf5 porting. <a href='http://commits.kde.org/okular/93918b1ec8f585dd135db1449e1b22b878f82fbc'>Commit.</a> </li>
</ul>
<h3><a name='palapeli' href='https://cgit.kde.org/palapeli.git'>palapeli</a> <a href='#palapeli' onclick='toggle("ulpalapeli", this)'>[Show]</a></h3>
<ul id='ulpalapeli' style='display: none'>
<li>Add appdata file. <a href='http://commits.kde.org/palapeli/6de085a058cdd5365ddc81b77567cc958e5aeb49'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128403'>#128403</a></li>
<li>Reverse dns desktop. <a href='http://commits.kde.org/palapeli/5c8a330ce16624dee73fe8c47364da63e7316d20'>Commit.</a> </li>
</ul>
<h3><a name='parley' href='https://cgit.kde.org/parley.git'>parley</a> <a href='#parley' onclick='toggle("ulparley", this)'>[Show]</a></h3>
<ul id='ulparley' style='display: none'>
<li>Remember practice direction for each practice mode. <a href='http://commits.kde.org/parley/2505ea493940784f3c0d5f76bc50c7ceb29739c9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128078'>#128078</a></li>
<li>Fix for configuration file saving. <a href='http://commits.kde.org/parley/63f15ce822537b82c25fc5462ccb01fe1c06acfc'>Commit.</a> </li>
<li>Fix wrong displayed grade/pregrade in practise mode. <a href='http://commits.kde.org/parley/b2ddc4a2bf4147ad5c435d4199e1cfc8f4176aba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368749'>#368749</a>. Code review <a href='https://git.reviewboard.kde.org/r/128901'>#128901</a></li>
<li>Readd missing png. <a href='http://commits.kde.org/parley/03f061d90a3db9534ac2e3b5834a336ca304883f'>Commit.</a> </li>
<li>More screenshots cleanup. <a href='http://commits.kde.org/parley/b4ae8f11aa1314cf880bedccd18225b99edf3d71'>Commit.</a> </li>
<li>Delete unused screenshots. <a href='http://commits.kde.org/parley/6487ca8bdcf22a293998bee02ff300cf0240dd97'>Commit.</a> </li>
<li>More screenshot updates. <a href='http://commits.kde.org/parley/9babe8156b0876c48100bebe86978043737520f3'>Commit.</a> </li>
<li>Add + update screenshots. <a href='http://commits.kde.org/parley/6285fb5f2951e2f9de641f7df53d900f81749e95'>Commit.</a> </li>
<li>Update Parley handbook to 16.04. <a href='http://commits.kde.org/parley/ba8c77eccf39c8bd2e427045eb5a927b91cb42cb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127901'>#127901</a></li>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/parley/ca9d9a5c7ff28ef763456f4434b1a36978fee6c7'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/parley/3d5d56477daf4ee9c4d839bf81b6225c8d578f4e'>Commit.</a> </li>
</ul>
<h3><a name='pim-data-exporter' href='https://cgit.kde.org/pim-data-exporter.git'>pim-data-exporter</a> <a href='#pim-data-exporter' onclick='toggle("ulpim-data-exporter", this)'>[Show]</a></h3>
<ul id='ulpim-data-exporter' style='display: none'><li>New in this release</li></ul>
<h3><a name='pim-sieve-editor' href='https://cgit.kde.org/pim-sieve-editor.git'>pim-sieve-editor</a> <a href='#pim-sieve-editor' onclick='toggle("ulpim-sieve-editor", this)'>[Show]</a></h3>
<ul id='ulpim-sieve-editor' style='display: none'><li>New in this release</li></ul>
<h3><a name='pim-storage-service-manager' href='https://cgit.kde.org/pim-storage-service-manager.git'>pim-storage-service-manager</a> <a href='#pim-storage-service-manager' onclick='toggle("ulpim-storage-service-manager", this)'>[Show]</a></h3>
<ul id='ulpim-storage-service-manager' style='display: none'><li>New in this release</li></ul>
<h3><a name='pimcommon' href='https://cgit.kde.org/pimcommon.git'>pimcommon</a> <a href='#pimcommon' onclick='toggle("ulpimcommon", this)'>[Show]</a></h3>
<ul id='ulpimcommon' style='display: none'>
<li>Make more use of KStandardGuiItem. <a href='http://commits.kde.org/pimcommon/e024cf2313498baddacbf3c9f004c294eb82ccb2'>Commit.</a> </li>
<li>Use QObject as argument. <a href='http://commits.kde.org/pimcommon/d4be704567161ca0627f696516d4cadb8009cb80'>Commit.</a> </li>
<li>USe new connect api. <a href='http://commits.kde.org/pimcommon/e516a21f11e1aab7e13f1ce8c54596a27c0ac2b9'>Commit.</a> </li>
<li>Use an unique network config manager. <a href='http://commits.kde.org/pimcommon/08b8197f1fd4c5a8f3b0ec5f292d06dc87c7255c'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/pimcommon/40990fe039bbfb31733d16e844a7f09657a9c5f4'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/pimcommon/088aee386e31a154a2d894c797afe21b35d4d6d9'>Commit.</a> </li>
<li>Add objectname for autotest. <a href='http://commits.kde.org/pimcommon/9e35d7c6ab7a2ff79230673218277237332e7fa0'>Commit.</a> </li>
<li>Allow to access to QDialogButtonBox. <a href='http://commits.kde.org/pimcommon/e92a74f9e15e0e806798ed34153e85956d6085f5'>Commit.</a> </li>
<li>Store user settings value. <a href='http://commits.kde.org/pimcommon/478b1d244dee8820acd625bf38e6c5da48970aa2'>Commit.</a> </li>
<li>Allow to make restore defaults. <a href='http://commits.kde.org/pimcommon/c9b10647e047ac1e32088000aca158ad15378af5'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/pimcommon/80efc15cf9de2e33b462187a7f05c05b8c30933e'>Commit.</a> </li>
<li>Allow to show documentation. <a href='http://commits.kde.org/pimcommon/5786aab193aea07c89c726d958e42ea4d3cba0c8'>Commit.</a> </li>
<li>Use icon here. <a href='http://commits.kde.org/pimcommon/a2edbf77cab30778fe32f2a94daa761880ea7395'>Commit.</a> </li>
<li>Remove unused method. <a href='http://commits.kde.org/pimcommon/864271ed0f4d6c632e1f989c3b32ca23589c81e3'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/pimcommon/8be145b1cc5b30c447d110f854861af5aad90138'>Commit.</a> </li>
<li>Add extra info. <a href='http://commits.kde.org/pimcommon/7392868b147b67755b2d9f02076b5aa063555d6d'>Commit.</a> </li>
<li>Move to constructor. <a href='http://commits.kde.org/pimcommon/b8f3ff3f7c5f64de31ce5f3a5ba2cfc1794cae89'>Commit.</a> </li>
<li>Reduce size. <a href='http://commits.kde.org/pimcommon/ea6af0daffd5ceb9ad9034970d84fb8cb0182b31'>Commit.</a> </li>
<li>Change API as we want to open a specific help. <a href='http://commits.kde.org/pimcommon/7f589923027892643bcb8aa9d95f2754ecad1eed'>Commit.</a> </li>
<li>Reduce button size. <a href='http://commits.kde.org/pimcommon/2356711d5d3e065db2a9fe4f30e5580cc3096651'>Commit.</a> </li>
<li>Fix setSectionResizeMode. <a href='http://commits.kde.org/pimcommon/93a8c7c103bbd1666a41b9c1a062daa7f56c31ac'>Commit.</a> </li>
<li>Activate configure plugin button. <a href='http://commits.kde.org/pimcommon/75fb78308c03799cef2d4304e3b0dafbf05aba1f'>Commit.</a> </li>
<li>Allow to show dialogbox. <a href='http://commits.kde.org/pimcommon/a2780a0b72d048259905c66e3b0bf88848da1814'>Commit.</a> </li>
<li>Add template configure dialog. <a href='http://commits.kde.org/pimcommon/7e8a66c90776a1b3390ce6435d3cc28b33aec6ea'>Commit.</a> </li>
<li>Get plugin from identifier. <a href='http://commits.kde.org/pimcommon/9c803f529b838837ea4988dc282f56ff6bf25b9d'>Commit.</a> </li>
<li>Remove unused variable. <a href='http://commits.kde.org/pimcommon/14b29dad2b35feac2d050985fac173360adc4be6'>Commit.</a> </li>
<li>Return plugin from identifier. <a href='http://commits.kde.org/pimcommon/e5473d835ed0c5a7f9e2563accde07eb43e573d2'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/pimcommon/4f5a51ef027e4c8e0ec4561a2a62a5a3c3f091ba'>Commit.</a> </li>
<li>React when we click on button. <a href='http://commits.kde.org/pimcommon/f30ca1b6ffc206ac3ee34f798dac4e393637924b'>Commit.</a> </li>
<li>Allow to add configureGroupName. <a href='http://commits.kde.org/pimcommon/08745698ed91037b72628b547f9268926115c97b'>Commit.</a> </li>
<li>Add support for configure plugin. <a href='http://commits.kde.org/pimcommon/ffc420d3988cd99178145ec2ead5b8df4e4ae635'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/pimcommon/e700338f055a518fbf4411e7809d92d4c121f81f'>Commit.</a> </li>
<li>Minor optimization. Allow to store info about support of configuration. <a href='http://commits.kde.org/pimcommon/8320de5fb56348d1dd54d1e5af246a5a688423e0'>Commit.</a> </li>
<li>USe abstract class. <a href='http://commits.kde.org/pimcommon/fa5ead4e626590cfa06d22720b6d1757792d4095'>Commit.</a> </li>
<li>Add abstract class for plugin and plugin interface. <a href='http://commits.kde.org/pimcommon/9fdb405f608216bb5e9397a3017e055771cb1378'>Commit.</a> </li>
<li>Move method to plugin directly. <a href='http://commits.kde.org/pimcommon/f82a210808b4aaed9adcd545c03df7f944f4d6d6'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/pimcommon/0d4dd578ed9f318ce5b8d957f182746706ca606e'>Commit.</a> </li>
<li>Look at to implement configure plugin support. <a href='http://commits.kde.org/pimcommon/8d71eaf903efa96223329592c2545f984efa291f'>Commit.</a> </li>
<li>Reduce duplicate code. <a href='http://commits.kde.org/pimcommon/0d6da5eb62cb11490a5a5624e5cb04a5cab17b86'>Commit.</a> </li>
<li>Rename categories. <a href='http://commits.kde.org/pimcommon/8db8d345867e50a583412ad07d885f21c973d36c'>Commit.</a> </li>
<li>Reduce duplicate code. <a href='http://commits.kde.org/pimcommon/815bd616941bf077edd13a780e51f07c047e668d'>Commit.</a> </li>
<li>Add inline method for returning config file name. <a href='http://commits.kde.org/pimcommon/87af94856adca9bca706f0376908104dae03af98'>Commit.</a> </li>
<li>Cleanup. <a href='http://commits.kde.org/pimcommon/bf5575ee902d4a07f2b8b4fb438ffd601ef291da'>Commit.</a> </li>
<li>Avoid to duplicate code. <a href='http://commits.kde.org/pimcommon/3ed73e2c234834d0ab11749d0d192c9e9fd5ad17'>Commit.</a> </li>
<li>Minor. <a href='http://commits.kde.org/pimcommon/89606b5444305e1e64d65d390a90dd2836c5580d'>Commit.</a> </li>
<li>Improve autotest. <a href='http://commits.kde.org/pimcommon/1178d0142e79c1adebb6ee29cb147c7f1dd83153'>Commit.</a> </li>
<li>Fix sorting tree items. <a href='http://commits.kde.org/pimcommon/1c2af2d7cf2557671818896f4a9c32ba540e8437'>Commit.</a> </li>
<li>Add api to get plugin info. <a href='http://commits.kde.org/pimcommon/c7113b28c370a0a235a9fa414916b1f9d81e5a40'>Commit.</a> </li>
<li>Add method to define actioncollection. <a href='http://commits.kde.org/pimcommon/fea2b196d2cdbd4737cdce36640426526f6dfc2b'>Commit.</a> </li>
<li>Add missing %1. <a href='http://commits.kde.org/pimcommon/2cfe4c263f6bc1e260325da975db597a88f6b7fe'>Commit.</a> </li>
<li>Add autotests. <a href='http://commits.kde.org/pimcommon/f26ec1a4ba96b08eae9b925f661be49fe0c282e8'>Commit.</a> </li>
<li>Install headers. <a href='http://commits.kde.org/pimcommon/b880936539f80a24fb6687b854b3e3109cad67fa'>Commit.</a> </li>
<li>Add configure plugin widget. <a href='http://commits.kde.org/pimcommon/aca21f7075b999fabc4c32664ffb1d3a33efa0c2'>Commit.</a> </li>
<li>Astyle kdelibs. <a href='http://commits.kde.org/pimcommon/fcbeb8fa67c4d4b4a3fe9384c50820a753a315c8'>Commit.</a> </li>
<li>Por to new api. <a href='http://commits.kde.org/pimcommon/397575e916112bc512cd0c8fb0391fe98eddf926'>Commit.</a> </li>
<li>Create a PluginUtilData => avoid to duplicate code. <a href='http://commits.kde.org/pimcommon/5c39a26fb0449d243d9707682b9778321ab91ec0'>Commit.</a> </li>
<li>Add missing %. <a href='http://commits.kde.org/pimcommon/4914065b84de1cccdc44736a01e46327b01d74ee'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/pimcommon/b6e811546135ea023d5d0de3564de4675ed82e4e'>Commit.</a> </li>
<li>Add save method. <a href='http://commits.kde.org/pimcommon/766ae9c5c15e2f695d9fee91ba67a48f2757b18e'>Commit.</a> </li>
<li>Add common code for load settings. <a href='http://commits.kde.org/pimcommon/6210783be8d65b50c032579cd61e03ca60f0feaa'>Commit.</a> </li>
<li>Use PimCommon::PluginUtil::isPluginActivated. <a href='http://commits.kde.org/pimcommon/2a1649f09aa5b8df4f5efab574d4ca4de19113fa'>Commit.</a> </li>
<li>Move isPluginActivated here. <a href='http://commits.kde.org/pimcommon/ea44df6f2c7833a4d5d2cff76a92925f08273d1d'>Commit.</a> </li>
<li>Use pluginId. <a href='http://commits.kde.org/pimcommon/0e857aeb41eaf6f3a489c69dd9acae8a1c09effc'>Commit.</a> </li>
<li>Get plugin info. <a href='http://commits.kde.org/pimcommon/a98cd49e617d7959176127b5119b6b792368f876'>Commit.</a> </li>
<li>Allow to enable/disable plugin. <a href='http://commits.kde.org/pimcommon/aa3e0280f9a6ed708415cd6e65d9c67b86b8b959'>Commit.</a> </li>
<li>Remove debug. <a href='http://commits.kde.org/pimcommon/7d2a64e762d5c3ffe6db0127666a8d16d675d608'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/pimcommon/8a6a11981071ec827186739fca156ada2534663c'>Commit.</a> </li>
<li>Load when isEnabledByDefault. <a href='http://commits.kde.org/pimcommon/6eaa9b89e2cb42b2689110d7c160a084da416936'>Commit.</a> </li>
<li>Add parent directly. <a href='http://commits.kde.org/pimcommon/282cec8151f9d6bda3664f4cad11e926e1f72061'>Commit.</a> </li>
<li>Increase dependancy. <a href='http://commits.kde.org/pimcommon/caadee6a926a688314c4d16bde9e6693d3a23000'>Commit.</a> </li>
<li>Use qCWarning. <a href='http://commits.kde.org/pimcommon/4b635c777526dc357fef0331edbe786812faeecb'>Commit.</a> </li>
<li>Add more infos. <a href='http://commits.kde.org/pimcommon/419c54767ac46e0eb963573a73727d30ebaa8e60'>Commit.</a> </li>
<li>Use non-deprecated QHeaderView::setSectionsMovable. <a href='http://commits.kde.org/pimcommon/ed41367eea671ae078aa16d7c428d066ea0cc60f'>Commit.</a> </li>
<li>Use QUrlQuery to fix compile when deprecated features are disabled. <a href='http://commits.kde.org/pimcommon/d64b70c3e8892b1fe8fd2a81147d29d98985174b'>Commit.</a> </li>
<li>Replace deprecated QAIM::reset with begin/endResetModel. <a href='http://commits.kde.org/pimcommon/1a594a71a930aa017280c3adf8681ed203d04c68'>Commit.</a> </li>
<li>PrintSupport is required. <a href='http://commits.kde.org/pimcommon/7a15ab89a42bc66dac99adf9deb9cdceac4e2761'>Commit.</a> </li>
<li>Add metainfo file. <a href='http://commits.kde.org/pimcommon/926fb990864b786b50ce88a5551e62986dc82ea6'>Commit.</a> </li>
</ul>
<h3><a name='rocs' href='https://cgit.kde.org/rocs.git'>rocs</a> <a href='#rocs' onclick='toggle("ulrocs", this)'>[Show]</a></h3>
<ul id='ulrocs' style='display: none'>
<li>Use KStandardGuiItem::overwrite(). <a href='http://commits.kde.org/rocs/c48f33e5350c7ac8899ec76503c71d15a8bc5302'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Show]</a></h3>
<ul id='ulspectacle' style='display: none'>
<li>Fix how the minimum size for the main widget is set. <a href='http://commits.kde.org/spectacle/ddb88b39bbf8de37df1b2cdab35718de2882b013'>Commit.</a> </li>
<li>Remove line about me being maintainer from README. <a href='http://commits.kde.org/spectacle/609c0ec03d222810ab5d31e59a570fb71e45193c'>Commit.</a> </li>
<li>Show message if kipi-plugins are not installed. <a href='http://commits.kde.org/spectacle/47e1301926266c05976109528671192468e35b9b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129168'>#129168</a>. Fixes bug <a href='https://bugs.kde.org/358557'>#358557</a></li>
<li>Lazy load export image, halving the startup time. <a href='http://commits.kde.org/spectacle/a6911eb80af1f67c9130d7749b3161cc40467041'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129167'>#129167</a></li>
<li>Fix initial region location for High DPI screens. <a href='http://commits.kde.org/spectacle/1244980882382639f35925cef1d4cf3eeb3c240c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129190'>#129190</a>. See bug <a href='https://bugs.kde.org/357022'>#357022</a></li>
<li>Fix blurry preview on HiDPI displays. <a href='http://commits.kde.org/spectacle/a41e83edbaadd27cd8ea85d4dfa375033e8ade84'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129189'>#129189</a></li>
<li>Refactor pixmap grabbing, add robustness. <a href='http://commits.kde.org/spectacle/7a5603fda8f1e591d31dba1ad178c44ad6847cc2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129176'>#129176</a>. Fixes bug <a href='https://bugs.kde.org/370303'>#370303</a></li>
<li>Capture all windows for "Window under cursor". <a href='http://commits.kde.org/spectacle/aab52c83ba70e9867632e83bd81715fc4405ffcf'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129163'>#129163</a>. Fixes bug <a href='https://bugs.kde.org/357223'>#357223</a>. Fixes bug <a href='https://bugs.kde.org/369330'>#369330</a></li>
<li>Replace KScreen by QScreen for current window grab. <a href='http://commits.kde.org/spectacle/e4c2e564a5b91497132d9a20d8f521af405286bd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129127'>#129127</a></li>
<li>Use native file dialog for the Save As option. <a href='http://commits.kde.org/spectacle/c9de3b0d09a41643b56e962561f8187ac84e955e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129156'>#129156</a>. Fixes bug <a href='https://bugs.kde.org/369174'>#369174</a></li>
<li>Make the preview respect devicePixelScale. <a href='http://commits.kde.org/spectacle/9bca22ef6689728facf3a0ea3eded845b91b2e8e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129136'>#129136</a></li>
<li>Fix memleak of xcb_image_t. <a href='http://commits.kde.org/spectacle/af7469148e15ae11c48e6e6fba7019ea8397a58b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129137'>#129137</a></li>
<li>Fix broken previews for small selection. <a href='http://commits.kde.org/spectacle/45751d7d8bc2a24abf75644d010018875c17daef'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129135'>#129135</a>. Fixes bug <a href='https://bugs.kde.org/362450'>#362450</a></li>
<li>Hide bottom help text when selection is overlapping. <a href='http://commits.kde.org/spectacle/45d3a0700aaa7564c5090398173a98828d38d2d4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129113'>#129113</a></li>
<li>Add selection size information. <a href='http://commits.kde.org/spectacle/362cf1c7b5eca33c46de7c838776124f230e2eca'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129109'>#129109</a>. Fixes bug <a href='https://bugs.kde.org/357080'>#357080</a></li>
<li>Update Spectacle docbook. <a href='http://commits.kde.org/spectacle/118bcd8a9a4c6c89445a589fa990d15ec9223099'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128936'>#128936</a></li>
<li>Fix minor typo. <a href='http://commits.kde.org/spectacle/d497183a4d07d1187e82771ab3541291ef0b3331'>Commit.</a> </li>
<li>Update codename. <a href='http://commits.kde.org/spectacle/c5a85e3b5b55be6346fb6190cdeca74edc305ac8'>Commit.</a> </li>
</ul>
<h3><a name='step' href='https://cgit.kde.org/step.git'>step</a> <a href='#step' onclick='toggle("ulstep", this)'>[Show]</a></h3>
<ul id='ulstep' style='display: none'>
<li>Update appstream path to use ECM variable. <a href='http://commits.kde.org/step/23753f6f9d3e96eade5424d10352596306ff286f'>Commit.</a> </li>
<li>Use path from KDE_INSTALL_METAINFO variable for appstream path to pick up new directory name. <a href='http://commits.kde.org/step/6e005459a504d6755ff4f9efb1c222063ef07d5e'>Commit.</a> </li>
</ul>
<h3><a name='svgpart' href='https://cgit.kde.org/svgpart.git'>svgpart</a> <a href='#svgpart' onclick='toggle("ulsvgpart", this)'>[Show]</a></h3>
<ul id='ulsvgpart' style='display: none'>
<li>Use our KAboutData. <a href='http://commits.kde.org/svgpart/7683b0e6704fd72299d8fc64c35e6a56202ca641'>Commit.</a> </li>
<li>Cmake: Only specify cmake_minimum_required one time at the top. <a href='http://commits.kde.org/svgpart/e0f06693c88849e1b8bc7c4fe120aa3c5b6f5055'>Commit.</a> </li>
<li>XmlGui is not explicitly needed. <a href='http://commits.kde.org/svgpart/47cae1176342ed2be4aae4b6c72466677aaf8f9a'>Commit.</a> </li>
<li>Initial port to Frameworks. <a href='http://commits.kde.org/svgpart/9cab49e26dbd64ce60d32086f23561aee3b11953'>Commit.</a> </li>
</ul>
<h3><a name='syndication' href='https://cgit.kde.org/syndication.git'>syndication</a> <a href='#syndication' onclick='toggle("ulsyndication", this)'>[Show]</a></h3>
<ul id='ulsyndication' style='display: none'>
<li>Fix version. <a href='http://commits.kde.org/syndication/01b7f5f61b9d3fca5a495dbf2ccc499ffaa95496'>Commit.</a> </li>
<li>Update/add .arcconfig. <a href='http://commits.kde.org/syndication/5df112a3fbcd0d5c02737e2696a46098bcc7bf0e'>Commit.</a> </li>
<li>Use kf5_version for ecm too. <a href='http://commits.kde.org/syndication/fe922337fca957b520eec271debe8c0a3593b44e'>Commit.</a> </li>
<li>Indeed we need to change it too. <a href='http://commits.kde.org/syndication/c4aed9f29f907cd29da86ae0546d993368361a1d'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Prepare fetching of debug packages which may be added to the installers in future. <a href='http://commits.kde.org/umbrello/e3e22e95a3aec9612c01738fc862137dafa7ca6e'>Commit.</a> </li>
<li>Make fetching packages from obs more robust. <a href='http://commits.kde.org/umbrello/da1700a5403fc1ce4b32db679a70c9ccf559c431'>Commit.</a> </li>
<li>Fix crash on importing c++ code. <a href='http://commits.kde.org/umbrello/80b341be5801373fb8237cdddbc56701dc397eb4'>Commit.</a> </li>
<li>Fix 'Dock window selection is changed after code import'. <a href='http://commits.kde.org/umbrello/3383c4505efbac99b74815d804e8770fbd5a1b34'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373347'>#373347</a></li>
<li>Make sure we have newest kde4-l10n packages because they are fetched from related OBS base distribution. <a href='http://commits.kde.org/umbrello/f0d1356e277e0bbc62a2fe48f0e1405e630b7a84'>Commit.</a> </li>
<li>Revert "Move welcome page to separate sub directory 'apphelp'.". <a href='http://commits.kde.org/umbrello/9a852e770b29e182712adfd91a3d6c97dda5d10f'>Commit.</a> </li>
<li>Restrict write access to UMLScene::widgetList to track NULL-pointer list entries. <a href='http://commits.kde.org/umbrello/c7250ebcb0886f47f4e694a6b4d2ea369adc0a89'>Commit.</a> See bug <a href='https://bugs.kde.org/371990'>#371990</a></li>
<li>Move welcome page to separate sub directory 'apphelp'. <a href='http://commits.kde.org/umbrello/052e0223d7501e25fe09f3721ed28583e23b4087'>Commit.</a> </li>
<li>Improve windows release script by using osc. <a href='http://commits.kde.org/umbrello/18784e96bebffce24dfef3911f956be9cfe99754'>Commit.</a> </li>
<li>Propagate variables for unstable features also to cmake build files. <a href='http://commits.kde.org/umbrello/27a444cff39b52ac658ae1166a7cba27d9eb9c4b'>Commit.</a> </li>
<li>Fix crash in case welcome file is not available. <a href='http://commits.kde.org/umbrello/44f3d39667460a750fc36cc4ce408f2f71111a13'>Commit.</a> </li>
<li>Welcome page requires additional stuff installed. Until that is fixed only build on request. <a href='http://commits.kde.org/umbrello/7e1d8adc4bf225691d0082b2286ad7a74a23c8e1'>Commit.</a> </li>
<li>Enable object window for beta and rc releases. <a href='http://commits.kde.org/umbrello/5d4c562b4d59c87802c835795e874b176ba43942'>Commit.</a> </li>
<li>Fix cross build support for KDE4 variant of welcome page. <a href='http://commits.kde.org/umbrello/08e4485dbbad8d0d9d56847724ebbe9616361fbb'>Commit.</a> </li>
<li>Fix 'After duplicating class or interface return type of operations are lost'. <a href='http://commits.kde.org/umbrello/6e0b03fd72de37893568bf6aaf2d650ab51a8849'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372587'>#372587</a></li>
<li>Fix 'Delete attributes or operations in classes'. <a href='http://commits.kde.org/umbrello/a86130613067940d27c98f6f845037817c705297'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372579'>#372579</a></li>
<li>Fix 'Umbrello crashes after undoing class attribute deletion'. <a href='http://commits.kde.org/umbrello/b391fe5d6c004080e4e68c5bafadf74b778f559b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372601'>#372601</a></li>
<li>Adjust umbrello version. <a href='http://commits.kde.org/umbrello/e56248b52cb70f055b194b6c41bac13ae67172f1'>Commit.</a> </li>
<li>Add script for releasing windows packages. <a href='http://commits.kde.org/umbrello/b68762e44ca61933221483e5328e022e3f9e2b95'>Commit.</a> </li>
<li>Cleanup order of tabbed dock widgets. <a href='http://commits.kde.org/umbrello/95db74ff692e1736efaea1c8e9a237b8085c42a4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372253'>#372253</a></li>
<li>Add welcome page as dock widget. <a href='http://commits.kde.org/umbrello/52a9df263c45d5539bba881b259524c42702356f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372252'>#372252</a></li>
<li>Check against zero pointer. <a href='http://commits.kde.org/umbrello/dc6ff10cba26cafbed9fedf2134a81cf9a5ccaf0'>Commit.</a> </li>
<li>Remove obsolate definitions. <a href='http://commits.kde.org/umbrello/6963032b6081895d79f22d7675f56c60ed7b9199'>Commit.</a> </li>
<li>Fix 'Crash when removing pin widgets after owning component has been removed before'. <a href='http://commits.kde.org/umbrello/536e52fe6f71145cdcee16b5833f1c73b64e61fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371990'>#371990</a></li>
<li>Add type wrapper for uml widget classes. <a href='http://commits.kde.org/umbrello/d4dfde3d75047a8e3db1878200cf1ae8e35888b6'>Commit.</a> </li>
<li>Use 0 instead of NULL to follow coding style. <a href='http://commits.kde.org/umbrello/657d51ce0224c5fee9aa8ef840da42962445c5a9'>Commit.</a> </li>
<li>Add type wrapper for uml widget classes. <a href='http://commits.kde.org/umbrello/c18413815334ccaebf28ae2e90b215377b34b84e'>Commit.</a> </li>
<li>Fix 'Font sizing is wrong when sharing diagrams'. <a href='http://commits.kde.org/umbrello/3409db0b671a61440cb7e89dd024adf961b21cee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/90103'>#90103</a></li>
<li>Write out typedefs located in classes for default code generator. <a href='http://commits.kde.org/umbrello/d4f45447822d251e6ff780da47f158c05e74474a'>Commit.</a> See bug <a href='https://bugs.kde.org/371274'>#371274</a></li>
<li>Introduce class UMLDatatype. <a href='http://commits.kde.org/umbrello/828f7dac7316cf8b0b13e290b51fcd114b3e06d1'>Commit.</a> See bug <a href='https://bugs.kde.org/371274'>#371274</a></li>
<li>Reorder include files alphanumeric. <a href='http://commits.kde.org/umbrello/652c076039a9b1db2151c820918b955347da9a0a'>Commit.</a> </li>
<li>Fix coverity check CID 155784: Uninitialized pointer field (UNINIT_CTOR). <a href='http://commits.kde.org/umbrello/0b9b23481cc5d73e5dae86f257f66e2b56d22708'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>Fix warning about unused variable. <a href='http://commits.kde.org/umbrello/bbe6b9522a94a58795f3c4652abb3157eb642c0c'>Commit.</a> </li>
<li>Fix coverity check CID 170379: Unchecked return value (CHECKED_RETURN). <a href='http://commits.kde.org/umbrello/c652fa72b97751d4783a35b90f54fd5baceb4042'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>Fix 'error: cast from 'UMLObject*' to 'long unsigned int' loses precision' on cross compiling for windows x86_64. <a href='http://commits.kde.org/umbrello/e67d069d857d8a42f3bc0937cdfd6de994ed396e'>Commit.</a> </li>
<li>Use the standard naming scheme for the appdata files for KF5. <a href='http://commits.kde.org/umbrello/5fde4756b926f19c13691905b0664121253dfcae'>Commit.</a> </li>
<li>Convert untranslated string into translatable string according to KDE rules. <a href='http://commits.kde.org/umbrello/2baf5b0b05c3837ecd8d2e738b12223ee82d9af3'>Commit.</a> See bug <a href='https://bugs.kde.org/369109'>#369109</a></li>
<li>Fix 'Untranslatable percent values in GUI'. <a href='http://commits.kde.org/umbrello/da0026a35568924aaa31902ca9920c1103200d9b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369109'>#369109</a></li>
<li>Compile fix. <a href='http://commits.kde.org/umbrello/f9f35fd62c83121b11978173cb53deb7d2d58338'>Commit.</a> See bug <a href='https://bugs.kde.org/100338'>#100338</a></li>
<li>Add support for array types to c++ generator. <a href='http://commits.kde.org/umbrello/ed217c453aba8d14cd666ff9eba8a4dca45477ce'>Commit.</a> See bug <a href='https://bugs.kde.org/100338'>#100338</a></li>
<li>Import c++ class member array types. <a href='http://commits.kde.org/umbrello/c99a72821608069baf5625c2f6fd0024ea1fe279'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/100338'>#100338</a></li>
<li>Keep filenames in 'cmds' subfolder in sync with class names. <a href='http://commits.kde.org/umbrello/2fcaa1db7b286b91bf6160c4479b65de05592776'>Commit.</a> </li>
<li>Crash fix. <a href='http://commits.kde.org/umbrello/6f8e0595229a67629cd861ee2007a655407427d1'>Commit.</a> </li>
<li>Include umbrelloui.rc into executable to avoid finding issue on KF5. <a href='http://commits.kde.org/umbrello/3bef8511ca5859e1345c798f8b38a9ae46dfdd72'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369079'>#369079</a></li>
<li>Avoid cmake warning in case not finding optional package CLANG. <a href='http://commits.kde.org/umbrello/f6a3dc221c16e225ba93c312a9af969cf5676c7b'>Commit.</a> </li>
<li>Let unit test class and file names be in sync with other umbrello source code. <a href='http://commits.kde.org/umbrello/348dbe73b5350b56c65bb4e0144a37025dc8da2d'>Commit.</a> </li>
<li>Build command line tools as console application on Windows and Mac OSX. <a href='http://commits.kde.org/umbrello/41e9adfbbc201e05e2619efe526c82b7ecc8efb9'>Commit.</a> </li>
<li>Add detecting of clang package to cmake build system. <a href='http://commits.kde.org/umbrello/f5e7ad324325d82f8cbca05a1c1ef9a302a083d1'>Commit.</a> </li>
<li>Add llvm parser test case. <a href='http://commits.kde.org/umbrello/3abff2a605bf2c1a6991017cb5b41ef404fd3ec6'>Commit.</a> </li>
<li>Delete object model in UMLDoc destructor. <a href='http://commits.kde.org/umbrello/3980eaaf5cb7a696c8c71c9fafbba35fe20144db'>Commit.</a> </li>
<li>Fix crash on cleanup of TEST_umlobject. <a href='http://commits.kde.org/umbrello/4dcf2cfc8a9aa51a93c2014ee3d98514c8442004'>Commit.</a> </li>
<li>Fix coverity check CID 169655:    (USE_AFTER_FREE). <a href='http://commits.kde.org/umbrello/ad83879702d80a4e85df7a14f50935dbb1f7d012'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>Fix coverity check CID 155784:  Uninitialized members  (UNINIT_CTOR). <a href='http://commits.kde.org/umbrello/03513107a4fc0703bba116f98888c3c35ec6767c'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>Fix coverity check CID 169653:  Null pointer dereferences  (FORWARD_NULL). <a href='http://commits.kde.org/umbrello/86e3b8cb5d4a9b91ac13266fd001dad3424cf3a6'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>Fix coverity check CID 169652:  Null pointer dereferences  (FORWARD_NULL). <a href='http://commits.kde.org/umbrello/c4389129412005b5c4ceb4ef683a2a5020e71e73'>Commit.</a> See bug <a href='https://bugs.kde.org/340646'>#340646</a></li>
<li>KF5 compile fix. <a href='http://commits.kde.org/umbrello/e1553eb3d71d28c1afd87d074330601e9f781144'>Commit.</a> </li>
<li>Clean log windows on document close. <a href='http://commits.kde.org/umbrello/42150620b4db1fff4997276ff51ee3bdaffd279f'>Commit.</a> </li>
<li>Extend parent check in objects window. <a href='http://commits.kde.org/umbrello/e43b4c195246ceab3f12fe11260fb91cd1f81dd1'>Commit.</a> </li>
<li>Check return value from dynamic_cast against zero in ClassGeneralPage constructor. <a href='http://commits.kde.org/umbrello/c036a0c04d9c967ba1ad2566eb731a663cc066d3'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Complete migration of dynamic_cast to UMLObject::asUML...() wrapper. <a href='http://commits.kde.org/umbrello/3e9f089d6f90085d3e312f3a06d780d9d1c026ff'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Complete migration of UMLObject related static_casts to UMLObject::asUML...() wrapper. <a href='http://commits.kde.org/umbrello/120af5d34c2424c2eb4dd631aea925edb227fded'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Add c++11 code import test cases. <a href='http://commits.kde.org/umbrello/13a513d629e86e8cc98d750f1d9e5741bbc8791a'>Commit.</a> See bug <a href='https://bugs.kde.org/338649'>#338649</a></li>
<li>Extend objects windows with additional states like 'is-saved', 'is-in-parent-list' and object pointer. <a href='http://commits.kde.org/umbrello/368c43e09de3cce0e2faf1ab1db0bd9e495aebcc'>Commit.</a> </li>
<li>Fix regression in type cast migration. <a href='http://commits.kde.org/umbrello/972a4253237b93dae76b18b6980eb8e8c1464ebf'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>KF5 compile fix. <a href='http://commits.kde.org/umbrello/cbef5268b98bef4d6cc8ded766a5ecc63b896b01'>Commit.</a> </li>
<li>Add UMLObject::setUMLParent()/umlParent(). <a href='http://commits.kde.org/umbrello/9d2e1fe6593ad8272bbfe8f6e2a76e4389922bc3'>Commit.</a> </li>
<li>Add property dialog for class UMLFolder for usage from objects dock window. <a href='http://commits.kde.org/umbrello/efa7e85c6e2a55286d64604639a1945a29f5bab5'>Commit.</a> </li>
<li>Keep title setting of diagram and stereotypes window in sync with objects dock window. <a href='http://commits.kde.org/umbrello/3c57b008bc2f732ce2785d04bdf14549aa41e826'>Commit.</a> </li>
<li>Fix selecting of sorted entries in diagrams and stereotypes window. <a href='http://commits.kde.org/umbrello/11f026090b4dce75e08b2e50ae81d2a16b023c39'>Commit.</a> </li>
<li>Add dock window showing all uml objects for further debugging. <a href='http://commits.kde.org/umbrello/d02e8a7e4d53e8ed2aac2de425c5ce554474e757'>Commit.</a> </li>
<li>Classimport cleanup: Move constructor and destructor definitions into cpp file. <a href='http://commits.kde.org/umbrello/37ba24a224b8e94d7e9f3ba06d20b13ab4d92f39'>Commit.</a> </li>
<li>Do not exclude parents of UMLClassifierListItem instances from copying and comparing. <a href='http://commits.kde.org/umbrello/d9b4afea8444320ca51f62dc5fa49d4ee2bfa8b3'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Reduce the number of calls to umlPackage() in UMLObject::fullyQualifiedName() by making a local copy. <a href='http://commits.kde.org/umbrello/191e52ef5dc2e9a03b3a6c42a6566c4a82630d66'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Fix warning about missing brackets and logic. <a href='http://commits.kde.org/umbrello/8c6bad8a76df59a11bf67ce62e76c3bda0f12007'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Fix crash in TEST_UMLObject::test_fullyQualifiedName() caused by creating uml model related objects on stack. <a href='http://commits.kde.org/umbrello/6d5686f264ddfda0e6bf030e16902569e7dc02df'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Removed obsolate UMLObject::m_pUMLPackage from uml model. <a href='http://commits.kde.org/umbrello/badc01d753ca0cb46b51a3fd2ed07d265cf9bfed'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Compile fix. <a href='http://commits.kde.org/umbrello/9f3df0e71f61d76bb4e1c24d571f7e0e82ae3d6e'>Commit.</a> </li>
<li>Replace usage of UMLObject::m_pUMLPackage by QObject member 'parent' through new method umlParent(). <a href='http://commits.kde.org/umbrello/dd847f1576b37f9da31df51084fa3dda243ccc3c'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Fix QPointer related dynamic_casts to use asUML... wrapper(). <a href='http://commits.kde.org/umbrello/fa3f51e764c4296c81454f25b340d00241f172d3'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Convert casts in UML model classes to use related casting methods. <a href='http://commits.kde.org/umbrello/a5f92b67494666e4e6779b1f2443b1a1df2b031d'>Commit.</a> See bug <a href='https://bugs.kde.org/368282'>#368282</a></li>
<li>Reorder some methods in UMLObject according to there declaration. <a href='http://commits.kde.org/umbrello/668c6ad4e493b80d1a8537a840632f98368ba579'>Commit.</a> </li>
<li>KF5 compile fix. <a href='http://commits.kde.org/umbrello/6811456a28954ad5d77949f4b8e1f6bc985589f6'>Commit.</a> See bug <a href='https://bugs.kde.org/368142'>#368142</a></li>
<li>Fix 'Operations property dialog do not show source code'. <a href='http://commits.kde.org/umbrello/e8126809c24240b36418d578dc7248d0495389e5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368142'>#368142</a></li>
<li>Add uml model type checker and wrapper methods to class UMLObject. <a href='http://commits.kde.org/umbrello/98966230b2ff8e18f90402decb622ac96549c220'>Commit.</a> </li>
<li>Fix 'The symbol which represents that a class implements another class is the same as the symbol used for inheritance between classes'. <a href='http://commits.kde.org/umbrello/23d3ebe759390ea8277669a1e38641d56a27497a'>Commit.</a> See bug <a href='https://bugs.kde.org/311808'>#311808</a></li>
<li>INSTALL. <a href='http://commits.kde.org/umbrello/370bb11cfc6343dbabb5d7cbfc61bd5356af7994'>Commit.</a> See bug <a href='https://bugs.kde.org/361479'>#361479</a></li>
<li>Fix umbrello version. <a href='http://commits.kde.org/umbrello/d3933e8646c251eb56150a142ef8cfdd2ac52d0b'>Commit.</a> </li>
<li>Fix bug not showing icons below tree view  settings category. <a href='http://commits.kde.org/umbrello/43a37c9b07b8ed8608a0532869e85a9295cd66ac'>Commit.</a> </li>
</ul>

