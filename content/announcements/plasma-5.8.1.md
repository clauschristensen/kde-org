---
title: KDE Plasma 5.8.1, Bugfix Release for October
release: plasma-5.8.1
description: KDE Ships Plasma 5.8.1.
date: 2016-10-11
layout: plasma
changelog: plasma-5.8.0-5.8.1-changelog
---

{{%youtube class="text-center" id="LgH1Clgr-uE"%}}

{{<figure src="/announcements/plasma-5.8/plasma-5.8.png" alt="KDE Plasma 5.8 " class="text-center" width="600px" caption="KDE Plasma 5.8">}}

Tuesday, 11 October 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.8.1. <a href='https://www.kde.org/announcements/plasma-5.8.0.php'>Plasma 5.8</a> was released in October with many feature refinements and new modules to complete the desktop experience.

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix modifier-only-shortcut after using Alt+Tab
- Support for EGL_KHR_platform_x11
- Fix crash when moving panel between two windows
- Many Wayland fixes, e.g. resize-only-borders
