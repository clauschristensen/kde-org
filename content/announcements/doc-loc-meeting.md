---
title: Linux Congress Documentation and Localization Workshop
date: "2000-10-02"
description: The first meeting of KDE translation and documentation teams has been a success. (...) This success demonstrates that there are strong energies willing to bring Unix Operating Systems and Free Software to the citizen with a documented, easy-to-use graphical interface, in their own language. This is an approach that proprietary operating systems are unable to sustain consistently. Now that computers have come to everyday life, this is putting a threat on cultural independance of many countries with respect to American language. On the contrary, all the people attending the workshop are committed to making computers - even using powerful Operating Systems like GNU/Linux - more easy to use for everyone.
---

Held on 23rd and 24th September, 2000

Report by Eric Bischoff [ebisch@cybercable.tm.fr](mailto:ebisch@cybercable.tm.fr)

Documents:

<ol>
  <li><a href="#summary">Summary</a>
  <li><a href="#agenda">Agenda</a>
  <li><a href="#decisions">Decided action items</a>
  <li><a href="#presentations">Presentations</a>
  <li><a href="#kdeeditorial">KDE Editorial Team Status Report</a>
</ol>
<HR>
<h2><a id="summary">1. Summary</a></h2>
The first meeting of KDE translation and documentation teams has been a
success.
We must thank:
<ul>
  <li>
    the German Unix Users Group for the initiative of this Workshop as
    part of the 7th Linux Kongress
  <li>
    the German Ministry of Education and Scientific Research for the
    funding
  <li>
    the Friedrich-Alexander University of Erlangen-N�rnberg for the
    hosting
  <li>
    the companies Caldera Deutschland and SuSE GmbH for the logistics.
</ul>
KDE is an Open Source software project (<a
  href="http://www.kde.org/index.html">www.kde.org</a>). The purpose of
the KDE Documentation and Localization Workshop was to give people
involved in KDE's documentation and translation an opportunity to:
<ul>
  <li>
    meet
  <li>
    define policies and priorities
  <li>
    coordinate work
  <li>
    solve technical issues.
</ul>
This success demonstrates that there are strong energies willing to
bring Unix Operating Systems and Free Software to the citizen with a
documented, easy-to-use graphical interface, in their own language.
This is an approach that proprietary operating systems are unable to
sustain consistently. Now that computers have come to everyday life,
this is putting a threat on cultural independance of many countries
with respect to American languages. On the contrary, all the people
attending the workshop are committed to making computers - even using
powerful Operating Systems like GNU/Linux - more easy to use for everyone.
Attending team leaders were:
<ul>
  <li>Thomas Diehl - applications translation coordination
  <li>Eric Bischoff - documentation coordination
  <li>Frederik Fouvry - DocBook technical issues
  <li>Stephan Kulow - servers and packaging
  <li>Lauri Watts - documentation editorial team
  <li>Karl Backstr�m - Swedish team
  <li>Juraj Bednar - Slovak team
  <li>G�rkem �etin - Turkish team
  <li>Fran�ois-Xavier Duranceau - French team
  <li>Matthias Kiefer - German team
  <li>Pedro Morais - Portuguese team
  <li>Denis Pershine - Russian team
  <li>Elvis Pfutzenreuter - Portuguese of Brazil team
  <li>Andrea Rizzi - Italian team
  <li>Victor Romero - Spanish team
  <li>Hasso Tepper - Estonian team
  <li>Lukas Tinkl - Czech Team
  <li>Andrej Vernekar - Slovenian team
</ul>
The KDE project (<a
  href="http://www.kde.org/index.html">www.kde.org</a>) represents more
than 50 languages by now and we were sorry not to be able to invite a
representative for each team. We are hoping to be able to reproduce
this event with other persons soon, with a special thought for
Japanese, Chinese, Korean, Hebrew, Arabic, Greek, Dutch, Romanian,
Hungarian, Icelandic, Hindi/Urdu and many other teams.
Persons having collaborated every day for years, either on a voluntary
or on a professional basis, were happy to meet personnaly for the first
time. The ambiance was nice and productive.
Covered areas of work included:
<ul>
  <li>
    applications translation
  <li>
    documentation writing
  <li>
    documentation translation
  <li>
    web servers translation
  <li>
    coordination with other projects.
</ul>
All the subjects of the agenda have been examined (with the exception of
the demonstration of the software tools that each team uses in prevision
of a future integration). A survey of the current situation has been
established and we reached a consensus on the short and long term plans
for the future.
<HR>
<h2><a id="agenda">2. Agenda</a></h2>
<h3>Saturday 23/09/00</h3>
<table class="tables">
  <tr>
    <td VALIGN="TOP">
      10:30 &ensp;
    <td>
      Welcome words, program, thanks to the sponsors
      (Eric Bischoff &amp; Thomas Diehl)
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      10:40 &ensp;
    <td>
      Status report from the national team leaders
      <ul>
        <li>
          accomplishments
        <li>
          projects
        <li>
          way of working
        <li>
          problems encountered
          (Team leaders, 10 minutes each)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      12:30 &ensp;
    <td>
      Lunch
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      14:00 &ensp;
    <td>
      Relations with other Free Software projects
      <ul>
        <li>LDP and Gnome
        <li>GNU/Linux &amp; Unix distributions
        <li>Dewey project
        <li>Li18nux project
        <li>Conglomerate project
        <li>Docbook-tools project
        <li>SGML-tools project
        <li>LSB
        <li>discussion: how can we standardize GNU/Linux help browsers
          (Eric Bischoff)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      14:30 &ensp;
    <td>
      Writing new documentation
      <ul>
        <li>current status of the work
        <li>reading a message from Mike
        <li>quality issues: how does one write a new doc?
        <li>discussion: how to get it faster/more efficient/better quality
          (Lauri Watts)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      15:30 &ensp;
    <td>
      Technical problems (web)
      <ul>
        <li>automatic redirection
          (Fran�ois-Xavier Duranceau)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      15:40 &ensp;
    <td>
      Technical problems (apps)
      <ul>
        <li>context information for strings
          (Stephan Kulow)
        <li>kBabel further improvements
          (Mathias Kiefer)
        <li>right to left suport
          (Hans Peter Bieker)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      16:10 &ensp;
    <td>
      Pause
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      16:20 &ensp;
    <td>
      Demonstrations (installation and usage)
      <ul>
        <li>KDE2 help browser
          (Matthias H�lzer-K�pfel)
        <li>kbabel
        <li>kdedict
          (Matthias Kiefer)
        <li>DocBook tools
        <li>remote doc validation service
        <li>conglomerate
          (Eric Bischoff)
        <li>emacs in PSGML mode
        <li>sgmldiff
          (Frederik Fouvry)
        <li>ispell
        <li>ortho
        <li>French web site
          (Fran�ois-Xavier Duranceau)
        <li>db-gui
          (Andrej Vernekar)
        <li>ktranslator
        <li>flexicon
          (Thomas Diehl)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      18:00 &ensp;
    <td>
      Hacking session
      <ul>
        <li>fix problems
        <li>write doc
        <li>translate doc and apps
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      20:00 &ensp;
    <td>
      Social event at "der Pleitegeier"
</table>
<h3>Sunday 24/09/00</h3>
<table class="tables">
  <tr>
    <td VALIGN="TOP">
      09:00 &ensp;
    <td>
      Technical problems (doc)
      <ul>
        <li>screen shots
          (Lauri Watts)
        <li>FDL licence usage
        <li>the anchors
        <li>localization of the style sheets
          (Frederik Fouvry)
        <li>DocBook philosophy and its implications
        <li>need for a DocBook editor
        <li>move to XML, XSL and DocBook 4.0/4.1?
          (Eric Bischoff)
        <li>CVS branches for doc?
        <li>docs on Web server?
          (Stephan Kulow)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      10:00 &ensp;
    <td>
      Internationalization issues
      <ul>
        <li>unicode and other encodings
        <li>locales
        <li>fonts for non-latin charsets
        <li>conversion &amp; visualization tools
          (Hans-Peter Bieker)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      10:20 &ensp;
    <td>
      Pause
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      10:30 &ensp;
    <td>
      Translation issues
      <ul>
        <li>motivation
        <li>communication with developpers
        <li>putting our work online?
        <li>dispatching the work
        <li>glossaries
        <li>visual dictionnary
          (Eric Bischoff)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      11:30 &ensp;
    <td>
      policies and priorities
      <ul>
        <li>what do we most need?
        <li>what do we do next?
        <li>to freeze or not to freeze?
        <li>distribute separatly docs &amp; apps?
        <li>attitude towards "small" languages
        <li>how can we organize better?
          (Thomas Diehl)
      </ul>
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      12:30 &ensp;
    <td>
      Lunch
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      14:00 &ensp;
    <td>
      Conclusion, farewell words
      (Thomas Diehl &amp; Eric Bischoff)
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      14:10 &ensp;
    <td>
      Visit to Caldera offices
      &nbsp;
  <tr>
    <td VALIGN="TOP">
      15:00 &ensp;
    <td>
      Hacking session
      <ul>
        <li>fix problems
        <li>write doc
        <li>translate doc and apps
      </ul>
</table>
<HR>
<h2><a id="decisions">3. Decided action items</a></h2>
<h3><a id="3.1">3.1 - Translation teams</a></h3>
Almost all the teams have finished applications translation by
now. Documentation rewriting for KDE 2 is 50% complete. Now it's time
to start translating the documentation itself.
If there are opportunities to coordinate dictionaries across projects
(LDP, Gnome, governemental organizations, etc), we must take them.
All the team leaders should be on kde-i18n-doc <TT>;-)</TT>.
Never fix mistakes of the original texts in the translation. Instead,
translate the mistake and forward a bug report.
Don't translate first drafts of documentation in the CVS: always
control the summary page on <a
  href="http://i18n.kde.org/doc">http://i18n.kde.org/doc</a>.
Do not update the <TT>&lt;date&gt;</TT> and the
<TT>&lt;releaseinfo&gt;</TT> when translating - it breaks the upcoming
updates process. Respect the formats 20/12/2000 and 1.01.25 to enable
automation.
Share the information that on a Mandrake, first uninstall the docbook
packages then reinstall the ones at <a
  href="ftp://ftp.kde.org/pub/kde/devel/docbook">ftp://ftp.kde.org/pub/kde/devel/docbook</a>.
<I>Note: the Mandrake packager of DocBook tools knows about this and
ensures this will be fixed for 7.2.</I>
We need to publish again the docs on some web server. Find a machine
first.
We should be prepared for KDE 2 press releases so they get
instantaneously translated. Team leaders should subscribe to kde-pr.
Pedro Morais will write a small "howto deal with translations" for
developers. Topics include anchors and calls to <TT>InvokeHelp()</TT>,
context information in po files, common mistakes, who should / how to
write documentation, etc.
<h3><a id="3.2">3.2 - Developpers</a></h3>
Developpers should help more the documentation writers to write their
doc, at least by answering questions.
When applications move from one package to the other, it should be
posted on kde-i18n-doc mailing list.
The default style and theme has changed again lately despite the UI
freeze! This means we have to redo all the screen shots <TT>:-(</TT>.
The UI freeze on messages should be respected more strictly. We know
it's hard, but...
KDE does not use locales to sort. This is a localization bug.
Find a suitable technical way to add the name of the translators to the
"about" box of the applications (for motivation reasons).
kwrite needs to be able to write files in UTF-8 format, otherwise the
translators keep their time recoding po files.
kdevelop has a documentation mainly done for KDE 1. A big update seems
necessary.
khelpcenter should show up a page to redirect people at run time if
the documentation did not compile, did not exist or was not installed,
to help them solve the problem.
koffice is still moving too much to be able to be localized.
kbabel should add context on the near lines in the po files.
Make sure kdelibs does not generate anchors with upper case in calls to
InvokeHelp() (there seems to be a technical problem here).
<h3><a id="3.3">3.3 - DocBook team</a></h3>
DocBook is still too hard to install despite many past efforts.
Continue the simplification and standardization.
Move the <TT>INSTALL</TT> file for DocBook to the web server.
Document the fact that the screenshot filename should be translated.
Document the fact that doc writers should not use revision histories,
they are duplicate with the CVS logs.
Investigate if it is possible to share an user glossary between
documents while keeping proper markup.
<h3><a id="3.4">3.4 - Editorial team</a></h3>
Merge the two coordination web pages (the one for writers and the one
for translators) (?). Add a section "new docs already updated" to start
the update process of KDE 2 docs.
The doc writers should be more visible on the mailing lists. For
example, they should subscribe to kde-i18n-doc. The can also subscribe
to kde-devel.
There's not enough communication between writers themselves. Create a
new mailing list?
The name and email of the doc author is not enough for reporting bugs
(bouncing emails) and some writers resent giving out their email, which
is their right. We need a mail address for documentation bugs:
<ul>
  <li>
    in khelpcenter pages
  <li>
    on http://bugs.kde.org
    (perharps use the hypotetical new mailing list for doc writers?)
</ul>
We need more style rules (capitalization, vocabulary, etc).
According to the FDL, we should copyright the docs.
In khelpcenter/contact.docbook, there should be a pointer to the teams
page so people know whom to contact to help translating.
Move the visual dictionnary from http://i18n.kde.org/doc to khelpcenter.
<h3><a id="3.5">3.5 - Web server maintainers</a></h3>
To encourage people to translate the Web server, the news should be
separated cleanly, thus enabling to translate only what is more static.
<h3><a id="3.5">3.6 - Troll Tech company</a></h3>
There should be a program to detect conflicts between menu
accelerators at run time in a Qt application.
<BR>
<I>Note: this program has been developped directly at the congress by
Matthias Ettrich and is named "Dr Krash". It's something really great
<TT>:-)</TT>.</I>
<HR>
<h2><a id="presentations">4. Presentations</a></h2>
<h3><a id="4.1">4.1 - Report from the translation teams</a></h3>
Number of persons per team vary from 1 to 20 very active people, and
usually the same quantity of occasional translators, with an average
of 10 active people per team. If we take a reasonable ratio for the
teams that were not represented at the workshop, we can estimate that
KDE has more than 300 active translators and the same number of
occasional helpers. These figures do not include the doc writers (25
active and 35 occasional) and the technical teams (4
active). Occasional helpers sometimes make lose more time than they
help gain because of bad quality.
Motivation is mainly ensured by having people's names in the
newspapers and the files. Other reasons for helping are altruism,
thankfulness to KDE, motivation by guiltiness, being paid by
commercial companies to help, and bad memories of proprietary
software. Articles in newspapers always bring new volunteers but a lot
of them do not stay.
Some teams divide the work by package (kdebase, kdelibs, etc), others
work on a per-file basis. In all teams, only a few people have CVS
access. A lot of coordination work is always necessary besides the
repartition of work; edict rules, maintain local web site, welcome
newcomers, answer or forward questions, dispatch tools and other
resources, explain a lot, and last but notleast, discuss vocabulary.
Mainling list are the preferred way to share information. The
Portuguese team directly puts the work to translate/already translated
on a web site, like it used to be before the move to kde-i18n. There
is a strong interest on doing this aggain for all teams.
A lot of teams suffered people disappearing after KDE 1. They lost a
lot of time and people due to instability of early releases of KDE 2,
people resenting to install it. Same problem for DocBook, even if it
was clear that some people could mark up and/or validate for the
others. But now most teams finished the application messages and will
start translating the documentation which has been half rewritten by
now. Only a few teams translate the KDE web server, the main problem
being the news. The French team translates the news and send them over
to a special mailing list with about 500 subscribers interested in
KDE's life. This team also collects and forwards a lot of bug reports.
On the tools hit-parade, kbabel is the star. Of course vi and emacs
(with or without po and psgml modes) are mentioned a lot too. Next
come midnight commander and kdedict. A lot of small home-made tools
also exist and would gain by being merged into kbabel or at least
being gathered on i18n.kde.org.
Most teams have already moved to UTF-8 encoding. The remaining ones
are requested to do it urgently. This change is too prematurate for
doc and will probably done at the same time as move to XML. Some teams
have problems with fonts and locale (like two letters missing in
iso-latin 1 for French and Slovak). villa@linux.ee and the Czech and
Slovak teams might help a lot on these issues. Distributions are
requested to help fix localization problems.
Some teams already share dictionnaries with other projects (Gnome,
Mozzilla, etc) or institutions. Some teams have to face the varietions
on their language (Portuguese, Spanish, ...). All the teams have
problems on how much technical terms have to be translated from
English, the extremes probably being Italian (translate almost no
technical term) and French (translate nearly all technical terms).
<h3><a id="4.2">4.2 - Coordination with other projects</a></h3>
There are tight contacts and mutual help with LDP (Guylhelm Aznar)
and Gnome (Dan Mueth), as well as many Linux distributions. These
contacts allow to think about the following topics:
<ul>
  <li>
    have metainformation for documentation
    (Dublin Core project), integrated to DocBook or not, like
    we already have Linux Standard Modules (LSMs).
  <li>
    Standardize the help browser (Dewey Project)
  <li>
    Standardize the DocBook tools, in conjunction with the DocBook-tools
    project at sourceware (Mark Galassi), the SGML-tools Lite project
    (Cees De Groot) and Linux Standard Base.
  <li>
    Watch the Li18nux project on infrastructure for localization
  <li>
    Help the conglomerate project to write a DocBook editor, maybe
    have own own someday
</ul>
and more generally to discuss any issue regarding internationalization,
translation and documentation.
<h3><a id="4.3">4.3 - Doc writers</a></h3>
Mike Mc Bride sent a detailed message on the status of the
editorial team (aka "English team") and Lauri read it during the
workshop. People (and especially developers) are encouraged to read
this great report that is attached to this document.
Eric was happy to announce the upcoming DocBook conversion service by
email developped by Laurent Larzilli�re. With this service, doc writers
(and translators) are not supposed anymore to install the DocBook tools.
<h3><a id="4.4">4.4 - Web server</a></h3>
Automatic redirection to the translated version of the main KDE web
server is something nice and on which the user has full control to
accept or not.
Translating a web server is a voluntary choice for big temas that are
able to translate the news regularly.
Because of that, the small teams would prefer a small flag to local
servers rather than automatic redirection. But the main page is already
crowded enough so it looks like there is no easy solution to this
problem.
<h3><a id="4.5">4.5 - Context information in po files</a></h3>
French team needs more context according to whether a term is found in
a menu or in a button due to linguistic conventions.
All slavic languages have problems with many assumptions coming from
English languagei in strings with <TT>%1</TT>, <TT>%2</TT>, etc:
<ul>
  <li>
    English only has singular and plural so two msgids are enough:
    <blockquote>
      <PRE>
  "Are you sure you want to delete this file"
  "Are you sure you want to delete those %1 files"
</PRE>
    </blockquote>
    but the termination of the names and adjectives depend on the last
    digit of the number displayed at <TT>%1</TT> placeholder.
  <li>
    The prepositions for days of the week, and months depend on the day or
    month that follows, while "on" and "in" are enough in English.
  <li>
    Etc
</ul>
<h3><a id="4.6">4.6 - kbabel</a></h3>
The future of kbabel is:
<ul>
  <li>modules for external programs
  <li>support for qml language in po files
  <li>dictionaries and anti-dictionaries
  <li>use other existing po files to start translation of a new one
  <li>context information on surrounding lines
</ul>
<h3><a id="4.7">4.7 - Screenshots</a></h3>
Use ksnapshot, it creates nice lightweight screenshots.
Translators are encouraged to localize the name of screenshot files.
(Note: it's the same for chapter and section IDs, but not for anchors).
<h3><a id="4.8">4.8 - FDL licence</a></h3>
It's a quite complicated licence intended for documentation (and books
in general)
It splits the document into several parts:
<ul>
  <li>title page
  <li>cover text (front and back)
  <li>main section (normal thing)
  <li>secondary sections (off topic)
  <li>invariant sections (might not be changed)
</ul>
Basically at KDE we will produce documentation with no invariant
sections.
<h3><a id="4.9">4.9 - Style sheets for documentation</a></h3>
Can be localized by changing:
<ul>
  <li>the fixed texts
  <li>the typographical conventions
  <li>the order of words
  <li>...
</ul>
They should be in unicode's formal U-xxxx; notation.
<h3><a id="4.10">4.10 - DocBook</a></h3>
The philosophy of this standard impones to think in terms of semantics,
not of desired rendering. This is very important.
<h3><a id="4.11">4.11 - Glossaries</a></h3>
For user glossaries, like the current visual dictionnary after it has
moved to khelpcenter:
If we want to mark them up like DocBook documents, it's not trivial
to share them between documents. Some work on this might be needed.
For translation glossaries, kdedict seems the good format if we want to
check consistency automatically.
<HR>
<h2><a id="kdeeditorial">5. KDE Editorial Team Status Report</a></h2>
<h3>5.1 Background (where we started from):</h3>
In November, 1999, the KDE Editorial Team (at that time called the
KDE English team), was non-existant. The previous documentation
coordinator had stopped answering his emails for many months prior to
my contact with anyone on the KDE project.
I signed on to document KWord, and began writting. At this time, I
was the only member of the documentation/translation team who belonged
to the English team. When I commited my first attempt at KWord
documentation (December 1999), three people wrote me directly to ask
how they could help with KOffice documentation. This was the full
extent of the "English team" until March 2000, when it became clear to
me that no one was stepping forward to help coordinate the English
team, I contacted Eric to offer any help (initially in an unofficial
position) in this area. After a few emails between Eric, Fredrik and
myself, I accepted the role of heading up the English team.
In April, a general call was put out on the KDE Home page, and several
other KDE related locations, asking for volunteers to write
documentation. Over 60 people expressed interest in helping.
Unfortunatly, at the time, KDE was very much an alpha project, and
many of these people became frustrated with the technical hurdles of
downloading, compiling and installing KDE at that stage, and left the
project. Others have left for reasons outside of KDE (school, family,
personal tragedy, etc), and today, the Editorial team consists of
approximately 25 members. Nearly all of these people have commited at
least one piece of documentation to the CVS, and many of them have
commited 3 or more seperate applications.
<h3>5.2 Accomplishments:</h3>
<ol>
  <li>
    The infrastructure is in place, and coordinators (Lauri and Mike) have
    enough background concerning the technical aspects of the KDE project
    to be efficient.
  <li>
    The editorial team has a website with information on writting, marking
    up, and submitting documentation, FAQ's, help for installing
    snapshots, and a status page so anyone can see who is working on which
    applications documentatin, and its status.
  <li>
    The editorial team has completed (in time for KDE 2.0):
    <blockquote>
      <PRE>
kdeadmin   66% (4 of 6)
kdebase  75% (9 of 12)
kdegames 36% (8 of 22)
kdegraphics 20 % (1 of 5)
kdemultimedia 50% (3 of 6)
kdenetwork 20% (2 of 8)
kdepim 50% (1 of 2)
kdeutils 71% (10 of 14)
TOTAL 38 pieces
</PRE>
    </blockquote>
    In order to qualify for this list, all of these documents must be: 1)
    updated for KDE 2.0; 2) Current (to the best of our knowledge), 3)
    Installed correctly, and correctly "attached" to the help features of
    the application; 4) Correctly marked up for docbook 3.1.
  <li>
    The editorial team is currently working on 19 additional
    applications, which are at various stages of development.
  <li>
    Every application in KDE 2.0 correctly loads a help page (even if
    documentation has not been completed).  The most complete and current
    version of documentation is made available, and if the documentation
    has not been written at all, a small help page pointing users to
    helpful links is loaded in its place.
  <li>
    All documentation for unreleased/discontinued applications has been
    moved/removed from the CVS.
  <li>
    All docbook files, compile without errors when using jade and the
    customized KDE files.
  <li>
    Several documenters spent many weeks adding "What is" help to
    kcontrol
    modules, and applications.
</ol>
<h3>5.3 Way of working</h3>
The KDE Editorial team is organized as follows:
Coordinators: Mike McBride (team coordination, recruitment,
helps documenters with technical problems, maintaining/editing CVS for
the team, misc tasks), Lauri Watts (docbook markup,
documentation consistency, proofreading documentation)
Working with the two coordinators, are documenters, which make up the
greatest proportion of our team
There is currently one person, who is proofreading documentation and
watching for application changes.
<h3>5.4 The stated mission of the editorial team is to:</h3>
<ol>
  <li>
    Compose new documentation and update previous versions of
    documentation for KDE applications.
  <li>
    Assist translation teams whenever possible so that they may
    effectively and accurately translate this documentation.
  <li>
    Work to ensure that all English documentation is correct and complete
    with regards to docbook markup.
  <li>
    Recruit new people to write documentation.
</ol>
<h3>5.5 New documentation is currently written as follows:</h3>
<ol>
  <li>
    New documenters contact Mike, who answers questions, asks them to
    read the
    FAQ and guides to writing documentation on the web page.
  <li>
    Documenter downloads a current snapshot and installs it
    (questions/problems are referred to Mike who answers them if he can,
    if not, the questions are referred to kde-devel).
  <li>
    Once installed, the documenters and Mike work together to choose an
    application for them to write about.
  <li>
    The documenter contacts the primary developer on the application.
  <li>
    The editorial team status page is marked "Started", to signify that
    someone is working on that documentation.
  <li>
    The documenter writes the documentation either in plain ASCII, or
    docboook, depending on their level of knowledge of docbook.  If there
    is previous documentation for an application, then the documenter uses
    the "old" documentation as a starting point for the new documentation.
  <li>
    After the documenter feels they have finished their documentation,
    they email the document to either Lauri or Mike (who forwards it
    immediately to Lauri).
  <li>
    Lauri reviews the submitted documentation for: Correct use of docbook
    markup, correct grammer/punctuation, accuracy, completeness, and
    overall organization. Lauri works with the documenter to correct any
    problems.
  <li>
    Lauri sends the corrected documentation to Mike, who verifies the
    docbook is error free, and briefly checks it for obvious errors.  Mike
    then submits the documentation to the CVS, updates the two status
    pages, updates the credits page, and informs the documenter that the
    documentation has been sent to the CVS.
  <li>
    The next time the documenter updates their source code, they check
    over the documentation to make sure everything is being installed
    correctly, and that there are no errors.
  <li>
    The documenter begins at step 3 with the next application.
</ol>
The Editorial team works with translators by helping them with
unfamiliar
words or phrases, updating errors discovered by translators, and by
monitoring the kde-i18n-doc and kde-docbook mailing lists.
<h3>5.6 Problems encountered: (In no particular order)</h3>
<ol>
  <li> Docbook tools are very difficult for most people to install.  Even
    though
    this is not a requirement for documenters, approximatly half of the
    documenters insist on installing this program so that they can check
    their
    work, see how it looks, etc.  More than 10 potential documenters have
    been
    lost from the frustration of installing docbook tools.  I know that Eric
    has
    been working on simplifying this, and the frustration that I have noted,
    has
    been less over the past couple of months, though occasional discussions
    of
    this subject do occur on different mailing lists.
  <li> Early volunteers often had many problems installing KDE, since it was
    still alpha code.  Now that KDE is more stable and robust, volunteers
    have an
    easier time installing and working with KDE, so fewer people are
    leaving.
  <li> It has been very difficult to determine which applications would be
    part
    of which packages (or had been discontinued entirely).  Changes are made
    to
    the contents of packages, and to my knowledge, there is no central
    location
    to determine what applications are in which packages and no official
    announcements are made.
  <li> The exact cutoff for submission of new documentation is not well
    defined
    in any of the recent release schedules.
  <li> Developers fail to return email messages from documenters or
    documentation
    coordinators regarding specific and usually technical questions. This
    has the
    obvious effect of delaying documentation, it also has the more
    signifigant
    effect of making documenters feel unappreciated and "like second class
    citizens".
  <li> Developers will change the user interface of an application without
    telling documenters, without any apparent announcements to help
    documenters
    out, and often after "feature freezes".
    (On September 16, Waldo Bastian has implemented an addition to the
    CVS,
    so that the documentation team is notified whenever developers include
    the
    phrase "GUI" in their CVS commits. It is too early to determine if this
    is
    effective yet, or not).
  <li> Markup choices have been a moving target for us, since many of the
    details
    were worked out as we went along and since Lauri and I were still
    learning
    the in's and outs of docbook as it relates to KDE.
  <li> Documenters have left the project without telling us.
  <li> Documentation that is written in a foreign language must be
    translated
    into English before other translations can translate it, but this
    Language X
    --> English translation is very slow.
  <li> A documenter was split off to help "Proofread the English of the
    user
    inteface."  After I sent him to the kde-i18n-doc mailing list, and told
    the
    people who had been requesting this person, no one ever helped him get
    started with the project, so he sat waiting for over 2 months for some
    help.
    This has resulted in 1) A docuemnter not helping the documentation team
    because he felt uneeded; 2) There are many minor problems with the
    messages
    of applications, that could have been fixed prior to a user interface
    freeze.
  <li> Certain consistency issues have not been successfully implemented.
    This
    is usually due to Lauri or Mike not recognizing the need for consistancy
    in
    this specific situation.
  <li> Snapshot and graphics files locations and requirements were changed
    part
    way through documentation.  The required that all snapshots be updated,
    and
    that many large application documents be edited.  This has not been
    completed.
  <li> Some documentation was written without informing the KDE
    documentation
    team.  This resulted in two instances where work on documentation was
    discarded because of two different versions of documentation for an
    application.
  <li> When applications were moved among the kde packages (into another
    package, to kdenonbeta, or deleted entirely), the documentation was
    often
    left in its original place.  This is really a minor problem, and if I am
    informed of package changes, then I can make sure the documentation is
    where
    it belongs.
</ol>
<h3>5.7 Writing new documentation</h3>
The current status of the KDE documentation project can always be
assessed at <a href="http://i18n.kde.org/teams/en/current.html.">http://i18n.kde.org/teams/en/current.html</a>.
This page lists all applications which the documentation team is aware
of, and what status they are in with reguards to KDE 2.0. After the
release of KDE 2.0, the format of this page will change, but this will
still be the most organized way to see how things progressing.
Translators will be more interested in the "Work in Progress" page,
located at <a
  href="http://i18n.kde.org/doc/work-in-progress.html">http://i18n.kde.org/doc/work-in-progress.html</a>.
Which lists the documentation files, in order of last update. It also
groups each help file into a category (Done, Still using KDE 1.1.x
documentation, Deleted, etc).
<h3>5.8 To summarized the status of the documentation project:</h3>
Approximately 50% of the documentation is completely up to date for
KDE 2.0. Focus has been placed on documentation for applications that
are either very popular (Kmail, konqueror, konsole, kscd, etc), are
used by new users (aKtion, kfloppy, etc) or that are more complex
(Kmail, KWrite, etc...). This was done because it is likely that
these are the applications that users will refer to the help files the
most. Approximately 6 or 7 applications have no documentation at all.
These are applications that are new to KDE, and they had no previous
documentation. When a user selects help, they are presented with a
small help file, which informs them that documentation is not complete
yet, and suggests they visit the kde web page, or use the kde-user
mailing list for answers to questions. The remainder of the
applications are using KDE 1.1.x documentation. These applications
fall into one of three categories: Applications which were not
stablized until late in the process. A good example of this, is
Knotes, which has only stablized its interface and operation in the
past couple weeks. Games. The previous documentation for games
generally contained good information on how the game is played. They
were lacking information on installation, menu entries, etc. Advanced
applications. A good example of this is kwuftpd. It has been
difficult to find someone on the documentation team who feels
comfortable writing documentation about this program. Most of the
members of the documentation team are not network managers, so
sometimes there is a technical hurdle to overcome.
<h3>5.9 What works and what doesn't.</h3>
My initial goal, when I started working on this project, was (of
course), to have all documentation updated for KDE by the release of
KDE 2.0. This goal has not been achieved. Some of the problems that
the team has encountered, I expected, and some problems caught me
entirely by suprise. I am, however, pleased with the progress that
the KDE documentation team has made in these past 6 months when I
consider where we were in April.
<h4>5.9.1 Works</h4>
The infrastructure is in place, and most of the bumps have been worked
out regarding coordination and communication within the team. There
is now a KDE editorial team website (<a
  href="http://i18n.kde.org/teams/en/index.html">http://i18n.kde.org/teams/en/index.html</a>)
, which contains a lot of information. Obviously there is more that I
would like to include on that page, but it is a starting place for
anyone who is interested in helping us. The new application licenses
are working well. It is now possible for the documentation to include
any of the major licenses that are in use in KDE applications.
<h4>5.9.2 Not working well (and some possible solutions)</h4>
<h5>Problem #1</h5>
There is a large communication gap which exists between the
editorial team, and the developers. I have tried to "get the word
out", that the editorial team is out, and hard at work, but I still
get notes from developers who "just found out there is a documentation
team".
I have sent messages to the kde-devel mailing list, but I am sure that
developers (I do this myself), only read the messages that they think
apply to them. So when a message appears about documentation, they
skip right over it, and it never gets read by the people who need the
information.
I think a better solution, is to improve the web presence of the
documentation (and translation) teams. Currently, the Editorial team
web page is buried so deep, I have to give people a URL for them to
find it. There has been talk recently about updating and changing the
KDE web page. I will work with the web designers to make it easier
for people to find out how to help the KDE documentation and
translation effort.
It is also necessary to improve the content of the documentation
servers. After KDE 2.0 is released, I will talk with Eric, Fredrik,
Lauri and anyone else interested in improving the web site. My goals
would be to make a site where people could:
Browse through the applications of KDE, and get a quick summary page
on the application, so that new users could find out which application
suits their needs the best. Use this menu so that users could
download an updated copy of the documentation, download a PDF file of
the documentation, read the documentation on line. They could also
send a message to the application developer, the bug system or the
documentation developer, through email links. Provide useful
instructions on installing new documentation or on
internationalization issues. Provide a system so that application
developers (whos application is not included in the base packages),
can add their documentation and links to the system. Provide an
easier way for people to volunteer as documentation writers or
translators.
This increased presence on the web, will show the developers that the
documentation efforts are important and effective.
<h5>Problem #2</h5>
There is a lot of information that is discussed and decided on in
informal circles (private emails, irc, etc). This is fine, and is
often the most convenient way to discuss information, but this
information must then be recorded, if it impacts people who were not
involved in the discussion. Some examples of information that was not
available to documenters and translators who needed it are:
<ol>
  <li>
    Current Release schedule.  An excellent release schedule was posted
    (<a
      href="http://developer.kde.org/development-versions/release-schedule.html">http://developer.kde.org/development-versions/release-schedule.html</a>),
    which clearly spelled out each step in the release cycle, who was
    responsible for that step, and what was completed.  Unfortunately,
    this page was no longer updated a few weeks later, and remains more
    than 45 days out of date.
  <li>
    Applications have been removed from the KDE release.  It has been
    difficult to ascertain which applications will be included in the KDE
    2.0 release.  Obviously this is a moving target, but no announcements
    were made to any kde mailing lists that I could find when such an
    event happened.
  <li>
    Which applications are part of which package.  This has been equally
    challenging to the documenters because applications are moved between
    packages, or into the kdenonbeta package, without any official
    announcement.
  <li>
    Who maintains the kde packages?  Who maintains the applications?.  It
    is often difficult to determine who you should talk to if you have a
    question about a package or an application.  Sometimes, you can use
    the Help-> About option, sometimes the AUTHORS file will help,
    sometimes, you can review the CVS logs to determine who is making the
    major changes, but sometimes you can not determine who it should be.
</ol>
I propose that two web pages be created (and updated).
A permenant KDE release schedule. This should be a permenant
fixture in the KDE web pages. There should always be some comment
about when the next release is planned. Early on, the release
schedule could be vague ("The release of KDE 2.01 is expected to occur
in the first quarter of 2001"), but as the project gets closer to
release, the web page should become much more detailed and specific.
Since this is a schedule which is primarily driven by development, it
should be maintained by someone who is intimately involved with
developement, and who is included in discussions concerning changes in
release dates.
Another page should be created that lists which applications are
currently within each package, who maintains each package, and the
primary maintainer of each application. A year ago, there was a page
like this for the KDE 1.1.x releases, and it was very helpful to me.
I am willing to maintain this page for the project if I can get
cooperation (people need to let me know when things change), and the
current information (who maintains what).
It is very important that these pages are updated on a regular basis.
<h5>Problem #3</h5>
Developers do not respond to documenters requests for information.
This even extends to documentation coordinators (Lauri and myself).
This produces several effects:
<ol>
  <li> The resulting documentation may be incomplete or inaccurate.
  <li> The documenter feels like a second class citizen to the developer she
    is
    trying to help.
  <li> Documentation updates are delayed.
  <li> Applications do not get documentation in time for release because
    their
    exact status cannot be verified.
</ol>
The solution to problem is obvious. The question is how to convince
developers that this is the best course of events. It seems to me it
is in the developers best interest to answer questions from
documenters. Every bit of documentation that the documenter writes,
the developer doesn't need to worry about. A good manual should
prevent the developer from answering the same question many times over
after its release. (Even if the question is still asked, a polite
RTFM will answer it now). It is not unreasonable to work with
documenters the same way you work with other developers on a project
you are programming on. I hope that everyone involved, will try to
continue to encourage developers to work with documenters.
<h5>Problem #4</h5>
Consistancy. We still need to work on some consistancy issues. These
will be corrected, after the KDE 2.0 release, when there is more time,
and when these issues have been decided. Some issues are:
Naming programs (KControl, Kcontrol, kcontrol, etc). Do we include
application web sites (they may change too often....)? Snapshots.
The standards for snapshots was changed part way through the 2.0
update. Some screen shots were not redone.
There are also some markup consistency issues. These will also be
reworked as time allows after KDE 2.0.