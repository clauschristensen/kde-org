---
title: KDE Ships KDE Applications 18.04.2
description: KDE Ships KDE Applications 18.04.2
date: 2018-06-07
version: 18.04.2
layout: application
changelog: fulllog_applications-18.04.2
---

{{% i18n_var "June 7, 2018. Today KDE released the second stability update for <a href='%[1]s'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-18.04.0" %}}

About 25 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize, Okular, among others.

Improvements include:

- Image operations in Gwenview can now be redone after undoing them
- KGpg no longer fails to decrypt messages without a version header
- Exporting of Cantor worksheets to LaTeX has been fixed for Maxima matrices
