---
title: Plasma 5.9.2 Complete Changelog
version: 5.9.2
hidden: true
plasma: true
type: fulllog
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Make sure we don't show warnings unless it's absolutely necessary. <a href='https://commits.kde.org/discover/d79cb42b7d77dd0c6704a3a40d85d06105de55b4'>Commit.</a>
- Make sure the Remove button doesn't overlap the text. <a href='https://commits.kde.org/discover/e417b9ed230859e29c4d076b7db7bebdd293477f'>Commit.</a>
- Improve local packages display on local packages. <a href='https://commits.kde.org/discover/1a5a9b5f1fa6ead2e3c4f50f1fb0b16e1ac14394'>Commit.</a> See bug <a href='https://bugs.kde.org/376276'>#376276</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Properly retrieve all boolean properties. <a href='https://commits.kde.org/kde-gtk-config/557e20b961fe478ecf7c8a0192830eaf0894098b'>Commit.</a>
- Don't break compatibility with old configs. <a href='https://commits.kde.org/kde-gtk-config/0a7fafc7e2f455a1c4f7ae9a104abce007374572'>Commit.</a>
- Fix gtk-primary-button-warps-slider with GTK 2. <a href='https://commits.kde.org/kde-gtk-config/4a06c4d34f2dd8859058618b1dbe9dbd6df9e9b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376273'>#376273</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Lazy QUrl::fromUserInput in web browser. <a href='https://commits.kde.org/kdeplasma-addons/912ad00230e361f7482d5ddbcb1a830ff397e03e'>Commit.</a>

### <a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a>

- Fix crash in Screen Locker KCM on teardown. <a href='https://commits.kde.org/kscreenlocker/51c4d6c8db8298dbd471fa91de6cb97bf8b4287a'>Commit.</a> See bug <a href='https://bugs.kde.org/373628'>#373628</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [autotests] Add test case for quick tiling on X11. <a href='https://commits.kde.org/kwin/8b4f284249f1ff6a52b7148c0536164c42ceaa73'>Commit.</a> See bug <a href='https://bugs.kde.org/376155'>#376155</a>
- Avoid a crash on Kwin decoration KCM teardown. <a href='https://commits.kde.org/kwin/29179f115c81ff7128bb5430cb27ef7b65f46b33'>Commit.</a> See bug <a href='https://bugs.kde.org/373628'>#373628</a>
- Support creation of PlasmaShellSurface prior to ShellSurface. <a href='https://commits.kde.org/kwin/8edd0336e67d81e93dfe1418dbe2409e2b03d405'>Commit.</a>

### <a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a>

- Look and feel - Window decoration. <a href='https://commits.kde.org/oxygen/dee50cfd16e67014d9c839c554549e0f36f2101a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129867'>#129867</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Fix crash when invoking Present Windows with the group dialog open. <a href='https://commits.kde.org/plasma-desktop/61489110c597ad4e4cb69b004d30dbda8b4110ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376205'>#376205</a>. Fixes bug <a href='https://bugs.kde.org/376234'>#376234</a>
- Fix crash when invoking Present Windows with the group dialog open. <a href='https://commits.kde.org/plasma-desktop/dc17f78ebcc76600040a8d2ef8c57b9a41a8d06e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376205'>#376205</a>. Fixes bug <a href='https://bugs.kde.org/376234'>#376234</a>
- Use proper version for baloo. <a href='https://commits.kde.org/plasma-desktop/07c05cc9f3df35dea4c72cb9c9581cc0b531e1d4'>Commit.</a>
- [Folder View] Don't show script execution prompt on desktop:/. <a href='https://commits.kde.org/plasma-desktop/c2f74cc8c8dd7b814518057be62cea79f930719c'>Commit.</a>
- [Folder View] Support extracting files to sub-directories with drag and drop from Ark. <a href='https://commits.kde.org/plasma-desktop/32c7d2d060c7a942f67737cec63f50dc85e0a6a7'>Commit.</a>
- Fix discover desktopid in favorties. <a href='https://commits.kde.org/plasma-desktop/ecfcc8ea6e494fa118e2d613c59b539bd0a74d2e'>Commit.</a>
- [ContextMenu] Ungrab mouse in taskmanager. <a href='https://commits.kde.org/plasma-desktop/c97c6024ca3276966258f82f3181a23bb4220daa'>Commit.</a>
- [taskmanager] Uniform tasmanager tooltips to systray ones. <a href='https://commits.kde.org/plasma-desktop/4c7022b936da587aa081cddbd419e32597718954'>Commit.</a>
- [Task Manager] Don't import QtQuick 2.7. <a href='https://commits.kde.org/plasma-desktop/5bbedd2c9f61dace6ffc62d993d03adff3063b4d'>Commit.</a> See bug <a href='https://bugs.kde.org/376199'>#376199</a>
- Reverse TaskManager DragDrop to blacklist Plasma instead of whitelisting URLs. <a href='https://commits.kde.org/plasma-desktop/8709b7edd5d17d2369e8ca4d261ec186387fa20e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375871'>#375871</a>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

- Do not treat filename in selection as URL. <a href='https://commits.kde.org/plasma-integration/e70f8134a2bc4b3647e245c05f469aeed462a246'>Commit.</a> See bug <a href='https://bugs.kde.org/376365'>#376365</a>
- QtQuick-based interfaces will now more reliably adapt to color scheme changes at runtime. <a href='https://commits.kde.org/plasma-integration/ab3298b3f5f728765d5afa8830aa7793140617c8'>Commit.</a>

### <a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a>

- Update to KDevplatform API change. <a href='https://commits.kde.org/plasma-sdk/ad6e3b7f937a846ec8a5532bb62f6bf2e923b49b'>Commit.</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Fix manually reordering launchers. <a href='https://commits.kde.org/plasma-workspace/dda4d42e891a386c747fbe0e5698ca019921d3a1'>Commit.</a>
- TaskManager: add icon to cache after painfully querying it over XCB/NETWM. <a href='https://commits.kde.org/plasma-workspace/1f2a372fd840d56b545971bfc9b0610c3ce61ea5'>Commit.</a>
- Pass args to DataEngine superclass constructor. <a href='https://commits.kde.org/plasma-workspace/278cca4e25d9b4e17b98bfde683a78ecc830fb2e'>Commit.</a>
- Turn the NotificationItem in a MouseArea. <a href='https://commits.kde.org/plasma-workspace/4c7335230f90b288fa875ba5d1a589e14bbc86fe'>Commit.</a>
- A minimized config dialog on the desktop (e.g. wallpaper settings) will now be un-minimized when trying to open it multiple times. <a href='https://commits.kde.org/plasma-workspace/92601876d18c0974aa524a84c3765f279c581ed9'>Commit.</a>
- Match QtQuick import to minimum Qt version. <a href='https://commits.kde.org/plasma-workspace/1e9a3875e5bda036bd2060aaedc57932edface72'>Commit.</a> See bug <a href='https://bugs.kde.org/376199'>#376199</a>
- [Clipboard plasmoid] Fix line breaks. <a href='https://commits.kde.org/plasma-workspace/ee26a139378535042411a345fea7d73efc79a73f'>Commit.</a>
- [System Tray] Part Revert "Trigger context menu on press" as this breaks xembedsniproxy. <a href='https://commits.kde.org/plasma-workspace/27b1030756002e91b60ba51483efe9c2c477d16e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375930'>#375930</a>
- Rename Application Menu to Global Menu to avoid conflict. <a href='https://commits.kde.org/plasma-workspace/2bef1c1b1f10d31413388e459fdbca206872238d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375478'>#375478</a>
- [kioslave/remote] Fix porting bugs. <a href='https://commits.kde.org/plasma-workspace/566bbf7ba6bc43b5f00f43667603b9b50efea0cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376131'>#376131</a>
- [Icon Applet] More sensible minimum height. <a href='https://commits.kde.org/plasma-workspace/f0eb59ed931492159a67b2b17b4618fd40fa9d11'>Commit.</a>
- Make services disqualification much stricter. <a href='https://commits.kde.org/plasma-workspace/7b1dc9a4bb039ff5ff2a5b71a2c6bc687ad1db72'>Commit.</a>
- [System Tray Containment] Drop useless Q_INVOKABLE from .cpp file. <a href='https://commits.kde.org/plasma-workspace/172ab88346683fcc0f1dff6b05b3a50b85659e98'>Commit.</a>
- [System Tray Containment] Ungrab mouse before opening context menu. <a href='https://commits.kde.org/plasma-workspace/bcb9ede5caabac3d5086dbcdcccfd7bc44211704'>Commit.</a>
