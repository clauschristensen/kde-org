---
title: KDE Ships KDE Applications 18.08.2
description: KDE Ships KDE Applications 18.08.2
date: 2018-10-11
version: 18.08.2
changelog: fulllog_applications-18.08.2
layout: application
---

{{% i18n_var "October 11, 2018. Today KDE released the second stability update for <a href='%[1]s'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-18.08.0" %}}

More than a dozen recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, KCalc, Umbrello, among others.

Improvements include:

- Dragging a file in Dolphin can no longer accidentally trigger inline renaming
- KCalc again allows both 'dot' and 'comma' keys when entering decimals
- A visual glitch in the Paris card deck for KDE's card games has been fixed
