---
title: KDE Plasma 5.12.8, Bugfix Release for March
release: "plasma-5.12.8"
version: "5.12.8"
description: KDE Ships Plasma 5.12.8.
date: 2019-03-05
changelog: plasma-5.12.7-5.12.8-changelog
layout: plasma
---

{{%youtube id="xha6DJ_v1E4"%}}

{{<figure src="/announcements/plasma-5.12/plasma-5.12.png" alt="KDE Plasma 5.12 LTS Beta " class="text-center" width="600px" caption="KDE Plasma 5.12 LTS Beta">}}

Tuesday, 05 March 2019.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.12.8." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in February 2018 with many feature refinements and new modules to complete the desktop experience." "5.12" %}}

This release adds six months' worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Plasma-pa: fix connecting to PulseAudio with Qt 5.12. <a href="https://commits.kde.org/plasma-pa/3b3b8c3d60e48db47d7b86e61f351bac03fd57b7">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D16443">D16443</a>
- weather dataengine: bbc,envcan,noaa: various fixes to update parsers. <a href="https://commits.kde.org/plasma-workspace/03e13b10d877733528d75f20a3f1d706088f7b9b">Commit.</a>
- SDDM KCM: fix autologin session loading. <a href="https://commits.kde.org/sddm-kcm/634a2dd1bef5dd8434db95a391300f68f30fd14e">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D18765">D18765</a>
