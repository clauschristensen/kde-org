---
title: KDE Plasma 5.6.3, bugfix Release for April
release: "plasma-5.6.3"
description: KDE Ships Plasma 5.6.3.
date: 2016-04-19
layout: plasma
changelog: plasma-5.6.2-5.6.3-changelog
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma-5.6/plasma-5.6.png" alt="KDE Plasma 5.6 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.6">}}

Tuesday, 19 April 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.6.3. <a href='https://www.kde.org/announcements/plasma-5.6.0.php'>Plasma 5.6</a> was released in March with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- KWin: Fix crash on repainting an invalid sizes decoration. The first ever KWin bug fix which is combined with an X11 integration test. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0df4406c2cf8df56f90a7a006eb911775a120886">Commit.</a> Fixes bug <a href="https://bugs.kde.org/361551">#361551</a>
- Fix hover effect on desktop in pager. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7a66cb7d63a505da0e630361dd8ae9377d0ba0d6">Commit.</a> Fixes bug <a href="https://bugs.kde.org/361392">#361392</a>
- Use actual installation information to infer popularity. <a href="http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=204497cdaef32839776e25f04ba73cc8227bbfa4">Commit.</a>
