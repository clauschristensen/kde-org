---
title: KDE Plasma 5.16.4, Bugfix Release for June
release: "plasma-5.16.4"
version: "5.16.4"
description: KDE Ships Plasma 5.16.4.
date: 2019-07-30
layout: plasma
changelop: plasma-5.16.3-5.16.4-changelog
---

{{% youtube id="T-29hJUxoFQ" %}}

{{<figure src="/announcements/plasma-5.16/plasma_5.16.png" alt="Plasma 5.16" class="text-center" width="600px" caption="KDE Plasma 5.16">}}

Tuesday, 30 July 2019.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.16.4." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in June with many feature refinements and new modules to complete the desktop experience." "5.16" %}}

This release adds three week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix compilation with Qt 5.13 (missing include QTime). <a href="https://commits.kde.org/plasma-desktop/7c151b8d850f7270ccc3ffb9a6b3bcd9860609a3">Commit.</a>
- [LNF KCM] make it possible to close the preview. <a href="https://commits.kde.org/plasma-desktop/d88f6c0c89e2e373e78867a5ee09b092239c72de">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D22543">D22543</a>
- Airplane mode improvements. <a href="https://commits.kde.org/plasma-nm/7dd740aa963057c255fbbe83366504bbe48a240e">Commit.</a> Fixes bug <a href="https://bugs.kde.org/399993">#399993</a>. Fixes bug <a href="https://bugs.kde.org/400535">#400535</a>. Fixes bug <a href="https://bugs.kde.org/405447">#405447</a>. Phabricator Code review <a href="https://phabricator.kde.org/D22680">D22680</a>
