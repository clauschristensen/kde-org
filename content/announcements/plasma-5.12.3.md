---
title: KDE Plasma 5.12.3, Bugfix Release for March
release: plasma-5.12.3
version: 5.12.3
description: KDE Ships 5.12.3
date: 2018-03-06
layout: plasma
changelog: plasma-5.12.2-5.12.3-changelog
---

{{%youtube id="xha6DJ_v1E4"%}}

{{<figure src="/announcements/plasma-5.12/plasma-5.12.png" alt="KDE Plasma 5.12 LTS Beta " class="text-center" width="600px" caption="KDE Plasma 5.12 LTS Beta">}}

Tuesday, 6 March 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.12.3." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.12" "March" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "a fortnight's" %}}

- Fix installation of Discover backends. <a href="https://commits.kde.org/discover/523942d2fa0bc93362d49906d973d351f0d95ed1">Commit.</a>
- KWin: Fix the build on armhf/aarch64. <a href="https://commits.kde.org/kwin/3fa287280b04746bebcee436f92545f9f8420452">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D10762">D10762</a>
- Fix the userswitcher when using the mouse for switching. <a href="https://commits.kde.org/plasma-workspace/1eb9ae7e33e2b0cb14ab10bc81710fa4b8f19ef5">Commit.</a> Fixes bug <a href="https://bugs.kde.org/391007">#391007</a>. Phabricator Code review <a href="https://phabricator.kde.org/D10802">D10802</a>
