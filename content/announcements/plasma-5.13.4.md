---
title: KDE Plasma 5.13.4, Bugfix Release for June
release: plasma-5.13.4
version: 5.13.4
description: KDE Ships 5.13.4
date: 2018-07-31
changelog: plasma-5.13.3-5.13.4-changelog
---

{{%youtube id="C2kR1_n_d-g"%}}

{{<figure src="/announcements/plasma-5.13/plasma-5.13.png" alt="Plasma 5.13" class="text-center" width="600px" caption="KDE Plasma 5.13">}}

Tue, 31 Jul 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.13.4." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.13" "June" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "three week's" %}}

- Discover: When sorting by release date, show newer first. <a href="https://commits.kde.org/discover/b6a3d2bbf1a75bac6e48f5ef5e8ace8f770d535c">Commit.</a>
- Don't unintentionally change font rendering when rendering preview images. <a href="https://commits.kde.org/plasma-desktop/79a4bbc36cee399d71f3cfb05429939b0850db25">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D14480">D14480</a>
- Honor ghns KIOSK restriction in new KCMs. <a href="https://commits.kde.org/plasma-desktop/4e2a515bb34f6262e7d0c39c11ee35b6556a6146">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D14041">D14041</a>
