---
title: KDE Ships KDE Applications 17.12.0
description: KDE Ships KDE Applications 17.12.0
date: 2017-12-14
version: 17.12.0
---

December 14, 2017. KDE Applications 17.12.0 are now released.

We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!

### What's new in KDE Applications 17.12

#### System

{{<figure src="/announcements/applications-17.12.0/dolphin1712.gif" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Dolphin</a>, our file manager, can now save searches and limit the search only to folders. Renaming files is now easier; simply double click on the file name. More file information is now at your hands, as the modification date and origin URL of downloaded files are now displayed in the information panel. Additionally, new Genre, Bitrate, and Release Year columns have been introduced." "https://www.kde.org/applications/system/dolphin/" %}}

#### Graphics

{{% i18n_var "Our powerful document viewer <a href='%[1]s'>Okular</a> gained support for HiDPI displays and Markdown language, and the rendering of documents that are slow to load is now shown progressively. An option is now available to share a document via email." "https://www.kde.org/applications/graphics/okular/" %}}

{{% i18n_var "<a href='%[1]s'>Gwenview</a> image viewer can now open and highlight images in the file manager, zooming is smoother, keyboard navigation has been improved, and it now supports the FITS and Truevision TGA formats. Images are now protected from being accidentally removed by the Delete key when they are not selected." "https://www.kde.org/applications/graphics/gwenview/" %}}

#### Multimedia

{{% i18n_var "<a href='%[1]s'>Kdenlive</a> now uses less memory when handling video projects which include many images, default proxy profiles have been refined, and an annoying bug related to jumping one second forward when playing backward has been fixed." "https://www.kde.org/applications/multimedia/kdenlive/" %}}

#### Utilities

{{<figure src="/announcements/applications-17.12.0/kate1712.png" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Ark</a>'s zip support in the libzip backend has been improved. <a href='%[2]s'>Kate</a> has a new <a href='%[3]s'>Preview plugin</a> that allows you to see a live preview of the text document in the final format, applying any available  KParts plugins (e.g. for Markdown, SVG, Dot graph, Qt UI, or patches). This plugin also works in <a href='%[4]s'>KDevelop.</a>" "https://www.kde.org/applications/utilities/ark/" "https://www.kde.org/applications/utilities/kate/" "https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/" "https://www.kde.org/applications/development/kdevelop/" %}}

#### Development

{{<figure src="/announcements/applications-17.12.0/kuiviewer1712.png" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>Kompare</a> now provides a context menu in the diff area, allowing for quicker access to navigation or modification actions. If you are a developer, you might find KUIViewers' new in-pane preview of UI object described by Qt UI files (widgets, dialogs, etc) useful. It now also supports KParts streaming API." "https://www.kde.org/applications/development/kompare/" %}}

#### Office

{{% i18n_var "The <a href='%[1]s'>Kontact</a> team has been hard at work improving and refining. Much of the work has been modernizing the code, but users will notice that encrypted messages display has been improved and support has been added for text/pgp and <a href='%[2]s'>Apple® Wallet Pass</a>. There is now an option to select IMAP folder during vacation configuration, a new warning in KMail when a mail gets reopened and identity/mailtransport is not the same, new <a href='%[3]s'>support for Microsoft® Exchange™</a>, support for Nylas Mail and improved Geary import in the akonadi-import-wizard, along with various other bug-fixes and general improvements." "https://www.kde.org/applications/office/kontact" "https://phabricator.kde.org/D8395" "https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/" %}}

#### Games

{{<figure src="/announcements/applications-17.12.0/ktuberling1712.jpg" width="600px" >}}

{{% i18n_var "<a href='%[1]s'>KTuberling</a> can now reach a wider audience, as it has been <a href='%[2]s'>ported to Android</a>. <a href='%[3]s'>Kolf</a>, <a href='%[4]s'>KsirK</a>, and <a href='%[5]s'>Palapeli</a> complete the porting of KDE games to Frameworks 5." "https://www.kde.org/applications/games/ktuberling/" "https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html" "https://www.kde.org/applications/games/kolf/" "https://www.kde.org/applications/games/ksirk/" "https://www.kde.org/applications/games/palapeli/" %}}

### More Porting to KDE Frameworks 5

{{% i18n_var "Even more applications which were based on kdelibs4 have now been ported to KDE Frameworks 5. These include the music player <a href='%[1]s'>JuK</a>, the download manager <a href='%[2]s'>KGet</a>, <a href='%[3]s'>KMix</a>, utilities such as <a href='%[4]s'>Sweeper</a> and <a href='%[5]s'>KMouth</a>, and <a href='%[6]s'>KImageMapEditor</a> and Zeroconf-ioslave. Many thanks to the hard-working developers who volunteered their time and work to make this happen!" "https://www.kde.org/applications/multimedia/juk/" "https://www.kde.org/applications/internet/kget/" "https://www.kde.org/applications/multimedia/kmix/" "https://www.kde.org/applications/utilities/sweeper/" "https://www.kde.org/applications/utilities/kmouth/" "https://www.kde.org/applications/development/kimagemapeditor/" %}}

### Applications moving to their own release schedule

{{% i18n_var "<a href='%[1]s'>KStars</a> now has its own release schedule; check this <a href='%[2]s'>developer's blog</a> for announcements. It is worth noting that several applications such as Kopete and Blogilo are <a href='%[3]s'>no longer shipped</a> with the Application series, as they have not yet been ported to KDE Frameworks 5, or are not actively maintained at the moment." "https://www.kde.org/applications/education/kstars/" "https://knro.blogspot.de" "https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore" %}}

### Bug Stomping

More than 110 bugs have been resolved in applications including the Kontact Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello and more!

### Full Changelog

{{% i18n_var "If you would like to read more about the changes in this release, <a href='%[1]s'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had." "../fulllog_applications-17.12.0" %}}

#### Spread the Word

Non-technical contributors play an important part in KDE's success. While proprietary software companies have huge advertising budgets for new software releases, KDE often depends on word of mouth.

{{% i18n_var "There are many ways to support the KDE Applications 17.12 release: you can report bugs, encourage others to join the KDE Community, or <a href='%[1]s'>support the non-profit organization behind the KDE Community</a>." "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5" %}}

Help us spread the word on the social web. You can submit stories to news sites like Reddit, Facebook and Twitter; upload screenshots of your new set-up to services like Snapchat, Instagram and Google+; or create screencasts and upload them to YouTube, Blip.tv, and Vimeo, or stream them live over Twitch!

Remember to tag your posts and uploaded materials with the <i>KDE</i> moniker, as this makes them easier to find and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 17.12 release.

#### Installing KDE Applications 17.12 Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 17.12 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.

#### Compiling KDE Applications 17.12

The complete source code for KDE Applications 17.12 may be <a href='http://download.kde.org/stable/applications/17.12.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='../../info/applications-17.12.0'>KDE Applications 17.12.0 Info Page</a>.

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
