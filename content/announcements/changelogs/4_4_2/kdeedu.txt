------------------------------------------------------------------------
r1098879 | jmhoffmann | 2010-03-05 04:35:02 +1300 (Fri, 05 Mar 2010) | 7 lines

Improve detection of gpsd API.
Even if API major is bumped, it does not mean that this code won't build. So
simply use >=.
Patch contributed by Modestas Vainius <modax@debian.org>.
Backport of r1098876 from trunk.
CCMAIL: modax@debian.org

------------------------------------------------------------------------
r1099268 | scripty | 2010-03-05 23:52:48 +1300 (Fri, 05 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1099460 | arieder | 2010-03-06 05:40:47 +1300 (Sat, 06 Mar 2010) | 2 lines

backport of r1099455: only install cantor_r.knsrc if Cantor is build with R support

------------------------------------------------------------------------
r1100404 | arieder | 2010-03-08 02:28:01 +1300 (Mon, 08 Mar 2010) | 2 lines

backport of r1100397: use the id instead of name to identify the right kns file (name might be translated)

------------------------------------------------------------------------
r1101203 | whiting | 2010-03-10 04:17:11 +1300 (Wed, 10 Mar 2010) | 5 lines

Backport German Dvorak keyboard layouts to 4.4 branch so they will make it into 4.4.2
Thanks to Thomas Michael Weissel <flexible@xapient.net>
CC: flexible@xapient.net


------------------------------------------------------------------------
r1103342 | nielsslot | 2010-03-15 10:28:29 +1300 (Mon, 15 Mar 2010) | 7 lines

Backport revision 1103340.

Don't call run() when we're executing from the console.

This in turn will connect a signal that will allow a call to the inspector dockwidget. This will crash since we don't keep a tree there code from the console.
BUG: 230752

------------------------------------------------------------------------
r1103900 | mlaurent | 2010-03-16 21:39:23 +1300 (Tue, 16 Mar 2010) | 2 lines

Backport fix mem leak

------------------------------------------------------------------------
r1103902 | mlaurent | 2010-03-16 21:46:21 +1300 (Tue, 16 Mar 2010) | 3 lines

Backport:
fix an other mem leak

------------------------------------------------------------------------
r1104036 | gladhorn | 2010-03-17 03:40:15 +1300 (Wed, 17 Mar 2010) | 3 lines

backport r1104035 for 4.4.2
remove tip of the day that says parley is not released

------------------------------------------------------------------------
r1105635 | scripty | 2010-03-21 09:15:09 +1300 (Sun, 21 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
