------------------------------------------------------------------------
r1120740 | grossard | 2010-04-30 08:31:30 +1200 (Fri, 30 Apr 2010) | 2 lines

added a translator

------------------------------------------------------------------------
r1121273 | scripty | 2010-05-01 13:45:18 +1200 (Sat, 01 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1121957 | carewolf | 2010-05-03 08:00:57 +1200 (Mon, 03 May 2010) | 3 lines

Check HTML5 media URLs. 
Back-port of c1121955

------------------------------------------------------------------------
r1122130 | fdekruijf | 2010-05-03 20:23:07 +1200 (Mon, 03 May 2010) | 1 line

update
------------------------------------------------------------------------
r1122506 | aseigo | 2010-05-04 15:27:42 +1200 (Tue, 04 May 2010) | 2 lines

backport r1122503

------------------------------------------------------------------------
r1122692 | orlovich | 2010-05-05 01:55:22 +1200 (Wed, 05 May 2010) | 5 lines

Merged revision:r1122691 | orlovich | 2010-05-04 09:53:57 -0400 (Tue, 04 May 2010) | 4 lines

Permit script kids for option. Used by some LinkSys router UIs.

BUG:236242
------------------------------------------------------------------------
r1122915 | scripty | 2010-05-05 13:54:41 +1200 (Wed, 05 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1123435 | orlovich | 2010-05-06 15:26:26 +1200 (Thu, 06 May 2010) | 6 lines

Merged revision:r1123418 | orlovich | 2010-05-05 22:23:22 -0400 (Wed, 05 May 2010) | 5 lines

Apparently some router boxes like empty usernames (and empty passwords are in 
principle fine too), so permit that.

BUG: 236267
------------------------------------------------------------------------
r1123585 | trueg | 2010-05-06 22:16:33 +1200 (Thu, 06 May 2010) | 1 line

Backport: We treat QUrl and Resource as interchangable, thus operator== should reflect that, too
------------------------------------------------------------------------
r1123596 | grossard | 2010-05-06 22:44:32 +1200 (Thu, 06 May 2010) | 2 lines

added an entity for a new translator

------------------------------------------------------------------------
r1124101 | vhanda | 2010-05-08 07:22:23 +1200 (Sat, 08 May 2010) | 1 line

BACKPORT: Nepomuk::ResourceData::removeProperty() should update the cache on completion.
------------------------------------------------------------------------
r1124675 | carewolf | 2010-05-10 08:29:35 +1200 (Mon, 10 May 2010) | 3 lines

Correct checkUserAuth DBUS call.
backport of c1124673

------------------------------------------------------------------------
r1124935 | dhaumann | 2010-05-10 22:46:04 +1200 (Mon, 10 May 2010) | 9 lines

backport SVN commit 1124513 by dhaumann:
    add auto completion for 'help'
    
    Since 'help' is not implemented as command, the auto completion did
    not work. Hence, solve it by adding help as a persistent item to
    the completion object.
    
    fixes bug #176070

------------------------------------------------------------------------
r1124936 | dhaumann | 2010-05-10 22:47:16 +1200 (Mon, 10 May 2010) | 14 lines

SVN commit 1124515 by dhaumann:
    fix modified flag in saved document state
    
    - Open an existing file (state A).
    - Modify the file (state B).
    - Save the file (file saved in state B).
    - Undo several times (state C between state A and state B).
    - Don't save the file: file state is "modified" (ok)
    - Redo everything (go back to state B).
    File state is still "modified": should be "non modified" since we are in state
    B and the file has been saved in state B.
    
    fixes bug #188429

------------------------------------------------------------------------
r1125185 | dfaure | 2010-05-11 10:05:49 +1200 (Tue, 11 May 2010) | 3 lines

Provide a fake SVGAngle javascript constructor so that the Confluence wiki works in khtml
(it uses svg if SVGAngle exists, and otherwise falls back to IE's VML stuff).

------------------------------------------------------------------------
r1125252 | scripty | 2010-05-11 14:37:44 +1200 (Tue, 11 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1125875 | lunakl | 2010-05-13 03:31:59 +1200 (Thu, 13 May 2010) | 7 lines

Backport: When writing to kdebugrc whether debug output is enabled or disabled,
do so only for kDebug(), since kWarning() etc. are enabled even in release
build. This prevents debug output from getting enabled by default in release
builds when an app calls kWarning() before first kDebug().
CCBUG: 227089


------------------------------------------------------------------------
r1126218 | mart | 2010-05-13 22:14:49 +1200 (Thu, 13 May 2010) | 2 lines

backport fix to 235851

------------------------------------------------------------------------
r1126664 | ppenz | 2010-05-15 03:30:06 +1200 (Sat, 15 May 2010) | 4 lines

Backport of SVN commit 1126660: Fix 2 issues in combination with natural sorting.

CCBUG: 237541
CCBUG: 237551
------------------------------------------------------------------------
r1127443 | orlovich | 2010-05-17 03:56:21 +1200 (Mon, 17 May 2010) | 5 lines

Backport: don't double-escape URLs.
Thanks to reavertm for testing.

BUG: 237734

------------------------------------------------------------------------
r1127472 | carewolf | 2010-05-17 06:09:31 +1200 (Mon, 17 May 2010) | 3 lines

Backport of natural sort fix
CCBUG: 234273

------------------------------------------------------------------------
r1127474 | carewolf | 2010-05-17 06:22:29 +1200 (Mon, 17 May 2010) | 2 lines

Fix typo not caught by my first tests

------------------------------------------------------------------------
r1127782 | ppenz | 2010-05-18 04:10:19 +1200 (Tue, 18 May 2010) | 4 lines

Backport of SVN commit 1127780: If a case insensitive comparison is done and the strings are equal, a case sensitive comparison must be done as fallback to have a deterministic sorting order. Otherwise it is possible that items "toggle" during hovering.

CCBUG: 217048
CCBUG: 237621
------------------------------------------------------------------------
r1127891 | mart | 2010-05-18 10:18:50 +1200 (Tue, 18 May 2010) | 2 lines

backport null pointer fix

------------------------------------------------------------------------
r1128088 | cfeck | 2010-05-18 22:00:43 +1200 (Tue, 18 May 2010) | 4 lines

Only hide horizontal scroll bar when isCategorized (backport r1125133)

CCBUG: 237109

------------------------------------------------------------------------
r1128208 | dfaure | 2010-05-19 05:16:05 +1200 (Wed, 19 May 2010) | 4 lines

Use the KReplace API the intended way, so that replacements with placeholders (\0) work.
BUG: 140970
FIXED-IN: 4.4.4

------------------------------------------------------------------------
r1128233 | mfuchs | 2010-05-19 06:41:15 +1200 (Wed, 19 May 2010) | 4 lines

Backport r1128227
When deleting an XDG path in the path KCM it is set to $HOME as suggested in the XDG-user-dirs spec.
This fixes some issues e.g. KGlobalSettings::downloadPath() pointing to "/".

------------------------------------------------------------------------
r1128871 | carewolf | 2010-05-21 02:25:36 +1200 (Fri, 21 May 2010) | 2 lines

Backport revert of rendundant fix.

------------------------------------------------------------------------
r1129507 | hpereiradacosta | 2010-05-23 05:29:14 +1200 (Sun, 23 May 2010) | 7 lines

Backport r1129506
paint klineEdit 'click' message (e.g. in systemsettings 'search' bar) in a 
way that is similar to what qlineedit::paintEvent does for normal text. 
This notably fixes the vertical alignment of the "click" message for some 
styles, including oxygen (see attached screenshot)


------------------------------------------------------------------------
r1130782 | trueg | 2010-05-26 22:09:50 +1200 (Wed, 26 May 2010) | 5 lines

Backport: exclude the class or property itself from the parent and children lists to avoid endless loops
and, thus, crashes when calling methods like isSubClassOf.

CCMAIL: nlecureuil@mandriva.com

------------------------------------------------------------------------
r1130787 | trueg | 2010-05-26 22:16:35 +1200 (Wed, 26 May 2010) | 2 lines

Do not call store recursively when a resource is its own grounding occurrence.
CCMAIL: nlecureuil@mandriva.com
------------------------------------------------------------------------
r1130801 | trueg | 2010-05-27 00:14:41 +1200 (Thu, 27 May 2010) | 1 line

Backport: properly set the file url to the kickoff uri
------------------------------------------------------------------------
r1131222 | orlovich | 2010-05-28 05:29:57 +1200 (Fri, 28 May 2010) | 3 lines

Backport r1131202.
CCBUG:238977

------------------------------------------------------------------------
r1131228 | cullmann | 2010-05-28 06:00:40 +1200 (Fri, 28 May 2010) | 5 lines

backport fix for 226409
wrong stuff in smart leads to random segfault
BUG: 226409
this is a stupid and ugly workaround

------------------------------------------------------------------------
r1131524 | mueller | 2010-05-28 22:15:55 +1200 (Fri, 28 May 2010) | 2 lines

version bump

------------------------------------------------------------------------
