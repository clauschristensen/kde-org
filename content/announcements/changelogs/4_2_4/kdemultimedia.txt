------------------------------------------------------------------------
r964139 | lueck | 2009-05-06 06:10:30 +0000 (Wed, 06 May 2009) | 3 lines

remove double entry artist in categories
word wrap text to display it completely in the dialog
set minimum number of digits to 1, can't be less
------------------------------------------------------------------------
r966493 | scripty | 2009-05-11 09:07:21 +0000 (Mon, 11 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r966855 | jriddell | 2009-05-12 00:55:56 +0000 (Tue, 12 May 2009) | 1 line

make it compile with Phonon build from Qt
------------------------------------------------------------------------
r966858 | jriddell | 2009-05-12 01:01:27 +0000 (Tue, 12 May 2009) | 1 line

make it compile with Phonon build from Qt
------------------------------------------------------------------------
r966935 | jriddell | 2009-05-12 10:10:17 +0000 (Tue, 12 May 2009) | 1 line

revert phonon header change, Qt creates Global now, http://qt.gitorious.org/qt/qt/commit/5299240db14579960358edeebfc72fcef905af13
------------------------------------------------------------------------
r967309 | scripty | 2009-05-13 08:05:33 +0000 (Wed, 13 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r970329 | rkcosta | 2009-05-20 03:28:52 +0000 (Wed, 20 May 2009) | 5 lines

Backport commit 970328.

CCBUG: 184117
CCMAIL: ian.monroe@gmail.com

------------------------------------------------------------------------
r971233 | rkcosta | 2009-05-22 04:35:57 +0000 (Fri, 22 May 2009) | 9 lines

Backport commit 971230.

Remove hack that immediately displayed the toolbar when
the mouse was moved over its position if it was at the top,
even when it was on the left, right or at the bottom.

CCBUG: 170071
CCMAIL: imonroe@kde.org

------------------------------------------------------------------------
r971240 | rkcosta | 2009-05-22 05:03:27 +0000 (Fri, 22 May 2009) | 7 lines

Backport commit 971239.

Fix tenSecondsForward() and tenSecondsBack() as they were jumping
ten miliseconds instead of ten seconds.

CCBUG: 189626

------------------------------------------------------------------------
r972061 | scripty | 2009-05-24 07:42:56 +0000 (Sun, 24 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r972951 | rkcosta | 2009-05-26 04:37:58 +0000 (Tue, 26 May 2009) | 2 lines

Backport 972949. Change the default step time in the position slider to 5 seconds.

------------------------------------------------------------------------
r972957 | rkcosta | 2009-05-26 04:56:50 +0000 (Tue, 26 May 2009) | 4 lines

Backport commit 972956.

Add the Previous Chapter and Next Chapter actions to the menu.

------------------------------------------------------------------------
