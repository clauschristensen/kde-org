------------------------------------------------------------------------
r877945 | abizjak | 2008-10-30 18:52:18 +0000 (Thu, 30 Oct 2008) | 2 lines

backport alternative fs drivers support from revision 877943

------------------------------------------------------------------------
r878703 | pino | 2008-11-01 17:10:06 +0000 (Sat, 01 Nov 2008) | 2 lines

fix icon (konsole -> utilities-terminal)

------------------------------------------------------------------------
r879286 | pino | 2008-11-02 22:03:18 +0000 (Sun, 02 Nov 2008) | 5 lines

Backport:
Compare the items in a shortcut list in a "natural" way.

CCBUG: 174114

------------------------------------------------------------------------
r879591 | woebbe | 2008-11-03 14:12:30 +0000 (Mon, 03 Nov 2008) | 3 lines

splitArgs(): really don't break on trivial case by applying code from trunk exactly

CCMAIL:ossi@kde.org
------------------------------------------------------------------------
r879898 | berendsen | 2008-11-04 10:19:34 +0000 (Tue, 04 Nov 2008) | 2 lines

Backport rev.879897 to 4.1 branch (LilyPond syntax update)

------------------------------------------------------------------------
r879946 | xvello | 2008-11-04 12:23:15 +0000 (Tue, 04 Nov 2008) | 3 lines

backporting r879904 : "add a warning if only version 3 of libmusicbrainz is found, as version 2 is needed"


------------------------------------------------------------------------
r880185 | porten | 2008-11-04 21:52:07 +0000 (Tue, 04 Nov 2008) | 2 lines

Merged revision 880184:
Implement createImageData() canvas function.
------------------------------------------------------------------------
r880220 | porten | 2008-11-04 23:09:22 +0000 (Tue, 04 Nov 2008) | 6 lines

Merged revision 880218:
Fix the assertion reported in #158532 in others. An empty text
caused a spike in the outline that the outline painting algorithm
got confused about. Remove such spikes formed by start and end point
now and dynamically calculate the initial borderside and direction
values.
------------------------------------------------------------------------
r880242 | dfaure | 2008-11-05 00:02:10 +0000 (Wed, 05 Nov 2008) | 2 lines

Backport 880241, fix for bug 173927 (Crash in KDirModelPrivate::nodeForUrl() if path contains two or more consecutive slashes)

------------------------------------------------------------------------
r880255 | dfaure | 2008-11-05 00:52:36 +0000 (Wed, 05 Nov 2008) | 7 lines

Better iterate over a copy than risk a detach that invalidates our iterator.
I couldn't reproduce bug 168601 (even though kcookiejartest calls this method and ends up deleting
a bunch of domains), but the backtrace clearly points to this possibility (I guess in my case
there was no detach happening...).

BUG: 168601

------------------------------------------------------------------------
r880264 | dfaure | 2008-11-05 01:15:30 +0000 (Wed, 05 Nov 2008) | 2 lines

backport 880260 (check program exists before downloading the file) - introduces a new i18n, but for a corner-case error-msg that's better than keeping that bug even longer.

------------------------------------------------------------------------
r880402 | woebbe | 2008-11-05 11:49:14 +0000 (Wed, 05 Nov 2008) | 1 line

compile
------------------------------------------------------------------------
r880601 | porten | 2008-11-05 22:39:55 +0000 (Wed, 05 Nov 2008) | 6 lines

Merged revision 880599:
One more hack for HTML comments in JS: all other browsers
all for C-style comments before -->.

I removed the C++ comment check for the beginning-of-line
marker as I don't really see its purpose anymore.
------------------------------------------------------------------------
r880719 | mkretz | 2008-11-06 10:01:16 +0000 (Thu, 06 Nov 2008) | 1 line

backport: nowadays HAL has the info.subsystem key with the value sound, which we don't care about we want to know whether it's USB or PCI or whatever, so when we see sound as subsystem we look at the grandparent
------------------------------------------------------------------------
r880847 | lunakl | 2008-11-06 17:02:11 +0000 (Thu, 06 Nov 2008) | 7 lines

Backport the '_k_session:' feature for shortcuts that should not be
remembered by kdedglobalaccel. Also, when such shortcut becomes inactive (i.e.)
goes away, dump it completely, as this would otherwise prevent reuse in the same session.
I'll also forwardport.
CCMAIL: Michael Jansen <kde@michael-jansen.biz>


------------------------------------------------------------------------
r880882 | pino | 2008-11-06 18:49:31 +0000 (Thu, 06 Nov 2008) | 2 lines

no need for tree-like decoration in the ACL list

------------------------------------------------------------------------
r880976 | rpedersen | 2008-11-07 01:06:18 +0000 (Fri, 07 Nov 2008) | 1 line

SVN_SILENT remove noise
------------------------------------------------------------------------
r880982 | porten | 2008-11-07 01:40:13 +0000 (Fri, 07 Nov 2008) | 3 lines

Merged revision 880981:
Same as for normal input elements: don't submit the form
when return was pressed in a completion popup.
------------------------------------------------------------------------
r881044 | lunakl | 2008-11-07 11:10:04 +0000 (Fri, 07 Nov 2008) | 5 lines

Merge r861974 fron trunk:
Fix a crash when stopping kded. _self is nulled, then the modules are
unloaded. khotkeys tries to use kdedglobalaccel which results in a crash.


------------------------------------------------------------------------
r881045 | lunakl | 2008-11-07 11:11:40 +0000 (Fri, 07 Nov 2008) | 5 lines

Make all these calls blocking, since Qt's in-process delivery seems
to be broken and results in non-blocking calls being processed
after blocking ones (breaks khotkeys kded module).


------------------------------------------------------------------------
r881252 | rdieter | 2008-11-07 15:21:18 +0000 (Fri, 07 Nov 2008) | 4 lines

backport fix for bug #172182

CCBUG: 172182

------------------------------------------------------------------------
r881716 | porten | 2008-11-08 20:59:48 +0000 (Sat, 08 Nov 2008) | 6 lines

Merged revision 881715:
Prevent a crash triggered by an internal error. The real
fix will be to recognize e.g. <object> as a block-level
element I think.

CCBUG:84498
------------------------------------------------------------------------
r882504 | orlovich | 2008-11-10 20:56:39 +0000 (Mon, 10 Nov 2008) | 6 lines

Restore the use of a forwarding factory method in khtmlimagepart. 
This makes this work right with --as-needed. Before this,
khtmlimagepart was literally an empty .so, and would 
rely on linking to libkhtml to get the entry point --- linking 
which -as-needed would kill, as nothing within module referred to it.

------------------------------------------------------------------------
r883156 | dfaure | 2008-11-12 12:13:24 +0000 (Wed, 12 Nov 2008) | 2 lines

Backport 883155: fix KWordWrap for the case where the input string has \n (and convert test to a unittest).

------------------------------------------------------------------------
r883330 | orlovich | 2008-11-12 18:06:06 +0000 (Wed, 12 Nov 2008) | 6 lines

Unbreak "don't ask again" checkboxes in various KIO dialogs, such as the https -> http one.
This got lost in one of the work branches; thankfully SlaveInterface is fully 
prepared to receive this info, so there are no compat worries

Thanks to Chani for reporting this.

------------------------------------------------------------------------
r884479 | ereslibre | 2008-11-15 02:47:20 +0000 (Sat, 15 Nov 2008) | 4 lines

Backport fix. Allow the user to write accented characters while the completion box is shown.

CCBUG: 174283

------------------------------------------------------------------------
r884488 | ereslibre | 2008-11-15 04:25:09 +0000 (Sat, 15 Nov 2008) | 4 lines

Backport fix. Don't mix up with both treeview and lineedit emitting signals at the same time.

CCBUG: 174697

------------------------------------------------------------------------
r884966 | ilic | 2008-11-16 12:37:21 +0000 (Sun, 16 Nov 2008) | 1 line

Complement toUpperFirst with toLowerFirst. (bport: 884964)
------------------------------------------------------------------------
r885091 | orlovich | 2008-11-16 16:11:43 +0000 (Sun, 16 Nov 2008) | 3 lines

Don't try to restore that which is not stored --- fixes bogus * 
in password fields when going back.

------------------------------------------------------------------------
r885095 | orlovich | 2008-11-16 16:15:23 +0000 (Sun, 16 Nov 2008) | 2 lines

Fix crashes on the cacheUpdate special --- hopefully, anyway, since I can't reproduce them

------------------------------------------------------------------------
r885372 | ggarand | 2008-11-17 08:18:42 +0000 (Mon, 17 Nov 2008) | 11 lines

automatically merged revision 884887:
make some efforts to reset the widget flags and uninstall the special event filter
when switching a widget from internal to native (e.g when some ns plugin
was found inside an iframe).

.dodges the broken interaction between WA_OpaquePaintEvent flag and
 QWidget::scroll()'s 3 args version that's causing scrolling artifacts.

.should also fix update event cycle of #174660.

BUG: 173165, 174660
------------------------------------------------------------------------
r885373 | ggarand | 2008-11-17 08:21:25 +0000 (Mon, 17 Nov 2008) | 2 lines

automatically merged revision 885364:
make clear() work in KSqueezedTextLabel.
------------------------------------------------------------------------
r885374 | ggarand | 2008-11-17 08:22:20 +0000 (Mon, 17 Nov 2008) | 7 lines

automatically merged revision 885366:
Apply patch by Frank Reininghaus <frank78ac googlemail com> :

properly clear the focus on form widgets, to avoid crashing when
changing the view profile.

BUG: 173473
------------------------------------------------------------------------
r885848 | porten | 2008-11-18 00:29:04 +0000 (Tue, 18 Nov 2008) | 4 lines

Merged revision 885847:
Don't create child renderers for render objects that don't
allow it. Fixes <select> in <applet> crash from bug #84498
and other cases I found by inspection.
------------------------------------------------------------------------
r886418 | scripty | 2008-11-19 08:23:31 +0000 (Wed, 19 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r886844 | wstephens | 2008-11-20 10:04:35 +0000 (Thu, 20 Nov 2008) | 3 lines

Backport r880489 to 4.1 
CCBUG:150772

------------------------------------------------------------------------
r886953 | rdieter | 2008-11-20 18:09:13 +0000 (Thu, 20 Nov 2008) | 4 lines

backport unmess Utilities menu

BUG: 161117

------------------------------------------------------------------------
r887309 | sengels | 2008-11-21 15:12:40 +0000 (Fri, 21 Nov 2008) | 2 lines

backport r887303
(fix knewstuff download)
------------------------------------------------------------------------
r887726 | orlovich | 2008-11-22 17:06:43 +0000 (Sat, 22 Nov 2008) | 2 lines

Backport button grouping fix.

------------------------------------------------------------------------
r888820 | dfaure | 2008-11-25 12:33:21 +0000 (Tue, 25 Nov 2008) | 2 lines

don't include passwords in url drags

------------------------------------------------------------------------
r888880 | dfaure | 2008-11-25 13:01:39 +0000 (Tue, 25 Nov 2008) | 2 lines

Fix "xine-2 not found" error, we should check for the initialServiceName, not for xine-2. As reported by Andr?\195?\169 W?\195?\182bbeking.

------------------------------------------------------------------------
r889247 | dfaure | 2008-11-26 13:17:47 +0000 (Wed, 26 Nov 2008) | 2 lines

Backport fix for patterns with two stars like *.ts.0*

------------------------------------------------------------------------
r889470 | sengels | 2008-11-26 21:58:54 +0000 (Wed, 26 Nov 2008) | 2 lines

backport r889454
default to Qt::NoBrush as a frame can be filled.
------------------------------------------------------------------------
r889593 | dfaure | 2008-11-27 09:33:45 +0000 (Thu, 27 Nov 2008) | 3 lines

Fix item not disappearing when renaming it to ".foo" (hidden file)
BUG: 164974

------------------------------------------------------------------------
r889717 | dfaure | 2008-11-27 12:45:55 +0000 (Thu, 27 Nov 2008) | 2 lines

Backport: Fix infinite recursion and crash when the slot connected to newItems calls openUrl, like ktorrent's scanfolder plugin does. BUG #174920

------------------------------------------------------------------------
r889786 | dfaure | 2008-11-27 16:17:41 +0000 (Thu, 27 Nov 2008) | 4 lines

Backport 889749+889785: fix for crash 135395 (closed long ago by mistake),
by hiding the lineedit when M_NORENAME is set. Testcase: resuming aborted download.
CCBUG: 135395

------------------------------------------------------------------------
r890033 | dfaure | 2008-11-28 11:21:14 +0000 (Fri, 28 Nov 2008) | 2 lines

Backport user-feedback improvement (busy cursor during e.g. "toggle hidden files")

------------------------------------------------------------------------
r890048 | dfaure | 2008-11-28 12:23:30 +0000 (Fri, 28 Nov 2008) | 2 lines

Backport r867820 by segato: change KDirListerCache::DirectoryData to KDirListerCacheDirectoryData to fix friend class error on older version of gcc

------------------------------------------------------------------------
r890050 | dfaure | 2008-11-28 12:27:59 +0000 (Fri, 28 Nov 2008) | 2 lines

Backport 871340 & 872225 (updates to remote mimetypes), mostly so that further fixes can be backported too.

------------------------------------------------------------------------
r890051 | dfaure | 2008-11-28 12:31:03 +0000 (Fri, 28 Nov 2008) | 2 lines

backport r866765 | habacker | msvc compile fix

------------------------------------------------------------------------
r890057 | dfaure | 2008-11-28 12:40:31 +0000 (Fri, 28 Nov 2008) | 3 lines

Backport fixes for toggling hidden files (#174788), (and const iterators) from trunk.
Now trunk and branch have the same code, except for the expandToUrl redesign/fix.

------------------------------------------------------------------------
r890178 | dfaure | 2008-11-28 14:16:08 +0000 (Fri, 28 Nov 2008) | 2 lines

add kWarning to detect the problem of "deleted item not found in model" more easily.

------------------------------------------------------------------------
r890181 | dfaure | 2008-11-28 14:19:34 +0000 (Fri, 28 Nov 2008) | 2 lines

konsole's copy/paste is broken, pastes many trailing spaces

------------------------------------------------------------------------
r890187 | dfaure | 2008-11-28 14:24:53 +0000 (Fri, 28 Nov 2008) | 2 lines

Backport 890184: url-mimedata fix for #175910

------------------------------------------------------------------------
r890189 | dfaure | 2008-11-28 14:38:07 +0000 (Fri, 28 Nov 2008) | 2 lines

backport: even kde3support code uses the kde4 kioslaves.

------------------------------------------------------------------------
r890299 | orlovich | 2008-11-28 20:24:57 +0000 (Fri, 28 Nov 2008) | 4 lines

Merged revision 890298:
Ergh, I suck. I broke textarea unsubmitted notification when fixing <input> one.

BUG:176359
------------------------------------------------------------------------
r890315 | neundorf | 2008-11-28 21:59:38 +0000 (Fri, 28 Nov 2008) | 5 lines

-change order of search directories, patch by Tejas Dinkar <tejas AT gja DOT in>

Alex


------------------------------------------------------------------------
r890350 | dfaure | 2008-11-28 23:58:36 +0000 (Fri, 28 Nov 2008) | 2 lines

repair source compat for kdebindings

------------------------------------------------------------------------
r890379 | dfaure | 2008-11-29 01:49:58 +0000 (Sat, 29 Nov 2008) | 4 lines

Backport r890378:
Don't reload the current directory when selecting a file in the file dialog
 (and especially not when that file is auto-selected because the previously current file got deleted externally - #173454 crash)

------------------------------------------------------------------------
r890380 | dfaure | 2008-11-29 01:52:01 +0000 (Sat, 29 Nov 2008) | 2 lines

this line wasn't unused in the 4.x branch, re-add it

------------------------------------------------------------------------
r890652 | orlovich | 2008-11-29 18:53:13 +0000 (Sat, 29 Nov 2008) | 5 lines

Merged revision 890651:
When doing a fast-add of an option in a single-select, make sure we still 
mark the first option as selected. Fixes NVidia driver searcher.

BUG:176253
------------------------------------------------------------------------
r891650 | dfaure | 2008-12-02 14:35:59 +0000 (Tue, 02 Dec 2008) | 3 lines

Fix crash when rowCount is called on a file, like when typing '*' in a treeview.
BUG: 176555

------------------------------------------------------------------------
r891821 | whiting | 2008-12-02 22:50:02 +0000 (Tue, 02 Dec 2008) | 1 line

backport ghost entries bugfix to 4.1 branch
------------------------------------------------------------------------
r892738 | scripty | 2008-12-05 07:37:18 +0000 (Fri, 05 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r893178 | yurchor | 2008-12-06 08:23:52 +0000 (Sat, 06 Dec 2008) | 1 line

Backport from trunk (removes header warnings)
------------------------------------------------------------------------
r893660 | scripty | 2008-12-07 07:50:46 +0000 (Sun, 07 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r894005 | woebbe | 2008-12-07 17:07:22 +0000 (Sun, 07 Dec 2008) | 3 lines

initialize richTextEnabled

not sure whether true is the correct default but better than random
------------------------------------------------------------------------
r894202 | scripty | 2008-12-08 07:39:23 +0000 (Mon, 08 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r894303 | ossi | 2008-12-08 12:41:49 +0000 (Mon, 08 Dec 2008) | 2 lines

backport: make pty master non-blocking to avoid deadlocks

------------------------------------------------------------------------
r894493 | aacid | 2008-12-08 19:08:25 +0000 (Mon, 08 Dec 2008) | 4 lines

backport r894491
Save the value before priv->m_slider->setRange otherwise this with the connect changes the value of the spinbox and we lose it
Fixes bug 177199

------------------------------------------------------------------------
r894522 | woebbe | 2008-12-08 20:22:03 +0000 (Mon, 08 Dec 2008) | 1 line

use false as default for richTextEnabled as in trunk
------------------------------------------------------------------------
r895086 | sebsauer | 2008-12-09 23:49:59 +0000 (Tue, 09 Dec 2008) | 3 lines

backport r895080
Fix crash/assert if the name was empty.

------------------------------------------------------------------------
r895175 | scripty | 2008-12-10 08:19:14 +0000 (Wed, 10 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r895704 | lueck | 2008-12-11 13:50:12 +0000 (Thu, 11 Dec 2008) | 3 lines

backport of  entity move for partitionmanager docs in extragear sysadmin
translators, please update your lang/user.entities in 4.1 if you want to use &partman; in doc translation
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r895710 | yurchor | 2008-12-11 14:00:30 +0000 (Thu, 11 Dec 2008) | 1 line

Backport
------------------------------------------------------------------------
r895748 | ogoffart | 2008-12-11 16:33:16 +0000 (Thu, 11 Dec 2008) | 4 lines

Backport 895747  (Crashes in Qt application using phonon)



------------------------------------------------------------------------
r895893 | ereslibre | 2008-12-11 22:35:38 +0000 (Thu, 11 Dec 2008) | 6 lines

Sorry ! forgot to backport this one to 4.1. This corresponds to commit 889449 in trunk. Backport fix of a fix =)

CCMAIL: woebbeking@kde.org
CCMAIL: wstephenson@kde.org
CCMAIL: mcguire@kde.org

------------------------------------------------------------------------
r895896 | woebbe | 2008-12-11 22:40:30 +0000 (Thu, 11 Dec 2008) | 3 lines

make the completion actions always checkable and DISABLE the disabled ones otherwise
the user can still activate them and it looks really strange that some actions in one
group are checkable/exclusive and some are not (e.g. address edits in KMail's composer).
------------------------------------------------------------------------
r896002 | porten | 2008-12-12 08:44:02 +0000 (Fri, 12 Dec 2008) | 7 lines

Merged revision 896001:
Ony way to fix TAB getting stuck on option elements. Another
would be to reimplement handleEvent() and catch things there.

Fixes
BUG: 156164
and 7 duplicates.
------------------------------------------------------------------------
r897682 | ehamberg | 2008-12-16 15:40:42 +0000 (Tue, 16 Dec 2008) | 5 lines

Fix crash when leaving block select mode

BUG: 177905


------------------------------------------------------------------------
r897692 | ehamberg | 2008-12-16 16:32:48 +0000 (Tue, 16 Dec 2008) | 3 lines

don't move the cursor if cursor wrapping is off


------------------------------------------------------------------------
r897715 | dhaumann | 2008-12-16 17:41:26 +0000 (Tue, 16 Dec 2008) | 7 lines

backport SVN commit 897704 by dhaumann:

fixes
 - when [ ] Wrap Cursor is off, make it possible to move the cursor to the right again
 - when switching from no-wrap-cursor to wrap-cursor, ensure the cursor column is valid
   (reuse this function for block selection mode)

------------------------------------------------------------------------
r897853 | ehamberg | 2008-12-16 23:56:12 +0000 (Tue, 16 Dec 2008) | 3 lines

ensureCursorColumnValid should check if the cursor is *in*valid...


------------------------------------------------------------------------
r898678 | dfaure | 2008-12-18 17:33:45 +0000 (Thu, 18 Dec 2008) | 2 lines

Fix buttons not having the right label (yes/no/cancel instead of store/never for this site/do not store)

------------------------------------------------------------------------
r899215 | ilic | 2008-12-20 10:32:44 +0000 (Sat, 20 Dec 2008) | 1 line

Preserve system value of LANGUAGE for the outside world. (bport: 899214)
------------------------------------------------------------------------
r899545 | scripty | 2008-12-21 07:52:12 +0000 (Sun, 21 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r901028 | uwolfer | 2008-12-24 12:39:36 +0000 (Wed, 24 Dec 2008) | 5 lines

Backport:
SVN commit 901025 by uwolfer:

Fix close button behavior for progress dialogs with busy indicators.
CCBUG:178648
------------------------------------------------------------------------
r901165 | winterz | 2008-12-24 14:34:19 +0000 (Wed, 24 Dec 2008) | 10 lines

backport SVN commit 901154 by winterz:

In simpleBackupFile(), remove the backup file before copying the source file.
This makes the copy work, just in case a backup file already exists.

The remove will fail if a backup file doesn't exist (on the first call),
but that's ok.

Patch from Robby Stephenson.

------------------------------------------------------------------------
r901179 | berendsen | 2008-12-24 15:33:42 +0000 (Wed, 24 Dec 2008) | 1 line

update lilypond syntax to 2.12: bookpart, and 2 context types are deprecated
------------------------------------------------------------------------
r901205 | dfaure | 2008-12-24 16:51:16 +0000 (Wed, 24 Dec 2008) | 2 lines

Backport a bunch of commits in order to fix bugs 164584 and 178560

------------------------------------------------------------------------
r901693 | dfaure | 2008-12-26 12:55:56 +0000 (Fri, 26 Dec 2008) | 3 lines

Backport fix for user-created desktop files not being useable on some distros (r901685)  --> the fix will be in KDE-4.1.4.
CCBUG: 178561

------------------------------------------------------------------------
r901922 | grossard | 2008-12-26 22:25:46 +0000 (Fri, 26 Dec 2008) | 2 lines

added a translator entity

------------------------------------------------------------------------
r902978 | grossard | 2008-12-29 15:50:29 +0000 (Mon, 29 Dec 2008) | 2 lines

added a translator entity

------------------------------------------------------------------------
r904385 | scripty | 2009-01-02 07:47:54 +0000 (Fri, 02 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r904442 | woebbe | 2009-01-02 11:11:55 +0000 (Fri, 02 Jan 2009) | 1 line

createStandardContextMenu(): only add the "Default" completion entry if it's not disabled.
------------------------------------------------------------------------
r904803 | scripty | 2009-01-03 08:15:37 +0000 (Sat, 03 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r906020 | dfaure | 2009-01-05 12:53:42 +0000 (Mon, 05 Jan 2009) | 2 lines

Backport r906013, fix KArchiveDirectory::entry("./file").

------------------------------------------------------------------------
r906060 | dfaure | 2009-01-05 13:18:01 +0000 (Mon, 05 Jan 2009) | 3 lines

Backport SVN commit 906059:
Fix short/ushort error which broke prettyUrl for e.g. chinese unicode characters.

------------------------------------------------------------------------
r906293 | dfaure | 2009-01-05 21:39:03 +0000 (Mon, 05 Jan 2009) | 3 lines

Fix wrong if() so that cervisia part can be loaded in konqueror.
CCBUG: 178603

------------------------------------------------------------------------
r906463 | mueller | 2009-01-06 11:42:50 +0000 (Tue, 06 Jan 2009) | 2 lines

KDE 4.1.4 preparations

------------------------------------------------------------------------
r906602 | dfaure | 2009-01-06 12:56:45 +0000 (Tue, 06 Jan 2009) | 6 lines

"Fix" for enable-final by disabling it here.
nepomuk/core/ontology/entitymanager.h:27: error: redefinition of ‘uint qHash(const QUrl&)’
nepomuk/core/ontology/entity.cpp:35: error: ‘uint qHash(const QUrl&)’ previously defined here
and:
error: no matching function for call to ‘qHash(const Nepomuk::Tag&)’

------------------------------------------------------------------------
r906618 | dfaure | 2009-01-06 13:15:51 +0000 (Tue, 06 Jan 2009) | 2 lines

Forgot to commit: the backport that makes extragear/base/konq-plugins/webarchiver compile with kde-4.1-branch.

------------------------------------------------------------------------
