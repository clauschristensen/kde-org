2005-11-26 22:24 +0000 [r483540]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/localcontactlisttask.cpp:
	  Fix bug 110438. Split the packet into multiple packets when the
	  list of contacts to add is too long. Patch written by my wife. :)

2005-11-27 15:35 +0000 [r483659]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnchatsession.cpp:
	  Fix Bug 117089: Kopete, MSN: error when I'm invisible and someone
	  changes state BUG: 117089

2005-11-28 11:03 +0000 [r483834]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msncontact.cpp:
	  Complete the fix of the bug 117089. CCBUG: 117089

2005-11-29 19:45 +0000 [r484195]  granberry

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircprotocol.cpp:
	  Make down button work in irc server configuration dialog

2005-12-01 16:55 +0000 [r484724]  granberry

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/libkirc/kircmessage.cpp:
	  Remove extra spaces from messages with empty message lists to
	  keep from upsetting broken servers. BUG:117368

2005-12-02 15:47 +0000 [r485011]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/kyahoo.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/kyahoo.cpp:
	  Fix Webcam disconnects/crashes. BUG:113439

2005-12-05 09:45 +0000 [r485649]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/ui/contactaddednotifydialog.cpp:
	  Fix spelling mistake in string. Sorry i18ners.

2005-12-08 09:43 +0000 [r486650]  cartman

	* branches/KDE/3.5/kdenetwork/wifi/interface_wireless_wirelessextensions.cpp:
	  Do a length check before memcpy, I got event->u.essid.length
	  26684 here!

2005-12-11 18:36 +0000 [r487731]  qbast

	* branches/KDE/3.5/kdenetwork/kdnssd/ioslave/_http._tcp,
	  branches/KDE/3.5/kdenetwork/kdnssd/ioslave/_webdav._tcp: These
	  are standard too.

2005-12-11 19:22 +0000 [r487744]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnaccount.cpp:
	  Use a stack instead of a queue to open chatwindow. and clear
	  regulary this stack. That mean the priority is given to the last
	  contact you ask to chat to. This fix Bug 118128: Wrong MSN
	  contact when a window opens BUG: 118128 Something which trigger
	  this bug is also the fact that it need to download lots of
	  picture when kopete start. disabling the automatic download of
	  picture may help. (this bug, with the combinaison of Bug 117089
	  may be really annoying.)

2005-12-13 10:27 +0000 [r488131]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetecontact.cpp:
	  Don't connect signal/slot to null account

2005-12-13 12:07 +0000 [r488148]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/identity/kopeteidentityconfig.cpp:
	  Fix signal: kcmodule signal is changed(bool)

2005-12-13 12:38 +0000 [r488156]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/tooltipeditdialog.cpp:
	  Use double click to add/remove

2005-12-13 12:55 +0000 [r488160]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircprotocol.cpp:
	  Use double-click to rename

2005-12-13 13:46 +0000 [r488173]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig.h:
	  Fix update button when we check/uncheck use economicons (now
	  knewstuff works fine, is it possible to activate by default "get
	  new theme" ? )

2005-12-13 14:09 +0000 [r488181]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/plugins/highlight/highlightpreferences.cpp:
	  Use double click to rename filter

2005-12-14 02:32 +0000 [r488340]  granberry

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetecommandhandler.cpp:
	  Properly anchor regexp to handle non-commands at the start of a
	  line BUG:118151

2005-12-14 06:15 +0000 [r488364]  granberry

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/private/kopeteemoticons.cpp:
	  Treat (internal) <br /> as whitespace when doing strict search
	  for emoticons. BUG:117928

2005-12-14 16:19 +0000 [r488465]  granberry

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/chatview.cpp:
	  Display correct number of people in chat BUG:109935

2005-12-14 23:47 +0000 [r488582]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/kopete.kdevelop,
	  branches/work/kopete/dev-0.12/kopete/kopete/chatwindow/chatmessagepart.h,
	  branches/work/kopete/dev-0.12/kopete/kopete/chatwindow/chatmessagepart.cpp:
	  Bring back some code from XSLT engine (for comparing purpose)
	  Improvement in the XHTML+CSS engine. -Respect RichText in
	  messages. -Proprely escape nickname -Map Action messages to
	  Status template.

2005-12-15 13:59 +0000 [r488695]  coolo

	* branches/KDE/3.5/kdenetwork/kpf/src/PropertiesDialogPlugin.cpp:
	  interesting. If you fix this typo, you get a "Sharing" and a
	  "Share" tab in konqueror properties. And now?

2005-12-16 02:17 +0000 [r488833]  bhards

	* branches/KDE/3.5/kdenetwork/krdc/rdp/rdphostpref.cpp: Pick up the
	  correct setting for keyboard layouts. Thanks to Jean-Max Reymond
	  for reporting this problem. BUG: 118241

2005-12-18 02:07 +0000 [r489328]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/plugins/nowlistening/nowlisteningplugin.cpp,
	  branches/work/kopete/dev-0.12/kopete/kopete/kopeteeditglobalidentitywidget.cpp,
	  branches/work/kopete/dev-0.12/kopete/libkopete/kopeteawayaction.cpp,
	  branches/work/kopete/dev-0.12/kopete/libkopete/ui/kopetelistviewitem.cpp,
	  branches/work/kopete/dev-0.12/kopete/libkopete/kopeteawayaction.h,
	  branches/work/kopete/dev-0.12/kopete/protocols/jabber/jabberaccount.cpp:
	  Fix memory leaks to make valgrind happy :) Can someone backport
	  this commit to /branches/KDE/3.5/kdenetwork/kopete ? Thank you.

2005-12-18 15:08 +0000 [r489393]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/kopete/kopeteeditglobalidentitywidget.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteawayaction.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/ui/kopetelistviewitem.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteawayaction.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabberaccount.cpp:
	  Backport of SVN commit 489328 to KDE 3.5.1: Fix memory leaks to
	  make valgrind happy :)

2005-12-18 16:36 +0000 [r489423]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp:
	  Backport: don't send picture information when connecting into
	  invisible state.

2005-12-19 11:51 +0000 [r489653-489652]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/chatview.cpp:
	  fix accept()

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig.cpp:
	  another misplaced ';'

2005-12-19 20:48 +0000 [r489819]  adridg

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/emoticonselector.cpp:
	  Using C99-style boolean operator keywords is a no-no for gcc 2.95
	  compatibility. CCMAIL: kde@freebsd.org

2005-12-20 21:04 +0000 [r490117]  adridg

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/libiris/iris/jabber/s5b.cpp:
	  Need sys/types for in.h on FreeBSD 4.X. Thanks for the patch to
	  the most lanolinine ovidious lofi@freebsd for doing awful drudge
	  work like compiling KDE on legacy systems in the name of
	  providing long-life support for a release. CCMAIL:
	  kde@freebsd.org

2005-12-21 06:48 +0000 [r490225]  bhards

	* branches/KDE/3.5/kdenetwork/krdc/rdp/rdphostpref.cpp: Ensure that
	  all the entries in the krdcrc file get cleaned up when the host
	  preferences are removed. Also correct a (harmless) compile time
	  error about initialisation order.

2005-12-22 19:52 +0000 [r490685]  ratz

	* branches/KDE/3.5/kdenetwork/kget/main.cpp: Accept multiple urls
	  from commandline. When there are two arguments and the 2nd is a
	  local file, interpret as destination. BUG: 117970

2005-12-23 23:08 +0000 [r490969]  goossens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/ui/kopetefileconfirmdialog.cpp:
	  the file size is a whole number

2005-12-24 13:27 +0000 [r491068]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/ui/jabberregisteraccount.cpp:
	  Bug 114631: Strange value in password field when registering a
	  jabber account BUG: 114631

2005-12-24 15:06 +0000 [r491090]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/ui/jabberregisteraccount.cpp:
	  fix comment

2005-12-24 23:41 +0000 [r491170]  goossens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/dispatcher.cpp:
	  Do not send the file name with a byte order marker, MSN Messenger
	  does not like that. This fixes the issue with strange characters
	  in the file name. CCBUG:113525 This does not fix the "empty file"
	  bug...

2005-12-24 23:50 +0000 [r491172]  goossens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/ui/kopetefileconfirmdialog.cpp:
	  We want to react to the clicked() signal, not pressed(). Prevents
	  the "browse" button from getting stuck.

2005-12-27 08:39 +0000 [r491700-491699]  hschaefer

	* branches/KDE/3.5/kdenetwork/kopete/configure.in.in: Use
	  AS_HELP_STRING to beautify the --enable-smpppd help string in
	  configure

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefsimpl.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationwidget.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdsearcher.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsiface.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kopete_smpppdcs.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefs.ui,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationui.ui
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationwidget.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdsearcher.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/onlineinquiry.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Changelog.smpppdcs,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefsimpl.cpp:
	  The 'netstat' option gets disabled if there is no netstat binary

2005-12-28 05:12 +0000 [r491949]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/messagereceivertask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/offlinemessagestask.cpp:
	  Commit patch by Oleg Girko that fixes the encodings for type-2
	  UTF8 messages as well as offline messages. Thanks for the patch!
	  Sorry for taking so long with it. CCMAIL: Oleg Girko
	  <ol@infoserver.ru>

2005-12-30 00:57 +0000 [r492466]  mhunter

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/outgoingtransfer.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnchatsession.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteutils.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig_emoticons.ui,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/kyahoo.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/ui/yahoowebcamdialog.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahoocontact.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/eventsrc,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/behavior/behaviorconfig_events.ui,
	  branches/KDE/3.5/kdenetwork/wifi/kcmwifi/configcrypto.ui,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/history/historydialog.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/icqprotocol.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/ui/msneditaccountui.ui,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/behavior/behaviorconfig_general.ui,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetechatsession.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/history/historyprefsui.ui,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/ui/icqsearchbase.ui:
	  Typographical corrections and changes CCMAIL:kde-i18n-doc@kde.org

2005-12-30 01:16 +0000 [r492474]  mhunter

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnchatsession.cpp:
	  Typographical corrections and changes

2005-12-31 01:34 +0000 [r492766]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/identity/kopeteidentityconfig.cpp:
	  Backport of: BUG: Fix crash(and weird) bug in Configure Global
	  Identity.

2006-01-01 17:43 +0000 [r493141]  hschaefer

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/onlineinquiry.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationwidget.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/iconnector.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detector.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/onlineinquiry.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationwidget.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detector.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefsimpl.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcspreferences.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdsearcher.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefsimpl.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcspreferences.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsiface.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kopete_smpppdcs.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdsearcher.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Changelog.smpppdcs:
	  * use of KSocketStream instead of deprecated KExtendedSocket *
	  progressbar while searching for an smpppd on the local network *
	  automatically found smpppd server is resolved via DNS

2006-01-01 19:32 +0000 [r493184]  hschaefer

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdsearcher.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detector.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Changelog.smpppdcs,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detector.h:
	  FIX for Bug 111369

2006-01-02 14:22 +0000 [r493451]  scripty

	* branches/KDE/3.5/kdenetwork/kppp/icons/hi64-app-kppp.png,
	  branches/KDE/3.5/kdenetwork/kppp/icons/hi128-app-kppp.png,
	  branches/KDE/3.5/kdenetwork/kget/icons/cr22-action-tool_queue.png,
	  branches/KDE/3.5/kdenetwork/kget/icons/cr22-action-tool_restart.png,
	  branches/KDE/3.5/kdenetwork/kopete/styles/data/Kopete/action.png,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/icons/cr16-action-yahoo_busy.png,
	  branches/KDE/3.5/kdenetwork/kget/kget_plug_in/cr22-action-khtml_kget.png,
	  branches/KDE/3.5/kdenetwork/kget/icons/cr22-action-tool_drop_target.png,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/icons/cr16-action-yahoo_mobile.png,
	  branches/KDE/3.5/kdenetwork/kget/icons/cr22-action-kget_dock.png,
	  branches/KDE/3.5/kdenetwork/kget/icons/cr22-action-tool_uselastdir.png:
	  Remove svn:executable from some typical non-executable files
	  (goutte)

2006-01-03 01:36 +0000 [r493721]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscaraccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscarlistnonservercontacts.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscarlistcontactsbase.ui,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscarlistnonservercontacts.h:
	  Fix bug 114899 by adding a 'Do not ask again' checkbox to the
	  dialog that askes whether or not to add the contacts that have
	  somehow been removed from the server side contact list. BUG:
	  114899 This should be in KDE 3.5.1

2006-01-03 02:18 +0000 [r493730]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscarcontact.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/icqcontact.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/userdetails.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/userdetails.cpp:
	  commit the part of the encoding fix patch that deals with merging
	  user details

2006-01-03 02:22 +0000 [r493731]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/coreprotocol.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/coreprotocol.h:
	  fix crash. Emitting incomingData() might get blocked for example
	  by a dialog. If another packet arrives while blocked, m_din is
	  overwritten and kopete segfaults, because m_din is deleted twice
	  after returning from imcomingData() Caughted and patched by Andre
	  Duffeck. Thanks!

2006-01-03 15:51 +0000 [r493900]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/configure.in.in: the usual
	  "daily unbreak compilation"

2006-01-03 16:39 +0000 [r493924]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabbercontact.cpp:
	  Backport of this commit: Applying patch by Brian Smith: Offline
	  message from Jabber Last Activty wasn't applied.

2006-01-06 10:02 +0000 [r494762]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemetacontact.cpp:
	  backport fix for empty displayname problem

2006-01-06 14:25 +0000 [r494891]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemetacontact.cpp:
	  backport the fix for my previous backport

2006-01-06 14:35 +0000 [r494895]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetecontactlist.cpp:
	  Backport from dev-0.12: Don't save and load myself metacontact
	  information when Global Identity is not enabled.

2006-01-06 18:11 +0000 [r494987]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/icqcontact.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/icqcontact.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/messagereceivertask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.h:
	  Fix encoding for away messages.

2006-01-08 11:01 +0000 [r495460]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp:
	  Fix malformed clientscaps

2006-01-08 19:56 +0000 [r495745]  binner

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefs.ui:
	  compile

2006-01-09 07:57 +0000 [r495865]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp:
	  Backport: Strip FADE and ALT tags.

2006-01-10 17:00 +0000 [r496508]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/networkscanning.cpp: fixes an
	  i18n issue, a string was passed to the command-line localized
	  when it shouldn't be. does not introduce a new string,
	  i18n("Managed") is already somewhere else in the code
	  CCBUG:119574

2006-01-10 22:53 +0000 [r496656]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/chatview.cpp:
	  Backport revert for bug 109935 CCBUG: 109935

2006-01-10 23:00 +0000 [r496661]  dfaure

	* branches/KDE/3.5/kdenetwork/filesharing/simple/fileshare.cpp:
	  Find those things in $PATH+/usr/bin CCMAIL: Tim McCormick
	  <tim@timmcc.co.uk>

2006-01-10 23:14 +0000 [r496666]  dfaure

	* branches/KDE/3.5/kdenetwork/filesharing/simple/controlcenter.ui:
	  I needed this so that it would compile for me; strange

2006-01-16 20:16 +0000 [r499014]  amantia

	* branches/KDE/3.5/kdenetwork/kppp/general.cpp,
	  branches/KDE/3.5/kdenetwork/kppp/modem.cpp: Fix initialization
	  for the Qualcomm Z010 CDMA modem. If there is anybody who feels
	  competent in this code, feel free to review (Harri Ported already
	  did.) Add 921600 as possible speed for the modem.

2006-01-17 08:52 +0000 [r499197]  scripty

	* branches/KDE/3.5/kdenetwork/krfb/krfb_httpd/krfb_httpd: Improve
	  portability by declaring using Bash with the help of /usr/bin/env
	  (Bug #95475) (goutte)

2006-01-17 12:33 +0000 [r499262]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/kopete/contactlist/kopetecontactlistview.cpp:
	  backport crashfix when deleting several contact that are in
	  several groups

2006-01-17 20:54 +0000 [r499442]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/interface_wireless_wirelessextensions.cpp,
	  branches/KDE/3.5/kdenetwork/wifi/interface_wireless_wirelessextensions.h:
	  as announced on core-devel: a last-minute fix for crashing when
	  scanning for networks under certain combinations of wireless
	  tools and kernel. This is a BIG change (API calls ->
	  command-line). Also introduces two new i18n() strings, but should
	  fix two annoying bugs. BUG:118677 BUG:115312

2006-01-18 11:14 +0000 [r499586]  hschaefer

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationwidget.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/onlineinquiry.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/iconnector.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detector.cpp
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationwidget.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/onlineinquiry.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetstat.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detector.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectordcop.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefsimpl.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdready.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/Makefile.am
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetstat.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectordcop.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdunsettled.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefsimpl.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdready.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kopete_smpppdcs.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefs.ui,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdunsettled.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdclient.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectorsmpppd.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdstate.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Changelog.smpppdcs,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdclient.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdstate.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectorsmpppd.h
	  (added): - refactoring to allow easy implementation of new
	  detection methods - speed improvements - communication with
	  smpppd now in own library

2006-01-18 13:37 +0000 [r499643]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemessageevent.cpp:
	  backport r499616 , that could be usefull

2006-01-19 19:19 +0000 [r500246]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION: version++

