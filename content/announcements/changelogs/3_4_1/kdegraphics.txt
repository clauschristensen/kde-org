2005-03-04 09:03 +0000 [r394780]  coolo

	* kpdf/xpdf/xpdf/GlobalParams.cc: compile fix for Solaris (100767)

2005-03-04 17:25 +0000 [r394902]  aacid

	* kpdf/ui/pageview.cpp, kpdf/ui/presentationwidget.cpp: Fix compile
	  on Solaris. Thanks for notyfiyng, sadly is too late for KDE
	  3.4.0, will come with KDE 3.4.1 BUGS: 100778

2005-03-05 13:26 +0000 [r395031]  aacid

	* kpdf/ui/pageview.cpp: Show contect menu when in FullScreen even
	  if no document is open. Thanks for notyfing will be in KDE 3.4.1
	  BUGS: 100881

2005-03-05 14:04 +0000 [r395037]  aacid

	* kpdf/VERSION, kpdf/shell/main.cpp: Incerase version to 0.4.1

2005-03-06 09:33 +0000 [r395195]  mbuesch

	* ksvg/configure.in.bot: update outdated fribidi link.

2005-03-11 21:50 +0000 [r396817]  lukas

	* ksnapshot/main.cpp: missing i18n()

2005-03-12 12:04 +0000 [r396938]  whuss

	* kviewshell/kmultipage.cpp: Make "read down" also work for the
	  last page.

2005-03-12 12:12 +0000 [r396943-396942]  whuss

	* kviewshell/kmultipage.h, kviewshell/kmultipage.cpp: Disable
	  "Select All" and "Find..." actions if no file is loaded, or text
	  search is not supported by the current multiPage.

	* kdvi/kdvi_multipage.cpp: Disable "Select All" and "Find..."
	  actions if no file is loaded.

2005-03-12 20:33 +0000 [r397057]  aacid

	* kpdf/xpdf/goo/gmem.h, kpdf/xpdf/goo/gmem.c: Fix the allocation
	  size of libgoo on 64bit architectures. Patch by Takashi Iwai
	  <tiwai AT suse DOT de>

2005-03-13 00:13 +0000 [r397100]  rich

	* ksnapshot/ksnapshot.cpp: BACKPORT BUG 94726 Fix autoinc

2005-03-13 08:29 +0000 [r397141]  whuss

	* kviewshell/kviewpart.cpp: Use the correct viewmode, if the
	  viewmode has been changed while no file was loaded.

2005-03-14 22:45 +0000 [r397662]  aacid

	* kpdf/ui/presentationwidget.cpp: Backport 101519

2005-03-16 14:05 +0000 [r398088]  bero

	* ksvg/impl/SVGBBoxTarget.h, ksvg/impl/SVGStopElementImpl.h,
	  ksvg/impl/libs/libtext2path/src/Converter.h,
	  ksvg/impl/libs/libtext2path/src/Affine.h,
	  ksvg/impl/SVGElementImpl.h, ksvg/core/KSVGCanvas.h,
	  ksvg/impl/SVGPaintImpl.h,
	  ksvg/impl/libs/libtext2path/src/Glyph.h,
	  ksvg/plugin/backends/libart/LibartCanvasItems.h,
	  ksvg/impl/SVGColorImpl.h, ksvg/impl/libs/libtext2path/src/Font.h:
	  Add some export macros, not 100% sufficient for
	  -fvisibility=hidden yet, though

2005-03-16 22:31 +0000 [r398218]  aacid

	* kpdf/core/page.cpp, kpdf/ui/pageview.cpp, kpdf/core/page.h:
	  Backport fix for bug 101648

2005-03-18 12:02 +0000 [r398681]  kebekus

	* kdvi/special.cpp: fixes TPIC problem found by Robert Cabane

2005-03-19 04:56 +0000 [r398897-398896]  binner

	* kfax/kfax.desktop: +X-KDE-More

	* ksnapshot/ksnapshot.desktop: -X-KDE-More

2005-03-21 20:57 +0000 [r399552]  aacid

	* kpdf/configure.in.in, kpdf/xpdf/xpdf/Makefile.am: Fix compile
	  problem due to the non-standard location of Xft2 header files.
	  Thanks Rex for reporting BUGS: 102119

2005-03-24 19:27 +0000 [r400300]  aacid

	* kpdf/xpdf/xpdf/GlobalParams.cc: Backport fix for bug 102337

2005-03-24 20:17 +0000 [r400319]  aacid

	* kpdf/part.cpp: Backport fix for bug 102332, it introduces a new
	  string but it is exactly the same as one present in kdelis.po so
	  i think that is allowed, if it is not please tell and i'll revert

2005-03-24 22:38 +0000 [r400347]  aacid

	* kpdf/ui/propertiesdialog.cpp: Fix for bug 102117 - properties
	  window doesn't fit when some entry is too long Patch by Enrico
	  Ros BUG: 102117

2005-03-25 13:42 +0000 [r400512]  aacid

	* kpdf/xpdf/xpdf/GlobalParams.cc: Add parsing of Light and
	  Condensed in font name

2005-03-27 18:58 +0000 [r401059]  aacid

	* kpdf/xpdf/xpdf/GlobalParams.cc: Complete the backport to parse
	  width, thanks Ferdinand for reporting BUG: 102591

2005-03-27 19:17 +0000 [r401063]  aacid

	* kpdf/ui/presentationwidget.cpp: Backport fix for first part of
	  102565

2005-03-30 06:42 +0000 [r401847]  kebekus

	* kdvi/fontMap.cpp: fixes issue #102251

2005-04-02 17:40 +0000 [r402675]  swinter

	* kolourpaint/tools/kptoolresizescale.cpp,
	  kolourpaint/kpdocument.cpp: fixing XML markup (KBabel complains)

2005-04-07 10:25 +0000 [r403734]  whuss

	* kviewshell/documentWidget.h, kviewshell/centeringScrollview.cpp,
	  kviewshell/centeringScrollview.h, kviewshell/documentWidget.cpp,
	  kviewshell/kmultipage.cpp: Backport part of the changes in
	  documentWidget drawing. This fixes flickering while scolling in
	  continuous view modes.

2005-04-12 20:03 +0000 [r405137]  aacid

	* kpdf/part.cpp: Fix 102869 on 3.4.x, that was already fixed on
	  HEAD Please keep in mind that kpdf 0.4.x has a memory leak when
	  using watch file that we "can't" fix (it's fixed on the
	  upcomingkpdf 0.5) BUGS: 102869

2005-04-13 15:47 +0000 [r405354]  whuss

	* kviewshell/centeringScrollview.cpp,
	  kviewshell/centeringScrollview.h, kviewshell/kmultipage.cpp:
	  Don't loose the current position in the document, if the
	  zoomlevel is changed.

2005-04-14 06:37 +0000 [r405480]  kebekus

	* kdvi/dviwin_prescan.cpp: fixes bug #103545

2005-04-14 19:05 +0000 [r405585]  aacid

	* kpdf/xpdf/xpdf/GlobalParams.cc: Backport "fix" for bug 103891

2005-04-16 10:18 +0000 [r405864]  kebekus

	* kdvi/kdvi_multipage.cpp, kviewshell/documentRenderer.cpp,
	  kviewshell/documentWidget.h, kviewshell/centeringScrollview.cpp,
	  kviewshell/centeringScrollview.h, kdvi/dviwin.cpp,
	  kviewshell/documentWidget.cpp, kviewshell/kmultipage.cpp: fixes
	  bug #102689

2005-04-16 23:17 +0000 [r405996]  aacid

	* kpdf/ui/pageview.cpp, kpdf/part.cpp, kpdf/ui/pageview.h,
	  kpdf/part.h: Fix a memory leak when reloading documents, it
	  introduces a new string but i asked on kde-i18n-doc five days ago
	  about it and got 1 Ok 0 No so i'm commiting
	  CCMAIL:kde-i18n-doc@kde.org

2005-04-22 22:27 +0000 [r407261]  aacid

	* kpdf/ui/presentationwidget.cpp, kpdf/ui/thumbnaillist.cpp:
	  Backport fix for issue 1.2 from the usability problems list

2005-04-26 20:42 +0000 [r408034]  aacid

	* kpdf/shell/kpdf.desktop: Backport fix for bug 104471

2005-04-29 11:54 +0000 [r408595]  dang

	* kolourpaint/tools/kptoolresizescale.cpp: "CVS_SILENT
	  Typographical corrections and changes" [BACKPORT 1.34:1.35
	  (Malcolm Hunter)] I'm backporting this because I think at least
	  "neighboring" should be spelt the American way in a release,
	  "neighbouring" is for en_GB - CC'ing kde-i18n-doc just in case.
	  CCMAIL: kde-i18n-doc@kde.org

2005-04-29 12:11 +0000 [r408600]  dang

	* kolourpaint/NEWS, kolourpaint/README, kolourpaint/BUGS,
	  kolourpaint/VERSION, kolourpaint/kolourpaint.cpp: up ver to
	  1.4.1_light in preparation for KDE 3.4.1; remove irrelevant info
	  about other branches from NEWS (this is not HEAD's NEWS)

2005-04-29 13:21 +0000 [r408611]  dang

	* kolourpaint/tools/kptoolresizescale.cpp: revertlast CCMAIL:
	  <kde-i18n-doc@kde.org>, <malcolm.hunter@gmx.co.uk> On Fri, 29 Apr
	  2005 10:53 pm, Malcolm Hunter wrote: > You should really ask
	  before committing. Please revert today until you get > approval
	  from the translators.

2005-04-30 14:36 +0000 [r408799]  aacid

	* kpdf/xpdf/fofi/FoFiType1.cc: backport fix for bug 104786

2005-05-06 12:00 +0000 [r410000]  binner

	* kpdf/ui/minibar.cpp: Fix disabled icons

2005-05-07 11:41 +0000 [r410277-410274]  binner

	* ksnapshot/ksnapshot.cpp, ksnapshot/ksnapshot.h: Rescale
	  screenshot preview when resizing window Patch by Isaac Clerencia
	  <isaac@sindominio.net> BUG: 104895

	* ksnapshot/ksnapshotwidget.ui: Improved layout for
	  larger/maximized window

2005-05-10 10:32 +0000 [r411876]  binner

	* kdegraphics.lsm: update lsm for release

2005-05-11 05:20 +0000 [r412248]  kebekus

	* kviewshell/kviewpart.cpp: GUI: fixes crash when unsupported files
	  are loaded

2005-05-12 10:12 +0000 [r412667]  aacid

	* kpdf/ui/pageview.cpp: Fix bug introduced when fixing 101648 BUGS:
	  105454

2005-05-17 20:34 +0000 [r415192]  aacid

	* kpdf/core/generator_pdf/gp_outputdev.cpp: Backport fix for bug
	  105630

2005-05-22 07:51 +0000 [r416650]  binner

	* ksnapshot/Makefile.am: New version, much better in KDE 3.4.1 ;-)

2005-05-22 12:26 +0000 [r416740]  coolo

	* kpdf/xpdf/xpdf/TextOutputDev.h,
	  kpovmodeler/pmrendermodesdialog.h, kpovmodeler/pmcommand.h: fix
	  compilation with gcc 4.0.1

