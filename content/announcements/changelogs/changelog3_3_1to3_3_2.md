---
title: "KDE 3.3.1 to KDE 3.3.2 Changelog"
hidden: true
---

<p>
This page tries to present as much as possible the additions
and corrections that occured in KDE between the 3.3.1 and 3.3.2 releases.
Nevertheless it should be considered as incomplete.
</p>

<h3><a name="arts">arts</a></h3><ul>
</ul>

<h3><a name="kdelibs">kdelibs</a> <font size="-2">[<a href="3_3_2/kdelibs.txt">all CVS changes</a>]</font></h3><ul>
      <li>kdecore: recognize known names for the MacRoman charset (useful for KHTML)</li>
      <li>khtml: only disallow fixed positioning on body, not relative/absolute (<a href="http://bugs.kde.org/77048">#77048</a>, <a href="http://bugs.kde.org/76982">#76982</a>)</li>
      <li>khtml: added Javascript-support for HTMLDocument.compatMode</li>
      <li>khtml: fixed textarea manipulation by DOM</li>
      <li>khtml: paged media support (page-break-before and -after) (<a href="http://bugs.kde.org/68930">#68930</a>)</li>
      <li>khtml: correctly handle text-transform on first-letter (<a href="http://bugs.kde.org/76078">#76078</a>)</li>
      <li>khtml: fixed display: block on pseudo-elements (<a href="http://bugs.kde.org/90917">#90917</a>)</li>
      <li>khtml: return Undefined instead of Null for out-of-range indices</li>
      <li>khtml: fixed stopPropagation when called from the target node itself (<a href="http://bugs.kde.org/90750">#90750</a>)</li>
      <li>khtml: fixed htmlCompat mode for documents served as text/html (<a href="http://bugs.kde.org/86446">#86446</a>)</li>
      <li>khtml: fixed display: compact (<a href="http://bugs.kde.org/75806">#75806</a>)</li>
      <li>khtml: support z-order for text areas and list boxes</li>
      <li>khtml: don't set assume &lt;select&gt; got items when we calculate a height for items (<a href="http://bugs.kde.org/87466">#87466</a>)</li>
      <li>khtml: changed default horizontal margins for H1-H6 from auto to 0 (<a href="http://bugs.kde.org/91327">#91327</a>)</li>
      <li>khtml: escape otherwise unencodable characters in form submits (<a href="http://bugs.kde.org/82018">#82018</a>)</li>
      <li>khtml: merged min,max-height fixes (<a href="http://bugs.kde.org/59502">#59502</a>, <a href="http://bugs.kde.org/59503">#59503</a>)</li>
      <li>khtml: merged handling of event handlers (<a href="http://bugs.kde.org/91408">#91408</a>)</li>
      <li>khtml: implemented CSS 2.1 compliant parsing of background-position (<a href="http://bugs.kde.org/91572">#91572</a>)</li>
      <li>khtml: fixed m_value vs ATTR_VALUE problem which didn't allow sending mail on gmail</li>
      <li>khtml: make use of the suggested filename for images (<a href="http://bugs.kde.org/57590">#57590</a>)</li>
      <li>khtml: tons of bidi fixes (<a href="http://bugs.kde.org/54916">#54916</a>, <a href="http://bugs.kde.org/59476">#59476</a>, <a href="http://bugs.kde.org/62904">#62904</a>, <a href="http://bugs.kde.org/64506">#64506</a>, <a href="http://bugs.kde.org/69806">#69806</a>, <a href="http://bugs.kde.org/74800">#74800</a>, <a href="http://bugs.kde.org/79365">#79365</a>, <a href="http://bugs.kde.org/86157">#86157</a>, <a href="http://bugs.kde.org/91955">#91955</a>)</li>
      <li>khtml: enable strict CSS parsing also for transitional doctypes (<a href="http://bugs.kde.org/83129">#83129</a>, <a href="http://bugs.kde.org/79970">#79970</a>)</li>
      <li>khtml: ignore height element for input elements that are not image (<a href="http://bugs.kde.org/79269">#79269</a>)</li>
      <li>khtml: form widget fixes for plastik (and others)</li>
      <li>khtml: make sure we are layouted before scrolling if we are still loading (<a href="http://bugs.kde.org/51473">#51473</a>)</li>
      <li>khtml: add compensated font scale from Todd Fahrner's "Toward a standard font size interval system" (<a href="http://bugs.kde.org/91228">#91228</a>)</li>
      <li>khtml: avoid triggering full repaints of the view before the first layout is done</li>
      <li>khtml: implement CSS3 property box-sizing to match MacIE, Opera and Mozilla</li>
      <li>khtml: fix globeandmail.com famlily of crashes (<a href="http://bugs.kde.org/65715">#65715</a>)</li>
      <li>khtml: floats and flowing around floats improvements (<a href="http://bugs.kde.org/92979">#92979</a>, <a href="http://bugs.kde.org/93511">#93511</a>, <a href="http://bugs.kde.org/89527">#89527</a>)</li>
      <li>khtml: fix textarea scrolling positions (<a href="http://bugs.kde.org/93193">#93193</a>)</li>
      <li>khtml: tons of fixes for tables (<a href="http://bugs.kde.org/70418">#70418</a>, <a href="http://bugs.kde.org/22657">#22657</a>)</li>
      <li>khtml: remove hidden widgets from the painting (<a href="http://bugs.kde.org/73984">#73984</a>)</li>
      <li>khtml: avoid crashes on XML documents (<a href="http://bugs.kde.org/87495">#87495</a>)</li>
      <li>khtml: ignore relative positioning on table sections (<a href="http://bugs.kde.org/90437">#90437</a>)</li>
      <li>khtml: fix static position calculation for positioned elements (<a href="http://bugs.kde.org/78449">#78449</a>, <a href="http://bugs.kde.org/83748">#83748</a>, <a href="http://bugs.kde.org/88193">#88193</a>, <a href="http://bugs.kde.org/65940">#65940</a>, <a href="http://bugs.kde.org/68303">#68303</a>, <a href="http://bugs.kde.org/73000">#73000</a>)</li>
      <li>khtml: support insertAdjacentHTML (<a href="http://bugs.kde.org/33968">#33968</a>)</li>
      <li>khtml: fix crash in sites manipulating frames (<a href="http://bugs.kde.org/67624">#67624</a>)</li>
      <li>khtml: async kwallet support (<a href="http://bugs.kde.org/70789">#70789</a>, <a href="http://bugs.kde.org/90915">#90915</a>, <a href="http://bugs.kde.org/75549">#75549</a>)</li>
      <li>khtml: fix selectorText() when no namespace is specified (<a href="http://bugs.kde.org/79645">#79645</a>)</li>
      <li>khtml: update the layout before fetching the computed values (<a href="http://bugs.kde.org/86515">#86515</a>)</li>
      <li>khtml: color 'http://foo' links as visited if 'http://foo/' is in history (<a href="http://bugs.kde.org/75771">#75771</a>)</li>
      <li>khtml: corrected ref-counting error leading to segfault/invalid memory usage (<a href="http://bugs.kde.org/87892">#87892</a>)</li>
      <li>khtml: fixed some memory corruptions with garbage HTML</li>
      <li>khtml: account for scrollbar height when sizing variable overflow:scroll/auto boxes (<a href="http://bugs.kde.org/89136">#89136</a>)</li>
      <li>khtml: restrict access to java classes from javascript</li>
      <li>kio: fix a crash that can appear in applications using treeviews to display a folder (<a href="http://bugs.kde.org/88763">#88763</a>)</li>
</ul>

<h3><a name="kdeaccessibility">kdeaccessibility</a> <font size="-2">[<a href="3_3_2/kdeaccessibility.txt">all CVS changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeaddons">kdeaddons</a> <font size="-2">[<a href="3_3_2/kdeaddons.txt">all CVS changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeadmin">kdeadmin</a> <font size="-2">[<a href="3_3_2/kdeadmin.txt">all CVS changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeartwork">kdeartwork</a> <font size="-2">[<a href="3_3_2/kdeartwork.txt">all CVS changes</a>]</font></h3><ul>
<li>plastik: improved performance when hovering over tabs (<a href="http://bugs.kde.org/show_bug.cgi?id=89712">bug #89712</a>)</li>
</ul>

<h3><a name="kdebase">kdebase</a> <font size="-2">[<a href="3_3_2/kdebase.txt">all CVS changes</a>]</font></h3><ul>
      <li>kcontrol: fix setting mouse buttons to left-handed for mice with many buttons(<a href="http://bugs.kde.org/show_bug.cgi?id=42609">#42609</a>)</li>
      <li>kcontrol: activate mouse cursor moving with keyboard if enabled (<a href="http://bugs.kde.org/show_bug.cgi?id=92957">#92957</a>)</li>
      <li>kcontrol-usbview: USB revision number and level was given in decimal instead of hex</li>
      <li>kdesu: do not cause several seconds delay during logout</li>
      <li>khotkeys: work around a bug in Qt keyboard compression breaking simulated keyboard input (<a href="http://bugs.kde.org/show_bug.cgi?id=84434">#84434</a>)</li>
      <li>klipper: fix mouse selection sometimes not being recorded</li>
      <li>klipper: work around a problem in OpenOffice.org causing it to stop updating selection (<a href="http://bugs.kde.org/show_bug.cgi?id=80302">#80302</a>)</li>
      <li>klipper: do not record partial selections when created using the keyboard (<a href="http://bugs.kde.org/show_bug.cgi?id=85198">#85198</a>)</li>
      <li>klipper: reduce heavy load caused by clipboard selected in Acrobat Reader (<a href="http://bugs.kde.org/show_bug.cgi?id=68173">#68173</a>)</li>
      <li>klipper: protect against heavy load caused by broken clipboard handling in Lyx (<a href="http://bugs.kde.org/show_bug.cgi?id=84595">#84595</a>)</li>
      <li>kwin: avoid window description dialog for special window settings and try to guess the information automatically (<a href="http://bugs.kde.org/show_bug.cgi?id=84605">#84605</a>)</li>
      <li>kwin: various small improvements for special window settings</li>
      <li>kwin: don't keep splashscreens above dialog windows (<a href="http://bugs.kde.org/show_bug.cgi?id=93832">#93832</a>)</li>
      <li>kwin: apply window-specific force settings immediatelly (<a href="http://bugs.kde.org/show_bug.cgi?id=89849">#89849</a>)</li>
      <li>kwin: keep active desktop borders working after resolution change(<a href="http://bugs.kde.org/show_bug.cgi?id=92583">#92583</a>)</li>
      <li>kwin: fix a case when a window was not activated due to incorrect focus stealing prevention(<a href="http://bugs.kde.org/show_bug.cgi?id=91766">#91766</a>)</li>
      <li>kwin: show dialogs also when minimized in the window list(<a href="http://bugs.kde.org/show_bug.cgi?id=91194">#91194</a>)</li>
      <li>kwin: when moving a window to another virtual desktop, move also its dialogs(<a href="http://bugs.kde.org/show_bug.cgi?id=90794">#90794</a>)</li>
</ul>

<h3><a name="kdebindings">kdebindings</a></h3><ul>
</ul>

<h3><a name="kdeedu">kdeedu</a> <font size="-2">[<a href="3_3_2/kdeedu.txt">all CVS changes</a>]</font></h3><ul>
<li>kalzium: degrees fahrenheit and celsius were reversed in information dialogs  (<a href="http://bugs.kde.org/show_bug.cgi?id=91287">bug #91287</a>)</li>
<li>kbruch: fraction conversion is wrong due to cut digits after colon  (<a href="http://bugs.kde.org/show_bug.cgi?id=93080">bug #93080</a>)</li>
<li>kbruch: In factorisation mode input box should look disabled (<a href="http://bugs.kde.org/show_bug.cgi?id=92482">bug #92482</a>)</li>
<li>kiten: clear correctly the internal list in radical selector</li>
<li>kiten: get rid of crash of double-clicking delete button in learn (<a href="http://bugs.kde.org/show_bug.cgi?id=93414">bug #93414</a>)</li>
<li>klatin: add Polish data files</li>
<li>kmplot: program hangs when plotting arcsin and arccos  (<a href="http://bugs.kde.org/show_bug.cgi?id=92014">bug #92014</a>)</li>
<li>kturtle: The default language must be one of the supported kturtle languages. Fixes the broken Open-Examples directory as well as kturtle refusing to run any commands, when the KDE locale is a different dialect of one of the kturtle languages.</li>
</ul>

<h3><a name="kdegames">kdegames</a> <font size="-2">[<a href="3_3_2/kdegames.txt">all CVS changes</a>]</font></h3><ul>
<li>atlantik: fix problems with accepting invitations</li>
<li>atlantik: put player views in a scrollview (<a href="http://bugs.kde.org/show_bug.cgi?id=69043">bug #69043</a>)</li>
<li>atlantik: show correct amount of players in trade widget</li>
</ul>

<h3><a name="kdegraphics">kdegraphics</a> <font size="-2">[<a href="3_3_2/kdegraphics.txt">all CVS changes</a>]</font></h3><ul>
<li>kfax: fix kfax does not save printer settings (<a href="http://bugs.kde.org/show_bug.cgi?id=91659">bug #91659</a>)</li>
<li>kfax: fix Truncations occur on dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=89934">bug #89934</a>)</li>
<li>kfax: fix kfax prints all pages in one g3-file to one page (<a href="http://bugs.kde.org/show_bug.cgi?id=87788">bug #87788</a>)</li>
<li>kfax: fix can not print to printer or file (<a href="http://bugs.kde.org/show_bug.cgi?id=75034">bug #75034</a>)</li>
<li>kfax: fix BoundingBox: line in PS file has width and height interchanged (<a href="http://bugs.kde.org/show_bug.cgi?id=51058">bug #51058</a>)</li>
<li>kfax: fix KFax has its own printer dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=47080">bug #47080</a>)</li>
<li>kfax: fix rotation not honored when printing (<a href="http://bugs.kde.org/show_bug.cgi?id=90604">bug #90604</a>)</li>
<li>kfax: fix G3/G4/tiff-files not assigned to kFax (<a href="http://bugs.kde.org/show_bug.cgi?id=89972">bug #89972</a>)</li>
<li>kfax: fix zoom error (<a href="http://bugs.kde.org/show_bug.cgi?id=20028">bug #20028</a>)</li>
<li>kfax: fix Problem with format of fax-files (<a href="http://bugs.kde.org/show_bug.cgi?id=19765">bug #19765</a>)</li>
<li>kfax: fix CTRL-R shortcut for rotate does not work in kfax (<a href="http://bugs.kde.org/show_bug.cgi?id=84147">bug #84147</a>)</li>
<li>kfax: fix Can't print only part of a document (<a href="http://bugs.kde.org/show_bug.cgi?id=50286">bug #50286</a>)</li>
<li>kolourpaint: Fix crash due to text box when scaling image behind it</li>
<li>kolourpaint: Don't get stuck on a wait cursor after attempting to paste empty text into a text box</li>
<li>kolourpaint: Smaller selection and text box resize handles - covers up fewer selected pixels, doesn't cover up text</li>
<li>kolourpaint: Restore mouse cursor after deselecting selection/text tools</li>
</ul>

<h3><a name="kdemultimedia">kdemultimedia</a> <font size="-2">[<a href="3_3_2/kdemultimedia.txt">all CVS changes</a>]</font></h3><ul>
<li>akodelib: Play even more broken and weird MP3s well. (<a href="http://bugs.kde.org/show_bug.cgi?id=90025">#90025</a>, etc.)</li>
<li>akodelib: Don't crash on when mmap fails (e.g. on NTFS). (<a href="http://bugs.kde.org/show_bug.cgi?id=93343">#93343</a>)</li>
<li>akode-artsplugin: Play dual-mono files as stereo (<a href="http://bugs.kde.org/show_bug.cgi?id=89487">#89487</a>)</li>
<li>akode-artsplugin: Halt playobject if a stream terminates (<a href="http://bugs.kde.org/show_bug.cgi?id=88092">#88092</a>)</li>
</ul>

<h3><a name="kdenetwork">kdenetwork</a> <font size="-2">[<a href="3_3_2/kdenetwork.txt">all CVS changes</a>]</font></h3><ul>
<li>kppp: increased maximum length of callback number</li>
</ul>

<h3><a name="kdepim">kdepim</a> <font size="-2">[<a href="3_3_2/kdepim.txt">all CVS changes</a>]</font></h3><ul>
<li>kalarm: fix KAlarm button on message windows to make it always display the main window</li>
<li>kalarm: show scheduled times, not reminder times, in alarm list and system tray tooltip</li>
<li>kmail: remember the size of the separate message window instead of using
a fixed window size</li>
<li>kmail: gracefully handle broken connections when checking for new mail with IMAP</li>
<li>kmail: don't clear the readerwindow when new mail arrives in an imap folder</li>
<li>kmail: fix aegypten issue39 by only using the body itself as a text part
if there are no body parts and we didn't get any other text so far, such
as for smime opaque encrypted mails</li>
<li>kmail: fix the "folders not visible" problem after starting KMail when the intro is shown</li>
<li>kmail: fix searching when the header field in question is
the first one of a message</li>
<li>kmail: disable wordwrap in the composer for all inline invitation mails, not just for those sent with autosending enabled</li>
<li>kmail: don't translate the "local" folder prefix; this fixes problems with KMail's summary plugin</li>
<li>kmail: fix adding attachments to inline-forwarded messages (<a href="http://bugs.kde.org/show_bug.cgi?id=91132">bug #91132</a>)</li>
<li>kmail: remember the selected crypto format when saving a message as draft</li>
<li>kmail: hide filter actions from "Configure Toolbars" dialog; fixes crashes</li>
<li>kmail: fix missing filter actions after switching to the Mail component in Kontact</li>
<li>kmail: remove BCC coming from identity on ical invitations (<a href="https://intevation.de/roundup/kolab/issue474">kolab issue #474</a>)</li>
<li>kmail: grab the signature only once (<a href="http://bugs.kde.org/show_bug.cgi?id=91891">bug #91891</a>)</li>
<li>kmail: fix problem with disappearing dimap folders</li>
<li>kmail: don't kill running mailchecks when cancelling the subscription dialog</li>
<li>kmail: add missing tooltip to the BCC [...] button</li>
<li>kmail: make sure that no signature is appended to inline invitation replies, Outlook chokes on them</li>
<li>kmail: don't sign or encrypt inline invitations or invitation replies</li>
<li>kmail: fix PGP/MIME encrypting messages which are BCC'ed (<a href="http://bugs.kde.org/show_bug.cgi?id=92412">bug #92412</a>)</li>
<li>kmail: make sure that all changes in the folder properties are saved</li>
<li>kmail: fix uncontrolled checkboxes in the composer's attachment list (<a href="http://bugs.kde.org/show_bug.cgi?id=88576">bug #88576</a>)</li>
<li>kmail: improved performance when dragging mails over the folder list</li>
<li>kmail: provide all reply methods and all forward methods in the separate message window (<a href="https://intevation.de/roundup/kolab/issue97">kolab issue #97</a>)</li>
<li>kmail: fix minor bugs in the antispam wizard</li>
<li>kmail: correctly end new-mail-check when an IMAP folder reports an error (<a href="http://bugs.kde.org/show_bug.cgi?id=92416">bug #92416</a>)</li>
<li>kmail: fix 'kstart --iconify --windowclass kmail kmail' which allows starting KMail in the system tray (<a href="http://bugs.kde.org/show_bug.cgi?id=73591">cf. bug #73591</a>)</li>
<li>kmail: fix broken order of folders in the folder selection dropdown box (<a href="http://bugs.kde.org/show_bug.cgi?id=92710">bug #92710</a>)</li>
<li>kmail: notice a manual change of the external editor command line</li>
<li>kmail: adjust antispam wizard to changes in bogofilter 0.93</li>
<li>kmail: don't look up recipient keys during autosave (aegypten issue #278)</li>
<li>kmail: fix "Folder Menu not updated after enabling Expiration for a Folder (<a href="http://bugs.kde.org/show_bug.cgi?id=92918">bug #92918</a>)</li>
<li>kmail: add support for additional antispam tools</li>
<li>kmail: various speed improvements</li>
<li>kontact: don't show empty tip of the day on startup</li>
</ul>

<h3><a name="kdesdk">kdesdk</a> <font size="-2">[<a href="3_3_2/kdesdk.txt">all CVS changes</a>]</font></h3><ul>
<li>cervisia: Use correct encoding for status messages. (<a href="http://bugs.kde.org/92576">#92576</a>)</li>
</ul>

<h3><a name="kdetoys">kdetoys</a> <font size="-2">[<a href="3_3_2/kdetoys.txt">all CVS changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeutils">kdeutils</a> <font size="-2">[<a href="3_3_2/kdeutils.txt">all CVS changes</a>]</font></h3><ul>
<li>khexedit: fix files with "?" in the filename can't be opened (<a href="http://bugs.kde.org/show_bug.cgi?id=91635">bug #91635</a>)</li>
<li>khexedit: fix loading and saving of remote files</li>
<li>khexedit: fix khexedit statistics columns not sorted correctly (<a href="http://bugs.kde.org/show_bug.cgi?id=92249">bug #92249</a>)</li>
<li>kdf: hide /dev/shm and sysfs filesystems</li>
</ul>

<h3><a name="kdevelop">kdevelop</a> <font size="-2">[<a href="3_3_2/kdevelop.txt">all CVS changes</a>]</font></h3><ul>
<li>fixed many 3.1.x regressions in project templates</li>
<li><em>custommakefiles</em>: don't insert targets twice into the menu, sort the targets alphabetically (this time it works)</li>
<li>fixed the bug "the kdevelop3 custom makefile based project files do not work with cmake out-of-source builds"</li>
<li>Don't overwrite existing files(<a href="http://bugs.kde.org/show_bug.cgi?id=90446">#90446</a>)</li>
<li>Fix the forkbomb created by KDevAssistant when it was told to use KDevAssistant to remotely open documentation...(<a href="http://bugs.kde.org/show_bug.cgi?id=90334">#90334</a>)</li>
<li>fix mem leak</li>
<li>fix amd64 crash</li>
<li><em>Perl parser</em>; Don't require a whitespace between function name and the opening bracket</li>
<li>make the incremental qlistview search work in the filelist part</li>
</ul>

<h3><a name="kdewebdev">kdewebdev</a> <font size="-2">[<a href="3_3_2/kdewebdev.txt">all CVS changes</a>]</font></h3><ul>
<li><h4>Quanta Plus</h4>
<ul>
<li>don't crash after editing a cell of a newly inserted row/column in the table editor</li>
<li>show the right index of the main cell in case of merged rows in a table</li>
<li>don't crash on column removal form a table</li>
<li>read the tables correctly also if the doctype definition is wrong(for example HTML tables inside XHTML)</li>
<li>fix node tree corruption while parsing scripts inside a tag (<a href="http://bugs.kde.org/show_bug.cgi?id=91508">bug #91508</a>)</li>
<li>don't crash when deleting a file using the context menu (<a href="http://bugs.kde.org/show_bug.cgi?id=92676">bug #92676</a>)</li>
<li>disable Proceed button in the upload dialog once the upload is started. Fixes various problems like non-responding Quanta after upload and possibly the bug described in (<a href="http://bugs.kde.org/show_bug.cgi?id=88892">bug #88892</a>).
</li>
<li>don't try to add a newly created action to a non-existent All toolbar</li>
<li>don't crash on exit if the user removed an action</li>
<li>enable full copy/paste from documentation and preview. Until now it worked only with the mouse through the selection. (<a href="http://bugs.kde.org/show_bug.cgi?id=92936">bug #92936</a>)</li>
<li><strong>improvement:</strong> recognize one-line PHP comments starting with #</li>
</ul>
</li>
<li><h4>KFileReplace</h4>
<ul>
<li>hide columns that are not useful (but confusing) when doing search only</li>
</ul>
</li>
</ul>
