2006-03-26 12:29 +0000 [r522664]  aacid

	* branches/KDE/3.5/kdegames/kbattleship/kbattleship/kbattleship.cpp:
	  Improve the protocol, do not send info about which ship was hit.
	  Patch by Stephen McCamant See "KBattleship network protocol
	  reveals ship types" at kde-games-devel mailing list for more info
	  CCMAIL: smcc@csail.mit.edu

2006-04-02 19:50 +0000 [r525759]  aacid

	* branches/KDE/3.5/kdegames/kasteroids/view.cpp: Place the missile
	  closer to the ship so it does not goes thorugh very closer
	  asteroids Thanks Henry Malthus for the patch BUGS: 13388

2006-04-12 21:20 +0000 [r529243]  lanius

	* branches/KDE/3.5/kdegames/kreversi/Makefile.am,
	  branches/KDE/3.5/kdegames/knetwalk/src/Makefile.am: make kreversi
	  and knetwalk global highscore aware

2006-04-14 12:12 +0000 [r529766]  thiago

	* branches/KDE/3.5/kdegames/kbounce/game.h,
	  branches/KDE/3.5/kdegames/kbounce/main.cpp,
	  branches/KDE/3.5/kdegames/kbounce/game.cpp: Properly use the
	  HAVE_ARTS define BUG:122277

2006-04-25 11:46 +0000 [r533624-533622]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/cmd_line.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/move.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/cl_chop.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/caas.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/README,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/app_str.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/acconfig.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/state.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/inline.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/USAGE,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/TODO (removed),
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/main.c (added),
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/state.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_enums.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/test_arr.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/Makefile.lite,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/rand.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/config.h.freecell,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/Makefile.win32
	  (removed),
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_cl.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/alloc.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/jhjtypes.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_isa.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/CREDITS,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_isa.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/card.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_user.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_hash.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/INSTALL,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/intrface.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/config.h.dkds,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/gen_makefile.pl
	  (removed),
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_dm.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/lookup2.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_move.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/lib.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/tests.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/preset.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/caas.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/pqueue.h: trying
	  to get closer to fcs 2.8.12

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/cmd_line.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_cl.h: some
	  more changes

2006-04-25 12:03 +0000 [r533632]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/md5.h (removed),
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/test_lib.c
	  (removed), branches/KDE/3.5/kdegames/kpat/freecell-solver/CHANGES
	  (removed), branches/KDE/3.5/kdegames/kpat/freecell-solver/md5c.c
	  (removed): removing unnecessary code

2006-04-25 12:07 +0000 [r533634]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/inline.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/rand.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/rand.h: removed
	  some warnings

2006-04-25 12:11 +0000 [r533640]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/app_str.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/state.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/state.h: some more
	  type correctness

2006-04-25 12:32 +0000 [r533653]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/intrface.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/lib.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/caas.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/cl_chop.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/app_str.c: fix
	  more warnings

2006-04-25 12:43 +0000 [r533657]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/cmd_line.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/state.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/freecell.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/simpsim.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_move.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/move.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/Makefile.lite:
	  fixed some more warnings

2006-04-25 13:02 +0000 [r533673]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/config.h.freecell
	  (removed),
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/config.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/acconfig.h
	  (removed),
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/config.h.dkds
	  (removed),
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/state.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/caas.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/Makefile.lite,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/app_str.c: more
	  warnings fixed

2006-04-25 13:19 +0000 [r533677]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/config.h
	  (removed),
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/alloc.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/intrface.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_hash.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/state.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/freecell.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_config.h
	  (added), branches/KDE/3.5/kdegames/kpat/freecell-solver/state.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_isa.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/scans.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/Makefile.lite:
	  that alone would explain a lot about my crashes

2006-04-25 16:22 +0000 [r533816]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/cmd_line.c,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_cl.h,
	  branches/KDE/3.5/kdegames/kpat/freecell-solver/inline.h: fixing
	  my fixes

2006-04-29 08:33 +0000 [r535307]  woebbe

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/fcs_cl.h:
	  -pedantic

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

