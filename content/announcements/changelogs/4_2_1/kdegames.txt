------------------------------------------------------------------------
r914647 | scripty | 2009-01-21 13:41:09 +0000 (Wed, 21 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r914861 | kleag | 2009-01-21 22:18:56 +0000 (Wed, 21 Jan 2009) | 13 lines

Merged revisions 913444,914856 via svnmerge from 
svn+ssh://kleag@svn.kde.org/home/kde/trunk/KDE/kdegames/ksirk

........
  r913444 | mlaurent | 2009-01-19 13:53:27 +0100 (lun., 19 janv. 2009) | 2 lines
  
  Now we use kde4
........
  r914856 | kleag | 2009-01-21 23:14:13 +0100 (mer., 21 janv. 2009) | 1 line
  
  some actions are authorized only if done by the current player
........

------------------------------------------------------------------------
r915379 | aacid | 2009-01-22 22:14:52 +0000 (Thu, 22 Jan 2009) | 6 lines

Install default_preview.png

Will be fixed in KDE 4.2.1

BUGS: 181596

------------------------------------------------------------------------
r916508 | scripty | 2009-01-25 12:51:47 +0000 (Sun, 25 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r917456 | schwarzer | 2009-01-27 21:27:06 +0000 (Tue, 27 Jan 2009) | 1 line

backport of r916293: fix typo in cheat notification dialog
------------------------------------------------------------------------
r917457 | schwarzer | 2009-01-27 21:27:16 +0000 (Tue, 27 Jan 2009) | 1 line

backport of r916294: fix game to be playable in pause mode
------------------------------------------------------------------------
r917631 | annma | 2009-01-28 08:30:26 +0000 (Wed, 28 Jan 2009) | 2 lines

fix build 

------------------------------------------------------------------------
r918222 | scripty | 2009-01-29 17:18:07 +0000 (Thu, 29 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r918882 | sengels | 2009-01-30 22:17:36 +0000 (Fri, 30 Jan 2009) | 1 line

add app icons - thx to parkotron for the hint
------------------------------------------------------------------------
r919948 | schwarzer | 2009-02-01 21:58:48 +0000 (Sun, 01 Feb 2009) | 1 line

backport of r919941: fix cheat more not being reset on restartGame action
------------------------------------------------------------------------
r920020 | scripty | 2009-02-02 08:36:37 +0000 (Mon, 02 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r920550 | scripty | 2009-02-03 08:33:40 +0000 (Tue, 03 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r922553 | scripty | 2009-02-07 08:48:35 +0000 (Sat, 07 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r923076 | scripty | 2009-02-08 07:40:51 +0000 (Sun, 08 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r924601 | scripty | 2009-02-11 08:22:50 +0000 (Wed, 11 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r927120 | piacentini | 2009-02-16 20:52:45 +0000 (Mon, 16 Feb 2009) | 2 lines

Backporting fixes to crashing bug 184523

------------------------------------------------------------------------
r928333 | mueller | 2009-02-19 12:33:14 +0000 (Thu, 19 Feb 2009) | 3 lines

fix build. if you want printf, you want stdio. you don't want printf
though

------------------------------------------------------------------------
r930981 | coolo | 2009-02-24 15:21:00 +0000 (Tue, 24 Feb 2009) | 3 lines

fixed the save game functionality
BUG: 184274

------------------------------------------------------------------------
