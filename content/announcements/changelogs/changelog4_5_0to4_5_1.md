---
title: "KDE 4.5.1 Changelog"
hidden: true
---

  <h2>Changes in KDE 4.5.1</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_5_1/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdecore">kdecore</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Possibly fix crash in KSharedDataCache when put under heavy load. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=243573">243573</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1167621&amp;view=rev">1167621</a>. </li>
        <li class="bugfix crash">Fix crash in KSharedDataCache when the cache is created with room for only 1 page. See SVN commit <a href="http://websvn.kde.org/?rev=1167621&amp;view=rev">1167621</a>. </li>
        <li class="bugfix crash">Fix crash in KArchive when extracting large files, eg USNO KStars database. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=237124">237124</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1167508&amp;view=rev">1167508</a>. </li>
      </ul>
      </div>
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">I/O performance increases in KIconLoader. See SVN commit <a href="http://websvn.kde.org/?rev=1164755&amp;view=rev">1164755</a>. </li>
      </ul>
      </div>
      <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix tooltip rendering glitches in eg Dolphin. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=245491">245491</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1160677&amp;view=rev">1160677</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Set the reverse link in the ChildFrame tree earlier, fixing a crash resizing child iframes early in their lifetime. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=245691">245691</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1164025&amp;view=rev">1164025</a>. </li>
        <li class="bugfix crash">Fix crash due to special child object lists getting out of sync. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170165">170165</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1164054&amp;view=rev">1164054</a>. </li>
        <li class="bugfix crash">Fix crashes involving :first-letter and dynamic content. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=161989">161989</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1164385&amp;view=rev">1164385</a>. </li>
      </ul>
      </div>
      <h4><a name="knewstuff">knewstuff</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when choosing from an Install button with a drop down menu. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=240898">240898</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1159909&amp;view=rev">1159909</a>. </li>
      </ul>
      </div>
      <h4><a name="libplasma">libplasma</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Allow resize/move Tux Eyes plasmoid. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=244958">244958</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1161819&amp;view=rev">1161819</a>. </li>
        <li class="bugfix ">Usability: Show mnemonics on pushbuttons. See SVN commit <a href="http://websvn.kde.org/?rev=1164886&amp;view=rev">1164886</a>. </li>
        <li class="bugfix crash">Fix crash when clearing all notifications while subset displayed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=248976">248976</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1168060&amp;view=rev">1168060</a>. </li>
      </ul>
      </div>

    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_5_1/kdebase.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix a crash that can happen when switching from Columns View to another view mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=247618">247618</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1165533&amp;view=rev">1165533</a>. </li>
        <li class="bugfix crash">Fix wrong behavior (or triggering of an assertion in the debug mode), if "Go Up" is used in the Columns View. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=248405">248405</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1165552&amp;view=rev">1165552</a>. </li>
        <li class="bugfix ">Initialize the zoom slider correctly on startup. See SVN commit <a href="http://websvn.kde.org/?rev=1165573&amp;view=rev">1165573</a>. </li>
      </ul>
      </div>
      <h4><a name="network:/ kio-slave">network:/ kio-slave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Wait for auto-starting of Cagibi per D-Bus, so finally UPnP devices/services are also listed. See SVN commit <a href="http://websvn.kde.org/?rev=1168035&amp;view=rev">1168035</a>. </li>
      </ul>
      </div>
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not switch virtual desktop when using minimize to desktop in alt+tab. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=247532">247532</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1162775&amp;view=rev">1162775</a>. </li>
        <li class="bugfix ">Synchronise on all desktops states between transient and its main window. See SVN commit <a href="http://websvn.kde.org/?rev=1165453&amp;view=rev">1165453</a>. </li>
        <li class="bugfix ">Ensure that windows don't go outside the workarea. See SVN commit <a href="http://websvn.kde.org/?rev=1165454&amp;view=rev">1165454</a>. </li>
        <li class="bugfix ">Fix regression which caused desktop effects not working with some drivers. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=243991">243991</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1167908&amp;view=rev">1167908</a>. </li>
      </ul>
      </div>
      <h4><a name="plasma workspaces">Plasma workspaces</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">KRunner: display file search results by tag correctly. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=246689">246689</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1159432&amp;view=rev">1159432</a>. </li>
        <li class="bugfix ">Make it harder to accidentally drag plasmoids out of the panel and onto the desktop. See SVN commit <a href="http://websvn.kde.org/?rev=1159603&amp;view=rev">1159603</a>. </li>
        <li class="bugfix ">More ergonomic panel applet positioning; use the mouse position to determin new position.  Allows reordering of long panel applets even to the first position. See SVN commit <a href="http://websvn.kde.org/?rev=1159643&amp;view=rev">1159643</a>. </li>
        <li class="bugfix ">Fix Newspaper activity scroll by scrollbar. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=245230">245230</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1159829&amp;view=rev">1159829</a>. </li>
        <li class="bugfix ">Fix wetter.com weather data engine web service. See SVN commit <a href="http://websvn.kde.org/?rev=1163541&amp;view=rev">1163541</a>. </li>
        <li class="bugfix ">Fix Plasma crash when changing layout of panels. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249020">249020</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1168062&amp;view=rev">1168062</a>. </li>
        <li class="bugfix ">Fix RSS dataengine not updating on network reconnect. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=245639">245639</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1170005&amp;view=rev">1170005</a>. </li>
        <li class="bugfix ">Fix Weather applets not updating on network reconnection. See SVN commit <a href="http://websvn.kde.org/?rev=1170006&amp;view=rev">1170006</a>. </li>
      </ul>
      </div>

    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_5_1/kdeutils.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/ark/" name="ark">Ark</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Support archive delimiters from different 7z versions. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=247628">247628</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1164369&amp;view=rev">1164369</a>. </li>
        <li class="bugfix normal">Valid subfolder values are checked only if extraction into subfolder is enabled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=248638">248638</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1166479&amp;view=rev">1166479</a>. </li>
        <li class="bugfix crash">Make sure the auxiliary extraction process is terminated in case of errors. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=248336">248336</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1166790&amp;view=rev">1166790</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix hang when changing owner trust to ultimate. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=244288">244288</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1160302&amp;view=rev">1160302</a>. </li>
        <li class="bugfix ">Fix decrypting files in editor. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=248161">248161</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1164871&amp;view=rev">1164871</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeplasma-addons"><a name="kdeplasma-addons">kdeplasma-addons</a><span class="allsvnchanges"> [ <a href="4_5_1/kdeplasma-addons.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a name="comic applet">Comic Applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Comic is shown on the correct monitor when using the fullview feature. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=245503">245503</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1161709&amp;view=rev">1161709</a>. </li>
        <li class="bugfix ">Removes empty area at the top of the comic applet. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=248810">248810</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1167272&amp;view=rev">1167272</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_5_1/kdenetwork.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kget">KGet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Do not delete the putjob if it deleted itself. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=245589">245589</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1161727&amp;view=rev">1161727</a>. </li>
        <li class="bugfix ">Respect the maximum size limit of VFAT. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=245623">245623</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1161757&amp;view=rev">1161757</a>. </li>
        <li class="bugfix crash">Fixes crash in KGet runner caused by DBus. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=221751">221751</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1163240&amp;view=rev">1163240</a>. </li>
        <li class="bugfix crash">KGetPieChart does not crash when a download is added to an empty download list. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=246668">246668</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1163288&amp;view=rev">1163288</a>. </li>
      </ul>
      </div>
      <h4><a name="kopete">Kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Don't show joins nor quits in the chatwindow, if user has selected not to show events. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=109822">109822</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1165283&amp;view=rev">1165283</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_5_1/kdegraphics.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org/" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Comicbook backend: do not crash when closing while opening a CBR document. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=245499">245499</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1160615&amp;view=rev">1160615</a>. </li>
        <li class="bugfix crash">Fax backend: do not crash when loading G3 faxes produced by Ghostscript. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=247300">247300</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1161850&amp;view=rev">1161850</a>. </li>
        <li class="bugfix ">When archiving, if the document is a symbolic link then resolve it. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=245243">245243</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1161860&amp;view=rev">1161860</a>. </li>
        <li class="bugfix ">Fix jumpy behaviour of Middle Mouse Button zooming. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=247797">247797</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1163539&amp;view=rev">1163539</a>. </li>
        <li class="bugfix ">KDE Security Advisory 2010-08-25-1 - CVE-2010-2575. See SVN commit <a href="http://websvn.kde.org/?rev=1167827&amp;view=rev">1167827</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_5_1/kdeedu.txt">all SVN changes</a> ]</span></h3>
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/marble" name="marble">Marble</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Do not load incompatible plugins and crash. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=239831">239831</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1168342&amp;view=rev">1168342</a>. </li>
        <li class="bugfix ">Add support for alpha blending (useful e.g. for a hillshading layer in maps). See SVN commit <a href="http://websvn.kde.org/?rev=1167901&amp;view=rev">1167901</a>. </li>
        <li class="bugfix ">Fix progress animation not being shown on Windows and Maemo. See SVN commit <a href="http://websvn.kde.org/?rev=1167631&amp;view=rev">1167631</a>. </li>
        <li class="bugfix ">Adjust downloadable maps for the new blending feature. See SVN commit <a href="http://websvn.kde.org/?rev=1167522&amp;view=rev">1167522</a>. </li>
        <li class="bugfix ">Fix cut off text on Windows. See SVN commit <a href="http://websvn.kde.org/?rev=1166805&amp;view=rev">1166805</a>. </li>
        <li class="bugfix ">Instead of reporting an xls:lang error when OpenRouteService cannot handle a specific language code, fall back to english (which is known to work). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=247339">247339</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1163352&amp;view=rev">1163352</a>. </li>
      </ul>
      </div>
      <h4><a href="http://edu.kde.org/kgeography" name="kgeography">KGeography</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Update the Malawi of flag. See SVN commit <a href="http://websvn.kde.org/?rev=1164838&amp;view=rev">1164838</a>. </li>
        <li class="bugfix ">Fix typo in Great Britain Countries map. See SVN commit <a href="http://websvn.kde.org/?rev=1167489&amp;view=rev">1167489</a>. </li>
      </ul>
      </div>
    </div>
