2008-03-27 22:10 +0000 [r790970]  qbast

	* branches/KDE/4.0/kdelibs/dnssd/avahi-domainbrowser.cpp: Backport
	  of avahi domain browser fix.

2008-03-28 21:09 +0000 [r791245]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/dialogs/katedialogs.cpp: backport:
	  enable the APPLY button if the other tab settings change

2008-03-29 06:22 +0000 [r791352]  zachmann

	* branches/KDE/4.0/kdelibs/kdecore/io/ksavefile.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/kernel/kglobal.h,
	  branches/KDE/4.0/kdelibs/kdecore/io/ktempdir.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/kernel/kglobal.cpp: o Fix: let
	  KSaveFile honor the umask. Added a method to KGlobal to return
	  the umask. The umask is read at program start to avoid race
	  conditions. Use the new function also in KTempDir.

2008-03-30 18:24 +0000 [r791908-791907]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.cpp: Ugh. Fix the
	  return value of replaceChild.

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_elementimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_elementimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.cpp: Fix up the
	  full-blown Attr codes to properly take care of their kids. Patch
	  by Harri, w/some tweaks from me to the ID-cache handling and a
	  removal of a no-longer-need hack in textContent

2008-03-30 20:43 +0000 [r791959]  pino

	* branches/KDE/4.0/kdelibs/kparts/browserextension.h: fully qualify
	  one of the arguments of the signal, so it's usable again

2008-03-31 16:16 +0000 [r792196]  lunakl

	* branches/KDE/4.0/kdebase/runtime/kde4 (added),
	  branches/KDE/4.0/kdelibs/kdeui/kernel/kapplication.cpp,
	  branches/KDE/4.0/kdebase/runtime/CMakeLists.txt: Make KDE4
	  applications restart in saved session using a wrapper script
	  'kde4' if not running in a KDE4 session
	  (http://lists.kde.org/?t=120569055200005&r=1&w=2).

2008-03-31 20:46 +0000 [r792333]  sebsauer

	* branches/KDE/4.0/kdelibs/kjsembed/kjsembed/qobject_binding.cpp:
	  backport r792331: fix handling of QUrl

2008-04-01 10:53 +0000 [r792495]  rjarosz

	* branches/KDE/4.0/kdelibs/kdecore/config/kconfig.cpp: Backport
	  commit 792494. Fix deletion of group with unicode characters.
	  CCBUG: 159949

2008-04-01 20:37 +0000 [r792683-792682]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/html/html_objectimpl.cpp: We want
	  to use effectiveURL here so we properly handle data: on the
	  object/embed mess.

	* branches/KDE/4.0/kdelibs/khtml/dom/dom_node.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.cpp: Properly
	  close DOM-created nodes. Fixes CNN.com election tracker, and 3
	  Acid3 tests (more like 3.5, actually --- gets one further). It
	  also makes Acid3 look uglier, since it exposes a bug in rendering
	  of alternate content (it didn't show up before since the object
	  was plain not getting loaded). I know what's wrong just need to
	  <strike>Bug dfaure for a solution</strike>figure out how to fix
	  it. CCBUG:156947

2008-04-01 22:49 +0000 [r792734]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.cpp: Properly
	  update currentTarget in capture phases. Fixes acid3 test31. (Line
	  got lost during container porting. Bad review job by me :( )
	  CCBUG:156947

2008-04-02 10:10 +0000 [r792821]  dfaure

	* branches/KDE/4.0/kdelibs/kio/kio/kfileitem.cpp,
	  branches/KDE/4.0/kdelibs/kio/kio/kfileitem.h: no-op docu cleanup
	  (already in trunk)

2008-04-02 12:59 +0000 [r792866]  lunakl

	* branches/KDE/4.0/kdelibs/kinit/klauncher.cpp,
	  branches/KDE/4.0/kdelibs/kinit/klauncher_main.cpp,
	  branches/KDE/4.0/kdelibs/kinit/klauncher.h: Do not call signal
	  unsafe code in a signal handler. BUG: 157733

2008-04-02 15:32 +0000 [r792924]  dfaure

	* branches/KDE/4.0/kdelibs/kinit/lnusertemp.c: The old fix for
	  creating missing parent dirs if needed; retested by Andras
	  Mantia.

2008-04-02 20:44 +0000 [r793010]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp: Provide
	  setter function for whoever wants to tackle the ToDo mentioned in
	  Dirk's comment.

2008-04-03 04:23 +0000 [r793181-793180]  ggarand

	* branches/KDE/4.0/kdelibs/khtml/css/css_base.cpp,
	  branches/KDE/4.0/kdelibs/khtml/css/css_stylesheetimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/html/html_headimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/css/css_ruleimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/css/css_base.h,
	  branches/KDE/4.0/kdelibs/khtml/css/css_stylesheetimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/html/html_headimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/css/css_ruleimpl.cpp:
	  automatically merged revision 792695: rework the tracking of
	  stylesheets' loaded state. @import logic merged from WebCore made
	  it subject to pathological loss of synchronisation. BUG:155493

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_textimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom2_rangeimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_xmlimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom2_rangeimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_xmlimpl.h: automatically
	  merged revision 793178: merge some ad hoc changes to
	  RangeImpl::surroundContents, whose main rationale is "keep
	  Acid3's 11th test happy". I tried hard to make up a sound
	  explanation for those, digging deep into the specification's
	  arcane semantics, but eventually, I don't think I really
	  convinced myself, and certainly not Harri. He says: "it's a
	  pragmatic solution". And that's what it is. CCBUG:156947

2008-04-04 08:48 +0000 [r793504]  dfaure

	* branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.h,
	  branches/KDE/4.0/kdelibs/kdecore/kernel/kglobal.cpp: Move the
	  umask value out of KConfigPrivate to avoid a crash on exit when
	  ~KConfig calls sync.

2008-04-04 19:07 +0000 [r793672]  orlovich

	* branches/KDE/4.0/kdelibs/kdeui/widgets/kanimatedbutton.cpp: Cache
	  and reuse pixmaps for each frame. Before, new ones were created,
	  and Qt would stick every single one of them into the pixmap
	  cache, potentially keeping as many as ~1000 X pixmaps
	  pointlessly, which could slow down things such as EXA (though
	  FredrikH made the latest devel X handle that better..)

2008-04-05 15:01 +0000 [r793889]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp: Moved bad
	  call out of constructor to all callers I could find. No
	  functional change, i.e. no badness removed but will allow for
	  easier incremental fixing of what's mentioned in Dirk's comment.

2008-04-05 15:23 +0000 [r793896]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.cpp: Make
	  createDocument() work as specified in DOM Level 2: Use the
	  user-supplied doctype and set its Node.ownerDocument attribute to
	  the document being created.

2008-04-05 15:30 +0000 [r793906]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp: Removed now
	  obsolete code. Should have done that with my previous commit
	  already.

2008-04-05 16:44 +0000 [r793941]  porten

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.h,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_html.h: pmk moved the
	  title property up (or down?) to the XML Document. Move the
	  JavaScript property there, too.

2008-04-06 19:33 +0000 [r794168]  buis

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/4.0/kdelibs/khtml/css/cssparser.cpp,
	  branches/KDE/4.0/kdelibs/khtml/css/cssvalues.c,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_style.h,
	  branches/KDE/4.0/kdelibs/khtml/css/cssstyleselector.cpp,
	  branches/KDE/4.0/kdelibs/khtml/css/cssvalues.in,
	  branches/KDE/4.0/kdelibs/khtml/css/cssvalues.h,
	  branches/KDE/4.0/kdelibs/khtml/css/css_renderstyledeclarationimpl.cpp:
	  Backport of my commit 770413.

2008-04-06 19:35 +0000 [r794169]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/khtmlview.cpp: Fix access keys,
	  and autoscroll suspension. (I will leave it to someone else to
	  ponder the issue of fallbacks showing up): 1) The key events
	  actually do show the modifier state after the event these days 2)
	  Use QLinkedList and not QList for the candidates list in fallback
	  computation as we need iterators to remain valid through removal
	  of other entries; QList's don't. 3) Adjust positioning code to
	  deal with the latest incarnation of the scroll widget 4) Give the
	  accesskey tooltips a background so at least one is readable if
	  they overlap.

2008-04-07 17:07 +0000 [r794482]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/khtml_part.cpp: Let a redirect
	  from within a javascript: frame source win out over the text..
	  (For src="javascript:this.window.location.href='foo'", on the old
	  freemail.hu, better written as src="foo") CCBUG:106748

2008-04-07 19:10 +0000 [r794513]  orlovich

	* branches/KDE/4.0/kdelibs/kparts/browserrun.cpp: Applying a patch
	  from David that permits KHTML to properly do fallback content on
	  error pages, but letting it know when that happens..

2008-04-07 19:20 +0000 [r794519]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/khtml_part.h,
	  branches/KDE/4.0/kdelibs/khtml/khtml_run.cpp,
	  branches/KDE/4.0/kdelibs/khtml/khtml_part.cpp: Properly fall back
	  to alternate content when doing delayed part creation due to
	  needing to lookup the mimetype (Acid3 red kitten)

2008-04-07 19:25 +0000 [r794524]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/khtml_run.h: I probably want to
	  commit this, too.

2008-04-07 22:26 +0000 [r794580]  ossi

	* branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/config/kconfig.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.h: backport:
	  fix copyGroup not marking the config dirty.

2008-04-07 23:54 +0000 [r794588-794587]  dfaure

	* branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/config/kconfig.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.h: revert
	  uncompileable 794580: there is no KConfigGroup::copyTo in
	  branch...

	* branches/KDE/4.0/kdelibs/kdecore/util/kpluginfactory.cpp: make
	  KPluginFactory::stringListToVariantList faster by being simpler

2008-04-08 08:05 +0000 [r794647]  ossi

	* branches/KDE/4.0/kdelibs/kdecore/tests/kconfigtest.cpp,
	  branches/KDE/4.0/kdelibs/kdecore/config/kconfig.cpp: fix
	  KConfig::copyTo() not marking the target dirty.

2008-04-08 13:57 +0000 [r794748]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/widgets/khistorycombobox.h: Port
	  sample code to KDE4 API

2008-04-08 16:19 +0000 [r794790]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/imload/tilecache.h: Don't
	  double-decrement the cache usage when removing tiles..

2008-04-08 16:29 +0000 [r794796]  orlovich

	* branches/KDE/4.0/kdelibs/kparts/browserrun.cpp: Don't crash if
	  this is called with a non-TransferJob...

2008-04-08 20:21 +0000 [r794895]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/view/kateviewhelpers.cpp,
	  branches/KDE/4.0/kdelibs/kate/view/kateviewhelpers.h: backport
	  fix crash: 1. m_blockRange is a smart-range that highlights the
	  block for the given codefolding region 2. when the mouse is over
	  the code folding border, m_blockRange is a valid smart range 3.
	  F5 reloads the document, which deletes the smart range 4. now
	  m_blockRange is a dangling pointer 5. moving the mouse deletes
	  this dangling pointer -> crash CCBUG: 160527

2008-04-08 21:22 +0000 [r794919]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/view/kateviewhelpers.cpp,
	  branches/KDE/4.0/kdelibs/kate/view/kateviewhelpers.h: backport
	  fix: Code Folding Bug: color doesn't disappear CCBUG: 159021

2008-04-09 02:16 +0000 [r794973]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/khtml_part.cpp: Make searching
	  within textareas work again. Seems like we used to attach their
	  kids back in the days? BUG: 160490

2008-04-09 11:12 +0000 [r795168]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/document/katetextline.cpp:
	  backport: fix off-by-one bug when converting a virtual column to
	  a real one

2008-04-09 11:26 +0000 [r795170]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/document/katetextline.cpp:
	  backport: == -> > operator in fromVirtualColumn

2008-04-09 11:34 +0000 [r795173]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/view/kateview.cpp: backport Fix
	  crash: collapsed method cursor behind line crash CCBUG: 160424

2008-04-09 21:47 +0000 [r795351]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/syntax/katehighlighthelpers.cpp:
	  fix uppercase-bug which was introduced in r439322. fixes bug:
	  Hexidecimal numbers not highlighted CCBUG: 158162

2008-04-09 22:18 +0000 [r795359]  mlaurent

	* branches/KDE/4.0/kdelibs/kio/kio/scheduler.h: Backport: Fix
	  assert when we reparse config (call by dbus) Q_CLASSINFO is
	  necessary (thanks Thiago to point me this pb)

2008-04-10 19:22 +0000 [r795564]  mkretz

	* branches/KDE/4.0/kdelibs/phonon/platform_kde/kiomediastream.cpp:
	  backport 795562: fix race condition:
	  ~/KDE/src/playground-multimedia/phonon/phonon-pcmio/platform_kde
	  - if a FileJob reached the end - then the backend requests a seek
	  - then the result signal is delivered - the job was cleaned up,
	  but the seek would never finish

2008-04-11 23:47 +0000 [r795939]  staniek

	* branches/KDE/4.0/kdelibs/kfile/kdiroperator.cpp,
	  branches/KDE/4.0/kdelibs/kfile/kfilewidget.cpp: backports for
	  kfile r794821, r795837: A set of fixes for improving experience
	  of file dialogs, mostly on Windows + some fixes for handling urls
	  KFileWidget: - check whether a string contains protocol part more
	  carefully because c:/ is an absolute path, not a protocol (static
	  containsProtocolSection()), so we assume the protocol has at
	  least two characters before ":/" - enclose
	  locationEdit->currentText() with QDir::fromNativeSeparators() and
	  add .trimmed() to cover more cases in conditions - slotOk(): add
	  code for handling accepting using OK button when directory path
	  is entered and directories are not acceptable result of the file
	  widget: cd to that directory then; in particuar it is now
	  possible to enter C:\ or / and press OK to change to the root dir
	  on windows (or linux, respectively) r795936: Fix changing the
	  current dir using the 'recent url' combo box. Upon accepting new
	  url in the 'directory' url combo box, append '/' if needed: the
	  combo does not add it, but tokenize() expects it because uses
	  KUrl::setFileName(), which would silently remove the last segment
	  of the path. Without the fix, if you select /home/js/foo/bar from
	  the url combo box, and click on file.txt, the resulting url would
	  be /home/js/foo/file.txt, not the expected
	  /home/js/foo/bar/file.txt. Another, and hopefully the last place
	  where we have to care about trailing slashes for dirs, is
	  kdiroperator. (reviewed)

2008-04-12 08:56 +0000 [r795997]  staniek

	* branches/KDE/4.0/kdelibs/interfaces/ktexteditor/ktexteditor.cpp:
	  backport r160183 KTextEditorFactoryList: make qAddPostRoutine
	  play well with K_GLOBAL_STATIC. Also, delete factory in
	  KTextEditor::editor() if qobject_cast<KTextEditor::Factory*>()
	  failed.

2008-04-12 21:18 +0000 [r796162-796160]  porten

	* branches/KDE/4.0/kdelibs/kjs/regexp.cpp: The PCRE author Philip
	  Hazel was so kind to introduce an optional JS-compat mode into
	  the next release (7.7). Activate it when present which fixes two
	  Acid3 bugs.

	* branches/KDE/4.0/kdelibs/cmake/modules/FindPCRE.cmake: Merged
	  pkg-config detection for PCRE from trunk.

2008-04-13 12:09 +0000 [r796353]  mueller

	* branches/KDE/4.0/kdelibs/cmake/modules/FindKDE4Internal.cmake:
	  merge -r796352

2008-04-14 09:23 +0000 [r796755]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/xmlgui/kxmlguibuilder.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/tests/kxmlgui_unittest.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/tests/kxmlgui_unittest.h: Don't
	  remove separator after action list, testcase by Joris Guisson,
	  patch by Albert Astals Cid, unit test by me.

2008-04-14 16:05 +0000 [r796975]  whiting

	* branches/KDE/4.0/kdelibs/knewstuff/knewstuff2/core/coreengine.cpp,
	  branches/KDE/4.0/kdelibs/knewstuff/knewstuff2/core/installation.cpp,
	  branches/KDE/4.0/kdelibs/knewstuff/knewstuff2/core/installation.h:
	  backport AbsoluteInstallPath target specification

2008-04-14 18:53 +0000 [r797028]  dfaure

	* branches/KDE/4.0/kdelibs/kwallet/backend/kwalletbackend.cc: Fix
	  assert due to KGlobal::dirs()->saveLocation("kwallet") called
	  before defining the "kwallet" resource. (BUG 160817)

2008-04-15 08:44 +0000 [r797263]  pino

	* branches/KDE/4.0/kdelibs/interfaces/ktexteditor/ktexteditor.cpp:
	  include qcoreapplication.h for q{Add,Remove}PostRoutine

2008-04-15 11:52 +0000 [r797300]  mueller

	* branches/KDE/4.0/kdelibs/kio/kfile/kfsprocess.cpp,
	  branches/KDE/4.0/kdelibs/kio/kfile/kdiskfreespace.cpp,
	  branches/KDE/4.0/kdelibs/kio/kfile/tests/kicondialogtest.cpp,
	  branches/KDE/4.0/kdelibs/kio/kfile/kdiskfreespace.h: merge
	  KDiskFreeSpace rewrite

2008-04-16 15:02 +0000 [r797620]  mueller

	* branches/KDE/4.0/kdelibs/solid/solid/deviceinterface.h,
	  branches/KDE/4.0/kdelibs/solid/solid/deviceinterface.cpp: merged
	  -r797619

2008-04-16 21:21 +0000 [r797834]  dhaumann

	* branches/KDE/4.0/kdelibs/kate/utils/katesearchbar.cpp: backport:
	  fix document reload crashes

2008-04-18 14:24 +0000 [r798531]  orlovich

	* branches/KDE/4.0/kdelibs/kio/misc/kpac/proxyscout.cpp,
	  branches/KDE/4.0/kdelibs/kio/kio/kprotocolmanager.cpp,
	  branches/KDE/4.0/kdelibs/kio/misc/kpac/proxyscout.h,
	  branches/KDE/4.0/kdelibs/kio/misc/kpac/script.cpp: Apply a
	  portion of proxy support patch from Sergey Saukh (username
	  thelich at the hostname yandex with tld ru). This portion doesn't
	  actually fix the proxies proper (I can't review that part), but
	  rather the autoconfiguration code, in particular: - It makes the
	  JS functions in the PAC script code actually work, by overriding
	  the proper thing for KJS4 - It makes the proxyForUrl D-bus method
	  we exported by giving it the marshallable QString parameter 1
	  instead of a KUrl - It actually calls "proxyForUrl" not
	  "proxyForURL"

2008-04-18 21:01 +0000 [r798684]  dfaure

	* branches/KDE/4.0/kdelibs/kio/kfile/kopenwithdialog.cpp: Fix
	  service not found assertion when clicking "remember application"
	  (e.g. after selecting konqueror in the list) (Bug 159118)

2008-04-19 08:22 +0000 [r798738]  porten

	* branches/KDE/4.0/kdelibs/kjs/regexp_object.cpp: Merged revision
	  798733: Provide a bit of file&line number with RegExp parse
	  errors.

2008-04-20 00:08 +0000 [r798931]  porten

	* branches/KDE/4.0/kdelibs/khtml/khtml_part.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/debugger/debugwindow.cpp:
	  Merged revision 798930: Fixed rich text display of pseudo file
	  name "<regexp>" and others with special chars in them. Thanks to
	  Chusslove for the introduction into the miracles if i18n().

2008-04-20 16:00 +0000 [r799153]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom2_eventsimpl.cpp: Don't
	  crash when calling with missing doc renderer. Could possibly
	  happen during document teardown but I got it with a form element
	  inserted into an XHTML document created with createDocument().

2008-04-20 22:19 +0000 [r799256]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_textimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_textimpl.cpp: Merged
	  revision 799254: Merged WebCore's r30088 to implement wholeText
	  property and replaceWholeText() function from DOM Level 3.

2008-04-20 22:45 +0000 [r799266]  porten

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.h: Merged revision
	  799265: Fixed copy&paste error.

2008-04-20 22:54 +0000 [r799272]  porten

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_textimpl.cpp: Merged
	  revision 799271: Applied Allen's patch to fix update of the
	  :empty pseudo-class. Now that we also support replaceWholeText()
	  we pass Acid3 test 38.

2008-04-22 14:12 +0000 [r799848-799842]  ggarand

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_block.h,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_object.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_table.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_block.cpp:
	  automatically merged revision 796955: make vertical alignment of
	  inline-tables and inline-blocks comply with requirements of CSS
	  2.1 - 10.8.1. we'll only apply the inline table logic to pure CSS
	  tables though, to remain compatible with what Gecko and Opera do.
	  merged WC helper getBaselineOfLastLineBox() in the process.

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_form.cpp:
	  automatically merged revision 796956: .rework sizing of textarea
	  so that it works in all styles. .optimised implementation of
	  textarea's setText for common case where new content begins with
	  a substring of the old content.

	* branches/KDE/4.0/kdelibs/khtml/rendering/bidi.cpp: automatically
	  merged revision 796957: fix absolutely positioned static elements
	  being misplaced and drawn on top of the border when they are the
	  only child of a block.

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_form.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_form.h,
	  branches/KDE/4.0/kdelibs/khtml/css/html4.css: automatically
	  merged revision 796958: rework our form widgets' baselines to
	  align on the text content, rather than on the widget boundaries.
	  Iow: _______ _______ | | | | | ipsum | Lorem | ipsum | rather
	  than: Lorem |_______| |_______|

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_box.cpp,
	  branches/KDE/4.0/kdelibs/khtml/rendering/render_box.h:
	  automatically merged revision 796959: .Fix solving of percentage
	  height for positioned elements, in light of the nota bene in CSS
	  2.1-10.5. .Satisfyingly, the same code path can also be used to
	  solve percentage intrinsic height (10.6.2) - (#160371) .Fix some
	  errors in the implicit height code, that wasn't converting to
	  content height. BUG:160371

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_box.cpp:
	  automatically merged revision 799834: likewise, make sure we
	  always use the padding edge when calculating the width of a
	  positioned object's containing block. (10.1.4) BUG: 137606

2008-04-23 00:12 +0000 [r799979]  pino

	* branches/KDE/4.0/kdelibs/kate/syntax/data/debianchangelog.xml:
	  give a name to the default context

2008-04-23 09:05 +0000 [r800082]  lunakl

	* branches/KDE/4.0/kdelibs/kde3support/kdecore/k3tempfile.cpp: Do
	  not paranoidly sync every time, it causes I/O performance
	  problems for some users. People who still want it for whatever
	  reason like using XFS can set $KDE_EXTRA_FSYNC to 1.
	  (http://lists.kde.org/?l=kde-devel&m=120880682813170&w=2)

2008-04-23 22:07 +0000 [r800324]  neundorf

	* branches/KDE/4.0/kdelibs/cmake/modules/FindKDE4Internal.cmake,
	  branches/KDE/4.0/kdelibs/cmake/modules/KDE4Macros.cmake: -some
	  syncing with trunk: some lowercasing setting up cmake 2.6
	  policies, so it behaves cmake 2.4 compatible using the same flags
	  for icc as in trunk Alex

2008-04-24 13:59 +0000 [r800606]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/rendering/render_block.cpp:
	  automatically merged revision 800242:

2008-04-24 18:03 +0000 [r800713-800709]  orlovich

	* branches/KDE/4.0/kdelibs/khtml/xml/dom_textimpl.cpp: Include this
	  too for kjs/fb's WTF

	* branches/KDE/4.0/kdelibs/khtml/ecma/kjs_dom.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/CMakeLists.txt,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom2_eventsimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_stringimpl.h,
	  branches/KDE/4.0/kdelibs/khtml/dom/dom_node.cpp,
	  branches/KDE/4.0/kdelibs/khtml/ecma/kjs_window.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_docimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_nodeimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom2_eventsimpl.cpp,
	  branches/KDE/4.0/kdelibs/khtml/xml/dom_stringimpl.cpp: Fix
	  handling of events with custom names, while proving some
	  infrastructure that is likely to be useful along the line.. That
	  piece is the IDString template which simplies string<->ID mapping
	  when a chunk of IDs is pre-allocated, could potentially be used
	  for ID_ and ATTR_ as well BUG:133887

	* branches/KDE/4.0/kdelibs/khtml/misc/idstring.cpp (added),
	  branches/KDE/4.0/kdelibs/khtml/misc/idstring.h (added): Need to
	  add these 2 as well..

2008-04-24 21:09 +0000 [r800801]  dfaure

	* branches/KDE/4.0/kdelibs/kparts/tests/parttest.cpp,
	  branches/KDE/4.0/kdelibs/kparts/mainwindow.cpp,
	  branches/KDE/4.0/kdelibs/kparts/tests/parttest.h: Backport
	  800799: fix "hide a toolbar in konqueror, then switch tabs -> the
	  toolbar shows again (unless you waited for the autosave timer to
	  kick in)"

2008-04-25 16:38 +0000 [r801088]  lunakl

	* branches/KDE/4.0/kdelibs/kded/kbuildsycoca.cpp: Fix the
	  waiting-for-another-kbuildsycoca-to-finish check.

2008-04-25 23:58 +0000 [r801224]  mueller

	* branches/KDE/4.0/kdelibs/khtml/imload/decoders/pngloader.cpp: fix
	  buffer overflow (CVE-2008-1670) CCBUG: 156623

2008-04-27 14:23 +0000 [r801711]  mueller

	* branches/KDE/4.0/kdelibs/cmake/modules/KDE4Defaults.cmake:
	  prepare for KDE 4.0.4

2008-04-27 16:25 +0000 [r801748]  mueller

	* branches/KDE/4.0/kdelibs/khtml/html/html_formimpl.cpp: always
	  return something

2008-04-27 22:54 +0000 [r801859]  dfaure

	* branches/KDE/4.0/kdelibs/kdeui/dialogs/kedittoolbar_p.h,
	  branches/KDE/4.0/kdelibs/kdeui/tests/kwindowtest.cpp,
	  branches/KDE/4.0/kdelibs/kdeui/dialogs/kedittoolbar.cpp: Drag and
	  drop inside a list in kedittoolbar didn't not work properly
	  because it was handled by QListWidget directly, which didn't let
	  us handle our internal structures... (BUG 160290)

2008-04-30 08:31 +0000 [r802660]  lunakl

	* branches/KDE/4.0/kdelibs/kdesu/process.cpp: Really match only at
	  line starts.

2008-04-30 13:50 +0000 [r802754]  mueller

	* branches/KDE/4.0/kdelibs/kinit/kinit.cpp: reducing debug

