---
title: "KDE 4.5.2 Changelog"
hidden: true
---

  <h2>Changes in KDE 4.5.2</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_5_2/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdecore">kdecore</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix regression with extracting zip files (e.g. plasmoid packages). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=251192">251192</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1171340&amp;view=rev">1171340</a>. </li>
        <li class="bugfix crash">Fix crash in KSharedDataCache when using it after calling ::clear(). See SVN commit <a href="http://websvn.kde.org/?rev=1173903&amp;view=rev">1173903</a>. </li>
        <li class="bugfix minor">Wait up to 10 seconds for KSharedDataCache to lock the cache instead of failing immediately if the lock can't be taken. See SVN commit <a href="http://websvn.kde.org/?rev=1176224&amp;view=rev">1176224</a>. </li>
        <li class="bugfix crash">Fix possible corruption of a KSharedDataCache's index table if the very last page of the cache was in use during defragmentation. This could eventually lead to a crash or slowdown if KSharedDataCache noticed the corruption and regenerated. Possibly fixes bug 243573. See SVN commit <a href="http://websvn.kde.org/?rev=1180816&amp;view=rev">1180816</a>. </li>
        <li class="bugfix minor">Use a more reliable check for a cache-in-use with an incompatible version. See SVN commit <a href="http://websvn.kde.org/?rev=1180816&amp;view=rev">1180816</a>. </li>
      </ul>
      </div>
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Optimizations:</em><ul>
        <li class="optimize">Minimize I/O in KIconLoader by caching pixmaps from more sources. See SVN commit <a href="http://websvn.kde.org/?rev=1175076&amp;view=rev">1175076</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when an application requests icons without group and size (e.g. KVpnc). Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=246016">246016</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=248116">248116</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1180089&amp;view=rev">1180089</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_5_2/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix issue that an old preview might be shown in a tooltip. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=250000">250000</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1171568&amp;view=rev">1171568</a>. </li>
        <li class="bugfix normal">Fix issue that tooltip-labels might get clipped. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=241608">241608</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1180692&amp;view=rev">1180692</a>. </li>
        <li class="bugfix normal">Update the tab title when closing the second view in split view mode. See SVN commit <a href="http://websvn.kde.org/?rev=1177954&amp;view=rev">1177954</a>. </li>
        <li class="bugfix normal">Fix the problem that parts of a file name are replaced by "..." although there is enough space available. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=251121">251121</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1179569&amp;view=rev">1179569</a>. </li>
      </ul>
      </div>
      <h4><a name="plasma">plasma</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix behaviour of systemmonitor dataengine when it was connected to before it had populated the list of sources. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=251459">251459</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1177290&amp;view=rev">1177290</a>. </li>
      </ul>
      </div>
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Optimizations:</em><ul>
        <li class="optimize">Make the lanczos shader use a fixed number of iterations in the loop. See SVN commit <a href="http://websvn.kde.org/?rev=1175024&amp;view=rev">1175024</a>. </li>
        <li class="optimize">Disable Lanczos filter while moving windows in Present Windows effect. See SVN commit <a href="http://websvn.kde.org/?rev=1176111&amp;view=rev">1176111</a>. </li>
        <li class="optimize">Don't set gl_TexCoord[0] in the vertex shader when the fragment shader doesn't use it. See SVN commit <a href="http://websvn.kde.org/?rev=1178357&amp;view=rev">1178357</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Add sanity check to Desktop Grid Effect in case of pager layout and only one desktop. See SVN commit <a href="http://websvn.kde.org/?rev=1169040&amp;view=rev">1169040</a>. </li>
        <li class="bugfix crash">Desktops cannot be dragged into empty areas in Desktop Grid effect. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=248817">248817</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1169042&amp;view=rev">1169042</a>. </li>
        <li class="bugfix crash">Block keyboard input during animations in Desktop Grid effect. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=244813">244813</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1169047&amp;view=rev">1169047</a>. </li>
        <li class="bugfix ">Update text shadow information whenever Tabs are added/removed in Aurorae theme engine. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=248754">248754</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1169086&amp;view=rev">1169086</a>. </li>
        <li class="bugfix crash">Drop keyboard events while moving windows in Desktop Grid effect. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249325">249325</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1169145&amp;view=rev">1169145</a>. </li>
        <li class="bugfix ">Exclude all transformed windows from Blur effect, fixes rendering issues with e.g. Magic Lamp effect. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=243693">243693</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1169158&amp;view=rev">1169158</a>. </li>
        <li class="bugfix ">Allow moving of "unmovable" windows to another desktio if Present Windows mode is used in Desktop Grid effect. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249337">249337</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1169323&amp;view=rev">1169323</a>. </li>
        <li class="bugfix ">Fix window movment regression (e.g. Pack Windows functionality) in 4.5. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=241049">241049</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1169432&amp;view=rev">1169432</a>. </li>
        <li class="bugfix ">Add sanity checks for OpenGL limitations to Blur effect. See SVN commit <a href="http://websvn.kde.org/?rev=1179269&amp;view=rev">1179269</a>. </li>
        <li class="bugfix ">Fix non functional electric borders in Present Windows effect after closing a window. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=200235">200235</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1179522&amp;view=rev">1179522</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_5_2/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kde.org/applications/graphics/kolourpaint/" name="kolourpaint">KolourPaint</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when zooming in too far on large images. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=237936">237936</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1169574&amp;view=rev">1169574</a>. </li>
      </ul>
      </div>
      <h4><a href="http://okular.kde.org/" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Do not crash the print preview mode when the user tries to select an area. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249436">249436</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1170081&amp;view=rev">1170081</a>. </li>
        <li class="bugfix ">Restore the sidebar visibility correctly after a file reload. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249345">249345</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1171493&amp;view=rev">1171493</a>. </li>
        <li class="bugfix ">Workaround potential crash on some XPS files with some locales. See SVN commit <a href="http://websvn.kde.org/?rev=1173927&amp;view=rev">1173927</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_5_2/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/ark/" name="ark">Ark</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix compilation with Sun Studio. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249552">249552</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1170166&amp;view=rev">1170166</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when an unsupported encoding is selected in editor. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249260">249260</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1169546&amp;view=rev">1169546</a>. </li>
        <li class="bugfix normal">Key generation would create DSA/ElGamal key even if RSA was requested for languages where RSA is not translated as "RSA". See SVN commit <a href="http://websvn.kde.org/?rev=1173534&amp;view=rev">1173534</a>. </li>
        <li class="bugfix normal">Properly handle subkeys with multiple usages (usually RSA subkeys). See SVN commit <a href="http://websvn.kde.org/?rev=1173545&amp;view=rev">1173545</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/okteta/" name="okteta">Okteta</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Update the font used if the global font with fixed width is changed in System Settings - Fonts. See SVN commit <a href="http://websvn.kde.org/?rev=1171462&amp;view=rev">1171462</a>. </li>
        <li class="bugfix normal">Fix wrong calculation of needed width of offset column. See SVN commit <a href="http://websvn.kde.org/?rev=1171575&amp;view=rev">1171575</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_5_2/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/marble" name="marble">Marble</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix coordinate parsing of waypoints in .gpx files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249632">249632</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1170370&amp;view=rev">1170370</a>. </li>
        <li class="bugfix normal">Hide now invalid route summary after clearing a route. Prevent am/pm from turning up in estimated travel duration. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=250338">250338</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1172219&amp;view=rev">1172219</a>. </li>
        <li class="bugfix normal">Do not query gazetteer.openstreetmap.org anymore for searches. It will be offline or redirect to nominatim.openstreetmap.org soon and nominatim is included by Marble already. See SVN commit <a href="http://websvn.kde.org/?rev=1172281&amp;view=rev">1172281</a>. </li>
        <li class="bugfix normal">Write xml that validates against http://www.topografix.com/GPX/1/1/gpx.xsd (gpx export). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=251676">251676</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1179820&amp;view=rev">1179820</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_5_2/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kget">KGet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Optimizations:</em><ul>
        <li class="optimize">Caches mimetypes of files being downloaded, makes the view more responsiv. See SVN commit <a href="http://websvn.kde.org/?rev=1170130&amp;view=rev">1170130</a>. </li>
        <li class="optimize">Adds and removes multiple transfers at once, speeding up the process. See SVN commits <a href="http://websvn.kde.org/?rev=1170590&amp;view=rev">1170590</a>, <a href="http://websvn.kde.org/?rev=1173956&amp;view=rev">1173956</a>, <a href="http://websvn.kde.org/?rev=1173957&amp;view=rev">1173957</a> and <a href="http://websvn.kde.org/?rev=1173963&amp;view=rev">1173963</a>. </li>
        <li class="optimize">Adds multiple transfers to the SQLHistory, drastically reducing the io operations. See SVN commit <a href="http://websvn.kde.org/?rev=1173959&amp;view=rev">1173959</a>. </li>
        <li class="optimize">Speeds up starting/stopping multiple transfers by not constantly rescheduling. See SVN commit <a href="http://websvn.kde.org/?rev=1179182&amp;view=rev">1179182</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Do not show the "new transfer added" notification during startup process, since those aren't new transfers. See SVN commit <a href="http://websvn.kde.org/?rev=1173960&amp;view=rev">1173960</a>. </li>
        <li class="bugfix normal">Only creates the target folder if a download is started. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=251441">251441</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1177576&amp;view=rev">1177576</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeartwork"><a name="kdeartwork">kdeartwork</a><span class="allsvnchanges"> [ <a href="4_5_2/kdeartwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kde asciiquarium">KDE Asciiquarium</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix minor">Fix "Nessie" to not clear out the wave underneath her when traveling from left to right. Noted and fixed by Ryan Meldrum. See SVN commit <a href="http://websvn.kde.org/?rev=1181097&amp;view=rev">1181097</a>. </li>
      </ul>
      </div>
    </div>
