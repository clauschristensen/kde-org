2008-02-21 18:53 +0000 [r777841]  dhaumann

	* branches/KDE/3.5/kdeaddons/kate/tabbarextension/plugin_katetabbarextension.cpp,
	  branches/KDE/3.5/kdeaddons/kate/tabbarextension/plugin_katetabbarextension.h:
	  fix crash and this time also all mem-leaks ;) Fixed in KDE4
	  anyway. BUG: 157834

2008-04-07 22:17 +0000 [r794576]  mlaurent

	* branches/KDE/3.5/kdeaddons/konq-plugins/babelfish/plugin_babelfish.cpp:
	  Fix bug found in qt4 version

2008-06-25 12:09 +0000 [r824239]  anagl

	* branches/KDE/3.5/kdeaddons/atlantikdesigner/atlantikdesigner.desktop,
	  branches/KDE/3.5/kdeaddons/ksig/ksig.desktop: Desktop validation
	  fixes: remove deprecated entries for Encoding and MapNotify.

2008-06-29 22:38 +0000 [r826169]  anagl

	* branches/KDE/3.5/kdeaddons/konq-plugins/microformat/mf_konqmficon.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/minitools/minitoolsplugin.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/uninstall.desktop,
	  branches/KDE/3.5/kdeaddons/kate/kpybrowser/kpybrowser.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/babelfish/plugin_babelfish.desktop,
	  branches/KDE/3.5/kdeaddons/kate/snippets/katesnippets.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/autorefresh/autorefresh.desktop,
	  branches/KDE/3.5/kdeaddons/kicker-applets/math/mathapplet.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/domtreeviewer/plugin_domtreeviewer.desktop,
	  branches/KDE/3.5/kdeaddons/kate/filelistloader/katefll.desktop,
	  branches/KDE/3.5/kdeaddons/kfile-plugins/folder/kfile_folder.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/akregator/akregator_konqfeedicon.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/searchbar/searchbar.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/metabar.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/dirfilter/dirfilterplugin.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/ark_plugin.desktop,
	  branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/mediacontrol.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kuick_plugin.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/fsview/fsview_part.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/validators/plugin_validators.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/kimgalleryplugin/kimgalleryplugin.desktop,
	  branches/KDE/3.5/kdeaddons/kate/scripts/html-tidy.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/crashes/crashesplugin.desktop,
	  branches/KDE/3.5/kdeaddons/kaddressbook-plugins/xxports/kworldclock/geo_xxport.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kcmkuick/kcmkuick.desktop,
	  branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/kolourpicker.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/uachanger/uachangerplugin.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/newsticker/news_add.desktop,
	  branches/KDE/3.5/kdeaddons/kicker-applets/kbinaryclock/kbinaryclock.desktop,
	  branches/KDE/3.5/kdeaddons/kfile-plugins/desktop/kfile_desktop.desktop,
	  branches/KDE/3.5/kdeaddons/kfile-plugins/lnk/lnkforward.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/webarchiver/plugin_webarchiver.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/mediaplayer/mediaplayerplugin.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/delicious/delicious_add.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/webarchiver/webarchivethumbnail.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/rellinks/plugin_rellinks.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/mediaplayer/mplayer_add.desktop,
	  branches/KDE/3.5/kdeaddons/kfile-plugins/cert/kfile_cert.desktop,
	  branches/KDE/3.5/kdeaddons/kfile-plugins/lnk/x-win-lnk.desktop,
	  branches/KDE/3.5/kdeaddons/kfile-plugins/txt/kfile_txt.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kuickplugin.desktop,
	  branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/ktimemon.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/khtmlsettingsplugin/khtmlsettingsplugin.desktop,
	  branches/KDE/3.5/kdeaddons/kfile-plugins/mhtml/kfile_mhtml.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/sidebar/metabar/src/metabar_add.desktop,
	  branches/KDE/3.5/kdeaddons/kaddressbook-plugins/xxports/gmx/gmx_xxport.desktop,
	  branches/KDE/3.5/kdeaddons/kfile-plugins/lnk/kfile_lnk.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/akregator/akregator_konqplugin.desktop,
	  branches/KDE/3.5/kdeaddons/konq-plugins/fsview/fsview.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-08-11 13:19 +0000 [r845315]  bminisini

	* branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/Makefile.am,
	  branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/kolourpicker.cpp,
	  branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/kolourpicker.h:
	  Use simple buttons in the colour picker applet to support
	  transparent backgrounds. Draws a gray circle around the picked
	  colour, so that it is visible on a transparent background.

2008-08-11 13:27 +0000 [r845323-845321]  bminisini

	* branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/Makefile.am,
	  branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/mediacontrol.cpp,
	  branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/mcslider.cpp,
	  branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/mediacontrol.h,
	  branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/mcslider.h:
	  Support for transparency in the media control applet.

	* branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/simplebutton.h
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/simplebutton.cpp
	  (added): Forgot to add the simple button source files.

	* branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/simplebutton.cpp
	  (added),
	  branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/simplebutton.h
	  (added): Forgot to add the simple button source files there too.

2008-08-17 12:22 +0000 [r848330]  bminisini

	* branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/simplebutton.h,
	  branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/simplebutton.cpp,
	  branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/simplebutton.h,
	  branches/KDE/3.5/kdeaddons/kicker-applets/mediacontrol/simplebutton.cpp:
	  Update SimpleButton class in media control and color picker
	  applets.

2008-08-19 19:46 +0000 [r849592]  coolo

	* branches/KDE/3.5/kdeaddons/kdeaddons.lsm: update for 3.5.10

