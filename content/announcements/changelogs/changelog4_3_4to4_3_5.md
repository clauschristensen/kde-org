---
title: "KDE 4.3.5 Changelog"
hidden: true
---

  <h2>Changes in KDE 4.3.5</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_3_5/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix lack of auto-update when viewing a directory via a symlink. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=213799">213799</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1048852&amp;view=rev">1048852</a>. </li>
        <li class="bugfix ">Fix crash when changing the current directory's icon or permissions. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=190535">190535</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1061513&amp;view=rev">1061513</a> and <a href="http://websvn.kde.org/?rev=1061529&amp;view=rev">1061529</a>. </li>
        <li class="bugfix ">Fix crash after an error happens in a TransferJob. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=214100">214100</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=202091">202091</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=197289">197289</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=180791">180791</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1062999&amp;view=rev">1062999</a>. </li>
        <li class="bugfix ">Ignore proxy urls that don't have a protocol, fixes FTP not working on OpenSuse due to an old kioslaverc file. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=214896">214896</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1063293&amp;view=rev">1063293</a>. </li>
        <li class="bugfix ">Fix crash when expanding a linked directory (regression due to 213799; did not happen in any 4.3.x release). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=219547">219547</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1064817&amp;view=rev">1064817</a>, <a href="http://websvn.kde.org/?rev=1065291&amp;view=rev">1065291</a>, <a href="http://websvn.kde.org/?rev=1065297&amp;view=rev">1065297</a> and <a href="http://websvn.kde.org/?rev=1071530&amp;view=rev">1071530</a>. </li>
        <li class="bugfix ">Fix inotify regression with linux kernel 2.6.31/32, thanks to Eric Paris' help. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=207361">207361</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1065898&amp;view=rev">1065898</a>. </li>
      </ul>
      </div>
      <h4><a name="kfile">kfile</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix possible crash when unmounting a volume just after a file has been moved or copied to it. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=211525">211525</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1075313&amp;view=rev">1075313</a>. </li>
      </ul>
      </div>
      <h4><a name="kded">kded</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix kded using 100% CPU (in a phonon module). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202744">202744</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1075552&amp;view=rev">1075552</a>. </li>
      </ul>
      </div>
      <h4><a name="kbuildsycoca4">kbuildsycoca4</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash when some directories in /usr/share/mime don't have +x permission. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202871">202871</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1057320&amp;view=rev">1057320</a>. </li>
      </ul>
      </div>
      <h4><a name="nepomuk">nepomuk</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix possible crash in DBus when Nepomuk is running. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=208921">208921</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1075174&amp;view=rev">1075174</a>. </li>
      </ul>
      </div>
      <h4><a name="katepart">katepart</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Prevent crash when folding. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=200858">200858</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1077737&amp;view=rev">1077737</a>. </li>
        <li class="bugfix ">Prevent random crashes due to messed up internal data structure when cursor gets placed at end of document. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=200450">200450</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1077629&amp;view=rev">1077629</a>. </li>
        <li class="bugfix ">Fix printing when a line is split between 2 pages. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=219598">219598</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1069227&amp;view=rev">1069227</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_3_5/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix the problem that only two items can be selected via Shift+Arrow in the Details View if Qt 4.6 is used. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=217447">217447</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1076115&amp;view=rev">1076115</a>. </li>
      </ul>
      </div>
      <h4><a name="kdialog">kdialog</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Update parameter values of --geticon to that of KDE4. See SVN commit <a href="http://websvn.kde.org/?rev=1074947&amp;view=rev">1074947</a>. </li>
      </ul>
      </div>
      <h4><a name="konqueror">konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fixed statusbar rendering bug (multiple views visible) when restoring multiple tabs. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=169124">169124</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=158900">158900</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1057148&amp;view=rev">1057148</a>. </li>
        <li class="bugfix ">Don't let the tabbar "buttons" (in konq's sidebar) take focus, it's confusing (hidden) and annoying (many tab keypresses to get out). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=45557">45557</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1048570&amp;view=rev">1048570</a>. </li>
        <li class="bugfix ">Repair 'right click goes back in history' feature. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=168439">168439</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1050276&amp;view=rev">1050276</a>. </li>
        <li class="bugfix ">Fix crash on Ctrl+Tab. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203809">203809</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1076829&amp;view=rev">1076829</a>. </li>
        <li class="bugfix ">Repair 'Send File' so that it only sends the selected files, like in kde3. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=218388">218388</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1065134&amp;view=rev">1065134</a>. </li>
      </ul>
      </div>
      <h4><a name="keditbookmarks">keditbookmarks</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Save/restore the expanded/collapsed state of bookmark folders. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131127">131127</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1050729&amp;view=rev">1050729</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_3_5/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kopete.kde.org/" name="kopete">Kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash when generating an avatar from a webcam. See SVN commit <a href="http://websvn.kde.org/?rev=1066826&amp;view=rev">1066826</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_3_5/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix a bug causing JuK to crash on shutdown on some systems. See SVN commit <a href="http://websvn.kde.org/?rev=1064488&amp;view=rev">1064488</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_3_5/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix non-ASCII characters being corrupted in emails sent by KAlarm. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=222222">222222</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1077865&amp;view=rev">1077865</a>, <a href="http://websvn.kde.org/?rev=1078244&amp;view=rev">1078244</a>, <a href="http://websvn.kde.org/?rev=1078258&amp;view=rev">1078258</a> and <a href="http://websvn.kde.org/?rev=1078260&amp;view=rev">1078260</a>. </li>
      </ul>
      </div>
    </div>
