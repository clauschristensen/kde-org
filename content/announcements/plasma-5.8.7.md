---
title: KDE Plasma 5.8.7 LTS, Bugfix Release for May
release: "plasma-5.8.7"
version: "5.8.7 LTS"
description: KDE Ships Plasma 5.8.7.
date: 2017-05-23
layout: plasma
changelog: plasma-5.8.6-5.8.7-changelog
---

{{%youtube id="lm0sqqVcotA"%}}

{{<figure src="/announcements/plasma-5.9/plasma-5.9.png" alt="KDE Plasma 5.9 " class="text-center" width="600px" caption="KDE Plasma 5.9">}}

Tuesday, 23 May 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.8.7 LTS." %}}

{{% i18n_var `<a href="https://www.kde.org/announcements/plasma-%[1]s.0.php">Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience.` "5.8" "October" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "three months" %}}

- User Manager: Make sure the new avatar is always saved. <a href="https://commits.kde.org/user-manager/826e41429917f6c1534e84e8b7821b8b53675910">Commit.</a> Fixes bug <a href="https://bugs.kde.org/350836">#350836</a>. Phabricator Code review <a href="https://phabricator.kde.org/D5779">D5779</a>
- [Logout Screen] Show suspend button only if supported. <a href="https://commits.kde.org/plasma-workspace/8bc32846a5a41fa67c106045c43bb8c4af7e7e6f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/376601">#376601</a>
- [Weather] Fix term used for thunderstorm in bbcukmet data db. <a href="https://commits.kde.org/plasma-workspace/12a82fcba5672ee6b4473dfc6d0a84280a2bfbbb">Commit.</a>
