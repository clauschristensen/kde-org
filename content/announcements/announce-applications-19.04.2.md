---
title: KDE Ships KDE Applications 19.04.2
major_version: "19.04"
version: "19.04.2"
release: "applications-19.04.2"
description: KDE Ships Applications 19.04.2.
date: 2019-06-06
layout: application
changelog: fulllog_applications-19.04.2
---

June 06, 2019.

{{% i18n_var "Today KDE released the second stability update for <a href='%[1]s'>KDE Applications %[2]s</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-19.04.0" "19.04" %}}

Nearly fifty recorded bugfixes include improvements to Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular, Spectacle, among others.

Improvements include:

- A crash with viewing certain EPUB documents in Okular has been fixed
- Secret keys can again be exported from the Kleopatra cryptography manager
- The KAlarm event reminder no longer fails to start with newest PIM libraries
