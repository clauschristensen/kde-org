---
title: Release of KDE Frameworks 5.12.0
description: KDE Ships Frameworks 5.12.0
date: 2015-07-10
version: 5.12.0
layout: framework
---

{{<figure src="/announcements/frameworks5TP/KDE_QT.jpg" class="text-center" caption="Collaboration between Qt and KDE" >}}

July 10, 2015. KDE today announces the release of KDE Frameworks 5.12.0.

KDE Frameworks are 60 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see <a href='http://kde.org/announcements/kde-frameworks-5.0.php'>the Frameworks 5.0 release announcement</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### Extra CMake Modules

- Improve error reporting of query_qmake macro

### BluezQt

- Remove all devices from adapter before removing the adapter (bug 349363)
- Update links in README.md

### KActivities

- Adding the option not to track the user when in specific activities (similar to the 'private browsing' mode in a web browser)

### KArchive

- Preserve executable permissions from files in copyTo()
- Clarify ~KArchive by removing dead code.

### KAuth

- Make it possible to use kauth-policy-gen from different sources

### KBookmarks

- Don't add a bookmark with url is empty and text is empty
- Encode KBookmark URL to fix compatibility with KDE4 applications

### KCodecs

- Remove x-euc-tw prober

### KConfig

- Install kconfig_compiler into libexec
- New code generation option TranslationDomain=, for use with TranslationSystem=kde; normally needed in libraries.
- Make it possible to use kconfig_compiler from different sources

### KCoreAddons

- KDirWatch: Only establish a connection to FAM if requested
- Allow filtering plugins and applications by formfactor
- Make it possible to use desktoptojson from different sources

### KDBusAddons

- Clarify exit value for Unique instances

### KDeclarative

- Add QQC clone of KColorButton
- Assign a QmlObject for each kdeclarative instance when possible
- Make Qt.quit() from QML code work
- Merge branch 'mart/singleQmlEngineExperiment'
- Implement sizeHint based on implicitWidth/height
- Subclass of QmlObject with static engine

### KDELibs 4 Support

- Fix KMimeType::Ptr::isNull implementation.
- Reenable support for KDateTime streaming to kDebug/qDebug, for more SC
- Load correct translation catalog for kdebugdialog
- Don't skip documenting deprecated methods, so that people can read the porting hints

### KDESU

- Fix CMakeLists.txt to pass KDESU_USE_SUDO_DEFAULT to the compilation so it is used by suprocess.cpp

### KDocTools

- Update K5 docbook templates

### KGlobalAccel

- private runtime API gets installed to allow KWin to provide plugin for Wayland.
- Fallback for componentFriendlyForAction name resolving

### KIconThemes

- Don't try to paint the icon if the size is invalid

### KItemModels

- New proxy model: KRearrangeColumnsProxyModel. It supports reordering and hiding columns from the source model.

### KNotification

- Fix pixmap types in org.kde.StatusNotifierItem.xml
- [ksni] Add method to retrieve action by its name (bug 349513)

### KPeople

- Implement PersonsModel filtering facilities

### KPlotting

- KPlotWidget: add setAutoDeletePlotObjects, fix memory leak in replacePlotObject
- Fix missing tickmarks when x0 &gt; 0.
- KPlotWidget: no need to setMinimumSize or resize.

### KTextEditor

- debianchangelog.xml: add Debian/Stretch, Debian/Buster, Ubuntu-Wily
- Fix for UTF-16 surrogate pair backspace/delete behavior.
- Let QScrollBar handle the WheelEvents (bug 340936)
- Apply patch from KWrite devel top update pure basic HL, "Alexander Clay" &lt;tuireann@EpicBasic.org&gt;

### KTextWidgets

- Fix enable/disable ok button

### KWallet Framework

- Imported and improved the kwallet-query command-line tool.
- Support to overwrite maps entries.

### KXMLGUI

- Don't show "KDE Frameworks version" in the About KDE dialog

### Plasma Framework

- Make the dark theme completely dark, also the complementary group
- Cache naturalsize separately by scalefactor
- ContainmentView: Do not crash on an invalid corona metadata
- AppletQuickItem: Do not access KPluginInfo if not valid
- Fix occasional empty applet config pages (bug 349250)
- Improve hidpi support in the Calendar grid component
- Verify KService has valid plugin info before using it
- [calendar] Ensure the grid is repainted on theme changes
- [calendar] Always start counting weeks from Monday (bug 349044)
- [calendar] Repaint the grid when show week numbers setting changes
- An opaque theme is now used when only the blur effect is available (bug 348154)
- Whitelist applets/versions for separate engine
- Introduce a new class ContainmentView

### Sonnet

- Allow to use highlight spellchecking in a QPainTextEdit

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.

