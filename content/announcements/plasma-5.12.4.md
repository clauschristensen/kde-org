---
title: KDE Plasma 5.12.4, Bugfix Release for March
release: plasma-5.12.4
version: 5.12.4
description: KDE Ships 5.12.4
date: 2018-03-27
layout: plasma
changelog: plasma-5.12.3-5.12.4-changelog
---

{{%youtube id="xha6DJ_v1E4"%}}

{{<figure src="/announcements/plasma-5.12/plasma-5.12.png" alt="KDE Plasma 5.12 LTS Beta " class="text-center" width="600px" caption="KDE Plasma 5.12 LTS Beta">}}

Tuesday, 27 March 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.12.4." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.12" "March" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:" "a fortnight's" %}}

* Fix pixelated icon scaling for HiDPI screens. <a href="https://commits.kde.org/kmenuedit/e8e3c0f8e4a122ffc13386ab55b408aba2d29e14">Commit.</a> Fixes bug <a href="https://bugs.kde.org/390737">#390737</a>. Phabricator Code review <a href="https://phabricator.kde.org/D11631">D11631</a>
* Discover: Simplify the tasks view. <a href="https://commits.kde.org/discover/1d7b96ec879eb59107b915e52f31941d7480ef0e">Commit.</a> Fixes bug <a href="https://bugs.kde.org/391760">#391760</a>
* Move to KRunner's second results item with a single keypress. <a href="https://commits.kde.org/plasma-workspace/b8ffa755ed2df6bf6a47b9268e7da93355a4d856">Commit.</a> Fixes bug <a href="https://bugs.kde.org/392197">#392197</a>. Phabricator Code review <a href="https://phabricator.kde.org/D11611">D11611</a>
