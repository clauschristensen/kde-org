---
title: Plasma 5.10.1 Complete Changelog
version: 5.10.1
hidden: true
plasma: true
type: fulllog
---

### <a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a>

- Wizard: Use emblem-success and emblem-error icons. <a href='https://commits.kde.org/bluedevil/f3b2736856c09928a37bd2340409a69368466036'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5974'>D5974</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Fix typo. <a href='https://commits.kde.org/discover/88da02bb1db88ad3d8750806aacf0820493874b4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6110'>D6110</a>
- Qt 5.7 is needed, make it explicit when searching for Qt. <a href='https://commits.kde.org/discover/7c00f25625ed67145597f0887f59e2c3fb78d2ea'>Commit.</a>
- Change look of the close description button. <a href='https://commits.kde.org/discover/254eba55a2c2e50b4d8019b6810dc913650bf0f6'>Commit.</a>
- Rename the generic snap library to DiscoverSnapClient. <a href='https://commits.kde.org/discover/d3975092b9ea9aab00bf194bcf0820bbff5050a8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380356'>#380356</a>
- Don't elide the name, always elide the category label. <a href='https://commits.kde.org/discover/8a7e3bf4bccbd962c3eb3bcb69cf559333f599a4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378351'>#378351</a>
- Improve how we initialize json objects. <a href='https://commits.kde.org/discover/6ff9619c6c557e5b0684aa7bd1ed71632b23b088'>Commit.</a>
- Make the tasks page a global overlay rather than just over the pages. <a href='https://commits.kde.org/discover/34c8c4e6dba77879ae56ca0fb4d8affd8da6baac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378566'>#378566</a>
- Remove unused file. <a href='https://commits.kde.org/discover/5a705be5d0f1b98ed84894923a9044100a4c94b2'>Commit.</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Make QuickShare plasmoid compatible with Purpose 1.1. <a href='https://commits.kde.org/kdeplasma-addons/27efc1c2abc9c56b7161b77fc558fb77c591d4fe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380883'>#380883</a>
- Remove the moon. <a href='https://commits.kde.org/kdeplasma-addons/7cbad16e8a85053b0e40ee75da4deeeda3efd799'>Commit.</a>

### <a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a>

- Fixup protocol mismatch between greeter and kcheckpass. <a href='https://commits.kde.org/kscreenlocker/23fa33cedfa55cbac83bbdcc514b988d721552dc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380491'>#380491</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6091'>D6091</a>
- Fix detection of sys/event.h on FreeBSD < 12. <a href='https://commits.kde.org/kscreenlocker/51210aa52144b8c84daf4da50909bedd42926652'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6024'>D6024</a>
- Kcheckpass: Add support in for non-Linux platforms via kevent. <a href='https://commits.kde.org/kscreenlocker/c2fd8526c5238085f30169fb1891ba56ee1cbda8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5825'>D5825</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Fix Aurorae decorations with non integer DPI. <a href='https://commits.kde.org/kwin/5091feb8f60304a4a0cbb15a85f21f3d28b2e85f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380524'>#380524</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6099'>D6099</a>

### <a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a>

- Add setScale option to kscreendoctor. <a href='https://commits.kde.org/libkscreen/bede0b95f7e74d9b40126358c07d515c66926d98'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6026'>D6026</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Set 5.7 as minimum Qt version as it will not compile with anything less. <a href='https://commits.kde.org/plasma-desktop/5e4fc3b987c4cafa02daf0b93c51f47460fad535'>Commit.</a>
- Disable session restoration in kaccess. <a href='https://commits.kde.org/plasma-desktop/2bd5eb446cb2ed6dfac330eee8736fdb6939cb4b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5989'>D5989</a>
- Force KAccess to run under XCB. <a href='https://commits.kde.org/plasma-desktop/c75338d4a57cdc382830fafd946d2ed887a08bab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372597'>#372597</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6035'>D6035</a>
- Fixed crash when dropping files on desktop with KDeclarative from KDE Frameworks 5.35. <a href='https://commits.kde.org/plasma-desktop/77f1e675178ac995f7eb74c0410b5028ca1d74de'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380806'>#380806</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6088'>D6088</a>
- Add missing semicolon. <a href='https://commits.kde.org/plasma-desktop/601ad4077780470941cb1f1bdf7318904a0ce51a'>Commit.</a>
- React to changes in the size hint. <a href='https://commits.kde.org/plasma-desktop/200d5a73cbdb76a1b0df29c5cf502fd88b922542'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378443'>#378443</a>
- Application Launcher supports searching for power actions. <a href='https://commits.kde.org/plasma-desktop/d7376097887769986c0115d9a01bc7dc0c75c35a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6020'>D6020</a>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

- Don't ignore initially selected mime type filter. <a href='https://commits.kde.org/plasma-integration/267e7c635733031d2990e78637cf6c10a56f9f05'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5957'>D5957</a>
- Make sure we always set a default mime filter in save mode. <a href='https://commits.kde.org/plasma-integration/0dafb9403266d6064074250d44b74dc0db946cfb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5956'>D5956</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Restrict shared connections to permanent hardware address. <a href='https://commits.kde.org/plasma-nm/029e81701499b9b74222ec62eac1a30c80659a28'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380379'>#380379</a>
- Allow to have wider password dialog while preffering minimum size. <a href='https://commits.kde.org/plasma-nm/dd80a159855222c6f1d6aec183b91810d60bc3a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380399'>#380399</a>
- Openconnect: make sure we accept the dialog. <a href='https://commits.kde.org/plasma-nm/0ebb1756a5a6c2cb56032fffa6153e6283dc42b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380299'>#380299</a>
- Properly pass specific vpn type when selecting new connection by double click. <a href='https://commits.kde.org/plasma-nm/a3df0adb74eb6060f1925bfd523bda6314076a3a'>Commit.</a>
- Openconnect (juniper): Properly make sure we are compatible with the rest of nm tools. <a href='https://commits.kde.org/plasma-nm/fff8679d984910e59453e46ddf5cdbda14500f7b'>Commit.</a>
- Openconnect (juniper): Make sure we are compatible with the rest of nm tools. <a href='https://commits.kde.org/plasma-nm/e52c259c2e7ca52b27c23aef6c018c579600e4b2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380244'>#380244</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- Don't call setDefaultSink/Source when already default. <a href='https://commits.kde.org/plasma-pa/2509b6bbf507818ced15b219a693de07b11d5285'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380414'>#380414</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6055'>D6055</a>

### <a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a>

- Fix typo. <a href='https://commits.kde.org/plasma-sdk/f09242707226c01b005aa79d9dc5af52c10b6865'>Commit.</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Fix testing when the test has qml syntax errors. <a href='https://commits.kde.org/plasma-workspace/af026c3fa8f0bea1a614150d7b29b2101f7116cf'>Commit.</a>
- Fix variable name and logic. <a href='https://commits.kde.org/plasma-workspace/fe5fac90039ffaf3371b3360eee6ba5aa507f902'>Commit.</a>
- Only enable MPRIS global shortcuts on demand. <a href='https://commits.kde.org/plasma-workspace/7bd909fa3a4f70bf4c03c43b025f7ed65c2e5b5c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380526'>#380526</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6100'>D6100</a>
- Small fix on test helper. <a href='https://commits.kde.org/plasma-workspace/8cc06c36c769344bea561ee92da3f628068e25f3'>Commit.</a>
- [xembedsniproxy] Fix check whether to use XTest. <a href='https://commits.kde.org/plasma-workspace/7df184afa19f148c1cd09ae9588645bb2b4556fc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362941'>#362941</a>. Fixes bug <a href='https://bugs.kde.org/375017'>#375017</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6048'>D6048</a>
- Don't crash if we don't have a launcherTasksModel. <a href='https://commits.kde.org/plasma-workspace/3d506f00979bedbc68d5253db22e7fa0bfdbb45e'>Commit.</a>
- Don't show logout when kauthorized doesn't want it. <a href='https://commits.kde.org/plasma-workspace/67904e212c4fde5870015c3b2bc3af41e648eb6c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380344'>#380344</a>
- [TaskGroupingProxyModel] Use LauncherUrlWithoutIcon. <a href='https://commits.kde.org/plasma-workspace/8b39e1a5597681edec2daa3185b83725c4d0fbea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5581'>D5581</a>
- Always add data sources in SystemMonitorEngine::sourceRequestEvent(). <a href='https://commits.kde.org/plasma-workspace/f060ad5f1ad06359088887cb2f7e691c994ccfc5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380209'>#380209</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5973'>D5973</a>
