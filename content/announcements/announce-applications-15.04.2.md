---
title: KDE Ships KDE Applications 15.04.2
description: KDE Ships KDE Applications 15.04.2
date: 2015-06-02
version: 15.04.2
changelog: fulllog_applications-15.04.2
layout: application
---

{{% i18n_var "June 2, 2015. Today KDE released the second stability update for <a href='%[1]s'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-15.04.0" %}}

More than 30 recorded bugfixes include improvements to gwenview, kate, kdenlive, kdepim, konsole, marble, kgpg, kig, ktp-call-ui and umbrello.

{{% i18n_var "This release also includes Long Term Support versions of Plasma Workspaces %[1]s, KDE Development Platform %[2]s and the Kontact Suite %[2]s." "4.11.20" "4.14.9" %}}
