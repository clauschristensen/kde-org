---
title: KDE Plasma 5.7.3, bugfix Release for August
release: plasma-5.7.3
description: KDE Ships Plasma 5.7.3.
date: 2016-08-02
layout: plasma
changelog: plasma-5.7.2-5.7.3-changelog
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma-5.7/plasma-5.7.png" alt="KDE Plasma 5.7 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.7">}}

Tuesday, 2 August 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.7.3. <a href='https://www.kde.org/announcements/plasma-5.7.0.php'>Plasma 5.7</a> was released in July with many feature refinements and new modules to complete the desktop experience.

This release adds two weeks' worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fixed first time initialization. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=89cb478eb205e5586751311594f2dcf4ec447199">Commit.</a>
- Speed up loading KCM with FileDialog lazy loading. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=60e970472a5ed7cb3a7e58419fe42d80412a32d7">Commit.</a>
- Don't try to load layout before kactivitymanagerd starts. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f7ef6ee87b8957bebc976b6fc9e0df279cea05f1">Commit.</a>
