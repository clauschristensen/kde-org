---
title: KDE Plasma 5.8.2, Bugfix Release for October
release: plasma-5.8.2
description: KDE Ships Plasma 5.8.2.
date: 2016-10-18
layout: plasma
changelog: plasma-5.8.1-5.8.2-changelog
---

{{% youtube id="LgH1Clgr-uE" %}}

{{<figure src="/announcements/plasma-5.8/plasma-5.8.png" alt="KDE Plasma 5.8 " class="mt-4 text-center" width="600px" caption="KDE Plasma 5.8">}}

Tuesday, 18 October 2016.

Today KDE releases a Bugfix update to KDE Plasma 5, versioned 5.8.2. <a href='https://www.kde.org/announcements/plasma-5.8.0.php'>Plasma 5.8</a> was released in October with many feature refinements and new modules to complete the desktop experience.

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix 'Default' color scheme. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=613194c293b63004af5fc43762b92bd421ddf5b6">Commit.</a>
- Restore all panel properties. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=aea33cddb547cc2ba98be5dd45dc7562b32b4b9a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/368074">#368074</a>. Fixes bug <a href="https://bugs.kde.org/367918">#367918</a>
