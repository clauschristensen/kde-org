---
title: "Miscellaneous KDE Resources"
description: "Miscellaneous KDE resources"
---

- <a href="https://community.kde.org/Books">Books on KDE</a>
    <div style="margin: 10px;">You'd like to learn more about KDE, a specific application, or how to develop an application for this project? Then this page will lead you to the best books about those topics.</div>

- <a href="./clipart">KDE Clipart</a>
    <div style="margin: 10px;">Here you can find an almost complete list of major artwork used in the KDE project, from the official KDE and Plasma logo to the KDE mascots.</div>

- <a href="./metastore">KDE MetaStore</a>
    <div style="margin: 10px;">Here you can find links to various stores that provide products and merchandising related to the KDE project.</div>

- <a href="../code-of-conduct/">KDE Code of Conduct</a>
    <div style="margin: 10px;">Principles for interacting within our community.</div>

- <a href="../thanks">Thank Yous</a>
    <div style="margin: 10px;">Those who have helped KDE.</div>
