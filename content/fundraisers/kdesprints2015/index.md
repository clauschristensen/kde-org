---
title: "KDE Sprints 2015 fundraising"
date: 2015
---

€15191 raised of a €38500 goal

<div style="width: 100%; height: 30px; border: 1px solid #888; background: rgb(204,204,204);position: relative;"><div style="height: 30px; background: rgb(68,132,242); width: 39%"></div></div>
<br/>
<b>The Randa Meetings 2015 Fundraiser has finished. Thank you everybody who supported us in this fundraiser. We didn't reach the set goal but we collected quite some money and that means there will be more KDE Sprints thanks to your support! See <a href="http://planet.kde.org">http://planet.kde.org</a> for more information to come and go to the <a href="https://www.kde.org/community/donations/index.php">KDE donation page</a> if you want to support us further.</b>

<strong>[update]</strong> The fundraising campaign has been extended. We had intended to have ongoing updates from the Randa Meetings. However, developers were working rather than writing articles and blogposts. By extending the campaign, more information can be provided about what was actually accomplished.
For the sixth year, the KDE sprints in Randa, Switzerland will include key projects and top developers, working concurrently under one roof, free from noise and distractions.

The week-long Randa Meetings have been intense and productive, generating exceptional results. Expectations are high again this year. Most importantly, the Randa Meetings have produced <a href="http://dot.kde.org/2011/06/29/platform-frameworks-kde-hackers-meet-switzerland">significant breakthroughs</a> that benefit all KDE software users and developers.

Randa Meetings participants donate their time to work on important KDE projects. The Randa Meetings organizers prepare meals and arrange breaks to maintain energy and effectiveness. Volunteer staff people take care of details that can interfere with the highly productive software development.
While the developers cover some of their own costs, there are still hard expenses like accommodation and travel to get the volunteer contributors to Randa. Having these expenses covered is a valuable incentive for Randa developers.
The theme of the 2015 Randa Meetings is <strong>Bring Touch to KDE</strong>. Participants will be working to expand touch capabilities in KDE applications for tablets, smartphones and other touch-enabled devices. These efforts will augment Plasma Mobile, the Free Mobile Platform <a href="https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform">announced on July 25th</a> at <a href="https://akademy.kde.org/2015">Akademy 2015</a>.
Some of the 2015 Randa projects are:

- <a href="https://community.kde.org/KDEConnect">KDE Connect</a> integrated communication for multiple devices
- KDE apps on Android
- <a href="https://www.digikam.org">digiKam</a> photo management
- <a href="https://www.kde.org/applications/education/gcompris/">GCompris</a> educational software
- Interface design
- Personal Information Management
- <a href="https://www.kdenlive.org">Kdenlive</a> video editor
- KDE and mobile integration with <a href="https://owncloud.org">ownCloud</a> user-controlled cloud storage and applications
- Security and privacy

The organizers have included other top developers so that the Randa Meetings produce extraordinary results.

{{<figure src="/content/fundraisers/randameetings2014/randa1.jpg" class="float-right" width="400px">}}

This year, the Randa Meetings fundraising campaign has been expanded to include all KDE sprints. In addition to the Randa Meetings, various KDE work teams gather for a few days throughout the year to make exceptional progress that simply would not be possible working only via the Internet and email. As one long-time Randa participant said:

<blockquote>In <a href="https://en.wikipedia.org/wiki/Agile_software_development">agile development</a>, sprints are a very important element to push the project forward. While sprints can be done over the web, they are hindered by timezones, external distractions, availability of contributors, etc. In-person sprints are more productive as all the hindrances of the web meetings are eliminated and productivity is greatly enhanced overall.</blockquote>
The results of sprints are directly reflected in the quality of KDE software for users. Consequently, sprints and similar high impact get-togethers are one of 
the primary uses of funds within KDE. This means that the KDE Community consistently offers the best of Free and Open technology for anyone to use.

To support development sprints, we have set a fundraising goal of €38500. Would you make a donation to support KDE development? Any amount will help and is appreciated. <b>The fundraising campaign has been extended until September 30 so that the participants at the Randa Meetings can report on what they accomplished.</b>

Donors can choose to receive a <b>memorable thank you</b> from the Randa Meetings. If you want to receive such a thank you memento, please send an email to <a href="mailto:fundraiser@kde.org">fundraiser@kde.org</a>. Include your name and full mailing address.</i>

Links:

- <a href="https://sprints.kde.org">KDE Sprints webpage</a>
- <a href="http://www.randa-meetings.ch">Website of the Randa Meetings</a>
- <a href="http://sprints.kde.org/sprint/271">Participants and plans for the Randa Meetings 2015</a>
- <a href="http://community.kde.org/Sprints/Randa">More information about past and current Randa Meetings</a>

All money received during this fundraising campaign will be used for KDE sprints, including the Randa Meetings.
If you prefer to use international bank transfers please <a href="/community/donations/others.php">see this page</a>.<br />
Please write us <a href="mailto:randa@kde.org">an email</a> so we can add you to the list of donors manually.

### List of donations

<table class="table" width="100%">
  <tbody>
    <tr>
      <th width="20">No.</th>
      <th width="180">Date</th>
      <th width="80">Amount</th>
      <th style="text-align:left">Donor Name</th>
    </tr>
    <tr>
      <td align="center">453</td>
      <td align="center">30th September 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">452</td>
      <td align="center">27th September 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">451</td>
      <td align="center">26th September 2015</td>
      <td align="right">€&nbsp;70.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">450</td>
      <td align="center">23rd September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">449</td>
      <td align="center">23rd September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Malcolm Paul</td>
    </tr>
    <tr>
      <td align="center">448</td>
      <td align="center">20th September 2015</td>
      <td align="right">€&nbsp;1.00</td>
      <td>??????? ???????</td>
    </tr>
    <tr>
      <td align="center">447</td>
      <td align="center">19th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">446</td>
      <td align="center">18th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">445</td>
      <td align="center">18th September 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">444</td>
      <td align="center">17th September 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">443</td>
      <td align="center">17th September 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Marius Steffen</td>
    </tr>
    <tr>
      <td align="center">442</td>
      <td align="center">17th September 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">441</td>
      <td align="center">17th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Kjetil Nes</td>
    </tr>
    <tr>
      <td align="center">440</td>
      <td align="center">17th September 2015</td>
      <td align="right">€&nbsp;24.00</td>
      <td>Jan Malik</td>
    </tr>
    <tr>
      <td align="center">437</td>
      <td align="center">15th September 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">436</td>
      <td align="center">15th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Alexander Louis Schlarb</td>
    </tr>
    <tr>
      <td align="center">435</td>
      <td align="center">15th September 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">454</td>
      <td align="center">15th September 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Jean-Jacques Finazzi</td>
    </tr>
    <tr>
      <td align="center">434</td>
      <td align="center">13th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Axel Dittrich</td>
    </tr>
    <tr>
      <td align="center">433</td>
      <td align="center">12th September 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Bartosz Bednarski</td>
    </tr>
    <tr>
      <td align="center">432</td>
      <td align="center">12th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Martin Steigerwald</td>
    </tr>
    <tr>
      <td align="center">439</td>
      <td align="center">12th September 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Wade Olson</td>
    </tr>
    <tr>
      <td align="center">431</td>
      <td align="center">12th September 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Sebastian Fleer</td>
    </tr>
    <tr>
      <td align="center">430</td>
      <td align="center">11th September 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">429</td>
      <td align="center">11th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Peter Danek</td>
    </tr>
    <tr>
      <td align="center">428</td>
      <td align="center">10th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Stephan Theelke</td>
    </tr>
    <tr>
      <td align="center">427</td>
      <td align="center">10th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">426</td>
      <td align="center">10th September 2015</td>
      <td align="right">€&nbsp;42.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">425</td>
      <td align="center">10th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Mark Murphy</td>
    </tr>
    <tr>
      <td align="center">424</td>
      <td align="center">10th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Andrea Cavaliero</td>
    </tr>
    <tr>
      <td align="center">423</td>
      <td align="center">10th September 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">422</td>
      <td align="center">9th September 2015</td>
      <td align="right">€&nbsp;80.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">421</td>
      <td align="center">9th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">420</td>
      <td align="center">9th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Alexander Schmiechen</td>
    </tr>
    <tr>
      <td align="center">419</td>
      <td align="center">9th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Bart Otten</td>
    </tr>
    <tr>
      <td align="center">418</td>
      <td align="center">8th September 2015</td>
      <td align="right">€&nbsp;60.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">417</td>
      <td align="center">8th September 2015</td>
      <td align="right">€&nbsp;60.00</td>
      <td>Joan Maspons</td>
    </tr>
    <tr>
      <td align="center">416</td>
      <td align="center">8th September 2015</td>
      <td align="right">€&nbsp;2.00</td>
      <td>Federico Bacci</td>
    </tr>
    <tr>
      <td align="center">415</td>
      <td align="center">8th September 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">414</td>
      <td align="center">8th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Andreas Erhart</td>
    </tr>
    <tr>
      <td align="center">413</td>
      <td align="center">7th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">412</td>
      <td align="center">7th September 2015</td>
      <td align="right">€&nbsp;6.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">411</td>
      <td align="center">6th September 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Ivan Radic</td>
    </tr>
    <tr>
      <td align="center">410</td>
      <td align="center">6th September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Jasem Mutlaq</td>
    </tr>
    <tr>
      <td align="center">409</td>
      <td align="center">6th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">408</td>
      <td align="center">6th September 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">407</td>
      <td align="center">6th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">406</td>
      <td align="center">6th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Teemu Erkkola</td>
    </tr>
    <tr>
      <td align="center">405</td>
      <td align="center">5th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Julien Nguyen</td>
    </tr>
    <tr>
      <td align="center">404</td>
      <td align="center">5th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">403</td>
      <td align="center">4th September 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">402</td>
      <td align="center">4th September 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Mike Petersen Odania IT</td>
    </tr>
    <tr>
      <td align="center">401</td>
      <td align="center">4th September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jan Krings</td>
    </tr>
    <tr>
      <td align="center">400</td>
      <td align="center">4th September 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Petre Tudor</td>
    </tr>
    <tr>
      <td align="center">399</td>
      <td align="center">4th September 2015</td>
      <td align="right">€&nbsp;4.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">398</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;4.00</td>
      <td>A Anton</td>
    </tr>
    <tr>
      <td align="center">397</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;1.11</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">396</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">395</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">394</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Wolnik Dawid</td>
    </tr>
    <tr>
      <td align="center">393</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Kjetil Koksvik</td>
    </tr>
    <tr>
      <td align="center">392</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">391</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;90.00</td>
      <td>Darin Miller</td>
    </tr>
    <tr>
      <td align="center">390</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jan Wiele</td>
    </tr>
    <tr>
      <td align="center">438</td>
      <td align="center">3rd September 2015</td>
      <td align="right">€&nbsp;300.00</td>
      <td>Nicolas Frattaroli</td>
    </tr>
    <tr>
      <td align="center">389</td>
      <td align="center">2nd September 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Sebastian Nellen</td>
    </tr>
    <tr>
      <td align="center">388</td>
      <td align="center">2nd September 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>David Ross</td>
    </tr>
    <tr>
      <td align="center">387</td>
      <td align="center">2nd September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>soenke jensen</td>
    </tr>
    <tr>
      <td align="center">386</td>
      <td align="center">2nd September 2015</td>
      <td align="right">€&nbsp;90.00</td>
      <td>Jeff Massie</td>
    </tr>
    <tr>
      <td align="center">385</td>
      <td align="center">2nd September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Alexander Wauck</td>
    </tr>
    <tr>
      <td align="center">384</td>
      <td align="center">2nd September 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Erik Fleischer</td>
    </tr>
    <tr>
      <td align="center">383</td>
      <td align="center">1st September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">382</td>
      <td align="center">1st September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">381</td>
      <td align="center">1st September 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Fahad Alduraibi</td>
    </tr>
    <tr>
      <td align="center">379</td>
      <td align="center">1st September 2015</td>
      <td align="right">€&nbsp;8.00</td>
      <td>ROGER ARAUJO</td>
    </tr>
    <tr>
      <td align="center">378</td>
      <td align="center">1st September 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">377</td>
      <td align="center">1st September 2015</td>
      <td align="right">€&nbsp;67.89</td>
      <td>Sune Vuorela</td>
    </tr>
    <tr>
      <td align="center">376</td>
      <td align="center">1st September 2015</td>
      <td align="right">€&nbsp;54.00</td>
      <td>Caleb Johnson</td>
    </tr>
    <tr>
      <td align="center">375</td>
      <td align="center">1st September 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">374</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">373</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">372</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Cristian Adam</td>
    </tr>
    <tr>
      <td align="center">371</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">370</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;60.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">369</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sergiy Kachanyuk</td>
    </tr>
    <tr>
      <td align="center">368</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">367</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">366</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Martin Cantieni</td>
    </tr>
    <tr>
      <td align="center">365</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">364</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Christian Nobis</td>
    </tr>
    <tr>
      <td align="center">363</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Peter Emmerling</td>
    </tr>
    <tr>
      <td align="center">362</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">455</td>
      <td align="center">31st August 2015</td>
      <td align="right">€&nbsp;27.00</td>
      <td>Leo Barichello</td>
    </tr>
    <tr>
      <td align="center">361</td>
      <td align="center">30th August 2015</td>
      <td align="right">€&nbsp;22.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">360</td>
      <td align="center">30th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Herve LE ROY</td>
    </tr>
    <tr>
      <td align="center">359</td>
      <td align="center">30th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Peter Dorman</td>
    </tr>
    <tr>
      <td align="center">358</td>
      <td align="center">30th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Michael Hall</td>
    </tr>
    <tr>
      <td align="center">357</td>
      <td align="center">30th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">356</td>
      <td align="center">30th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">355</td>
      <td align="center">30th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Santiago Aguirre Erauso</td>
    </tr>
    <tr>
      <td align="center">354</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>shishir verma</td>
    </tr>
    <tr>
      <td align="center">353</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sam Muirhead</td>
    </tr>
    <tr>
      <td align="center">352</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Stefanos Charchalakis</td>
    </tr>
    <tr>
      <td align="center">351</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">350</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Xavier Corredor Llano</td>
    </tr>
    <tr>
      <td align="center">349</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Heiko Philippin</td>
    </tr>
    <tr>
      <td align="center">348</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Xavier Vello</td>
    </tr>
    <tr>
      <td align="center">347</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;8.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">346</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Thomas Brix Larsen</td>
    </tr>
    <tr>
      <td align="center">345</td>
      <td align="center">29th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Scott Aiton</td>
    </tr>
    <tr>
      <td align="center">344</td>
      <td align="center">28th August 2015</td>
      <td align="right">€&nbsp;4.00</td>
      <td>Raffaele Barone</td>
    </tr>
    <tr>
      <td align="center">343</td>
      <td align="center">28th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Javier Jardon</td>
    </tr>
    <tr>
      <td align="center">342</td>
      <td align="center">28th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Shantanu Tushar</td>
    </tr>
    <tr>
      <td align="center">341</td>
      <td align="center">28th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Leonardo Salerno</td>
    </tr>
    <tr>
      <td align="center">340</td>
      <td align="center">28th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">339</td>
      <td align="center">28th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sandeep Raghuraman</td>
    </tr>
    <tr>
      <td align="center">338</td>
      <td align="center">28th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">337</td>
      <td align="center">28th August 2015</td>
      <td align="right">€&nbsp;1.00</td>
      <td>Aram Grigoryan</td>
    </tr>
    <tr>
      <td align="center">336</td>
      <td align="center">28th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Steffen Ullrich</td>
    </tr>
    <tr>
      <td align="center">380</td>
      <td align="center">27th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>John Byrnes</td>
    </tr>
    <tr>
      <td align="center">335</td>
      <td align="center">27th August 2015</td>
      <td align="right">€&nbsp;70.00</td>
      <td>Ian Stanistreet</td>
    </tr>
    <tr>
      <td align="center">334</td>
      <td align="center">27th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Ivan Onyshchuk</td>
    </tr>
    <tr>
      <td align="center">333</td>
      <td align="center">27th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Marco Cimmino</td>
    </tr>
    <tr>
      <td align="center">332</td>
      <td align="center">27th August 2015</td>
      <td align="right">€&nbsp;23.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">331</td>
      <td align="center">27th August 2015</td>
      <td align="right">€&nbsp;2.00</td>
      <td>Silviu Marin-Caea</td>
    </tr>
    <tr>
      <td align="center">330</td>
      <td align="center">27th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Mykhaylo Avdonin</td>
    </tr>
    <tr>
      <td align="center">329</td>
      <td align="center">27th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Martin Bednar</td>
    </tr>
    <tr>
      <td align="center">328</td>
      <td align="center">27th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Anthony RENOUX</td>
    </tr>
    <tr>
      <td align="center">327</td>
      <td align="center">26th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Denis Zimmermann</td>
    </tr>
    <tr>
      <td align="center">326</td>
      <td align="center">26th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>matthias gatto</td>
    </tr>
    <tr>
      <td align="center">325</td>
      <td align="center">26th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">324</td>
      <td align="center">26th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Silvan Jegen</td>
    </tr>
    <tr>
      <td align="center">323</td>
      <td align="center">26th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">322</td>
      <td align="center">26th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Jan Schnackenberg</td>
    </tr>
    <tr>
      <td align="center">321</td>
      <td align="center">25th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Clive Johnston</td>
    </tr>
    <tr>
      <td align="center">320</td>
      <td align="center">25th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Arthur De Kimpe</td>
    </tr>
    <tr>
      <td align="center">319</td>
      <td align="center">25th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Merike Sell</td>
    </tr>
    <tr>
      <td align="center">318</td>
      <td align="center">25th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">317</td>
      <td align="center">25th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Fabien Valthier</td>
    </tr>
    <tr>
      <td align="center">316</td>
      <td align="center">25th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Carl Gressum</td>
    </tr>
    <tr>
      <td align="center">315</td>
      <td align="center">25th August 2015</td>
      <td align="right">€&nbsp;200.00</td>
      <td>Justinas Petravi?ius</td>
    </tr>
    <tr>
      <td align="center">314</td>
      <td align="center">25th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Carl Atnip</td>
    </tr>
    <tr>
      <td align="center">313</td>
      <td align="center">25th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Lukas Spies</td>
    </tr>
    <tr>
      <td align="center">312</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Leonardo Ferraguzzi</td>
    </tr>
    <tr>
      <td align="center">311</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Jean PRUNNEAUX</td>
    </tr>
    <tr>
      <td align="center">310</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Gauthier Guerin</td>
    </tr>
    <tr>
      <td align="center">309</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Andreas Keller</td>
    </tr>
    <tr>
      <td align="center">308</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Thomas Gufler</td>
    </tr>
    <tr>
      <td align="center">307</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;57.38</td>
      <td>Albert Astals Cid</td>
    </tr>
    <tr>
      <td align="center">306</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jose Millan Soto</td>
    </tr>
    <tr>
      <td align="center">305</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Kieran McCusker</td>
    </tr>
    <tr>
      <td align="center">304</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Torsten Bielen</td>
    </tr>
    <tr>
      <td align="center">303</td>
      <td align="center">24th August 2015</td>
      <td align="right">€&nbsp;9.00</td>
      <td>Adam Geddis</td>
    </tr>
    <tr>
      <td align="center">302</td>
      <td align="center">23rd August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">301</td>
      <td align="center">23rd August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Michael Abrahams</td>
    </tr>
    <tr>
      <td align="center">300</td>
      <td align="center">23rd August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Nicolas GANDRIAU</td>
    </tr>
    <tr>
      <td align="center">299</td>
      <td align="center">23rd August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Thomas Vergnaud</td>
    </tr>
    <tr>
      <td align="center">298</td>
      <td align="center">23rd August 2015</td>
      <td align="right">€&nbsp;87.00</td>
      <td>Kurt Hindenburg</td>
    </tr>
    <tr>
      <td align="center">297</td>
      <td align="center">23rd August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Southern Web Developer</td>
    </tr>
    <tr>
      <td align="center">296</td>
      <td align="center">23rd August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>? ??</td>
    </tr>
    <tr>
      <td align="center">295</td>
      <td align="center">23rd August 2015</td>
      <td align="right">€&nbsp;123.00</td>
      <td>Marc-Andre Beck</td>
    </tr>
    <tr>
      <td align="center">294</td>
      <td align="center">23rd August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Adriano Fantini</td>
    </tr>
    <tr>
      <td align="center">293</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;7.00</td>
      <td>Marios Andreopoulos</td>
    </tr>
    <tr>
      <td align="center">292</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Elvis Angelaccio</td>
    </tr>
    <tr>
      <td align="center">291</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">290</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;22.00</td>
      <td>James Teitsworth</td>
    </tr>
    <tr>
      <td align="center">289</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">288</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Manfred Mislik</td>
    </tr>
    <tr>
      <td align="center">287</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">286</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">285</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">284</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">283</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">282</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;88.00</td>
      <td>Andrew Fisher</td>
    </tr>
    <tr>
      <td align="center">281</td>
      <td align="center">22nd August 2015</td>
      <td align="right">€&nbsp;23.00</td>
      <td>Ovidiu Bogdan</td>
    </tr>
    <tr>
      <td align="center">280</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Hugues Berthaut</td>
    </tr>
    <tr>
      <td align="center">279</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Andreas Lauser</td>
    </tr>
    <tr>
      <td align="center">278</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>tanguy salmon</td>
    </tr>
    <tr>
      <td align="center">277</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Markus Mauder</td>
    </tr>
    <tr>
      <td align="center">276</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">275</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">274</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">273</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Fernando Moreira</td>
    </tr>
    <tr>
      <td align="center">272</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">271</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Santhosh T R</td>
    </tr>
    <tr>
      <td align="center">270</td>
      <td align="center">21st August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">269</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Alexandre Moore</td>
    </tr>
    <tr>
      <td align="center">268</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">267</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sebastien Morenne</td>
    </tr>
    <tr>
      <td align="center">266</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">265</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;37.62</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">264</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Sylvain Leterreur</td>
    </tr>
    <tr>
      <td align="center">263</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">262</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;1.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">261</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;60.00</td>
      <td>Yves Mermoud</td>
    </tr>
    <tr>
      <td align="center">260</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Jan-Matthias Braun</td>
    </tr>
    <tr>
      <td align="center">259</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Tuukka Verho</td>
    </tr>
    <tr>
      <td align="center">258</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;21.63</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">257</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">256</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">255</td>
      <td align="center">20th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">254</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;13.37</td>
      <td>Beder Andreas</td>
    </tr>
    <tr>
      <td align="center">253</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Linus Seelinger</td>
    </tr>
    <tr>
      <td align="center">252</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">251</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">250</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Rolf Gottschling</td>
    </tr>
    <tr>
      <td align="center">249</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Arne Brix</td>
    </tr>
    <tr>
      <td align="center">248</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">247</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Robin Appelman</td>
    </tr>
    <tr>
      <td align="center">246</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;1.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">245</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">244</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Pozzi Diego</td>
    </tr>
    <tr>
      <td align="center">243</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Vojtech Trefny</td>
    </tr>
    <tr>
      <td align="center">242</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">241</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Marcel Meyer</td>
    </tr>
    <tr>
      <td align="center">240</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">239</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Luciano Montanaro</td>
    </tr>
    <tr>
      <td align="center">238</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">237</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Patrick Rifici</td>
    </tr>
    <tr>
      <td align="center">236</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jakub Dvorak</td>
    </tr>
    <tr>
      <td align="center">235</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">234</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Kevin Whitaker</td>
    </tr>
    <tr>
      <td align="center">233</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;9.00</td>
      <td>Ezequiel Coelho</td>
    </tr>
    <tr>
      <td align="center">232</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Doug McMillan</td>
    </tr>
    <tr>
      <td align="center">456</td>
      <td align="center">19th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Frank Dornheim FDo</td>
    </tr>
    <tr>
      <td align="center">231</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Thiago Bauermann</td>
    </tr>
    <tr>
      <td align="center">230</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Franck Moreau</td>
    </tr>
    <tr>
      <td align="center">229</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Alessandro do Nascimento Marques</td>
    </tr>
    <tr>
      <td align="center">228</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Dominic Lyons</td>
    </tr>
    <tr>
      <td align="center">227</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jochen Hoff</td>
    </tr>
    <tr>
      <td align="center">226</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Matthieu Moy</td>
    </tr>
    <tr>
      <td align="center">225</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">224</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">223</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">222</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Vicente Salvador Cubedo</td>
    </tr>
    <tr>
      <td align="center">221</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Emanuele Tartaglia</td>
    </tr>
    <tr>
      <td align="center">220</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Kevin O'Brien</td>
    </tr>
    <tr>
      <td align="center">219</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">218</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Fabian Bornschein</td>
    </tr>
    <tr>
      <td align="center">217</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Michael Patris</td>
    </tr>
    <tr>
      <td align="center">216</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;12.00</td>
      <td>carlos pinto</td>
    </tr>
    <tr>
      <td align="center">215</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;200.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">214</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Martin Riethmayer</td>
    </tr>
    <tr>
      <td align="center">213</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Victor Bouvier-Deleau</td>
    </tr>
    <tr>
      <td align="center">212</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">211</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">210</td>
      <td align="center">18th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">209</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Guillaume ZITTA</td>
    </tr>
    <tr>
      <td align="center">208</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">207</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">206</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">205</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Wolfgang Mader</td>
    </tr>
    <tr>
      <td align="center">204</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">203</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">202</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">201</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Marcus Lucke</td>
    </tr>
    <tr>
      <td align="center">200</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Ronny Standtke</td>
    </tr>
    <tr>
      <td align="center">199</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">198</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Janne Aho</td>
    </tr>
    <tr>
      <td align="center">197</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">196</td>
      <td align="center">17th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>pascal d'Hermilly</td>
    </tr>
    <tr>
      <td align="center">195</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jim Cochrane</td>
    </tr>
    <tr>
      <td align="center">194</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;77.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">193</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">192</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Christian Gmeiner</td>
    </tr>
    <tr>
      <td align="center">191</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">190</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Jakub Benda</td>
    </tr>
    <tr>
      <td align="center">189</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">188</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Xavier Francisco Gilabert</td>
    </tr>
    <tr>
      <td align="center">187</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;150.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">186</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Oliver Nautsch</td>
    </tr>
    <tr>
      <td align="center">185</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Jan Dolstra</td>
    </tr>
    <tr>
      <td align="center">184</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Markus Falkner</td>
    </tr>
    <tr>
      <td align="center">183</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>sacha schutz</td>
    </tr>
    <tr>
      <td align="center">182</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">181</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">180</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">179</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;2.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">178</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Adrian Popirlan</td>
    </tr>
    <tr>
      <td align="center">177</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">176</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">175</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Robert Kratky</td>
    </tr>
    <tr>
      <td align="center">174</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">173</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">172</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Mahendra Tallur</td>
    </tr>
    <tr>
      <td align="center">171</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">170</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Sung Jae Cho</td>
    </tr>
    <tr>
      <td align="center">169</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Roman Gilg</td>
    </tr>
    <tr>
      <td align="center">168</td>
      <td align="center">16th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">167</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Stefan Schoenberger</td>
    </tr>
    <tr>
      <td align="center">166</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>William Thorup</td>
    </tr>
    <tr>
      <td align="center">165</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Peter Nirschl</td>
    </tr>
    <tr>
      <td align="center">164</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;0.20</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">163</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>stephen donovan</td>
    </tr>
    <tr>
      <td align="center">162</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>John Cleveland</td>
    </tr>
    <tr>
      <td align="center">161</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">158</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">157</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">156</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Philipp Stefan</td>
    </tr>
    <tr>
      <td align="center">155</td>
      <td align="center">15th August 2015</td>
      <td align="right">€&nbsp;18.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">154</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">153</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">152</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Anderson de Arruda Casimiro</td>
    </tr>
    <tr>
      <td align="center">151</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">150</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Philippe Cattin</td>
    </tr>
    <tr>
      <td align="center">149</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">148</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Jaroslav Reznik</td>
    </tr>
    <tr>
      <td align="center">147</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Oliver Pabst</td>
    </tr>
    <tr>
      <td align="center">146</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">145</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;59.00</td>
      <td>Ariel Rosenfeld</td>
    </tr>
    <tr>
      <td align="center">144</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Martin Gysel</td>
    </tr>
    <tr>
      <td align="center">143</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">142</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Ulrich Bogensperger</td>
    </tr>
    <tr>
      <td align="center">141</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;6.80</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">140</td>
      <td align="center">14th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">139</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>chris fudge</td>
    </tr>
    <tr>
      <td align="center">138</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Josef Filzmaier</td>
    </tr>
    <tr>
      <td align="center">137</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Thomas Schyschka</td>
    </tr>
    <tr>
      <td align="center">136</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Yuen Hoe Lim</td>
    </tr>
    <tr>
      <td align="center">135</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">134</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Alfred Panzer</td>
    </tr>
    <tr>
      <td align="center">133</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Heiko Geier</td>
    </tr>
    <tr>
      <td align="center">132</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Bernhard Friedreich</td>
    </tr>
    <tr>
      <td align="center">131</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Andreas Neumann</td>
    </tr>
    <tr>
      <td align="center">130</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Jonathan Foster</td>
    </tr>
    <tr>
      <td align="center">129</td>
      <td align="center">13th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Peter Kurtenacker</td>
    </tr>
    <tr>
      <td align="center">128</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Bernhard Geier</td>
    </tr>
    <tr>
      <td align="center">127</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">126</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;60.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">125</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Yevgeniy Melnichuk</td>
    </tr>
    <tr>
      <td align="center">124</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>MR JONATHAN WOOLLARD</td>
    </tr>
    <tr>
      <td align="center">123</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">122</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">121</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;90.00</td>
      <td>Peter Hartmann</td>
    </tr>
    <tr>
      <td align="center">120</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Franz Pammer</td>
    </tr>
    <tr>
      <td align="center">119</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Christoph Langbein</td>
    </tr>
    <tr>
      <td align="center">118</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Florian Bauer</td>
    </tr>
    <tr>
      <td align="center">117</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Max Bruckner</td>
    </tr>
    <tr>
      <td align="center">116</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Christoph Hamann</td>
    </tr>
    <tr>
      <td align="center">115</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Christian Paulsen</td>
    </tr>
    <tr>
      <td align="center">114</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Sascha Zaluskowski</td>
    </tr>
    <tr>
      <td align="center">113</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Erwin Burema</td>
    </tr>
    <tr>
      <td align="center">112</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;80.00</td>
      <td>Michael Schier</td>
    </tr>
    <tr>
      <td align="center">111</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Jesse DuBord</td>
    </tr>
    <tr>
      <td align="center">110</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Jens Rauch</td>
    </tr>
    <tr>
      <td align="center">109</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">108</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Pascal Mages</td>
    </tr>
    <tr>
      <td align="center">107</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Sebastian Mueller</td>
    </tr>
    <tr>
      <td align="center">106</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">105</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">104</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">103</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">102</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Jan-Philipp Schulze</td>
    </tr>
    <tr>
      <td align="center">101</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Christopher Marra</td>
    </tr>
    <tr>
      <td align="center">100</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">99</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">98</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Bodo Hinueber</td>
    </tr>
    <tr>
      <td align="center">97</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Johannes Felko</td>
    </tr>
    <tr>
      <td align="center">96</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">95</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Alfred Fuss</td>
    </tr>
    <tr>
      <td align="center">94</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Gottfried Mueller</td>
    </tr>
    <tr>
      <td align="center">93</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Martin Aschenbrenner</td>
    </tr>
    <tr>
      <td align="center">92</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">91</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">90</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">89</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Harald Fernengel</td>
    </tr>
    <tr>
      <td align="center">88</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Godber Kraas</td>
    </tr>
    <tr>
      <td align="center">87</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Christian Hartmann</td>
    </tr>
    <tr>
      <td align="center">86</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Johannes Jensen</td>
    </tr>
    <tr>
      <td align="center">85</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Heiko Gimbel</td>
    </tr>
    <tr>
      <td align="center">84</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">83</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Lindsay Steele</td>
    </tr>
    <tr>
      <td align="center">159</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;385.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">160</td>
      <td align="center">12th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">82</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>daniele scasciafratte</td>
    </tr>
    <tr>
      <td align="center">81</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">80</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Dejan Noveski</td>
    </tr>
    <tr>
      <td align="center">79</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Christoph Baldauf</td>
    </tr>
    <tr>
      <td align="center">78</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Michal Klaus</td>
    </tr>
    <tr>
      <td align="center">77</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Benjamin Gutzmann</td>
    </tr>
    <tr>
      <td align="center">76</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">75</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">74</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">73</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Phillip Rak</td>
    </tr>
    <tr>
      <td align="center">72</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Noble Hays</td>
    </tr>
    <tr>
      <td align="center">71</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">70</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">69</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Franz Senftl</td>
    </tr>
    <tr>
      <td align="center">68</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">67</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Thomas Pallmann</td>
    </tr>
    <tr>
      <td align="center">66</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Udo Hickmann</td>
    </tr>
    <tr>
      <td align="center">65</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td>Wolnik Dawid</td>
    </tr>
    <tr>
      <td align="center">64</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">63</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">62</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Thomas Stein</td>
    </tr>
    <tr>
      <td align="center">61</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">60</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">59</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;30.00</td>
      <td>Thomas Eimers</td>
    </tr>
    <tr>
      <td align="center">58</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">57</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Thomas Wegner</td>
    </tr>
    <tr>
      <td align="center">56</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">55</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>wolfgang geisendoerfer</td>
    </tr>
    <tr>
      <td align="center">54</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Arran Gems</td>
    </tr>
    <tr>
      <td align="center">53</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">52</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">51</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">50</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Torge Dellert</td>
    </tr>
    <tr>
      <td align="center">49</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Geert Janssens</td>
    </tr>
    <tr>
      <td align="center">48</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Torge Dellert</td>
    </tr>
    <tr>
      <td align="center">47</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">46</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Johannes Tiemer</td>
    </tr>
    <tr>
      <td align="center">45</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Andreas Grundler</td>
    </tr>
    <tr>
      <td align="center">44</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Frank Hess</td>
    </tr>
    <tr>
      <td align="center">43</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Stefan Schmid</td>
    </tr>
    <tr>
      <td align="center">42</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">41</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">40</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">39</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">38</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">37</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">36</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">35</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">34</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>AR Soft</td>
    </tr>
    <tr>
      <td align="center">33</td>
      <td align="center">11th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Urs Joss</td>
    </tr>
    <tr>
      <td align="center">32</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Enno Richter</td>
    </tr>
    <tr>
      <td align="center">31</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Kjetil Kilhavn</td>
    </tr>
    <tr>
      <td align="center">30</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">29</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Guillaume AUDARD</td>
    </tr>
    <tr>
      <td align="center">28</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">27</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Algot Runeman</td>
    </tr>
    <tr>
      <td align="center">26</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">25</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Simon Heimbach</td>
    </tr>
    <tr>
      <td align="center">24</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Anonymous</td>
    </tr>
    <tr>
      <td align="center">23</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td>Kjetil Koksvik</td>
    </tr>
    <tr>
      <td align="center">22</td>
      <td align="center">10th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Thomas Kleindienst</td>
    </tr>
    <tr>
      <td align="center">21</td>
      <td align="center">9th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Darin Miller</td>
    </tr>
    <tr>
      <td align="center">20</td>
      <td align="center">9th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">19</td>
      <td align="center">7th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Kevin Volkert</td>
    </tr>
    <tr>
      <td align="center">18</td>
      <td align="center">6th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">17</td>
      <td align="center">6th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Bartosz Mikucewicz</td>
    </tr>
    <tr>
      <td align="center">16</td>
      <td align="center">6th August 2015</td>
      <td align="right">€&nbsp;15.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">15</td>
      <td align="center">5th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Adriano Fantini</td>
    </tr>
    <tr>
      <td align="center">14</td>
      <td align="center">5th August 2015</td>
      <td align="right">€&nbsp;70.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">13</td>
      <td align="center">5th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">12</td>
      <td align="center">5th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td>Christos Lazaridis</td>
    </tr>
    <tr>
      <td align="center">11</td>
      <td align="center">5th August 2015</td>
      <td align="right">€&nbsp;5.00</td>
      <td>Marcin S?gol</td>
    </tr>
    <tr>
      <td align="center">10</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Robert Zimmerman</td>
    </tr>
    <tr>
      <td align="center">9</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Karl Ove Hufthammer</td>
    </tr>
    <tr>
      <td align="center">8</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;100.00</td>
      <td>Christian Loosli</td>
    </tr>
    <tr>
      <td align="center">7</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;20.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">6</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;25.00</td>
      <td>Mateusz Kopyto</td>
    </tr>
    <tr>
      <td align="center">5</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Markus Falkner</td>
    </tr>
    <tr>
      <td align="center">4</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Georg Gadinger</td>
    </tr>
    <tr>
      <td align="center">3</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;40.00</td>
      <td><i>Anonymous donation</i></td>
    </tr>
    <tr>
      <td align="center">2</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;10.00</td>
      <td>Harald Sitter</td>
    </tr>
    <tr>
      <td align="center">1</td>
      <td align="center">4th August 2015</td>
      <td align="right">€&nbsp;50.00</td>
      <td>Helio Castro</td>
    </tr>
  </tbody>
</table>
