---
version: "15.12.0"
title: "KDE Applications 15.12.0 Info Page"
announcement: /announcements/announce-applications-15.12
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---

