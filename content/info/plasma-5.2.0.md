---
version: "5.2.0"
title: "Plasma 5.2.0 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.2_Errata
    name: 5.2 Errata
type: info/plasma5
---


This the first release of Plasma 5.2, featuring Plasma Desktop and
other essential software for your desktop computer. Details in the <a href="/announcements/plasma-5.2.0">Plasma 5.2.0 announcement</a>.

For an overview of the differences from Plasma 4 read the initial <a
href="/announcements/plasma5.0/">Plasma 5.0 announcement</a>.

It works with Qt 5.3 or Qt 5.4 but has some bugfixes which work only with Qt 5.4.
