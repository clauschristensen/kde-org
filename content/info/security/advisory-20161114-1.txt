KDE Project Security Advisory
=============================

Title:          KDE neon: insecure package archive
Risk Rating:    Important
Platforms:      All
Versions:       KDE neon packages installed before 16:00UTC 10 November 2016
Author:         Jonathan Riddell <jr@jriddell.org>
Date:           14 November 2016

Overview
========

The package archive used by KDE neon was incorrectly configured
allowing anyone to upload packages to it.  There is no reason to think
that anyone actually did so but as a precaution we have emptied the archives
and removed ISOs built before this date.  The archive is being rebuilt
and ISOs regenerated.

Impact
======

Anyone discovering the insecure archive server could have uploaded
packages to it which would be installed and run on computers running
KDE neon.  We do not believe this has happened but would welcome
reports of any problems.

This does not impact KDE software distributed by any other means, i.e.
other distributions or the source tarballs.

Workaround
==========

None

Solution
========

Upgrade to the latest packages once rebuilt.

All packages have larger version numbers than were previously in the archive.

For extra security reinstall KDE neon from a freshly built ISO.
