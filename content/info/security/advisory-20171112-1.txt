KDE Project Security Advisory
=============================

Title:          Konversation: Crash in IRC message parsing
Risk Rating:    High
CVE:            CVE-2017-15923
Versions:       konversation <= 1.7.2
Date:           12 November 2017


Overview
========
Konversation has support for colors in IRC messages. Any malicious user connected to the
same IRC network can send a carefully crafted message that will crash the Konversation user client.


Workaround
==========
Go to Interface → Colors in the Configure Konversation dialog and uncheck Allow Colored Text in IRC Messages (near the bottom)

Solution
========
Update to Konversation > 1.7.2

Or apply the following patches:
1.7: https://cgit.kde.org/konversation.git/commit/?h=1.7&id=34cc9556c1a089fac6b674d3bd6f2248e9512902
1.6: https://cgit.kde.org/konversation.git/commit/?h=1.6&id=cebf8d7658b0e3afb0292c273704ec4d2ea4019f
1.5: https://cgit.kde.org/konversation.git/commit/?h=1.5&id=6a7f59ee1b9dbc6e5cf9e5f3b306504d02b73ef0
1.4: the patch for 1.5 will apply, but you should upgrade

Credits
=======
Thanks to Joseph Bisch for the report and to Eli MacKenzie for the fix.
