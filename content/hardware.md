---
title: Buy hardware with Plasma and KDE Applications
layout: free
sassFiles:
  - /sass/hardware.scss
---
<div class="container">
  <h1>Buy hardware with Plasma and KDE Applications</h1>

  <p>Here you can find a list of computers with KDE Plasma pre-installed that you can buy right now:</p>
</div>

<div class="container text-center">
  <h1 class="mb-4">KDE Slimbook</h1>
  <h2>Powerful Hardware, Sleek Software</h2>
  <p class="h5 text-md mx-auto" style="max-width: 700px">The first Linux ultrabook with a Ryzen 4000 processor and KDE's full-featured Plasma desktop and hundreds of Open Source programs and utilities.</p>
</div>
<div style="max-width: 1300px" class="w-100 mx-auto">
  <img src="https://kde.slimbook.es/assets/img/duo-ok.png" class="img-fluid" alt="Image showing the two slimbook models">
</div>
<div class="diagonal-box bg-two">
  <section class="container content">
    <h2 class="text-center mb-4">Choose your size</h2>
    <div class="hardware-grid">
      <div class="slimbook-small">
        <img src="https://kde.slimbook.es/assets/img/14.png" aria-hidden="true" alt="KDE Slimbook 13"/>
      </div>
      <div class="slimbook-small-desc h5">
        <h2 class="h3 mt-0">KDE Slimbook 14”</h2>
        <div class="mb-2">AMD Ryzen 7 4800 H</div>
        <div class="mb-2">14-inch IPS LED display with 1920 by 1080 resolution at 60Hz</div>
        <div class="mb-2">Up to 2TB storage SSD</div>
        <div class="mb-2">Up to 64GB memory</div>
        <div class="mb-2">47‑watt‑hour battery</div>
        <div class="mb-3">1 kg</div>
        <div><a href="https://slimbook.es/en/store/slimbook-kde/kde-slimbook-14-comprar">Configure</a></div>
      </div>
      <div class="slimbook">
        <img src="https://kde.slimbook.es/assets/img/15.png" aria-hidden="true" alt="KDE Slimbook 15">
      </div>
      <div class="slimbook-desc h5">
        <h2 class="h3 mt-0">KDE Slimbook 15.6”</h2>
        <div class="mb-2">AMD Ryzen 7 4800 H</div>
        <div class="mb-2">15.6-inch IPS LED display with 1920 by 1080 resolution at 60Hz</div>
        <div class="mb-2">Up to 2TB storage SSD</div>
        <div class="mb-2">Up to 64GB memory</div>
        <div class="mb-2">92‑watt‑hour battery</div>
        <div class="mb-3">1.5 kg</div>
        <div><a href="https://slimbook.es/en/store/slimbook-kde/kde-slimbook-15-comprar">Configure</a></div>
      </div>
    </div>
    <div class="text-center"><a href="https://kde.slimbook.es/specs/" class="learn-more h4 mt-5">See the full comparison</a></div>
  </section>
</div>

<section class="pt-4 pb-5 kfocus diagonal-box">
  <div class="container content">
    <div class="row text-center">
      <div class="col-12">
        <h2 class="h1" id="focus">Kubuntu Focus M2</h2>
        <h3 class="h3">A powerful laptop for daily use or CPU-intensive work</h3>
        <p class="mx-auto" style="max-width: 700px">
          The Kubuntu Focus laptop is a high-powered, workflow-focused laptop which ships with Kubuntu installed. This is the first officially Kubuntu-branded computer. It is designed for work that requires intense GPU computing, fast NVMe storage and optimal cooling to unleash the CPU's maximum potential.<br />
        </p>
      </div>
      <div class="col-12 mt-4">
        <img src="https://kfocus.org/img/m2/front-screen_50-kbd_high_green-color-DSC00710-SMALL.jpg?1601256043" alt="Kubuntu Focus Picture" class="img-fluid">
      </div>
    </div>
    <div>
      <h3>Specifications</h3>
      <p>
        <b>Operating system pre-installed:</b> <a href="https://kubuntu.org">Kubuntu</a><br />
        <b>CPU:</b> 10th Generation Intel Core i7-10875H Processor, 8 core / 16 thread. 2.3GHz Base, 5.1GHz Turbo<br />
        <b>GPU:</b> NVIDIA GeForce RTX 2060/2070/2080 GPU PCIe x 16 AND Intel UHD 630 Graphics with CUDA and cuDNN<br />
        <b>Memory</b> Up to 64GB Dual Channel DDR4 3200 MHz<br />
        <b>Display</b> Full HD 16.1” matte 1080p IPS 144Hz<br />
        <b>Storage</b> 2 x M.2 2280 PCIe Gen3x4 interface featuring Samsung 970 Evo Plus at 3,500MB/s and 2,700MB/s seq. read and write<br />
        <b>Price:</b> starting at $1,795
      </p>
      <a href="https://kfocus.org/" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
    </div>
  </div>
</section>

<section class="diagonal-box bg-purple pt-4 pb-5">
  <div class="container content">
    <div class="row">
      <div class="col-12 col-md-5 col-lg-6">
        <h2 id="pinebook">Pinebook Pro</h2>
        <p>
         The Pinebook Pro is meant to deliver solid day-to-day Linux or *BSD experience and to be a compelling alternative to mid-ranged Chromebooks that people convert into Linux laptops. In contrast to most mid-ranged Chromebooks however, the Pinebook Pro comes with an IPS 1080p 14″ LCD panel, a premium magnesium alloy shell, 64/128GB of eMMC storage, a 10,000 mAh capacity battery and the modularity / hackability that only an open source project can deliver – such as the unpopulated PCIe m.2 NVMe slot. The USB-C port on the Pinebook Pro, apart from being able to transmit data and charge the unit, is also capable of digital video output up-to 4K at 60hz.<br />
        </p>
      </div>
      <div class="col-12 col-md-7 col-lg-6 align-self-center mt-4">
        <img src="/content/hardware/pinebook-pro.png" alt="Pinebook Pro picture" class="laptop-picture">
      </div>
    </div>
    <div>
      <h3>Specifications</h3>
      <p>
        <b>Operating system pre-installed:</b> <a href="https://manjaro.org">Manjaro KDE</a><br />
        <b>CPU:</b> Rockchip RK3399 SOC<br />
        <b>GPU:</b> Mali T860 MP4<br />
        <b>Memory:</b> 4GB LPDDR4 <br />
        <b>Display:</b> 1080p IPS Panel<br />
        <b>Storage:</b> 64GB of eMMC (Upgradable) <br />
        <b>Price:</b> starting at $199
      </p>
        <a href="https://www.pine64.org/pinebook-pro/" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
    </div>
  </div>
</section>

<div class="container py-5">
  <h1>Hardware sellers with KDE Plasma as an option</h1>
  <p>You can find here a list of hardware sellers who offer Plasma as an option when buying the laptop:</p>
</div>

{{< diagonal-box color="blue" href="https://slimbook.es/en" title="Slimbook" src="/content/hardware/slimbook.jpg" >}}

SLIMBOOK was born in 2015 with the idea of being the best brand in the computer
market with GNU / Linux (although it is also verified the absolute compatibility
with Microsoft Windows). They assemble computers searching for excellent quality
and warranty service, at a reasonable price. So much so, that in 2018 they were
awarded Best Open Source Service / Solution Provider at the OpenExpo Europe 2018.

{{< /diagonal-box >}}

{{< diagonal-box color="yellow" href="https://starlabs.systems/" title="Star Labs" src="/content/hardware/starlabs.png" >}}

Star Labs offers a range of laptops designed and built specifically for Linux.
Two version of their laptop are offered: the Star Lite Mk III 11-inch and the 
Star LabTop Mk IV 13-inch.

{{< /diagonal-box >}}

{{< diagonal-box color="green" href="https://www.tuxedocomputers.com/en" title="Tuxedo" src="/content/hardware/Tuxedo.png" >}}

Tuxedo builds tailor-made hardware and all this with Linux!

TUXEDO Computers are individually built computers/PCs and notebooks which are fully Linux compatible, i. e. Linux hardware in tailor-made suit.

All TUXEDO devices are delivered in such a way that you only have to unpack, connect and switch them on. All the computers and notebooks are assembled and installed in-house, not outsourced.

They provide you with self-programmed driver packages, support, installation scripts and everything around our hardware, so that every hardware component really works.

{{< /diagonal-box >}}


{{< diagonal-box href="https://zareason.com/" color="purple" href="https://www.tuxedocomputers.com/en" title="ZaRreason" src="/content/hardware/zareason.jpg" >}}

More than a decade ago, in 2006, ZaReason built their first desktop as part of a volunteer project. In 2007, ZaReason opened for business and their first build went to a PhD Chemistry student at UC Berkeley. Everything grew from there. 

{{< /diagonal-box >}}
