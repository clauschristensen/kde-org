# kde.org

## Clone

```
git clone --depth 5 git@invent.kde.org:websites/kde-org.git
```
This will perform a shallow clone of kde-org.git, reducing the amount of data that needs to be downloaded without usually impacting your workflow. Remove the `--depth 5` part if you want to do a full clone.

## Build websites

You need the latest version of hugo expanded version. It can be found here: https://github.com/gohugoio/hugo/releases

```
hugo serve
```

