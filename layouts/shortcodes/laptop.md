<div class="laptop-with-overlay d-block mt-3 mx-auto" style="max-width: 800px">
  <img class="laptop img-fluid mt-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
  <div class="laptop-overlay">
    <img class="img-fluid mt-3" src="{{ .Get "src" }}" alt="{{ .Get "caption" }}"  loading="lazy" width="800" height="450" />
  </div>
</div>
