<details id="{{ .Get "id" }}">
  <summary class="h4">{{ with .Get "href" }}<a href="{{ . }}">{{ end }}{{ .Get "title" }}{{ with .Get "href" }}</a>{{ end }}</summary>
  {{ .Inner | $.Page.RenderString }}
</details>
